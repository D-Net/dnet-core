package eu.dnetlib.data.collective.transformation.engine.functions;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import eu.dnetlib.common.profile.Resource;
import eu.dnetlib.data.collective.transformation.VocabularyMap;
import eu.dnetlib.data.collective.transformation.VocabularyRegistry;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ConvertTest {

	static final String type_vocabulary = "type_vocabulary.xml";
	static final String lang_vocabulary = "lang_vocabulary.xml";
	Convert c;
	Vocabulary v_type;
	Vocabulary v_lang;
	@Mock
	private transient VocabularyRegistry vocabularyRegistry;
	private transient VocabularyMap vocabularyMapWrapper = new VocabularyMap();
	private transient Map<String, Vocabulary> vocabulariesMap = new HashMap<String, Vocabulary>();
	
	@Before
	public void setUp(){

		c = new Convert();
		v_type = new Vocabulary();
		v_type.setCaseSensitive(false);
		v_type.setResource(getResource(type_vocabulary));
		v_lang = new Vocabulary();
		v_lang.setCaseSensitive(false);
		v_lang.setDelimiter("/");
		v_lang.setResource(getResource(lang_vocabulary));
		v_type.setName("someQuery");
		vocabulariesMap.put(v_type.getVocabularyName(), v_type);
		v_lang.setName("someQuery");
		vocabulariesMap.put(v_lang.getVocabularyName(), v_lang);
		vocabularyMapWrapper.setMap(vocabulariesMap);
		when(vocabularyRegistry.getVocabularies()).thenReturn(vocabularyMapWrapper);		
		c.setVocabularyRegistry(vocabularyRegistry);
		when(vocabularyRegistry.getVocabulary(v_type.getVocabularyName())).thenReturn(v_type);
		when(vocabularyRegistry.getVocabulary(v_lang.getVocabularyName())).thenReturn(v_lang);
	}
	
	private Resource getResource(String vocabularyName){
		Resource r = null;
		SAXReader reader = new SAXReader();
		Document d;
		try {
			d = reader.read(this.getClass().getClassLoader().getResourceAsStream(vocabularyName));
			r = new Resource(d);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return r;
	}
	
	@Test
	public void testTypeEncoding() throws ProcessingException{
		String[] values1 = {"abc"};
		assertEquals("0000", c.executeSingleValue(v_type.getVocabularyName(), Arrays.asList(values1)));
		String[] values2 = {"abc", "Aufsatz"};
		assertEquals("0001", c.executeSingleValue(v_type.getVocabularyName(), Arrays.asList(values2)));
		//String[] values3 = {"Conference report"};
		String[] values3 = {"Conference or workshop item"};
		assertEquals("0004", c.executeSingleValue(v_type.getVocabularyName(), Arrays.asList(values3)));
		
	}
	
	@Test
	public void testLangEncoding() throws ProcessingException{
		String[] values1 = {"he"};
		assertEquals("heb", c.executeSingleValue(v_lang.getVocabularyName(), Arrays.asList(values1)));
		String[] values2 = {"jkjhh"};
		assertEquals("und", c.executeSingleValue(v_lang.getVocabularyName(), Arrays.asList(values2)));		
		String[] values3 = {"german"};
		assertEquals("deu/ger", c.executeSingleValue(v_lang.getVocabularyName(), Arrays.asList(values3)));
		String[] values4 = {"eng"};
		assertEquals("eng", c.executeSingleValue(v_lang.getVocabularyName(), Arrays.asList(values4)));
		String[] values5 = {"ger"};
		assertEquals("deu/ger", c.executeSingleValue(v_lang.getVocabularyName(), Arrays.asList(values5)));
		String[] values6 = {"deu/ger"};
		assertEquals("deu/ger", c.executeSingleValue(v_lang.getVocabularyName(), Arrays.asList(values6)));
	}
}
