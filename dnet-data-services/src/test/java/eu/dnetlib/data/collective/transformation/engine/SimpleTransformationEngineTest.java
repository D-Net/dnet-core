package eu.dnetlib.data.collective.transformation.engine;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.StringReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.transform.TransformerConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import eu.dnetlib.common.profile.ResourceDao;
import eu.dnetlib.data.collective.transformation.VocabularyMap;
import eu.dnetlib.data.collective.transformation.VocabularyRegistry;
import eu.dnetlib.data.collective.transformation.engine.core.TransformationImpl;
import eu.dnetlib.data.collective.transformation.engine.functions.DateVocabulary;
import eu.dnetlib.data.collective.transformation.engine.functions.PersonVocabulary;
import eu.dnetlib.data.collective.transformation.engine.functions.ProcessingException;
import eu.dnetlib.data.collective.transformation.engine.functions.Vocabulary;
import eu.dnetlib.data.collective.transformation.rulelanguage.RuleLanguageParser;

@RunWith(MockitoJUnitRunner.class)
public class SimpleTransformationEngineTest {

	private static final Log log = LogFactory.getLog(SimpleTransformationEngineTest.class);
	
	private static final String xslTemplatePath = "eu/dnetlib/data/collective/transformation/engine/oaftemplate.xsl";
	private transient Resource xslTemplateResource = new ClassPathResource(xslTemplatePath);
	private static final String schemaPath = "eu/dnetlib/data/collective/transformation/schema/DMFSchema_vTransformator.xsd";
	private transient Resource schemaResource = new ClassPathResource(schemaPath);
	
	private static final String xslTemplatePath_oaf = "eu/dnetlib/data/collective/transformation/engine/oaftemplate.xsl";         // OpenAIRE specific
	private transient Resource xslTemplateResource_oaf = new ClassPathResource(xslTemplatePath_oaf);                              // OpenAIRE specific
	private static final String schemaPath_oaf = "eu/dnetlib/data/collective/transformation/schema/OAFSchema_vTransformator.xsd"; // OpenAIRE specific
	private transient Resource schemaResource_oaf = new ClassPathResource(schemaPath_oaf);                                        // OpenAIRE specific

	// class under test
	private transient SimpleTransformationEngine transformationEngine;
	
	private transient TransformationImpl transformation;
	
	private transient TransformationImpl transformationOAF;                                      // OpenAIRE specific
	
	private transient TransformationImpl transformationProvenance;                               // OpenAIRE specific + provenance
	
	private transient TransformationImpl transformationAnyFunderProject;							// OpenAIREplus specific
	
	private transient TransformationImpl transformationWoS;
	
	@Mock
	private transient eu.dnetlib.common.profile.Resource resource;
	@Mock
	private transient ResourceDao resourceDao;
	@Mock
	private transient VocabularyRegistry vocabularyRegistry;
	private transient VocabularyMap vocabularyMapWrapper = new VocabularyMap();
	private transient Map<String, Vocabulary> vocabularies = new HashMap<String, Vocabulary>();
	@Mock
	private transient Vocabulary vocabularyLang;
	
	private transient DateVocabulary vocabularyDate = new DateVocabulary();
	
	private transient PersonVocabulary vocabularyPerson = new PersonVocabulary();

	private transient Vocabulary vocabularyTypes;
	
	private transient Vocabulary vocabularyRights;
	private transient String[] rights = {"info:eu-repo/semantics/openAccess", 
			                             "info:eu-repo/semantics/closedAccess",
			                             "info:eu-repo/semantics/embargoedAccess", 
			                             "info:eu-repo/semantics/restrictedAccess"};
	
	private transient String repositoryId = "profile-123";
	
	private transient String dataSinkId = "dnet://MDStoreDS/4-9c7cf682-849b-48bd-92cf-e65367f38e14_TURTdG9yZURTUmVzb3VyY2VzL01EU3RvcmVEU1Jlc291cmNlVHlwZQ==?type=REFRESH";

	
	@SuppressWarnings("unchecked")
	@Before
	public void setUp() throws TransformerConfigurationException, ProcessingException{
		System.setProperty("javax.xml.transform.TransformerFactory", "net.sf.saxon.TransformerFactoryImpl");
		List<String> rightsOA = new LinkedList<String>();
		rightsOA.add(rights[0]);

		vocabularyTypes = new Vocabulary();
		@SuppressWarnings("rawtypes")
		Map encodingMap = new HashMap();
		encodingMap.put("name", "Article");
		encodingMap.put("encoding", "0001");
		encodingMap.put("code", "0001");
		encodingMap.put("synonyms", Arrays.asList(new String[]{"info:eu-repo/semantics/article"}));
		List<Map<String, ?>> termList = new LinkedList<Map<String,?>>();
		termList.add(encodingMap);
		encodingMap = new HashMap();
		encodingMap.put("name", "Unknown");
		encodingMap.put("encoding", "0000");
		encodingMap.put("code", "0000");
		encodingMap.put("synonyms", Arrays.asList(new String[]{}));
		termList.add(encodingMap);

		vocabularyTypes.setResource(termList);
		
		vocabularyRights = new Vocabulary();
		encodingMap = new HashMap();
		encodingMap.put("name", "");
		encodingMap.put("encoding", "OPEN");
		encodingMap.put("code", "OPEN");
		encodingMap.put("synonyms", Arrays.asList(new String[]{"info:eu-repo/semantics/openAccess"}));
		termList = new LinkedList<Map<String,?>>();
		termList.add(encodingMap);
		vocabularyRights.setResource(termList);
		when(vocabularyRegistry.getVocabularies()).thenReturn(vocabularyMapWrapper);
		when(vocabularyRegistry.getVocabulary("LangVocab")).thenReturn(vocabularyLang);
		when(vocabularyRegistry.getVocabulary("RightsVocab")).thenReturn(vocabularyRights);
		//when(vocabularyRegistry.getVocabulary("DateISO8601")).thenReturn(vocabularyDate);
		//when(vocabularyRegistry.getVocabulary("Person")).thenReturn(vocabularyPerson);
		//when(vocabularyRegistry.getVocabulary("TypesVocab")).thenReturn(vocabularyTypes);
		when(vocabularyLang.encoding(anyList())).thenReturn("Unknown Language");
		//when(vocabularyLang.getName()).thenReturn("someQuery");
		vocabularies.put("LangVocab", vocabularyLang);
		vocabularies.put("RightsVocab", vocabularyRights);                                       // OpenAIRE specific
		vocabularies.put("DateISO8601", vocabularyDate);
		vocabularies.put("Person", vocabularyPerson);
		vocabularies.put("TypesVocab", vocabularyTypes);

		vocabularyMapWrapper.setMap(vocabularies);
		transformationEngine = new SimpleTransformationEngine();
		transformationEngine.setVocabularyRegistry(vocabularyRegistry);
		transformationEngine.setResourceDao(resourceDao);
		transformation = new TransformationImpl();
		transformation.addJobConstant(TransformationImpl.JOBCONST_DATASINKID, dataSinkId);
		transformation.setTemplate(xslTemplateResource);
		transformation.setSchema(schemaResource);
		transformation.init();
		transformation.setRuleLanguageParser(getRuleLanguageParser(getTransformationScript()));
		//transformation.setRootElement("record");
		transformation.configureTransformation();
		// OpenAIRE specific
		transformationOAF = new TransformationImpl();
		transformationOAF.addJobConstant(TransformationImpl.JOBCONST_DATASINKID, dataSinkId);
		transformationOAF.setTemplate(xslTemplateResource_oaf);
		transformationOAF.setSchema(schemaResource_oaf);
		transformationOAF.init();
		transformationOAF.setRuleLanguageParser(getRuleLanguageParser(getOAFTransformationScript()));
		System.out.println("OAF CONFIGURE TRANSFORMATIOn");
		transformationOAF.configureTransformation();
		
		transformationProvenance = new TransformationImpl();
		transformationProvenance.addJobConstant(TransformationImpl.JOBCONST_DATASINKID, dataSinkId);
		transformationProvenance.setTemplate(xslTemplateResource_oaf);
		transformationProvenance.setSchema(schemaResource_oaf);
		transformationProvenance.init();
		transformationProvenance.setRuleLanguageParser(getRuleLanguageParser(getProvenanceTransformationScript() ));
		transformationProvenance.configureTransformation();
		
		transformationAnyFunderProject = new TransformationImpl();
		transformationAnyFunderProject.addJobConstant(TransformationImpl.JOBCONST_DATASINKID, dataSinkId);
		transformationAnyFunderProject.setTemplate(xslTemplateResource_oaf);
		transformationAnyFunderProject.setSchema(schemaResource_oaf);
		transformationAnyFunderProject.init();
//		transformationAnyFunderProject.setRuleLanguageParser(getRuleLanguageParser(getFunderTransformationScript() ));
		transformationAnyFunderProject.setRuleLanguageParser(getRuleLanguageParser(getOpenaireplusCompatibleFunderTransformationScript() ));
		transformationAnyFunderProject.configureTransformation();
		
		transformationWoS = new TransformationImpl();
		transformationWoS.addJobConstant(TransformationImpl.JOBCONST_DATASINKID, dataSinkId);
		transformationWoS.setTemplate(xslTemplateResource_oaf);
		transformationWoS.setSchema(schemaResource_oaf);
		transformationWoS.init();
//		transformationAnyFunderProject.setRuleLanguageParser(getRuleLanguageParser(getFunderTransformationScript() ));
		transformationWoS.setRuleLanguageParser(getRuleLanguageParser(getWosTransformationScript() ));
		transformationWoS.configureTransformation();

		
		String xpathExprOnProfile = "//CONFIGURATION/OFFICIAL_NAME";
		String valueOnProfile = "repositoryOfficialName";
		try{
			when(resourceDao.getResourceByQuery("collection('/db/DRIVER/RepositoryServiceResources')//RESOURCE_PROFILE[.//EXTRA_FIELDS/FIELD[key=\"NamespacePrefix\"][value=\"\"]]")).thenReturn(resource);
			when(resourceDao.getResource(repositoryId)).thenReturn(resource);
			//when(resourceDao.getResourceByQuery("concat('collection()', '')")).thenReturn(resource);
			when(resourceDao.getResourceByQuery("collection()")).thenReturn(resource);			
		}catch(Exception e){
			e.printStackTrace();
		}
		when(resource.getValue(xpathExprOnProfile)).thenReturn(valueOnProfile);
		String xpathExprDataSourceId = "//EXTRA_FIELDS/FIELD[key='OpenAireDataSourceId']/value"; // OpenAIRE specific
		String valueDataSourceId = "opendoar::166";                                              // OpenAIRE specific
		when(resource.getValue(xpathExprDataSourceId)).thenReturn(valueDataSourceId);            // OpenAIRE specific
		String xpathExprDataSourceType = "//EXTRA_FIELDS/FIELD[key='DataSourceType']/value"; // prototype
		String valueDataSourceType = "Aggregator";                                              // prototype
		when(resource.getValue(xpathExprDataSourceType)).thenReturn(valueDataSourceType);            // prototype

	}
	
	@Test
	public void testTransformationWithObjectRecords() throws DocumentException{
		transformationEngine.setTransformation(transformation);
		List<String> mdRecords = new LinkedList<String>();
		mdRecords.add(getMdRecord("obj-132", "md-1", getDC()));
//		mdRecords.add(getMdRecord("obj-132", "md-2", getDidl()));
//		List<String> objRecords = new LinkedList<String>();
//		objRecords.add(getObjectRecord(mdRecords));
		@SuppressWarnings("unused")
		String dump;
		assertNotNull(dump = transformation.dumpStylesheet());
		List<String> transformedMdRecordsResult = new LinkedList<String>();
		for (String srcRecord: mdRecords){
			transformedMdRecordsResult.add(transformationEngine.transform(srcRecord));
		}
//		assertEquals(objRecords.size(), transformedMdRecordsResult.size());
		//System.out.println(dump);
		Document record = (new SAXReader()).read(new StringReader(transformedMdRecordsResult.get(0)));
		assertEquals("", record.valueOf("//*[local-name()='header']/@status"));
		assertNotNull("record contains no header node", record.selectSingleNode("//*[local-name()='header']"));
		assertNotNull("record contains no metadata node", record.selectSingleNode("//*[local-name()='metadata']"));
		assertEquals("http://somehost", record.valueOf("//dc:identifier"));
	}
	
	@Test
	public void testTransformationWithMdRecords() throws DocumentException{
		transformationEngine.setTransformation(transformation);
//		List<String> mdRecords = new LinkedList<String>();
//		mdRecords.add(getMdRecord("obj-132", "md-1==::oai:bla-1", getDC()));
		@SuppressWarnings("unused")
		String dump;
		assertNotNull(dump = transformation.dumpStylesheet());
		System.out.println("DUMP: " + dump);
		
		String transformedRecordResult = transformationEngine.transform(getMdRecord("obj-132", "md-1==::oai:bla-1", getDC()));
//		assertEquals(mdRecords.size(), transformedMdRecordsResult.size());
		Document record = (new SAXReader()).read(new StringReader(transformedRecordResult));
		System.out.println(record.asXML());
//		assertEquals("", record.valueOf("//*[local-name()='header']/@status"));
//		assertNotNull("record contains no header node", record.selectSingleNode("//*[local-name()='header']"));
//		assertNotNull("record contains no metadata node", record.selectSingleNode("//*[local-name()='metadata']"));
//		assertEquals("http://somehost", record.valueOf("//dc:identifier"));
	}
	
	@Test
	public void testTransformationWithMdRecords_oaf_failed() throws DocumentException{
		transformationEngine.setTransformation(transformationOAF);
//		List<String> mdRecords = new LinkedList<String>();
//		mdRecords.add(getMdRecord("obj-132", "md-1", getOAFDC()));
		String dump;
		assertNotNull(dump = transformationOAF.dumpStylesheet());
		//System.out.println(dump);
		String transformedRecordResult = transformationEngine.transform(getMdRecord("obj-132", "md-1", getOAFDC()));
//		assertEquals(mdRecords.size(), transformedMdRecordsResult.size());
		System.out.println("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*\r\n" + dump);
		Document record = (new SAXReader()).read(new StringReader(transformedRecordResult));
		System.out.println(record.asXML());
		assertEquals("", record.valueOf("//*[local-name()='header']/@status"));
		assertNotNull("record contains no header node", record.selectSingleNode("//*[local-name()='header']"));
		assertNotNull("record contains no metadata node", record.selectSingleNode("//*[local-name()='metadata']"));
		assertEquals("failed", record.valueOf("//*[local-name()='header']/@syntaxcheck"));
		//assertEquals("http://somehost", record.valueOf("//*[local-name()='metadata']//*[local-name()='identifier']"));
		//assertEquals("OPEN", record.valueOf("//oaf:accessrights")); // test convert function with rights vocabulary for many dc:rights elements
		//assertEquals("0001", record.valueOf("//dr:CobjCategory"));
	}
	
	@Test
	public void testTransformationOfProjectInformation_oaf() throws DocumentException{
		transformationEngine.setTransformation(transformationAnyFunderProject);
//		List<String> mdRecords = new LinkedList<String>();
//		mdRecords.add(getMdRecordWithProvenance("obj-132", "md-1", getOAFDC(), getProvenance()));
		String dump;
		System.err.println("BEFORE DUMP\r\n");
		assertNotNull(dump = transformationAnyFunderProject.dumpStylesheet());
		String transformedRecordResult = transformationEngine.transform(getMdRecordWithProvenance("obj-132", "md-1", getOAFDC(), getProvenance()));
//		assertEquals(mdRecords.size(), transformedMdRecordsResult.size());
		System.err.println("DUMP\r\n" + dump);
		Document record = (new SAXReader()).read(new StringReader(transformedRecordResult));
		System.out.println(record.asXML());
		assertEquals("", record.valueOf("oaf:projectid"));
	}
	
	@Test
	public void testTransformationOfWos_oaf() throws DocumentException{
		transformationEngine.setTransformation(transformationWoS);
//		List<String> mdRecords = new LinkedList<String>();
//		mdRecords.add(getMdRecordWithProvenance("obj-132", "md-1", getOAFDC(), getProvenance()));
		String dump;
		assertNotNull(dump = transformationWoS.dumpStylesheet());
		String transformedRecordResult = transformationEngine.transform(getMdRecordWithProvenance("obj-132", "md-1", getWOS(), getProvenance()));
//		assertEquals(mdRecords.size(), transformedMdRecordsResult.size());
		System.out.println("___WOS___");
		System.out.println(dump);
		System.out.println("___WOS___");
		Document record = (new SAXReader()).read(new StringReader(transformedRecordResult));
		System.out.println(record.asXML());
		assertEquals("", record.valueOf("oaf:projectid"));
	}

	
	@Test
	public void testTransformationWithMdRecords_provenance() throws DocumentException{
		transformationEngine.setTransformation(transformationProvenance);
//		List<String> mdRecords = new LinkedList<String>();
//		mdRecords.add(getMdRecordWithProvenance("obj-132", "md-1", getOAFDC(), getProvenance() ));
		String dump;
		assertNotNull(dump = transformationProvenance.dumpStylesheet());
		String transformedRecordResult = transformationEngine.transform(getMdRecordWithProvenance("obj-132", "md-1", getOAFDC(), getProvenance() ));
//		assertEquals(mdRecords.size(), transformedMdRecordsResult.size());
		System.out.println(dump);
		Document record = (new SAXReader()).read(new StringReader(transformedRecordResult));
		System.out.println("_______________________________________");
		System.out.println(record.asXML());
		System.out.println("_______________________________________");
		assertEquals("", record.valueOf("//*[local-name()='header']/@status"));
		assertNotNull("record contains no header node", record.selectSingleNode("//*[local-name()='header']"));
		assertNotNull("record contains no metadata node", record.selectSingleNode("//*[local-name()='metadata']"));
		assertEquals("http://somehost", record.valueOf("//dc:identifier"));
		assertEquals("OPEN", record.valueOf("//oaf:accessrights")); // test convert function with rights vocabulary for many dc:rights elements
		
	}

//	@Test
//	public void testTransformationWithSkippedRecord() throws DocumentException{
//		transformationEngine.setTransformation(transformation);
//		List<String> mdRecords = new LinkedList<String>();
//		mdRecords.add(getMdRecord("obj-132", "md-1==::oai:bla-1", getDC()));
//		mdRecords.add(getMdRecord("obj-133", "md-1==::oai:bla-2", getDC2()));
//		List<String> transformedMdRecordsResult = transformationEngine.transform(mdRecords);
//		assertEquals(mdRecords.size() - 1, transformedMdRecordsResult.size());
//		//System.out.println(dump);
//		Document record = (new SAXReader()).read(new StringReader(transformedMdRecordsResult.get(0)));
//		System.out.println(record.asXML());
//		assertEquals("", record.valueOf("//*[local-name()='header']/@status"));
//		assertNotNull("record contains no header node", record.selectSingleNode("//*[local-name()='header']"));
//		assertNotNull("record contains no metadata node", record.selectSingleNode("//*[local-name()='metadata']"));
//		assertEquals("http://somehost", record.valueOf("//dc:identifier"));		
//	}
	
	@Test
	public void testTransformationWithDeletedRecord() throws DocumentException{
		transformationEngine.setTransformation(transformation);
//		List<String> mdRecords = new LinkedList<String>();
//		mdRecords.add(getDeletedMdRecord("obj-132", "md-1"));
		@SuppressWarnings("unused")
		String dump;
		assertNotNull(dump = transformation.dumpStylesheet());
		String transformedRecordResult = transformationEngine.transform(getDeletedMdRecord("obj-132", "md-1"));
//		assertEquals(mdRecords.size(), transformedMdRecordsResult.size());
		//System.out.println(dump);
		Document record = (new SAXReader()).read(new StringReader(transformedRecordResult));
		//System.out.println(record.asXML());
		assertEquals("deleted", record.valueOf("//*[local-name()='header']/@status"));
		assertNotNull("record contains no header node", record.selectSingleNode("//*[local-name()='header']"));
		assertNull("record contains a metadata node", record.selectSingleNode("//*[local-name()='metadata']"));
	}
	
	@Test
	public void testTransformationBehaviourWithEmptyMetadata() throws DocumentException{
		log.debug("applying OAF transformation");
		transformationEngine.setTransformation(transformationOAF);
//		List<String> mdRecords = new LinkedList<String>();
//		mdRecords.add(getEmptyMetadataMdRecord("obj-132", "md-1"));
		@SuppressWarnings("unused")
		String dump;
		assertNotNull(dump = transformationOAF.dumpStylesheet());
		String transformedRecordResult = transformationEngine.transform(getEmptyMetadataMdRecord("obj-132", "md-1"));
//		assertEquals(mdRecords.size(), transformedMdRecordsResult.size());
		log.debug("record output: " + transformedRecordResult);
	}
	
	private RuleLanguageParser getRuleLanguageParser(String aTransformationScript){
		RuleLanguageParser parser = new RuleLanguageParser();
		System.out.println(aTransformationScript);
		StringReader reader = new StringReader(aTransformationScript);
		parser.parse(reader);
		return parser;
	}
	
	private String getTransformationScript(){
		StringBuilder scriptBuilder = new StringBuilder();
		scriptBuilder.append("declare_script \"MainSample\";\r\n");
		scriptBuilder.append("declare_ns dr = \"http://www.driver-repository.eu/namespace/dr\";\r\n");
		scriptBuilder.append("declare_ns dri = \"http://www.driver-repository.eu/namespace/dri\";\r\n");
		scriptBuilder.append("declare_ns dc = \"http://purl.org/dc/elements/1.1/\";\r\n");
		scriptBuilder.append("declare_ns didl = \"urn:mpeg:mpeg21:2002:02-DIDL-NS\";\r\n");
		//scriptBuilder.append("$a1 = Convert(xpath:\"//dc:language[2]\",LangVocab);\r\n");
		//scriptBuilder.append("dri:mdFormat = $a1;\r\n");
		scriptBuilder.append("dr:CobjMDFormats = xpath:\"//dc:title\";\r\n");
		scriptBuilder.append("dr:repositoryName = getValue(PROFILEFIELD, [xpath:\"//dri:repositoryId\", xpath:\"//CONFIGURATION/OFFICIAL_NAME\"]);\r\n");
		scriptBuilder.append("dc:relation = xpath:\"//didl:Resource/@ref\";\r\n");
		scriptBuilder.append("dc:title = copy(\"dc:title\", \"//dc:title\", \"@*|node()\");\r\n");
		//scriptBuilder.append("if xpath:\"//dc:title\" dr:aggregatorName = \"abc\"; else dr:repositoryName = \"def\";\r\n");
		scriptBuilder.append("if xpath:\"//dc:language[4]\" dr:CobjTypology = xpath:\"//dc:language[2]\"; else dr:CobjTypology = \"test\";\r\n");
		scriptBuilder.append("apply xpath:\"//dc:identifier\" if xpath:\"starts-with(normalize-space(.), 'http')\" dc:identifier = xpath:\"normalize-space(.)\"; else dr:CobjIdentifier = xpath:\"normalize-space(.)\";\r\n");
		//scriptBuilder.append("dc:language = Convert(xpath:\"//dc:language[2]\",LangVocab);\r\n");
		scriptBuilder.append("dc:language = Convert(xpath:\"normalize-space(//dc:language[2])\",LangVocab);\r\n");
		scriptBuilder.append("dri:recordIdentifier = RegExpr(xpath:\"//dri:recordIdentifier\", $var1, \"s/^(.*)(::)/$2/\");\r\n");
		scriptBuilder.append("$var0 = \"''\";\r\n");
		scriptBuilder.append("static $var1 = RegExpr($job.datasinkid, $var0, \"s/^(dnet:\\/\\/MDStoreDS\\/)|(\\?.*)//g\");\r\n");
		scriptBuilder.append("if xpath:\"//dc:format[text()='digital']\" dc:publisher = xpath:\"//dc:publisher\"; else dc:publisher = skipRecord();\r\n");
		scriptBuilder.append("end\r\n");
		
		return scriptBuilder.toString();
	}
	
	private String getOAFTransformationScript(){
		StringBuilder scriptBuilder = new StringBuilder();
		scriptBuilder.append("declare_script \"MainSample_OAF\";\r\n");
		scriptBuilder.append("declare_ns oaf = \"http://namespace.openaire.eu/oaf\";\r\n");
		scriptBuilder.append("declare_ns dr = \"http://www.driver-repository.eu/namespace/dr\";\r\n");
		scriptBuilder.append("declare_ns dri = \"http://www.driver-repository.eu/namespace/dri\";\r\n");
		scriptBuilder.append("declare_ns dc = \"http://purl.org/dc/elements/1.1/\";\r\n");
		scriptBuilder.append("oaf:dateAccepted = Convert(xpath:\"descendant-or-self::dc:date\", DateISO8601, \"yyyy-MM-dd\", \"min()\");\r\n");
//		scriptBuilder.append("oaf:dateAccepted = Convert(xpath:\"//dc:date\", DateISO8601);\r\n");
		//scriptBuilder.append("dc:creator = Convert(xpath:\"descendant-or-self::dc:date\", DateISO8601, \"yyyy-MM-dd\", \"min()\");\r\n");
		scriptBuilder.append("apply xpath:\"//dc:date\" if xpath:\"starts-with(normalize-space(.), 'info:eu-repo/date')\" oaf:embargoenddate = RegExpr(xpath:\"normalize-space(.)\", $var0, \"s/^(.*info:eu-repo\\/date\\/embargoEnd\\/)//g\"); else $var0 = \"''\";\r\n");  // retrieve from dc:date , test the info prefix
		scriptBuilder.append("apply xpath:\"//dc:relation\" if xpath:\"string-length(translate(normalize-space(.),'info:eu-repo/grantAgreement/EC/FP7','')) = 5\" oaf:projectid = RegExpr(xpath:\"normalize-space(.)\", $var0, \"s/^(.*info:eu-repo\\/grantAgreement\\/EC\\/FP7\\/)//gm\"); else dc:relation = xpath:\"normalize-space(.)\";\r\n");  // retrieve from dc:relation , test the info prefix
//		scriptBuilder.append("apply xpath:\"//dc:relation\" if xpath:\"starts-with(normalize-space(.), 'info:eu-repo/grantAgreement/EC/FP7')\" oaf:projectid = RegExpr(xpath:\"normalize-space(.)\", $var0, \"s/^(.*info:eu-repo\\/grantAgreement\\/EC\\/FP7\\/)//gm\"); else dc:relation = xpath:\"normalize-space(.)\";\r\n");  // retrieve from dc:relation , test the info prefix
//		scriptBuilder.append("apply xpath:\"//dc:rights\" if xpath:\"starts-with(normalize-space(.), 'info:eu-repo/semantics')\" dc:rights = empty; else dc:rights = xpath:\"normalize-space(.)\";\r\n");  // retrieve from dc:rights, better retrieve from vocabulary
		scriptBuilder.append("oaf:accessrights = Convert(xpath:\"//dc:rights\", RightsVocab);\r\n");
		scriptBuilder.append("oaf:datasourceid = getValue(PROFILEFIELD, [xpath:\"concat('collection()', '')\", xpath:\"//EXTRA_FIELDS/FIELD[key='OpenAireDataSourceId']/value\"]);\r\n"); // retrieve from profile
		scriptBuilder.append("dr:CobjMDFormats = xpath:\"//dc:title\";\r\n");
		scriptBuilder.append("dc:rights = skipRecord();\r\n");
		scriptBuilder.append("dc:language = Convert(xpath:\"//dc:language\", LangVocab);\r\n");
		scriptBuilder.append("$varFulltext = xpath:\"//dc:relation[starts-with(., 'http')]\";\r\n");
		scriptBuilder.append("if xpath:\"//dc:relation[starts-with(., 'http')]\" oaf:fulltext = $varFulltext; else $var0 = \"''\";\r\n");
		scriptBuilder.append("oaf:person = set(xpath:\"//dc:creator\", @normalized = Convert(xpath:\".\", Person););\r\n");
//		scriptBuilder.append("apply xpath:\"//dc:creator\" if xpath:\"string-length(.) > 0\" oaf:person = set(xpath:\".\", @normalized = \"test\";); else dc:creator = xpath:\"normalize-space(.)\";\r\n");
//		scriptBuilder.append("dc:creator = xpath:\"//dc:creator\";\r\n");
//		scriptBuilder.append("apply xpath:\"//dc:creator\" if xpath:\"string-length(.) > 0\"  dc:creator = Convert(xpath:\".\", Person); else $var0 = \"''\";\r\n");
		scriptBuilder.append("$varjournaltitle = \"'some title'\";\r\n");
		scriptBuilder.append("oaf:journal = set($varjournaltitle, @issn=\"1234-5678\"; , @eissn=\"1234-5679\";);\r\n");
		scriptBuilder.append("dr:repositoryName = getValue(PROFILEFIELD, [xpath:\"concat('collection()', '')\", xpath:\"//CONFIGURATION/OFFICIAL_NAME\"]);\r\n");
		//scriptBuilder.append("if xpath:\"//dc:title\" dr:aggregatorName = \"abc\"; else dr:repositoryName = \"def\";\r\n");
		//scriptBuilder.append("if xpath:\"//dc:language[4]\" dr:CobjTypology = xpath:\"//dc:language[2]\"; else dr:CobjTypology = \"test\";\r\n");
		scriptBuilder.append("apply xpath:\"//dc:identifier\" if xpath:\"starts-with(normalize-space(.), 'http')\" dc:identifier = xpath:\"normalize-space(.)\"; else dr:CobjIdentifier = xpath:\"normalize-space(.)\";\r\n");
		//scriptBuilder.append("dc:language = Convert(xpath:\"//dc:language[2]\",LangVocab);\r\n");
		scriptBuilder.append("$var0 = \"''\";\r\n"); // workaround - placeholder for an empty string
		scriptBuilder.append("oaf:hostedBy = set(\"''\", @name = \"hostedName\"; , @id = \"hostedId\";);\r\n");
		scriptBuilder.append("$varId = identifierExtract('[\"//dc:identifier\"]' , xpath:\"./record\" , '(10[.][0-9]{4,}[^\\s\"/<>]*/[^\\s\"<>]+)');\r\n");
		scriptBuilder.append("oaf:identifier = set(xpath:\"$varId//value\", @identifierType = \"doi\";);\r\n");
		scriptBuilder.append("dr:CobjCategory = Convert(xpath:\"//dc:type\",TypesVocab);\r\n");
		
		scriptBuilder.append("end\r\n");
		
		return scriptBuilder.toString();		
	}
	
	private String getProvenanceTransformationScript(){
		StringBuilder scriptBuilder = new StringBuilder();

		scriptBuilder.append("declare_script \"MainSample_Provenance\";\r\n");
		scriptBuilder.append("declare_ns oaf = \"http://namespace.openaire.eu/oaf\";\r\n");
		scriptBuilder.append("declare_ns dr = \"http://www.driver-repository.eu/namespace/dr\";\r\n");
		scriptBuilder.append("declare_ns dri = \"http://www.driver-repository.eu/namespace/dri\";\r\n");
		scriptBuilder.append("declare_ns dc = \"http://purl.org/dc/elements/1.1/\";\r\n");
		scriptBuilder.append("declare_ns prov = \"http://www.openarchives.org/OAI/2.0/provenance\";\r\n");
//		scriptBuilder.append("dc:dateAccepted = Convert(xpath:\"//dc:date\", DateISO8601);\r\n");
		//scriptBuilder.append("dc:creator = Convert(xpath:\"descendant-or-self::dc:date\", DateISO8601, \"yyyy-MM-dd\", \"min()\");\r\n");
		//scriptBuilder.append("dc:creator = xpath:\"//dc:creator\";\r\n");
		scriptBuilder.append("apply xpath:\"//dc:date\" if xpath:\"starts-with(normalize-space(.), 'info:eu-repo/date')\" oaf:embargoenddate = RegExpr(xpath:\"normalize-space(.)\", $var0, \"s/^(.*info:eu-repo\\/date\\/embargoEnd\\/)//g\"); else $var0 = \"''\";\r\n");  // retrieve from dc:date , test the info prefix
		scriptBuilder.append("apply xpath:\"//dc:relation\" if xpath:\"starts-with(normalize-space(.), 'info:eu-repo/grantAgreement/EC/FP7')\" oaf:projectid = RegExpr(xpath:\"normalize-space(.)\", $var0, \"s/^(.*info:eu-repo\\/grantAgreement\\/EC\\/FP7\\/)//gm\"); else dc:relation = xpath:\"normalize-space(.)\";\r\n");  // retrieve from dc:relation , test the info prefix
//		scriptBuilder.append("apply xpath:\"//dc:rights\" if xpath:\"starts-with(normalize-space(.), 'info:eu-repo/semantics')\" dc:rights = empty; else dc:rights = xpath:\"normalize-space(.)\";\r\n");  // retrieve from dc:rights, better retrieve from vocabulary
		scriptBuilder.append("oaf:accessrights = Convert(xpath:\"//dc:rights\", RightsVocab);\r\n");
		scriptBuilder.append("oaf:datasourceid = getValue(PROFILEFIELD, [xpath:\"concat('collection()', '')\", xpath:\"//EXTRA_FIELDS/FIELD[key='OpenAireDataSourceId']/value\"]);\r\n"); // retrieve from profile
		scriptBuilder.append("static $varDsType = getValue(PROFILEFIELD, [xpath:\"concat('collection()', '')\", xpath:\"//EXTRA_FIELDS/FIELD[key='DataSourceType']/value\"]);\r\n");
		scriptBuilder.append("if xpath:\"$varDsType='Aggregator'\" oaf:hostingDatasourceid = xpath:\"//prov:baseURL\"; else oaf:hostingDatasourceid = getValue(PROFILEFIELD, [xpath:\"concat('collection()', '')\", xpath:\"//EXTRA_FIELDS/FIELD[key='OpenAireDataSourceId']/value\"]);\r\n");
		scriptBuilder.append("oaf:collectedDatasourceid = getValue(PROFILEFIELD, [xpath:\"concat('collection()', '')\", xpath:\"//EXTRA_FIELDS/FIELD[key='OpenAireDataSourceId']/value\"]);\r\n");
		scriptBuilder.append("oaf:concept = \"'CONCEPT'\";\r\n");
		scriptBuilder.append("dr:CobjMDFormats = xpath:\"//dc:title\";\r\n");
		scriptBuilder.append("dc:language = Convert(xpath:\"//dc:language\", LangVocab);\r\n");
		scriptBuilder.append("%myTemplate = split(xpath:\"//dc:creator/text()\", \"dc:creator\", \";\");\r\n");
		scriptBuilder.append("dr:repositoryName = getValue(PROFILEFIELD, [xpath:\"concat('collection()', '')\", xpath:\"//CONFIGURATION/OFFICIAL_NAME\"]);\r\n");
		//scriptBuilder.append("if xpath:\"//dc:title\" dr:aggregatorName = \"abc\"; else dr:repositoryName = \"def\";\r\n");
		//scriptBuilder.append("if xpath:\"//dc:language[4]\" dr:CobjTypology = xpath:\"//dc:language[2]\"; else dr:CobjTypology = \"test\";\r\n");
		scriptBuilder.append("apply xpath:\"//dc:identifier\" if xpath:\"starts-with(normalize-space(.), 'http')\" dc:identifier = xpath:\"normalize-space(.)\"; else dr:CobjIdentifier = xpath:\"normalize-space(.)\";\r\n");
		//scriptBuilder.append("dc:language = Convert(xpath:\"//dc:language[2]\",LangVocab);\r\n");
		scriptBuilder.append("$var0 = \"''\";\r\n"); // workaround - placeholder for an empty string
		scriptBuilder.append("end\r\n");
		
		return scriptBuilder.toString();		
	}
	
	private String getWosTransformationScript(){
		StringBuilder scriptBuilder = new StringBuilder();

		scriptBuilder.append("declare_script \"MainSample\";\r\n");
		scriptBuilder.append("declare_ns oaf = \"http://namespace.openaire.eu/oaf\";\r\n");
		scriptBuilder.append("declare_ns dr = \"http://www.driver-repository.eu/namespace/dr\";\r\n");
		scriptBuilder.append("declare_ns dri = \"http://www.driver-repository.eu/namespace/dri\";\r\n");
		scriptBuilder.append("declare_ns dc = \"http://purl.org/dc/elements/1.1/\";\r\n");
		scriptBuilder.append("declare_ns prov = \"http://www.openarchives.org/OAI/2.0/provenance\";\r\n");
		// header
		scriptBuilder.append("dri:objIdentifier = xpath:\"//dri:objIdentifier\";\r\n");
		scriptBuilder.append("dri:recordIdentifier = xpath:\"//csvRecord/row[@name='UT']\";\r\n");
		scriptBuilder.append("dri:dateOfCollection = xpath:\"//dri:dateOfCollection\";\r\n");
//		scriptBuilder.append("dri:repositoryId;\r\n");
//		scriptBuilder.append("dri:datasourceprefix;\r\n");
		// metadata
		scriptBuilder.append("dc:language = Convert(xpath:\"//csvRecord/row[@name='LA']\", LangVocab);\r\n");
		scriptBuilder.append("dc:title = xpath:\"//csvRecord/row[@name='TI']\";\r\n");
		scriptBuilder.append("%myTemplate = split(xpath:\"//csvRecord/row[@name='AF']/text()\", \"dc:creator\", \";\");\r\n");
		scriptBuilder.append("$varIssn = xpath:\"//csvRecord/row[@name='SN']\";");
		scriptBuilder.append("oaf:journal = set(xpath:\"//csvRecord/row[@name='SO']\", @issn = $varIssn;);\r\n");
//		scriptBuilder.append("dr:CobjCategory = Convert(xpath:\"//csvRecord/row[@name='DT']\", TextTypologies);\r\n");
		scriptBuilder.append("dr:CobjCategory = Convert(xpath:\"//csvRecord/row[@name='DT']\", LangVocab);\r\n");
		scriptBuilder.append("dc:subject = xpath:\"//csvRecord/row[@name='ID']\";\r\n");
		scriptBuilder.append("dc:description = xpath:\"//csvRecord/row[@name='AB']\";\r\n");
		scriptBuilder.append("dc:publisher = xpath:\"//csvRecord/row[@name='PU']\";\r\n");
		scriptBuilder.append("dc:dateAccepted = xpath:\"//csvRecord/row[@name='PY']\";\r\n");
		scriptBuilder.append("$varDoi = xpath:\"concat('http://dx.doi.org/', normalize-space(//csvRecord/row[@name='DI']))\";\r\n");
		scriptBuilder.append("dc:identifier = $varDoi;\r\n");
		scriptBuilder.append("$varPart1 = xpath:\"concat('Test', 'No.2')\";\r\n");
		scriptBuilder.append("dc:subject = $varPart1;\r\n");
		scriptBuilder.append("$varIfTest = xpath:\"//dc:creator\";\r\n");
		scriptBuilder.append("if xpath:\"count($varIfTest) &gt; 0\" dc:subject = \"'yes'\"; else dc:subject = \"'no'\";\r\n");
		scriptBuilder.append("oaf:identifier = set(xpath:\"//csvRecord/row[@name='DI']\", @identifierType = \"doi\";);\r\n");
		scriptBuilder.append("oaf:fundingunit = xpath:\"//csvRecord/row[@name='FU']\";\r\n");
		scriptBuilder.append("oaf:fundingtext = xpath:\"//csvRecord/row[@name='FX']\";\r\n");
		// adapt the attribute values for name and id
		scriptBuilder.append("oaf:hostedBy = set(\"''\", @name=\"Unknown Repository\";, @id=\"openaire____::55045bd2a65019fd8e6741a755395c8c\";);\r\n");
		scriptBuilder.append("oaf:collectedFrom = set(\"''\", @name=\"Unknown Repository\";, @id=\"openaire____::55045bd2a65019fd8e6741a755395c8c\";);\r\n");

		scriptBuilder.append("end\r\n");
		return scriptBuilder.toString();		
	}

	private String getFunderTransformationScript(){
		StringBuilder scriptBuilder = new StringBuilder();

		scriptBuilder.append("declare_script \"MainSample\";\r\n");
		scriptBuilder.append("declare_ns oaf = \"http://namespace.openaire.eu/oaf\";\r\n");
		scriptBuilder.append("declare_ns dr = \"http://www.driver-repository.eu/namespace/dr\";\r\n");
		scriptBuilder.append("declare_ns dri = \"http://www.driver-repository.eu/namespace/dri\";\r\n");
		scriptBuilder.append("declare_ns dc = \"http://purl.org/dc/elements/1.1/\";\r\n");
		scriptBuilder.append("declare_ns prov = \"http://www.openarchives.org/OAI/2.0/provenance\";\r\n");
//		scriptBuilder.append("dc:dateAccepted = Convert(xpath:\"//dc:date\", DateISO8601);\r\n");
		//scriptBuilder.append("dc:creator = Convert(xpath:\"descendant-or-self::dc:date\", DateISO8601, \"yyyy-MM-dd\", \"min()\");\r\n");
		//scriptBuilder.append("dc:creator = xpath:\"//dc:creator\";\r\n");
		scriptBuilder.append("apply xpath:\"//dc:date\" if xpath:\"starts-with(normalize-space(.), 'info:eu-repo/date')\" oaf:embargoenddate = RegExpr(xpath:\"normalize-space(.)\", $var0, \"s/^(.*info:eu-repo\\/date\\/embargoEnd\\/)//g\"); else $var0 = \"''\";\r\n");  // retrieve from dc:date , test the info prefix
		scriptBuilder.append("apply xpath:\"//dc:relation\" if xpath:\"starts-with(normalize-space(.), 'info:eu-repo/grantAgreement/EC/FP7')\" oaf:projectid = RegExpr(xpath:\"normalize-space(.)\", $var0, \"/info:eu-repo/grantAgreement/([A-Za-z]+)/(.*)/([0-9]+)/\"); else dc:relation = xpath:\"normalize-space(.)\";\r\n");  // retrieve from dc:relation , test the info prefix
//		scriptBuilder.append("apply xpath:\"//dc:rights\" if xpath:\"starts-with(normalize-space(.), 'info:eu-repo/semantics')\" dc:rights = empty; else dc:rights = xpath:\"normalize-space(.)\";\r\n");  // retrieve from dc:rights, better retrieve from vocabulary
		scriptBuilder.append("oaf:accessrights = Convert(xpath:\"//dc:rights\", RightsVocab);\r\n");
		scriptBuilder.append("oaf:datasourceid = getValue(PROFILEFIELD, [xpath:\"concat('collection(')\", xpath:\"//EXTRA_FIELDS/FIELD[key='OpenAireDataSourceId']/value\"]);\r\n"); // retrieve from profile
		scriptBuilder.append("static $varDsType = getValue(PROFILEFIELD, [xpath:\"concat('collection(')\", xpath:\"//EXTRA_FIELDS/FIELD[key='DataSourceType']/value\"]);\r\n");
		scriptBuilder.append("if xpath:\"$varDsType='Aggregator'\" oaf:hostingDatasourceid = xpath:\"//prov:baseURL\"; else oaf:hostingDatasourceid = getValue(PROFILEFIELD, [xpath:\"//dri:repositoryId\", xpath:\"//EXTRA_FIELDS/FIELD[key='OpenAireDataSourceId']/value\"]);\r\n");
		scriptBuilder.append("oaf:collectedDatasourceid = getValue(PROFILEFIELD, [xpath:\"concat('collection(')\", xpath:\"//EXTRA_FIELDS/FIELD[key='OpenAireDataSourceId']/value\"]);\r\n");
		scriptBuilder.append("dr:CobjMDFormats = xpath:\"//dc:title\";\r\n");
		scriptBuilder.append("dc:language = Convert(xpath:\"//dc:language\", LangVocab);\r\n");
		scriptBuilder.append("%myTemplate = split(xpath:\"//dc:creator/text()\", \"dc:creator\", \";\");\r\n");
		scriptBuilder.append("dr:repositoryName = getValue(PROFILEFIELD, [xpath:\"concat('collection(')\", xpath:\"//CONFIGURATION/OFFICIAL_NAME\"]);\r\n");
		//scriptBuilder.append("if xpath:\"//dc:title\" dr:aggregatorName = \"abc\"; else dr:repositoryName = \"def\";\r\n");
		//scriptBuilder.append("if xpath:\"//dc:language[4]\" dr:CobjTypology = xpath:\"//dc:language[2]\"; else dr:CobjTypology = \"test\";\r\n");
		scriptBuilder.append("apply xpath:\"//dc:identifier\" if xpath:\"starts-with(normalize-space(.), 'http')\" dc:identifier = xpath:\"normalize-space(.)\"; else dr:CobjIdentifier = xpath:\"normalize-space(.)\";\r\n");
		//scriptBuilder.append("dc:language = Convert(xpath:\"//dc:language[2]\",LangVocab);\r\n");
		scriptBuilder.append("$var0 = \"''\";\r\n"); // workaround - placeholder for an empty string
		scriptBuilder.append("$varPmc = \"'PMC:123456'\";\r\n"); 		
		scriptBuilder.append("oaf:identifier = set($varPmc, @identifierType = \"pmc\";);");
		scriptBuilder.append("end\r\n");
		
		return scriptBuilder.toString();		
	}
	
	private String getOpenaireplusCompatibleFunderTransformationScript(){
		StringBuilder scriptBuilder = new StringBuilder();

		scriptBuilder.append("declare_script \"MainSample\";\r\n");
		scriptBuilder.append("declare_ns oaf = \"http://namespace.openaire.eu/oaf\";\r\n");
		scriptBuilder.append("declare_ns dr = \"http://www.driver-repository.eu/namespace/dr\";\r\n");
		scriptBuilder.append("declare_ns dri = \"http://www.driver-repository.eu/namespace/dri\";\r\n");
		scriptBuilder.append("declare_ns dc = \"http://purl.org/dc/elements/1.1/\";\r\n");
		scriptBuilder.append("declare_ns prov = \"http://www.openarchives.org/OAI/2.0/provenance\";\r\n");
//		scriptBuilder.append("dc:dateAccepted = Convert(xpath:\"//dc:date\", DateISO8601);\r\n");
		//scriptBuilder.append("dc:creator = Convert(xpath:\"descendant-or-self::dc:date\", DateISO8601, \"yyyy-MM-dd\", \"min()\");\r\n");
		//scriptBuilder.append("dc:creator = xpath:\"//dc:creator\";\r\n");
		scriptBuilder.append("apply xpath:\"//dc:date\" if xpath:\"starts-with(normalize-space(.), 'info:eu-repo/date')\" oaf:embargoenddate = RegExpr(xpath:\"normalize-space(.)\", $var0, \"s/^(.*info:eu-repo\\/date\\/embargoEnd\\/)//g\"); else $var0 = \"''\";\r\n");  // retrieve from dc:date , test the info prefix
//		String regExpr = "s/^.*info:eu-repo\\/grantAgreement\\/EC\\/FP7\\/([0-9]+).*/$1/gm";
		String arg = "$1"; // TODO
//		scriptBuilder.append("$varPrj0 = RegExpr(xpath:\"//dc:relation[0][starts-with(normalize-space(.), 'info:eu-repo/grantAgreement/EC/FP7')]\", $var0, \"s/^.*info:eu-repo\\/grantAgreement\\/EU\\/FP7\\/([0-9]+)//gm\");");
		scriptBuilder.append("$varCorda = \"'corda_______::$1'\";\r\n");

		String regExpr = "s/^.*info:eu-repo\\/grantAgreement\\/EC\\/FP7\\/(\\d\\d\\d\\d\\d\\d).*/$1/gm";
		scriptBuilder.append("$varArg = \"'$1'\";\r\n");
		scriptBuilder.append("$varPrj1 = " +
"RegExpr(xpath:\"//dc:relation[1][starts-with(normalize-space(.), 'info:eu-repo/grantAgreement/EC/FP7')]\", " +
"$varCorda, \"s/^.*info:eu-repo\\/grantAgreement\\/EC\\/FP7\\/(\\d\\d\\d\\d\\d\\d).*//gm\");\r\n");
		scriptBuilder.append("$varPrj2 = " +
"RegExpr(xpath:\"//dc:relation[2][starts-with(normalize-space(.), 'info:eu-repo/grantAgreement/EC/FP7')]\", " +
"$varCorda, \"s/^.*info:eu-repo\\/grantAgreement\\/EC\\/FP7\\/(\\d\\d\\d\\d\\d\\d).*//gm\");\r\n");
		scriptBuilder.append("$varPrj3 = " +
"RegExpr(xpath:\"//dc:relation[3][starts-with(normalize-space(.), 'info:eu-repo/grantAgreement/EC/FP7')]\", " +
"$varCorda, \"s/^.*info:eu-repo\\/grantAgreement\\/EC\\/FP7\\/(\\d\\d\\d\\d\\d\\d).*//gm\");\r\n");
		scriptBuilder.append("$varPrj4 = " +
"RegExpr(xpath:\"//dc:relation[4][starts-with(normalize-space(.), 'info:eu-repo/grantAgreement/EC/FP7')]\", " +
"$varCorda, \"s/^.*info:eu-repo\\/grantAgreement\\/EC\\/FP7\\/(\\d\\d\\d\\d\\d\\d).*//gm\");\r\n");
		scriptBuilder.append("$varPrj5 = " +
"RegExpr(xpath:\"//dc:relation[5][starts-with(normalize-space(.), 'info:eu-repo/grantAgreement/EC/FP7')]\", " +
"$varCorda, \"s/^.*info:eu-repo\\/grantAgreement\\/EC\\/FP7\\/(\\d\\d\\d\\d\\d\\d).*//gm\");\r\n");
		scriptBuilder.append("$varPrj6 = " +
"RegExpr(xpath:\"//dc:relation[6][starts-with(normalize-space(.), 'info:eu-repo/grantAgreement/EC/FP7')]\", " +
"$varCorda, \"s/^.*info:eu-repo\\/grantAgreement\\/EC\\/FP7\\/(\\d\\d\\d\\d\\d\\d).*//gm\");\r\n");
		scriptBuilder.append("if xpath:\"string-length($varPrj1) = 20\" oaf:projectid = $varPrj1; else $var0 = \"''\";\r\n");
		scriptBuilder.append("if xpath:\"string-length($varPrj2) = 20\" oaf:projectid = $varPrj2; else $var0 = \"''\";\r\n");
		scriptBuilder.append("if xpath:\"string-length($varPrj3) = 20\" oaf:projectid = $varPrj3; else $var0 = \"''\";\r\n");
		scriptBuilder.append("if xpath:\"string-length($varPrj4) = 20\" oaf:projectid = $varPrj4; else $var0 = \"''\";\r\n");
		scriptBuilder.append("if xpath:\"string-length($varPrj5) = 20\" oaf:projectid = $varPrj5; else $var0 = \"''\";\r\n");
		scriptBuilder.append("if xpath:\"string-length($varPrj6) = 20\" oaf:projectid = $varPrj6; else $var0 = \"''\";\r\n");
//		scriptBuilder.append("apply xpath:\"//dc:relation[starts-with(., 'info:eu-repo/grantAgreement')]\" if xpath:\"string-length() = 6\" oaf:projectid = RegExpr(xpath:\"normalize-space(.)\", $var0, \"/info:eu-repo/grantAgreement/([A-Za-z]+)/(.*)/([0-9]+)/\"); else dc:relation = xpath:\"normalize-space(.)\";\r\n");  // retrieve from dc:relation , test the info prefix
//		scriptBuilder.append("apply xpath:\"//dc:relation\" if xpath:\"starts-with(normalize-space(.), 'info:eu-repo/grantAgreement/EC/FP7')\" oaf:projectid = RegExpr(xpath:\"normalize-space(.)\", $var0, \"/info:eu-repo/grantAgreement/([A-Za-z]+)/(.*)/([0-9]+)/\"); else dc:relation = xpath:\"normalize-space(.)\";\r\n");  // retrieve from dc:relation , test the info prefix
//		scriptBuilder.append("apply xpath:\"//dc:rights\" if xpath:\"starts-with(normalize-space(.), 'info:eu-repo/semantics')\" dc:rights = empty; else dc:rights = xpath:\"normalize-space(.)\";\r\n");  // retrieve from dc:rights, better retrieve from vocabulary
		scriptBuilder.append("oaf:accessrights = Convert(xpath:\"//dc:rights\", RightsVocab);\r\n");
		scriptBuilder.append("oaf:datasourceid = getValue(PROFILEFIELD, [xpath:\"concat('collection()', '')\", xpath:\"//EXTRA_FIELDS/FIELD[key='OpenAireDataSourceId']/value\"]);\r\n"); // retrieve from profile
		scriptBuilder.append("static $varDsType = getValue(PROFILEFIELD, [xpath:\"concat('collection()', '')\", xpath:\"//EXTRA_FIELDS/FIELD[key='DataSourceType']/value\"]);\r\n");
		scriptBuilder.append("if xpath:\"$varDsType='Aggregator'\" oaf:hostingDatasourceid = xpath:\"//prov:baseURL\"; else oaf:hostingDatasourceid = getValue(PROFILEFIELD, [xpath:\"concat('collection()', '')\", xpath:\"//EXTRA_FIELDS/FIELD[key='OpenAireDataSourceId']/value\"]);\r\n");
		scriptBuilder.append("oaf:collectedDatasourceid = getValue(PROFILEFIELD, [xpath:\"concat('collection()', '')\", xpath:\"//EXTRA_FIELDS/FIELD[key='OpenAireDataSourceId']/value\"]);\r\n");
		scriptBuilder.append("dr:CobjMDFormats = xpath:\"//dc:title\";\r\n");
		scriptBuilder.append("dc:language = Convert(xpath:\"//dc:language\", LangVocab);\r\n");
		scriptBuilder.append("%myTemplate = split(xpath:\"//dc:creator/text()\", \"dc:creator\", \";\");\r\n");
		scriptBuilder.append("dr:repositoryName = getValue(PROFILEFIELD, [xpath:\"concat('collection()', '')\", xpath:\"//CONFIGURATION/OFFICIAL_NAME\"]);\r\n");
		//scriptBuilder.append("if xpath:\"//dc:title\" dr:aggregatorName = \"abc\"; else dr:repositoryName = \"def\";\r\n");
		//scriptBuilder.append("if xpath:\"//dc:language[4]\" dr:CobjTypology = xpath:\"//dc:language[2]\"; else dr:CobjTypology = \"test\";\r\n");
		scriptBuilder.append("apply xpath:\"//dc:identifier\" if xpath:\"starts-with(normalize-space(.), 'http')\" dc:identifier = xpath:\"normalize-space(.)\"; else dr:CobjIdentifier = xpath:\"normalize-space(.)\";\r\n");
		//scriptBuilder.append("dc:language = Convert(xpath:\"//dc:language[2]\",LangVocab);\r\n");
		scriptBuilder.append("$var0 = \"''\";\r\n"); // workaround - placeholder for an empty string
		scriptBuilder.append("$varPmc = \"'PMC:123456'\";\r\n"); 		
		scriptBuilder.append("oaf:identifier = set($varPmc, @identifierType = \"pmc\";);");
		scriptBuilder.append("end\r\n");
		
		return scriptBuilder.toString();		
	}


	private String getObjectRecord(List<String> mdRecords){
		StringBuilder builder = new StringBuilder();
		builder.append("<objectRecord>");
		for (String record: mdRecords) builder.append(record);
		builder.append("</objectRecord>");
		return builder.toString();
	}
	
	private String getMdRecord(String objIdentifier, String recordIdentifier, String metadata){
		StringBuilder builder = new StringBuilder();
		builder.append("<record xmlns:dri=\"http://www.driver-repository.eu/namespace/dri\">");
		builder.append("<header>");
		builder.append("<dri:objIdentifier>");
		builder.append(objIdentifier);
		builder.append("</dri:objIdentifier>");
		builder.append("<dri:repositoryId>profile-123</dri:repositoryId>");
		builder.append("<dri:recordIdentifier>");
		builder.append(recordIdentifier);
		builder.append("</dri:recordIdentifier>");
		builder.append("<dri:dateOfCollection>2009-09-30T13:08:57Z</dri:dateOfCollection>");
		builder.append("<dri:mdFormat/>");
		builder.append("<dri:mdFormatInterpretation/>");
		builder.append("<dri:repositoryId>71f5069a-9ea2-41fa-968a-4f69a5722ad0_UmVwb3NpdG9yeVNlcnZpY2VSZXNvdXJjZXMvUmVwb3NpdG9yeVNlcnZpY2VSZXNvdXJjZVR5cGU=</dri:repositoryId>");
		builder.append("</header><metadata>");
		builder.append(metadata);
		builder.append("</metadata>");
		builder.append("</record>");
		return builder.toString();
	}
	
	private String getMdRecordWithProvenance(String objIdentifier, String recordIdentifier, String metadata, String provenance){
		StringBuilder builder = new StringBuilder();
		builder.append("<record xmlns:dri=\"http://www.driver-repository.eu/namespace/dri\">");
		builder.append("<header>");
		builder.append("<dri:objIdentifier>");
		builder.append(objIdentifier);
		builder.append("</dri:objIdentifier>");
		builder.append("<dri:repositoryId>profile-123</dri:repositoryId>");
		builder.append("<dri:recordIdentifier>");
		builder.append(recordIdentifier);
		builder.append("</dri:recordIdentifier>");
		builder.append("<dri:dateOfCollection>2009-09-30T13:08:57Z</dri:dateOfCollection>");
		builder.append("<dri:mdFormat/>");
		builder.append("<dri:mdFormatInterpretation/>");
		builder.append("<dri:repositoryId>71f5069a-9ea2-41fa-968a-4f69a5722ad0_UmVwb3NpdG9yeVNlcnZpY2VSZXNvdXJjZXMvUmVwb3NpdG9yeVNlcnZpY2VSZXNvdXJjZVR5cGU=</dri:repositoryId>");
		builder.append("</header><metadata>");
		builder.append(metadata);
		builder.append("</metadata>");
		builder.append("<about>");
		builder.append(provenance);
		builder.append("</about>");
		builder.append("</record>");
		return builder.toString();		
	}
	
	private String getDeletedMdRecord(String objIdentifier, String recordIdentifier){
		StringBuilder builder = new StringBuilder();
		builder.append("<record xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:dri=\"http://www.driver-repository.eu/namespace/dri\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\">");
		builder.append("<header status=\"deleted\">");
		builder.append("<dri:objIdentifier>");
		builder.append(objIdentifier);
		builder.append("</dri:objIdentifier>");
		builder.append("<dri:repositoryId>profile-123</dri:repositoryId>");
		builder.append("<dri:recordIdentifier>");
		builder.append(recordIdentifier);
		builder.append("</dri:recordIdentifier>");
		builder.append("<dri:dateOfCollection>2009-09-30T13:08:57Z</dri:dateOfCollection>");
		builder.append("<dri:mdFormat/>");
		builder.append("<dri:mdFormatInterpretation/>");
		builder.append("<dri:repositoryId>71f5069a-9ea2-41fa-968a-4f69a5722ad0_UmVwb3NpdG9yeVNlcnZpY2VSZXNvdXJjZXMvUmVwb3NpdG9yeVNlcnZpY2VSZXNvdXJjZVR5cGU=</dri:repositoryId>");
		builder.append("</header>");
		builder.append("</record>");		
		return builder.toString();
	}
	
	private String getEmptyMetadataMdRecord(String objIdentifier, String recordIdentifier){
		// this is an exception case
		StringBuilder builder = new StringBuilder();
		builder.append("<record>");
		builder.append("<header xmlns:oai=\"http://www.openarchives.org/OAI/2.0/\" xmlns:dri=\"http://www.driver-repository.eu/namespace/dri\">");
		builder.append("<dri:objIdentifier>");
		builder.append(objIdentifier);
		builder.append("</dri:objIdentifier>");
		builder.append("<dri:recordIdentifier>");
		builder.append(recordIdentifier);
		builder.append("</dri:recordIdentifier>");
		builder.append("<dri:dateOfCollection>2011-03-29T08:41:48Z</dri:dateOfCollection>");
		builder.append("<dri:repositoryId>profile-123</dri:repositoryId>");
		builder.append("<identifier>oai:openaire.cern.ch:8</identifier>");
		builder.append("<datestamp>2010-12-11T19:14:26Z</datestamp>");
		builder.append("<setSpec>EC_fundedresources</setSpec>");
		builder.append("</header>");
		builder.append("<metadata>");
		builder.append("</metadata>");
		builder.append("</record>");
		return builder.toString();
	}
	
	private String getDC(){
		StringBuilder builder = new StringBuilder();
		builder.append("<oai_dc:dc xmlns:oai_dc=\"http://www.openarchives.org/OAI/2.0/oai_dc/\" xmlns=\"http://www.openarchives.org/OAI/2.0/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xsi:schemaLocation=\"http://www.openarchives.org/OAI/2.0/oai_dc/  http://www.openarchives.org/OAI/2.0/oai_dc.xsd\">");
		builder.append("<dc:title xml:lang=\"en\">SomeTitle</dc:title>");
		builder.append("<dc:title xml:lang=\"de\">Irgendein Titel</dc:title>");
		//builder.append("<title>SomeTitle</title>");
		builder.append("<dc:language>firstLang</dc:language>");
		builder.append("<dc:language>   \r\n" + "middleLang   </dc:language>");
		builder.append("<dc:language>lastLang</dc:language>");
		builder.append("<dc:creator>Any Author</dc:creator>");
		builder.append("<dc:contributor>First Contributor</dc:contributor>");
		builder.append("<dc:contributor>Second Contributor</dc:contributor>");
		builder.append("<dc:format>9</dc:format>");
		builder.append("<dc:format>application/pdf</dc:format>");
		builder.append("<dc:format>digital</dc:format>");
		builder.append("<dc:format>dc</dc:format>");
		builder.append("<dc:identifier> http://somehost </dc:identifier>");
		builder.append("<dc:identifier>urn:nbn:123-456</dc:identifier>");
		builder.append("<dc:source>4-9c7cf682-849b-48bd-92cf-e65367f38e14_TURTdG9yZURTUmVzb3VyY2VzL01EU3RvcmVEU1Jlc291cmNlVHlwZQ==</dc:source>");
		builder.append("<dc:type>someType</dc:type>");
		builder.append("<dc:type>info:eu-repo/semantics/article</dc:type>");
		builder.append("</oai_dc:dc>");
		return builder.toString();		
	}
	
	private String getDC2(){
		StringBuilder builder = new StringBuilder();
		builder.append("<oai_dc:dc xmlns:oai_dc=\"http://www.openarchives.org/OAI/2.0/oai_dc/\" xmlns=\"http://www.openarchives.org/OAI/2.0/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xsi:schemaLocation=\"http://www.openarchives.org/OAI/2.0/oai_dc/  http://www.openarchives.org/OAI/2.0/oai_dc.xsd\">");
		builder.append("<dc:title>SomeTitle</dc:title>");
		//builder.append("<title>SomeTitle</title>");
		builder.append("<dc:language>firstLang</dc:language>");
		builder.append("<dc:language>middleLang</dc:language>");
		builder.append("<dc:language>lastLang</dc:language>");
		builder.append("<dc:creator>Any Author</dc:creator>");
		builder.append("<dc:contributor>First Contributor</dc:contributor>");
		builder.append("<dc:contributor>Second Contributor</dc:contributor>");
		builder.append("<dc:identifier> http://somehost </dc:identifier>");
		builder.append("<dc:identifier>urn:nbn:123-456</dc:identifier>");
		builder.append("<dc:source>4-9c7cf682-849b-48bd-92cf-e65367f38e14_TURTdG9yZURTUmVzb3VyY2VzL01EU3RvcmVEU1Jlc291cmNlVHlwZQ==</dc:source>");
		builder.append("<dc:type>someType</dc:type>");
		builder.append("<dc:type>info:eu-repo/semantics/article</dc:type>");
		builder.append("</oai_dc:dc>");
		return builder.toString();		
	}
	
	private String getDidl(){
		StringBuilder builder = new StringBuilder();
		builder.append("<didl:DIDL xmlns:didl=\"urn:mpeg:mpeg21:2002:02-DIDL-NS\" xmlns=\"http://www.openarchives.org/OAI/2.0/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:diext=\"http://library.lanl.gov/2004-04/STB-RL/DIEXT\" xmlns:dcterms=\"http://purl.org/dc/terms/\" xmlns:dii=\"urn:mpeg:mpeg21:2002:01-DII-NS\" xmlns:dip=\"urn:mpeg:mpeg21:2002:01-DIP-NS\" DIDLDocumentId=\"DIDL:URN:NBN:NL:UI:10-1874-1678\" xsi:schemaLocation=\" urn:mpeg:mpeg21:2002:02-DIDL-NS    http://standards.iso.org/ittf/PubliclyAvailableStandards/MPEG-21_schema_files/did/didl.xsd urn:mpeg:mpeg21:2002:01-DII-NS    http://standards.iso.org/ittf/PubliclyAvailableStandards/MPEG-21_schema_files/dii/dii.xsd urn:mpeg:mpeg21:2005:01-DIP-NS    http://standards.iso.org/ittf/PubliclyAvailableStandards/MPEG-21_schema_files/dip/dip.xsd http://library.lanl.gov/2004-04/STB-RL/DIEXT   http://purl.lanl.gov/STB-RL/schemas/2004-04/DIEXT.xsd\">");
		builder.append("<didl:Item>");
        builder.append("<didl:Descriptor>"); 
        builder.append("<didl:Statement mimeType=\"application/xml\">");
        builder.append("<dip:ObjectType>info:eu-repo/semantics/humanStartPage</dip:ObjectType>");
        builder.append("</didl:Statement></didl:Descriptor><didl:Component>"); 
        builder.append("<didl:Resource ref=\"http://igitur-archive.library.uu.nl/bio/2001-0803-123812/UUindex.html\" mimeType=\"text/html\"/>");
        builder.append("</didl:Component></didl:Item>");
		builder.append("</didl:DIDL>");
		
		return builder.toString();				
	}
	
	private String getWOS(){
		StringBuilder builder = new StringBuilder();
		builder.append("<oai:record xmlns:oai=\"http://www.openarchives.org/OAI/2.0/\"" +
				" xmlns:dnet=\"eu.dnetlib.data.transform.xml.DataciteToHbaseXsltFunctions\"" +
				" xmlns:oaf=\"http://namespace.openaire.eu/oaf\" xmlns:dri=\"http://www.driver-repository.eu/namespace/dri\">" +
				"    <header>" +
				"        <dri:objIdentifier>::00a3e38eff10c4f2f35ffde55ee22e63</dri:objIdentifier>" +
				"        <dri:recordIdentifier>WOS:000298601300043</dri:recordIdentifier>" +
				"        <dri:dateOfCollection>2013-10-29T10:25:51+01:00</dri:dateOfCollection>" +
				"        <dri:repositoryId/>" +
				"        <oaf:datasourceprefix/>" +
				"    </header>" +
				"    <metadata>" +
				"        <csvRecord>" +
				"            <row name=\"OA\">true</row>" +
				"            <row name=\"PT\">J</row>" +
				"            <row name=\"AU\">Punta, M; Coggill, PC; Eberhardt, RY; Mistry, J; Tate, J; Boursnell, C; Pang, N;Forslund, K; Ceric, G; Clements, J; Heger, A; Holm, L; Sonnhammer, ELL; Eddy, SR; Bateman, A; Finn, RD</row>" +
				"            <row name=\"AF\">Punta, Marco; Coggill, Penny C.; Eberhardt, Ruth Y.; Mistry, Jaina; Tate, John;                Boursnell, Chris; Pang, Ningze; Forslund, Kristoffer; Ceric, Goran; Clements, Jody; Heger,                Andreas; Holm, Liisa; Sonnhammer, Erik L. L.; Eddy, Sean R.; Bateman, Alex; Finn, Robert D.</row>" +
				"    <row name=\"TI\">The Pfam protein families database</row>" +
				"            <row name=\"SO\">NUCLEIC ACIDS RESEARCH</row>" +
				"            <row name=\"LA\">English</row>" +
				"            <row name=\"DT\">Article</row>" +
				"            <row name=\"ID\">CRYSTAL-STRUCTURE; DOMAIN; IDENTIFICATION; ANNOTATION; HOMOLOGY; CAPSULE;                REVEALS; SEARCH</row>" +
				"            <row name=\"AB\">Pfam is a widely used database of protein families, currently containing more                than 13 000 manually curated protein families as of release 26.0. Pfam is available via                servers in the UK (http://pfam.sanger.ac.uk/), the USA (http://pfam.janelia.org/) and Sweden" +
				"                (http://pfam.sbc.su.se/). Here, we report on changes that have occurred since our 2010 NAR   paper (release 24.0). Over the last 2 years, we have generated 1840 new families and" +
				"                increased coverage of the UniProt Knowledgebase (UniProtKB) to nearly 80%. Notably, we have" +
				"                taken the step of opening up the annotation of our families to the Wikipedia community, by" +
				"                linking Pfam families to relevant Wikipedia pages and encouraging the Pfam and Wikipedia" +
				"                communities to improve and expand those pages. We continue to improve the Pfam website and" +
				"                add new visualizations, such as the 'sunburst' representation of taxonomic distribution of" +
				"                families. In this work we additionally address two topics that will be of particular" +
				"                interest to the Pfam community. First, we explain the definition and use of family-specific," +
				"                manually curated gathering thresholds. Second, we discuss some of the features of domains of" +
				"                unknown function (also known as DUFs), which constitute a rapidly growing class of families" +
				"                within Pfam.</row>" +
				"     <row name=\"C1\">[Punta, Marco; Coggill, Penny C.; Eberhardt, Ruth Y.; Mistry, Jaina; Tate, John;" +
				"                Boursnell, Chris; Pang, Ningze; Bateman, Alex] Wellcome Trust Sanger Inst, Hinxton CB10 1SA," +
				"                England; [Forslund, Kristoffer; Sonnhammer, Erik L. L.] Stockholm Univ, Dept Biochem &amp;" +
				"                Biophys, Sci Life Lab, Swedish eSci Res Ctr,Stockholm Bioinformat Ctr, SE-17121 Solna," +
				"                Sweden; [Ceric, Goran; Clements, Jody; Eddy, Sean R.; Finn, Robert D.] HHMI Janelia Farm Res" +
				"                Campus, Ashburn, VA 20147 USA; [Heger, Andreas] Univ Oxford, MRC Funct Genom Unit, Dept" +
				"                Physiol Anat &amp; Genet, Oxford OX1 3QX, England; [Holm, Liisa] Univ Helsinki, Inst" +
				"                Biotechnol, Helsinki 00014, Finland; [Holm, Liisa] Univ Helsinki, Dept Biol &amp; Environm" +
				"                Sci, FIN-00014 Helsinki, Finland</row>" +
				"     <row name=\"RP\">Punta, M (reprint author), Wellcome Trust Sanger Inst, Wellcome Trust Genome" +
				"                Campus, Hinxton CB10 1SA, England.</row>" +
				"     <row name=\"EM\">mp13@sanger.ac.uk</row>" +
				"     <row name=\"FU\">Wellcome Trust [WT077044/Z/05/Z]; BBSRC [BB/F010435/1]; Howard Hughes Medical" +
				"               Institute; Stockholm University; Royal Institute of Technology; Swedish Natural Sciences" +
				"                Research Council</row>" +
				"     <row name=\"FX\">Wellcome Trust (grant numbers WT077044/Z/05/Z); BBSRC Bioinformatics and" +
				"                Biological Resources Fund (grant numbers BB/F010435/1); Howard Hughes Medical Institute (to" +
				"                G. C., J.C., S. R. E and R. D. F.); Stockholm University, Royal Institute of Technology and" +
				"                the Swedish Natural Sciences Research Council (to K. F. and E. L. L. S.) and Systems, Web" +
				"                and Database administration teams at Wellcome Trust Sanger Institute (WTSI) (infrastructure" +
				"                support). Funding for open access charge: Wellcome Trust (grant numbers WT077044/Z/05/Z);" +
				"                BBSRC Bioinformatics and Biological Resources Fund (grant numbers BB/F010435/1).</row>" +
				"      <row name=\"NR\">29</row>" +
				"		<row name=\"TC\">92</row>" +
				"             <row name=\"Z9\">94</row>" +
				"            <row name=\"PU\">OXFORD UNIV PRESS</row>" +
				"            <row name=\"PI\">OXFORD</row>            " +
				"            <row name=\"PA\">GREAT CLARENDON ST, OXFORD OX2 6DP, ENGLAND</row>" +
				"          <row name=\"SN\">0305-1048</row>" +
				"                 <row name=\"J9\">NUCLEIC ACIDS RES</row>" +
				"                        <row name=\"JI\">Nucleic Acids Res.</row>" +
				"                        <row name=\"PD\">JAN</row>" +
				"                        <row name=\"PY\">2012</row>" +
				"                        <row name=\"VL\">40</row>" +
				"                        <row name=\"IS\">D1</row>" +
				"                        <row name=\"BP\">D290</row>" +
				"                       <row name=\"EP\">D301</row>" +
				"            <row name=\"DI\">10.1093/nar/gkr1065</row>            " +
				"<row name=\"PG\">12</row>" +
				"                        <row name=\"WC\">Biochemistry &amp; Molecular Biology</row>" +
				"                        <row name=\"SC\">Biochemistry &amp; Molecular Biology</row>" +
				"                        <row name=\"GA\">869MD</row>" +
				"                        <row isID=\"true\" name=\"UT\">WOS:000298601300043</row>" +
				"                    </csvRecord>" +
				"    </metadata>" +
				"</oai:record>");
		return builder.toString();
	}
	
	private String getOAFDC(){
		StringBuilder builder = new StringBuilder();
		builder.append(
				"<oai_dc:dc xmlns:oai_dc=\"http://www.openarchives.org/OAI/2.0/oai_dc/\" xmlns=\"http://www.openarchives.org/OAI/2.0/\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd\">" +
				"<dc:title>Grass roots lobbying: marketing politics and policy 'beyond the Beltway'</dc:title>" +
				"<dc:creator>McGrath, Conor; Xyz, Opq</dc:creator>" +
				"<dc:creator>Abc, Def</dc:creator>" +
				"<dc:creator>Muñoz-Castellanos, L</dc:creator>" +
				"<dc:subject>JA Political science (General)</dc:subject>" +
				"<dc:description/>" +
				"<dc:publisher/>" +
				"<dc:date>2011</dc:date>" +
				"<dc:date>info:eu-repo/date/embargoEnd/2011-05-12</dc:date>" +
				"<dc:date>2004-03-15</dc:date>" +
				"<dc:date>2009-02-24T13:27:42Z</dc:date>" +
				"<dc:date>2009-02</dc:date>" +
				"<dc:date>2009</dc:date>" +
				"<dc:type>Conference or Workshop Item</dc:type>" +
				"<dc:type>NonPeerReviewed</dc:type>" +
				"<dc:type>info:eu-repo/semantics/article</dc:type>" +
				"<dc:identifier>http://somehost</dc:identifier>" +
				"<dc:format>application/pdf</dc:format>" +
				"<dc:relation>info:eu-repo/grantAgreement/EC/FP7/241479</dc:relation>" +
				"<dc:relation>http://sherpa.bl.uk/1/01/PMMcgrath.pdf</dc:relation>" +
				"<dc:relation>info:eu-repo/grantAgreement/EC/FP7/246682/EU/Towards a 10-Year Vision for Global Research Data Infrastructures/GRDI2020</dc:relation>" +
				"<dc:relation>info:eu-repo/grantAgreement/EC/FP7/PITN-GA-2009-237252</dc:relation>" +
			    "<dc:relation>info:eu-repo/grantAgreement/EC/FP7/PITN-GA-2009-235114</dc:relation>" +
			    "<dc:relation>info:eu-repo/grantAgreement/EC/FP7/237252</dc:relation>" +
			    "<dc:identifier>http://dx.doi.org/10.1103/PhysRevLett.104.126402</dc:identifier>" +
			    "<dc:rights>Tots els drets reservats</dc:rights>" +
			    "<dc:rights>Used by permission of the publisher</dc:rights>" +
				"<dc:rights>info:eu-repo/semantics/openAccess  </dc:rights>" +
				"</oai_dc:dc>");
		return builder.toString();
	}
	
	private String getProvenance(){
		StringBuilder builder = new StringBuilder();
		builder.append(
				"<provenance xmlns=\"http://www.openarchives.org/OAI/2.0/provenance\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.openarchives.org/OAI/2.0/provenance http://www.openarchives.org/OAI/2.0/provenance.xsd\">" +
				"<originDescription altered=\"true\" harvestDate=\"2012-01-20T00:04:10Z\">" +
				"<baseURL>http://dspace.library.uu.nl:8080/dspace-oai/request</baseURL>" +
				"<identifier>oai:dspace.library.uu.nl:1874/218065</identifier>" +
				"<datestamp>2012-01-19T12:38:56Z</datestamp>" +
				"<metadataNamespace>http://www.loc.gov/mods/v3</metadataNamespace>" +
				"</originDescription>" +
				"</provenance>");		
		return builder.toString();
		
	}

}
