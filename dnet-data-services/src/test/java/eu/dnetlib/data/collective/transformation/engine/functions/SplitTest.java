/**
 * 
 */
package eu.dnetlib.data.collective.transformation.engine.functions;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * @author js
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SplitTest {
	private transient Split splitFunc;
	
	@Before
	public void setUp(){
		splitFunc = new Split();
	}
	
	@Test
	public void testSplit() throws ProcessingException{
		String[] authors = {"Simon, Bolivar ; Müller, Heiner"};
		assertEquals("Simon, Bolivar", splitFunc.executeSingleValue(Arrays.asList(authors), ";", "idxyz"));
		assertEquals("Müller, Heiner", splitFunc.executeSingleValue("idxyz"));
		assertEquals(null, splitFunc.executeSingleValue("idxyz"));		
	}
}
