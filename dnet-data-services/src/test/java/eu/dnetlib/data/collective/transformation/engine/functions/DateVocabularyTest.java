package eu.dnetlib.data.collective.transformation.engine.functions;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class DateVocabularyTest {

	private transient DateVocabulary dateVocab;
	
	@Before
	public void setUp(){
		dateVocab = new DateVocabulary();
	}
	
	@Test
	public void test()throws ProcessingException{
		// '2004' has higher priority than '01.07.1998' in this implementation
		String[] date = {"2004", "01.07.1998"};
		assertEquals("2004-01-01", dateVocab.encoding(Arrays.asList(date)));
		String[] date2 = {"02-03-2009"};
		assertEquals("2009-03-02", dateVocab.encoding(Arrays.asList(date2)));
		String[] date3 = {"02/03/2009"};
		assertEquals("2009-03-02", dateVocab.encoding(Arrays.asList(date3)));
		String[] date4 = {"2012-03-28T08:44:17Z"};
		assertEquals("2012-03-28", dateVocab.encoding(Arrays.asList(date4)));
		String[] date5 = {"17750/1799"}; // invalid date
		assertEquals("", dateVocab.encoding(Arrays.asList(date5)));
		String[] date6 = {"2002-4-18"};
		assertEquals("2002-04-18", dateVocab.encoding(Arrays.asList(date6)));
		String[] date7 = {"2003-12   "};
		assertEquals("2003-12-01", dateVocab.encoding(Arrays.asList(date7)));
		
	}
	
	@Test
	public void testDefaultPattern() throws ProcessingException{
		String[] date = {"2011", "info:eu-repo/date/embargoEnd/2011-05-12", "2004-03-15", "2009-02-24T13:27:42Z", "2009-02", "2009"};
		// the embargoEnd date is ignored, then the resulting list size must be 5
		assertEquals(5, dateVocab.encoding(Arrays.asList(date), "yyyy-MM-dd", "").size());
	}
	
	@Test
	public void testDefaultPatternWithFilter() throws ProcessingException{
		String[] date = {"2011", "info:eu-repo/date/embargoEnd/2011-05-12", "2004-03-15", "2009-02-24T13:27:42Z", "2009-02", "2009"};
		// the embargoEnd date is ignored, then the oldest date must be '2004-03-15'
		List<String> results = dateVocab.encoding(Arrays.asList(date), "yyyy-MM-dd", "min()");
		assertEquals(1, results.size());
		assertEquals("2004-03-15", results.get(0));
	}
	
	@Test
	public void testDefaultPatternWithFilterAndInvalidDate() throws ProcessingException{
		String[] date = {"2011", "2004-03-15", "2009-02-24T13:27:42Z", "2009-02", "2009", "17750/1799"};
		// the embargoEnd date is ignored, then the oldest date must be '2004-03-15'
		List<String> results = dateVocab.encoding(Arrays.asList(date), "yyyy-MM-dd", "min()");
		assertEquals(1, results.size());
		assertEquals("2004-03-15", results.get(0));
	}

	@Test
	public void testDefaultPatternWithFilterAndNoValidDate() throws ProcessingException{
		String[] date = {"uuuu"};
		// the embargoEnd date is ignored, then the oldest date must be '2004-03-15'
		List<String> results = dateVocab.encoding(Arrays.asList(date), "yyyy-MM-dd", "min()");
		assertEquals(0, results.size());
	}

}
