/**
 * 
 */
package eu.dnetlib.data.collective.transformation.engine;

import java.io.StringReader;

import javax.xml.transform.TransformerConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import eu.dnetlib.data.collective.transformation.engine.core.TransformationImpl;
import eu.dnetlib.data.collective.transformation.engine.functions.ProcessingException;
import eu.dnetlib.data.collective.transformation.rulelanguage.RuleLanguageParser;

/**
 * @author jochen
 *
 */
public class StylesheetTest {

	private static final String xslTemplatePath_oaf = "eu/dnetlib/data/collective/transformation/engine/oaftemplate.xsl";         // OpenAIRE specific
	private transient Resource xslTemplateResource = new ClassPathResource(xslTemplatePath_oaf);                              // OpenAIRE specific
	private static final String schemaPath_oaf = "eu/dnetlib/data/collective/transformation/schema/OAFSchema_vTransformator.xsd"; // OpenAIRE specific
	private transient Resource schemaResource = new ClassPathResource(schemaPath_oaf);                                        // OpenAIRE specific

	private transient TransformationImpl transformation;

	@SuppressWarnings("unchecked")
	@Before
	public void setUp() throws TransformerConfigurationException, ProcessingException{
		System.setProperty("javax.xml.transform.TransformerFactory", "net.sf.saxon.TransformerFactoryImpl");
		
		transformation = new TransformationImpl();
		transformation.setTemplate(xslTemplateResource);
		transformation.setSchema(schemaResource);
		transformation.init();
		transformation.setRuleLanguageParser(getRuleLanguageParser(getTransformationScript()));
		//transformation.setRootElement("record");
		transformation.configureTransformation();

	}
	
	@Test
	public void testStylesheetSkip(){
		System.out.println(transformation.dumpStylesheet());
	}
	
	private String getTransformationScript(){
		StringBuilder scriptBuilder = new StringBuilder();
		scriptBuilder.append("declare_script \"MainSample\";\r\n");
		scriptBuilder.append("declare_ns dr = \"http://www.driver-repository.eu/namespace/dr\";\r\n");
		scriptBuilder.append("declare_ns dri = \"http://www.driver-repository.eu/namespace/dri\";\r\n");
		scriptBuilder.append("declare_ns dc = \"http://purl.org/dc/elements/1.1/\";\r\n");
		scriptBuilder.append("declare_ns oaf = \"http://namespace.openaire.eu/oaf\";\r\n");
		
		//scriptBuilder.append("$a1 = Convert(xpath:\"//dc:language[2]\",LangVocab);\r\n");
		//scriptBuilder.append("dri:mdFormat = $a1;\r\n");
		scriptBuilder.append("dr:CobjMDFormats = xpath:\"//dc:title\";\r\n");
		scriptBuilder.append("dr:repositoryName = getValue(PROFILEFIELD, [xpath:\"//dri:repositoryId\", xpath:\"//CONFIGURATION/OFFICIAL_NAME\"]);\r\n");
		scriptBuilder.append("dc:title = copy(\"dc:title\", \"//dc:title\", \"@*|node()\");\r\n");
		//scriptBuilder.append("if xpath:\"//dc:title\" dr:aggregatorName = \"abc\"; else dr:repositoryName = \"def\";\r\n");
		scriptBuilder.append("if xpath:\"//dc:language[4]\" dr:CobjTypology = xpath:\"//dc:language[2]\"; else dr:CobjTypology = \"test\";\r\n");
		scriptBuilder.append("apply xpath:\"//dc:identifier\" if xpath:\"starts-with(normalize-space(.), 'http')\" dc:identifier = xpath:\"normalize-space(.)\"; else dr:CobjIdentifier = xpath:\"normalize-space(.)\";\r\n");
		//scriptBuilder.append("dc:language = Convert(xpath:\"//dc:language[2]\",LangVocab);\r\n");
		scriptBuilder.append("dc:language = Convert(xpath:\"normalize-space(//dc:language[2])\",LangVocab);\r\n");
		scriptBuilder.append("$var0 = \"''\";\r\n");
		scriptBuilder.append("if xpath:\"//dc:format[text()='digital']\" dc:publisher = xpath:\"//dc:publisher\"; else dc:publisher = skipRecord();\r\n");
		scriptBuilder.append("end\r\n");
		
		return scriptBuilder.toString();
	}
	private RuleLanguageParser getRuleLanguageParser(String aTransformationScript){
		RuleLanguageParser parser = new RuleLanguageParser();
		System.out.println(aTransformationScript);
		StringReader reader = new StringReader(aTransformationScript);
		parser.parse(reader);
		return parser;
	}

}
