package eu.dnetlib.data.collective.transformation.engine.core;

import java.io.StringReader;

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import eu.dnetlib.data.collective.transformation.rulelanguage.RuleLanguageParser;

public class TransformationImplTest {

	private static final String xslTemplatePath = "eu/dnetlib/data/collective/transformation/engine/template.xsl";
	private transient Resource xslTemplateResource = new ClassPathResource(xslTemplatePath);
//	private static final String schemaPath = "eu/dnetlib/data/collective/transformation/schema/DMFSchema_vTransformator.xsd";
	private static final String schemaPath = "eu/dnetlib/data/collective/transformation/schema/OAFSchema_vTransformator.xsd";
	private transient Resource schemaResource = new ClassPathResource(schemaPath);
	private transient RuleLanguageParser parser;
	private static final String dataSinkId = "132";
	
	private transient TransformationImpl transformation;
	
	@Before
	public void setUp(){
		System.setProperty("javax.xml.transform.TransformerFactory", "net.sf.saxon.TransformerFactoryImpl");

		parser = new RuleLanguageParser();

		transformation = new TransformationImpl();
		transformation.addJobConstant(TransformationImpl.JOBCONST_DATASINKID, dataSinkId);
		transformation.setTemplate(xslTemplateResource);
		transformation.setSchema(schemaResource);
		transformation.init();
		transformation.setRuleLanguageParser(parser);
		
	}
	
	@Test
	public void testStyleSheet() throws TransformerConfigurationException{
		StringReader reader = new StringReader(dc2DmfScript());
		parser.parse(reader);
		try{
			transformation.configureTransformation();						
		}catch(TransformerConfigurationException e){
			e.printStackTrace();
		}
		System.out.println(transformation.dumpStylesheet());
	}
	
	private String dc2DmfScript(){
	   StringBuilder scriptBuilder = new StringBuilder();
	   scriptBuilder.append("declare_script \"MainSample\";\r\n");
	   
	   scriptBuilder.append("declare_ns oaf = \"http://namespace.openaire.eu/oaf\";\r\n");
	   scriptBuilder.append("declare_ns dr = \"http://www.driver-repository.eu/namespace/dr\";\r\n");
	   scriptBuilder.append("declare_ns dri = \"http://www.driver-repository.eu/namespace/dri\";\r\n");
	   scriptBuilder.append("declare_ns dc = \"http://purl.org/dc/elements/1.1/\";\r\n");
	
	   scriptBuilder.append("$var0 = \"''\";\r\n");
	   scriptBuilder.append("static $var1 = RegExpr($job.datasinkid, $var0, \"s/^(dnet:\\/\\/MDStoreDS\\/)|(\\?.*)//g\");\r\n");
       scriptBuilder.append("dr:objectIdentifier = xpath:\"//dri:objIdentifier\";\r\n");
	   scriptBuilder.append("dr:dateOfCollection = getValue(CURRENTDATE, []);\r\n");
	   scriptBuilder.append("dr:CobjContentSynthesis = empty;\r\n");
	   scriptBuilder.append("dr:CobjTypology = \"Textual\";\r\n");
	   scriptBuilder.append("dr:CobjModel = \"OAI\";\r\n");
	   scriptBuilder.append("dr:CobjMdFormats = \"oai_dc\";\r\n");
	   scriptBuilder.append("dr:CobjDescriptionSynthesis = empty;\r\n");
	   scriptBuilder.append("//dr:aggregatorName = getValue(PROFILEFIELD, [\"transformationmanager-service-profile-id\", xpath:\"//PROPERTY/@key='name'\"]);\r\n");
	   scriptBuilder.append("dr:aggregatorInstitution = empty;\r\n");
	   scriptBuilder.append("dr:repositoryName = getValue(PROFILEFIELD, [xpath:\"//dri:repositoryId\", xpath:\"//CONFIGURATION/OFFICIAL_NAME\"]);\r\n");
	   scriptBuilder.append("dr:repositoryLink = getValue(PROFILEFIELD, [xpath:\"//dri:repositoryId\", xpath:\"//REPOSITORY_WEBPAGE\"]);\r\n");
	   scriptBuilder.append("dr:repositoryCountry = getValue(PROFILEFIELD, [xpath:\"//dri:repositoryId\", xpath:\"//COUNTRY\"]);\r\n");
	   scriptBuilder.append("dr:repositoryInstitution = getValue(PROFILEFIELD, [xpath:\"//dri:repositoryId\", xpath:\"//REPOSITORY_INSTITUTION\"]);\r\n");
	   scriptBuilder.append("dc:creator = xpath:\"//dc:creator\";\r\n");
	   scriptBuilder.append("dc:title = xpath:\"//dc:title\";\r\n");
	   scriptBuilder.append("dc:subject = xpath:\"//dc:subject\";\r\n");
	   scriptBuilder.append("dr:CobjCategory = Convert(xpath:\"//dc:type\", TextTypologies);\r\n");
	   scriptBuilder.append("%templ = split(xpath:\"/dc:creator\", \"dc:creator\", \";\");\r\n");
	   //scriptBuilder.append("dc:language = Convert(xpath:\"//dc:language\", Languages);\r\n");
//	   scriptBuilder.append("dc:language = Extract(language);\r\n");
//	   scriptBuilder.append("dc:dateAccepted = Convert(xpath:\"//dc:date\", DateISO8601);\r\n");
//	   scriptBuilder.append("apply xpath:\"//dc:identifier\" if xpath:\"starts-with(., 'http')\" dc:identifier = xpath:\".\"; else dr:CobjIdentifier = xpath:\".\";\r\n");
       scriptBuilder.append("apply xpath:\"//dc:relation\" if xpath:\"starts-with(., 'http')\" dc:identifier = RegExpr(xpath:\"//dc:relation\", xpath:\"//dc:relation\", \"s/^(.*info:eu-repo\\/grantAgreement\\/EC\\/FP7\\/)//g\"); else dr:CobjIdentifier = xpath:\".\";\r\n");
//       apply xpath:"//dc:rights" if xpath:"starts-with(normalize-space(.), 'info:eu-repo/semantics')" oaf:accessrights = Convert(xpath:"normalize-space(.)", AccessRights); else dc:rights = xpath:"normalize-space(.)";
       //	   scriptBuilder.append("dc:publisher = xpath:\"//dc:publisher\";\r\n");
//	   scriptBuilder.append("dc:source = xpath:\"//dc:source\";\r\n");
//	   scriptBuilder.append("dc:contributor = xpath:\"//dc:contributor\";\r\n");
//	   scriptBuilder.append("dc:relation = xpath:\"//dc:relation\";\r\n");
//	   scriptBuilder.append("dc:description = xpath:\"//dc:description\";\r\n");
	   scriptBuilder.append("end\r\n"); 		
	   return scriptBuilder.toString();
	}

}
