package eu.dnetlib.data.collective.transformation.rulelanguage;

import static org.junit.Assert.assertEquals;

import java.io.StringReader;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import eu.dnetlib.data.collective.transformation.core.xsl.ext.TransformationFunctionProxy;
import eu.dnetlib.data.collective.transformation.engine.SimpleTransformationEngine;
import eu.dnetlib.data.collective.transformation.rulelanguage.util.Converter;
import eu.dnetlib.data.collective.transformation.rulelanguage.util.FunctionCall;


public class TransformationTest {

	
	private static final String declareScript 	= "declare_script abc;\r\n";
	private static final String ruleTypology 	= "CobjTypology = Text;\r\n";
	private static final String ruleFormat 		= "CobjMDformats = oai_dc;\r\n";
	private static final String ruleFuncGetvalue = "repositoryName = getValue(profileField,[abc, def]);\r\n";
	private static final String ruleFuncConvert_withoutPrefixes = "lv1 = Convert(xpath:/metadata/language,LangVocab);\r\n";
	private static final String ruleFuncConvert_withPrefixes = "lv2 = Convert(xpath:/dc:metadata/dc:language,LangVocab);\r\n";
	private static final String ruleFuncConvert_withAttribute = "lv3 = Convert(xpath:/dc:metadata/language[@attr],LangVocab);\r\n";
	private static final String endScript = "end";
	private StringBuilder scriptBuilder;
	private StringBuilder recordBuilder;
	
	
	private String buildSourceRecord(String lang){
		recordBuilder = new StringBuilder();
		recordBuilder.append("<objectRecord>\r\n");
		recordBuilder.append("<record xmlns:dri=\"http://www.driver-repository.eu/namespace/dri\">\r\n");
		recordBuilder.append("<header/>\r\n");
		recordBuilder.append("<metadata>\r\n");
		recordBuilder.append("<language>" + lang + "</language> \r\n");		
		recordBuilder.append("</metadata>\r\n");
		recordBuilder.append("</record>\r\n");
		recordBuilder.append("</objectRecord>\r\n");
		return recordBuilder.toString();
	}
	
	private List<String> getSourceRecords(){
		List<String> records = new LinkedList<String>();
		records.add(buildSourceRecord("ger"));
		records.add(buildSourceRecord("eng"));
		return records;
	}
	
//	private String getStylesheet(String stmt) {
//		RuleLanguageParser rlp = new RuleLanguageParser();
//		StringReader r = new StringReader(stmt);
//		rlp.parse(r);
//		return rlp.getStyleSheet();
//	}
//	
//	//@Test
//	public void parseMinimalScript(){
//		scriptBuilder = new StringBuilder();
//		scriptBuilder.append(declareScript);
//		scriptBuilder.append(ruleFuncConvert_withoutPrefixes);
//		scriptBuilder.append(endScript);
//		System.out.println(getStylesheet(scriptBuilder.toString()));
//	}
/*
	@Test
	public void testTransformationEngine(){
		TreeMap<String, Set<Rules>> ruleMapping = new TreeMap<String, Set<Rules>>();
		Set<Rules> ruleSet = new HashSet<Rules>();
		
		Map<String, String> paramMap = new LinkedHashMap<String, String>();
		paramMap.put("expr1", Converter.getXpathFromXpathExpr("/metadata/language"));
		paramMap.put("vocabulary", "LangVocab");
		FunctionCall fc = new FunctionCall();
		fc.setExternalFunctionName("convert");
		fc.setParameters(paramMap);
		Rules r = new Rules();
		r.setFunctionCall(fc);
		ruleSet.add(r);
		ruleMapping.put("language", ruleSet);
		r = new Rules();
		r.setConstant("Text");
		ruleSet = new HashSet<Rules>();
		ruleSet.add(r);
		ruleMapping.put("CobjTypology", ruleSet);
		
		TransformationPrototype t = new TransformationPrototype();
		t.setTemplate(new ClassPathResource("/eu/dnetlib/data/collective/transformation/engine/template.xsl"));
		t.init();
		t.setMapping(ruleMapping);
		t.configureTransformation();
		
		List<String> records = getSourceRecords();
		SimpleTransformationEngine engine = new SimpleTransformationEngine();
		engine.setObjectRecords(records);
		engine.setTransformation(t);
		engine.transform();
		List<String> mdRecords = engine.getMdRecords();		
		assertEquals(2, mdRecords.size());
		for (String record: mdRecords){
			System.out.println(record);
		}
		
	}
*/	
	@Test
	public void testTransformationFunctions(){
		
	}
}
