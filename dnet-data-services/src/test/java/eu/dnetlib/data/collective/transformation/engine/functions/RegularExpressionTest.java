package eu.dnetlib.data.collective.transformation.engine.functions;

import static org.junit.Assert.*;

import org.apache.oro.text.perl.Perl5Util;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RegularExpressionTest {

	private transient RegularExpression regExprFunc;
	
	@Before
	public void setUp(){
		regExprFunc = new RegularExpression();
	}
	
	@Test
	public void testSubstitute() throws ProcessingException{
		
		String projId = "241479";
		String regExpr = "s/^(.*info:eu-repo\\/grantAgreement\\/EC\\/FP7\\/)//gm";
		String input = "info:eu-repo/grantAgreement/EC/FP7/241479";
		assertEquals(projId, regExprFunc.executeSingleValue(regExpr, input, "anyValue"));
		
	}
	
//	@Test
//	public void testExtractGrantNumber() throws ProcessingException{
//		String projectStringCorrect = "info:eu-repo/grantAgreement/EC/FP7/258169/EU/Game and Learning Alliance/GALA";
//		String projectStringIssue = "info:eu-repo/grantAgreement/EC/FP7//EU/A Digital Library Infrastructure on Grid Enabled Technology./DILIGENT";
//		
//		String regExpr = "s/^.*info:eu-repo\\/grantAgreement\\/EC\\/FP7\\/(\\d\\d\\d\\d\\d\\d).*/$1/gm";
////		String regExpr = "s/^(.*info:eu-repo\\/grantAgreement\\/EC\\/FP7\\/)[0-9]6(.*)//gm";
////		String regExpr = "s/^(.*info:eu-repo\\/grantAgreement\\/EC\\/FP7\\/)[0-9]6(.*)//gm";
//		String grantId = "258169";
//		assertEquals(grantId, regExprFunc.executeSingleValue(regExpr, projectStringCorrect, "anyValue"));
//		assertEquals("", regExprFunc.executeSingleValue(regExpr, projectStringIssue, "anyValue"));
//	}
	
	@Test
	public void testExtractPMC() throws ProcessingException{
		String input = "PMC: PMC169570";
		String regExpr = "s/^(.*PMC: )//gm";
		String pmcid = "PMC169570";
		assertEquals(pmcid, regExprFunc.executeSingleValue(regExpr, input, "anyValue"));
	}
	
	@Test
	public void testExtractDoi() throws ProcessingException{
		String input = "doi: 10.1093/dnares/dsn019";
		String regExpr = "s/^(.*doi: )//gm";
		String doiid = "10.1093/dnares/dsn019";
		assertEquals(doiid, regExprFunc.executeSingleValue(regExpr, input, "anyValue"));
	}
	
	@Test
	public void testExtractIssn() throws ProcessingException{
		String issn = "02582279";
//		String regExpr = "s/^(.*issn=[~0-9]*)([^&]*)//gm";
//		String regExpr = "s/^(.*issn=([^&]+).*)?//gm";
		String regExpr = "m/issn=([^&]+)/";
		String input = "http://www.doaj.org/doaj?func=openurl&amp;genre=article&amp;issn=02582279&amp;date=1995&amp;volume=16&amp;issue=2&amp;spage=215";
		assertEquals(issn, regExprFunc.executeSingleValue(regExpr, input, "anyValue"));
//		Perl5Util perlRegExpr = new Perl5Util();
//		if (perlRegExpr.match(regExpr, input))
//			System.out.println(perlRegExpr.group(1));
//		else
//			System.out.println("no match.");
	}
}
