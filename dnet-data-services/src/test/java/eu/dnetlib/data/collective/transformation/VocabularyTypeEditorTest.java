package eu.dnetlib.data.collective.transformation;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * @author js
 *
 */
public class VocabularyTypeEditorTest {

	// class under test
	private transient VocabularyTypeEditor editor;
	private final String json = "{\"map\":{\"Languages\":{\"name\":\"Names of Languages\", \"caseSensitive\":\"false\", \"delimiter\":\"/\"}, \"TextTypologies\":{\"name\":\"Names of Text Object Typologies\", \"caseSensitive\":\"false\"}}}";
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		editor = new VocabularyTypeEditor();
	}
	
	@Test
	public void testGenerate(){
		editor.setAsText(json);
		assertNotNull(editor.getValue());
		assertTrue(editor.getValue() instanceof VocabularyMap);
		assertTrue(((VocabularyMap)editor.getValue()).containsKey("Languages"));
		assertFalse(((VocabularyMap)editor.getValue()).getMap().get("Languages").isCaseSensitive());
	}

}
