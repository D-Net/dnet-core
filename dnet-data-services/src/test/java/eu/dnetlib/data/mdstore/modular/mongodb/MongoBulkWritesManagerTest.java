package eu.dnetlib.data.mdstore.modular.mongodb;

import java.util.Map;

import com.google.common.collect.Maps;
import com.mongodb.DBObject;
import com.mongodb.WriteConcern;
import com.mongodb.client.MongoCollection;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

/**
 * Created by Alessia Bardi on 2019-04-12.
 *
 * @author Alessia Bardi
 */
@RunWith(MockitoJUnitRunner.class)
public class MongoBulkWritesManagerTest {

	private MongoBulkWritesManager mng;

	@Mock
	MongoCollection<DBObject> coll;

	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		when(coll.withWriteConcern(WriteConcern.ACKNOWLEDGED)).thenReturn(coll);
		mng = new MongoBulkWritesManager(coll, coll, null, 10, null, false);
	}

	@Test
	public void buildDBObjectTest(){
		Map<String, String> props = Maps.newHashMap();
		props.put("timestamp", "1555078665140");
		props.put("id", "od______4301::5af4702a60ddf0615fd1dfd6ded104df");
		props.put("originalId", "x");
		props.put("body","<body/>");

		DBObject obj = mng.buildDBObject("<x/>", props, null);
		System.out.println(obj);
	}
}
