package eu.dnetlib.data.mdstore.modular.mongodb;

import java.net.UnknownHostException;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import eu.dnetlib.data.mdstore.modular.RecordParserFactory;
import eu.dnetlib.data.mdstore.modular.StreamingRecordParser;
import eu.dnetlib.data.mdstore.modular.connector.MDStoreDao;
import eu.dnetlib.data.mdstore.modular.connector.MDStoreTransactionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigurationTestConfig {

	@Bean
	public MongoDatabase db() throws UnknownHostException {
		final MongoClient mongo = new MongoClient("localhost", 27017);
		return mongo.getDatabase("mdstore_test");
	}

	@Bean
	public MDStoreTransactionManager manager() throws UnknownHostException {
		final MDStoreTransactionManagerImpl manager = new MDStoreTransactionManagerImpl();
		manager.setDb(db());
		return manager;
	}

	@Bean
	public RecordParserFactory recordParserFactory() {
		final RecordParserFactory rpfactory = new RecordParserFactory();
		rpfactory.setParserType(StreamingRecordParser.class);
		return rpfactory;
	}

	@Bean
	public MDStoreDao mdstoreDao() throws UnknownHostException {
		final MDStoreDaoImpl dao = new MDStoreDaoImpl();
		dao.setDb(db());
		dao.setRecordParserFactory(recordParserFactory());
		return dao;
	}

}
