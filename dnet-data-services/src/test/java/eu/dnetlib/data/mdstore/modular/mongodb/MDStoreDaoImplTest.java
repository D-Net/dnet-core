package eu.dnetlib.data.mdstore.modular.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import eu.dnetlib.data.mdstore.MDStoreServiceException;
import eu.dnetlib.data.mdstore.modular.connector.MDStoreDBStatus;
import eu.dnetlib.data.mdstore.modular.connector.MDStoreDao;
import eu.dnetlib.data.mdstore.modular.connector.MDStoreTransactionManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ConfigurationTestConfig.class)
public class MDStoreDaoImplTest {

	@Autowired
	private MDStoreDao dao;
	@Autowired
	private MongoDatabase db;
	@Autowired
	private MDStoreTransactionManager manager;

	@After
	public void tearDown() throws MDStoreServiceException {
		dao.deleteMDStore("1");
		dao.deleteMDStore("2");
		dao.deleteMDStore("3");
		dao.deleteMDStore("4");
		dao.deleteMDStore("5");
		dao.deleteMDStore("6");
	}

	@Before
	public void setUp() throws Exception {
		dao.createMDStore("1", "F", "I", "L");
		dao.createMDStore("2", "F", "I", "L");
		dao.createMDStore("3", "F", "I", "L");
		dao.createMDStore("4", "F", "I", "L");
		dao.createMDStore("5", "F1", "I", "L");
		dao.createMDStore("6", "F1", "I", "L");

		final MongoCollection<DBObject> metadata = db.getCollection(MDStoreDaoImpl.METADATA_NAME, DBObject.class);

		metadata.findOneAndUpdate(new BasicDBObject(MDStoreDaoImpl.MD_ID, "1"), new BasicDBObject("$set", new BasicDBObject(MDStoreDaoImpl.SIZE, 10)));
		metadata.findOneAndUpdate(new BasicDBObject(MDStoreDaoImpl.MD_ID, "2"), new BasicDBObject("$set", new BasicDBObject(MDStoreDaoImpl.SIZE, 10)));
		metadata.findOneAndUpdate(new BasicDBObject(MDStoreDaoImpl.MD_ID, "3"), new BasicDBObject("$set", new BasicDBObject(MDStoreDaoImpl.SIZE, 10)));
		metadata.findOneAndUpdate(new BasicDBObject(MDStoreDaoImpl.MD_ID, "4"), new BasicDBObject("$set", new BasicDBObject(MDStoreDaoImpl.SIZE, 10)));
		metadata.findOneAndUpdate(new BasicDBObject(MDStoreDaoImpl.MD_ID, "5"), new BasicDBObject("$set", new BasicDBObject(MDStoreDaoImpl.SIZE, 10)));
		metadata.findOneAndUpdate(new BasicDBObject(MDStoreDaoImpl.MD_ID, "6"), new BasicDBObject("$set", new BasicDBObject(MDStoreDaoImpl.SIZE, 10)));

	}

	@Test
	public void test() throws MDStoreServiceException {
		assertEquals(40, dao.getSumOfSizes("F", "L", "I"));
		assertEquals(20, dao.getSumOfSizes("F1", "L", "I"));
		assertEquals(0, dao.getSumOfSizes("F_0", "L", "I"));
	}

	@Test
	public void getDBStatusTest() {
		final MDStoreDBStatus dbStatus = dao.getDBStatus();
		System.out.println(dbStatus);

	}

}
