package eu.dnetlib.data.mdstore.modular.mongodb;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import eu.dnetlib.data.mdstore.MDStoreServiceException;
import eu.dnetlib.data.mdstore.modular.MDFormatDescription;
import eu.dnetlib.data.mdstore.modular.mongodb.utils.IndexFieldRecordParser;
import eu.dnetlib.data.mdstore.modular.mongodb.utils.IndexFieldRecordParserException;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 * Created by sandro on 11/29/16.
 */
public class IndexFieldParserTest {


    @Test
    public void parserTest() throws IOException, MDStoreServiceException, IndexFieldRecordParserException {
        InputStream inputStream = this.getClass().getResourceAsStream("/eu/dnetlib/data/mdstore/modular/mongodb/inputRecord.xml");

        final String inputRecord = IOUtils.toString(inputStream);

        final IndexFieldRecordParser parser = new IndexFieldRecordParser();

        final List<MDFormatDescription> mdref = new ArrayList<>();

        mdref.add(new MDFormatDescription("id", "//*[local-name()='objIdentifier']"));
        mdref.add(new MDFormatDescription("title", "//*[local-name()='title']"));
        mdref.add(new MDFormatDescription("creator", "//*[local-name()='creator']"));

        Map<String, List<String>> stringListMap = parser.parseRecord(inputRecord, mdref);


        for (String key : stringListMap.keySet()) {
            System.out.println("key = " + key);
            for (String value : stringListMap.get(key)) {
                System.out.println("\t" + value);
            }

        }


    }


}
