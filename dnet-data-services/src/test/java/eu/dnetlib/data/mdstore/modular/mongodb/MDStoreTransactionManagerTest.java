package eu.dnetlib.data.mdstore.modular.mongodb;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.Mongo;
import com.mongodb.client.MongoDatabase;
import eu.dnetlib.data.mdstore.MDStoreServiceException;
import eu.dnetlib.data.mdstore.modular.connector.MDStore;
import eu.dnetlib.data.mdstore.modular.connector.MDStoreDao;
import eu.dnetlib.data.mdstore.modular.connector.MDStoreTransactionManager;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

// TODO: reimplement tests

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ConfigurationTestConfig.class)
public class MDStoreTransactionManagerTest {

	@Autowired
	private MongoDatabase db;

	@Autowired
	private MDStoreTransactionManager manager;

	@Autowired
	private MDStoreDao dao;

	@Test
	public void testCreateandRetrieve() throws MDStoreServiceException {
		UUID idCreation = UUID.randomUUID();
		db.getCollection("metadataManager").drop();
		((MDStoreTransactionManagerImpl) manager).setManagerTable(null);

		String mdId = idCreation.toString() + "_TURTdG9yZURTUmVzb3VyY2VzL01EU3RvcmVEU1Jlc291cmNlVHlwZQ==";
		manager.createMDStore(mdId);

		Assert.assertNotNull(manager.startTransaction(mdId, true));
		Assert.assertNotNull(manager.startTransaction(mdId, true));

		String s = manager.getMDStoreCollection(mdId);
		Assert.assertNotNull(s);
		this.manager.dropMDStore(mdId);
		s = manager.getMDStoreCollection(mdId);
		Assert.assertNull(s);
		db.getCollection("metadataManager").drop();
		((MDStoreTransactionManagerImpl) manager).setManagerTable(null);
	}

	@Test
	public void testReadMdStore() throws MDStoreServiceException {
		UUID idCreation = UUID.randomUUID();
		String mdId = idCreation.toString() + "_TURTdG9yZURTUmVzb3VyY2VzL01EU3RvcmVEU1Jlc291cmNlVHlwZQ==";
		manager.createMDStore(mdId);
		Assert.assertNotNull(manager.readMdStore(mdId));
		Assert.assertNotNull(manager.startTransaction(mdId, true));
		Assert.assertNotNull(manager.readMdStore(mdId));
		db.getCollection("metadataManager").drop();
		((MDStoreTransactionManagerImpl) manager).setManagerTable(null);
		db.getCollection("metadataManager").drop();
		((MDStoreTransactionManagerImpl) manager).setManagerTable(null);

	}

	@Test
	public void testCommit() throws MDStoreServiceException {
		UUID idCreation = UUID.randomUUID();
		String mdId = idCreation.toString() + "_TURTdG9yZURTUmVzb3VyY2VzL01EU3RvcmVEU1Jlc291cmNlVHlwZQ==";
		manager.createMDStore(mdId);
		String idCurrent = manager.readMdStore(mdId);
		String transaction = manager.startTransaction(mdId, true);
		// Assert.assertTrue(manager.commit(transaction, mdId));
		Assert.assertNotSame(idCurrent, manager.readMdStore(mdId));

	}

	@Ignore
	@Test
	public void testDateTime() throws MDStoreServiceException, UnknownHostException {
		Mongo mongo = new Mongo("localhost", 27017);
		DB dbinput = mongo.getDB("mdstore");
		DBCollection inputCollection = dbinput.getCollection("70e07e9f-b3bf-4423-8777-b159819e0c6a");

		Assert.assertNotNull(inputCollection.findOne().get("body"));
		UUID idCreation = UUID.randomUUID();

		String mdId = idCreation.toString() + "_TURTdG9yZURTUmVzb3VyY2VzL01EU3RvcmVEU1Jlc291cmNlVHlwZQ==";

		manager.createMDStore(mdId);
		dao.createMDStore(mdId, "a", "a", "a");

		String transId = manager.startTransaction(mdId, true);

		ArrayList<String> data = new ArrayList<String>();
		DBCursor cursor = inputCollection.find();

		for (int i = 0; i < 1000; i++) {
			data.add((String) cursor.next().get("body"));
		}
		dao.getMDStore(transId).feed(data, true);
		// manager.commit(transId, mdId);

		cursor = inputCollection.find();
		transId = manager.startTransaction(mdId, false);

		data.clear();
		for (int i = 0; i < 10; i++) {
			data.add(cursor.next().get("body").toString().replace("oai:pumaoai.isti.cnr.it:", "SUUUCAAA"));
		}

		String currentId = manager.readMdStore(mdId);

		final MDStore newMdstore = dao.getMDStore(currentId);

		new Thread(() -> {
			List<String> dataInput = null;
			try {
				dataInput = newMdstore.deliver("", "", null).getResult(0, 10);
				for (int i = 0; i < 10; i++) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println(dataInput.get(i));
				}
			} catch (MDStoreServiceException e) {
				e.printStackTrace();
			}
		}).start();

		dao.getMDStore(transId).feed(data, true);
		// manager.commit(transId, mdId);

	}
}
