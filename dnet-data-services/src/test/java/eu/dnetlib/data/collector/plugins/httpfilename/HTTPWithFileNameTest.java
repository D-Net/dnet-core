package eu.dnetlib.data.collector.plugins.httpfilename;
import java.util.Iterator;

import org.junit.Ignore;
import org.junit.Test;


/**
 * Created by miriam on 07/05/2018.
 */
@Ignore
public class HTTPWithFileNameTest {

    private void iterate(Iterator<String> iterator, boolean exit){
        try{
            while (iterator.hasNext()){

                System.out.println(iterator.next());
                if(exit)
                    System.exit(0);


            }

        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    @Test
    @Ignore
    public void testRSCollectorFrontiers()
    {
        HTTPWithFileNameCollectorIterable rsc = new HTTPWithFileNameCollectorIterable("https://dev-openaire.d4science.org/RS/Frontiers/data/Frontiers/metadata/000/",null);
        iterate(rsc.iterator(),false);

    }

    @Test
    @Ignore
    public void testRSCollectorPLOSCount()
    {
        HTTPWithFileNameCollectorIterable rsc = new HTTPWithFileNameCollectorIterable("https://dev-openaire.d4science.org/RS/PLOS/data/public_library_of_science/metadata/354/","article-type=\"correction\"");
        Iterator<String> iterator = rsc.iterator();
        int count = 0;
        int body = 0;
        int corrections = 0;
        try{
            while (iterator.hasNext()){

                String meta = iterator.next();
                if (!meta.contains("article-type=\"correction\"")){
                    count++;
                    int index = meta.indexOf("<body>");
                    if(meta.substring(index).contains("<sec"))
                        body++;
                    else {
                        System.out.println(meta);
                        System.out.println(count);
                    }

                }else
                    corrections++;

            }
            System.out.println(count + "       "  + body + "                  " + corrections);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    @Test
    @Ignore
    public void testRSCollectorPLOS()
    {
        HTTPWithFileNameCollectorIterable rsc = new HTTPWithFileNameCollectorIterable("https://dev-openaire.d4science.org/RS/PLOS/data/public_library_of_science/metadata/400/","article-type=\"correction\"");


        iterate(rsc.iterator(),false);
    }

    @Test
    @Ignore
    public void testRSCollectorSpringer()
    {
        HTTPWithFileNameCollectorIterable rsc = new HTTPWithFileNameCollectorIterable("https://dev-openaire.d4science.org/RS/Springer-OA/data/Springer-OA/metadata/8a0/",null);

        iterate(rsc.iterator(),false);

    }

    @Test
    public void testEmptyCollection()
    {
        HTTPWithFileNameCollectorIterable rsc = new HTTPWithFileNameCollectorIterable("",null);

        iterate(rsc.iterator(),true);
    }

    @Test
    @Ignore
    public void testCheck(){
        HTTPWithFileNameCollectorIterable rsc = new HTTPWithFileNameCollectorIterable("https://dev-openaire.d4science.org/RS/Elsevier/data/elsevier/metadata/000/",null);
        Iterator<String> iterator = rsc.iterator();
        int count=0;
        while (iterator.hasNext()) {
            iterator.next();
            count += 1;
        }
        System.out.println("count = " + count);
    }

}
