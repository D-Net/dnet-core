package eu.dnetlib.data.collector.plugins.csv;

import java.net.URISyntaxException;
import java.util.HashMap;

import eu.dnetlib.data.collector.plugins.HttpCSVCollectorPlugin;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class HTTPCSVCollectorPluginTest {

	private String FILE_URL = HTTPCSVCollectorPluginTest.class.getResource("testCSVwithBOM.csv").toString();
	final HttpCSVCollectorPlugin plugin = new HttpCSVCollectorPlugin();

	@Test
	public void testCSVHeader() throws URISyntaxException, CollectorServiceException {

		final InterfaceDescriptor descr = new InterfaceDescriptor();
		final HashMap<String, String> params = new HashMap<String, String>();

		params.put("separator", ",");
		params.put("quote", "\"");
		params.put("identifier", "ID");
		descr.setBaseUrl(FILE_URL);
		descr.setParams(params);

		int i = 0;
		for (final String s : plugin.collect(descr, null, null)) {
			assertTrue(s.length() > 0);
			System.out.println(s);
			i++;
		}
		System.out.println(i);
		assertTrue(i > 0);
	}

	@Test
	public void testVerifyQuotesOk(){
		String correct = "\"5\",\"Il Padrino\",\"EEEEEEEE \"\"ZZZZZ\"\" EEEEEEEEEE\",1970";
		assertTrue(plugin.verifyQuotes(correct, ','));
	}

	@Test
	public void testVerifyQuotesWRONG(){
		String correct = "5\",\"Il Padrino\",\"EEEEEEEE \"ZZZZZ\" EEEEEEEEEE\",1970";
		assertFalse(plugin.verifyQuotes(correct, ','));
	}

	@Test
	public void testSNSF(){
		String s = "\"8773\";\"3101-008773\";\"EMBO workshop on structure, function and regulation of membrane transport proteins\";\"\";\"Rossier Bernard C.\";\"Scientific Conferences\";\"Science communication\";\"DÃ©partement de Pharmacologie & Toxicologie FacultÃ© de Biologie et de MÃ©decine UniversitÃ© de Lausanne\";\"UniversitÃ© de Lausanne - LA\";\"30103\";\"Cellular Biology, Cytology\";\"Biology and Medicine;Basic Biological Research\";\"01.04.1987\";\"30.09.1987\";\"10000.00\";\"\";\"30103\"" ;
		assertTrue(plugin.verifyQuotes(s, ';'));
	}

	@Test
	public void testSNSF2(){
		String s = "\"11000\";\"4021-011000\";\"Literarische und nationale Erziehung : Schweizerisches SelbstverstÃ¤ndnis in der Literatur fÃ¼r Kinder und Jugend- liche\";\"\";\"Tschirky Rosmarie\";\"NRP 21 Cultural Diversity and National Identity\";\"Programmes;National Research Programmes (NRPs)\";\"Schweiz. Inst. fÃ¼r Kinder- und Jugendmedien\";\"UniversitÃ¤t ZÃ¼rich - ZH\";\"10501\";\"German and English languages and literature\";\"Human and Social Sciences;Linguistics and literature, philosophy\";\"10501\";\"01.10.1986\";\"31.03.1990\";\"308807.00\";\"\"";
		assertTrue(plugin.verifyQuotes(s, ';'));
	}

	@Test
	public void testSNSFInvalid(){
		String s = "\"35918\";\"1113-035918\";\"Entwicklung eines dreisprachigen Thesaurus des schweizerischen Rechts zur UnterstÃ¼tzung der Suche in Volltextdatenbanken.\";\"\";\"Verein \"Schweizerische Juristische Datenbank\"\";\"Project funding (Div. I-III)\";\"Project funding\";\"Verein \"\"Schweizerische Juristische Datenbank\"\"\";\"NPO (Biblioth., Museen, Verwalt.) - NPO\";\"10205\";\"Legal sciences\";\"Human and Social Sciences;Economics, law\";\"10205\";\"01.12.1992\";\"31.03.1995\";\"500366.00\";\"\"";
		assertFalse(plugin.verifyQuotes(s, ';'));
	}

}
