/**
 * 
 */
package eu.dnetlib.data.collector.plugins.rest;

import org.junit.Ignore;
import org.junit.Test;

/**
 * 
 * @author js, Andreas Czerniak
 * @date 2018-08-06
 */
public class RestIteratorTest {

	private String baseUrl = "https://share.osf.io/api/v2/search/creativeworks/_search";
	private String resumptionType = "count";
	private String resumptionParam = "from";
	private String resumptionXpath = "";
	private String resultTotalXpath = "//hits/total";
	private String entityXpath = "//hits/hits";
	private String resultFormatParam = "format";
	private String resultFormatValue = "Json";              //  Change from lowerCase to one UpperCase
	private String resultSizeParam = "size";
        private String resultSizeValue = "10";                  //  add  new
	private String query = "q=%28sources%3ASocArXiv+AND+type%3Apreprint%29";


	@Ignore
	@Test
	public void test(){
		final RestIterator iterator = new RestIterator(baseUrl, resumptionType, resumptionParam, resumptionXpath, resultTotalXpath, resultFormatParam, resultFormatValue, resultSizeParam, resultSizeValue, query, entityXpath);
		int i =20;
		while (iterator.hasNext() && i > 0) {
			String result = iterator.next();
			
			i--;
		}
	}
}

