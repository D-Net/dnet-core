package eu.dnetlib.data.collector.plugins.excel;

import java.util.HashMap;
import java.util.Iterator;

import eu.dnetlib.data.collector.plugins.HttpCSVCollectorPlugin;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
/**
 * Created by miriam on 10/05/2017.
 */
@Ignore
public class ReadExcelTest {
	private InterfaceDescriptor descr;
	private Read r;
	private Object asserNotNul;

	@Before
	public void setUp() throws Exception {
		descr = new InterfaceDescriptor();
		descr.setBaseUrl("https://pf.fwf.ac.at/en/research-in-practice/project-finder.xlsx?&&&search%5Bcall%5D=&search%5Bdecision_board_ids%5D=&search%5Bend_date%5D=&search%5Binstitute_name%5D=&search%5Blead_firstname%5D=&search%5Blead_lastname%5D=&search%5Bper_page%5D=10&search%5Bproject_number%5D=&search%5Bproject_title%5D=&search%5Bscience_discipline_id%5D=&search%5Bstart_date%5D=&search%5Bstatus_id%5D=&search%5Bwhat%5D=&action=index&controller=projects&locale=en&per_page=10" );
		HashMap<String, String> params = new HashMap<String, String>();

		params.put("argument", "{\"replace\":{\"header\":[{\"from\":\"&\",\"to\":\"and\"}],\"body\":[{\"from\":\"\\n\",\"to\":\" \"}]}," +
				"\"replace_currency\":[{\"from\":\"$\",\"to\":\"â‚¬\"}],\"col_currency\":10}");

		params.put("argument", "{\"replace\":{\"header\":[{\"from\":\"&\",\"to\":\"and\"}],\"body\":[{\"from\":\"\\n\",\"to\":\" \"}]}," +
				"\"replace_currency\":[{\"from\":\"$\",\"to\":\"â‚¬\"}],\"col_currency\":10}");
		params.put("header_row","4");
		params.put("tmp_file","//tmp//fwf.xslx");
		params.put("remove_empty_lines","yes");
		params.put("remove_lines_with_id"," â€“ ");
		params.put("col_id","1");
		params.put("remove_tmp_file","no");
		params.put("sheet_number","0");
		params.put("file_to_save","/tmp/project_search.2017.05.10.csv");
		params.put("separator", ",");
		params.put("quote","\"");
		descr.setParams(params);
//		descr.setBaseUrl("file:///tmp/gsrt_whole.xlsx");
//		HashMap<String, String> params = new HashMap<String, String>();
//
//		params.put("header_row","0");
//		params.put("tmp_file","/tmp/ERC.xslx");
//		params.put("remove_empty_lines","yes");
//		params.put("remove_lines_with_id"," â€“ ");
//		params.put("col_id","2");
//		params.put("remove_tmp_file","no");
//		params.put("sheet_number","0");
//		params.put("file_to_save","/tmp/ERC.csv");
//		params.put("separator", ",");
//		params.put("quote","\"");
//		descr.setParams(params);
//		r = new Read(descr);
//		r.setCollector(new HttpCSVCollectorPlugin());
	}

	@Test
	@Ignore
	public void readExcelFromUrl()throws Exception{
		Iterator<String> it = r.parseFile().iterator();
		int i = 0;
		String st = null;
		try {
			while (it.hasNext()) {
				st = it.next();
				Assert.assertNotNull(st);
				i++;
				//System.out.println(it.next());
			}
		}catch(Exception e){
			System.out.println("linea " + i);
			e.printStackTrace();
			System.out.println(st);
		}

	}

//	@Test
//	public void readExcelFromFile() throws Exception{
//		Iterator<String> it = r.parseFile().iterator();
//		int i =1;
//		String st = null;
//		try {
//			while (it.hasNext()) {
//				st = it.next();
//				Assert.assertNotNull(st);
//				i++;
//				//System.out.println(it.next());
//			}
//		}catch(Exception e){
//			System.out.println("linea " + i);
//			e.printStackTrace();
//			System.out.println(st);
//		}
//
//
//	}
}
