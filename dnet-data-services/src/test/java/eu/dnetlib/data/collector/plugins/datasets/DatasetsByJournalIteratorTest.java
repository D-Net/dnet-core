package eu.dnetlib.data.collector.plugins.datasets;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Lists;

import eu.dnetlib.data.collector.rmi.CollectorServiceException;

public class DatasetsByJournalIteratorTest {

	@Before
	public void setUp() throws Exception {}

	@Test
	public void test() throws CollectorServiceException {
		List<PangaeaJournalInfo> inputList = Lists.newArrayList();

		PangaeaJournalInfo jp = new PangaeaJournalInfo();
		jp.setDatasourceId("dsId1");
		jp.setJournalId("journal10825");
		jp.setJournalName("journal Name");
		inputList.add(jp);

		DatasetsByJournalIterator iterator = new DatasetsByJournalIterator(inputList.iterator());

		int i = 0;
		for (String s : iterator) {
			Assert.assertNotNull(s);
			if (i++ == 100) {
				i--;
				break;
			}
		}
		Assert.assertEquals(i, 100);

	}

}
