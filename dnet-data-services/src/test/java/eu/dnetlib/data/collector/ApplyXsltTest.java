package eu.dnetlib.data.collector;

import org.junit.Before;
import org.junit.Test;

import eu.dnetlib.miscutils.functional.xml.ApplyXslt;

public class ApplyXsltTest {

	private final String xslt = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
			+ "<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" "
			+ "xmlns:dyn=\"http://exslt.org/dynamic\" extension-element-prefixes=\"dyn\"> " + "<xsl:param name=\"elem\" select=\"string('aa')\"/> "
			+ "<xsl:template match=\"/\"> " + "<output>" + "<cc><xsl:value-of select=\"//*[local-name()=$elem]\" /></cc>" + "</output>"
			+ "</xsl:template>" + "</xsl:stylesheet>";

	private ApplyXslt f;

	@Before
	public void setUp() throws Exception {
		//System.out.println(xslt);
		f = new ApplyXslt(xslt);
		System.out.println(f.getTransformer().getClass().getCanonicalName());
	}

	@Test
	public void test() {
		System.out.println(f.evaluate("<record><aa>1234</aa><bb>metadata</bb></record>"));
	}

}
