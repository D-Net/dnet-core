package eu.dnetlib.data.collector.plugins.datasets;

import java.net.URL;
import java.util.HashMap;

import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
@Ignore
public class DatasetsByProjectIteratorTest {

	@Before
	public void setUp() throws Exception {}

	@Test
	public void test() throws CollectorServiceException {
		URL resource = DatasetsByProjectIteratorTest.class.getResource("pangaea-eu-projects_Openaire.csv");
		InterfaceDescriptor descr = new InterfaceDescriptor();
		HashMap<String, String> params = new HashMap<String, String>();
		descr.setBaseUrl(resource.toString());
		descr.setParams(params);
		DatasetsByProjectPlugin plugin = new DatasetsByProjectPlugin();
		Iterable<String> result = plugin.collect(descr, "", "");

		int i = 0;
		for (String s : result) {
			Assert.assertNotNull(s);
			System.out.println(s);
			//System.out.println("Parsed " + i++);
			break;
		}

	}

}
