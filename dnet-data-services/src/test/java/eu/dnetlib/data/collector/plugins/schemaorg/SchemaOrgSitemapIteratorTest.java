package eu.dnetlib.data.collector.plugins.schemaorg;

import eu.dnetlib.data.collector.plugins.schemaorg.sitemapindex.SitemapFileIterator;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Ignore
public class SchemaOrgSitemapIteratorTest {
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() throws CollectorServiceException {
		URL resource = SchemaOrgSitemapIteratorTest.class.getResource("sitemap.xml");

		HashMap<String,String> params = new HashMap<>();
		params.put("repositoryAccessType", "sitemapindex");
		params.put("consumerBlockPolling", Boolean.toString(true));
		params.put("consumerBlockPollingTimeout", "2");
		params.put("consumerBlockPollingTimeoutUnit", TimeUnit.MINUTES.toString());
		params.put("endpointCharset", StandardCharsets.UTF_8.name());
		params.put("updatedDateFormat", "YYYY-MM-DD");
		params.put("createdDateFormat", "YYYY-MM-DD");
		params.put("publicationDateFormat", "YYYY-MM-DD");
		params.put("contributorFallbackType", DatasetDocument.Contributor.ContributorType.Other.toString());
		params.put("identifierFallbackType", null);
		params.put("identifierFallbackURL", Boolean.toString(true));
		params.put("identifierMappingARK", "ark, ARK");
		params.put("identifierMappingDOI", "doi, DOI");
		params.put("identifierMappingHandle", "Handle, HANDLE");
		params.put("identifierMappingPURL", "purl, PURL");
		params.put("identifierMappingURN", "urn, URN");
		params.put("identifierMappingURL", "url, URL");

		params.put("repositoryAccessType", "sitemapindex");
		params.put("sitemap_queueSize", "100");
		params.put("sitemap_IndexCharset", StandardCharsets.UTF_8.name());
		params.put("sitemap_FileCharset", StandardCharsets.UTF_8.name());
		params.put("sitemap_FileSchema", SitemapFileIterator.Options.SitemapSchemaType.Text.toString());
		params.put("sitemap_FileType", SitemapFileIterator.Options.SitemapFileType.Text.toString());

		InterfaceDescriptor descriptor = new InterfaceDescriptor();
		descriptor.setId("schema.org - reactome");
		descriptor.setBaseUrl(resource.toString());
		descriptor.setParams(params);

		SchemaOrgPlugin schemaOrgPlugin = new SchemaOrgPlugin();

		Iterable<String> iterable = schemaOrgPlugin.collect(descriptor, null, null);

		List<Integer> lengths =new ArrayList<>();
		int count =0;
		for(String item : iterable) {
			count += 1;
			lengths.add(item.length());
		}
		Assert.assertEquals(2, count);
		Assert.assertEquals(1626, (int)lengths.get(0));
		Assert.assertEquals(48, (int)lengths.get(1));

	}
}
