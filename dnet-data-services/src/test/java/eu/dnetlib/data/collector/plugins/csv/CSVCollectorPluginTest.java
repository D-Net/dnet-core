package eu.dnetlib.data.collector.plugins.csv;

import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;

import eu.dnetlib.data.collector.plugins.FileCSVCollectorPlugin;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;

public class CSVCollectorPluginTest {

	@Test
	public void testCSVHeader() throws URISyntaxException, CollectorServiceException {
		URL resource = CSVCollectorPluginTest.class.getResource("/eu/dnetlib/data/collector/filesystem/csv/input.tsv");
		InterfaceDescriptor descr = new InterfaceDescriptor();
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("header", "TrUe");
		params.put("separator", "\t");
		params.put("identifier", "56");
		descr.setBaseUrl(resource.toString());
		descr.setParams(params);
		FileCSVCollectorPlugin plugin = new FileCSVCollectorPlugin();
		int i = 0;
		for (String s : plugin.collect(descr, null, null)) {
			Assert.assertTrue(s.length() > 0);
			i++;
			System.out.println(s);
			break;
		}
		Assert.assertTrue(i > 0);

	}


	@Test
	public void testTSVQuote() throws URISyntaxException, CollectorServiceException {
		URL resource = CSVCollectorPluginTest.class.getResource("/eu/dnetlib/data/collector/filesystem/csv/input-quoted.tsv");
		InterfaceDescriptor descr = new InterfaceDescriptor();
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("header", "true");
		params.put("separator", ";");
		params.put("identifier", "0");
		params.put("quote", "\\\"");
		descr.setBaseUrl(resource.toString());
		descr.setParams(params);
		FileCSVCollectorPlugin plugin = new FileCSVCollectorPlugin();
		int i = 0;
		for (String s : plugin.collect(descr, null, null)) {
			Assert.assertTrue(s.length() > 0);
			i++;
			System.out.println(s);
			break;
		}
		Assert.assertTrue(i > 0);

	}
}
