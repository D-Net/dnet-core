package eu.dnetlib.data.collector.plugins.projects.gtr2;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileWriter;
import java.io.PrintWriter;

import com.ximpleware.AutoPilot;
import com.ximpleware.VTDGen;
import com.ximpleware.VTDNav;
import org.apache.commons.lang3.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
@Ignore
public class VTDXMLTest {

	private VTDGen vg;
	private VTDNav vn;
	private AutoPilot ap;

	private VTDGen vg_tmp;
	private VTDNav vn_tmp;
	private AutoPilot ap_tmp;

	private PrintWriter writer;
	//TODO: use resource and not full path
	private String inputFilePath =
			"/Users/alessia/workspace/dnet/dnet-collector-plugins/src/test/resources/eu.dnetlib.data.collector.plugins.projects.gtr2/projects.xml";

	@Test
	public void test() throws Exception {
		vg = new VTDGen();
		vg.parseFile(inputFilePath, false);
		vn = vg.getNav();
		ap = new AutoPilot(vn);
		String ns = "";
		ap.selectXPath(".//projects");
		ap.evalXPath();
		ns += "xmlns:ns1=\"" + vn.toNormalizedString(vn.getAttrVal("ns1")) + "\" ";
		ns += "xmlns:ns2=\"" + vn.toNormalizedString(vn.getAttrVal("ns2")) + "\" ";
		ns += "xmlns:ns3=\"" + vn.toNormalizedString(vn.getAttrVal("ns3")) + "\" ";
		ns += "xmlns:ns4=\"" + vn.toNormalizedString(vn.getAttrVal("ns4")) + "\" ";
		ns += "xmlns:ns5=\"" + vn.toNormalizedString(vn.getAttrVal("ns5")) + "\" ";
		ns += "xmlns:ns6=\"" + vn.toNormalizedString(vn.getAttrVal("ns6")) + "\" ";

		ap.selectXPath("//project");
		int res = -1;
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		int i = 0;
		while ((res = ap.evalXPath()) != -1) {
			writer = new PrintWriter(new BufferedWriter(new FileWriter("projectPackage_"+(++i)+".xml")));
			System.out.println(res);
			writer.println("<doc " + ns + ">");
			writeFragment(vn);
			VTDNav clone = vn.cloneNav();
			AutoPilot ap2 = new AutoPilot(clone);
			ap2.selectXPath(".//link[@rel='FUND']");
			vg_tmp = new VTDGen();

			while (ap2.evalXPath() != -1) {
				//String fund = clone.toNormalizedString(clone.getAttrVal("href"));
				evalXpath(clone.toNormalizedString(clone.getAttrVal("href")), ".//link[@rel='FUNDER']");
				String funder = vn_tmp.toNormalizedString(vn_tmp.getAttrVal("href"));
				vn_tmp.toElement(VTDNav.ROOT);
				writeFragment(vn_tmp);
				writeNewTagAndInfo(funder, "//name", "<funder> <name>", "</name></funder>", null);
			}
			ap2.resetXPath();
			ap2.selectXPath(".//link[@rel='LEAD_ORG']");
			while (ap2.evalXPath() != -1) {
				writeNewTagAndInfo(clone.toNormalizedString(clone.getAttrVal("href")), "//name", "<lead-org><name>", "</name>", null);
				writeNewTagAndInfo(clone.toNormalizedString(clone.getAttrVal("href")), ".", "<id>", "</id></lead-org>", "id");
			}
			ap2.resetXPath();
			ap2.selectXPath(".//link[@rel='PP_ORG']");
			while (ap2.evalXPath() != -1) {
				writeNewTagAndInfo(clone.toNormalizedString(clone.getAttrVal("href")), "//name", "<pp-org><name>", "</name></pp-org>", null);
				writeNewTagAndInfo(clone.toNormalizedString(clone.getAttrVal("href")), ".", "<id>", "</id></lead-org>", "id");
			}
			ap2.resetXPath();

			ap2.selectXPath(".//link[@rel='PI_PER']");
			while (ap2.evalXPath() != -1) {
				setNavigator(clone.toNormalizedString(clone.getAttrVal("href")));
				vn_tmp.toElement(VTDNav.ROOT);
				writeFragment(vn_tmp);
			}
			writer.println("</doc>");
			writer.close();
		}

	}

	private void setNavigator(String httpUrl) {
		vg_tmp.clear();
		vg_tmp.parseHttpUrl(httpUrl, false);
		vn_tmp = vg_tmp.getNav();
	}

	private int evalXpath(String httpUrl, String xPath) throws Exception {
		setNavigator(httpUrl);
		ap_tmp = new AutoPilot(vn_tmp);
		ap_tmp.selectXPath(xPath);
		return ap_tmp.evalXPath();
	}

	private void writeFragment(VTDNav nav) throws Exception {
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		nav.dumpFragment(b);
		writer.println(b);
		b.reset();
	}

	private void writeNewTagAndInfo(String search, String xPath, String xmlOpenTag, String xmlCloseTag, String attrName) throws Exception {
		int nav_res = evalXpath(search, xPath);
		if (nav_res != -1) {
			writer.println(xmlOpenTag);
			if(StringUtils.isNotBlank(attrName)) writer.println(vn_tmp.toNormalizedString(vn_tmp.getAttrVal(attrName)));
			else
				writer.println(vn_tmp.toNormalizedString(vn_tmp.getText()));
			writer.println(xmlCloseTag);
		}
	}

}
