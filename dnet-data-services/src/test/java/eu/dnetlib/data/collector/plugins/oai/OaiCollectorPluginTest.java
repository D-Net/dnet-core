package eu.dnetlib.data.collector.plugins.oai;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.internal.verification.Times;
import org.mockito.junit.MockitoJUnitRunner;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;
import eu.dnetlib.data.collector.rmi.ProtocolDescriptor;
import eu.dnetlib.data.collector.rmi.ProtocolParameter;

@RunWith(MockitoJUnitRunner.class)
public class OaiCollectorPluginTest {

	private OaiCollectorPlugin oai;

	@Mock
	private OaiIteratorFactory oaiIteratorFactory;

	private List<String> elements = Lists.newArrayList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12");

	private Iterator<String> oaiIterator1 = elements.subList(0, 3).iterator();
	private Iterator<String> oaiIterator2 = elements.subList(3, 7).iterator();
	private Iterator<String> oaiIterator3 = elements.subList(7, elements.size()).iterator();

	private static final String BASE_URL = "http://oai.test.it/oai";
	private static final String FORMAT = "oai_dc";
	private static final String PROTOCOL = "OAI";
	private static final String SET_1 = "set01";
	private static final String SET_2 = "set02";
	private static final String SET_3 = "set03";

	@Before
	public void setUp() {
		oai = new OaiCollectorPlugin();
		oai.setOaiIteratorFactory(oaiIteratorFactory);
		oai.setProtocolDescriptor(new ProtocolDescriptor(PROTOCOL, new ArrayList<ProtocolParameter>()));
		when(oaiIteratorFactory.newIterator(BASE_URL, FORMAT, SET_1, null, null)).thenReturn(oaiIterator1);
		when(oaiIteratorFactory.newIterator(BASE_URL, FORMAT, SET_2, null, null)).thenReturn(oaiIterator2);
		when(oaiIteratorFactory.newIterator(BASE_URL, FORMAT, SET_3, null, null)).thenReturn(oaiIterator3);
	}

	public void test() {
		oai = new OaiCollectorPlugin();
	}

	@Test
	public void testGetProtocol() {
		assertEquals(PROTOCOL, oai.getProtocol());
	}

	@Test
	public void testCollect() throws Exception {
		final InterfaceDescriptor iface = new InterfaceDescriptor();
		iface.setId("123");
		iface.setProtocol(PROTOCOL);
		iface.setBaseUrl(BASE_URL);
		iface.setParams(new HashMap<String, String>());
		iface.getParams().put("format", FORMAT);
		iface.getParams().put("set", Joiner.on(", ").join(SET_1, SET_2, SET_3));

		final Iterable<String> records = oai.collect(iface, null, null);

		assertNotNull(records);
		verify(oaiIteratorFactory, new Times(0)).newIterator(BASE_URL, FORMAT, SET_1, null, null);
		verify(oaiIteratorFactory, new Times(0)).newIterator(BASE_URL, FORMAT, SET_2, null, null);
		verify(oaiIteratorFactory, new Times(0)).newIterator(BASE_URL, FORMAT, SET_3, null, null);

		int count = 0;
		for (String s : records) {
			System.out.println("RECORD: " + s);
			assertEquals("" + count, s);
			count++;
		}
		assertEquals(elements.size(), count);
		verify(oaiIteratorFactory).newIterator(BASE_URL, FORMAT, SET_1, null, null);
		verify(oaiIteratorFactory).newIterator(BASE_URL, FORMAT, SET_2, null, null);
		verify(oaiIteratorFactory).newIterator(BASE_URL, FORMAT, SET_3, null, null);
	}
}
