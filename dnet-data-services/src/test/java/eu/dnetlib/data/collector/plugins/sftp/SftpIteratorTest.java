package eu.dnetlib.data.collector.plugins.sftp;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Set;

import com.google.common.collect.Sets;
import com.jcraft.jsch.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by andrea on 12/01/16.
 */
@Ignore
public class SftpIteratorTest {

	private static final Log log = LogFactory.getLog(SftpIteratorTest.class);
/*
	private String baseUrl = "sftp://fts.ec.europa.eu/H2020";
	private String username = "openaire";
	private String password = "I732x6854e";
*/

    private String baseUrl = "sftp://node6.t.openaire.research-infrastructures.eu/corda_h2020/";
    private String username = "openaire";
    private String password = "OpenDnet.";


    private boolean isRecursive = true;
    private Set<String> extensions = Sets.newHashSet("xml");

	private int mtime = 1458471600;
	private String mtimeStr = "Sun Mar 20 12:00:00 CET 2016";

	@Ignore
	@Test
    public void test() {
	    SftpIterator iterator = new SftpIterator(baseUrl, username, password, isRecursive, extensions, null);
	    while (iterator.hasNext()) {
            String remotePath = iterator.next();
            System.out.println(remotePath);
        }
    }

	@Ignore
	@Test
	public void timeTest() throws JSchException, SftpException, URISyntaxException {
		URI sftpServer = new URI(baseUrl);
		final String sftpURIScheme = sftpServer.getScheme();
		final String sftpServerAddress = sftpServer.getHost();
		final String remoteSftpBasePath = sftpServer.getPath();
		JSch jsch = new JSch();
		JSch.setConfig("StrictHostKeyChecking", "no");
		Session sftpSession = jsch.getSession(username, sftpServerAddress);
		sftpSession.setPassword(password);
		sftpSession.connect();

		Channel channel = sftpSession.openChannel(sftpURIScheme);
		channel.connect();
		ChannelSftp sftpChannel = (ChannelSftp) channel;
		sftpChannel.cd(sftpChannel.pwd() + remoteSftpBasePath);
		log.info("Connected to SFTP server " + sftpServerAddress);

		final SftpATTRS lstat = sftpChannel.lstat("H20_OpenAire_Project_633002.xml");
		int mtime = lstat.getMTime();
		String mtimeStr = lstat.getMtimeString();
		log.info("mtime int: " + mtime);
		log.info("mtime string: " + mtimeStr);

		sftpChannel.exit();
		sftpSession.disconnect();

		log.info("trying to get Date from mtimestring");
		DateTime dt = new DateTime(mtimeStr);
		log.info("Joda DateTime from mtimeString: " + dt.toString());
	}

	@Test
	public void timeTestParseString() {
		log.info("trying to get DateTime from mtimestring");
		//DateTimeFormatter formatter = DateTimeFormat.forPattern("EEE MMM dd HH:mm:ss 'CET' yyyy");
		DateTimeFormatter formatter = DateTimeFormat.forPattern("EEE MMM dd %");
		DateTime dt = DateTime.parse(mtimeStr, formatter);
		log.info("Joda DateTime from mtimeString: " + dt.toString());
	}

	@Test
	public void testTimeFromInt() {
		DateTime dt = new DateTime(mtime * 1000L);
		log.info("Joda DateTime from mtimeInt: " + dt.toString());
	}

	@Test
	public void timeTestAfter() {
		DateTime dt = new DateTime(mtime * 1000L);
		DateTime now = new DateTime();
		assertFalse(now.isBefore(dt));
		assertTrue(now.isAfter(dt));
	}

}