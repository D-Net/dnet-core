package eu.dnetlib.data.collector.plugins.projects.grist;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * GristProjectsIterable Tester.
 *
 * @author alessia
 * @version 1.0
 * @since <pre>Apr 22, 2016</pre>
 */
@Ignore
public class GristProjectsIterableTest {

	private String baseUrl = "http://www.ebi.ac.uk/europepmc/GristAPI/rest/get/query=ga:%22Wellcome%20Trust%22&resultType=core";
	private GristProjectsIterable iterable;
	private Iterator<String> it;
	private SAXReader reader;

	@Before
	public void before() throws Exception {
		iterable = new GristProjectsIterable(baseUrl);
		it = iterable.iterator();
		reader = new SAXReader();
	}

	/**
	 * Method: hasNext()
	 */
	@Test
	public void testHasNext() throws Exception {
		assertTrue(it.hasNext());
	}

	/**
	 * Method: next()
	 */
	@Test
	public void testNext() throws Exception {
		assertNotNull(it.next());
	}

	/**
	 * Method: remove()
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void testRemove() throws Exception {
		it.remove();
	}

	@Test
	public void iterateToNextPage() {
		for (int maxInPage = 25; maxInPage > 0; maxInPage--) {
			it.next();
		}
		if (it.hasNext()) {
			System.out.println(it.next());
		}
	}

	@Test
	public void checkProjectIdentifiers() throws DocumentException, IOException {
		List<String> identifiers = Lists.newArrayList();
		List<String> duplicates = Lists.newArrayList();
		Iterator<String> it2 = iterable.iterator();
		while (it2.hasNext()) {
			String id = parseId(it2.next());
			if (identifiers.contains(id)) {
				System.out.println("Found duplicate identifier: " + id);
				duplicates.add(id);
			}
			identifiers.add(id);
		}

		int listSize = identifiers.size();
		System.out.println("Total grant ids " + listSize);
		Set<String> set = Sets.newHashSet(identifiers);
		Set<String> dupSet = Sets.newHashSet(duplicates);
		System.out.println("Unique grant ids: " + set.size());
		System.out.println("Duplicate grant ids: " + dupSet.size());
		System.out.println();
		serializeSetOnFile(dupSet);
	}

	private String parseId(String record) throws DocumentException {
		Document doc = reader.read(IOUtils.toInputStream(record));
		return doc.selectSingleNode("//Grant/Id").getText();
	}

	private void serializeSetOnFile(Set<String> s) throws IOException {
		File tmpDir = FileUtils.getTempDirectory();
		System.out.println("Saving list in " + tmpDir.getAbsolutePath());
		FileUtils.writeLines(FileUtils.getFile(tmpDir, "WT_duplicates.txt"), s);
	}

}
