package eu.dnetlib.data.collector.plugins.opentrial;

/**
 * Created by miriam on 07/03/2017.
 */

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Iterator;

import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class OpentrialTest {

	@Test
	public void importOpentrial() throws Exception {
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter("opentrials.xml")));
		OpenTrialIterator trial = new OpenTrialIterator("https://api.opentrials.net/v1/search?",null,null);
		Iterator<String> iterator = trial.iterator();
		int parse_number = 0;
		while(iterator.hasNext() && parse_number < 30){
			writer.println("<doc>" + iterator.next() + "</doc>");
			parse_number++;
		}
		writer.close();

	}

}
