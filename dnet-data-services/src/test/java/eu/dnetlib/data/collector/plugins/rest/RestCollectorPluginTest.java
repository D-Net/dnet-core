/**
 * 
 */
package eu.dnetlib.data.collector.plugins.rest;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;
import eu.dnetlib.data.collector.rmi.ProtocolDescriptor;

/**
 * @author js, Andreas Czerniak
 *
 */
public class RestCollectorPluginTest {

	private String baseUrl = "https://share.osf.io/api/v2/search/creativeworks/_search";
	private String resumptionType = "count";
	private String resumptionParam = "from";
	private String entityXpath = "//hits/hits";
	private String resumptionXpath = "//hits";
	private String resultTotalXpath = "//hits/total";
	private String resultFormatParam = "format";
	private String resultFormatValue = "json";
	private String resultSizeParam = "size";
        private String resultSizeValue = "10";
	// private String query = "q=%28sources%3ASocArXiv+AND+type%3Apreprint%29";
	private String query = "q=%28sources%3AengrXiv+AND+type%3Apreprint%29";
    // private String query = "=(sources:engrXiv AND type:preprint)";
    
	private String protocolDescriptor = "rest_json2xml"; 
	private InterfaceDescriptor ifDesc = new InterfaceDescriptor();
	private RestCollectorPlugin rcp;
	
	@Before
	public void setUp() {
		HashMap<String, String> params = new HashMap<>();
		params.put("resumptionType", resumptionType);
		params.put("resumptionParam", resumptionParam);
		params.put("resumptionXpath", resumptionXpath);
		params.put("resultTotalXpath", resultTotalXpath);
		params.put("resultFormatParam", resultFormatParam);
		params.put("resultFormatValue", resultFormatValue);
		params.put("resultSizeParam", resultSizeParam);
                params.put("resultSizeValue", resultSizeValue);
		params.put("queryParams", query);
		params.put("entityXpath", entityXpath);
		
		ifDesc.setBaseUrl(baseUrl);
		ifDesc.setParams(params);
		
		ProtocolDescriptor pd = new ProtocolDescriptor();
		pd.setName(protocolDescriptor);
		rcp = new RestCollectorPlugin();
		rcp.setProtocolDescriptor(pd);
	}

	@Ignore
	@Test
	public void test() throws CollectorServiceException{
		
		int i = 0;
		for (String s : rcp.collect(ifDesc, "", "")) {
			Assert.assertTrue(s.length() > 0);
			i++;
			System.out.println(s);
			if (i > 200)
				break;
		}
		System.out.println(i);
		Assert.assertTrue(i > 0);
	}
}

