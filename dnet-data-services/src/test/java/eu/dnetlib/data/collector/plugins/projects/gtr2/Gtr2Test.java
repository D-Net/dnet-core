package eu.dnetlib.data.collector.plugins.projects.gtr2;

import java.util.Iterator;

import com.ximpleware.VTDGen;
import eu.dnetlib.data.collector.plugins.HttpConnector;
import eu.dnetlib.miscutils.functional.xml.TryIndentXmlString;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Ignore
public class Gtr2Test {

	private String baseURL = "http://gtr.rcuk.ac.uk/gtr/api";
	private Gtr2Helper helper;
	private Gtr2ProjectsIterable it;
	private HttpConnector connector;

	@Before
	public void prepare() {
		helper = new Gtr2Helper();
		//System.setProperty("jsse.enableSNIExtension","false");
	}

	@Test
	public void testOne() throws Exception {
		System.out.println("one project");
		VTDGen vg_tmp = new VTDGen();
		connector = new HttpConnector();
		byte[] bytes = connector.getInputSource("http://gtr.rcuk.ac.uk/gtr/api/projects/E178742B-571B-498F-8402-122F17C47546").getBytes("UTF-8");
		//vg_tmp.parseHttpUrl("https://gtr.rcuk.ac.uk/gtr/api/projects/E178742B-571B-498F-8402-122F17C47546", false);
		vg_tmp.setDoc(bytes);
		vg_tmp.parse(false);
		String s = helper.processProject(vg_tmp.getNav(), "xmlns:ns=\"http:///afgshs\"");
		System.out.println(s);
	}

	@Test
	public void testPaging() throws Exception {
		it = new Gtr2ProjectsIterable(baseURL, null, 2, 3);
		TryIndentXmlString indenter = new TryIndentXmlString();
		Iterator<String> iterator = it.iterator();
		while (iterator.hasNext()) {
			Thread.sleep(300);
			String res = iterator.next();
			assertNotNull(res);
			indenter.evaluate(res);
		}
	}

	@Test
	public void testOnePage() throws Exception {
		it = new Gtr2ProjectsIterable(baseURL, null, 12, 12);
		int count = iterateAndCount(it.iterator());
		assertEquals(20, count);
	}

	@Test
	public void testIncrementalHarvestingNoRecords() throws Exception {
		System.out.println("incremental Harvesting");
		it = new Gtr2ProjectsIterable(baseURL, "2050-12-12", 11, 13);
		int count = iterateAndCount(it.iterator());
		assertEquals(0, count);
	}

	@Test
	public void testIncrementalHarvesting() throws Exception {
		System.out.println("incremental Harvesting");
		it = new Gtr2ProjectsIterable(baseURL, "2016-11-30", 11, 11);
		int count = iterateAndCount(it.iterator());
		assertEquals(20, count);
	}

	@Test
	public void testMultithreading() throws Exception {
		System.out.println("testing new multithreading configuration");
		it = new Gtr2ProjectsIterable(baseURL, null);
		TryIndentXmlString indenter = new TryIndentXmlString();
		it.setEndAtPage(3);
		Iterator<String> iterator = it.iterator();
		while (iterator.hasNext()) {
			String res = iterator.next();
		assertNotNull(res);
		//	System.out.println(res);
//			Scanner keyboard = new Scanner(System.in);
//			System.out.println("press enter for next record");
//			keyboard.nextLine();

		}
	}

	private int iterateAndCount(final Iterator<String> iterator) throws Exception{
		int i = 0;
		while (iterator.hasNext()) {
			assertNotNull(iterator.next());
			i++;
		}
		System.out.println("Got "+i+" projects");
		return i;
	}
}
