package eu.dnetlib.data.objectstore.filesystem;

import java.net.UnknownHostException;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

// TODO: Auto-generated Javadoc
/**
 * The Class ConfigurationTestConfig.
 */
@Configuration
public class ConfigurationTestConfig {

	/**
	 * Mongo db.
	 *
	 * @return the db
	 * @throws UnknownHostException the unknown host exception
	 */
	@Bean
	public MongoDatabase objectstoreMongoDB() throws UnknownHostException {
		MongoClient mongoClient = new MongoClient("localhost");
		return mongoClient.getDatabase("objectStoreTest");

	}


	@Bean
	public FileSystemObjectStoreDao fsObjectStoreDAO() {
		FileSystemObjectStoreDao fileSystemObjectStoreDao = new FileSystemObjectStoreDao();

		fileSystemObjectStoreDao.setObjectStoreRESTURI("http://www.objectstore.com");
		return fileSystemObjectStoreDao;

	}

	/**
	 * Fs utility.
	 *
	 * @return the file system utility
	 */
	@Bean
	public FileSystemUtility fsUtility () {
		return new FileSystemUtility();
	}


}
