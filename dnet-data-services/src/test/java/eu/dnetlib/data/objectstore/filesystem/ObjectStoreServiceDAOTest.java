package eu.dnetlib.data.objectstore.filesystem;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import com.mongodb.client.MongoDatabase;
import eu.dnetlib.data.objectstore.modular.connector.ObjectStore;
import eu.dnetlib.data.objectstore.rmi.ObjectStoreFile;
import eu.dnetlib.data.objectstore.rmi.ObjectStoreServiceException;
import eu.dnetlib.enabling.resultset.ResultSetListener;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * TODO: Test class set to ignored because it requires a mongo server running on localhost. Tests with mocks should be prepared.
 * @author sandro
 *
 */
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ConfigurationTestConfig.class)
public class ObjectStoreServiceDAOTest {
	private static final Log log = LogFactory.getLog(ObjectStoreServiceDAOTest.class); // NOPMD by marko on 11/24/08 5:02 PM

	@Autowired
	private FileSystemObjectStoreDao objectStoreDAO;

	@Autowired
	private MongoDatabase objectstoreMongoDB;

	private String interpretation = "interp";
	private String basePath = "/tmp/basePath";
	//must be 36 chars
	private String obsID = "test56789111315171921232527293133350";
	private String pdfPath = "./test.pdf";

	@Before
	public void setup() throws IOException, ObjectStoreServiceException {
		Path baseDirPath = FileSystems.getDefault().getPath(basePath);
		if (!Files.exists(baseDirPath)) {
			Files.createDirectory(baseDirPath);
		}
		objectStoreDAO.createObjectStore(obsID + "_T2JqZWN0U3RvcmVEU1Jlc291cmNlcy9PYmplY3RTdG9yZURTUmVzb3VyY2VUeXBl", interpretation, basePath);
	}

	@After
	public void tearDown() throws IOException, ObjectStoreServiceException {
		objectstoreMongoDB.drop();
		FileUtils.deleteDirectory(new File(basePath));
	}

	@Test
	public void testList() {
		final List<String> objectStores = this.objectStoreDAO.listObjectStores();
		for (String o : objectStores)
			System.out.println(o);
		assertTrue(objectStores.contains(obsID + "_T2JqZWN0U3RvcmVEU1Jlc291cmNlcy9PYmplY3RTdG9yZURTUmVzb3VyY2VUeXBl"));
	}

	@Test
	public void testGet() throws ObjectStoreServiceException {
		ObjectStore o = objectStoreDAO.getObjectStore(obsID);
		System.out.println(o.toString());
		assertNotNull(o);
	}

	@Test
	public void testFeed() throws ObjectStoreServiceException, FileNotFoundException {
		objectStoreDAO.getObjectStore(obsID).feed(new InputIterator(),true);
	}

	@Test
	public void testDeliverObject() throws ObjectStoreServiceException {
		objectStoreDAO.getObjectStore(obsID).feed(new InputIterator(), true);
		final ObjectStoreFile obj2 = objectStoreDAO.getObjectStore(obsID).deliverObject("Oggetto_2");
		System.out.println(obj2.toJSON());
		assertNotNull(obj2);
	}

	@Test
	public void testGetStream() throws Exception {
		objectStoreDAO.getObjectStore(obsID).feed(new InputIterator(), true);
		ResultSetListener rs = objectStoreDAO.getObjectStore(obsID).deliver(0L, System.currentTimeMillis());
		for (int i=1; i< (rs.getSize()/10); ) {

			int from  = i;
			int to = Math.max(rs.getSize(), i*10);

			List<String> data = rs.getResult(from, to);

			for (String s: data) {
				System.out.println(s);
			}

			i= to ;
		}
	}


}
