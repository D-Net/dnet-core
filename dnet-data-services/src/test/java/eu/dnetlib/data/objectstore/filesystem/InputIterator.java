/**
 *
 */
package eu.dnetlib.data.objectstore.filesystem;

import java.io.IOException;
import java.util.Iterator;

import eu.dnetlib.data.objectstore.modular.ObjectStoreRecord;
import eu.dnetlib.data.objectstore.rmi.ObjectStoreFile;
import eu.dnetlib.data.objectstore.rmi.Protocols;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 * @author sandro
 *
 */
public class InputIterator implements Iterable<ObjectStoreRecord>, Iterator<ObjectStoreRecord> {

	private int counter = 0;
	private String pdfResourcePath = "test.pdf";
	private Resource testPDF = new ClassPathResource(pdfResourcePath);

	/**
	 * {@inheritDoc}
	 * @see java.util.Iterator#hasNext()
	 */
	@Override
	public boolean hasNext() {

		return counter<100;
	}

	/**
	 * {@inheritDoc}
	 * @see java.util.Iterator#next()
	 */
	@Override
	public ObjectStoreRecord next() {
		try {
			counter ++;
			ObjectStoreRecord record = new ObjectStoreRecord();
			ObjectStoreFile fileMetadata = new ObjectStoreFile();
			fileMetadata.setAccessProtocol(Protocols.File_System);

			fileMetadata.setDownloadedURL("file://" + pdfResourcePath);
			fileMetadata.setURI("file://" + pdfResourcePath);
			fileMetadata.setObjectID("Oggetto_"+counter);
			fileMetadata.setMimeType("application/pdf");
			System.out.println("Aggiungo Elemento "+counter);
			record.setInputStream(testPDF.getInputStream());
			record.setFileMetadata(fileMetadata );
			return record;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public void remove() {
		// TODO Auto-generated method stub

	}

	/**
	 * {@inheritDoc}
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<ObjectStoreRecord> iterator() {
		counter =0;
		return this;
	}

}
