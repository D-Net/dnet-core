package eu.dnetlib.enabling.tools.blackboard;

import eu.dnetlib.data.mdstore.modular.action.MDStoreActions;

public class SampleCreateAction implements BlackboardServerAction<MDStoreActions> {

	@Override
	public void execute(BlackboardServerHandler handler, BlackboardJob job) {
		handler.done(job);
	}

}
