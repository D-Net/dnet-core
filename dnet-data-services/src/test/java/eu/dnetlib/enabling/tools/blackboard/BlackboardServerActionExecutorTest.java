package eu.dnetlib.enabling.tools.blackboard;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.dnetlib.data.mdstore.modular.action.MDStoreActions;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class BlackboardServerActionExecutorTest {

	@Resource
	public transient BlackboardServerHandler blackboardHandler;

	@Resource
	public transient BlackboardServerActionExecutor<MDStoreActions> executor;

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void testExecutor() {
		assertNotNull(executor);

		BlackboardJob job = mock(BlackboardJob.class);
		when(job.getAction()).thenReturn("CREATE");

		executor.execute(job);

		verify(blackboardHandler).done(eq(job));
	}

	@Test
	public void testExecutorUnimplemented() {
		assertNotNull(executor);

		BlackboardJob job = mock(BlackboardJob.class);
		when(job.getAction()).thenReturn("DELETE");

		executor.execute(job);

		verify(blackboardHandler).failed(eq(job), (Throwable) anyObject());
	}
}
