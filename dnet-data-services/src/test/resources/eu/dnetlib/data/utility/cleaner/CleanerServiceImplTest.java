package eu.dnetlib.data.utility.cleaner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnit44Runner;

import eu.dnetlib.data.utility.cleaner.rmi.CleanerException;
import eu.dnetlib.enabling.resultset.MappedResultSetFactory;
import eu.dnetlib.test.utils.EPRTestUtil;

@RunWith(MockitoJUnit44Runner.class)
public class CleanerServiceImplTest {

	/**
	 * Class under test.
	 */
	private CleanerServiceImpl service;

	@Mock
	private CleaningRuleFactory cleaningRuleFactory;
	@Mock
	private MappedResultSetFactory mappedResultSetFactory;
	@Mock
	private CleaningRule cleaningRule;

	private W3CEndpointReference epr_IN = EPRTestUtil.getTestEpr("http://1");
	private W3CEndpointReference epr_OUT = EPRTestUtil.getTestEpr("http://2");

	private static final String RULE_ID = "RULE_01";

	@Before
	public void setUp() throws Exception {
		when(cleaningRuleFactory.obtainCleaningRule(RULE_ID)).thenReturn(cleaningRule);
		when(mappedResultSetFactory.createMappedResultSet(epr_IN, cleaningRule)).thenReturn(epr_OUT);

		service = new CleanerServiceImpl();
		service.setCleaningRuleFactory(cleaningRuleFactory);
		service.setMappedResultSetFactory(mappedResultSetFactory);
	}

	@Test
	public void testClean() throws CleanerException {
		W3CEndpointReference epr = service.clean(epr_IN, RULE_ID);
		assertNotNull(epr);
		assertEquals(epr_OUT, epr);
		verify(cleaningRuleFactory).obtainCleaningRule(RULE_ID);
	}

	@Test(expected = CleanerException.class)
	public void testClean_null_1() throws CleanerException {
		service.clean(epr_IN, null);
	}

	@Test(expected = CleanerException.class)
	public void testClean_null_2() throws CleanerException {
		service.clean(null, RULE_ID);
	}
}
