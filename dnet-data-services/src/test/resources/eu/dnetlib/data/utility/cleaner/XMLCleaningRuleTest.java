package eu.dnetlib.data.utility.cleaner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnit44Runner;

import com.google.common.collect.Lists;

import eu.dnetlib.data.utility.cleaner.rmi.CleanerException;

@RunWith(MockitoJUnit44Runner.class)
public class XMLCleaningRuleTest {

	private static final String INPUT_VALID = "<record>" + "	<header />" + "	<metadata>" + "		<a>HELLO</a>" + "	</metadata>" + "</record>";

	private static final String INPUT_INVALID = "<record>" + "	<header />" + "	<metadata>" + "		<a>GOOD BYE</a>" + "	</metadata>" + "</record>";

	/**
	 * Class under test.
	 */
	private CleaningRule xmlRule;
	private XPATHCleaningRule mockXpathRule = new XPATHCleaningRule() {

		@Override
		protected Map<String, String> verifyValue(final String value) throws CleanerException {
			if (value.equals("CIAO")) { return null; }

			Map<String, String> err = new HashMap<String, String>();
			err.put("term", value);
			return err;
		}

		@Override
		protected String calculateNewValue(final String oldValue) throws CleanerException {
			if (oldValue.equals("HELLO")) { return "CIAO"; }
			return oldValue;
		}
	};;

	@Before
	public void setUp() throws Exception {
		xmlRule = new CleaningRule();

		mockXpathRule.setStrict(true);
		mockXpathRule.setXpath("//a");

		xmlRule.setXpathRules(Lists.newArrayList(mockXpathRule));
	}

	@Test
	public void testEvaluate_valid() {
		String s = xmlRule.evaluate(INPUT_VALID);
		assertTrue(s.contains("CIAO"));
		assertFalse(s.contains("invalid"));
	}

	@Test
	public void testEvaluate_invalid() {
		String s = xmlRule.evaluate(INPUT_INVALID);
		System.out.println(s);
		assertFalse(s.contains("CIAO"));
		assertTrue(s.contains("invalid"));
	}

}
