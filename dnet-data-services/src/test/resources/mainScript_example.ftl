// declare the import of script(s)
import (subScript_example.ftl);

// declare namespace(s)
declare_ns prefix=http://somenamespace.xyz.de;

// declare the script name
declare_script MainSample;

// set a rule of type function
lv = Convert(xpath:/dc.metadata.language,LangVocab);

// mark the end of the script
end
