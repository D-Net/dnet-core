package eu.dnetlib.common.profile;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.Node;

import eu.dnetlib.common.utils.XMLException;
import eu.dnetlib.common.utils.XMLUtils;

public abstract class DnetResource {

	private static final Log log = LogFactory.getLog(DnetResource.class);
	
	Document resource;
	
	public DnetResource() {
	}
	
	public DnetResource(Document resource){
		this.resource = resource;
	}
	
	public void setResource(Document resource){
		this.resource = resource;
	}
	
	public Document getResource(){
		return this.resource;
	}
	
	public String getValue(String xpathExpr){
		String value = null;
		try {
			value = XMLUtils.evaluate(resource, xpathExpr);
		} catch (XMLException e) {
			log.error(e);
		}
		return value;
	}
	
	public void setValue(String xpathExpr, String value){
		XMLUtils.getNode(resource, xpathExpr).setText(value);
	}
	
	public List<Node> getNodeList(String xpathExpr){
		List<Node> nodeList = null;
		try {
			nodeList = XMLUtils.getNodes(resource, xpathExpr);
		} catch (XMLException e) {
			log.error(e);
		}
		return nodeList;
	}
	
}
