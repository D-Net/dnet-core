package eu.dnetlib.common.profile;

import java.util.List;

public interface IResourceDao {
// TODO documentation
	public List<Resource> getResources(String xquery);
	
	public Resource getResource(String id) throws Exception;
	
	public Resource getResourceByQuery(String query) throws Exception;
	
	public void removeResource(String id, Resource resource);

	public void updateResource(String id, Resource resource);
	
}
