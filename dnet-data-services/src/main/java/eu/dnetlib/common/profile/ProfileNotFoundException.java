/**
 * 
 */
package eu.dnetlib.common.profile;

/**
 * @author jochen
 *
 */
public class ProfileNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6272083305345284826L;

	public ProfileNotFoundException(Throwable e) {
		super(e);
	}
	
	public ProfileNotFoundException(String msg, Throwable e) {
		super(msg, e);
	}
	
	public ProfileNotFoundException(String msg) {
		super(msg);
	}
	
	
}
