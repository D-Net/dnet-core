/**
 * 
 */
package eu.dnetlib.common.profile;

import java.util.List;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpDocumentNotFoundException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService;
// import eu.dnetlib.enabling.locators.DefaultUniqueServiceLocator;
import eu.dnetlib.enabling.tools.ServiceLocator;

/**
 * @author jochen
 *
 */
public class ResourceCache implements IResourceDaoSupport{

	private final LoadingCache<String, Resource> cache;
	@javax.annotation.Resource(name="lookupLocator")
	private ServiceLocator<ISLookUpService> lookupLocator;
	
	public ResourceCache() {
		cache = CacheBuilder.newBuilder().expireAfterWrite(24, TimeUnit.HOURS).build(new CacheLoader<String, Resource>(){

			@Override
			public Resource load(String aKey) throws Exception {
				Resource resource = null;
				if (aKey.startsWith("collection")){
					return new Resource(byQuery(aKey));
				}else{
					return new Resource(byId(aKey));
				}
			}
			
			private String byQuery(String aQuery) throws ISLookUpDocumentNotFoundException, ISLookUpException{
				return lookupLocator.getService().getResourceProfileByQuery(aQuery);
			}
			
			private String byId(String aId) throws ISLookUpDocumentNotFoundException, ISLookUpException{
				return lookupLocator.getService().getResourceProfile(aId);
			}
			
		});
	}
	
	@Override
	public List<Resource> getResources(String xquery) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Resource getResourceByXquery(String xquery)throws Exception {
		return cache.get(xquery);
	}

	@Override
	public Resource getResource(String id)throws Exception {
		return cache.get(id);
	}

	@Override
	public void updateResource(String id, Resource resource) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeResource(String id, Resource resource) {
		// TODO Auto-generated method stub
		
	}
	
	public void setLookupLocator(ServiceLocator<ISLookUpService> lookupLocator) {
		this.lookupLocator = lookupLocator;
	}

	public ServiceLocator<ISLookUpService> getLookupLocator() {
		return lookupLocator;
	}


}
