package eu.dnetlib.common.profile;

import java.util.List;


public class ResourceDao implements IResourceDao {

	private IResourceDaoSupport daoSupport;
	
	public List<Resource> getResources(String xquery) {
		return daoSupport.getResources(xquery);
	}
	
	@Override
	public Resource getResourceByQuery(String query)throws Exception {
		// currently only Xquery is supported
		return daoSupport.getResourceByXquery(query);
	}
	
	public Resource getResource(String id)throws Exception {
		return daoSupport.getResource(id);
	}
	
	public void removeResource(String id, Resource resource) {
		daoSupport.removeResource(id, resource);
	}

	public void updateResource(String id, Resource resource) {
		daoSupport.updateResource(id, resource);
	}

	public void setDaoSupport(IResourceDaoSupport daoSupport) {
		this.daoSupport = daoSupport;
	}

	public IResourceDaoSupport getDaoSupport() {
		return daoSupport;
	}



}
