package eu.dnetlib.common.profile;

import java.util.List;


public interface IResource {

	public String getValue(String xpathExpr);
	@SuppressWarnings("unchecked")
	public List getNodeList(String xpathExpr);
}
