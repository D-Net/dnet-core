package eu.dnetlib.common.profile;

import java.io.InputStream;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.io.SAXReader;

/**
 * @author jochen
 *
 */
public class Resource extends DnetResource implements IResource{
	

	public Resource(){super();}
	
	public Resource(Document resource){
		super(resource);
	}
	
	public Resource(String resourceProfile) throws DocumentException{
		super(DocumentHelper.parseText(resourceProfile));
	}
	
	public Resource(InputStream resourceProfileStream) throws DocumentException{
		super( (new SAXReader()).read(resourceProfileStream));
	}
	
}
