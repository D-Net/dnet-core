package eu.dnetlib.common.profile;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.DocumentException;

import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpDocumentNotFoundException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService;
import eu.dnetlib.enabling.is.registry.rmi.ISRegistryException;
import eu.dnetlib.enabling.is.registry.rmi.ISRegistryService;
import eu.dnetlib.enabling.tools.ServiceLocator;

/**
 * 
 * @author jochen
 * @deprecated this class is deprecated, use ResourceCache instead.
 *
 */
@Deprecated
public class ResourceDaoRemoteSupport implements IResourceDaoSupport{
	private static final Log log = LogFactory.getLog(ResourceDaoRemoteSupport.class);

	@javax.annotation.Resource(name="lookupLocator")
	private ServiceLocator<ISLookUpService> lookupLocator;
	@javax.annotation.Resource(name="registryLocator")
	private ServiceLocator<ISRegistryService> registryLocator;
	
	public List<Resource> getResources(String xquery){
		List<Resource> list = new LinkedList<Resource>();
		try {
			List<String> profileList = lookupLocator.getService().quickSearchProfile(xquery);
			if (profileList != null){
				for (String profile: profileList){
					Resource resource = new Resource(profile);
					list.add(resource);
				}				
			}
		} catch (ISLookUpException e) {
			log.error(e);
		} catch (DocumentException e) {
			log.error(e);
		}
		return list;
	}

	@Override
	public Resource getResourceByXquery(String xquery) {
		Resource resource = null;
		String profile;
		try{
			profile = lookupLocator.getService().getResourceProfileByQuery(xquery);
			resource = new Resource(profile);
		} catch (ISLookUpDocumentNotFoundException e) {
			log.error(e);
		} catch (ISLookUpException e) {
			log.error(e);
		} catch (DocumentException e) {
			log.error(e);
		}
		return resource;
	}
	
	public Resource getResource(String id) {
		Resource resource = null;
		String profile;
		try {
			profile = lookupLocator.getService().getResourceProfile(id);
			resource = new Resource(profile);
		} catch (ISLookUpDocumentNotFoundException e) {
			log.error(e);
		} catch (ISLookUpException e) {
			log.error(e);
		} catch (DocumentException e) {
			log.error(e);
		}
		return resource;
	}

	
	@Override
	public void removeResource(String id, Resource resource) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateResource(String id, Resource resource) {
		try {
			registryLocator.getService().updateProfile(resource.getValue("//RESOURCE_IDENTIFIER/@value"), resource.getResource().asXML(), resource.getValue("//RESOURCE_TYPE/@value"));
		} catch (ISRegistryException e) {
			log.error(e);
			throw new IllegalStateException("cannot update profile.", e);
		}
	}

	public void setRegistryLocator(ServiceLocator<ISRegistryService> registryLocator) {
		this.registryLocator = registryLocator;
	}

	public ServiceLocator<ISRegistryService> getRegistryLocator() {
		return registryLocator;
	}

	public void setLookupLocator(ServiceLocator<ISLookUpService> lookupLocator) {
		this.lookupLocator = lookupLocator;
	}

	public ServiceLocator<ISLookUpService> getLookupLocator() {
		return lookupLocator;
	}




}
