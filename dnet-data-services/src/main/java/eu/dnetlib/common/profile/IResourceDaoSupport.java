package eu.dnetlib.common.profile;

import java.util.List;

public interface IResourceDaoSupport {
// TODO documentation
	public List<Resource> getResources(String xquery);
	public Resource getResourceByXquery(String xquery) throws Exception;
	public Resource getResource(String id) throws Exception;
	public void updateResource(String id, Resource resource);
	public void removeResource(String id, Resource resource);
}
