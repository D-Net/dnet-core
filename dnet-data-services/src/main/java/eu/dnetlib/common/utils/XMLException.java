package eu.dnetlib.common.utils;

public class XMLException extends Exception {

	static final long serialVersionUID = 2413331108861490367L;

	public XMLException(String errorMessage){
		super(errorMessage);
	}
	
	public XMLException(Exception exc){
		super(exc);
	}
	
	public XMLException(String errorMessage, Throwable e){
		super(errorMessage, e);
	}

}
