/**
 * Copyright 2008-2009 DRIVER PROJECT (ICM UW)
 * Original author: Marek Horst
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.dnetlib.common.utils;

import java.io.StringReader;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.ws.wsaddressing.W3CEndpointReference;
import javax.xml.ws.wsaddressing.W3CEndpointReferenceBuilder;

import org.apache.log4j.Logger;
import org.dom4j.io.DocumentResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * Information Service utils class.
 * @author Marek Horst
 * @version 0.7.6
 *
 */
public class EprUtils {
	
	protected static final Logger log = Logger.getLogger(EprUtils.class);
	
	public static final String INDEX_RESULT_SET_NAME = "ICMResultSet";
	public static final String SERVICE_NAME = "IndexService";
	
	private DocumentResult infoset = new DocumentResult();

	
	/**
	 * @param epr - W3CEndpoint reference
	 * @param nsMap - mapping of namespace-prefix,uri pairs
	 */
	public EprUtils(W3CEndpointReference epr, Map<String, String> nsMap){
		epr.writeTo(infoset);
		XMLUtils.setNamespaces(nsMap);
	}

	/**
	 * @param xpathExpr
	 * @return the value obtained by the xpath evaluation
	 * @throws XMLException
	 */
	public String getValue(String xpathExpr) throws XMLException{
		return XMLUtils.evaluate(infoset.getDocument(), xpathExpr);
	}


	/**
	 * Parses ResultSetEPR to the String[] where: 
	 * String[0] - ResultSetService location, 
	 * String[1] - ResultSetId
	 * @param resultSetEPR
	 * @return string array where: String[0] - ResultSetService location, String[1] - ResultSetId
	 */
	@Deprecated
	public static String[] parseResultSetEPR(String resultSetEPR) {
		if (resultSetEPR==null || resultSetEPR.length()==0)
			return null;
        DocumentBuilderFactory factory =
            DocumentBuilderFactory.newInstance();
        factory.setIgnoringComments(true);
        factory.setValidating(false);
        DocumentBuilder db;
		try {
			db = factory.newDocumentBuilder();
			Document doc = db.parse(new InputSource(new StringReader(resultSetEPR)));
			Element documentElement = doc.getDocumentElement();
	    	NodeList nodeList = documentElement.getElementsByTagName("ResourceIdentifier:ResourceIdentifier");
	    	if (nodeList.getLength()!=1) {
	    		log.error("Invalid notifications of nodes for driver:ResourceIdentifier element. Expected 1, found: "+nodeList.getLength());
	    		return null;
	    	}
	    	if (nodeList.item(0)==null) {
	    		log.error("Couldn't find ResourceIdentifier:ResourceIdentifier element!");
	    		return null;
	    	}
	    	NodeList nodeListWSA = documentElement.getElementsByTagName("Address");
	    	if (nodeListWSA.getLength()!=1) {
	    		nodeListWSA = documentElement.getElementsByTagName("Address");
	    		if (nodeListWSA.getLength()!=1) {
	    			log.error("Invalid notifications of nodes for Address element. Expected 1, found: "+nodeListWSA.getLength());
	    			return null;
	    		}	
	    	}
	    	if (nodeListWSA.item(0)==null) {
	    		log.error("Couldn't find Address element!");
	    		return null;
	    	}
	    	return new String[] {
	    		getStringFromNode(nodeListWSA.item(0)),
	    		getStringFromNode(nodeList.item(0))
	    	};
	    	
		} catch (Exception e) {
			log.error("Exception occured when extracting ResultSet id from ResultSet service xml-type response!",e);
			return null;
		}
	}
	
	/**
	 * Extracts ResultSet identifier from ResultSet xml-type response.
	 * @param sourceResultSetId
	 * @return ResultSet identifier.
	 */
	public static String extractResultSetId(String sourceResultSetId) {
		if (sourceResultSetId==null || sourceResultSetId.length()==0)
			return null;
        DocumentBuilderFactory factory =
            DocumentBuilderFactory.newInstance();
        factory.setIgnoringComments(true);
        factory.setValidating(false);
        DocumentBuilder db;
		try {
			db = factory.newDocumentBuilder();
			Document doc = db.parse(new InputSource(new StringReader(sourceResultSetId)));
			Element documentElement = doc.getDocumentElement();
	    	NodeList nodeList = documentElement.getElementsByTagName("ResourceIdentifier:ResourceIdentifier");
	    	if (nodeList.getLength()!=1) {
	    		log.error("Invalid notifications of nodes for ResourceIdentifier:ResourceIdentifier element. Expected 1, found: "+nodeList.getLength());
	    		return null;
	    	}
	    	
	    	if (nodeList.item(0)==null) {
	    		log.error("Couldn't find ResourceIdentifier:ResourceIdentifier element!");
	    		return null;
	    	}
	    	return getStringFromNode(nodeList.item(0));
	    	
		} catch (Exception e) {
			log.error("Exception occured when extracting ResultSet id from ResultSet service xml-type response!",e);
			return null;
		}
	}
	
	
	private static String getStringFromNode(Node node)	{
	    
//		This code may not work on some jdk
//		Element resourceIdentifier = (Element) node;
//    	return resourceIdentifier.getTextContent();
		
		/*
		try {
			DOMSource domSource = new DOMSource(node);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.transform(domSource, result);
			return writer.toString();
		} catch (TransformerException e) {
			log.error("Exception occured when transforming node value!", e);
			return null;
		}
		*/
		
		return node.getFirstChild().getNodeValue(); 
	}

	/**
	 * Builds ResultSet end point reference for given serviceAddress and resultSetId.
	 * @param serviceAddress
	 * @param resultSetId
	 * @param wsdlLocation
	 * @return resultSet EPR
	 */
	public static String buildResultSetEPR(String serviceAddress, String resultSetId, 
			String wsdlLocation) {
		StringBuffer strBuff = new StringBuffer();
		strBuff.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
		strBuff.append("<wsa:EndpointReference xmlns:wsa=\"http://www.driver.org/schema\" xmlns:driver=\"http://www.driver.org\" xmlns:wsaw=\"http://www.w3.org/2006/02/addressing/wsdl\" xmlns:wsdl=\"http://www.w3.org/2005/08/wsdl-instance\">");
		strBuff.append("<Address>");
		strBuff.append(serviceAddress);
		strBuff.append("</Address>");
		strBuff.append("<wsa:ReferenceParameters>");
		strBuff.append("<ResourceIdentifier:ResourceIdentifier>");
		strBuff.append(resultSetId);
		strBuff.append("</ResourceIdentifier:ResourceIdentifier>");
		strBuff.append("</wsa:ReferenceParameters>");
		strBuff.append("<wsa:Metadata wsdl:wsdlLocation=\""+wsdlLocation+"\">");
		strBuff.append("<wsaw:ServiceName>");
		strBuff.append(INDEX_RESULT_SET_NAME);
		strBuff.append("</wsaw:ServiceName>");
		strBuff.append("</wsa:Metadata>");
		strBuff.append("</wsa:EndpointReference>");
		return strBuff.toString();
	}
	
	/**
	 * Builds W3C ResultSet end point reference for given serviceAddress and
	 * resultSetId.
	 * 
	 * @param serviceAddress
	 * @param resultSetId
	 * @param wsdlLocation
	 * @return W3C resultSet EPR
	 * @throws ParserConfigurationException 
	 */
	public static W3CEndpointReference buildW3CEPR(
			String serviceAddress, String wsdlLocation) throws ParserConfigurationException {

		final W3CEndpointReferenceBuilder W3CResultSetEPR = new W3CEndpointReferenceBuilder();

		W3CResultSetEPR.address(serviceAddress);
		W3CResultSetEPR.serviceName(new QName("http://www.w3.org/2006/02/addressing/wsdl",SERVICE_NAME));
		W3CResultSetEPR.endpointName(new QName("http://www.driver.org/schema",SERVICE_NAME));
		W3CResultSetEPR.wsdlDocumentLocation(wsdlLocation);

		/*
		final Document doc = DocumentBuilderFactory.newInstance()
				.newDocumentBuilder().newDocument();
		
		final Element referenceElement = doc.createElementNS(
				"http://www.driver.org", "driver:ResourceIdentifier");
		referenceElement.setTextContent(resultSetId);
		W3CResultSetEPR.referenceParameter(referenceElement);
		*/
		return W3CResultSetEPR.build();
	}
	
}
