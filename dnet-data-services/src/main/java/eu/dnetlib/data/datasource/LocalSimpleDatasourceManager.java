package eu.dnetlib.data.datasource;

import eu.dnetlib.enabling.datasources.common.*;

public interface LocalSimpleDatasourceManager extends LocalDatasourceManager<Datasource<Organization<?>, Identity>, Api<ApiParam>> {
}
