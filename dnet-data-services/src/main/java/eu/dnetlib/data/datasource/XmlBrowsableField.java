package eu.dnetlib.data.datasource;

import eu.dnetlib.enabling.datasources.common.BrowsableField;

public class XmlBrowsableField extends BrowsableField {

    private String xpath;
    private String vocabulary;

    public XmlBrowsableField() {
        super();
    }

    public XmlBrowsableField(final String id, final String label) {
        super(id, label);
    }

    public XmlBrowsableField(final String id, final String label, final String xpath, final String vocabulary) {
        super(id, label);
        this.xpath = xpath;
        this.vocabulary = vocabulary;
    }

    public String getXpath() {
        return xpath;
    }

    public void setXpath(final String xpath) {
        this.xpath = xpath;
    }

    public String getVocabulary() {
        return vocabulary;
    }

    public void setVocabulary(final String vocabulary) {
        this.vocabulary = vocabulary;
    }

}