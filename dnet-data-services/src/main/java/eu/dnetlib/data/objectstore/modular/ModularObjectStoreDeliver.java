package eu.dnetlib.data.objectstore.modular;

import javax.annotation.Resource;
import javax.xml.ws.wsaddressing.W3CEndpointReference;

import eu.dnetlib.data.objectstore.modular.connector.ObjectStoreDao;
import eu.dnetlib.data.objectstore.rmi.ObjectStoreFile;
import eu.dnetlib.data.objectstore.rmi.ObjectStoreServiceException;
import eu.dnetlib.enabling.resultset.ResultSetFactory;
import eu.dnetlib.enabling.resultset.client.ResultSetClientFactory;
import org.springframework.beans.factory.annotation.Required;

/**
 * The Class ModularObjectStoreDeliver is responsible of delivering data from the object Store.
 */
public class ModularObjectStoreDeliver {

	/** The dao. */
	private ObjectStoreDao dao;

	/** The result set factory. */
	@Resource
	private ResultSetFactory resultSetFactory;

	/** The result set client factory. */
	private ResultSetClientFactory resultSetClientFactory;

	/**
	 * Gets the dao.
	 *
	 * @return the dao
	 */
	public ObjectStoreDao getDao() {
		return dao;
	}

	/**
	 * Sets the dao.
	 *
	 * @param dao
	 *            the new dao
	 */
	@Required
	public void setDao(final ObjectStoreDao dao) {
		this.dao = dao;
	}

	/**
	 * Deliver.
	 *
	 * @param objectStoreID
	 *            the object store id
	 * @param from
	 *            the from
	 * @param until
	 *            the until
	 * @return the w3 c endpoint reference
	 * @throws ObjectStoreServiceException
	 */
	public W3CEndpointReference deliver(final String objectStoreID, final Long from, final Long until) throws ObjectStoreServiceException {
		return resultSetFactory.createResultSet(dao.getObjectStore(objectStoreID).deliver(from, until));
	}

	/**
	 * Deliver ids.
	 *
	 * @param objectStoreID
	 *            the object store id
	 * @param eprId
	 *            the epr id
	 * @return the w3 c endpoint reference
	 * @throws ObjectStoreServiceException
	 */
	public W3CEndpointReference deliverIds(final String objectStoreID, final W3CEndpointReference eprId) throws ObjectStoreServiceException {

		Iterable<String> ids = resultSetClientFactory.getClient(eprId);
		return resultSetFactory.createResultSet(dao.getObjectStore(objectStoreID).deliverIds(ids));
	}

	/**
	 * Exist id starts with.
	 *
	 * @param obsId
	 *            the obs id
	 * @param startId
	 *            the start id
	 * @return true, if successful
	 * @throws ObjectStoreServiceException
	 */
	public boolean existIDStartsWith(final String obsId, final String startId) throws ObjectStoreServiceException {
		return dao.getObjectStore(obsId).existIDStartsWith(startId);
	}

	/**
	 * Deliver object.
	 *
	 * @param objectStoreID
	 *            the object store id
	 * @param objectId
	 *            the object id
	 * @return the object store file
	 * @throws ObjectStoreServiceException
	 */
	public ObjectStoreFile deliverObject(final String objectStoreID, final String objectId) throws ObjectStoreServiceException {
		return dao.getObjectStore(objectStoreID).deliverObject(objectId);
	}

	/**
	 * Gets the result set client factory.
	 *
	 * @return the result set client factory
	 */
	public ResultSetClientFactory getResultSetClientFactory() {
		return resultSetClientFactory;
	}

	/**
	 * Sets the result set client factory.
	 *
	 * @param resultSetClientFactory
	 *            the new result set client factory
	 */
	@Required
	public void setResultSetClientFactory(final ResultSetClientFactory resultSetClientFactory) {
		this.resultSetClientFactory = resultSetClientFactory;
	}

}
