package eu.dnetlib.data.objectstore.modular;

import eu.dnetlib.data.objectstore.rmi.ObjectStoreServiceException;
import eu.dnetlib.enabling.tools.blackboard.BlackboardJob;
import eu.dnetlib.enabling.tools.blackboard.BlackboardServerHandler;
import org.springframework.beans.factory.annotation.Required;

// TODO: Auto-generated Javadoc
/**
 * The Class FeedCompleteObjectAction is responsible to execute the blacboard action of type FEED OBJECT which is Metadata created for WOS case of openaire.
 */
public class FeedCompleteObjectAction extends AbstractObjectStoreAction {

	/** The store feeder. */
	private ModularObjectStoreFeeder storeFeeder;

	@Override
	protected void executeAsync(final BlackboardServerHandler handler, final BlackboardJob job) throws ObjectStoreServiceException {
		try {
			final String objStoreID = job.getParameters().get("obsID");
			final String eprRs = job.getParameters().get("epr");
			final String mime = job.getParameters().get("mime");
			int count = storeFeeder.feedMetadataObjectRecord(objStoreID, eprRs, mime);
			job.getParameters().put("total", "" + count);
			completeWithSuccess(handler, job);
		} catch (Exception e) {
			completeWithFail(handler, job, e);
		}
	}

	/**
	 * Gets the store feeder.
	 *
	 * @return the store feeder
	 */
	public ModularObjectStoreFeeder getStoreFeeder() {
		return storeFeeder;
	}

	/**
	 * Sets the store feeder.
	 *
	 * @param storeFeeder the new store feeder
	 */
	@Required
	public void setStoreFeeder(final ModularObjectStoreFeeder storeFeeder) {
		this.storeFeeder = storeFeeder;
	}

}
