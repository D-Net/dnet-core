package eu.dnetlib.data.objectstore.modular;

/**
 * The Enumeration of the blacboard action of the  ObjectStore.
 */
public enum ObjectStoreActions {
	
	/** The create. */
	CREATE,
	/**
	 * The delete.
	 */
	DELETE,
	/**
	 * The feed.
	 */
	FEED,
	/**
	 * The feedobject.
	 */
	FEEDOBJECT, DROP_CONTENT
}
