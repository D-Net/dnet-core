package eu.dnetlib.data.objectstore.modular.connector;

import java.util.List;

import eu.dnetlib.data.objectstore.rmi.ObjectStoreServiceException;

/**
 * The Interface ObjectStoreDao.
 */
public interface ObjectStoreDao {

	/**
	 * Gets an Object Store with the given identifier.
	 * <p>
	 * If an Object Store with the given identifier does not exist, a new one is created.
	 * </p>
	 *
	 * @param obsId
	 *            the object store identifier
	 * @return the object store
	 * @throws ObjectStoreServiceException
	 */
	ObjectStore getObjectStore(String obsId) throws ObjectStoreServiceException;

	/**
	 * List all the Object stores.
	 *
	 * @return the list of object store ids
	 */
	List<String> listObjectStores();

	/**
	 * Creates an Object Store with the given identifier.
	 * <p>
	 * If an Object Store with the given identifier already exists this method does nothing.
	 * </p>
	 *
	 * @param obsId            	the object store identifier
	 * @param interpretation    the interpretation of the store
	 * @param basePath 			the base path to store the object Store in case of file system implementation
	 * @return true, if successful
	 * @throws ObjectStoreServiceException
	 */
	boolean createObjectStore(String obsId, String interpretation, String basePath) throws ObjectStoreServiceException;

	/**
	 * Upddate an Object Store metadata with the given identifier.
	 * <p>
	 * If an Object Store with the given identifier does not exist, a new one is created.
	 * </p>
	 *
	 * @param obsId
	 *            the object store identifier
	 * @param interpretation
	 *            the interpretation of the store
	 * @return true, if successful
	 */
	boolean updateObjectStore(String obsId, String interpretation);

	/**
	 * Delete object store.
	 *
	 * @param obsId
	 *            the object store identifier
	 * @return true, if successful
	 * @throws ObjectStoreServiceException
	 */
	boolean deleteObjectStore(String obsId) throws ObjectStoreServiceException;

	boolean dropContent(String obsId) throws ObjectStoreServiceException;

}
