package eu.dnetlib.data.objectstore.filesystem;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import javax.annotation.Resource;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.UpdateResult;
import eu.dnetlib.data.objectstore.modular.connector.ObjectStore;
import eu.dnetlib.data.objectstore.modular.connector.ObjectStoreDao;
import eu.dnetlib.data.objectstore.rmi.ObjectStoreFileNotFoundException;
import eu.dnetlib.data.objectstore.rmi.ObjectStoreServiceException;
import eu.dnetlib.miscutils.collections.MappedCollection;
import eu.dnetlib.miscutils.functional.UnaryFunction;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.conversions.Bson;

/**
 * @author sandro
 *
 */
public class FileSystemObjectStoreDao implements ObjectStoreDao {

	public static final String INTERPRETATION_FIELD = "interpretation";
	public final static String OBJECTSTORE_METADATA_NAME_FIELD = "metadataObjectStore";
	public final static String OBJECTSTORE_ID_FIELD = "obsId";
	public final static String BASE_PATH_FIELD = "basePath";
	private static final Log log = LogFactory.getLog(FileSystemObjectStoreDao.class); // NOPMD by marko on 11/24/08 5:02 PM
	private static final String OBJECTSTORE_PROFILE_SUFFIX = "_T2JqZWN0U3RvcmVEU1Jlc291cmNlcy9PYmplY3RTdG9yZURTUmVzb3VyY2VUeXBl";
	@Resource(name="objectstoreMongoDB")
	private MongoDatabase db;

	private boolean upsert;

	private String objectStoreRESTURI;

	/**
	 * {@inheritDoc}
	 * @throws ObjectStoreServiceException
	 *
	 * @see eu.dnetlib.data.objectstore.modular.connector.ObjectStoreDao#getObjectStore(java.lang.String)
	 */
	@Override
	public ObjectStore getObjectStore(final String obsId) throws ObjectStoreServiceException {
		String currentId = obsId.substring(0, 36);
		String find_id = obsId;
		if (find_id.length() == 36) {
			find_id += OBJECTSTORE_PROFILE_SUFFIX;
		}

		MongoCollection<DBObject> metadataObjectStore = getDb().getCollection(OBJECTSTORE_METADATA_NAME_FIELD, DBObject.class);
		Bson query = Filters.eq(OBJECTSTORE_ID_FIELD, find_id);
		log.debug("QUERY :" + query.toString());
		DBObject resultQuery = metadataObjectStore.find(query).first();
		log.debug("result " + resultQuery);
		if ((resultQuery == null)) throw new ObjectStoreFileNotFoundException("the objectStore with identifier: "+obsId+" was not found");

		final String basePath = resultQuery.get(BASE_PATH_FIELD).toString();
		final String interpretation = resultQuery.get("interpretation").toString();

		if (!resultQuery.containsField(BASE_PATH_FIELD) || StringUtils.isBlank(basePath))
			throw new ObjectStoreServiceException("Can't Get Objectstore, the metadata doesn't contain info about the basepath");

		final MongoCollection<DBObject> collection = getDb().getCollection(currentId, DBObject.class);
		return new FileSystemObjectStore(currentId, interpretation, basePath, collection, objectStoreRESTURI);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see eu.dnetlib.data.objectstore.modular.connector.ObjectStoreDao#listObjectStores()
	 */
	@Override
	public List<String> listObjectStores() {
		MongoCollection<DBObject> metadata = getDb().getCollection(OBJECTSTORE_METADATA_NAME_FIELD, DBObject.class);
		return MappedCollection.listMap(metadata.find(), new UnaryFunction<String, DBObject>() {

			@Override
			public String evaluate(final DBObject object) {
				return (String) object.get(OBJECTSTORE_ID_FIELD);
			}
		});
	}

	/**
	 * {@inheritDoc}
	 *
	 * @throws ObjectStoreServiceException
	 */
	@Override
	public boolean createObjectStore(final String obsId, final String interpretation, final String basePath) throws ObjectStoreServiceException {

		log.debug(String.format("Create object Store method\n\t Id:%s  Interpretation:%s BasePath : %s", obsId, interpretation, basePath) );

		if (StringUtils.isBlank(basePath)) throw new ObjectStoreServiceException("Can't create the object store: the base path cannot be blank");
		Path path = FileSystems.getDefault().getPath(basePath);
		if (!Files.exists(path) || !Files.isDirectory(path))
			throw new ObjectStoreServiceException("Can't create the object store: base path: '" + basePath + "' doesn't exist or it is not a folder");
		try {
			String currentObsId = obsId.substring(0, 36);
			log.debug("Cleaned objectStore Id " + currentObsId);
			if (Files.exists(path.resolve(currentObsId)))
				throw new ObjectStoreServiceException("Can't create the object store: base path: '" + path.resolve(currentObsId) + "' already exists");
			Files.createDirectory(path.resolve(currentObsId));
			MongoCollection<DBObject> coll = getDb().getCollection(OBJECTSTORE_METADATA_NAME_FIELD, DBObject.class);
			final BasicDBObject obj = new BasicDBObject();
			obj.put(OBJECTSTORE_ID_FIELD, obsId);
			obj.put(INTERPRETATION_FIELD, interpretation);
			obj.put(BASE_PATH_FIELD, basePath);
			coll.insertOne(obj);
			MongoCollection<DBObject> objectStore = getDb().getCollection(currentObsId, DBObject.class);
			objectStore.createIndex(new BasicDBObject("id", 1));
			objectStore.createIndex(new BasicDBObject("timestamp", 1));
			return true;
		} catch (Throwable e) {
			throw new ObjectStoreServiceException("Can't Create the object Store id: '" + obsId, e);
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see eu.dnetlib.data.objectstore.modular.connector.ObjectStoreDao#updateObjectStore(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean updateObjectStore(final String obsId, final String interpretation) {
		MongoCollection<DBObject> coll = getDb().getCollection(OBJECTSTORE_METADATA_NAME_FIELD, DBObject.class);
		final BasicDBObject obj = new BasicDBObject();
		obj.put("$set", new BasicDBObject(INTERPRETATION_FIELD, interpretation));

		final UpdateResult updateResult = coll.updateOne(Filters.eq(OBJECTSTORE_ID_FIELD, obsId), obj);
		if (updateResult.isModifiedCountAvailable()) {
			log.debug("Matched / Modified " + updateResult.getMatchedCount() + " / " + updateResult.getModifiedCount());
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @throws ObjectStoreServiceException
	 * @see eu.dnetlib.data.objectstore.modular.connector.ObjectStoreDao#deleteObjectStore(java.lang.String)
	 */
	@Override
	public boolean deleteObjectStore(final String obsId) throws ObjectStoreServiceException {
		MongoCollection<DBObject> coll = getDb().getCollection(OBJECTSTORE_METADATA_NAME_FIELD, DBObject.class);
		Bson query = Filters.eq(OBJECTSTORE_ID_FIELD, obsId);
		DBObject resultQuery = coll.find(query).first();
		String basePath = checkAndGetFsPathField(resultQuery, obsId);
		String currentObsId = obsId.substring(0, 36);
		Path basePathFS = FileSystems.getDefault().getPath(basePath, currentObsId);
		if (!Files.exists(basePathFS))
			throw new ObjectStoreServiceException("Can't Delete ObjectStore " + obsId + ": the base path does not exist :" + basePathFS);
		try {
			FileSystemUtility.deleteFolderRecursive(basePathFS);
		} catch (IOException e) {
			throw new ObjectStoreServiceException("Can't Delete ObjectStore " + obsId, e);
		}
		coll.deleteOne(Filters.eq(OBJECTSTORE_ID_FIELD, obsId));
		getDb().getCollection(obsId).drop();
		return true;
	}

	@Override
	public boolean dropContent(final String obsId) throws ObjectStoreServiceException {
		return getObjectStore(obsId).dropContent();
	}

	private String checkAndGetFsPathField(final DBObject resultQuery, final String objectStoreID) throws ObjectStoreServiceException {
		if (resultQuery == null || !resultQuery.containsField(BASE_PATH_FIELD))
			throw new ObjectStoreServiceException("ObjectStore with identifier :" + objectStoreID + " not found or missing " + BASE_PATH_FIELD + " field");
		String pathStr = (String) resultQuery.get(BASE_PATH_FIELD);
		if (StringUtils.isBlank(pathStr))
			throw new ObjectStoreServiceException("ObjectStore with identifier :" + objectStoreID + " with blank " + BASE_PATH_FIELD);
		return pathStr;
	}


	public boolean isUpsert() {
		return upsert;
	}

	public void setUpsert(final boolean upsert) {
		this.upsert = upsert;
	}

	public String getObjectStoreRESTURI() {
		return objectStoreRESTURI;
	}

	public void setObjectStoreRESTURI(final String objectStoreRESTURI) {
		this.objectStoreRESTURI = objectStoreRESTURI;
	}

	public MongoDatabase getDb() {
		return db;
	}

	public void setDb(final MongoDatabase db) {
		this.db = db;
	}

}
