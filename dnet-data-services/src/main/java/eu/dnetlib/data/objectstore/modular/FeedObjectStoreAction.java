package eu.dnetlib.data.objectstore.modular;

import eu.dnetlib.data.objectstore.rmi.ObjectStoreServiceException;
import eu.dnetlib.data.objectstore.rmi.Protocols;
import eu.dnetlib.enabling.tools.blackboard.BlackboardJob;
import eu.dnetlib.enabling.tools.blackboard.BlackboardServerHandler;
import org.springframework.beans.factory.annotation.Required;


/**
 * The Class FeedObjectStoreAction to execute the blacboard action of type FEED.
 */
public class FeedObjectStoreAction extends AbstractObjectStoreAction {

	/** The profile creator. */
	private ObjectStoreProfileCreator profileCreator;

	/** The store feeder. */
	private ModularObjectStoreFeeder storeFeeder;

	@Override
	protected void executeAsync(final BlackboardServerHandler handler, final BlackboardJob job) throws ObjectStoreServiceException {
		try {
			final String objStoreID = job.getParameters().get("obsID");
			final String objID = job.getParameters().get("objID");
			final String URI = job.getParameters().get("URI");
			final String eprRs = job.getParameters().get("epr");
			final String protocolString = job.getParameters().get("protocol");

			final Protocols protocol;
			if (protocolString == null)
				protocol = Protocols.None;
			else
				protocol = Protocols.valueOf(job.getParameters().get("protocol"));
			final String mime = job.getParameters().get("mime");
			final String login = job.getParameters().get("login");
			final String password = job.getParameters().get("password");

			if (URI != null && URI.length() > 0) {
				storeFeeder.feedObject(objStoreID, objID, URI, protocol, login,
						password, mime);
			} else if (eprRs != null && eprRs.length() > 0) {
				storeFeeder
						.feed(objStoreID, eprRs, protocol, login, password, mime);
			}
			completeWithSuccess(handler, job);
		} catch (Exception e) {
			completeWithFail(handler, job, e);
		}
	}



	/**
	 * Gets the profile creator.
	 *
	 * @return the profile creator
	 */
	public ObjectStoreProfileCreator getProfileCreator() {
		return profileCreator;
	}

	/**
	 * Sets the profile creator.
	 *
	 * @param profileCreator the new profile creator
	 */
	@Required
	public void setProfileCreator(ObjectStoreProfileCreator profileCreator) {
		this.profileCreator = profileCreator;
	}

	/**
	 * Gets the store feeder.
	 *
	 * @return the store feeder
	 */
	public ModularObjectStoreFeeder getStoreFeeder() {
		return storeFeeder;
	}

	/**
	 * Sets the store feeder.
	 *
	 * @param storeFeeder the new store feeder
	 */
	@Required
	public void setStoreFeeder(ModularObjectStoreFeeder storeFeeder) {
		this.storeFeeder = storeFeeder;
	}

}
