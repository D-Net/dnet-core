package eu.dnetlib.data.objectstore.modular;

import java.util.Set;

/**
 * Created by Sandro La Bruzzo on 10/16/15.
 */
public class ObjectStoreIntegrityInfo {

    private Set<String> objectStoreWithoutProfile;

    private Set<String> profileWithoutObjectStore;


    public Set<String> getObjectStoreWithoutProfile() {
        return objectStoreWithoutProfile;
    }

    public void setObjectStoreWithoutProfile(Set<String> objectStoreWithoutProfile) {
        this.objectStoreWithoutProfile = objectStoreWithoutProfile;
    }

    public Set<String> getProfileWithoutObjectStore() {
        return profileWithoutObjectStore;
    }

    public void setProfileWithoutObjectStore(Set<String> profileWithoutObjectStore) {
        this.profileWithoutObjectStore = profileWithoutObjectStore;
    }
}
