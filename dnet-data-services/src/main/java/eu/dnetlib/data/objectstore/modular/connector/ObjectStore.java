package eu.dnetlib.data.objectstore.modular.connector;

import eu.dnetlib.data.objectstore.modular.ObjectStoreRecord;
import eu.dnetlib.data.objectstore.rmi.MetadataObjectRecord;
import eu.dnetlib.data.objectstore.rmi.ObjectStoreFile;
import eu.dnetlib.data.objectstore.rmi.ObjectStoreServiceException;
import eu.dnetlib.enabling.resultset.ResultSetListener;

/**
 * The Interface ObjectStore.
 */
public interface ObjectStore {

	/**
	 * Gets the id of the objectStore.
	 *
	 * @return the id
	 */
	String getId();

	/**
	 * Gets the interpretation the objectStore.
	 *
	 * @return the interpretation
	 */
	String getInterpretation();

	/**
	 * Feed record into the objectstore.
	 *
	 * @param records
	 *            the records
	 * @param incremental
	 *            the incremental
	 * @return the int
	 */
	int feed(Iterable<ObjectStoreRecord> records, boolean incremental) throws ObjectStoreServiceException;

	/**
	 * Feed metadata record into the objectStore.
	 *
	 * @param records
	 *            the records
	 * @param incremental
	 *            the incremental
	 * @return the int
	 */
	int feedMetadataRecord(Iterable<MetadataObjectRecord> records, boolean incremental) throws ObjectStoreServiceException;

	/**
	 * Feed a single object record into the objectStore.
	 *
	 * @param record
	 *            the record
	 * @return the string
	 */
	String feedObjectRecord(ObjectStoreRecord record) throws ObjectStoreServiceException;

	/**
	 * Deliver Object from the objectStore.
	 *
	 * @param from
	 *            : start date which you want to filter
	 * @param until
	 *            : end date which you want to filter
	 * @return the result set listener
	 */
	ResultSetListener deliver(Long from, Long until) throws ObjectStoreServiceException;

	/**
	 * Deliver ids.
	 *
	 * @param ids
	 *            the ids
	 * @return the result set listener
	 */
	ResultSetListener deliverIds(Iterable<String> ids) throws ObjectStoreServiceException;

	/**
	 * Deliver object.
	 *
	 * @param objectId
	 *            the object id
	 * @return the object store file
	 * @throws ObjectStoreServiceException
	 */
	ObjectStoreFile deliverObject(String objectId) throws ObjectStoreServiceException;

	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	int getSize() throws ObjectStoreServiceException;

	/**
	 * Delete object.
	 *
	 * @param objectId
	 *            the object id
	 */
	void deleteObject(String objectId) throws ObjectStoreServiceException;

	/**
	 * Gets the object.
	 *
	 * @param recordId
	 *            the record id
	 * @return the object
	 */
	String getObject(String recordId) throws ObjectStoreServiceException;

	/**
	 * Find if exist an ID startingwith the string startId.
	 *
	 * @param startId
	 *            the start id
	 * @return if exist or less an id
	 */
	boolean existIDStartsWith(String startId) throws ObjectStoreServiceException;

	/**
	 * drop the content of the ObjectStore
	 * THIS METHOD DELETE ALL THE CONTENT INSIDE THE OBJECTSTORE PAY ATTENTION WHEN YOU CALL IT
	 *
	 * @return if the content has been refreshed
	 * @throws ObjectStoreServiceException
	 */
	boolean dropContent() throws ObjectStoreServiceException;

}
