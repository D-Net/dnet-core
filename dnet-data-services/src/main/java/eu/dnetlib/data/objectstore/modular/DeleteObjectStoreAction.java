package eu.dnetlib.data.objectstore.modular;

import eu.dnetlib.data.objectstore.rmi.ObjectStoreServiceException;
import eu.dnetlib.enabling.is.registry.rmi.ISRegistryService;
import eu.dnetlib.enabling.locators.UniqueServiceLocator;
import eu.dnetlib.enabling.tools.blackboard.BlackboardJob;
import eu.dnetlib.enabling.tools.blackboard.BlackboardServerHandler;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * The Class DeleteObjectStoreAction is responsible to execute the blacboard action of type DELETE.
 */
public class DeleteObjectStoreAction extends AbstractObjectStoreAction {

	/**
	 * The profile creator.
	 */

	private ObjectStoreProfileCreator profileCreator;

	@Autowired
	private UniqueServiceLocator serviceLocator;

	@Override
	protected void executeAsync(final BlackboardServerHandler handler, final BlackboardJob job) throws ObjectStoreServiceException {
		try {
			final String objID = job.getParameters().get("obsID");
			serviceLocator.getService(ISRegistryService.class).deleteProfile(objID);
			getDao().deleteObjectStore(objID);
			completeWithSuccess(handler, job);

		} catch (Exception e) {
			completeWithFail(handler, job, e);
		}
	}

	/**
	 * Gets the profile creator.
	 *
	 * @return the profile creator
	 */
	public ObjectStoreProfileCreator getProfileCreator() {
		return profileCreator;
	}

	/**
	 * Sets the profile creator.
	 *
	 * @param profileCreator the new profile creator
	 */
	public void setProfileCreator(ObjectStoreProfileCreator profileCreator) {
		this.profileCreator = profileCreator;
	}

}
