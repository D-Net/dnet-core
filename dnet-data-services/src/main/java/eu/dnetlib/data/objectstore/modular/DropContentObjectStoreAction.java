package eu.dnetlib.data.objectstore.modular;

import eu.dnetlib.data.objectstore.rmi.ObjectStoreServiceException;
import eu.dnetlib.enabling.tools.blackboard.BlackboardJob;
import eu.dnetlib.enabling.tools.blackboard.BlackboardServerHandler;

/**
 * Created by sandro on 2/26/16.
 */
public class DropContentObjectStoreAction extends AbstractObjectStoreAction {

	@Override
	protected void executeAsync(final BlackboardServerHandler handler, final BlackboardJob job) throws ObjectStoreServiceException {
		try {
			final String objID = job.getParameters().get("obsID");

			final boolean status = getDao().dropContent(objID);
			job.getParameters().put("dropStatus", "" + status);
			completeWithSuccess(handler, job);
		} catch (Exception e) {
			completeWithFail(handler, job, e);
		}
	}

}
