package eu.dnetlib.data.objectstore.modular;

import java.io.InputStream;

import eu.dnetlib.data.objectstore.rmi.ObjectStoreFile;

/**
 * The Class ObjectStoreRecord is a serialization of the object to be ingested in the objectStore
 * metadata + inputstream of the data.
 */
public class ObjectStoreRecord {

	/** The file metadata. */
	private ObjectStoreFile fileMetadata;

	/** The input stream. */
	private InputStream inputStream;

	/**
	 * Gets the input stream.
	 *
	 * @return the input stream
	 */
	public InputStream getInputStream() {
		return inputStream;
	}

	/**
	 * Sets the input stream.
	 *
	 * @param inputStream the new input stream
	 */
	public void setInputStream(final InputStream inputStream) {
		this.inputStream = inputStream;
	}

	/**
	 * Gets the file metadata.
	 *
	 * @return the file metadata
	 */
	public ObjectStoreFile getFileMetadata() {
		return fileMetadata;
	}

	/**
	 * Sets the file metadata.
	 *
	 * @param fileMetadata the new file metadata
	 */
	public void setFileMetadata(final ObjectStoreFile fileMetadata) {
		this.fileMetadata = fileMetadata;
	}

}
