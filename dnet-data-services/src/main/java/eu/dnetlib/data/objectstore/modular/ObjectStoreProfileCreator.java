package eu.dnetlib.data.objectstore.modular;

import eu.dnetlib.enabling.is.registry.rmi.ISRegistryException;
import eu.dnetlib.enabling.is.registry.rmi.ISRegistryService;
import eu.dnetlib.enabling.locators.UniqueServiceLocator;
import eu.dnetlib.soap.EndpointReferenceBuilder;
import org.antlr.stringtemplate.StringTemplate;
import org.springframework.beans.factory.annotation.Required;

import javax.xml.ws.Endpoint;

// TODO: Auto-generated Javadoc

/**
 * The Class ObjectStoreProfileCreator is responsible of creating profile of the ObjectStore
 */
public class ObjectStoreProfileCreator {

    /**
     * service locator.
     */
    private UniqueServiceLocator serviceLocator;

    /**
     * objectstore ds template.
     */
    private StringTemplate objectstoreDsTemplate;

    /**
     * service endpoint.
     */
    private Endpoint endpoint;

    /**
     * endpoint builder.
     */
    private EndpointReferenceBuilder<Endpoint> eprBuilder;

    /**
     * Register profile.
     *
     * @param interpretation the interpretation
     * @return the string
     * @throws ISRegistryException the IS registry exception
     */
    public String registerProfile(final String interpretation)
            throws ISRegistryException {
        // XXX: mini hack
        StringTemplate template = new StringTemplate(
                objectstoreDsTemplate.getTemplate());
        template.setAttribute("serviceUri", eprBuilder.getAddress(endpoint));
        template.setAttribute("interpretation", interpretation);

        return serviceLocator.getService(ISRegistryService.class, true).registerProfile(template.toString());
    }

    public void deleteProfile(final String profileId) throws ISRegistryException {
        serviceLocator.getService(ISRegistryService.class).deleteProfile(profileId);
    }

    /**
     * Gets the endpoint.
     *
     * @return the endpoint
     */
    public Endpoint getEndpoint() {
        return endpoint;
    }

    /**
     * Sets the endpoint.
     *
     * @param endpoint the new endpoint
     */
    @Required
    public void setEndpoint(final Endpoint endpoint) {
        this.endpoint = endpoint;
    }

    /**
     * Gets the epr builder.
     *
     * @return the epr builder
     */
    public EndpointReferenceBuilder<Endpoint> getEprBuilder() {
        return eprBuilder;
    }

    /**
     * Sets the epr builder.
     *
     * @param eprBuilder the new epr builder
     */
    @Required
    public void setEprBuilder(final EndpointReferenceBuilder<Endpoint> eprBuilder) {
        this.eprBuilder = eprBuilder;
    }

    /**
     * Gets the objectstore ds template.
     *
     * @return the objectstore ds template
     */
    public StringTemplate getObjectstoreDsTemplate() {
        return objectstoreDsTemplate;
    }

    /**
     * Sets the objectstore ds template.
     *
     * @param objectstoreDsTemplate the new objectstore ds template
     */
    @Required
    public void setObjectstoreDsTemplate(final StringTemplate objectstoreDsTemplate) {
        this.objectstoreDsTemplate = objectstoreDsTemplate;
    }

    public UniqueServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    @Required
    public void setServiceLocator(final UniqueServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
