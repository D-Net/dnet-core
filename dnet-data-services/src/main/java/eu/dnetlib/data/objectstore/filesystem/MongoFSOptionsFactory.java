package eu.dnetlib.data.objectstore.filesystem;

import com.mongodb.MongoClientOptions;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.FactoryBean;

public class MongoFSOptionsFactory implements FactoryBean<MongoClientOptions> {

	private int connectionsPerHost;

	@Override
	public MongoClientOptions getObject() throws BeansException {
		return MongoClientOptions.builder().connectionsPerHost(connectionsPerHost).build();
	}

	@Override
	public Class<MongoClientOptions> getObjectType() {
		return MongoClientOptions.class;
	}

	@Override
	public boolean isSingleton() {
		return false;
	}

	public int getConnectionsPerHost() {
		return connectionsPerHost;
	}

	public void setConnectionsPerHost(final int connectionsPerHost) {
		this.connectionsPerHost = connectionsPerHost;
	}

}
