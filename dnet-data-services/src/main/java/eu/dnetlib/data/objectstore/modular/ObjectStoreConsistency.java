package eu.dnetlib.data.objectstore.modular;

import java.util.List;
import java.util.Set;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import eu.dnetlib.data.objectstore.modular.connector.ObjectStoreDao;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService;
import eu.dnetlib.enabling.locators.UniqueServiceLocator;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class ObjectStoreConsistency {

    private static final Log log = LogFactory.getLog(ObjectStoreConsistency.class);

    @Autowired
    private ObjectStoreDao objectStoreDao;

    @Autowired
    private UniqueServiceLocator serviceLocator;

    private ObjectStoreIntegrityInfo integrityInfo;


    @RequestMapping(value = "/ui/objectStore/infoConsistency.do")
    @ResponseBody
    public ObjectStoreIntegrityInfo getConsistencyInfo(final ModelMap map) {
        return getIntegrityInfo();
    }

    public void refreshConsistency() {
        try {
            Set<String> profiles = listProfileIds();
            Set<String> objectStore = listObjectStoreIds();
            Set<String> objectStoreNotInProfile = Sets.newHashSet(objectStore);
            objectStoreNotInProfile.removeAll(profiles);
            Set<String> profilesNotInObjectStore = Sets.newHashSet(profiles);
            profilesNotInObjectStore.removeAll(objectStore);
            ObjectStoreIntegrityInfo info = new ObjectStoreIntegrityInfo();
            info.setObjectStoreWithoutProfile(objectStoreNotInProfile);
            info.setProfileWithoutObjectStore(profilesNotInObjectStore);
            this.integrityInfo = info;
        } catch (ISLookUpException e) {
            log.error("Error on refreshing consistency of objectStore ", e);
        }


    }


    private Set<String> listObjectStoreIds() {
        return Sets.newHashSet(
                Lists.transform(objectStoreDao.listObjectStores(), new Function<String, String>() {
                    @Override
                    public String apply(String input) {
                        return StringUtils.substringBefore(input, "_");
                    }
                }));
    }


    private Set<String> listProfileIds() throws ISLookUpException {

        final ISLookUpService lookupService = serviceLocator.getService(ISLookUpService.class);
        final String query = "for $x in collection('/db/DRIVER/ObjectStoreDSResources/ObjectStoreDSResourceType') return $x//RESOURCE_IDENTIFIER/@value/string()";

        List<String> resultList = lookupService.quickSearchProfile(query);


        return Sets.newHashSet(Lists.transform(resultList, new Function<String, String>() {
            @Override
            public String apply(String input) {
                return StringUtils.substringBefore(input, "_");
            }
        }));

    }


    public ObjectStoreIntegrityInfo getIntegrityInfo() {
        if (integrityInfo == null) {
            refreshConsistency();
        }
        return integrityInfo;
    }

    public void setIntegrityInfo(ObjectStoreIntegrityInfo integrityInfo) {
        this.integrityInfo = integrityInfo;
    }
}
