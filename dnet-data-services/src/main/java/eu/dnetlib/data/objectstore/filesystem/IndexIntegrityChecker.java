package eu.dnetlib.data.objectstore.filesystem;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.IndexOptions;
import eu.dnetlib.data.objectstore.modular.connector.ObjectStoreDao;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static eu.dnetlib.data.objectstore.filesystem.FileSystemObjectStoreDao.OBJECTSTORE_ID_FIELD;
import static eu.dnetlib.data.objectstore.filesystem.FileSystemObjectStoreDao.OBJECTSTORE_METADATA_NAME_FIELD;

/**
 * Created by claudio on 15/09/16.
 */
public class IndexIntegrityChecker {

	private static final Log log = LogFactory.getLog(IndexIntegrityChecker.class);

	@Autowired
	private ObjectStoreDao dao;

	public void check() {
		checkIndexes();
	}

	private void checkIndexes() {
		log.info("objectStore indexes integrity start");

		final MongoDatabase db = ((FileSystemObjectStoreDao) dao).getDb();
		final IndexOptions bg = new IndexOptions().background(true);

		for (String objectStoreId : dao.listObjectStores()) {
			final String id = StringUtils.substringBefore(objectStoreId, "_");
			final MongoCollection<DBObject> objectStore = db.getCollection(id, DBObject.class);
			if (log.isDebugEnabled()) {
				log.debug(String.format("creating index (id, timestamp) on objectStore %s", id));
			}
			objectStore.createIndex(new BasicDBObject("id", 1), bg);
			objectStore.createIndex(new BasicDBObject("timestamp", 1), bg);
		}
		if (log.isDebugEnabled()) {
			log.debug(String.format("creating index (%s) on %s", OBJECTSTORE_ID_FIELD, OBJECTSTORE_METADATA_NAME_FIELD));
		}
		final MongoCollection<DBObject> metadata = db.getCollection(OBJECTSTORE_METADATA_NAME_FIELD, DBObject.class);
		metadata.createIndex(new BasicDBObject(OBJECTSTORE_ID_FIELD, 1), bg);

		log.info("objectStore indexes integrity completed");
	}

}
