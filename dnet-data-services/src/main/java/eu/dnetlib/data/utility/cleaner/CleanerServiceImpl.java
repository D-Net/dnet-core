package eu.dnetlib.data.utility.cleaner;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

import org.springframework.beans.factory.annotation.Required;

import eu.dnetlib.data.utility.cleaner.rmi.CleanerException;
import eu.dnetlib.data.utility.cleaner.rmi.CleanerService;
import eu.dnetlib.enabling.resultset.MappedResultSetFactory;
import eu.dnetlib.enabling.tools.AbstractBaseService;

public class CleanerServiceImpl extends AbstractBaseService implements CleanerService {

	private CleaningRuleFactory cleaningRuleFactory;

	private MappedResultSetFactory mappedResultSetFactory;

	@Override
	public W3CEndpointReference clean(final W3CEndpointReference epr, final String ruleId) throws CleanerException {
		if ((ruleId == null) || (ruleId.isEmpty())) { throw new CleanerException("Invalid ruleId: id is empty"); }
		if (epr == null) { throw new CleanerException("Passed epr is empty"); }

		return mappedResultSetFactory.createMappedResultSet(epr, cleaningRuleFactory.obtainCleaningRule(ruleId));
	}

	@Required
	public MappedResultSetFactory getMappedResultSetFactory() {
		return mappedResultSetFactory;
	}

	@Required
	public void setMappedResultSetFactory(final MappedResultSetFactory mappedResultSetFactory) {
		this.mappedResultSetFactory = mappedResultSetFactory;
	}

	public CleaningRuleFactory getCleaningRuleFactory() {
		return cleaningRuleFactory;
	}

	@Required
	public void setCleaningRuleFactory(final CleaningRuleFactory cleaningRuleFactory) {
		this.cleaningRuleFactory = cleaningRuleFactory;
	}

}
