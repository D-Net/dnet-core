package eu.dnetlib.data.utility.cleaner;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.base.Joiner;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import eu.dnetlib.data.utility.cleaner.rmi.CleanerException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService;

/**
 * @author michele
 * 
 *         Vocabulary rules must be declared in a CleanerDS profile, for each vocabulary must be present the relative VocabularyDS profile:
 * 
 *         <RULE xpath="..." vocabularies="VOC1" /> <RULE xpath="..." vocabularies="VOC1, VOC2, VOC3" />
 */

public class VocabularyRule extends XPATHCleaningRule {

	private Set<String> vocabularies;

	private static final Log log = LogFactory.getLog(VocabularyRule.class); // NOPMD by marko on 11/24/08 5:02 PM

	private Map<String, String> synonyms = Maps.newHashMap();
	private Set<String> validTerms = Sets.newHashSet();

	public VocabularyRule(final Set<String> vocabularies, final ISLookUpService lookup) throws CleanerException {
		this.vocabularies = vocabularies;

		loadSynonymsAndTerms(lookup);
	}

	@Override
	protected String calculateNewValue(final String oldValue) throws CleanerException {
		log.debug("calculating new value for: " + oldValue);

		if (synonyms.isEmpty()) {
			log.warn("Vocabulary terms is void, vocabularies: " + this.vocabularies);
		}

		String newValue = null;

		if (synonyms.containsKey(oldValue.toLowerCase())) {
			newValue = synonyms.get(oldValue.toLowerCase());
		}

		if (newValue == null) {
			log.debug("Synonym " + oldValue + " not found in vocabulary");
			return oldValue;
		}

		return newValue;
	}

	private void loadSynonymsAndTerms(final ISLookUpService lookup) throws CleanerException {

		for (final String vocabulary : vocabularies) {
			try {
				final String query = "for $x in collection('/db/DRIVER/VocabularyDSResources/VocabularyDSResourceType')"
						+ "//RESOURCE_PROFILE[.//VOCABULARY_NAME/@code='" + vocabulary + "']//TERM return "
						+ "( concat($x/@code,'|-:-|', $x/@code), concat($x/@english_name,'|-:-|', $x/@code), concat($x/@native_name,'|-:-|', $x/@code), "
						+ "for $y in $x//SYNONYM return concat($y/@term,'|-:-|', $x/@code) )";

				for (final String s : lookup.quickSearchProfile(query)) {
					log.debug("SYNONYM : " + s);
					final String[] arr = s.split("\\|-:-\\|");
					if (arr[0] == null || arr[0].isEmpty()) {
						continue;
					}
					synonyms.put(arr[0].toLowerCase(), arr[1]);
					validTerms.add(arr[1].toLowerCase());
				}

				log.info("VOCABULARY " + vocabulary.trim() + " - terms size " + synonyms.size());
			} catch (final Exception e) {
				throw new CleanerException("Error obtaining vocabulary " + vocabulary, e);
			}
		}

	}

	@Override
	protected Map<String, String> verifyValue(final String value) throws CleanerException {
		if (synonyms.isEmpty()) {
			log.warn("Vocabulary terms is void, vocabularies: " + this.vocabularies);
		}

		if (validTerms.contains(value.toLowerCase())) { return null; }

		final Map<String, String> error = new HashMap<String, String>();
		error.put("term", value);
		error.put("vocabularies", this.vocabularies.toString().replaceAll("\\[", "").replaceAll("\\]", ""));
		error.put("xpath", this.getXpath());
		return error;
	}

	public Map<String, String> getVocabularyTerms() {
		return synonyms;
	}

	@Override
	public String toString() {
		return "VOCABULARIES: [" + Joiner.on(", ").join(vocabularies) + "]";
	}

}
