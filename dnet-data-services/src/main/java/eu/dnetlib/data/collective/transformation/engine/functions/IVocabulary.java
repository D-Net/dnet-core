/**
 * 
 */
package eu.dnetlib.data.collective.transformation.engine.functions;

import java.util.List;

/**
 * @author jochen
 *
 */
public interface IVocabulary {

	/**
	 * return the encoding for a given list of values
	 * @param keys
	 * @return the encoding as string
	 * @throws ProcessingException
	 */
	public String encoding(List<String> keys) throws ProcessingException;
	
	/**
	 * return the encoding for a given list of values using a default pattern and applying a filter function
	 * @param aKeys
	 * @param aDefaultPattern
	 * @param aFilterFunction
	 * @return the list of encoded values
	 * @throws ProcessingException
	 */
	public List<String> encoding(List<String> aKeys, String aDefaultPattern, String aFilterFunction) throws ProcessingException;
}
