/**
 * 
 */
package eu.dnetlib.data.collective.transformation.rulelanguage.parser;

import eu.dnetlib.data.collective.transformation.rulelanguage.Rules;

/**
 * @author jochen
 *
 */
public abstract class AbstractNode extends SimpleNode {

	Rules rule;
	
	/**
	 * @param i
	 */
	public AbstractNode(int i) {
		super(i);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param p
	 * @param i
	 */
	public AbstractNode(FtScript p, int i) {
		super(p, i);
		// TODO Auto-generated constructor stub
	}
	
	public Rules getRule() {
		return rule;
	}

	public void setRule(Rules rule) {
		this.rule = rule;
	}


}
