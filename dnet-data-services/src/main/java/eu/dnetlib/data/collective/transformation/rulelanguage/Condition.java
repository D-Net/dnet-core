package eu.dnetlib.data.collective.transformation.rulelanguage;

/**
 * @author jochen
 *
 */
public class Condition {

	private String applyExpression;
	private String conditionExpression;
	private Rules primaryRule;
	private Rules secondaryRule;

	/**
	 * @param applyExpression the applyExpression to set
	 */
	public void setApplyExpression(String applyExpression) {
		this.applyExpression = applyExpression;
	}

	/**
	 * @return the applyExpression
	 */
	public String getApplyExpression() {
		return applyExpression;
	}
	
	/**
	 * @param conditionExpression the conditionExpression to set
	 */
	public void setConditionExpression(String conditionExpression) {
		this.conditionExpression = conditionExpression;
	}

	/**
	 * @return the conditionExpression
	 */
	public String getConditionExpression() {
		return conditionExpression;
	}
	
	public boolean isPrimary(Rules aRule){
		if (aRule.equals(primaryRule)) return true;
		return false;
	}

	/**
	 * @param primaryRule the primaryRule to set
	 */
	public void setPrimaryRule(Rules primaryRule) {
		this.primaryRule = primaryRule;
	}

	/**
	 * @return the primaryRule
	 */
	public Rules getPrimaryRule() {
		return primaryRule;
	}

	/**
	 * @param secondaryRule the secondaryRule to set
	 */
	public void setSecondaryRule(Rules secondaryRule) {
		this.secondaryRule = secondaryRule;
	}

	/**
	 * @return the secondaryRule
	 */
	public Rules getSecondaryRule() {
		return secondaryRule;
	}


}
