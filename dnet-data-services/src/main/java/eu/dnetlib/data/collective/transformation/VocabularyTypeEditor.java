/**
 * 
 */
package eu.dnetlib.data.collective.transformation;

import java.beans.PropertyEditorSupport;

import org.svenson.JSONParser;

/**
 * @author js
 *
 */
public class VocabularyTypeEditor extends PropertyEditorSupport {

	/**
	 * Sets the property value by parsing the given JsonString. May raise java.lang.IllegalArgumentException if either the String is badly formatted or if this kind of property can't be expressed as text.
	 * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
	 */
	@Override
	public void setAsText(String aJsonString) throws IllegalArgumentException {
		VocabularyMap map = JSONParser.defaultJSONParser().parse(VocabularyMap.class, aJsonString);
		setValue(map);
	}
}
