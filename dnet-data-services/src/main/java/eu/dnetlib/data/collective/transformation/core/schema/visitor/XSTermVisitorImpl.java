package eu.dnetlib.data.collective.transformation.core.schema.visitor;

import org.apache.commons.lang3.NotImplementedException;

import com.sun.xml.xsom.XSElementDecl;
import com.sun.xml.xsom.XSModelGroup;
import com.sun.xml.xsom.XSModelGroupDecl;
import com.sun.xml.xsom.XSParticle;
import com.sun.xml.xsom.XSWildcard;
import com.sun.xml.xsom.visitor.XSTermVisitor;

/**
 * @author jochen
 *
 */
public class XSTermVisitorImpl implements XSTermVisitor {

	private Visitor visitor;
	
	@Override
	public void elementDecl(XSElementDecl aElementDecl) {
		if (aElementDecl.isLocal()){
			  this.visitor.elementDecl(aElementDecl);
		}else{
			// ignore non local element declarations
		}
	}

	@Override
	public void modelGroup(XSModelGroup aModelGroup) {
		XSContentTypeVisitorImpl contentTypeVisitor = new XSContentTypeVisitorImpl();
		contentTypeVisitor.setVisitor(this.visitor);
		for (XSParticle p: aModelGroup.getChildren()){
			contentTypeVisitor.particle(p);
		}										
	}

	@Override
	public void modelGroupDecl(XSModelGroupDecl arg0) {
		throw new NotImplementedException("TODO modelGroupDecl");
	}

	@Override
	public void wildcard(XSWildcard arg0) {
		throw new NotImplementedException("TODO wildcard");
	}

	public void setVisitor(Visitor visitor) {
		this.visitor = visitor;
	}

	public Visitor getVisitor() {
		return visitor;
	}

}
