package eu.dnetlib.data.collective.transformation.core.xsl;

/**
 * 
 * @author js
 *
 */
public class XsltConstants {

	public static final String applyTemplates = "xsl:apply-templates";
	public static final String attribute    = "xsl:attribute";
	public static final String callTemplate = "xsl:call-template";
	public static final String choose       = "xsl:choose";
	public static final String copy         = "xsl:copy";
	public static final String copyOf       = "xsl:copy-of";
	public static final String element      = "xsl:element";
	public static final String forEach 	    = "xsl:for-each";
	public static final String ifCondition  = "xsl:if";
	public static final String message		= "xsl:message";
	public static final String otherwise    = "xsl:otherwise";
	public static final String param        = "xsl:param";
	public static final String template	    = "xsl:template";
	public static final String text         = "xsl:text";
	public static final String valueOf 	    = "xsl:value-of";
	public static final String variable	    = "xsl:variable";
	public static final String when         = "xsl:when";
	public static final String withParam    = "xsl:with-param";
	
	public static final String nsXsl	    = "xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"";
	public static final String extFuncNS    = "TransformationFunction";

}
