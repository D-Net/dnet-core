package eu.dnetlib.data.collective.transformation.rulelanguage.visitor;

import eu.dnetlib.data.collective.transformation.rulelanguage.parser.ASTMyAssign;
import eu.dnetlib.data.collective.transformation.rulelanguage.parser.ASTMyAttribute;
import eu.dnetlib.data.collective.transformation.rulelanguage.parser.ASTMyOp;
import eu.dnetlib.data.collective.transformation.rulelanguage.parser.ASTStart;
import eu.dnetlib.data.collective.transformation.rulelanguage.parser.FtScriptVisitor;
import eu.dnetlib.data.collective.transformation.rulelanguage.parser.SimpleNode;

public abstract class AbstractVisitor implements FtScriptVisitor {

	/* (non-Javadoc)
	 * @see examples.jjtree.FtScriptVisitor#visit(examples.jjtree.ASTMyAssign, java.lang.Object)
	 */
	@Override
	public Object visit(ASTMyAssign node, Object data) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see examples.jjtree.FtScriptVisitor#visit(examples.jjtree.ASTMyAttribute, java.lang.Object)
	 */
	@Override
	public Object visit(ASTMyAttribute node, Object data) {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see examples.jjtree.FtScriptVisitor#visit(examples.jjtree.ASTMyOp, java.lang.Object)
	 */
	@Override
	public Object visit(ASTMyOp node, Object data) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see examples.jjtree.FtScriptVisitor#visit(examples.jjtree.ASTStart, java.lang.Object)
	 */
	@Override
	public Object visit(ASTStart node, Object data) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see examples.jjtree.FtScriptVisitor#visit(examples.jjtree.SimpleNode, java.lang.Object)
	 */
	@Override
	public Object visit(SimpleNode node, Object data) {
		// TODO Auto-generated method stub
		return null;
	}

}
