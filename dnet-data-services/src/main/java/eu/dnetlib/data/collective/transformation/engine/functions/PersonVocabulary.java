/**
 * 
 */
package eu.dnetlib.data.collective.transformation.engine.functions;

import java.util.List;

import prototype.Person;

/**
 * @author jochen
 *
 */
public class PersonVocabulary extends Vocabulary{

	@Override
	public String encoding(List<String> keys)throws ProcessingException{
		Person p;
		String result = "";
		for (String input: keys){
			p = new Person(input);
			result = p.getNormalisedFullname();
		}
		return result;
	}
}
