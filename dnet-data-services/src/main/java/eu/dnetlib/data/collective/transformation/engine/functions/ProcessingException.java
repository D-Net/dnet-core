/**
 * 
 */
package eu.dnetlib.data.collective.transformation.engine.functions;

/**
 * @author jochen
 *
 */
public class ProcessingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8648116731979859467L;

	/**
	 * 
	 */
	public ProcessingException() {
		super();
	}

	/**
	 * @param arg0
	 */
	public ProcessingException(String arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 */
	public ProcessingException(Throwable arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public ProcessingException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
