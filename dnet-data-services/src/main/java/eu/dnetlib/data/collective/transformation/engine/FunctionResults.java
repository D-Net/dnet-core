package eu.dnetlib.data.collective.transformation.engine;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author js
 *
 */
public class FunctionResults {

	private Map<String, List<String>> resultMap = new LinkedHashMap<String, List<String>>();
	
	
	/**
	 * get the first single result from the result list at the given index
	 * @param aIndex
	 * @return a result
	 */
	public String get(int aIndex){
		return resultMap.get(aIndex + "").get(0);
	}
	
	/**
	 * get the single result for the node at the given position from the list at the given index
	 * @param aIndex
	 * @param aPosition
	 * @return a result
	 */
	public String get(int aIndex, int aPosition){
		if (aPosition <= 0){
			throw new IllegalArgumentException("position is " + aPosition + ", must be greater 0");
		}
		return resultMap.get(aIndex + "").get(aPosition - 1);
	}
	
	/**
	 * add a collection containing the results for each record
	 * @param aCollection
	 */
	public void addAll(Collection<? extends String> aCollection){
		for (String result : aCollection){
			add(result);
		}
	}
	
	/**
	 * add a single result calculated for a record node
	 * @param aResult
	 */
	public void add(String aResult){
		List<String> resultList = new LinkedList<String>();
		resultList.add(aResult);
		resultMap.put(resultMap.size() + "", resultList);
	}
	
	/**
	 * add a list of results calculated for all resp. record nodes
	 * @param aResults
	 */
	public void add(List<String> aResults){
		resultMap.put(resultMap.size() + "", aResults);
	}
}
