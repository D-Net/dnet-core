package eu.dnetlib.data.collective.transformation.rulelanguage.util;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringEscapeUtils;

import eu.dnetlib.data.collective.transformation.core.xsl.XsltConstants;
import eu.dnetlib.data.collective.transformation.engine.functions.Convert;
import eu.dnetlib.data.collective.transformation.engine.functions.IdentifierExtract;
import eu.dnetlib.data.collective.transformation.engine.functions.Lookup;
import eu.dnetlib.data.collective.transformation.engine.functions.RegularExpression;
import eu.dnetlib.data.collective.transformation.engine.functions.Split;
import eu.dnetlib.data.collective.transformation.rulelanguage.Argument;

/**
 * TODO: make this class abstract and function classes (getValue, regexpr, ...) extending this class
 * @author jochen
 *
 */
public class FunctionCall {

	private String externalFunctionName;
	private Map<String, String> paramMap;
	private List<String> paramList;
	private String uuid;
	private List<Argument> argList = new LinkedList<Argument>();
	private boolean isStatic = false;
	private boolean doPreprocess = true;
	
	public FunctionCall(boolean aIsStatic){
		uuid = UUID.randomUUID().toString();
		this.isStatic = aIsStatic;
	}
	
	public FunctionCall(boolean aIsStatic, boolean aDoPreprocess){
		this(aIsStatic);
		this.doPreprocess = aDoPreprocess;
	}
	
	public boolean doPreprocess(){
		return this.doPreprocess;
	}
	
	public String getXSLpreparatedFunctionCall(){
		 return XsltConstants.extFuncNS + ":" + externalFunctionName + "(" + "$tf" + ", '" + uuid + "', $index" + ")";
	}
	
	public String getXSLpositionFunctionCall(){
		 return XsltConstants.extFuncNS + ":" + externalFunctionName + "(" + "$tf" + ", '" + uuid + "', $index" + ", $posVar" + ")";
	}
	
    public String getXSLdirectFunctionCall(String aCallId){
    	if (externalFunctionName.equals("regExpr")){
        	return XsltConstants.extFuncNS + ":" + externalFunctionName + "(" + "$tf" + ", " + this.paramMap.get(RegularExpression.paramExpr1) + ", " + this.paramMap.get(RegularExpression.paramExpr2) + ", '" + this.paramMap.get(RegularExpression.paramRegularExpr) + "')";
    	}else if (externalFunctionName.equals("convert")){
    		if (this.paramMap.containsKey(Convert.paramDefaultPattern) && this.paramMap.containsKey(Convert.paramFunction))
    			return XsltConstants.extFuncNS + ":" + externalFunctionName + "(" + "$tf" + ", " + this.paramMap.get(Convert.paramFieldValue) + ", '" + this.paramMap.get(Convert.paramVocabularyName) + "', '" + this.paramMap.get(Convert.paramDefaultPattern) + "', '" + this.paramMap.get(Convert.paramFunction) + "')";
   			else
   				return XsltConstants.extFuncNS + ":" + externalFunctionName + "(" + "$tf" + ", " + this.paramMap.get(Convert.paramFieldValue) + ", '" + this.paramMap.get(Convert.paramVocabularyName) + "')";
    	}else if (externalFunctionName.equals("convertString")){
    		if (this.paramMap.containsKey(Convert.paramDefaultPattern) && this.paramMap.containsKey(Convert.paramFunction))
    			return XsltConstants.extFuncNS + ":" + externalFunctionName + "(" + "$tf" + ", " + this.paramMap.get(Convert.paramFieldValue) + ", '" + this.paramMap.get(Convert.paramVocabularyName) + "', '" + this.paramMap.get(Convert.paramDefaultPattern) + "', '" + this.paramMap.get(Convert.paramFunction) + "')";
   			else
   				return XsltConstants.extFuncNS + ":" + externalFunctionName + "(" + "$tf" + ", " + this.paramMap.get(Convert.paramFieldValue) + ", '" + this.paramMap.get(Convert.paramVocabularyName) + "')";
    	}else if (externalFunctionName.equals("split")){
    		return XsltConstants.extFuncNS + ":" + externalFunctionName + "(" + "$tf" + ", " + this.paramMap.get(Split.paramInputExpr) + ", '" + this.paramMap.get(Split.paramRegExpr) + "', '" + aCallId + "')";
    	}else if (externalFunctionName.equals("lookup")){
    		return XsltConstants.extFuncNS + ":" + externalFunctionName + "(" + "$tf" + ", " + this.paramMap.get(Lookup.paramExprIdentifier) + ", '" + this.paramMap.get(Lookup.paramExprProperty) + "')";
    	}else if (externalFunctionName.equals("identifierExtract")){
    		return XsltConstants.extFuncNS + ":" + externalFunctionName + "(" + "$tf" + ", '" + StringEscapeUtils.escapeXml10(this.paramMap.get(IdentifierExtract.paramXpathExprJson)) + "', " + this.paramMap.get(IdentifierExtract.paramXpathExprInSource) + ", '" + StringEscapeUtils.escapeXml10(this.paramMap.get(IdentifierExtract.paramRegExpr)) + "')";
    	}else{
    		throw new IllegalStateException("unsupported function call: " + externalFunctionName);
    	}
    }	
    
    public String getXSLdirectFunctionCallById(String aCallId){
    	if (externalFunctionName.equals("split")){
    		return XsltConstants.extFuncNS + ":" + externalFunctionName + "(" + "$tf" + ", '" + aCallId + "')";
    	}else{
    		throw new IllegalStateException("unsupported function call: " + externalFunctionName);    		
    	}   	
    }

	public void setExternalFunctionName(String externalFunctionName) {
		this.externalFunctionName = externalFunctionName;
	}

	public String getExternalFunctionName() {
		return externalFunctionName;
	}
	
	public void addArgument(Argument arg){
		this.argList.add(arg);
	}

	public void setArguments(List<Argument> aArgList){
		this.argList = aArgList;
	}
	
	public List<Argument> getArguments(){
		return this.argList;
	}
	
	public void setParameters(Map<String, String> parameters) {
		this.paramMap = parameters;
	}

	public Map<String, String> getParameters() {
		return paramMap;
	}

	public String getUuid() {
		return uuid;
	}

	/**
	 * @param isStatic the isStatic to set
	 */
	public void setStatic(boolean isStatic) {
		this.isStatic = isStatic;
	}

	/**
	 * @return the isStatic
	 */
	public boolean isStatic() {
		return isStatic;
	}

	/**
	 * @return the paramList
	 */
	public List<String> getParamList() {
		return paramList;
	}

	/**
	 * @param paramList the paramList to set
	 */
	public void setParamList(List<String> paramList) {
		this.paramList = paramList;
	}
}
