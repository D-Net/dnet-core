package eu.dnetlib.data.collective.transformation.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

public class BlacklistConsumer {

	public List<String> getBlackList(String apiURL){
		List<String> blacklist = new LinkedList<String>();
		try{
			URL blacklistApi = new URL(apiURL);
			InputStream in = blacklistApi.openStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			String line;
			while((line = reader.readLine()) != null) {
			    blacklist.add(line);
			}
			System.out.println(blacklist.size());
			System.out.println(blacklist.get(0));			
		}catch(IOException e){
			throw new IllegalStateException("error in blacklist api: " + e.getMessage());
		}
		return blacklist;
	}
}
