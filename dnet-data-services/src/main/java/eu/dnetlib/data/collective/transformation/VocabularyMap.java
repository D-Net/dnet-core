package eu.dnetlib.data.collective.transformation;

import java.util.Map;

import org.svenson.JSONProperty;
import org.svenson.JSONTypeHint;

import eu.dnetlib.data.collective.transformation.engine.functions.Vocabulary;

public class VocabularyMap {

	private Map<String, Vocabulary> map;
	
	/**
	 * Returns true if the vocabulary map contains the key argument. Method implemented for backward compatibility.
	 * @param aKey vocabulary name as a key
	 * @return true if key exist else false
	 */
	public boolean containsKey(String aKey){
		return map.containsKey(aKey);
	}

	/**
	 * @return the map
	 */
	@JSONProperty(ignoreIfNull = true)
	public Map<String, Vocabulary> getMap() {
		return map;
	}

	/**
	 * @param map the map to set
	 */
	@JSONTypeHint(Vocabulary.class)
	public void setMap(Map<String, Vocabulary> map) {
		this.map = map;
	}
}
