package eu.dnetlib.data.collective.transformation.rulelanguage;

/**
 * @author jochen
 *
 */
public interface IRule {

	public String getUniqueName();
	
	public boolean hasCondition();
	
	/**
	 * returns true when the rule has pending rules that set an element
	 * @return true if it has a ruleSet, false otherwise
	 */
	public boolean hasSet();
	
	public boolean definesVariable();
		
	public boolean definesTargetField();
	
	public boolean definesTemplate();
	
	public boolean definesTemplateMatch();

}
