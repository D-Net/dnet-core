package eu.dnetlib.data.collective.transformation;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.DocumentException;

import eu.dnetlib.data.collective.transformation.engine.functions.DateVocabulary;
import eu.dnetlib.data.collective.transformation.engine.functions.IVocabulary;
import eu.dnetlib.data.collective.transformation.engine.functions.PersonVocabulary;
// import eu.dnetlib.data.collective.transformation.engine.functions.PmcVocabulary;
import eu.dnetlib.data.collective.transformation.engine.functions.Vocabulary;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpDocumentNotFoundException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpException;
import eu.dnetlib.enabling.locators.DefaultUniqueServiceLocator;

/**
 * @author jochen
 * 
 */
public class VocabularyRegistry {

	private static final Log log = LogFactory.getLog(VocabularyRegistry.class);
	private static final String dateVocabularyName = "DateISO8601";
	// private static final String pmcVocabularyName = "PMC";
	private static final String personVocabularyName = "Person";

	@Resource
	private DefaultUniqueServiceLocator uniqueServiceLocator;
	private VocabularyMap vocabularies;
	private boolean isInitialized = false;

	public void init() {
		String vocabularyQueryPrefix = "collection('/db/DRIVER/VocabularyDSResources/VocabularyDSResourceType')//RESOURCE_PROFILE";
		String targetVocabulary = "";
		for (String key : vocabularies.getMap().keySet()) {
			try {
				Vocabulary v = vocabularies.getMap().get(key);
				targetVocabulary = vocabularies.getMap().get(key).getName();
				v.setResource(new eu.dnetlib.common.profile.Resource(uniqueServiceLocator.getIsLookupService().getResourceProfileByQuery(
						vocabularyQueryPrefix + "[.//VOCABULARY_NAME='" + targetVocabulary + "' or .//VOCABULARY_NAME/@code='" + targetVocabulary + "'] ")));
			} catch (ISLookUpDocumentNotFoundException e) {
				throw new IllegalStateException("vocabulary profile not found for name or code " + targetVocabulary, e);
			} catch (ISLookUpException e) {
				log.fatal("ISLookupException in VocabularyRegistry, key = " + key + " : ", e);
				throw new IllegalStateException(e);
			} catch (DocumentException e) {
				log.fatal("DocumentException in VocabularyRegistry, key = " + key + " : ", e);
				throw new IllegalStateException(e);
			}
		}
		vocabularies.getMap().put(dateVocabularyName, new DateVocabulary());
		vocabularies.getMap().put(personVocabularyName, new PersonVocabulary());
		// PmcVocabulary pmcVocab = new PmcVocabulary();
		// pmcVocab.setMappingFile(mappingFile);
		// vocabularies.getMap().put(pmcVocabularyName, pmcVocab);
		isInitialized = true;
		log.info("VocabularyRegistry is initialized.");
	}

	public IVocabulary getVocabulary(final String aVocabularyName) {
		if (!isInitialized) {
			init();
		}
		return vocabularies.getMap().get(aVocabularyName);
	}

	public VocabularyMap getVocabularies() {
		if (!isInitialized) {
			init();
		}
		return vocabularies;
	}

	public void setVocabularies(final VocabularyMap vocabularies) {
		this.vocabularies = vocabularies;
	}

	public void addVocabulary(final String aVocabularyName, final Vocabulary aVocabulary) {
		this.vocabularies.getMap().put(aVocabularyName, aVocabulary);
	}

	public void removeVocabulary(final String aVocabulary) {
		this.vocabularies.getMap().remove(aVocabulary);
	}

	public DefaultUniqueServiceLocator getUniqueServiceLocator() {
		return uniqueServiceLocator;
	}

	public void setUniqueServiceLocator(final DefaultUniqueServiceLocator uniqueServiceLocator) {
		this.uniqueServiceLocator = uniqueServiceLocator;
	}

}
