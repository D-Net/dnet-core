/**
 * 
 */
package eu.dnetlib.data.collective.transformation;

import java.util.List;

/**
 * @author jochen
 *
 */
public interface IDatabaseConnector {

	/**
	 * executes a SQL query 
	 * @param aSQLquery
	 * @return List containing the results of this query execution
	 */
	List<String> getResult(String aSQLquery) throws TransformationException;
}
