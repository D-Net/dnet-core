package eu.dnetlib.data.collective.transformation.core.xsl;

import java.util.LinkedList;
import java.util.List;

/**
 * @author jochen
 *
 */
public abstract class AbstractXslElement {

	private String functionName;
	protected List<String> attrList = new LinkedList<String>();
	protected StringBuilder enclosedElements = new StringBuilder();
	protected List<String> nsList = new LinkedList<String>();


	public AbstractXslElement(String aFunctioName) {
		this.functionName = aFunctioName;
	}
	
	public String asXml(boolean isEmpty){
		StringBuilder builder = new StringBuilder();
		builder.append("<");
		builder.append(functionName + " ");
		for (String ns: nsList){
			builder.append(ns + " ");			
		}
		
		for (String attr: attrList){
			builder.append(attr);
		}
		if (isEmpty){
			builder.append("/>");
		}else{
			builder.append(">");
			builder.append(enclosedElements.toString());
			builder.append("</");
			builder.append(functionName + ">");			
		}
		return builder.toString();		
	}
	
	public String asXml() {
		return asXml(false);
	}

}
