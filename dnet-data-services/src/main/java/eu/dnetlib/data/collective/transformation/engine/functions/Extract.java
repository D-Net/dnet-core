/**
 * 
 */
package eu.dnetlib.data.collective.transformation.engine.functions;

import java.util.List;

import eu.dnetlib.data.collective.transformation.TransformationException;

/**
 * @author jochen
 *
 */
public class Extract extends AbstractTransformationFunction {

	public static final String paramNameFeature = "feature";
	private IFeatureExtraction featureExtraction;
	
	/* (non-Javadoc)
	 * @see eu.dnetlib.data.collective.transformation.engine.functions.AbstractTransformationFunction#execute()
	 */
	@Override
	String execute() throws ProcessingException {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<String> execute(List<String> aObjectRecords, String aFeature) throws ProcessingException{
		try {
			return featureExtraction.execute(aObjectRecords, aFeature);
		} catch (TransformationException e) {
			throw new ProcessingException(e);
		}
	}

	/**
	 * @param featureExtraction the featureExtraction to set
	 */
	public void setFeatureExtraction(IFeatureExtraction featureExtraction) {
		this.featureExtraction = featureExtraction;
	}

	/**
	 * @return the featureExtraction
	 */
	public IFeatureExtraction getFeatureExtraction() {
		return featureExtraction;
	}
	
}
