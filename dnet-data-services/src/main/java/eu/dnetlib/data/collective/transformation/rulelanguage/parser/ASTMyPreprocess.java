/* Generated By:JJTree: Do not edit this line. ASTMyPreprocess.java Version 4.3 */
/* JavaCCOptions:MULTI=true,NODE_USES_PARSER=false,VISITOR=true,TRACK_TOKENS=false,NODE_PREFIX=AST,NODE_EXTENDS=,NODE_FACTORY=,SUPPORT_CLASS_VISIBILITY_PUBLIC=true */
package eu.dnetlib.data.collective.transformation.rulelanguage.parser;

public class ASTMyPreprocess extends SimpleNode {
	private String id;
	private String funcName;
	private String parameter;
	
  public ASTMyPreprocess(int id) {
    super(id);
  }

  public ASTMyPreprocess(FtScript p, int id) {
    super(p, id);
  }

  /** Accept the visitor. **/
  public Object jjtAccept(FtScriptVisitor visitor, Object data) {
    return visitor.visit(this, data);
  }

  public String getFunctionName(){
	  return this.funcName;
  }
  
  public String getParameter(){
	  return this.parameter;
  }
  
  public String getId(){
	  return id;
  }

  public void preprocess(String aPreprocessId, String aFunction, String aParameter) {
	this.id = aPreprocessId;
	this.funcName = aFunction;
	// unquote
	if (aParameter.length() > 0){
		this.parameter = aParameter.substring(1, aParameter.length() - 1);
	}
  }

  public void preprocess(String aFunction, String aParameter){
	  this.funcName = aFunction;
		// unquote
		if (aParameter.length() > 0){
			this.parameter = aParameter.substring(1, aParameter.length() - 1);
		}
  }
  
  public void preprocess(String aFunction) {
	  this.funcName = aFunction;
  }
}
/* JavaCC - OriginalChecksum=b9229360af18a53de1ce87664846e442 (do not edit this line) */
