package eu.dnetlib.data.collective.transformation.rulelanguage;

/**
 * @author jochen
 *
 */
public class Argument {

	public enum Type {VALUE, INPUTFIELD, JOBCONST, VAR};
	
	private Type type;
	private String argument;
	
	public Argument(Type aType, String aArgument) {
		this.type = aType;
		this.argument = aArgument;
	}
	
	public boolean isValue(){
		return this.type.equals(Type.VALUE);
	}
	
	public boolean isInputField(){
		return this.type.equals(Type.INPUTFIELD);
	}
	
	public boolean isJobConst(){
		return this.type.equals(Type.JOBCONST);
	}
	
	public boolean isVariable(){
		return this.type.equals(Type.VAR);
	}
	
	public String getArgument(){
		return this.argument;
	}
}
