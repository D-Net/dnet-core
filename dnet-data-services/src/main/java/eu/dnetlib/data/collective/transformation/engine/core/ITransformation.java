package eu.dnetlib.data.collective.transformation.engine.core;

import java.util.Map;
import java.util.Properties;

import net.sf.saxon.instruct.TerminationException;
import eu.dnetlib.data.collective.transformation.TransformationException;
import eu.dnetlib.data.collective.transformation.rulelanguage.RuleLanguageParser;

/**
 * @author jochen
 *
 */
public interface ITransformation {

	public static final String JOBCONST_DATASINKID = "$job.datasinkid";
	public static final String XSLSyntaxcheckfailed = "syntaxcheckfailed.xsl";

	/**
	 * transforms a single record
	 * 
	 * @param aRecord the record to transform
	 * @param aIndex
	 * @return the transformed record
	 * @throws TerminationException, TransformationServiceException
	 */
	public String transformRecord(String aRecord, int aIndex) throws TerminationException, TransformationException;
	
	/**
	 * transforms a single record whyle applying a stylesheet
	 * @param aRecord
	 * @param aStylesheet
	 * @return
	 * @throws TransformationException
	 */
	public String transformRecord(String aRecord, String aStylesheet) throws TransformationException;
	
	public String transformRecord(String aRecord, Map<String, String> aStylesheetParams) throws TerminationException, TransformationException;
	
	/**
	 * @return the rule language parser
	 */
	public RuleLanguageParser getRuleLanguageParser();
	
	public Map<String, String> getStaticTransformationResults();
	
	public Map<String, String> getJobProperties();

	/**
	 * get log information that was recorded during transformation
	 * @return properties
	 */
	public Properties getLogInformation();
}
