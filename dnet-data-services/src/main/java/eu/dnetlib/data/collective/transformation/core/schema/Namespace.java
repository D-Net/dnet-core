/**
 * 
 */
package eu.dnetlib.data.collective.transformation.core.schema;

/**
 * @author jochen
 *
 */
public class Namespace {

	String prefix;
	String uri;
	
	public Namespace(String aPrefix, String aUri) {
		this.prefix = aPrefix;
		this.uri = aUri;
	}

	/**
	 * @return the prefix
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * @param prefix the prefix to set
	 */
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	/**
	 * @return the uri
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * @param uri the uri to set
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}
}
