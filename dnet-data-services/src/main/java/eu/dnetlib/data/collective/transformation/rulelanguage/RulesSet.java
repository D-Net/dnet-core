/**
 * 
 */
package eu.dnetlib.data.collective.transformation.rulelanguage;

import java.util.LinkedList;
import java.util.List;

/**
 * @author jochen
 *
 */
public class RulesSet {

	private List<Rules> pendingRules = new LinkedList<Rules>();
	/**
	 * @return the pendingRules
	 */
	public List<Rules> getPendingRules() {
		return pendingRules;
	}
	/**
	 * @param pendingRules the pendingRules to set
	 */
	public void setPendingRules(List<Rules> pendingRules) {
		this.pendingRules = pendingRules;
	}
		
}
