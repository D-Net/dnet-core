package eu.dnetlib.data.collective.transformation.core.xsl;

import java.util.Collection;


/**
 * @author jochen
 *
 */
public class XslElement extends AbstractXslElement {

	public XslElement(String aFunctionName) {
		super(aFunctionName);
	}
	
	public XslElement(String aFunctionName, String aAttrName, String aAttrValue) {
		super(aFunctionName);
		addAttribute(aAttrName, aAttrValue);
	}
	
	public void addAttribute(String aName, String aValue){
		this.attrList.add(aName + "=\"" + aValue + "\" ");
	}
	
	public void addEnclosedElements(String aElements){
		this.enclosedElements.append(aElements);
	}
	
	public void addBoundPrefix(String aNamespace){
		this.nsList.add(aNamespace);
	}
	
	public void addAllBoundPrefixes(Collection<String> aNamespaceList){
		this.nsList.addAll(aNamespaceList);
	}
	
	/**
	 * @return the isEmpty
	 */
	public boolean isEmpty() {
		return !(enclosedElements.length() > 0);
	}
}
