/**
 * 
 */
package eu.dnetlib.data.collective.transformation.core.schema;

/**
 * @author jochen
 *
 */
public class SchemaAttribute {

	private String name;
	private boolean required;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the required
	 */
	public boolean isRequired() {
		return required;
	}

	/**
	 * @param required the required to set
	 */
	public void setRequired(boolean required) {
		this.required = required;
	}
}
