/**
 * 
 */
package eu.dnetlib.data.collective.transformation.engine.functions;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Properties;

/**
 * @author jochen
 *
 */
public class LookupRecord {

	private HashMap<String, Properties> recordMap = new LinkedHashMap<String, Properties>();
	
	public void setRecord(String aRecordKey, String aPropertyKey, String aPropertyValue){
		if (recordMap.containsKey(aRecordKey)){
			recordMap.get(aRecordKey).setProperty(aPropertyKey, aPropertyValue);
		}else{
			Properties p = new Properties();
			p.setProperty(aPropertyKey, aPropertyValue);
			recordMap.put(aRecordKey, p);
		}
	}
	
	public String getPropertyValue(String aRecordKey, String aPropertyKey){
		if (!recordMap.containsKey(aRecordKey))	return "UNKNOWN";
		return recordMap.get(aRecordKey).getProperty(aPropertyKey, "UNKNOWN");
	}
	
}
