package eu.dnetlib.data.collective.transformation.engine.functions;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import eu.dnetlib.data.collective.transformation.VocabularyRegistry;

/**
 * @author jochen
 *
 */
public class Convert extends AbstractTransformationFunction {
	
	public static final String paramVocabularyName = "vocabularyName";
	public static final String paramFieldValue = "fieldValue";
	public static final String paramDefaultPattern = "defaultPattern";
	public static final String paramFunction = "function";
	
	@Resource
	private VocabularyRegistry vocabularyRegistry;
	

	/**
	 * not implemented
	 * @see eu.dnetlib.data.collective.transformation.engine.functions.AbstractTransformationFunction#execute()
	 */
	public String execute() throws ProcessingException {
		return null;
	}
	
	/**
	 * extracts and returns the encoded value as used in the vocabulary
	 * @param vocabularyName the name of the vocabulary to be used
	 * @param fieldValues the list of values to normalize
	 * @return encoded value
	 * @throws ProcessingException
	 */
	public String executeSingleValue(String vocabularyName, List<String> fieldValues)throws ProcessingException{
		if (!vocabularyRegistry.getVocabularies().containsKey(vocabularyName)){
			throw new ProcessingException("unknown vocabulary: " + vocabularyName);
		}
		String returnValue = vocabularyRegistry.getVocabulary(vocabularyName).encoding(fieldValues);
		return returnValue;
	}
	
	public List<String> executeAllValues(String vocabularyName, List<String> fieldValues) throws ProcessingException{
		if (!vocabularyRegistry.getVocabularies().containsKey(vocabularyName)){
			throw new ProcessingException("unknown vocabulary: " + vocabularyName);
		}
		List<String> computedValues = new LinkedList<String>();
		int numOfComputedValues = fieldValues.size();
		if (numOfComputedValues == 0) numOfComputedValues = 1; // return at least 1 value
		String returnValue = vocabularyRegistry.getVocabulary(vocabularyName).encoding(fieldValues);
		for (int i = 0; i < numOfComputedValues; i++){
			computedValues.add(returnValue);
		}
		return computedValues;
	}
	
	public List<String> executeFilterByParams(String vocabName, List<String> fieldValues, String defaultPattern, String filterFunction) throws ProcessingException{
		return vocabularyRegistry.getVocabulary(vocabName).encoding(fieldValues, defaultPattern, filterFunction);
	}
	
	public VocabularyRegistry getVocabularyRegistry() {
		return vocabularyRegistry;
	}

	public void setVocabularyRegistry(VocabularyRegistry vocabularyRegistry) {
		this.vocabularyRegistry = vocabularyRegistry;
	}

}
