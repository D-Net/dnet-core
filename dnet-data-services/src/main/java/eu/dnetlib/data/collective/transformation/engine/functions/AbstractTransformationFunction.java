package eu.dnetlib.data.collective.transformation.engine.functions;

import java.util.List;

public abstract class AbstractTransformationFunction implements
		ITransformationFunction {

	List<String> objectRecords;
	List<String> resultRecords;
	
	abstract String execute() throws ProcessingException;
}
