package eu.dnetlib.data.collective.transformation.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;

public class NamespaceContextImpl implements NamespaceContext {

	private Map<String, String> nsMap = new HashMap<String, String>();
	
	public void addNamespace(String aPrefix, String aURI){
		nsMap.put(aPrefix, aURI);
	}
	
	@Override
	public String getNamespaceURI(String aPrefix) {
		return nsMap.get(aPrefix);
	}

	@Override
	public String getPrefix(String aNamespaceURI) {
		if (aNamespaceURI == null){
			throw new IllegalStateException();
		}
		if (aNamespaceURI.equals(XMLConstants.XML_NS_URI)){
			return XMLConstants.XML_NS_PREFIX;
		}else if (aNamespaceURI.equals(XMLConstants.XMLNS_ATTRIBUTE_NS_URI)){
			return XMLConstants.XMLNS_ATTRIBUTE;
		}else if (nsMap.values().contains(aNamespaceURI)){
			for (String prefix: nsMap.keySet()){
				if (nsMap.get(prefix).equals(aNamespaceURI)){
					return prefix;
				}
			}
		}
		return null;
	}

	@Override
	public Iterator getPrefixes(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}
