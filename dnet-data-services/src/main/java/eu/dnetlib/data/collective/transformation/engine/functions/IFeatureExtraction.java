/**
 * 
 */
package eu.dnetlib.data.collective.transformation.engine.functions;

import java.util.List;

import eu.dnetlib.data.collective.transformation.TransformationException;


/**
 * @author jochen
 *
 */
public interface IFeatureExtraction {

	/**
	 * applies the extraction of a feature on objectRecords
	 * @param aObjectRecords
	 * @param aFeatureName
	 * @return list of extracted results
	 * @throws TransformationServiceException
	 */
	public List<String> execute(List<String> aObjectRecords, String aFeatureName) throws TransformationException;
}
