package eu.dnetlib.data.collective.transformation.core.schema;

import java.util.LinkedList;
import java.util.List;

/**
 * @author jochen
 *
 */
public class SchemaElement {

	private String targetNamespace;
	private String name;
	private boolean isRepeatable;
	private boolean isRoot;
	private boolean containsSimpleType;
	private int minOccurs;
	private int maxOccurs;
	private List<SchemaElement> childList = new LinkedList<SchemaElement>();
	private List<SchemaAttribute> attributeList = new LinkedList<SchemaAttribute>();
	private Namespace namespace;

	/**
	 * @return the targetNamespace
	 */
	public String getTargetNamespace() {
		return targetNamespace;
	}
	/**
	 * @param targetNamespace the targetNamespace to set
	 */
	public void setTargetNamespace(String targetNamespace) {
		this.targetNamespace = targetNamespace;
	}
	/**
	 * @return the isRepeatable
	 */
	public boolean isRepeatable() {
		return isRepeatable;
	}
	/**
	 * @param isRepeatable the isRepeatable to set
	 */
	public void setRepeatable(boolean isRepeatable) {
		this.isRepeatable = isRepeatable;
	}
	/**
	 * @return the isMandatory
	 */
	public boolean isMandatory() {
		if (minOccurs > 0) return true;
		return false;
	}
	
	/**
	 * @return the minOccurs
	 */
	public int getMinOccurs() {
		return minOccurs;
	}
	/**
	 * @param minOccurs the minOccurs to set
	 */
	public void setMinOccurs(int minOccurs) {
		this.minOccurs = minOccurs;
	}
	/**
	 * @return the maxOccurs
	 */
	public int getMaxOccurs() {
		return maxOccurs;
	}
	/**
	 * @param maxOccurs the maxOccurs to set
	 */
	public void setMaxOccurs(int maxOccurs) {
		this.maxOccurs = maxOccurs;
	}
	/**
	 * @return the childList
	 */
	public List<SchemaElement> getChildList() {
		return childList;
	}
	/**
	 * @param childList the childList to set
	 */
	public void setChildList(List<SchemaElement> childList) {
		this.childList = childList;
	}
	
	/**
	 * @param name the name of the element to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the name of this element
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * sets true if this element contains a simpleType, false else
	 * @param containsSimpleType
	 */
	public void setContainsSimpleType(boolean containsSimpleType) {
		this.containsSimpleType = containsSimpleType;
	}
	
	/**
	 * @return the containsSimpleType
	 */
	public boolean containsSimpleType() {
		return containsSimpleType;
	}
	
	/**
	 * @param isRoot the isRoot to set
	 */
	public void setRoot(boolean isRoot) {
		this.isRoot = isRoot;
	}
	
	/**
	 * @return the isRoot
	 */
	public boolean isRoot() {
		return isRoot;
	}
	/**
	 * @param namespace the namespace to set
	 */
	public void setNamespace(Namespace namespace) {
		this.namespace = namespace;
	}
	/**
	 * @return the namespace
	 */
	public Namespace getNamespace() {
		return namespace;
	}
	/**
	 * @return the attributeList
	 */
	public List<SchemaAttribute> getAttributeList() {
		return attributeList;
	}
	/**
	 * @param attributeList the attributeList to set
	 */
	public void addAttribute(SchemaAttribute aAttribute) {
		this.attributeList.add(aAttribute);
	}
	
	
}
