/**
 * 
 */
package eu.dnetlib.data.collective.transformation;

/**
 * @author jochen
 *
 */
public class TransformationException extends Exception {

	/**
	 * 
	 */
	public TransformationException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public TransformationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public TransformationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public TransformationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
