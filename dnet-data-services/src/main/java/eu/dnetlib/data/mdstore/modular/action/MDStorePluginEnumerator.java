package eu.dnetlib.data.mdstore.modular.action;

import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.ListableBeanFactory;

public class MDStorePluginEnumerator implements BeanFactoryAware {

	// private static final Log log = LogFactory.getLog(MDStorePluginEnumerator.class); // NOPMD by marko on 11/24/08 5:02 PM

	/**
	 * bean factory.
	 */
	private ListableBeanFactory beanFactory;

	@Override
	public void setBeanFactory(final BeanFactory beanFactory) throws BeansException {
		this.beanFactory = (ListableBeanFactory) beanFactory;
	}

	/**
	 * Get all beans implementing the MDStorePlugin interface.
	 *
	 * @return
	 */
	public Map<String, MDStorePlugin> getAll() {
		return beanFactory.getBeansOfType(MDStorePlugin.class);
	}

}
