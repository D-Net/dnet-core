package eu.dnetlib.data.mdstore.modular.connector;

import java.util.List;

import eu.dnetlib.data.mdstore.MDStoreServiceException;
import eu.dnetlib.data.mdstore.modular.MDStoreDescription;

public interface MDStoreDao {

	MDStore getMDStore(final String mdId) throws MDStoreServiceException;

	MDStore readMDStore(final String mdId) throws MDStoreServiceException;

	MDStore startTransaction(final String mdId, final boolean refresh) throws MDStoreServiceException;

	void commit(final String transactionId, final String mdId) throws MDStoreServiceException;

	List<MDStoreDescription> listMDStores() throws MDStoreServiceException;

	List<String> listMDStores(final String format, final String layout, final String interpretation) throws MDStoreServiceException;

	void createMDStore(final String mdId, final String format, final String interpretation, final String layout) throws MDStoreServiceException;

	void deleteMDStore(final String id) throws MDStoreServiceException;

	int getCachedSize(final String id) throws MDStoreServiceException;

	void refreshSizes() throws MDStoreServiceException;

	int refreshSize(final String id) throws MDStoreServiceException;

	int getSumOfSizes(final String format, final String layout, final String interpretation) throws MDStoreServiceException;

	void startGarbage() throws MDStoreServiceException;

	void invalidTransaction(final String transactionId, final String mdId) throws MDStoreServiceException;

	MDStoreDBStatus getDBStatus();
}
