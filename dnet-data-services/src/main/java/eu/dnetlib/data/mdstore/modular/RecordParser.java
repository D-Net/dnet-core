package eu.dnetlib.data.mdstore.modular;

import java.util.Map;

import eu.dnetlib.miscutils.datetime.DateUtils;

/**
 * Parses a mdrecord and extracts the minimum information (like id, date etc) which is necessary for the mdstoring
 * process.
 * 
 * @author marko
 * 
 */
public interface RecordParser {

	Map<String, String> parseRecord(String record);

	void setTimestamp(long ts);

	default long getTimestamp() {
		return DateUtils.now();
	}

}
