package eu.dnetlib.data.mdstore.modular.connector;

import java.util.ArrayList;
import java.util.List;

public class MDStoreManagerInfo {

	private String mdId;
	private String currentId;
	private List<MDStoreExpiredInfo> stillUsed;
	private List<MDStoreTransactionInfo> transactions;

	public String getMdId() {
		return mdId;
	}

	public void setMdId(final String mdId) {
		this.mdId = mdId;
	}

	public String getCurrentId() {
		return currentId;
	}

	public void setCurrentId(final String currentId) {
		this.currentId = currentId;
	}

	public List<MDStoreExpiredInfo> getStillUsed() {
		return stillUsed;
	}

	public void setStillUsed(final List<MDStoreExpiredInfo> stillUsed) {
		this.stillUsed = stillUsed;
	}

	public List<MDStoreTransactionInfo> getTransactions() {
		return transactions;
	}

	public void setTransactions(final List<MDStoreTransactionInfo> transactions) {
		this.transactions = transactions;
	}

	public void addExpiredItem(final MDStoreExpiredInfo info) {
		if (stillUsed == null) {
			stillUsed = new ArrayList<MDStoreExpiredInfo>();
		}
		stillUsed.add(info);
	}

	public void addTransactionInfo(final MDStoreTransactionInfo info) {
		if (transactions == null) {
			transactions = new ArrayList<MDStoreTransactionInfo>();
		}
		transactions.add(info);
	}

}
