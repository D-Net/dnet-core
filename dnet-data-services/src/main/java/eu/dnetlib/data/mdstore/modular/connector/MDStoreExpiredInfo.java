package eu.dnetlib.data.mdstore.modular.connector;

import java.util.Date;

public class MDStoreExpiredInfo {

	private String id;
	private Date lastRead;

	/**
	 * @return the lastRead
	 */
	public Date getLastRead() {
		return lastRead;
	}

	/**
	 * @param lastRead
	 *            the lastRead to set
	 */
	public void setLastRead(final Date lastRead) {
		this.lastRead = lastRead;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(final String id) {
		this.id = id;
	}
}
