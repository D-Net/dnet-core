package eu.dnetlib.data.mdstore.modular;

import org.springframework.beans.factory.annotation.Required;

import eu.dnetlib.miscutils.factory.Factory;

public class RecordParserFactory implements Factory<RecordParser> {
	
	private Class<? extends RecordParser> parserType;

	@Override
	public RecordParser newInstance() {
		try {
			return getParserType().newInstance();
		} catch (Throwable e) {
			throw new RuntimeException(e);
		} 	
	}

	@Required
	public void setParserType(Class<? extends RecordParser> parserType) {
		this.parserType = parserType;
	}

	public Class<? extends RecordParser> getParserType() {
		return parserType;
	}


}
