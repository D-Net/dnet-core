package eu.dnetlib.data.mdstore.modular.action;

import java.util.Map;

import eu.dnetlib.data.mdstore.MDStoreServiceException;
import eu.dnetlib.data.mdstore.modular.connector.MDStoreDao;

public interface MDStorePlugin {

	public void run(MDStoreDao dao, Map<String, String> params, DoneCallback doneCallback) throws MDStoreServiceException;

}
