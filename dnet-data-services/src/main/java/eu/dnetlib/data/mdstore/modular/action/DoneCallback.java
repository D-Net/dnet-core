package eu.dnetlib.data.mdstore.modular.action;

import java.util.Map;

public interface DoneCallback {

	void call(Map<String, String> params);
}
