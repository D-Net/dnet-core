package eu.dnetlib.data.mdstore.modular.connector;

import eu.dnetlib.data.mdstore.MDStoreServiceException;

public interface MDStoreTransactionManager {

	/**
	 * Verify consistency.
	 *
	 * @throws MDStoreServiceException
	 *             the MD store service exception
	 */
	public abstract void verifyConsistency() throws MDStoreServiceException;

	/**
	 * Creates the md store.
	 *
	 * @param mdId
	 *            the md id
	 * @throws MDStoreServiceException
	 *             the MD store service exception
	 */
	public abstract void createMDStore(String mdId) throws MDStoreServiceException;

	/**
	 * Drop md store.
	 *
	 * @param mdId
	 *            the md id
	 * @throws MDStoreServiceException
	 *             the MD store service exception
	 */
	public abstract void dropMDStore(String mdId) throws MDStoreServiceException;

	/**
	 * Gets the MD store collection.
	 *
	 * @param mdId
	 *            the md id
	 * @return the MD store collection
	 * @throws MDStoreServiceException
	 *             the MD store service exception
	 */
	public abstract String getMDStoreCollection(String mdId) throws MDStoreServiceException;

	/**
	 * Start a new transaction for writing in the mdstore this will create a temporary mdstore in which save the content. and after finished
	 * switch it to the existing one
	 *
	 * @param mdId
	 * @param refresh
	 * @throws MDStoreServiceException
	 *             if the mdStore Id doesn't exists in the metadata
	 */
	public abstract String startTransaction(String mdId, boolean refresh) throws MDStoreServiceException;

	/**
	 * Commit the transaction.
	 *
	 * @param transactionId
	 *            the transaction id
	 * @param mdstoreId
	 *            the mdstore id
	 * @param currentMDStore
	 *            the current md store
	 * @return true, if successful
	 * @throws MDStoreServiceException
	 *             the MD store service exception
	 */
	public abstract boolean commit(String transactionId, String mdstoreId, MDStore currentMDStore) throws MDStoreServiceException;

	/**
	 * Book a a current mdstore, so the transaction manager can't delete during a possible commit, until the resultset associate has not
	 * expired
	 *
	 * @param mdStoreId
	 * @return
	 * @throws MDStoreServiceException
	 */
	public abstract String readMdStore(String mdStoreId) throws MDStoreServiceException;

	/**
	 * Return a JSON about the transaction assigned to a particular mdstore
	 *
	 * @param mdStoreId
	 * @return
	 * @throws MDStoreServiceException
	 */
	public abstract MDStoreManagerInfo getInfoForCurrentMdStore(String mdStoreId) throws MDStoreServiceException;

	/**
	 * Manually drop an old collection assigned to a particular mdStore
	 *
	 * @param mdId
	 *            : the id of the mdStore
	 * @param idToDrop
	 *            : The id of the collection to be dropped
	 * @return
	 * @throws MDStoreServiceException
	 */
	public abstract Boolean dropUsed(String mdId, String idToDrop) throws MDStoreServiceException;

	/**
	 * Manually drop an old collection assigned to a particular mdStore
	 *
	 * @param mdId
	 *            : the id of the mdStore
	 * @param idToDrop
	 *            : The id of the collection to be dropped
	 * @return
	 * @throws MDStoreServiceException
	 */
	public abstract Boolean dropTransaction(String mdId, String idToDrop) throws MDStoreServiceException;

	/**
	 * Start the garbage collection of the old mdstore not used
	 *
	 * @throws MDStoreServiceException
	 */
	public abstract void garbage() throws MDStoreServiceException;

}
