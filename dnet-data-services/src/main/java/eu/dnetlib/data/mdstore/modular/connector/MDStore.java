package eu.dnetlib.data.mdstore.modular.connector;

import java.util.List;
import java.util.Map;

import eu.dnetlib.data.mdstore.DocumentNotFoundException;
import eu.dnetlib.data.mdstore.MDStoreServiceException;
import eu.dnetlib.data.mdstore.modular.MDFormatDescription;
import eu.dnetlib.enabling.resultset.ResultSetListener;

public interface MDStore {

	String getId();

	String getFormat();

	String getInterpretation();

	String getLayout();

	void truncate();

	int feed(Iterable<String> records, boolean incremental);

    int feed(Iterable<String> records, boolean incremental, List<MDFormatDescription> mdformats);

	ResultSetListener deliver(String from, String until, String recordFilter) throws MDStoreServiceException;

	ResultSetListener deliverIds(String from, String until, String recordFilter) throws MDStoreServiceException;

	Iterable<String> iterate();

	int getSize();

	void deleteRecord(String recordId);

	String getRecord(String recordId) throws DocumentNotFoundException;

    List<String> deliver(String mdId, int pageSize, int offset, Map<String, String> queryParam);
}
