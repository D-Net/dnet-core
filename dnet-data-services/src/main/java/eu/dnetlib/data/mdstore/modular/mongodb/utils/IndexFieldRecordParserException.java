package eu.dnetlib.data.mdstore.modular.mongodb.utils;

/**
 * Created by Alessia Bardi on 14/06/2017.
 *
 * @author Alessia Bardi
 */
public class IndexFieldRecordParserException extends Exception {

	public IndexFieldRecordParserException() {
		super();
	}

	public IndexFieldRecordParserException(final String message) {
		super(message);
	}

	public IndexFieldRecordParserException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public IndexFieldRecordParserException(final Throwable cause) {
		super(cause);
	}
}
