package eu.dnetlib.data.mdstore.modular.action;

import eu.dnetlib.data.mdstore.MDStoreServiceException;
import eu.dnetlib.data.mdstore.modular.MDStoreProfileCreator;
import eu.dnetlib.enabling.is.registry.rmi.ISRegistryException;
import eu.dnetlib.enabling.tools.blackboard.BlackboardJob;
import eu.dnetlib.enabling.tools.blackboard.BlackboardServerHandler;
import org.springframework.beans.factory.annotation.Required;

public class CreateAction extends AbstractMDStoreAction {

	private MDStoreProfileCreator profileCreator;

	@Override
	public void executeAsync(final BlackboardServerHandler handler, final BlackboardJob job) throws MDStoreServiceException {
		final String format = job.getParameters().get("format");
		final String interpretation = job.getParameters().get("interpretation");
		final String layout = job.getParameters().get("layout");

		try {
			String mdId = profileCreator.registerProfile(format, interpretation, layout);

			getDao().createMDStore(mdId, format, interpretation, layout);

			job.getParameters().put("id", mdId);

			completeWithSuccess(handler, job);
		} catch (ISRegistryException e) {
			throw new MDStoreServiceException(e);
		}
	}

	public MDStoreProfileCreator getProfileCreator() {
		return profileCreator;
	}

	@Required
	public void setProfileCreator(final MDStoreProfileCreator profileCreator) {
		this.profileCreator = profileCreator;
	}

}
