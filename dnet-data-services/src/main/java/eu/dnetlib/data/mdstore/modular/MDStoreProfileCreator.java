package eu.dnetlib.data.mdstore.modular;

import javax.xml.ws.Endpoint;

import org.antlr.stringtemplate.StringTemplate;
import org.springframework.beans.factory.annotation.Required;

import eu.dnetlib.enabling.is.registry.rmi.ISRegistryException;
import eu.dnetlib.enabling.is.registry.rmi.ISRegistryService;
import eu.dnetlib.enabling.locators.UniqueServiceLocator;
import eu.dnetlib.soap.EndpointReferenceBuilder;

public class MDStoreProfileCreator {
	/**
	 * service locator.
	 */
	private UniqueServiceLocator serviceLocator;

	/**
	 * mdstore ds template.
	 */
	private StringTemplate mdstoreDsTemplate;

	/**
	 * service endpoint.
	 */
	private Endpoint endpoint;

	/**
	 * endpoint builder.
	 */
	private EndpointReferenceBuilder<Endpoint> eprBuilder;

	public String registerProfile(String format, String interpretation, String layout) throws ISRegistryException {
		// XXX: mini hack
		StringTemplate template = new StringTemplate(mdstoreDsTemplate.getTemplate());
		template.setAttribute("serviceUri", eprBuilder.getAddress(endpoint));
		template.setAttribute("format", format);
		template.setAttribute("interpretation", interpretation);
		template.setAttribute("layout", layout);

		return serviceLocator.getService(ISRegistryService.class).registerProfile(template.toString());
	}

	public StringTemplate getMdstoreDsTemplate() {
		return mdstoreDsTemplate;
	}

	@Required
	public void setMdstoreDsTemplate(StringTemplate mdstoreDsTemplate) {
		this.mdstoreDsTemplate = mdstoreDsTemplate;
	}

	public Endpoint getEndpoint() {
		return endpoint;
	}

	@Required
	public void setEndpoint(Endpoint endpoint) {
		this.endpoint = endpoint;
	}

	public EndpointReferenceBuilder<Endpoint> getEprBuilder() {
		return eprBuilder;
	}

	@Required
	public void setEprBuilder(EndpointReferenceBuilder<Endpoint> eprBuilder) {
		this.eprBuilder = eprBuilder;
	}

	public UniqueServiceLocator getServiceLocator() {
		return serviceLocator;
	}

	@Required
	public void setServiceLocator(UniqueServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}
}
