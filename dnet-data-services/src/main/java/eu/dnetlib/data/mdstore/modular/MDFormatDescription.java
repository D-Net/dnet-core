package eu.dnetlib.data.mdstore.modular;

/**
 * Created by sandro on 11/29/16.
 */
public class MDFormatDescription {

    private String name;

    private String xpath;

    public MDFormatDescription() {

    }

    public MDFormatDescription(String name, String xpath) {
        this.name = name;
        this.xpath = xpath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getXpath() {
        return xpath;
    }

    public void setXpath(String xpath) {
        this.xpath = xpath;
    }
}
