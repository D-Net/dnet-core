package eu.dnetlib.data.mdstore.modular.mongodb.utils;

import java.util.Comparator;
import java.util.Date;

import com.google.common.base.Function;
import com.mongodb.DBObject;

import eu.dnetlib.miscutils.functional.UnaryFunction;

public class MDStoreUtils {

	public static UnaryFunction<String, DBObject> mdId() {
		return arg -> (String) arg.get("mdId");
	}

	public static UnaryFunction<Boolean, DBObject> dboFilter(final String format, final String layout, final String interpretation) {
		return dbo -> dbo.get("format").toString().equals(format) && dbo.get("layout").toString().equals(layout)
				&& dbo.get("interpretation").toString().equals(interpretation);
	}

	public static Function<DBObject, String> body() {
		return dbo -> (String) dbo.get("body");
	}

	public static Comparator<DBObject> getComparatorOnDate() {
		return (o1, o2) -> {
			Date d1 = (Date) o1.get("date");
			Date d2 = (Date) o2.get("date");
			return d1.compareTo(d2);
		};
	}
}
