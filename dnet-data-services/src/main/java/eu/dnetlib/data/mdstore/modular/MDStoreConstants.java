package eu.dnetlib.data.mdstore.modular;

public class MDStoreConstants {

	public static final String ID =  "id";
	public static final String OBJIDENTIFIER =  "objIdentifier";
	public static final String TIMESTAMP = "timestamp";
	public static final String ORIGINALID = "originalId";
	public static final String BODY = "body";


}
