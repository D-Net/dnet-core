package eu.dnetlib.data.mdstore.modular.action;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.dnetlib.data.mdstore.modular.connector.MDStoreDao;

public class DummyPlugin implements MDStorePlugin {

	private static final Log log = LogFactory.getLog(DummyPlugin.class);

	@Override
	public void run(final MDStoreDao dao, final Map<String, String> params, final DoneCallback done) {
		log.info("Nothing to do, I'm a dummy plugin. Bye.");
		HashMap<String, String> res = new HashMap<String, String>();
		res.put("ret", "cheers!");
		done.call(res);
	}

}
