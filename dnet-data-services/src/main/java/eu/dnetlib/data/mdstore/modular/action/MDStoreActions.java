/**
 *
 */
package eu.dnetlib.data.mdstore.modular.action;

public enum MDStoreActions {
	CREATE,
	DELETE,
	FEED,
	RUN_PLUGIN
}
