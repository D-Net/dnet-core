package eu.dnetlib.data.mdstore.modular.connector;

public class MDStoreResults {
	private Iterable<String> records;
	private int size;

	public MDStoreResults(Iterable<String> records, int size) {
		super();
		this.records = records;
		this.size = size;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public Iterable<String> getRecords() {
		return records;
	}

	public void setRecords(Iterable<String> records) {
		this.records = records;
	}
}
