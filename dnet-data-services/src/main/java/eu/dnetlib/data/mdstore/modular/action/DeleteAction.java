package eu.dnetlib.data.mdstore.modular.action;

import eu.dnetlib.data.mdstore.MDStoreServiceException;
import eu.dnetlib.enabling.is.registry.rmi.ISRegistryService;
import eu.dnetlib.enabling.locators.UniqueServiceLocator;
import eu.dnetlib.enabling.tools.blackboard.BlackboardJob;
import eu.dnetlib.enabling.tools.blackboard.BlackboardServerHandler;
import org.springframework.beans.factory.annotation.Required;

public class DeleteAction extends AbstractMDStoreAction {

	private UniqueServiceLocator serviceLocator;

	@Override
	public void executeAsync(final BlackboardServerHandler handler, final BlackboardJob job) throws MDStoreServiceException {
		final String currentId = job.getParameters().get("id");
		try {
			serviceLocator.getService(ISRegistryService.class).deleteProfile(currentId);
			getDao().deleteMDStore(currentId);
			completeWithSuccess(handler, job);
		} catch (Exception e) {
			throw new MDStoreServiceException("Error deleting mdstore with id " + currentId, e);
		}
	}

	public UniqueServiceLocator getServiceLocator() {
		return serviceLocator;
	}

	@Required
	public void setServiceLocator(final UniqueServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

}
