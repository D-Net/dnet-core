package eu.dnetlib.data.mdstore.modular.connector;

public class MDStoreDBStatus {

	private int handledDatastructures;
	private int usedDiskSpace;
	private String date;

	public MDStoreDBStatus() {}

	public MDStoreDBStatus(final int handledDatastructures, final int usedDiskSpace, final String date) {
		this.handledDatastructures = handledDatastructures;
		this.usedDiskSpace = usedDiskSpace;
		this.date = date;
	}

	public int getHandledDatastructures() {
		return handledDatastructures;
	}

	public void setHandledDatastructures(final int handledDatastructures) {
		this.handledDatastructures = handledDatastructures;
	}

	public int getUsedDiskSpace() {
		return usedDiskSpace;
	}

	public void setUsedDiskSpace(final int usedDiskSpace) {
		this.usedDiskSpace = usedDiskSpace;
	}

	public String getDate() {
		return date;
	}

	public void setDate(final String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "MDStoreDBStatus{" +
				"handledDatastructures=" + handledDatastructures +
				", usedDiskSpace=" + usedDiskSpace +
				", date='" + date + '\'' +
				'}';
	}
}
