package eu.dnetlib.data.mdstore.modular;


public class MDStoreDescription {

	private String id;

	private String format;

	private String layout;

	private String interpretation;

	private int size;

	private boolean indexed;

	public MDStoreDescription(final String id, final String format, final String layout, final String interpretation, final int size, final boolean indexed) {
		super();
		this.id = id;
		this.format = format;
		this.layout = layout;
		this.interpretation = interpretation;
		this.size = size;
		this.indexed = indexed;
	}

    public MDStoreDescription() {

    }

    public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(final String format) {
		this.format = format;
	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(final String layout) {
		this.layout = layout;
	}

	public String getInterpretation() {
		return interpretation;
	}

	public void setInterpretation(final String interpretation) {
		this.interpretation = interpretation;
	}

	public int getSize() {
		return size;
	}

	public void setSize(final int size) {
		this.size = size;
	}

	public boolean isIndexed() {
		return indexed;
	}

	public void setIndexed(final boolean indexed) {
		this.indexed = indexed;
	}

}
