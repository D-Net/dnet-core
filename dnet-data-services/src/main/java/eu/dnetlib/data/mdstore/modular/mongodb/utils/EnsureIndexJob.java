package eu.dnetlib.data.mdstore.modular.mongodb.utils;

import eu.dnetlib.data.mdstore.modular.MDStoreDescription;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Required;

import eu.dnetlib.data.mdstore.MDStoreServiceException;
import eu.dnetlib.data.mdstore.modular.connector.MDStoreDao;
import eu.dnetlib.data.mdstore.modular.mongodb.MongoMDStore;
import eu.dnetlib.enabling.tools.AbstractSchedulable;

public class EnsureIndexJob extends AbstractSchedulable {

	private static final Log log = LogFactory.getLog(EnsureIndexJob.class); // NOPMD by marko on 11/24/08 5:02 PM

	private MDStoreDao dao;

	@Override
	protected void doExecute() {
		log.info("performing mdstore index check");

		try {
			for (MDStoreDescription mdstore : getDao().listMDStores()) {
				try {
					log.info("ensureindex for mdStoreId:" + mdstore.getId());
					((MongoMDStore) getDao().getMDStore(mdstore.getId())).ensureIndices();
				} catch (Throwable e) {
					log.warn("unable to reindex mdstore: " + mdstore.getId(), e);
				}
			}
		} catch (MDStoreServiceException e) {
			log.warn("unable to reindex mdstore ", e);
		}

		log.info("mdstore index check completed");
	}

	@Required
	public void setDao(final MDStoreDao dao) {
		this.dao = dao;
	}

	public MDStoreDao getDao() {
		return dao;
	}

}
