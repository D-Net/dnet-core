package eu.dnetlib.data.mdstore.modular.action;

public interface FailedCallback {

	void call(Throwable e);
}
