package eu.dnetlib.data.collector.plugins.sftp;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by andrea on 11/01/16.
 */
public class SftpIteratorFactory {

    public Iterator<String> newIterator(final String baseUrl,
                                        final String username,
                                        final String password,
                                        final boolean isRecursive,
            final Set<String> extensionsSet, final String fromDate) {
        return new SftpIterator(baseUrl, username, password, isRecursive, extensionsSet, fromDate);
    }
}
