package eu.dnetlib.data.collector.plugins.httpfilename;

import eu.dnetlib.data.collector.rmi.CollectorServiceException;

/**
 * Created by miriam on 07/05/2018.
 */
public interface ConnectorInterface {

    void get(final String requestUrl) throws CollectorServiceException;

    String getResponse();

    boolean isStatusOk();


    boolean responseTypeContains(String string);

}
