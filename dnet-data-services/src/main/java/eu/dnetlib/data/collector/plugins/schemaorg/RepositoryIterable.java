package eu.dnetlib.data.collector.plugins.schemaorg;

import java.util.Iterator;

public interface RepositoryIterable extends Iterable<String> {
	public static String TerminationHint = "df667391-676d-4c0f-9c40-426b1001607a";
}
