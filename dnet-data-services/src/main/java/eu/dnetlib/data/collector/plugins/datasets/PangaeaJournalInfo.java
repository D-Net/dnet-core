package eu.dnetlib.data.collector.plugins.datasets;

/**
 * The Class PangaeaJorunalInfo.
 */
public class PangaeaJournalInfo {

	/** The journal name. */
	private String journalName;

	/** The journal id. */
	private String journalId;

	/** The datasource id. */
	private String datasourceId;

	/** The journal issn. */
	private String journalISSN;

	/**
	 * Gets the journal name.
	 *
	 * @return the journal name
	 */
	public String getJournalName() {
		return journalName;
	}

	/**
	 * Sets the journal name.
	 *
	 * @param journalName
	 *            the new journal name
	 */
	public void setJournalName(final String journalName) {
		this.journalName = journalName;
	}

	/**
	 * Gets the journal id.
	 *
	 * @return the journal id
	 */
	public String getJournalId() {
		return journalId;
	}

	/**
	 * Sets the journal id.
	 *
	 * @param journalId
	 *            the new journal id
	 */
	public void setJournalId(final String journalId) {
		this.journalId = journalId;
	}

	/**
	 * Gets the datasource id.
	 *
	 * @return the datasource id
	 */
	public String getDatasourceId() {
		return datasourceId;
	}

	/**
	 * Sets the datasource id.
	 *
	 * @param datasourceId
	 *            the new datasource id
	 */
	public void setDatasourceId(final String datasourceId) {
		this.datasourceId = datasourceId;
	}

	/**
	 * @return the journalISSN
	 */
	public String getJournalISSN() {
		return journalISSN;
	}

	/**
	 * @param journalISSN
	 *            the journalISSN to set
	 */
	public void setJournalISSN(final String journalISSN) {
		this.journalISSN = journalISSN;
	}

}
