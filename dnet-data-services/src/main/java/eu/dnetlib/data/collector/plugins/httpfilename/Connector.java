package eu.dnetlib.data.collector.plugins.httpfilename;

import eu.dnetlib.data.collector.plugins.HttpConnector;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;


/**
 * Created by miriam on 07/05/2018.
 */
public class Connector extends HttpConnector implements ConnectorInterface  {
    private String response;

    @Override
    public void get(final String requestUrl) throws CollectorServiceException {
        response = getInputSource(requestUrl);
    }

    @Override
    public String getResponse() {
        return response;
    }

    @Override
    public boolean isStatusOk() {
        return (response != null);
    }

    @Override
    public boolean responseTypeContains(String string) {
        String responseType = getResponseType();
        if (responseType != null)
            return responseType.contains(string);
        return false;
    }


}
