package eu.dnetlib.data.collector.plugins.opentrial;

import eu.dnetlib.data.collector.plugin.AbstractCollectorPlugin;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;



/**
 * Created by miriam on 07/03/2017.
 */
public class OpenTrialPlugin extends AbstractCollectorPlugin{


	@Override
	public Iterable<String> collect(final InterfaceDescriptor interfaceDescriptor, final String fromDate, final String untilDate)
			throws CollectorServiceException {
		try {

			OpenTrialIterator iterator = new OpenTrialIterator(interfaceDescriptor.getBaseUrl(),fromDate,untilDate);
			return iterator;
		} catch (Exception e) {
			throw new CollectorServiceException("OOOPS something bad happen on creating iterator ", e);
		}

	}
}
