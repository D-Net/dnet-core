package eu.dnetlib.data.collector.plugins.httplist;

import eu.dnetlib.data.collector.plugin.AbstractCollectorPlugin;
import eu.dnetlib.data.collector.plugins.HttpConnector;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;
import org.springframework.beans.factory.annotation.Autowired;

public class HttpListCollectorPlugin extends AbstractCollectorPlugin {

	@Autowired
	private HttpConnector httpConnector;
	@Override
	public Iterable<String> collect(final InterfaceDescriptor interfaceDescriptor, final String fromDate, final String untilDate)
			throws CollectorServiceException {
		final String baseUrl = interfaceDescriptor.getBaseUrl();
		final String listAddress = interfaceDescriptor.getParams().get("listUrl");

		return () -> new HttpListIterator(baseUrl, listAddress, httpConnector);
	}
}
