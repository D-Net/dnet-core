package eu.dnetlib.data.collector.plugins.datasets;

public class RequestField {

	private QueryField query;

	/**
	 * @return the query
	 */
	public QueryField getQuery() {
		return query;
	}

	/**
	 * @param query the query to set
	 */
	public void setQuery(QueryField query) {
		this.query = query;
	}

}
