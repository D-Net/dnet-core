package eu.dnetlib.data.collector.plugins.datasources;

import java.io.IOException;

import eu.dnetlib.data.collector.plugin.AbstractCollectorPlugin;
import eu.dnetlib.data.collector.plugins.HttpConnector;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Plugin to collect metadata record about data repositories from re3data.
 * <p>
 * Documentation on re3data API: http://service.re3data.org/api/doc.
 * </p>
 * <p>
 * BaseURL: http://service.re3data.org
 * </p>
 * <p>
 * API to get the list of repos: baseURL + /api/v1/repositories
 * </p>
 * <p>
 * API to get a repository: baseURL + content of link/@href of the above list
 * </p>
 *
 * @author alessia
 *
 */
public class Re3DataCollectorPlugin extends AbstractCollectorPlugin {

	private String repositoryListPath = "/api/v1/repositories";

	@Autowired
	private HttpConnector httpConnector;

	@Override
	public Iterable<String> collect(final InterfaceDescriptor interfaceDescriptor, final String fromDate, final String untilDate)
			throws CollectorServiceException {
		String repositoryListURL = interfaceDescriptor.getBaseUrl() + repositoryListPath;
		String input;
		try {
			input = httpConnector.getInputSource(repositoryListURL);
			return new Re3DataRepositoriesIterator(IOUtils.toInputStream(input, "UTF-8"), interfaceDescriptor.getBaseUrl(), getHttpConnector());
		} catch (IOException e) {
			throw new CollectorServiceException(e);
		}

	}

	public String getRepositoryListPath() {
		return repositoryListPath;
	}

	public void setRepositoryListPath(final String repositoryListPath) {
		this.repositoryListPath = repositoryListPath;
	}

	public HttpConnector getHttpConnector() {
		return httpConnector;
	}

	public void setHttpConnector(final HttpConnector httpConnector) {
		this.httpConnector = httpConnector;
	}
}
