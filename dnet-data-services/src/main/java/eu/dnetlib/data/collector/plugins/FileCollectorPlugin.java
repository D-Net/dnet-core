package eu.dnetlib.data.collector.plugins;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.net.URL;

import eu.dnetlib.data.collector.rmi.CollectorServiceException;

public class FileCollectorPlugin extends AbstractSplittedRecordPlugin {

	@Override
	protected BufferedInputStream getBufferedInputStream(final String baseUrl) throws CollectorServiceException {
		try {
			return new BufferedInputStream(new FileInputStream(new URL(baseUrl).getPath()));
		} catch (Exception e) {
			throw new CollectorServiceException("Error reading file " + baseUrl, e);
		}
	}

}
