package eu.dnetlib.data.collector.plugins.schemaorg;

import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class SchemaOrgMainKaggle {

    private static final Log log = LogFactory.getLog(SchemaOrgMainKaggle.class);

    public static void main(String[] args) throws Exception {

        ConsoleAppender console = new ConsoleAppender();
        console.setLayout(new PatternLayout("%d [%p|%c|%C{1}] %m%n"));
        console.setThreshold(Level.DEBUG);
        console.activateOptions();
        Logger.getLogger("eu.dnetlib.data.collector.plugins").addAppender(console);

        HashMap<String,String> params = new HashMap<>();
        params.put("consumerBlockPolling", Boolean.toString(true));
        params.put("consumerBlockPollingTimeout", "2");
        params.put("consumerBlockPollingTimeoutUnit", TimeUnit.MINUTES.toString());
        params.put("endpointCharset", StandardCharsets.UTF_8.name());
        params.put("updatedDateFormat", "YYYY-MM-DD");
        params.put("createdDateFormat", "YYYY-MM-DD");
        params.put("publicationDateFormat", "YYYY-MM-DD");
        params.put("contributorFallbackType", DatasetDocument.Contributor.ContributorType.Other.toString());
        params.put("identifierFallbackType", DatasetDocument.Identifier.IdentifierType.Handle.toString());
        params.put("identifierFallbackURL", Boolean.toString(true));
        params.put("identifierMappingARK", "ark, ARK");
        params.put("identifierMappingDOI", "doi, DOI");
        params.put("identifierMappingHandle", "Handle, HANDLE");
        params.put("identifierMappingPURL", "purl, PURL");
        params.put("identifierMappingURN", "urn, URN");
        params.put("identifierMappingURL", "url, URL");

        params.put("repositoryAccessType", "httpapi-kaggle");

        params.put("httpapi-kaggle_queueSize", "100");
        params.put("httpapi-kaggle_APICharset", StandardCharsets.UTF_8.name());
        params.put("httpapi-kaggle_queryUrl", "https://www.kaggle.com/datasets_v2.json?sortBy=updated&group=public&page={PAGE}&pageSize=20&size=sizeAll&filetype=fileTypeAll&license=licenseAll");
        params.put("httpapi-kaggle_queryPagePlaceholder", "{PAGE}");
        params.put("httpapi-kaggle_responsePropertyTotalDataset", "totalDatasetListItems");
        params.put("httpapi-kaggle_responsePropertyDatasetList", "datasetListItems");
        params.put("httpapi-kaggle_responsePropertyDatasetUrl", "datasetUrl");
        params.put("httpapi-kaggle_responseBaseDatasetUrl", "https://www.kaggle.com");
        params.put("httpapi-kaggle_producerBlockPollingTimeout", "2");
        params.put("httpapi-kaggle_producerBlockPollingTimeoutUnit", TimeUnit.MINUTES.toString());

        InterfaceDescriptor descriptor = new InterfaceDescriptor();
        descriptor.setId("schema.org - kaggle");
        descriptor.setBaseUrl("https://www.kaggle.com");

        descriptor.setParams(params);

        SchemaOrgPlugin schemaOrgPlugin = new SchemaOrgPlugin();

        Iterable<String> iterable = schemaOrgPlugin.collect(descriptor, null, null);

        String outDir = params.get("repositoryAccessType");

        log.info("saving content in " + outDir);

        File directory = new File(outDir);
        if (directory.exists()) {
            log.info(directory.getAbsolutePath() + " exists, cleaning up");
            FileUtils.deleteDirectory(directory);
        }
        FileUtils.forceMkdir(directory);
        Utils.writeFiles(iterable, outDir);

    }

}
