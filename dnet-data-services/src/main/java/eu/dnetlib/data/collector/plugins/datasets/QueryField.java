package eu.dnetlib.data.collector.plugins.datasets;

import java.util.HashMap;
import java.util.Map;

public class QueryField {

	private Map<String, String> term;

	public QueryField() {
		setTerm(new HashMap<String, String>());
	}

	/**
	 * @return the term
	 */
	public Map<String, String> getTerm() {
		return term;
	}

	/**
	 * @param term
	 *            the term to set
	 */
	public void setTerm(final Map<String, String> term) {
		this.term = term;
	}

}
