package eu.dnetlib.data.collector.plugins.datasets;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import eu.dnetlib.data.collector.plugin.AbstractCollectorPlugin;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;

public class DatasetsByProjectPlugin extends AbstractCollectorPlugin {

	@Override
	public Iterable<String> collect(final InterfaceDescriptor interfaceDescriptor, final String fromDate, final String untilDate)
			throws CollectorServiceException {
		try {
			URL url = new URL(interfaceDescriptor.getBaseUrl());
			url.openConnection();
			InputStreamReader reader = new InputStreamReader(url.openStream());
			DatasetsByProjectIterator iterator = new DatasetsByProjectIterator(reader);
			return iterator;
		} catch (IOException e) {
			throw new CollectorServiceException("OOOPS something bad happen on creating iterator ", e);
		}

	}
}
