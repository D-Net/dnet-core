package eu.dnetlib.data.collector.plugins.excel;


import eu.dnetlib.data.collector.plugin.AbstractCollectorPlugin;
import eu.dnetlib.data.collector.plugins.HttpCSVCollectorPlugin;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

/**
 * Created by miriam on 10/05/2017.
 */
public class ReadExcelPlugin extends AbstractCollectorPlugin{

	private static final Log log = LogFactory.getLog(ReadExcelPlugin.class);
	@Autowired
	HttpCSVCollectorPlugin httpCSVCollectorPlugin;



	@Override
	public Iterable<String> collect(final InterfaceDescriptor interfaceDescriptor, final String fromDate, final String untilDate)
			throws CollectorServiceException {
		Read r = new Read(interfaceDescriptor);
		r.setCollector(httpCSVCollectorPlugin);

		try {
			return r.parseFile();
		}catch(Exception e){
			log.error("Error importing excel file");
			throw new CollectorServiceException(e);
		}


	}
}
