package eu.dnetlib.data.collector.plugins.projects.grist;

import eu.dnetlib.data.collector.plugin.AbstractCollectorPlugin;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;

/**
 * Plugin to collect metadata record about projects and fundings via the europePMC GRIST API (e.g. WT projects).
 * <p>
 * Documentation on GRIST API: http://europepmc.org/GristAPI.
 * </p>
 * <p>
 * BaseURL: http://www.ebi.ac.uk/europepmc/GristAPI/rest/get/query=ga:"Wellcome Trust"&resultType=core
 * where resultType=core asks for the complete information (including abstracts).
 * The results returned by the API are XMLs.
 * </p>
 * <p>
 * Pagination: use parameter 'page'. When the response contains empty 'RecordList', it means we reached the end.
 * </p>
 *
 * @author alessia
 */
public class GristCollectorPlugin extends AbstractCollectorPlugin {

	@Override
	public Iterable<String> collect(final InterfaceDescriptor interfaceDescriptor, final String fromDate, final String untilDate)
			throws CollectorServiceException {
		//baseURL: http://www.ebi.ac.uk/europepmc/GristAPI/rest/get/query=ga:%22Wellcome%20Trust%22&resultType=core
		return new GristProjectsIterable(interfaceDescriptor.getBaseUrl());
	}

}
