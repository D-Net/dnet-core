package eu.dnetlib.data.collector.plugins.schemaorg.httpapi;

import eu.dnetlib.data.collector.plugins.schemaorg.RepositoryIterable;

public interface HttpApiRepositoryIterable extends RepositoryIterable {
}
