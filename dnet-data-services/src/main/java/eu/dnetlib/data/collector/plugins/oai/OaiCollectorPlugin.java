package eu.dnetlib.data.collector.plugins.oai;

import java.util.List;

import com.google.common.base.Splitter;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import eu.dnetlib.data.collector.plugin.AbstractCollectorPlugin;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;
import org.springframework.beans.factory.annotation.Required;

public class OaiCollectorPlugin extends AbstractCollectorPlugin {

	private static final String FORMAT_PARAM = "format";
	private static final String OAI_SET_PARAM = "set";

	private OaiIteratorFactory oaiIteratorFactory;

	@Override
	public Iterable<String> collect(final InterfaceDescriptor interfaceDescriptor, final String fromDate, final String untilDate)
			throws CollectorServiceException {
		final String baseUrl = interfaceDescriptor.getBaseUrl();
		final String mdFormat = interfaceDescriptor.getParams().get(FORMAT_PARAM);
		final String setParam = interfaceDescriptor.getParams().get(OAI_SET_PARAM);
		final List<String> sets = Lists.newArrayList();
		if (setParam != null) {
			sets.addAll(Lists.newArrayList(Splitter.on(",").omitEmptyStrings().trimResults().split(setParam)));
		}
		if (sets.isEmpty()) {
			// If no set is defined, ALL the sets must be harvested
			sets.add("");
		}

		if (baseUrl == null || baseUrl.isEmpty()) { throw new CollectorServiceException("Param 'baseurl' is null or empty"); }

		if (mdFormat == null || mdFormat.isEmpty()) { throw new CollectorServiceException("Param 'mdFormat' is null or empty"); }

		if (fromDate != null && !fromDate.matches("\\d{4}-\\d{2}-\\d{2}")) { throw new CollectorServiceException("Invalid date (YYYY-MM-DD): " + fromDate); }

		if (untilDate != null && !untilDate.matches("\\d{4}-\\d{2}-\\d{2}")) { throw new CollectorServiceException("Invalid date (YYYY-MM-DD): " + untilDate); }

		return () -> Iterators.concat(
				sets.stream()
					.map(set -> oaiIteratorFactory.newIterator(baseUrl, mdFormat, set, fromDate, untilDate))
					.iterator());
	}

	public OaiIteratorFactory getOaiIteratorFactory() {
		return oaiIteratorFactory;
	}

	@Required
	public void setOaiIteratorFactory(final OaiIteratorFactory oaiIteratorFactory) {
		this.oaiIteratorFactory = oaiIteratorFactory;
	}

}
