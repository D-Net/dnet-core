package eu.dnetlib.data.collector.plugins.httplist;

import eu.dnetlib.data.collector.plugins.HttpConnector;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Iterator;

public class HttpListIterator implements Iterator<String> {

	private HttpConnector httpConnector;

	private String baseUrl;
	private String currentLine;
	private BufferedReader reader;

	public HttpListIterator(final String baseUrl, final String listAddress, final HttpConnector httpConnector) {
		try {
			this.baseUrl = baseUrl;
			this.reader = new BufferedReader(new StringReader(download(listAddress)));
			this.httpConnector = httpConnector;
			this.currentLine = reader.readLine();
		} catch (Exception e) {
			throw new RuntimeException("Error creating iterator", e);
		}
	}

	@Override
	public synchronized boolean hasNext() {
		return StringUtils.isNotBlank(currentLine);
	}

	@Override
	public synchronized String next() {
		try {
			if (StringUtils.isNotBlank(currentLine)) {
				return download(baseUrl + currentLine);
			} else {
				throw new RuntimeException("Iterator has reached the end");
			}
		} finally {
			try {
				this.currentLine = reader.readLine();
			} catch (IOException e) {
				throw new RuntimeException("Error obtaining next element " + currentLine, e);
			}
		}
	}

	private String download(final String url) {
		try {
			return httpConnector.getInputSource(url);
		} catch (CollectorServiceException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void remove() {}

}
