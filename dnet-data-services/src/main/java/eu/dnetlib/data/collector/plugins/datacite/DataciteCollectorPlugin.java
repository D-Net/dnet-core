package eu.dnetlib.data.collector.plugins.datacite;

import java.text.ParseException;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import eu.dnetlib.data.collector.plugin.AbstractCollectorPlugin;
import eu.dnetlib.data.collector.plugin.CollectorPlugin;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DataciteCollectorPlugin extends AbstractCollectorPlugin implements CollectorPlugin {

	private static final Log log = LogFactory.getLog(DataciteCollectorPlugin.class);

	private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	@Override
	public Iterable<String> collect(InterfaceDescriptor interfaceDescriptor, String fromDate, String untilDate) throws CollectorServiceException {

		String baseurl = interfaceDescriptor.getBaseUrl();
		if (StringUtils.isBlank(baseurl)) throw new CollectorServiceException("baseUrl cannot be empty");
		long timestamp = 0;
		if (StringUtils.isNotBlank(fromDate)) {
			try {
				Date date = org.apache.commons.lang.time.DateUtils.parseDate(
						fromDate,
						new String[] { "yyyy-MM-dd", "yyyy-MM-dd'T'HH:mm:ssXXX", "yyyy-MM-dd'T'HH:mm:ss.SSSX", "yyyy-MM-dd'T'HH:mm:ssZ",
								"yyyy-MM-dd'T'HH:mm:ss.SX" });
				//timestamp =parsed.getTime() /1000;
				timestamp = date.toInstant().toEpochMilli() / 1000;
				log.info("Querying for Datacite records from timestamp " + timestamp + " (date was " + fromDate + ")");

			} catch (ParseException e) {
				throw new CollectorServiceException(e);
			}
		}
		final long finalTimestamp = timestamp;
		return () -> {
			try {
				return new DataciteESIterator(finalTimestamp, baseurl);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		};
	}

}
