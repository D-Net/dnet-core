/**
 * 
 */
package eu.dnetlib.data.collector.plugins.rest;

import eu.dnetlib.data.collector.plugin.AbstractCollectorPlugin;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;
import org.apache.commons.lang3.StringUtils;

/**
 * @author js, Andreas Czerniak
 *
 */
public class RestCollectorPlugin extends AbstractCollectorPlugin {

	@Override
	public Iterable<String> collect(InterfaceDescriptor ifDescriptor, String arg1, String arg2)
			throws CollectorServiceException {
		final String baseUrl = ifDescriptor.getBaseUrl();
		final String resumptionType = ifDescriptor.getParams().get("resumptionType");
		final String resumptionParam = ifDescriptor.getParams().get("resumptionParam");
		final String resumptionXpath = ifDescriptor.getParams().get("resumptionXpath");
		final String resultTotalXpath = ifDescriptor.getParams().get("resultTotalXpath");
		final String resultFormatParam = ifDescriptor.getParams().get("resultFormatParam");
		final String resultFormatValue = ifDescriptor.getParams().get("resultFormatValue");
		final String resultSizeParam = ifDescriptor.getParams().get("resultSizeParam");
		final String resultSizeValue = (StringUtils.isBlank(ifDescriptor.getParams().get("resultSizeValue"))) ? "100" : ifDescriptor.getParams().get("resultSizeValue");
                final String queryParams = ifDescriptor.getParams().get("queryParams");
		final String entityXpath = ifDescriptor.getParams().get("entityXpath");
		
		if (StringUtils.isBlank(baseUrl)) {throw new CollectorServiceException("Param 'baseUrl' is null or empty");}
		if (StringUtils.isBlank(resumptionType)) {throw new CollectorServiceException("Param 'resumptionType' is null or empty");}
		if (StringUtils.isBlank(resumptionParam)) {throw new CollectorServiceException("Param 'resumptionParam' is null or empty");}
		// if (StringUtils.isBlank(resumptionXpath)) {throw new CollectorServiceException("Param 'resumptionXpath' is null or empty");}
		// if (StringUtils.isBlank(resultTotalXpath)) {throw new CollectorServiceException("Param 'resultTotalXpath' is null or empty");}
		// resultFormatParam can be emtpy because some Rest-APIs doesn't like this argument in the query
                //if (StringUtils.isBlank(resultFormatParam)) {throw new CollectorServiceException("Param 'resultFormatParam' is null, empty or whitespace");}
		if (StringUtils.isBlank(resultFormatValue)) {throw new CollectorServiceException("Param 'resultFormatValue' is null or empty");}
		if (StringUtils.isBlank(resultSizeParam)) {throw new CollectorServiceException("Param 'resultSizeParam' is null or empty");}
                // prevent resumptionType: discover -- if (Integer.valueOf(resultSizeValue) <= 1) {throw new CollectorServiceException("Param 'resultSizeValue' is less than 2");}
                if (StringUtils.isBlank(queryParams)) {throw new CollectorServiceException("Param 'queryParams' is null or empty");}
		if (StringUtils.isBlank(entityXpath)) {throw new CollectorServiceException("Param 'entityXpath' is null or empty");}
		
		return () -> new RestIterator(
				baseUrl,
				resumptionType,
				resumptionParam,
				resumptionXpath,
				resultTotalXpath,
				resultFormatParam,
				resultFormatValue,
				resultSizeParam,
                                resultSizeValue,
				queryParams,
				entityXpath);
	}

}
