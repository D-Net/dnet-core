
package eu.dnetlib.data.collector.plugins.datacite.schema;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("originalId")
    @Expose
    private String originalId;
    @SerializedName("timestamp")
    @Expose
    private Integer timestamp;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOriginalId() {
        return originalId;
    }

    public void setOriginalId(String originalId) {
        this.originalId = originalId;
    }

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

}
