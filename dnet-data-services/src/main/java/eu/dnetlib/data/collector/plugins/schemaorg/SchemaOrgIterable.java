package eu.dnetlib.data.collector.plugins.schemaorg;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;

public class SchemaOrgIterable implements Iterable<String> {
	private static final Log log = LogFactory.getLog(SchemaOrgIterable.class);

	public static class Options {
		private EndpointAccessIterator.Options endpointAccessOptions;
		private DatasetMappingIterator.Options datasetMappingOptions;

		public EndpointAccessIterator.Options getEndpointAccessOptions() {
			return endpointAccessOptions;
		}

		public void setEndpointAccessOptions(EndpointAccessIterator.Options endpointAccessOptions) {
			this.endpointAccessOptions = endpointAccessOptions;
		}

		public DatasetMappingIterator.Options getDatasetMappingOptions() {
			return datasetMappingOptions;
		}

		public void setDatasetMappingOptions(DatasetMappingIterator.Options datasetMappingOptions) {
			this.datasetMappingOptions = datasetMappingOptions;
		}
	}

	private Options options;
	private RepositoryIterable repository;

	public SchemaOrgIterable(Options options, RepositoryIterable repository){
		this.options = options;
		this.repository = repository;
	}

	@Override
	public Iterator<String> iterator() {
		Iterator<String> repositoryIterator = this.repository.iterator();
		EndpointAccessIterator endpointAccessIterator = new EndpointAccessIterator(options.getEndpointAccessOptions(), repositoryIterator);
		DatasetMappingIterator datasetMappingIterator = new DatasetMappingIterator(options.getDatasetMappingOptions(), endpointAccessIterator);

		return datasetMappingIterator;
	}
}
