
package eu.dnetlib.data.collector.plugins.datacite.schema;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataciteSchema {

    @SerializedName("counter")
    @Expose
    private Integer counter;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;
    @SerializedName("scroll_id")
    @Expose
    private String scrollId;
    @SerializedName("total")
    @Expose
    private Integer total;

    public Integer getCounter() {
        return counter;
    }

    public void setCounter(Integer counter) {
        this.counter = counter;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public String getScrollId() {
        return scrollId;
    }

    public void setScrollId(String scrollId) {
        this.scrollId = scrollId;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

}
