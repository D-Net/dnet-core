package eu.dnetlib.data.collector.plugins.projects.gtr2;

import eu.dnetlib.data.collector.plugin.AbstractCollectorPlugin;
import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;

/**
 * Plugin to collect metadata record about projects and fundings via the RCUK grt2 API.
 * <p>
 * Documentation : http://gtr.rcuk.ac.uk/resources/api.html.
 * </p>
 * <p>
 * BaseURL: http://gtr.rcuk.ac.uk/gtr/api
 * The results returned by the API are XMLs.
 * </p>
 * <p>
 * Pagination: TO BE DEFINED. Exceeding the number of pages available will result in a HTTP response code of 404
 * </p>
 *
 * @author alessia
 */
public class Gtr2CollectorPlugin extends AbstractCollectorPlugin {

	@Override
	public Iterable<String> collect(final InterfaceDescriptor interfaceDescriptor, final String fromDate, final String untilDate)
			throws CollectorServiceException {
		if (fromDate != null && !fromDate.matches("\\d{4}-\\d{2}-\\d{2}")) { throw new CollectorServiceException("Invalid date (YYYY-MM-DD): " + fromDate); }

		return new Gtr2ProjectsIterable(interfaceDescriptor.getBaseUrl(), fromDate);
	}

}
