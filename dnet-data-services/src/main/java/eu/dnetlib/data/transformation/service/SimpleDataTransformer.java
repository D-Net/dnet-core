package eu.dnetlib.data.transformation.service;

import javax.xml.transform.TransformerConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.Resource;

import eu.dnetlib.common.profile.ProfileNotFoundException;
import eu.dnetlib.common.profile.ResourceDao;
import eu.dnetlib.data.collective.transformation.VocabularyRegistry;
import eu.dnetlib.data.collective.transformation.engine.SimpleTransformationEngine;
import eu.dnetlib.data.collective.transformation.engine.core.TransformationImpl;
import eu.dnetlib.data.collective.transformation.utils.TransformationRulesImportTool;
import eu.dnetlib.miscutils.functional.UnaryFunction;

public class SimpleDataTransformer implements UnaryFunction<String, String> {

	/**
	 * logger.
	 */
	private static final Log log = LogFactory.getLog(SimpleDataTransformer.class);

	/**
	 * Transformation rule profile
	 */
	private String ruleProfile;
	private SimpleTransformationEngine transformationEngine;

	public SimpleDataTransformer(final String ruleProfile) {
		this.ruleProfile = ruleProfile;

		// TODO
		// instantiate here the xml transformer

		if (log.isDebugEnabled()) {
			log.debug("************************************************************");
			log.debug("New transformer created from profile " + ruleProfile);
			log.debug("************************************************************");
		}

	}

	@Override
	public String evaluate(String record) {
		if (log.isDebugEnabled()) {
			log.debug("************************************************************");
			log.debug("INPUT: " + record);
			log.debug("************************************************************");
		}

		final String output = transform(record);

		if (log.isDebugEnabled()) {
			log.debug("************************************************************");
			log.debug("OUTPUT: " + output);
			log.debug("************************************************************");
		}

		return output;
	}

	private String transform(String record) {
		// use here the xml transformer
		return transformationEngine.transform(record);
	}

	protected void setupEngine(VocabularyRegistry vocabularyRegistry, Resource transformationTemplate, 
			Resource defaultSchema, TransformationRulesImportTool rulesProfileUtil, ResourceDao resourceDao, Resource blacklistApi)throws TransformerConfigurationException, ProfileNotFoundException{
		transformationEngine = new SimpleTransformationEngine();
		transformationEngine.setVocabularyRegistry(vocabularyRegistry);
		TransformationImpl transformation = new TransformationImpl();
		transformation.setSchema(defaultSchema);
		transformation.setTemplate(transformationTemplate);
		transformation.init();
		if (log.isDebugEnabled()) {
			log.debug("************************************************************");
			log.debug(ruleProfile);
			log.debug("************************************************************");
		}
		transformation.setRuleLanguageParser(rulesProfileUtil.getRuleLanguageParser(ruleProfile));
		transformation.configureTransformation();
		transformationEngine.setTransformation(transformation);
		transformationEngine.setResourceDao(resourceDao);
		transformationEngine.setBlacklistApi(blacklistApi);
	}
	
	public String getRuleProfile() {
		return ruleProfile;
	}

	public void setRuleProfile(String ruleProfile) {
		this.ruleProfile = ruleProfile;
	}

}
