package eu.dnetlib.data.transformation.service;

import javax.annotation.Resource;

import eu.dnetlib.common.profile.ResourceDao;
import eu.dnetlib.data.collective.transformation.VocabularyRegistry;
import eu.dnetlib.data.collective.transformation.utils.TransformationRulesImportTool;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpDocumentNotFoundException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpException;

public class DataTransformerFactory {

	@Resource(name = "vocabularyRegistry")
	private VocabularyRegistry vocabularyRegistry;

	@Resource(name = "transformationTemplate")
	private org.springframework.core.io.Resource transformationTemplate;

	@Resource(name = "defaultSchema")
	private org.springframework.core.io.Resource defaultSchema;

	@Resource(name = "transformationRuleProfileUtil")
	private TransformationRulesImportTool transformationRuleProfileUtil;

	@Resource(name = "resourceDao")
	private ResourceDao resourceDao;
	
	@Resource(name = "blacklistApi")
	private org.springframework.core.io.Resource blacklistApi;

	public SimpleDataTransformer createTransformer(final String ruleid) throws ISLookUpDocumentNotFoundException, ISLookUpException {
		// String profile = lookupLocator.getService().getResourceProfile(ruleid);
		SimpleDataTransformer transformer = new SimpleDataTransformer(ruleid);
		try {
			transformer.setupEngine(vocabularyRegistry, transformationTemplate, defaultSchema, transformationRuleProfileUtil, resourceDao, blacklistApi);
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
		return transformer;
	}
}
