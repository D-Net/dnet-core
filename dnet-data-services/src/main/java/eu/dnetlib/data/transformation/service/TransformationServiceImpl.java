package eu.dnetlib.data.transformation.service;

import javax.annotation.Resource;
import javax.xml.ws.wsaddressing.W3CEndpointReference;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.dnetlib.data.transformation.service.rmi.TransformationService;
import eu.dnetlib.data.transformation.service.rmi.TransformationServiceException;
import eu.dnetlib.enabling.resultset.MappedResultSetFactory;
import eu.dnetlib.enabling.tools.AbstractBaseService;

public class TransformationServiceImpl extends AbstractBaseService implements TransformationService {

	@Resource
	private MappedResultSetFactory mappedResultSetFactory;
	
	@Resource
	private DataTransformerFactory dataTransformerFactory;
	
	/**
	 * logger.
	 */
	private static final Log log = LogFactory.getLog(TransformationServiceImpl.class);
	
	@Override
	public W3CEndpointReference transform(String ruleid, W3CEndpointReference epr) throws TransformationServiceException {
		try {
			return mappedResultSetFactory.createMappedResultSet(epr, dataTransformerFactory.createTransformer(ruleid));
		} catch (Exception e) {
			log.error("Error generating mapped resultset - ruleId: " + ruleid, e);
			throw new TransformationServiceException("Error generating mapped resultset - ruleId: " + ruleid, e);
		}
	}

}
