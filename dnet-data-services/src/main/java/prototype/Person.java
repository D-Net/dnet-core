package prototype;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.util.List;
import java.util.Set;

import prototype.utils.Capitalize;
import prototype.utils.DotAbbreviations;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.hash.Hashing;

//import eu.dnetlib.pace.clustering.NGramUtils;
//import eu.dnetlib.pace.util.Capitalise;
//import eu.dnetlib.pace.util.DotAbbreviations;

public class Person {
	private List<String> name = Lists.newArrayList();
	private List<String> surname = Lists.newArrayList();
	private List<String> fullname = Lists.newArrayList();

	private static Set<String> particles = null;

	public Person(String s) {
		s = Normalizer.normalize(s, Normalizer.Form.NFD); // was NFD
		s = s.replaceAll("\\(.+\\)", "");
		s = s.replaceAll("\\[.+\\]", "");
		s = s.replaceAll("\\{.+\\}", "");
		s = s.replaceAll("\\s+-\\s+", "-");
		

//		s = s.replaceAll("[\\W&&[^,-]]", " ");
		
//		System.out.println("class Person: s: " + s);

//		s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}&&[^,-]]", " ");
		s = s.replaceAll("[\\p{Punct}&&[^-,]]", " ");
		s = s.replaceAll("\\d", " ");
		s = s.replaceAll("\\n", " ");
		s = s.replaceAll("\\.", " ");
		s = s.replaceAll("\\s+", " ");

		if (s.contains(",")) {
	//		System.out.println("class Person: s: " + s);

			String[] arr = s.split(",");
			if (arr.length == 1) {
				fullname = splitTerms(arr[0]);
			} else if (arr.length > 1) {
				surname = splitTerms(arr[0]);
				name = splitTermsFirstName(arr[1]);
//				System.out.println("class Person: surname: " + surname);
//				System.out.println("class Person: name: " + name);

				fullname.addAll(surname);
				fullname.addAll(name);
			}
		} else {
			fullname = splitTerms(s);

			int lastInitialPosition = fullname.size();
			boolean hasSurnameInUpperCase = false;

			for (int i = 0; i < fullname.size(); i++) {
				String term = fullname.get(i);
				if (term.length() == 1) {
					lastInitialPosition = i;
				} else if (term.equals(term.toUpperCase())) {
					hasSurnameInUpperCase = true;
				}
			}
			if (lastInitialPosition < fullname.size() - 1) { // Case: Michele G. Artini
				name = fullname.subList(0, lastInitialPosition + 1);
				System.out.println("name: " + name);
				surname = fullname.subList(lastInitialPosition + 1, fullname.size());
			} else if (hasSurnameInUpperCase) { // Case: Michele ARTINI
				for (String term : fullname) {
					if (term.length() > 1 && term.equals(term.toUpperCase())) {
						surname.add(term);
					} else {
						name.add(term);
					}
				}
			} else if (lastInitialPosition == fullname.size()){
				surname = fullname.subList(lastInitialPosition - 1, fullname.size());
				name = fullname.subList(0,  lastInitialPosition - 1);
			}
			
		}
	}
	
	private List<String> splitTermsFirstName(String s){
		List<String> list = Lists.newArrayList();
		for (String part : Splitter.on(" ").omitEmptyStrings().split(s)) {
			if (s.trim().matches("\\p{Lu}{2,3}")){
				String[] parts = s.trim().split("(?=\\p{Lu})"); // (Unicode UpperCase)
				for (String p: parts){
					if (p.length() > 0)
						list.add(p);					
				}				
			}else{
				list.add(part);				
			}

		}
		return list;
	}

	private List<String> splitTerms(String s) {
		if (particles == null) {
//			particles = NGramUtils.loadFromClasspath("/eu/dnetlib/pace/config/name_particles.txt");
		}

		List<String> list = Lists.newArrayList();
		for (String part : Splitter.on(" ").omitEmptyStrings().split(s)) {
	//		if (!particles.contains(part.toLowerCase())) {
			list.add(part);				

	//		}
		}
		return list;
	}

	public List<String> getName() {
		return name;
	}

	public List<String> getSurname() {
		return surname;
	}

	public List<String> getFullname() {
		return fullname;
	}
	
	public String hash() {
		return Hashing.murmur3_128().hashString(getNormalisedFullname(),StandardCharsets.UTF_8).toString();
	}
	
	public String getNormalisedFullname() {
		return isAccurate() ? 
				Joiner.on(" ").join(getSurname()) + ", " + Joiner.on(" ").join(getNameWithAbbreviations()) : 
				Joiner.on(" ").join(fullname);
//				return isAccurate() ? 
//						Joiner.on(" ").join(getCapitalSurname()) + ", " + Joiner.on(" ").join(getNameWithAbbreviations()) : 
//						Joiner.on(" ").join(fullname);
	}
	
	public List<String> getCapitalSurname() {
		return Lists.newArrayList(Iterables.transform(surname, new Capitalize() ));
	}
	
	public List<String> getNameWithAbbreviations() {
		return Lists.newArrayList(Iterables.transform(name, new DotAbbreviations() ));
	}	

	public boolean isAccurate() {
		return (name != null && surname != null && !name.isEmpty() && !surname.isEmpty());
	}
}
