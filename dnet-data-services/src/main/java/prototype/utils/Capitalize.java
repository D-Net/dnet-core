package prototype.utils;

import org.apache.commons.lang3.text.WordUtils;

import com.google.common.base.Function;

public class Capitalize implements Function<String, String>{

	@Override
	public String apply(String s){
		return WordUtils.capitalize(s.toLowerCase());
	}
}
