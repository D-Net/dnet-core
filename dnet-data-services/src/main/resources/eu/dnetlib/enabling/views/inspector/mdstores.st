$inspector/master(it={

<h2>MDStores</h2>
<a href="doGarbage.do" >Start Garbage</a>
<table>
  <tr>
    <th>Format</th>
    <th>Layout</th>
    <th>Interpretation</th>
    <th>Size <a href="refreshSizes.do">[R]</a></th>
    <th>ID</th>
    <th>Indexed?</th>
    <th>Ensure index</th>
    <th>MDStore Transaction</th>
  </tr>
  $mdstores:{
    <tr><td>$it.format$</td><td>$it.layout$</td><td>$it.interpretation$</td><td>$it.size$</td><td><a href="mdstore.do?id=$it.id$">$it.id$</a></td><td><span class="$if(it.indexed)$enabled$else$disabled$endif$">$it.indexed$</span></td><td><a href="ensure.do?id=$it.id$">GO</a></td><td><a href="infoTransaction.do?id=$it.id$">VIEW</a></td></tr>
  }$
</table>
})$