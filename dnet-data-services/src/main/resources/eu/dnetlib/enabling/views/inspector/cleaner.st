$inspector/master(it={

<style type="text/css">
  #results {
    width: 100%;
  }

  #results td:first-child {
   width: 2em;
  }

  #results td {
    border: 1px solid #cecece;
  }
</style>

<h2>Browse indices</h2>

<form method="POST">
Cleaner rules:
<select name="rule">
$rules:{<option $if(it.selected)$selected$endif$>$it.value$</option>}$
</select><br /><br />

Dirty Record:<br />
<textarea name="dirtyText" cols="80" rows="10">$dirtyText$</textarea>
<br /><br />
<input type="submit" value="submit"/>
</form>
<br />

Cleaned Record:<br />
<textarea readonly="readonly" cols="80" rows="10">$cleanedText$</textarea>


})$