$inspector/master(it={

<style type="text/css">
  #results {
    width: 100%;
  }

  #results td:first-child {
   width: 2em;
  }

  #results td {
    border: 1px solid #cecece;
  }
</style>

<h2>Test transformation:</h2>

<form method="POST">
Transformation rules:
<select name="rule">
$rules:{<option $if(it.selected)$selected$endif$ value="$it.id$">$it.name$</option>}$
</select><br /><br />

Input Record:<br />
<textarea name="record" cols="100" rows="10">$input$</textarea>
<br /><br />
<input type="submit" value="submit"/>
</form>
<br />

Output Record:<br />
<textarea readonly="readonly" cols="100" rows="10">$output$</textarea>


})$