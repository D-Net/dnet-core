$inspector/master(it={

<style type="text/css">
 .mdstoreRecords li.record {
   display: block;
   border-top: 1px solid #ccc;
 }
 
 .mdstoreRecords ul.nav li {
   display: inline;
 }
</style>

<h2>MDStore details</h2>
<p>

<form action="mdstore.do?id=$id$" method="POST">
GREP: <input name="regex" value="$regex$"/> REPLACEMENT: <input name="replace" value="$replace$"/>
Replace? <input type="checkbox" name="checkReplace" $if(checkReplace)$checked="checked"$endif$/>
<input type="submit" value="grep/repl"/>
</form>
</p>

<p>Show from: $start$ (<a href="?id=$id$&start=$prevPage$&regex=$regex$">prev</a>) out of $size$ (<a href="?id=$id$&start=$nextPage$&regex=$regex$">next</a>)</p>

<ul class="mdstoreRecords">
$page:{r|
  <li class="record">
   <ul class="nav">
     <li><a href="mdstoreEditResult.do?mdId=$r.mdId$&docId=$r.docId$">edit</a></li>
     <li><a href="mdstoreDeleteRecord.do?mdId=$r.mdId$&docId=$r.docId$">delete</a></li>
   </ul>
	 <pre WRAP="off">$r.body$</pre>
  </li>
}$
</ul>
})$
