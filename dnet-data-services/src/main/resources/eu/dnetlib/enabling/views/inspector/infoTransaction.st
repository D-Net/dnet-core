$inspector/master(it={

<h2>MDStores Transaction Info</h2>

<h3>MDStore Id : </h3>
<a href="mdstore.do?id=$info.mdId$">$info.mdId$</a>

<h3>Current collection id: </h3>
<a href="mdstore.do?id=$info.mdId$">$info.currentId$</a>







<h3> Read- Lock MDStore : </h3>
<table>
  <tr>
    <th>Id</th>
    <th>Last time read</th>    
    <th>drop</th>    
  </tr>  
  $info.stillUsed:{
   	<tr><td>$it.id$</td> <td>$it.lastRead$</td><td><a href="dropUsedCollection.do?mdId=$info.mdId$&id=$it.id$">drop </a></td> </tr>
   }$ 
</table>


<h3> Transaction MDStore : </h3>
<table>
  <tr>
    <th>Id</th>
    <th>Date</th>    
    <th>Size</th>
    <th>drop</th>      
  </tr>  
  $info.transactions:{
   	<tr><td>$it.id$</td> <td>$it.date$</td><td>$it.size$</td> <td><a href="invalidTransactionCollection.do?mdId=$info.mdId$&id=$it.id$">drop </a></td> </tr>
   }$ 
</table>


})$