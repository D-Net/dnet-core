$inspector/master(it={

<style type="text/css">
 #recordbody {
  height: 60em;
  width: 100%;
  border: 1px solid #B0B0FF;
  padding: 4px;
 }
</style>

<h2>Low level edit mdstore record:</h2>

<ul>
  <li><a href="javascript:document.forms[0].submit()">save</a></li>
</ul>

<p> It will <b>not</b> trigger a mdstore modification date. </p>
<form action="mdstoreSaveRecord.do" method="POST" accept-charset="UTF-8">
<input type="hidden" name="mdId" value="$mdId$"/>
<textarea id="recordbody" name="body" WRAP="off">
$body$
</textarea>

</form>

})$
