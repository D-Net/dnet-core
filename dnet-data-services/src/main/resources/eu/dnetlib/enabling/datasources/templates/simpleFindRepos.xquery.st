for \$x in
	collection('/db/DRIVER/RepositoryServiceResources/RepositoryServiceResourceType')
where
	\$x//DATASOURCE_TYPE = '$type$'
return
	concat(\$x//RESOURCE_IDENTIFIER/@value, ' @=@ ', \$x//OFFICIAL_NAME, ' @=@ ', \$x//DATASOURCE_ORIGINAL_ID, ' @=@ ', string-join(\$x//INTERFACE/@id, ','), ' @=@ true'),
for \$x in
	collection('/db/DRIVER/PendingRepositoryResources/RepositoryServiceResourceType')
where
	\$x//DATASOURCE_TYPE = '$type$'
return
	concat(\$x//RESOURCE_IDENTIFIER/@value, ' @=@ ', \$x//OFFICIAL_NAME, ' @=@ ', \$x//DATASOURCE_ORIGINAL_ID, ' @=@ ', string-join(\$x//INTERFACE/@id, ','), ' @=@ false')