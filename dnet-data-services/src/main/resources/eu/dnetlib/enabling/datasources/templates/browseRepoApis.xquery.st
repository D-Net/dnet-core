let \$list := collection('/db/DRIVER/RepositoryServiceResources/RepositoryServiceResourceType')//INTERFACE/$xpath$

for \$x in distinct-values(\$list)
where string-length(\$x) > 0
return concat(\$x, ' @-@-@ ', count(\$list[.=\$x]))