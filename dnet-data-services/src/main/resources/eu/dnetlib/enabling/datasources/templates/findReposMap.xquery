for $x in
	collection('/db/DRIVER/RepositoryServiceResources/RepositoryServiceResourceType')
where
	$x//NUMBER_OF_OBJECTS != 0 and not($x//LATITUDE = 0 and $x//LONGITUDE = 0)
return
<ds>
	<dsId>{$x//RESOURCE_IDENTIFIER/@value/string()}</dsId>
	<name>{$x//OFFICIAL_NAME/text()}</name>
	<lat>{$x//LATITUDE/text()}</lat>
	<lng>{$x//LONGITUDE/text()}</lng>
</ds>