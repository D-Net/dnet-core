for \$x in
collection('/db/DRIVER/RepositoryServiceResources/RepositoryServiceResourceType')//INTERFACE[$cond$]
return
	<ROW>
		<REPO
			id="{\$x/../../../../HEADER/RESOURCE_IDENTIFIER/@value/string()}"
			name="{\$x/../../OFFICIAL_NAME/text()}"
			country="{\$x/../../COUNTRY/text()}"
			prefix="{\$x/../..//EXTRA_FIELDS/FIELD[./key='NamespacePrefix']/value}" />
		{\$x}
	</ROW>