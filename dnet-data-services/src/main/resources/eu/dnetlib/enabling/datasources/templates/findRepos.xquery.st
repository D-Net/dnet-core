for \$x in collection('/db/DRIVER/RepositoryServiceResources/RepositoryServiceResourceType')
$if(conds)$
	where $conds.keys:{xpath| \$x$xpath$ = '$conds.(xpath)$'}; separator=" and "$
$endif$
return \$x
,
for \$x in collection('/db/DRIVER/PendingRepositoryResources/RepositoryServiceResourceType')
$if(conds)$
	where $conds.keys:{xpath| \$x$xpath$ = '$conds.(xpath)$'}; separator=" and "$
$endif$
return \$x