<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:import href="identity.xsl"/>
    
    <xsl:template match="//*[local-name()='objIdentifier']"/>
    
	<xsl:template match="//*[local-name()='header']">
	  <xsl:copy>
	    <xsl:attribute name="syntaxcheck">failed</xsl:attribute>	  
	    <xsl:copy-of select="@*"/>
	    <xsl:apply-templates/>
	  </xsl:copy>
	</xsl:template>
</xsl:stylesheet>