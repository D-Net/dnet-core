<?xml version="1.0" encoding="UTF-8"?>
 <xsl:stylesheet  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.1"
 				  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
 				  xmlns="http://www.driver-repository.eu/"
        		  xmlns:TransformationFunction="xalan://eu.dnetlib.data.collective.transformation.core.xsl.ext.TransformationFunctionProxy"
                  extension-element-prefixes="TransformationFunction"
                  exclude-result-prefixes="TransformationFunction">
 				  
  <xsl:output method='xml' indent='yes'/>
 
  <xsl:param name="index" select="''"/>
  <xsl:variable name="tf" select="TransformationFunction:getInstance()"/>
  
  <xsl:template name="terminate">
	  <xsl:message terminate="yes"/>
  </xsl:template>  

</xsl:stylesheet>
