<?xml version="1.0" encoding="UTF-8"?>
 <xsl:stylesheet  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
 				  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
 				  xmlns="http://namespace.openaire.eu/"
        		  xmlns:TransformationFunction="eu.dnetlib.data.collective.transformation.core.xsl.ext.TransformationFunctionProxy"
                  extension-element-prefixes="TransformationFunction"
                  exclude-result-prefixes="TransformationFunction">

  <xsl:param name="index" select="0"/>
  <xsl:variable name="tf" select="TransformationFunction:getInstance()"/>

  <xsl:template name="terminate">
	  <xsl:message terminate="yes"/>
  </xsl:template>  
</xsl:stylesheet>
