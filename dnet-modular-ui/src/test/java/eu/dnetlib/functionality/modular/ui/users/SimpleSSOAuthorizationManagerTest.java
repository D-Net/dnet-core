package eu.dnetlib.functionality.modular.ui.users;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

public class SimpleSSOAuthorizationManagerTest {
	
	private final static String PAYLOAD = "{\"uid\":\"michele.artini\",\"fullname\":\"Michele Artini\",\"email\":\"michele.artini@isti.cnr.it\"}";
	private final static String SIGNATURE = "89f7a05d39d7a1e6489506c0a27f6087cf4ca0c7f0298c29d61464f5522be146363e8ad63312cc3d7fdcf5b5f1ff7b26e64ed1009b52b6562deebbd267dc6f49e25714f54a8cd2936db6b15e27e9dfc7ab762728d33449e8734101265bbb8b70e5efe2845d5ae2da6e43f25ab75e531fa6cc15e31497bb344b1cca6d687ecf2501c407316423c3eecbe98df43a0721b63f3a1a07fc1e7d75855cc6dc9dce18817b008d60fb4b225d462755d5671fdd01d3c7b08d5d039524e45a78a808377a3cc44b13c0c8e314aa1893e88cd32120e8beeacf5df8412c8228c1e8aa046a159ea1d6a36de160456f32001e23978b1b2d15490a07943d95a56d9dfd0f7bc6b4c6";

	private SimpleSSOAuthorizationManager manager = new SimpleSSOAuthorizationManager();
	
	
	@Before
	public void setup() throws Exception {
		manager.setPubKeyAlgo("RSA");
		manager.setPubKeyFile(new ClassPathResource("/eu/dnetlib/functionality/modular/ui/users/pubkey.der"));
		manager.setSignatureAlgo("SHA1withRSA");
		manager.init();
	}
	@Test
	public void testIsValidMessage() {
		assertTrue(manager.isValidMessage(PAYLOAD, SIGNATURE));
		assertFalse(manager.isValidMessage(PAYLOAD.toLowerCase(), SIGNATURE));
		assertFalse(manager.isValidMessage(PAYLOAD + "123", SIGNATURE));
		assertFalse(manager.isValidMessage(PAYLOAD, SIGNATURE + "123"));
		assertFalse(manager.isValidMessage(PAYLOAD + PAYLOAD, SIGNATURE));
		assertFalse(manager.isValidMessage("aaa", "aaa"));
		assertFalse(manager.isValidMessage(null, SIGNATURE));
		assertFalse(manager.isValidMessage(PAYLOAD, null));
		assertFalse(manager.isValidMessage("", SIGNATURE));
		assertFalse(manager.isValidMessage(PAYLOAD, ""));
		assertFalse(manager.isValidMessage(null, ""));
		assertFalse(manager.isValidMessage("", null));
		assertFalse(manager.isValidMessage("", ""));
		assertFalse(manager.isValidMessage(null, null));
	}

}
