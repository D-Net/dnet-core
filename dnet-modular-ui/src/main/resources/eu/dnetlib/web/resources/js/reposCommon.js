function common_init($scope, $http, $sce, $location) {
	$scope.currentRepoId = '';
	$scope.currentRepoName = '';
	$scope.currentRepoHTML = '';
	
	$scope.go = function(path) {
		$location.path(path); 
	}
	
	$scope.showError = function(error) {
		show_notification("error", error);
	}
	
	$scope.showNotification = function(message) {
		show_notification("info", message);
	}
	
	$scope.showSpinner = function() {
		showSpinner();
	}

	$scope.hideSpinner = function() {
		hideSpinner();
	}
	
	initSpinner();
	
	$scope.showDetails = function(id, name) {
		$scope.showSpinner();

		$http.get('getRepoDetails.do?id=' + id)
        .success(
            function(data) {
            	$scope.currentRepoId = id;
            	$scope.currentRepoName = name;
            	$scope.currentRepoHTML = $sce.trustAsHtml(data);
            	$scope.hideSpinner();
            	$('#repoDetailsModal').modal('show');
            }
        ).error(
            function() {
            	$scope.showError('Something really bad must have happened to our fellow hamster..');
            	$scope.hideSpinner();
            }
        );
	}
	
	$scope.resizeMainElement = function(elem) {
		var height = 0;
		var body = window.document.body;
		if (window.innerHeight) {
			height = window.innerHeight;
		} else if (body.parentElement.clientHeight) {
			height = body.parentElement.clientHeight;
		} else if (body && body.clientHeight) {
			height = body.clientHeight;
		}
		elem.style.height = ((height - elem.offsetTop - 80) + "px");
	}

	
}