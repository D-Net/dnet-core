var module = angular.module('isManager', ['ngRoute', 'isManagerControllers']);

module.config([
	'$routeProvider',
	function($routeProvider) {
		$routeProvider
			.when('/list',                 { templateUrl: '../resources/html/is/list.html',          controller: 'listCollectionsCtrl' })
			.when('/profiles/:kind/:type', { templateUrl: '../resources/html/is/profiles.html',      controller: 'profilesCtrl' })
			.when('/profile/:id',          { templateUrl: '../resources/html/is/profile.html',       controller: 'profileCtrl' })
			.when('/schemas',              { templateUrl: '../resources/html/is/schemas.html',       controller: 'schemasCtrl' })
			.when('/schema/:name',         { templateUrl: '../resources/html/is/schema.html',        controller: 'schemaCtrl' })
			.when('/services',             { templateUrl: '../resources/html/is/services.html',      controller: 'servicesCtrl' })
			.when('/q',                    { templateUrl: '../resources/html/is/isQuery.html',       controller: 'isQueryCtrl' })
			.when('/register',             { templateUrl: '../resources/html/is/register.html',      controller: 'registerProfileCtrl' })
			.when('/bulk',                 { templateUrl: '../resources/html/is/bulk.html',          controller: 'bulkImporterCtrl' })
			.when('/verify',               { templateUrl: '../resources/html/is/verify.html',        controller: 'validateProvilesCtrl' })
			.when('/blackboard',           { templateUrl: '../resources/html/is/blackboard.html',    controller: 'blackboardCtrl' })
			.when('/backup',               { templateUrl: '../resources/html/is/backup.html',        controller: 'backupCtrl' })
			.otherwise({ redirectTo: '/list' });
	}
]);


module.controller('moduleMenuCtrl', [ '$scope', '$location',  
	function ($scope, $location) {
		$scope.isActive = function(regex) {
			var re = new RegExp(regex);
			return re.test($location.path());
		}
	}
]);

module.filter('encodeURIComponent', function() {
    return window.encodeURIComponent;
});

module.filter('reverse', function () {
	return function(items) {
		return items.slice().reverse();
	}
});