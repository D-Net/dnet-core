var module = angular.module('transformUI', []);

module.controller('transformCtrl', function ($scope, $http) {
	$scope.outputRecord = '';
	$scope.error = {};
	
	$scope.transform = function(rule, s) {
		$scope.outputRecord = '';
		$scope.error = {};
		$http.defaults.headers.post["Content-Type"] = "application/json; charset=UTF-8";
		$http.post('transform/transform.do?rule=' + encodeURIComponent(rule), s).success(function(res) {
            	if(res) {
            		$scope.outputRecord = res;
            	} else {
            		$scope.error({
            			'message' : 'empty response',
            		});
            	}
		}).error(function(err) {
			$scope.error = err;
		});
				
	}
});

module.filter('encodeURIComponent', function() {
	return window.encodeURIComponent;
});
