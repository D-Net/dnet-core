function validValues(arr) {
	var res = [];
	for (i=0; i<arr.length; i++) {
		res.push({'id': arr[i], 'name': arr[i]});
	}
	return res;
}

function intRange(from, to) {
	var res = [];
	for (i=from; i<to; i++) {
		res.push({'id': i, 'name': i});
	}
	return res;
}

function obtainValues(bean, params) {
	var strReturn = [];
	jQuery.ajax({
		type: "POST",
		url: "wf_obtainValidValues.list?bean=" + bean,
		async:false,
		data: params,
		dataType: "json",
		success: function(json) {
			strReturn = json;
		}
	});
	return strReturn;
}

function listFiles(path, ext)      { return obtainValues('listFiles', { 'path': path, 'ext': ext }); }
function listProfiles(type, xpath) { return obtainValues('listProfiles', { 'type': type, 'xpath': xpath }); }
