var module = angular.module('dnetLogUI', ['luegg.directives']);


module.controller('dnetLogCtrl', function ($scope, $http, $timeout, $sce) {
	initSpinner();

	$scope.logs = [];
	$scope.size = 100;
	$scope.interval = 2000;
	$scope.prepaused = false;
	$scope.paused = false;
	
	$scope.last = undefined; 

	$scope.filterLog = '';

	$scope.highlight = function(text, search) {
		return $sce.trustAsHtml(search ? text.replace(new RegExp(search, 'gi'), '<span style="color: red">$&</span>') : text);
	};

	$scope.pause = function() {
		$scope.prepaused = true;
	}
	
	$scope.play = function() {
		if ($scope.paused) {
			$scope.nextLogs();
		} 
		$scope.prepaused = false;
		$scope.paused = false;
	}
	
	$scope.loadLogs = function() {
		showSpinner();
		$scope.logs = [];
		
		if ($scope.size) {
			$http.get('log/tail/' + $scope.size).success(function(data) {
				hideSpinner();
				if (data.length > 0) {
					$scope.logs = data;
					
					if (!$scope.last) { // The nextLogs cycle is already running
						$timeout($scope.nextLogs, $scope.interval);
					}
				} else {
					$timeout($scope.loadLogs, $scope.interval);
				}
			}).error(function() {
				show_notification("error","An error occurred while fetching logs");
				hideSpinner();
			});
		} else {
			show_notification("error","Invalid value in [Number of lines]");
			$scope.paused = true;
		}
	}
			
	$scope.nextLogs = function() {
		$scope.nextLogsStarted = true;

		$scope.last = $scope.logs[$scope.logs.length - 1].id;
		
		$http.get('log/continue/' + $scope.last).success(function(data) {
			if (data.length > 0) {
				var newLogs = [];
				for (var i=0; i < $scope.logs.length; i++) {
					newLogs.push($scope.logs[i]);
				}
				for (var i=0; i < data.length; i++) {
					newLogs.push(data[i]);
				}
				if (newLogs.length > $scope.size) {
					newLogs.splice 
				}
				
				$scope.logs = newLogs.length <= $scope.size ? newLogs : newLogs.slice(-$scope.size);
			}
			if ($scope.prepaused) {
				$scope.paused = true;
			} else {
				$timeout($scope.nextLogs, $scope.interval);
			}
		}).error(function() {
			show_notification("error","An error occurred while fetching logs");
			hideSpinner();
		});
	}
	
	$scope.setTmpParams = function() {
		$scope.tmpInterval = $scope.interval;
		$scope.tmpSize = $scope.size;
	};
	
	$scope.updateParams = function(tmpInterval, tmpSize) {
		$scope.interval = tmpInterval;
		$scope.size = tmpSize;
	};
	
	$scope.updateLogLevel = function(currentResource, currentLevel) {
		showSpinner();
		$http.get('log/level/' + currentLevel + "/" + currentResource).success(function(data) {
			show_notification("info","Log level updated");
			hideSpinner();
		}).error(function(err) {
			show_notification("error","An error occurred updating log level:\n" + err);
			hideSpinner();
		});
	}
	
	$scope.resizeMainElement = function(elem) {
		var height = 0;
		var body = window.document.body;
		if (window.innerHeight) {
			height = window.innerHeight;
		} else if (body.parentElement.clientHeight) {
			height = body.parentElement.clientHeight;
		} else if (body && body.clientHeight) {
			height = body.clientHeight;
		}
		elem.style.height = ((height - elem.offsetTop - 100) + "px");
	}
	
	$scope.resizeMainElement(document.getElementById('divLogs'))
	$scope.loadLogs();
});


window.onresize = function() {
	var elem = document.getElementById('divLogs');
    angular.element(elem).scope().resizeMainElement(elem);
//    elem.scrollTop = elem.scrollHeight;
};