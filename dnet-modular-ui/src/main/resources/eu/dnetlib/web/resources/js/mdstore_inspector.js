var module = angular.module('indexInspector', ['ngGrid', 'ngRoute', 'mdstoreInspectorControllers']);

module.config([
	'$routeProvider',
	function($routeProvider) {
		$routeProvider
			.when('/list',		{ templateUrl: '../resources/html/mdstore/list.html', controller: 'mdstoresController' })
			.when('/inspect.do/:id',	{ templateUrl: '../resources/html/mdstore/inspect.html', controller: 'inspectMdstoresController' })
			.otherwise({ redirectTo: '/list' });
	}
]);


module.controller('moduleMenuCtrl', [ '$scope', '$location',  
	function ($scope, $location) {
		$scope.isActive = function(regex) {
			var re = new RegExp(regex);			
			return re.test($location.path());
		}
	}
]);

module.filter('encodeURIComponent', function() {
    return window.encodeURIComponent;
});