var module = angular.module('workflowsUI', ['ngGrid']);

addCommonDirectives(module);

function workflowsCtrl($scope, $http, $sce) {

	commonInitialize($scope, $http, $sce);
	
	wf_init($scope, $http, $sce);
	
	$scope.getMetaWorkflow($scope.initialMetaWf);
}
