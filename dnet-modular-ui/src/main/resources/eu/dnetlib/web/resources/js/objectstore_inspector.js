var module = angular.module('objectStoreInspector', ['ngGrid', 'ngRoute', 'objectStoresControllers']);

module.config([
    '$routeProvider',
    function ($routeProvider) {
        $routeProvider
            .when('/list', {
                templateUrl: '../resources/html/objectStore/list.html',
                controller: 'objectStoresController'
            })
            .when('/inspect.do/:id', {
                templateUrl: '../resources/html/objectStore/inspect.html',
                controller: 'inspectObjectStoreController'
            }).when('/inspect.do/:id/:start/:query', {
                    templateUrl: '../resources/html/objectStore/inspect.html',
                    controller: 'inspectObjectStoreController'
                }).when('/inspect.do/:id/:start', {
                templateUrl: '../resources/html/objectStore/inspect.html',
                controller: 'inspectObjectStoreController'
            })
            .otherwise({redirectTo: '/list'});
    }
]);


module.controller('moduleMenuCtrl', ['$scope', '$location',
    function ($scope, $location) {
        $scope.isActive = function (regex) {
            var re = new RegExp(regex);
            return re.test($location.path());
        }
    }
]);

module.filter('encodeURIComponent', function () {
    return window.encodeURIComponent;
});
