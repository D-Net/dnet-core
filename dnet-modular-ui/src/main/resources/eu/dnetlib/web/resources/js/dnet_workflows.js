var module = angular.module('workflowsUI', ['ngGrid']);

addCommonDirectives(module);

function workflowsCtrl($scope, $http, $sce) {
	$scope.currentSection = getSection();
	$scope.initialWf = getInitialWf();
	$scope.initialMetaWf = getInitialMetaWf();
	
	commonInitialize($scope, $http, $sce);
	
	wf_init($scope, $http, $sce);

	$scope.cloneMetaWf = function(id, name) {
		$scope.showSpinner();
		$http.get('clone_metaworkflow.do?id=' + id +'&name=' + name)
        .success(
            function(data) {
            	$scope.hideSpinner();
				$scope.reload(false);
				$scope.getMetaWorkflow(data);
            }
        ).error(
            function(err) {
            	$scope.showError(err.message);
            	$scope.hideSpinner();
            }
        );
	}	
	
	$scope.reload = function(loadFirstElement) {
		if ($scope.currentSection) { $scope.getWorkflows(loadFirstElement, 'section=' + $scope.currentSection) }
		else                       { $scope.hideSpinner(); }
	}
	
	$scope.reload(true);
}
