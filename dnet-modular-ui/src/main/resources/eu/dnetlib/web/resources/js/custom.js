// Pnotify notifications ~ Andrea Mannocci
$.pnotify.defaults.history = false; //disable top-right history menu
$.pnotify.defaults.styling = "bootstrap3";

function show_notification(type, message) {
    var opts = {
        title: "Over Here",
        text: "Check me out.",
        addclass: "stack-bar-bottom",
        cornerclass: "",
//        width: "70%",
    };
    switch (type) {
    case 'error':
        opts.title = "Error";
        opts.type = "error";
        break;
    case 'info':
        opts.title = "Info";
        opts.type = "info";
        break;
    case 'success':
        opts.title = "Success";
        opts.type = "success";
        break;
    }
    opts.text = message;

    $.pnotify(opts);
}

// Show/hide Pnotify permanent notification ~ Andrea Mannocci
var permanotice;

function showPermanotice(message) {
	if (permanotice == null) {
		permanotice = $.pnotify({
			title : 'Warning!',
			text : message,
			nonblock : true,
			hide : false,
			closer : false,
			sticker : false,
			history: false
		});
	}
}

function hidePermanotice() {
	if (permanotice) {
		permanotice.pnotify_remove();
		permanotice = null;
	}
}

function probePermanotice() {
	return (permanotice)?true:false;
}

// Spinner show/hide methods ~ Andrea Mannocci
var spinnerOpts;
var spinnerTarget;
var spinner;

function initSpinner() {
	spinnerOpts = {
			lines: 15,
			length: 16,
			width: 5,
			radius: 25,
			color: '#eeeeee',
			className: 'spinner',
			top: '40%'
		};
	spinnerTarget = document.getElementById('spinnerdiv');
}

function showSpinner() {
	spinner = new Spinner(spinnerOpts).spin(spinnerTarget);
	spinnerTarget.style.visibility = 'visible';
}

function hideSpinner()  {
	spinnerTarget.style.visibility = 'hidden';
	spinner.stop();
}