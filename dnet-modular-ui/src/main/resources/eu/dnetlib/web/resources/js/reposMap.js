var module = angular.module('reposMapUI', []);

function reposMapCtrl($scope, $http, $sce, $location) {
	
	common_init($scope, $http, $sce, $location);
	
	$scope.mapHeight = "400px";
	
	$scope.updateReposMap = function() {
		$scope.showSpinner();
		$http.get('listRepositories.map')
        .success(
            function(data) {
            	$scope.redrawReposMap(data);
            	$scope.hideSpinner();
            }
        ).error(
            function() {
            	$scope.showError('Something really bad must have happened to our fellow hamster..');
            	$scope.hideSpinner();
            }
        );
	}	

	$scope.redrawReposMap = function(entries) {
		
		$scope.resizeMainElement(document.getElementById('map_canvas_container'));
		
		var map = new google.maps.Map(document.getElementById("map_canvas"), {
			zoom: 4,
			center: new google.maps.LatLng(44, 6),
			disableDefaultUI: true,
		    zoomControl: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});
		
		angular.forEach(entries, function(repo) {
	
			var marker = new google.maps.Marker({
				map: map,
				position: new google.maps.LatLng(repo.lat, repo.lng),
				animation: google.maps.Animation.DROP,
				title: repo.name
			});
			
			google.maps.event.addListener(marker, 'click', function() {
				$scope.showDetails(repo.id, repo.name);
			});
		});
	}
	
	$scope.updateReposMap();
}

window.onresize = function() {
	var elem = document.getElementById('map_canvas_container');
    angular.element(elem).scope().resizeMainElement(elem);
};

