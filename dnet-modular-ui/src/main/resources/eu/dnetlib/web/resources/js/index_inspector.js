var module = angular.module('indexInspector', ['ngRoute', 'indexInspectorControllers']);

module.config([
	'$routeProvider',
	function($routeProvider) {
		$routeProvider
			.when('/query',		                                    { templateUrl: '../resources/html/index/indexQuery.html', controller: 'indexQueryController' })
			.when('/query/:mdformat/:backend/:index/:start/:query', { templateUrl: '../resources/html/index/indexQuery.html', controller: 'indexQueryController' })
			.when('/browse',	{ templateUrl: '../resources/html/index/browse.html', controller: 'indexBrowseController' })
			.when('/manage',	{ templateUrl: '../resources/html/index/indexManage.html', controller: 'indexmanageController' })
			.when('/deletebyquery',	{ templateUrl: '../resources/html/index/deletebyquery.html', controller: 'indexdbqController' })			
			.otherwise({ redirectTo: '/query' });
	}
]);


module.controller('moduleMenuCtrl', [ '$scope', '$location',  
	function ($scope, $location) {
		$scope.isActive = function(regex) {
			var re = new RegExp(regex);			
			return re.test($location.path());
		}
	}
]);

module.filter('encodeURIComponent', function() {
    return window.encodeURIComponent;
});