var module = angular.module('reposUI', ['ngGrid', 'ngRoute', 'repoControllers']);

addCommonDirectives(module);

module.config([
	'$routeProvider',
	function($routeProvider) {
		$routeProvider
		.when('/browse',                           { templateUrl: '../resources/html/repoBrowse.html', controller: 'repoBrowseCtrl' })
		.when('/list/:param/:value',               { templateUrl: '../resources/html/repoList.html',   controller: 'repoListCtrl'   })
		.when('/api/:repoId/:ifaceId/:metawf/:wf', { templateUrl: './repos/repoApi.html',    controller: 'repoApiCtrl'    })
		.otherwise({ redirectTo: '/browse' });
	}
]);

module.directive('ngApiMdstoreInfo', function() {
	return {
		restrict: 'E',
		scope: {
			'label' : '@',
			'date'  : '=',
			'total' : '=',
			'id'    : '='
		},
		templateUrl: '../resources/html/ngApiMdstoreInfo.html'
	}
});

module.directive('ngApiObjectstoreInfo', function() {
    return {
        restrict: 'E',
        scope: {
            'label' : '@',
            'date'  : '=',
            'total' : '=',
            'id'    : '='
        },
        templateUrl: '../resources/html/ngApiObjectstoreInfo.html'
    }
});

