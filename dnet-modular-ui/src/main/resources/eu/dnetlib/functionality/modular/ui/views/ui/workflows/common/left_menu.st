<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
	<div ng-show="metaworkflows">
		<label>{{currentSection}}</label>
		<ul class="nav nav-pills nav-stacked">
			<li ng-class="{ active: wf.wfId == currentMetaWorkflow.wfId }" ng-repeat="wf in metaworkflows">
				<a href="javascript:void(0)" ng-click="getMetaWorkflow(wf.wfId)">
					<span class="glyphicon glyphicon-chevron-right pull-right"></span>
					{{wf.name}}
				</a>
			</li>
		</ul>
	</div>
	
	<div ng-hide="metaworkflows">
		<label>(No Metaworkflows)</label>
	</div>
</div>
