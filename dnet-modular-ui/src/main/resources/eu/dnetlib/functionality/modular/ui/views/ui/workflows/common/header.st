<script type="text/javascript" src="../resources/js/angular.min.js" ></script>
<script type="text/javascript" src="../resources/js/dnet_workflows_common.js"></script>
<script type="text/javascript" src="../resources/js/ng-grid-2.0.7.min.js"></script>
<script type="text/javascript" src="../resources/js/dnet_param_values_functions.js"></script>

<link rel="stylesheet" type="text/css" href="../resources/css/ng-grid.min.css" />

<style>
	.gridStyle {
		width: 100%; 
		height: 150px;
		margin-bottom: 20px;
		border: 1px solid lightgrey;
	}
	
	.gridStyle-big {
		width: 100%; 
		height: 300px;
		margin-bottom: 20px;
		font-size: 11px;
		border: 1px solid lightgrey;
	}
	
	.wf_missing * { color: #696969; }
	.wf_warning * { color: #cc4444; }
		
	.modal-large {
		width: 80%;
		left: 10%;
		margin-left: auto;
		margin-right: auto;
		margin-top: 50px;
		margin-bottom: 50px;
	}
	
</style>
