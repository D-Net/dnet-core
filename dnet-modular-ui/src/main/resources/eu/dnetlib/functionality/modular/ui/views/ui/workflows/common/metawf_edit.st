<div id="metaWfEditModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{currentMetaWorkflow.name}}</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-5 control-label" for="inputMetaWfName">Meta Workflow name</label>
						<div class="col-sm-7">
							<input class="form-control" id="inputMetaWfName" type="text" required placeholder="[unknown]" ng-model="tempMetaWorkflow.name" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-5 control-label" for="inputMetaWfName">Email notification</label>
						<div class="col-sm-7">
							<input class="form-control" id="inputMetaWfName" type="text" required placeholder="email address" ng-model="tempMetaWorkflow.email" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-5 control-label" for="inputEnableScheduling">Enable scheduling</label>
						<div class="col-sm-7">
							<input type="checkbox" id="inputEnableScheduling" ng-model="tempMetaWorkflow.scheduled" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-5 control-label" for="inputCronExpression">Cron expression</label>
						<div class="col-sm-7">
							<input class="form-control" id="inputCronExpression" type="text" required placeholder="0 0 0 * * ?" ng-model="tempMetaWorkflow.cronExpression" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-5 control-label" for="inputMinInterval">Minimum interval (in minutes)</label>
						<div class="col-sm-7">
							<input class="form-control" id="inputMinInterval" type="text" required placeholder="0" ng-model="tempMetaWorkflow.minInterval" />
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal">Close</button>
				<button class="btn btn-primary" ng-click="saveCurrentMetaWf()" data-dismiss="modal">Save changes</button>
			</div>
		</div>
	</div>
</div>	