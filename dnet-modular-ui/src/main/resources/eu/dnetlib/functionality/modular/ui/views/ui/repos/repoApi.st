<div class="row">
	<div class="col-xs-12 col-md-6">
			<button class="btn btn-sm btn-primary btnRefreshApi" type="button" data-toggle="modal" data-target="#metaWfSelectionModal"><span class="glyphicon glyphicon-plus"></span> add metaworkflow</button>
			<button class="btn btn-sm btn-danger" type="button" ng-click="removeApi()" ng-if="currentRepoApi.removable">Delete API</button>
	</div>
	<div class="col-xs-12 col-md-6 text-right">
		<button class="btn btn-sm btn-default btnRefreshApi" type="button" ng-click="loadApi()"><span class="glyphicon glyphicon-refresh"></span></button>
		<button class="btn btn-sm btn-default" type="button" onclick="history.go(-1);">back</button>
	</div>
</div>


<div class="well" style="margin-top: 20px; margin-bottom: 20px">
	<div class="row">
		<div class="input-group input-group-sm col-xs-12" style="padding: 10px; float: left;">
	  		<span class="input-group-addon" style="width: 150px;"><b>Datasource</b></span>
			<span class="form-control">
				{{currentRepoApi.repoName}}
				<span class="pull-right" ng-show="currentRepoApi.repoCountry">
					<img ng-src="../resources/img/flags/{{currentRepoApi.repoCountry}}.gif" /> 
					{{currentRepoApi.repoCountry}}
				</span>
			</span>
			<span class="input-group-btn">
				<a class="btn btn-primary" href="isManager.do#/profile/{{currentRepoApi.repoId}}"><span class="glyphicon glyphicon-link"></span></a>
			</span>
			<span class="input-group-btn">
				<button type="button" class="btn btn-primary" ng-click="showDetails(currentRepoApi.repoId, currentRepoApi.repoName)"><span class="glyphicon glyphicon-info-sign"></span></button>
			</span>
		</div>
		
		<div class="input-group input-group-sm col-xs-12" style="padding: 10px; float: left;">
	  		<span class="input-group-addon" style="width: 150px;"><b>Datasource details</b></span>
			<span class="form-control"><b>contact email:</b> <a href="mailto:{{currentRepoApi.email}}" ng-show="currentRepoApi.email">{{currentRepoApi.email}}</a></span>
			<span class="form-control"><b>software typology:</b> {{currentRepoApi.repoType}}</span>
		</div>
		
		<div class="input-group input-group-sm col-xs-12" style="padding: 10px; float: left;" ng-show="validatorDetailsAddress || validatorBlacklistAddress">
	  		<span class="input-group-addon" style="width: 150px;"><b>Validation</b></span>
			<span class="form-control" ng-show="validatorDetailsAddress"><a href="{{validatorDetailsAddress}}">show last validation details</a></span>
			<span class="form-control" ng-show="validatorBlacklistAddress"><a href="{{validatorBlacklistAddress}}">show blacklist</a></span>
		</div>
		
		<div class="input-group input-group-sm col-xs-12 col-md-6" style="padding: 10px; float: left;">
			<span class="input-group-addon" style="width: 150px;"><b>API details</b></span>
			<span class="form-control" ng-repeat="p in currentRepoApi.commonParams" ng-hide="p.name == 'compliance'" style="white-space: nowrap;"><b>{{p.name}}:</b> {{p.value}}</span>
			<span class="form-control"><b>ns prefix:</b> {{currentRepoApi.repoPrefix}}</span>
			<span class="form-control" ng-show="currentRepoApi.otherParams.length > 0"><a href="javascript:void(0)" data-toggle="modal" data-target="#showOtherParamsModal">other details...</a></span>
		</div>
				
		<div class="input-group input-group-sm col-xs-12 col-md-6" style="padding: 10px; float: left;">
	  		<span class="input-group-addon" style="width: 150px;"><b>Protocol</b></span>
			<div class="form-control">
				{{currentRepoApi.protocol}}
				<span class="pull-right" ng-show="currentRepoApi.protocol == 'oai'">
					<a href="javascript:void(0)" ng-click="testOAI(currentRepoApi.accessParams)">verify</a>				
				</span>
			</div>
			<span class="input-group-btn">
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editAccessParamsModal" ng-click="prepareUpdateRepoApi()">access params</button>
			</span>
		</div>
		
		<div class="input-group input-group-sm col-xs-12 col-md-6" style="padding: 10px; float: left;" ng-repeat="p in currentRepoApi.commonParams" ng-show="p.name == 'compliance'">
			<span class="input-group-addon" style="width: 150px;"><b>Compliance</b></span>
			<span class="form-control" ng-show="p.otherValue" style="color: #c06a00">{{p.value}} (override)</span>
			<span class="form-control" ng-hide="p.otherValue">{{p.value}} (by validator)</span>
		
			<div class="input-group-btn">
				<button type="button" data-toggle="dropdown" class="btn btn-sm dropdown-toggle" ng-class="{ 'btn-primary' : !p.otherValue, 'btn-warning' : p.otherValue }">
					<span class="caret pull-right"></span>
				</button>
			    <ul class="dropdown-menu dropdown-menu-right" role="menu">
			        <li role="presentation" ng-repeat="l in compatibilityLevels">
			        	<a role="menuitem" tabindex="-1" href="javascript:void(0)" ng-click="overrideApiCompliance(l.name)">override to <b>{{l.name}}</b> (<i>{{l.desc}}</i>)</a>
			        </li>
					<li class="divider" role="presentation" ng-show="p.otherValue"></li>
			        <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" ng-show="p.otherValue" ng-click="resetApiCompliance()">reset to the original value</b></a></li>
			    </ul>
			</div>
		</div>
			
		<div class="input-group input-group-sm col-xs-12 col-md-6" style="padding: 10px; float: left;">
	  		<span class="input-group-addon" style="width: 150px;"><b>Last aggregation</b></span>
	  		<ng-api-mdstore-info label="Collect" date="currentRepoApi.collDate" total="currentRepoApi.collTotal" id="currentRepoApi.collMdId"></ng-api-mdstore-info>	  		
	  		<ng-api-mdstore-info label="Transform" date="currentRepoApi.aggrDate" total="currentRepoApi.aggrTotal" id="currentRepoApi.aggrMdId"></ng-api-mdstore-info>
	  		<ng-api-objectstore-info label="Download" date="currentRepoApi.downloadDate" total="currentRepoApi.downloadTotal" id="currentRepoApi.downloadObjId"></ng-api-objectstore-info>
		</div>		
		
	</div>
</div>
	
<div ng-hide="metaworkflows.length != 0" class="text-center" style="margin-top: 40px">
	<button class="btn btn-lg btn-primary btnRefreshApi" type="button" data-toggle="modal" data-target="#metaWfSelectionModal"><span class="glyphicon glyphicon-plus"></span> add first metaworkflow</button>
</div>

<div class="panel-group" id="accordion" ng-show="metaworkflows.length != 0">
	<div class="panel panel-default" ng-repeat="m in metaworkflows" style="overflow: visible;">
		<div class="panel-heading">
			<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" data-target="#collapse_{{m.id | limitTo: 36}}" ng-click="getMetaWorkflow(m.id)">
				<b>{{m.name}}</b>
			</a>
		</div>
		
		<div id="collapse_{{m.id | limitTo: 36}}" class="panel-collapse collapse out">
			<div class="panel-body" ng-show="m.deleting">
				<span class="muted" ng-show="m.deleting">deleting...</span>
			</div>

			<table class="table" id="metaWfTable" ng-hide="m.deleting">
				<thead>
					<tr>
						<th>Aggregation step</th>
						<th class="col-xs-2" style="white-space: nowrap;">Last execution date</th>
						<th class="col-xs-1 text-center">Last status</th>
						<th class="col-xs-1 text-center">Launch mode</th>
						<th class="col-xs-4 col-md-3 text-right">Actions</th>
					</tr>
				</thead>
				<tbody ng-bind-html="to_trusted(currentMetaWorkflow.html)" compile-template></tbody>
			</table>

			<div class="panel-footer" ng-hide="m.deleting">
				<form class="form-inline">
					<a href="isManager.do#/profile/{{currentMetaWorkflow.wfId}}" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-link"></span></a>
					<div class="btn-group">
						<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#metaWfEditModal" ng-click="prepareMetaWfEdit()">configure</button>
						<button type="button" class="btn btn-default btn-sm" data-toggle="modal" ng-click="updateMetaWorkflowHistory()">history</button>
					</div>
					<button type="button" class="btn btn-danger btn-sm btnRefreshApi" ng-show="m.destroyWorkflow && !m.deleting" ng-click="destroyDataProviderWorkflow(m)">
						<span class="glyphicon glyphicon-remove"></span> delete
					</button>
				</form>
			</div>
		</div>
	</div>
</div>

<div id="metaWfSelectionModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Available MetaWorkflows for current API</h4>
			</div>
			<div class="modal-body">
				<div class="panel panel-default" ng-repeat="e in availableRepohiWfs" ng-show="verifyMetaWf(currentRepoApi, e)">
					<div class="panel-heading">
						<a href="javascript:void(0)" data-dismiss="modal" ng-click="newDataProviderWorkflow(e.id)">{{e.name}}</a>
					</div>
					<div class="panel-body">
						<span ng-repeat="f in e.fields">
							<strong >{{f.k}}: </strong>{{f.v}}<br />
						</span>
						<strong>Typology prefixes: </strong>{{e.ifaceTypes.join()}}
						<br />
						<strong>Compliance prefixes: </strong>{{e.compliances.join()}}
						
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="editAccessParamsModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Access params (protocol: {{currentRepoApi.protocol}})</h4>
			</div>
			<div class="modal-body">
				<div class="well">
					<form class="form-horizontal" role="form">
						<div class="form-group" ng-repeat="p in tempAccessParams">
							<label for="input_ap_{{p.name}}" class="col-sm-3 control-label">{{p.name}}</label>
							<div class="col-sm-9" ng-hide="currentRepoApi.protocol == 'oai' && p.name == 'set'">
								<input type="text" class="form-control" id="input_ap_{{p.name}}" ng-model="p.value"/>
							</div>
							<div class="col-sm-9" ng-show="currentRepoApi.protocol == 'oai' && p.name == 'set'"> 
								<select class="form-control" style="font-size: 11px" size="10" multiple ng-model="selectedSets" ng-change="updateSets(selectedSets, tempAccessParams)">
									<option ng-repeat="set in currentSets" value="{{set.id}}" ng-selected="set.selected">{{set.name}}</option>
								</select>
								<span><b>Current set(s): </b>{{selectedSets.join(', ')}}</span>
								<button class="btn btn-xs btn-default pull-right" style="margin-top: 4px" ng-click="listSets(tempAccessParams)"><span class="glyphicon glyphicon-refresh"></span> refresh</button>
							</div>
  						</div>
  						<div class="form-group">
							<label for="input_mdid_path" class="col-sm-3 control-label">ID XPath</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="input_mdid_path" ng-model="tempMdIdPath"/>
							</div>
  						</div>
 					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="updateRepoApi()">Save</button>
			</div>
		</div>
	</div>
</div>

<div id="showOtherParamsModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Other parameters</h4>
			</div>
			<div class="modal-body">
				<div class="well">
					<table class="table table-bordered table-condensed">
						<thead>
							<th>
								<span>Name</span>
								<span class="pull-right">Value</span>
							</th>
						</thead>
						<tbody>
							<tr ng-repeat="p in currentRepoApi.otherParams">
								<td>
									<b>{{p.name}}</b>
									<span class="pull-right" ng-if="p.value">{{p.value}}</span>
									<span class="pull-right text-muted" ng-if="!p.value"><i>missing value</i></span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


$ui/repos/repoDetails()$
$ui/workflows/common/atomwf()$
$ui/workflows/common/metawf_edit()$		
$ui/workflows/common/atomwf_edit()$
$ui/workflows/common/wf_monitor()$
$ui/workflows/common/wf_monitor_proc( backFunction={updateMetaWorkflowHistory()} )$
$ui/workflows/common/wf_monitor_proc_node()$
$ui/workflows/common/wf_journal_entry()$
