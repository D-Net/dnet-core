$common/master( header={
	<script type="text/javascript" src="../resources/js/angular.min.js" ></script>
	<script type="text/javascript" src="../resources/js/angular-route.min.js"></script>
	<script type="text/javascript" src="../resources/js/angular-local-storage.min.js"></script>
	
	<script type="text/javascript" src="../resources/js/index_inspector_controllers.js"></script>
	<script type="text/javascript" src="../resources/js/index_inspector.js"></script>
}, body={
	<div ng-app="indexInspector" class="row">
		<div class="col-sm-3 col-lg-2" ng-controller="moduleMenuCtrl">
			<ul class="nav nav-pills nav-stacked">
				<li ng-class="{active : isActive('^\/q')}"><a href="#/query">Query</a></li>
				<li ng-class="{active : isActive('^\/(schemas|schema)')}"><a href="#/deletebyquery">Delete By Query</a></li>
				<li ng-class="{active : isActive('^\/browse')}"><a href="#/browse">Browse</a></li>
				<li ng-class="{active : isActive('^\/register')}"><a href="#/manage">Manage Index DS</a></li>				
			</ul>
		</div>
		<div ng-view class="col-sm-9 col-lg-10"></div>
		<script type="text/javascript" src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js"></script>
		<script>
function reload(){
	alert("prima");
	prettyPrint();
	alert("dopo");
}

</script>
	</div>
} )$
