$common/master( 
	header={
		<script type="text/javascript" src="../resources/js/angular.min.js"></script>
		<script type="text/javascript" src="../resources/js/users.js"></script>
	},
	onLoad={}, 
	body={
		
		<div class="well" ng-app="usersUI" ng-controller="usersCtrl">
		
			<div>
				<form class="form-inline" role="form" ng-submit="addUser(newUser)">
					<div class="form-group">
						<label class="sr-only" for="userAdd">new user</label>
						<input id="userAdd" class="form-control" type="text" placeholder="new user" ng-model="newUser">
					</div>
					<button class="btn btn-default" type="submit">
						<span class="glyphicon glyphicon-plus"></span>
					</button>
					<button class="btn btn-default" ng-click="saveUsers()" type="button">
						<span class="glyphicon glyphicon-save"></span>
					</button>
				</form>
			</div>
			<br />
			<table class="table table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>User</th>
						$availableLevels:{l|<th class="text-center" title="$l.details$">$l.label$</th>}$
						<th>&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="user in users">
						<td>{{\$index + 1}}</td>
						<td><b>{{user.id}}</b></td>
						
						$availableLevels:{l|<td class="text-center"><input type="checkbox" ng-checked="user.permissionLevels.indexOf('$l.level$') >= 0" ng-click="updateUser(\$index, '$l.level$', \$event.target.checked)"></td>}$
						
						<td class="text-right">
							<button class="btn btn-danger btn-xs" ng-click="dropUser(\$index)" type="button">
								<span class="glyphicon glyphicon-minus"></span>
							</button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		
		
	}
)$
