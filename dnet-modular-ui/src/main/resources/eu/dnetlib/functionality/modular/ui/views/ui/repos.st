$common/master(

header={
	$ui/repos/header()$
	<script type="text/javascript" src="../resources/js/repoEnabler.js"></script>
	<script type="text/javascript">
		function getTypologies() { return $types$ }
	</script>
	
	<style>
		.popover { max-width: 450px;  width: auto; }
	</style>
},
onLoad={},
body={
	<div id="main" ng-app="repoEnablerUI" ng-controller="repoEnablerCtrl">
		$ui/repos/repoDetails()$
	
		<div class="row">
			<div class="col-sm-4 col-lg-3">
				<ul class="nav nav-pills nav-stacked">
					<li ng-class="{active : t.id == currType}"
						ng-repeat="t in types">
						<a href="javascript:void(0)" ng-click="selectType(t.name)">{{t.desc}}</a>
					</li>
				</ul>
			</div>
			<div class="col-sm-8 col-lg-9">
				<div class="well" ng-hide="repos.length > 0">
					<h5><i>No datasource</i></h5>
				</div>
						
				<div class="panel panel-default" ng-show="repos.length > 0">
					<div class="panel-heading">
						<form class="form-inline text-right" role="form">
							<div class="form-group">
								<label class="sr-only" for="repoFilter">Filter...</label>
 								<input type="text" class="form-control input-sm" id="repoFilter" ng-model="repoFilter.name" placeholder="Filter..." />
				  			</div>
							<button ng-click="selectType(currType)" class="btn btn-sm btn-default" type="button"><span class="glyphicon glyphicon-refresh"></span></button>
						</form>
					</div>
					<table class="table table-bordered table-striped">
						<tr ng-repeat="r in repos | filter: repoFilter">
							<td>
								<a href="isManager.do#/profile/{{r.id}}" class="btn btn-xs btn-primary" title="xml profile"><span class="glyphicon glyphicon-link"></span></a>
								<a href="javascript:void(0)" ng-click="showDetails(r.id, r.name)">
									{{r.name}}
								</a>
								<div ng-show="r.valid" class="pull-right">
									<a href="javascript:void(0)" ng-click="setValidation(r.id, false)">force invalidate</a> 
									/ 
									<span class="label label-success">valid</span>
								</div>
								<div ng-show="!r.valid" class="pull-right">
									<span class="label label-danger">pending</span>  
									/ 
									<a href="javascript:void(0)" ng-click="setValidation(r.id, true)">force validate</a>
								</div>
							</td>
						</tr>
					</table>
					<div class="panel-footer"></div>
				</div>
			</div>
		</div>
	</div>
})$ 
