$common/master( header={
	<script type="text/javascript" src="../resources/js/angular.min.js" ></script>
	<script type="text/javascript" src="../resources/js/angular-route.min.js"></script>
	<script type="text/javascript" src="../resources/js/angular-local-storage.min.js"></script>
	<script type="text/javascript" src="../resources/js/ng-grid-2.0.7.min.js"></script>
	<script type="text/javascript" src="../resources/js/objectstore_inspector_controllers.js"></script>
	<script type="text/javascript" src="../resources/js/objectstore_inspector.js"></script>
	<link rel="stylesheet" type="text/css" href="../resources/css/ng-grid.min.css" />
}, body={
	<div ng-app="objectStoreInspector" class="row">
		<div ng-view class="col-lg-12"></div>
	</div>
} )$
