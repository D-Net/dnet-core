$common/master( header={
	<script type="text/javascript" src="../resources/js/angular-1.4.5.min.js"></script>
	<script type="text/javascript" src="../resources/js/scrollglue.js"></script>
	<script type="text/javascript" src="../resources/js/dnetLog/dnetLog.js"></script>
}, onLoad={}, body={
	<div ng-app="dnetLogUI" ng-controller="dnetLogCtrl">
		<div class="row">
			<div class="col-xs-12 col-md-6" style="padding-top: 6px">
				<strong>Log File: </strong>
				$logFile$
			</div>
			<div class="col-xs-12 col-md-6">
				<form class="form-inline pull-right">
					<button ng-click="play()" class="btn btn-primary btn btn-sm" type="button" ng-show="paused">continue</button>
					<button ng-click="pause()" class="btn btn-primary btn btn-sm" type="button" ng-hide="prepaused">pause</button>
					<button class="btn btn-default btn btn-sm" type="button" data-target="#editLevelsModal" data-toggle="modal">modify level for class</button>
					<button ng-click="setTmpParams()" class="btn btn-default btn btn-sm" type="button" data-target="#editParamsModal" data-toggle="modal"><span class="glyphicon glyphicon-cog"></span></button>
					<button ng-click="loadLogs()" class="btn btn-default btn btn-sm" type="button"><span class="glyphicon glyphicon-refresh"></span></button>
				</form>
			</div>
		</div>
				
		<div class="well" style="margin-top: 20px;">
			<input type="text" class="form-control" ng-model="filterLog" placeholder="Filter..." ng-model-options="{ debounce: 500 }"  />
		
			<div scroll-glue id="divLogs" style="margin-top: 20px; height: 300px; overflow-y: scroll; border: 1px solid #dddddd">
				<table class="table table-bordered">
					<tr ng-repeat="l in logs | filter: filterLog">
						<td class="text-center" style="width: 80px;"><span class="label label-default" ng-class="{
							'label-info' : l.level == 'INFO',
							'label-warning' :  l.level == 'WARN',
							'label-danger' : l.level == 'ERROR' || l.level == 'FATAL',
							'label-primary' : l.level == 'DEBUG'  
						}">{{l.level}}</span></td>
						<td style="width: 220px; font-size: 12px; font-family: monospace; text-align: center; white-space: pre;" ng-bind-html="highlight(l.date, filterLog)"></td>
						<td style="width: 500px; font-size: 12px; font-weight: bolder;" ng-bind-html="highlight(l.name, filterLog)"></td>
						<td>
							<span style="font-size: 12px; font-family: monospace; unicode-bidi: embed; white-space: pre;" ng-bind-html="highlight(l.message, filterLog)"></span><br />
							<span class="text-warning" style="font-size: 12px; font-family: monospace; unicode-bidi: embed; white-space: pre;" ng-bind-html="highlight(l.stacktrace, filterLog)"></span>
						</td>
					</tr>
				</table>
				<img src="../resources/img/ajax-loader-mini.gif" style="margin-left: 20px; margin-bottom: 20px;" ng-hide="paused" />
			</div>
		</div>
		
		<div role="dialog" tabindex="-1" class="modal fade" id="editLevelsModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button data-dismiss="modal" class="close" type="button">&times;</button>
						<h4 class="modal-title">Update log level</h4>
					</div>
					<div class="modal-body">
						<form role="form" class="form-horizontal">
							<div class="form-group">
								<label for="inputForResource" class="col-sm-3 control-label">Class / package</label>
								<div class="col-sm-9">
									<input type="text" ng-model="currentResource" placeholder="eu.dnetlib.enabling" required="required" id="inputForResource" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label for="inputForLevel" class="col-sm-3 control-label">New level</label>
								<div class="col-sm-9">
									<select class="form-control" ng-model="currentLevel" required="required" id="inputForLevel">
										<option value="OFF">OFF</option>
										<option value="FATAL">FATAL</option>
										<option value="ERROR">ERROR</option>
										<option value="WARN">WARN</option>
										<option value="INFO">INFO</option>
										<option value="DEBUG">DEBUG</option>
									</select>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button data-dismiss="modal" class="btn btn-default">Close</button>
						<button data-dismiss="modal" ng-click="updateLogLevel(currentResource, currentLevel)" class="btn btn-primary" ng-disabled="!currentResource || !currentLevel">Update</button>
					</div>
				</div>
			</div>
		</div>
		
		<div role="dialog" tabindex="-1" class="modal fade" id="editParamsModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button data-dismiss="modal" class="close" type="button">&times;</button>
						<h4 class="modal-title">Update log level</h4>
					</div>
					<div class="modal-body">
						<form role="form" class="form-horizontal">
							<div class="form-group" style="margin-left: 20px">
								<label for="inputInterval" class="col-sm-5 control-label">Refresh interval (ms)</label>
								<div class="col-sm-7">
									<input type="number" min="500" max="5000" class="form-control" id="inputInterval" ng-model="tmpInterval" />
								</div>
							</div>
							<div class="form-group" style="margin-left: 20px">
								<label for="inputSize" class="col-sm-5 control-label">Number of lines</label>
								<div class="col-sm-7">
									<input type="number" min="1" max="2000" class="form-control" id="inputSize" ng-model="tmpSize" />
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button data-dismiss="modal" class="btn btn-default">Close</button>
						<button data-dismiss="modal" ng-click="updateParams(tmpInterval, tmpSize)" class="btn btn-primary" ng-disabled="!tmpInterval || !tmpSize">Update</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
})$
