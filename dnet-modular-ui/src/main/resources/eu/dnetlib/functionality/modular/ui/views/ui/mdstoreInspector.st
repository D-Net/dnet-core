$common/master( header={
	<script type="text/javascript" src="../resources/js/angular.min.js" ></script>
	<script type="text/javascript" src="../resources/js/angular-route.min.js"></script>
	<script type="text/javascript" src="../resources/js/angular-local-storage.min.js"></script>
	<script type="text/javascript" src="../resources/js/ng-grid-2.0.7.min.js"></script>
	<script type="text/javascript" src="../resources/js/mdstore_inspector_controllers.js"></script>
	<script type="text/javascript" src="../resources/js/mdstore_inspector.js"></script>
	<link rel="stylesheet" type="text/css" href="../resources/css/ng-grid.min.css" />
}, body={
	<div ng-app="indexInspector" class="row">
	<!--
		<div class="col-sm-3 col-lg-2" ng-controller="moduleMenuCtrl">
			<ul class="nav nav-pills nav-stacked">
				<li ng-class="{active : isActive('^\/q')}"><a href="#/list">MdStores</a></li>
				<li ng-class="{active : isActive('^\/q')}"><a href="#/list">MdStores</a></li>								
			</ul>
		</div>
	-->


		<div ng-view class="col-xs-12"></div>
	</div>
} )$
