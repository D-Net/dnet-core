<div id="atomWfModal" class="modal fade" tabindex="-1" role="dialog" style="z-index: 2000;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><b>Workflow:</b> <i>{{currentWorkflow.name}}</i></h4>
			</div>
			<div class="modal-body text-center" style="height: 600px; overflow-y: auto; overflow-x: auto;">
				<div ng-hide="currentNode">
					<h5>Click on the nodes to inspect parameters</h5>
					<map name="processAtomicWorkflowMap" id="processAtomicWorkflowMap" ng-bind-html="to_trusted(currentWorkflow.mapContent)"></map>
					<img ng-src="{{currentWorkflow.imageUrl}}" usemap="#processAtomicWorkflowMap"/>
				</div>
				<div ng-show="currentNode">
					<h5>
						<b>Node:</b> <i>{{currentNode.name}}</i>
						<small ng-show="currentNode.description"><br /><b>Description:</b> <i>{{currentNode.description}}</i></small>
					</h5>
					<div ng-hide="currentNode.params.length > 0" style="margin-top: 20px;">
						<i>No parameters</i>
					</div>
					<div class="text-left" style="margin-top: 20px;">
						<div ng-repeat="p in currentNode.params" style="border-top: 1px solid #dadada; display: inline-block; padding: 4px; width: 100%;">
							<div style="display: inline-block;"><b>{{p.name}}</b></div>
							<div class="pull-right">
								<div class="text-success text-right" ng-show="!p.required || (p.value.length > 0 && verifyType(p.value, p.type))">
									{{p.value}}
									<span class="glyphicon glyphicon-ok pull-right" style="margin-left: 8px" ng-class="{'glyphicon-ok' : p.userParam, 'glyphicon-cog' : !p.userParam}"></span> 
								</div>
								<div class="text-danger text-right" ng-show="p.value.length == 0 && p.required">
									<i>Missing value</i>
									<span class="glyphicon glyphicon-remove pull-right" style="margin-left: 8px"></span> 
								</div>
								<div class="text-danger text-right" ng-show="p.value.length > 0 && !verifyType(p.value, p.type)">
									<i>invalid type for <b>{{p.value}}</b>, required is <b>{{p.type}}</b></i>
									<span class="glyphicon glyphicon-remove pull-right" style="margin-left: 8px"></span> 
								</div>
							</div>
							<div ng-show="p.value" class="text-center">
								<a href="{{ui.url | sprintf: p.value}}" ng-repeat="ui in p.uis" ng-show="ui.paramRequired" style="padding: 0px 10px">[{{ui.label}}]</a>
							</div>
						</div>
					</div>
					<button class="btn btn-default btn-sm" style="margin: 30px" ng-click="currentNode = null">back</button>
				</div>
			</div>
			
			<div class="modal-footer">
				<a href="isManager.do#/profile/{{currentWorkflow.wfId}}" class="btn btn-default"><span class="glyphicon glyphicon-link"></span></a>
				<button class="btn btn-default" data-dismiss="modal">Close</button>
				<button class="btn btn-primary" data-dismiss="modal" ng-show="currentWorkflow.ready" ng-click="executeWf(currentWorkflow.wfId)">Launch</button>
			</div>
			
		</div>
	</div>
</div>
