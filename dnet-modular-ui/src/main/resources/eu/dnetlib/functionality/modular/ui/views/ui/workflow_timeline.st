$common/master( header={

	$ui/workflows/common/header()$
	<script type="text/javascript" src="../resources/js/dnet_wf_timeline.js"></script>
	<script type="text/javascript" src="http://www.google.com/jsapi"></script>
	<script type="text/javascript" src="../resources/js/timeline.js"></script>
	<script type="text/javascript" src="../resources/js/ui-bootstrap.js"></script>
	
	<link rel="stylesheet" type="text/css" href="../resources/css/timeline.css" />
	
	<style>  
		#wfJournalTable {
			width: 100%; 
			height: 500px;
			margin-bottom: 20px;
			font-size: 11px;
		}
		
		#wfLogDetails {
			width: 100%; 
			height: 300px;
			margin-bottom: 20px;
			font-size: 11px;
		}
	</style>

	<script type="text/javascript">
		google.load('visualization', '1');
	</script>

}, body={
	<div ng-app="wfTimelineUI" ng-controller="wfTimelineCtrl">

		$ui/workflows/common/wf_monitor_proc()$
		$ui/workflows/common/wf_monitor_proc_node()$
		$ui/workflows/common/wf_journal_entry()$
	
	
		<div class="row">
			<div class="col-xs-12 col-md-6">
				Showing workflows from <strong>{{formatDate(startDate)}}</strong> to <strong>{{formatDate(endDate)}}</strong>
			</div>
			<div class="col-xs-12 col-md-6">
				<form class="form-inline pull-right" role="form">
					<input class="form-control input-sm" type="text" ng-model="filterText" ng-change="drawVisualization(data)" placeholder="Filter...">
					<button ng-click="refresh()" class="btn	btn-default btn-sm" type="button"><span class="glyphicon glyphicon-refresh"></span></button>
				</form>
			</div>
		</div>
		<br/>
		<div class="row">
			<div id="divTimeline" class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>
		</div>
	</div>

} )$
