$common/master( header={
	<script type="text/javascript" src="../resources/js/angular.min.js" ></script>
	<script type="text/javascript" src="../resources/js/angular-route.min.js"></script>
	<script type="text/javascript" src="../resources/js/angular-local-storage.min.js"></script>
	<script type="text/javascript" src="../resources/js/is_manager.js"></script>
	<script type="text/javascript" src="../resources/js/is_manager_controllers.js"></script>
}, body={
	<div ng-app="isManager" class="row">
		<div class="col-sm-3 col-lg-2" ng-controller="moduleMenuCtrl">
			<ul class="nav nav-pills nav-stacked">
				<li ng-class="{active : isActive('^\/(list|profile|profiles)')}"><a href="#/list">Profiles</a></li>
				<li ng-class="{active : isActive('^\/(schemas|schema)')}"><a href="#/schemas">Schemas</a></li>
				<li ng-class="{active : isActive('^\/q')}"><a href="#/q">XQuery</a></li>
				<li ng-class="{active : isActive('^\/services')}"><a href="#/services">Registered services</a></li>
				<li ng-class="{active : isActive('^\/register')}"><a href="#/register">Register new profile</a></li>
				<li ng-class="{active : isActive('^\/bulk')}"><a href="#/bulk">Bulk importer</a></li>
				<li ng-class="{active : isActive('^\/verify')}"><a href="#/verify">Validation checker</a></li>
				<li ng-class="{active : isActive('^\/blackboard')}"><a href="#/blackboard">Blackboard messages</a></li>
				<li ng-class="{active : isActive('^\/backup')}"><a href="#/backup">Backup</a></li>
			</ul>
		</div>
		<div ng-view class="col-sm-9 col-lg-10"></div>
	</div>
} )$
