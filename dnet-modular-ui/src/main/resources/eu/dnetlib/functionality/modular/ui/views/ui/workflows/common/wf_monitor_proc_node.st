<div id="monitorProcNodeModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Node: {{currentProcNode.name}}</h4>
			</div>
			<div class="modal-body">
				
				<span>
					<b>Node Started at:</b> {{currentProcNode.start > 0 ? (currentProcNode.start | date:'yyyy-MM-dd HH:mm:ss') : '-'}}<br />
					<b>Node Finished at:</b> {{currentProcNode.end > 0 ? (currentProcNode.end | date:'yyyy-MM-dd HH:mm:ss') : '-'}}<br />
					<b>Duration:</b> {{calculateDateDiff(currentProcNode.start, currentProcNode.end)}}<br />
				</span>
				<hr />
				<div id="tableCurrentProcNodeEnvParams" class="control gridStyle-big" ng-grid="gridCurrentProcNodeEnvParams"></div>
				<div class="well" style="font-size: 11px" ng-show="currentValue.name">
					<p><strong>{{currentValue.name}}</strong></p>
					<p>{{currentValue.value}}</p>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" 
					data-dismiss="modal"
					data-toggle="modal" data-target="#monitorProcWfModal"
					ng-click="showProcess(currentProc.wfId)">Back</button>
			</div>
		</div>
	</div>
</div>
