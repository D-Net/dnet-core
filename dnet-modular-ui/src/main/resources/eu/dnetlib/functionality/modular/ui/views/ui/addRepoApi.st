$common/master(

header={
	$ui/repos/header()$
	
	<link rel="stylesheet" type="text/css" href="../resources/css/chosen.css">
	
	<link rel="stylesheet" type="text/css" href="../resources/css/chosen-bootstrap.css">
    
	
	
	
	<script type='text/javascript' src="../resources/js/chosen/chosen.jquery.js"></script>
	<script type='text/javascript' src="../resources/js/chosen/angular-chosen.js"></script>
	
	
	<script type="text/javascript" src="../resources/js/dnet_workflows_common.js"></script>
	<script type="text/javascript" src="../resources/js/dnet_param_values_functions.js"></script>
	<script type="text/javascript" src="../resources/js/addRepoApi.js"></script>
	<script type="text/javascript">
		function getCompliances()         { return $compliances$ }
		function getTypes()               { return $types$ }
		function getContentDescriptions() { return $contentDescriptions$ }
		function getProtocols()           { return $protocols$ }
	</script>
	
	<style>
		.popover { max-width: 450px;  width: auto; }
	</style>
	

},
onLoad={},
body={
<div ng-app="addRepoApiUI" ng-controller="addRepoApiCtrl">
	<div ng-show="done" class="text-center">
		<h2>A new api has been registered !</h2> 
	</div>
	<div ng-hide="done">
		<form class="form-horizontal" role="form" ng-hide="repo.id">
			<ng-select-vocabulary-field label="Typology" vocabulary="values.types" selection="selectedTypology"></ng-select-vocabulary-field>
			<ng-select-repo-field label="Datasource" typology="selectedTypology" selection="repo"></ng-select-repo-field>
		</form>
	
		<form class="form-horizontal" role="form" ng-show="repo.id" name="newApiForm">
			<ng-fixed-value-field label="Datasource" value="repo.name"></ng-fixed-value-field>
			<ng-fixed-value-field label="Typology" value="api.typology"></ng-fixed-value-field>
			<ng-api-id-field label="Api ID" prefix="api_________::{{repo.origId}}::" exclusion="repo.apis" selection="api.id"></ng-api-id-field>
			<!-- <ng-select-vocabulary-field label="Typology" vocabulary="values.types" selection="api.typology"></ng-select-vocabulary-field> -->
			<ng-select-vocabulary-field label="Compatibility level" vocabulary="values.compliances" selection="api.compatibility"></ng-select-vocabulary-field>
			<ng-select-vocabulary-field label="Content description" vocabulary="values.contentDescriptions" selection="api.contentdescription"></ng-select-vocabulary-field>
			<ng-select-vocabulary-field label="Protocol" vocabulary="values.protocols" selection="api.protocol" contextual-params="currentParams"></ng-select-vocabulary-field>
			<ng-simple-edit-field label="BaseURL" selection="api.baseurl" regex="^(http|https|ftp|file|sftp|jar):\/\/"></ng-simple-edit-field>
			
			
			
			<ng-simple-edit-field 
				ng-repeat="param in currentParams | filter: { functionPopulated : false }" 
				label="{{param.name}} [{{api.protocol}} protocol]" 
				selection="apiParams[param.name]" 
				optional="{{param.optional}}" 
				regex="{{param.regex}}" 
				type="{{param.type}}"></ng-simple-edit-field>
				
			<ng-simple-select-field 
				ng-repeat="param in currentParams | filter: { functionPopulated : true, type : 'TEXT' }" 
				label="{{param.name}} [{{api.protocol}} protocol]" 
				selection="apiParams[param.name]"
				valid-values="validValues[api.baseurl + '@@@' + param.name]"
				refresh-function="listValidValuesForParam(param.name)"
				optional="{{param.optional}}"></ng-simple-select-field>
			
			<ng-multi-select-field 
				ng-repeat="param in currentParams | filter: { functionPopulated : true, type : 'LIST' }" 
				label="{{param.name}} [{{api.protocol}} protocol]" 
				selection="apiParams[param.name]"
				valid-values="validValues[api.baseurl + '@@@' + param.name]"
				refresh-function="listValidValuesForParam(param.name)"
				optional="{{param.optional}}"></ng-multi-select-field>
			
			<ng-simple-edit-field label="Xpath for Metadata Identifier" selection="api.metadataIdentifierPath"></ng-simple-edit-field>
				
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-9">
					<button class="btn btn-default" type="button" ng-click="resetForm()">reset</button>
					<button class="btn btn-primary" ng-disabled="newApiForm.\$invalid" ng-click="registerApi()">add</button>
				</div>
			</div>
		</form>
		<hr />
	</div>
	
	<!-- ENABLE TO DEBUG -->
	<!--
	<p><b>Typology:</b> {{selectedTypology}}</p>
	<p><b>Repo:</b> {{repo}}</p>
	<p><b>Api:</b> {{api}}</p>
	-->
	
</div>
}

)$