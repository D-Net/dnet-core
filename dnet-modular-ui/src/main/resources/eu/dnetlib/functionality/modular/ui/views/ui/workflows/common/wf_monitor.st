<div id="monitorWfModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{currentMetaWorkflow.name}} - History</h4>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-12 pull-right">
							<form class="form-inline pull-right">
								<input type="text" class="form-control input-sm" ng-model="filterMetaWfHistory.filterText" placeholder="Filter..." />
								<button type="button" class="btn btn-default btn-sm" ng-click="updateMetaWorkflowHistory()"><span class="glyphicon glyphicon-refresh"></span></button>
							</form>
						</div>
					</div>
					<div class="row" style="margin-top: 20px">
						<div class="col-xs-12">
							<div id="tableMetaWfHistory" class="control gridStyle-big" ng-grid="gridHistory"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
