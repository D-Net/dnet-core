<script type="text/javascript" src="../resources/js/angular.min.js"></script>
<script type="text/javascript" src="../resources/js/ng-grid-2.0.7.min.js"></script>
<script type="text/javascript" src="../resources/js/reposCommon.js"></script>

<link rel="stylesheet" type="text/css" href="../resources/css/ng-grid.min.css" />

<style>
	.gridStyle {
		width: 100%;
		height: 150px;
		margin-bottom: 20px;
		font-size: 11px;
	}
	
	.gridStyle-big {
		width: 100%; 
		height: 300px;
		margin-bottom: 20px;
		font-size: 11px;
	}
</style>