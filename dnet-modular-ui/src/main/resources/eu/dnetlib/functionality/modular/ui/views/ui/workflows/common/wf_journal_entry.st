<div id="currentLogDetailsModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="logDetailsLabel">Final environment of process <i>{{currentProcId}}</i></h4>
			</div>
			<div class="modal-body">
				<p>
					<strong>Started at: </strong>{{currentStartWf | date:'yyyy-MM-dd HH:mm:ss'}}<br/>
					<strong>Finished at: </strong>{{currentEndWf | date:'yyyy-MM-dd HH:mm:ss'}}<br/>
					<strong>Duration: </strong> {{calculateDateDiff(currentStartWf, currentEndWf)}}<br/>
				</p>
				<div id="wfLogDetails" class="gridStyle-big" ng-grid="gridCurrentLogDetails"></div>
				<div class="well" style="font-size: 11px" ng-show="currentValue.name">
					<p><strong>{{currentValue.name}}</strong></p>
					<p>{{currentValue.value}}</p>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default pull-left" data-dismiss="modal" data-toggle="modal" data-target="#monitorProcWfModal" ng-show="currentProc">Show graph</button>
				<button class="btn btn-default pull-left" ng-click="currentLog=currentAdvancedLog" ng-show="currentLog.length < currentAdvancedLog.length">Show all parameters</button>
				<button class="btn btn-primary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
