<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
	<div class="well" ng-hide="currentMetaWorkflow.wfId">
		Workflow not selected
	</div>

	<div class="well" ng-show="currentMetaWorkflow.wfId">
		<fieldset>
			<legend>
				{{currentMetaWorkflow.name}}
			</legend>
			<div>
				<div class="row" ng-show="currentMetaWorkflow.wfId">
					<div class="col-xs-12">
						<form class="form-inline">
							<button type="button" class="btn btn-primary btn-sm" ng-disabled="currentMetaWorkflow.statusEnum != 'EXECUTABLE'" ng-click="executeMetaWf()">launch now</button>
							<a href="isManager.do#/profile/{{currentMetaWorkflow.wfId}}" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-link"></span></a>
							<div class="btn-group">
								<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#metaWfEditModal" ng-click="prepareMetaWfEdit()">configure</button>
								<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#cloneWfModal">clone</button>
								<button type="button" class="btn btn-default btn-sm" data-toggle="modal" ng-click="updateMetaWorkflowHistory()">history</button>
							</div>
						</form>
					</div>
				</div>
				<div class="row" style="margin-top: 20px;">
					<div class="col-xs-12">
						<table class="table" id="metaWfTable">
							<thead>
								<tr>
									<th>Workflow</th>
									<th class="col-xs-2" style="white-space: nowrap;">Last execution date</th>
									<th class="col-xs-1 text-center">Last status</th>
									<th class="col-xs-1 text-center">Launch mode</th>
									<th class="col-xs-4 col-md-3 text-right">Actions</th>
								</tr>
							</thead>
							<tbody ng-bind-html="to_trusted(currentMetaWorkflow.html)" compile-template></tbody>
						</table>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</div>