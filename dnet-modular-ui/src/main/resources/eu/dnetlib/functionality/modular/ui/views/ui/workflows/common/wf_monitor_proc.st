<div id="monitorProcWfModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">
					Process {{currentProc.wfId}} ({{currentProc.name}})
					<span style="margin-left:10px" class="label" ng-class="{'label-success': currentProc.status == 'SUCCESS', 'label-danger': currentProc.status == 'FAILURE', 'label-info': currentProc.status == 'EXECUTING'}">{{currentProc.status}}</span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-7">
							<b>Started at:</b> {{currentProc.startDate > 0 ? (currentProc.startDate | date:'yyyy-MM-dd HH:mm:ss') : '-'}}<br />
							<b>Finished at:</b> {{currentProc.endDate > 0 ? (currentProc.endDate | date:'yyyy-MM-dd HH:mm:ss') : '-'}}<br />
							<b>Duration:</b> {{calculateDateDiff(currentProc.startDate, currentProc.endDate)}}<br />
						</div>
						<div class="col-xs-5">
							<form class="form-inline pull-right">
								<button type="button" class="btn btn-default btn-sm" ng-click="killProcess(currentProc.wfId)" ng-show="currentProc.status == 'EXECUTING'">kill</button>
								<button type="button" class="btn btn-default btn-sm" ng-click="showProcess(currentProc.wfId)" ng-show="(currentProc.status == 'EXECUTING') || (currentProc.status == 'CREATED')"><span class="glyphicon glyphicon-refresh"></span></button>
							</form>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12" style="margin-top: 20px; overflow-x: auto;" ng-show="currentProc.imageUrl">
							<map name="processMap" id="processMap" ng-bind-html="to_trusted(currentProc.mapContent)"></map>
							<img style="border:2px black solid" ng-src="{{currentProc.imageUrl}}" usemap="#processMap"/>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				$if(backFunction)$
					<button class="btn btn-default"	data-dismiss="modal"  ng-click="$backFunction$">Back</button>
				$else$
					<button class="btn btn-default"	data-dismiss="modal">Close</button>
				$endif$
			</div>
		</div>
	</div>
</div>

