<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:oai="http://www.openarchives.org/OAI/2.0/"
	xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
	xmlns:dc="http://purl.org/dc/doc:elements/1.1/"
	exclude-result-prefixes="oai oai_dc dc">

	<xsl:output method="html" omit-xml-declaration="yes" encoding="UTF-8" standalone="no" />

	<xsl:template match="/">
		<xsl:apply-templates select="oai:OAI-PMH/oai:error" />
		<xsl:apply-templates select="oai:OAI-PMH/oai:Identify" />
		<xsl:apply-templates select="oai:OAI-PMH/oai:ListSets" />
		<xsl:apply-templates select="oai:OAI-PMH/oai:ListRecords" />
		<xsl:apply-templates select="oai:OAI-PMH/oai:ListIdentifiers" />
		<xsl:apply-templates select="oai:OAI-PMH/oai:ListMetadataFormats" />
		<xsl:apply-templates select="oai:OAI-PMH/oai:GetRecord" />
	</xsl:template>

	<xsl:template match="oai:OAI-PMH/oai:error">
		<div class="alert alert-danger">
			<h4>Error</h4>
			<p>
				<xsl:value-of select="text()"></xsl:value-of>
			</p>
		</div>
	</xsl:template>

	<xsl:template match="oai:OAI-PMH/oai:Identify">
		<xsl:call-template name="verbTitle">
			<xsl:with-param name="title" select="string('Repository Information')"/>
		</xsl:call-template>
	
		<table class="table table-striped table-bordered">
			<tr>
				<td>
					<b>Repository Name</b>
				</td>
				<td>
					<xsl:value-of select="oai:repositoryName/text()" />
				</td>
			</tr>
			<xsl:for-each select="oai:adminEmail">
				<tr>
					<td>
						<b>E-Mail Contact</b>
					</td>
					<td>
						<a href="{concat('mailto:', text())}">
							<xsl:value-of select="text()" />
						</a>
					</td>
				</tr>
			</xsl:for-each>
			<tr>
				<td>
					<b>Description</b>
				</td>
				<td>
					<xsl:value-of select="oai:description/node()/text()" />
				</td>

			</tr>
			<tr>
				<td>
					<b>Protocol Version</b>
				</td>
				<td>
					<xsl:value-of select="oai:protocolVersion/text()" />
				</td>

			</tr>
			<tr>
				<td>
					<b>Earliest Registered Date</b>
				</td>
				<td>
					<xsl:value-of select="translate(oai:earliestDatestamp/text(), 'TZ' ,' ')" />
				</td>

			</tr>
			<tr>
				<td>
					<b>Date Granularity</b>
				</td>
				<td>
					<xsl:value-of select="translate(oai:granularity/text(), 'TZ', ' ')" />
				</td>

			</tr>
			<tr>
				<td>
					<b>Deletion Mode</b>
				</td>
				<td>
					<xsl:value-of select="oai:deletedRecord/text()" />
				</td>

			</tr>
		</table>
	</xsl:template>

	<xsl:template match="oai:OAI-PMH/oai:ListSets">
		<xsl:call-template name="verbTitle">
			<xsl:with-param name="title" select="string('Available Sets')"/>
			<xsl:with-param name="countPath" select="oai:set" />
		</xsl:call-template>
	
		<div class="list-group">
			<xsl:for-each select="oai:set">
				<div class="list-group-item">
					<h5 class="list-group-item-heading">
						<xsl:choose>
							<xsl:when test="string-length(oai:setName/text()) &gt; 83">
								<xsl:value-of select="substring(oai:setName/text(),0, 80 )" />
								...
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="oai:setName/text()" />
							</xsl:otherwise>
						</xsl:choose>
						<small>
							[
							<xsl:value-of select="oai:setSpec/text()" />
							]
						</small>
					</h5>
					<div class="spec">
						<div class="btn-group">
							<button type="button" class="btn btn-default btn-sm" ng-click="listRecords('oai_dc', '{oai:setSpec/text()}')">Records</button>
							<button type="button" class="btn btn-default btn-sm" ng-click="listIdentifiers('oai_dc', '{oai:setSpec/text()}')">Identifiers</button>
						</div>
					</div>
				</div>
			</xsl:for-each>
		</div>

		<xsl:apply-templates select="oai:resumptionToken" />
	</xsl:template>

	<xsl:template match="oai:OAI-PMH/oai:ListRecords|oai:OAI-PMH/oai:GetRecord">
		
		<xsl:choose>
			<xsl:when test="local-name() = 'GetRecord'">
				<xsl:call-template name="verbTitle">
					<xsl:with-param name="title" select="string('Record Details')"/>
					<xsl:with-param name="countPath" select="oai:record" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="verbTitle">
					<xsl:with-param name="title" select="string('List of Records')"/>
					<xsl:with-param name="countPath" select="oai:record" />
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		
		<xsl:apply-templates select="oai:resumptionToken" />
		
		<xsl:for-each select="oai:record">
			<div class="panel panel-default">
				<div class="panel-body">
					<table class="table">
						<tr>
							<th>Identifier</th>
							<td><xsl:value-of select="oai:header/oai:identifier/text()" /></td>
						</tr>
						<tr>
							<th>Last Modfied</th>
							<td><xsl:value-of select="translate(oai:header/oai:datestamp/text(), 'TZ', ' ')"></xsl:value-of></td>
						</tr>
						<tr>
							<th>Sets</th>
							<td>
								<xsl:for-each select="oai:header/oai:setSpec">
									<xsl:if test="position() &gt; 1">, </xsl:if>
									<a href="javascript:void(0)" ng-click="listRecords('oai_dc', '{text()}')">
										<xsl:value-of select="text()" />
									</a>
								</xsl:for-each>
							</td>
						</tr>
						<tr>
							<th>Record</th>
							<td>
								<xsl:choose>
									<xsl:when test="count(oai:metadata/*) &gt; 0">
										<xsl:for-each select="oai:metadata/*">
											<xsl:call-template name="showRecord" />
										</xsl:for-each>
									</xsl:when>
									<xsl:otherwise>
										<span class="label label-warning">Missing Metadata</span>										
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
					</table>
					<!-- <button class="btn btn-default btn-sm pull-right" ng-click="getRecord('{oai:header/oai:identifier/text()}', 'oai_dc')">View Details</button> -->
				</div>
			</div>
		</xsl:for-each>

		<xsl:apply-templates select="oai:resumptionToken" />
	</xsl:template>


	<xsl:template name="showRecord">
		<xsl:param name="indent" select="string('')" />
		<xsl:value-of select="$indent" />
		<strong><xsl:value-of select="local-name()"/>:	</strong><xsl:value-of select="text()"/>
		<xsl:if test="count(@*) &gt; 0">
			[<xsl:for-each select="@*">
				<xsl:if test="position() &gt; 1">, </xsl:if>
				<xsl:value-of select="local-name()" />: <xsl:value-of select="." />
			</xsl:for-each>]
		</xsl:if>
		<br />
		<xsl:for-each select="*">
			<xsl:call-template name="showRecord">
				<xsl:with-param name="indent" select="concat('&#160;&#160;&#160;&#160;', $indent)" />
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="oai:OAI-PMH/oai:ListIdentifiers">
		<xsl:call-template name="verbTitle">
			<xsl:with-param name="title" select="string('List of Identifiers')"/>
			<xsl:with-param name="countPath" select="oai:header" />
		</xsl:call-template>

		<xsl:apply-templates select="oai:resumptionToken" />
		
		<xsl:for-each select="oai:header">
			<div class="panel panel-default">
				<div class="panel-body">
					<table class="table">
						<tr>
							<th>Identifier</th>
							<td><xsl:value-of select="oai:identifier/text()" /></td>
						</tr>
						<tr>
							<th>Last Modfied</th>
							<td><xsl:value-of select="translate(oai:datestamp/text(), 'TZ', ' ')"></xsl:value-of></td>
						</tr>
						<tr>
							<th>Sets</th>
							<td>
								<xsl:for-each select="oai:setSpec">
									<xsl:if test="position() &gt; 1">, </xsl:if>
									<a href="javascript:void(0)" ng-click="listRecords('oai_dc', '{text()}')">
										<xsl:value-of select="text()" />
									</a>
								</xsl:for-each>
							</td>
						</tr>
					</table>
					<button class="btn btn-default btn-sm pull-right" ng-click="getRecord('{oai:identifier/text()}', 'oai_dc')">View Details</button>
				</div>
			</div>
		</xsl:for-each>

		<xsl:apply-templates select="oai:resumptionToken" />
	</xsl:template>

	<xsl:template match="oai:OAI-PMH/oai:ListMetadataFormats">
		<xsl:call-template name="verbTitle">
			<xsl:with-param name="title" select="string('List of Metadata Formats')"/>
			<xsl:with-param name="countPath" select="oai:metadataFormat" />
		</xsl:call-template>
		
		<xsl:for-each select="oai:metadataFormat">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="row">
						<div class="col-lg-9">
							<h5>
								<xsl:value-of select="oai:metadataPrefix/text()"></xsl:value-of>
							</h5>
						</div>
						<div class="col-lg-3">
							<button class="btn btn-default btn-sm pull-right" ng-click="listRecords('{oai:metadataPrefix/text()}', null)">
								List Records
							</button>
						</div>
					</div>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-9">
							<h5>
								<strong>Namespace: </strong><xsl:value-of select="oai:metadataNamespace/text()" />
							</h5>
							<h5>
								<strong>Schema: </strong><xsl:value-of select="oai:schema/text()"/>
							</h5>
						</div>
					</div>
				</div>
			</div>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="oai:resumptionToken">
		<xsl:if test="text() != ''">
			<div class="text-center">
				<button class="btn btn-primary btn-sm" ng-click="nextPage('{text()}')">Show More</button>
			</div>
			<br />
		</xsl:if>
	</xsl:template>

	<xsl:template name="result-count">
		<xsl:param name="path" />
		<xsl:variable name="cursor" select="$path/../oai:resumptionToken/@cursor" />
		<xsl:variable name="total" select="$path/../oai:resumptionToken/@completeListSize" />
		<xsl:variable name="count" select="count($path)" />
		<xsl:choose>
			<xsl:when test="$cursor">
				<xsl:choose>
					<xsl:when test="normalize-space($path/../oai:resumptionToken/text()) = ''">
						<xsl:value-of select="$total - $count" />
						-
						<xsl:value-of select="$total" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$cursor+1" />
						-
						<xsl:value-of select="$cursor + $count" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$count" />
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="$total">
			of
			<xsl:value-of select="$total" />
		</xsl:if>
	</xsl:template>

	<xsl:template name="verbTitle">
		<xsl:param name="title" />
		<xsl:param name="count" select="string('')"/>
		<xsl:param name="countPath" select="string('')"/>
		
		<h2><xsl:value-of select="$title"></xsl:value-of></h2>
		<h5><strong>Response Date: </strong><xsl:value-of select="translate(/oai:OAI-PMH/oai:responseDate/text(), 'TZ', ' ')" /></h5>
		<xsl:if test="$count">
			<h5><strong>Number of results: </strong><xsl:value-of select="$count" /></h5>
		</xsl:if>	
		<xsl:if test="$countPath">
			<h5>
				<strong>Number of entries: </strong>
				<xsl:call-template name="result-count">
					<xsl:with-param name="path" select="$countPath" />
				</xsl:call-template>
			</h5>
		</xsl:if>	
		<br />	
	</xsl:template>

</xsl:stylesheet>