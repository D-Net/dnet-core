$common/master(

header={
	$ui/repos/header()$
	<script type="text/javascript" src="../resources/js/dnet_param_values_functions.js"></script>
	<script type="text/javascript" src="../resources/js/addRepo.js"></script>
	<script type="text/javascript">
		function getTypes()     { return $types$ }
		function getCountries() { return $countries$ }
	</script>
	
	<style>
		.popover { max-width: 450px;  width: auto; }
	</style>
	
	
},
onLoad={},
body={
<div ng-app="addRepoUI" ng-controller="addRepoCtrl">
	<div ng-show="done" class="text-center">
		<h2>A new datasource has been registered !</h2> 
	</div>
	<div ng-hide="done">
		<form class="form-horizontal" role="form" name="newRepoForm">
		
			<fieldset>
				<legend>General Info</legend>
				<ng-simple-edit-field label="Datasource ID (template: 12Chars_____::theId)" selection="repo.id" regex="^\w{12}::\w+\$"></ng-simple-edit-field>
				<ng-simple-edit-field label="Official Name" selection="repo.officialname"></ng-simple-edit-field>
				<ng-simple-edit-field label="English Name" selection="repo.englishname" optional="true"></ng-simple-edit-field>
				<ng-simple-edit-field label="Organization" selection="org.legalname"></ng-simple-edit-field>
				<ng-select-vocabulary-field label="Typology Class" vocabulary="validTypes" selection="repo.typology"></ng-select-vocabulary-field>
			</fieldset>
			
			<fieldset>
				<legend>Contacts</legend>
				<ng-simple-edit-field label="WebSite Url" selection="repo.websiteurl" regex="^(http|https):\/\/"></ng-simple-edit-field>
				<ng-simple-edit-field label="Contact Email" selection="repo.contactemail" regex="^.+@.+"></ng-simple-edit-field>
			</fieldset>
				
			<fieldset>
				<legend>Geographical Info</legend>
				<ng-select-vocabulary-field label="Country" vocabulary="validCountries" selection="org.country"></ng-select-vocabulary-field>
				<ng-simple-edit-field label="Latitude" selection="repo.latitude" type="NUMBER"></ng-simple-edit-field>
				<ng-simple-edit-field label="Longitude" selection="repo.longitude" type="NUMBER"></ng-simple-edit-field>
			</fieldset>
			
			<fieldset>
				<legend>Technical Info</legend>
				<ng-simple-edit-field label="Namespace Prefix (12 chars)" selection="repo.namespaceprefix" regex="^\w{12}\$"></ng-simple-edit-field>
				<ng-simple-edit-field label="Software Typology" selection="repo.platform" optional="true"></ng-simple-edit-field>
				<ng-simple-edit-field label="Logo Url" selection="repo.logourl" regex="^(http|https):\/\/" optional="true"></ng-simple-edit-field>
			</fieldset>
			
			<br /><br />
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-9">
					<button class="btn btn-default" type="button" ng-click="resetForm()">reset</button>
					<button class="btn btn-primary" ng-disabled="newRepoForm.\$invalid" ng-click="registerRepo()">add</button>
				</div>
			</div>
		</form>
		<hr />
	</div>
	
	<!-- ENABLE TO DEBUG -->
	
	<!-- <p><b>Repo:</b> {{repo}}</p> -->
	
	
</div>
}

)$
