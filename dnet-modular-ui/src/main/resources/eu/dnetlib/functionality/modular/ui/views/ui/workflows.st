$if(redirect)$
	<!DOCTYPE HTML>
		<html lang="en-US">
			<head>
				<meta charset="UTF-8">
				<meta http-equiv="refresh" content="1;url=$redirect$">
				<script type="text/javascript">
					window.location.href = "$redirect$"
				</script>
				<title>Page Redirection</title>
			</head>
		<body>
			If you are not redirected automatically, follow the <a href='$redirect$'>link</a>
		</body>
	</html>
$else$
	$common/master( header={
		$ui/workflows/common/header()$
	
		<script type="text/javascript" src="../resources/js/dnet_workflows.js"></script>
		
		<script>
			function getSection()       { return $if(section)$$section$$else$''$endif$ }
			function getInitialWf()     { return $if(initialWf)$$initialWf$$else$''$endif$ }
			function getInitialMetaWf() { return $if(initialMetaWf)$$initialMetaWf$$else$''$endif$ }
			function getDatasourceId()  { return $if(dsId)$$dsId$$else$''$endif$ }
		</script>
	}, body={
		<div ng-app="workflowsUI" ng-controller="workflowsCtrl">
	
			$ui/workflows/common/metawf_edit()$		
			$ui/workflows/common/atomwf_edit()$
			$ui/workflows/common/wf_monitor()$
			$ui/workflows/common/wf_monitor_proc( backFunction={updateMetaWorkflowHistory()} )$
			$ui/workflows/common/wf_monitor_proc_node()$
			$ui/workflows/common/wf_journal_entry()$
	
			$ui/workflows/simple/wf_clone()$
			
			$ui/workflows/common/atomwf()$
	
			<div class="row">
				$ui/workflows/common/left_menu()$
				$ui/workflows/simple/metawf()$
			</div>			
		</div>
	} )$
$endif$
