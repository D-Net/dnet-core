<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:fn="http://www.w3.org/2005/xpath-functions">

	<xsl:output method="html" encoding="UTF-8" indent="yes" omit-xml-declaration="yes" />

	<xsl:template match="/">
		<p
			style="color:black; font-family:'Courier New', Courier, monospace; font-size:small">
			<xsl:call-template name="xmlItem">

				<xsl:with-param name="indent" select="string('')" />
			</xsl:call-template>
		</p>
	</xsl:template>


	<xsl:template name="xmlAttr">
		<span style='color:blue'>
			<xsl:value-of select="concat(' ', name())" />
		</span>
		<span style='color:red'>
		<xsl:value-of select="concat('=&quot;', ., '&quot;')" />
		</span>

	</xsl:template>

	<xsl:template name="xmlItem">
		<xsl:param name="indent" />

		<xsl:variable name="tag" select="name()" />

		<xsl:variable name="newindent">
			<xsl:value-of select="$indent" />
			&#160;
		</xsl:variable>

		<xsl:value-of select="$indent" />

		<xsl:choose>
			<xsl:when test="string-length($tag) &gt; 0">
				<span style='color:darkblue'>
					<xsl:value-of select="concat('&lt;',$tag)" />
					<xsl:for-each select="@*">
						<xsl:call-template name="xmlAttr" />
					</xsl:for-each>

					<span style='color:blue'>&gt;</span>
				</span>
			</xsl:when>
		</xsl:choose>


		<!-- <xsl:value-of select="$attribs" /> -->


		<xsl:value-of select="text()" />

		<xsl:choose>
			<xsl:when test="count(child::*) &gt; 0">
				<br />
				<xsl:for-each select="child::*">
					<xsl:call-template name="xmlItem">
						<xsl:with-param name="indent" select="$newindent" />
					</xsl:call-template>
					<br />
				</xsl:for-each>
				<xsl:value-of select="$indent" />
			</xsl:when>
			<xsl:otherwise>

			</xsl:otherwise>
		</xsl:choose>

		<xsl:choose>
			<xsl:when test="string-length($tag) &gt; 0">
				<span style='color:blue'>
					<xsl:value-of select="concat('&lt;/',$tag,'&gt;')" />
				</span>
			</xsl:when>
		</xsl:choose>



	</xsl:template>

</xsl:stylesheet>