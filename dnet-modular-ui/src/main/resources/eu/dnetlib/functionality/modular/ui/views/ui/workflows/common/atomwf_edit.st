<div id="atomWfEditModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Workflow parameters</h4>
			</div>
			<div class="modal-body">
				<p ng-hide="currentUserParams.data.length > 0">
					No parameters
				</p>
			
				<div class="panel panel-default" ng-repeat="n in currentUserParams.data">
					<div class="panel-heading"><strong>Node:</strong> {{n.desc}}</div>
					<div class="panel-body">
						<form role="form" class="form-horizontal">
							<div class="form-group" ng-repeat="p in n.params">
								<label title="Type: {{p.type}}" for="input_{{n.node}}_{{p.name}}" class="col-sm-5 control-label">{{p.name}}</label>
								<div class="col-sm-7">
									<div class="has-feedback" ng-class="{'has-success' : !p.required || (p.value.length > 0 && verifyType(p.value, p.type)), 'has-warning' : p.value.length == 0 && p.required , 'has-error' : p.value.length > 0 && !verifyType(p.value, p.type) }" ng-show="p.type != 'boolean' && !p.function">
										<input type="text" class="form-control" id="input_{{n.node}}_{{p.name}}" ng-model="p.value" ng-disabled="!p.userParam" />
										<span class="glyphicon form-control-feedback" ng-class="{'glyphicon-ok' : !p.required || (p.value.length > 0 && verifyType(p.value, p.type)), 'glyphicon-warning-sign' : p.value.length == 0 && p.required , 'glyphicon-remove' : p.value.length > 0 && !verifyType(p.value, p.type) }"></span>
									</div>
									
									<div ng-class="{'has-success' : !p.required || (p.value.length > 0 && verifyType(p.value, p.type)), 'has-warning' : p.value.length == 0 && p.required , 'has-error' : p.value.length > 0 && !verifyType(p.value, p.type) }" ng-show="p.type != 'boolean' && p.function">
										<select ng-model="p.value" class="form-control" ng-input="p.value" ng-options="item.id as item.name for item in listValidValuesForUserParam(p.function)" ng-disabled="!p.userParam">
											<option value=""></option>
										</select>
									</div>
									
									<div ng-show="p.type == 'boolean'">
										<input type="checkbox" id="input_{{n.node}}_{{p.name}}" ng-model="p.value" ng-true-value="true" ng-false-value="false" ng-disabled="!p.userParam" />
									</div>
									
									<div class="text-right" ng-show="p.uis.length > 0" style="padding: 10px 0px;">
										<span ng-repeat="ui in p.uis" ng-show="p.value || !ui.paramRequired">
											<a class="btn btn-sm btn-primary" href="{{ui.url}}" ng-hide="ui.paramRequired">{{ui.label}}</a>
											<a class="btn btn-sm btn-primary" href="{{ui.url | sprintf: p.value }}" ng-show="ui.paramRequired">{{ui.label}}</a>
										</span>
									</div>
								</div>
	  						</div>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal">Close</button>
				<button class="btn btn-primary" data-dismiss="modal" ng-show="currentUserParams.data.length > 0" ng-click="saveWorkflowsUserParams()">Save changes</button>
			</div>
		</div>
	</div>
</div>	
			