<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output omit-xml-declaration="yes" method="html" />

	<xsl:template match="/">
		<div>
			<div class="well">
				<fieldset>
					<legend><h4>General</h4></legend>
					<table class="table table-bordered table-striped">
						<tbody>
							<tr>
								<td width="25%">Name</td>
								<td><xsl:value-of select=".//OFFICIAL_NAME" /></td>
							</tr><tr>
								<td width="25%">Typology</td>
								<td><xsl:value-of select=".//DATASOURCE_TYPE" /></td>
							</tr><tr>
								<td width="25%">Level of compliance</td>
								<td><xsl:value-of select=".//DATASOURCE_COMPLIANCE_DEGREE" /></td>
							</tr><tr>
								<td width="25%">Country</td>
								<td><xsl:value-of select=".//COUNTRY" /></td>
							</tr><tr>
								<td width="25%">Institution</td>
								<td><a href="{.//REPOSITORY_WEBPAGE}"><xsl:value-of select=".//REPOSITORY_INSTITUTION" /></a></td>
							</tr><tr>
								<td width="25%">Contact email</td>
								<td><a href="mailto:{.//ADMIN_INFO}"><xsl:value-of select=".//ADMIN_INFO" /></a></td>
							</tr>
						</tbody>
					</table>
				</fieldset>
			</div>
			<div class="well">
				<fieldset>
					<legend><h4>Status</h4></legend>
					<table class="table table-bordered table-striped">
						<tbody>
							<tr>
								<td width="25%">Size</td>
								<td><xsl:value-of select=".//NUMBER_OF_OBJECTS" /></td>
							</tr><tr>
								<td width="25%">Last update</td>
								<td><xsl:value-of select=".//LAST_UPDATE" /></td>
							</tr><tr>
								<td width="25%">Registered By</td>
								<td><xsl:value-of select=".//REGISTERED_BY" /></td>
							</tr>
						</tbody>
					</table>
				</fieldset>
			</div>
			<div class="well">
				<fieldset>
					<legend><h4>Configuration parameters</h4></legend>
					<table class="table table-bordered table-striped">
						<tbody>
							<xsl:for-each select=".//EXTRA_FIELDS/FIELD">
								<tr>
									<td width="25%"><xsl:value-of select="./key" /></td>
									<td><xsl:value-of select="./value" /></td>
								</tr>
							</xsl:for-each>
						</tbody>
					</table>
				</fieldset>
			</div>
			<xsl:for-each select="//INTERFACE">
				<div class="well">
					<fieldset>
						<legend><h4>Interface: <xsl:value-of select="./@typology" /></h4></legend>
						<table class="table table-bordered table-striped">
							<tbody>
								<xsl:for-each select="@*|.//*[string-length(normalize-space(text())) &gt; 0]|.//*[name() != 'INTERFACE_EXTRA_FIELD']/@*">
									<tr>
										<td width="25%">
											<xsl:choose>
												<xsl:when test="name() = 'INTERFACE_EXTRA_FIELD'">
													<xsl:value-of select="@name" />
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="local-name(.)" />		
												</xsl:otherwise>
											</xsl:choose>
										</td>
										<td><xsl:value-of select="." /></td>
									</tr>
								</xsl:for-each>
							</tbody>
						</table>
					</fieldset>
				</div>
			</xsl:for-each>
		</div>
	</xsl:template>

</xsl:stylesheet> 