$common/master( header={}, onLoad={}, body={
	<div class="well">
		<p><b>User ID: </b><br />$ui_user.id$</p>
		<p><b>Fullname: </b><br />$ui_user.fullname$</p>
		<p><b>Email: </b><br />$ui_user.email$</p>
		<p>
			<b>Roles: </b>
			<ul>
				$ui_user.permissionLevels:{level|<li>$level$</li>}$
			</ul>
		</p>
	</div>
})$
