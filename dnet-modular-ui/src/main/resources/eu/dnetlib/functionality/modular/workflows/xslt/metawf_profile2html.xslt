<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="html" omit-xml-declaration="yes"
		encoding="UTF-8" standalone="no" />

	<xsl:template match="/">
		<xsl:for-each select="//CONFIGURATION/WORKFLOW">
			<xsl:call-template name="WORKFLOW_ROW"/>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="WORKFLOW_ROW">
		<xsl:param name="space" select="number(0)" />
		<tr>
			<td>
				<span style="padding-left: {$space}px">
					<span class="glyphicon glyphicon-chevron-right" style="margin: 8px"></span>
					<a href="javascript:void(0)" ng-click="getAtomicWorkflow('{@id}')"><xsl:value-of select="@name"/></a>
				</span>
			</td><td>
				<xsl:choose>
					<xsl:when test="@date">
						{{ '<xsl:value-of select="@date" />' | date: "yyyy-MM-dd HH:mm:ss" }}
					</xsl:when>
					<xsl:otherwise><i class="muted" style="white-space: nowrap">not yet executed</i></xsl:otherwise>
				</xsl:choose>
			</td><td class="text-center">
				<xsl:choose>
					<xsl:when test="@status = 'SUCCESS'">
						<span class="label label-success">SUCCESS</span>
					</xsl:when>
					<xsl:when test="@status = 'FAILURE'">
						<span class="label label-danger">FAILURE</span>
					</xsl:when>
					<xsl:otherwise>-</xsl:otherwise>
				</xsl:choose>
			</td>
			<td class="text-center">
				<div class="btn-group text-left">
					<button type="button" data-toggle="dropdown" style="width: 100px;">
						<xsl:attribute name="class">
						 	btn btn-sm dropdown-toggle
						 	<xsl:choose>
						 		<xsl:when test="@start = 'auto'">btn-default</xsl:when>
						 		<xsl:when test="@start = 'manual'">btn-warning</xsl:when>
								<xsl:otherwise>btn-danger</xsl:otherwise>						 	
						 	</xsl:choose>
						</xsl:attribute>
					
						<span class="pull-left"><xsl:value-of select="@start" /></span><div class="pull-right"><span class="caret"></span></div>
					</button>
				    <ul class="dropdown-menu" role="menu">
				        <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" ng-click="changeWfStartMode('{@id}', 'auto')">auto</a></li>
				        <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" ng-click="changeWfStartMode('{@id}', 'manual')">manual</a></li>
				        <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" ng-click="changeWfStartMode('{@id}', 'disabled')">disabled</a></li>
				    </ul>
				</div>
			</td><td>
				<form class="form-inline pull-right">
				  	<button data-toggle="modal" data-target="#atomWfEditModal" ng-click="listWorkflowsUserParams('{@id}')">
				  		<xsl:attribute name="class">
				  			<xsl:choose>
					  			<xsl:when test="@configured != 'true'">btn btn-warning btn-sm</xsl:when>
					  			<xsl:otherwise>btn btn-default btn-sm</xsl:otherwise>
				  			</xsl:choose>
				  		</xsl:attribute>
				  		<span style="margin-right: 8px">
				  			<xsl:attribute name="class">
					  			<xsl:choose>
						  			<xsl:when test="@configured != 'true'">glyphicon glyphicon-warning-sign</xsl:when>
						  			<xsl:otherwise>glyphicon glyphicon-ok</xsl:otherwise>
					  			</xsl:choose>
					  		</xsl:attribute>
				  		</span>
				  		parameters
				  	</button>
					<button class="btn btn-primary btn-sm" style="margin-left: 4px" ng-click="executeWf('{@id}')">
						<xsl:if test="@configured != 'true' or @disabled = 'true'">
							<xsl:attribute name="disabled">disabled</xsl:attribute>
						</xsl:if>
						launch
					</button>
				</form>
			</td>
		</tr>

		<xsl:for-each select="./WORKFLOW">
			<xsl:call-template name="WORKFLOW_ROW">
				<xsl:with-param name="space" select="$space + 40" />
			</xsl:call-template>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>



