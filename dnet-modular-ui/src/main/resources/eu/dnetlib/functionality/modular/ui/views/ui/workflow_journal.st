$common/master( header={
	$ui/workflows/common/header()$

	<script type="text/javascript" src="../resources/js/dnet_wf_journal.js"></script>
	<script type="text/javascript" src="../resources/js/ui-bootstrap.min.js"></script>
	<script type="text/javascript" src="../resources/js/jquery-ui-1.10.4.min.js"></script>
	<script type="text/javascript" src="../resources/js/jQDateRangeSlider-withRuler-min.js"></script>
	
	<link rel="stylesheet" type="text/css" href="../resources/css/jquery-ui-1.10.4.min.css" />
	<link rel="stylesheet" type="text/css" href="../resources/css/jQRangeSlider-dnet.min.css" />
	
	<style>
		#wfJournalTable {
			width: 100%; 
			height: 500px;
			margin-bottom: 20px;
			font-size: 11px;
		}
		
		#wfLogDetails {
			width: 100%; 
			height: 300px;
			margin-bottom: 20px;
			font-size: 11px;
		}
	</style>
	
	<script type="text/javascript">
		function initProcId() {
			return '$procId$';
		}
		
		function getFamily() {
			return '$family$';
		}
	</script>
	
}, body={
	<div ng-app="wfJournalUI" ng-controller="wfJournalCtrl">

		$ui/workflows/common/wf_monitor_proc()$
		$ui/workflows/common/wf_monitor_proc_node()$
		$ui/workflows/common/wf_journal_entry()$
	
		<div ng-hide="family">
			<div class="row">
				<div class="col-xs-12 col-md-9">
					<div slider-date-range 
							min="minDate" 
							max="maxDate" 
							start="startDate" 
							end="endDate" 
							update-function="updateDates"></div>
				</div>
				<div class="col-xs-12 col-md-3">
					<form class="form-inline pull-right" role="form">
						<input class="form-control input-sm" type="text" ng-model="filterJournal.filterText" placeholder="Filter...">
						<button ng-click="refresh()" class="btn	btn-default btn	btn-sm" type="button"><span class="glyphicon glyphicon-refresh"></span></button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<span><strong>{{journal.length}}</strong> workflow(s) from <strong>{{formatDate(startDate)}}</strong> to <strong>{{formatDate(endDate)}}</strong></span>
				</div>
			</div>
		</div>
		<div ng-show="family" class="row">
			<div class="col-xs-12 col-md-9">
				<span><strong>{{journal.length}}</strong> workflow(s) of family <strong>{{family}}</strong></span>
			</div>
			<div class="col-xs-12 col-md-3">
				<form class="form-inline pull-right" role="form">
					<input class="form-control input-sm" type="text" ng-model="filterJournal.filterText" placeholder="Filter...">
					<button ng-click="refresh()" class="btn	btn-default btn	btn-sm" type="button"><span class="glyphicon glyphicon-refresh"></span></button>
				</form>
			</div>
		</div>
		
		<div class="row" style="margin-top: 20px">
			<div class="col-xs-12">
				<div id="wfJournalTable" class="gridStyle" ng-grid="gridWfJournal"></div>
			</div>
		</div>
	</div>

} )$
