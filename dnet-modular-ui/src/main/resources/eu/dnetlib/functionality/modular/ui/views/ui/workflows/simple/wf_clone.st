<div id="cloneWfModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{currentMetaWorkflow.name}}</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-sm-3 control-label" for="inputCronExpression">New name</label>
						<div class="col-sm-9">
							<input class="form-control" id="cloneWfNewName" type="text" required placeholder="clone of {{currentMetaWorkflow.name}}" ng-model="cloneWfNewName" />
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default" data-dismiss="modal">Close</button>
				<button class="btn btn-primary" ng-click="cloneMetaWf(currentMetaWorkflow.wfId, cloneWfNewName)" data-dismiss="modal">Clone</button>
			</div>
		</div>
	</div>
</div>
	