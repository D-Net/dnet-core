$common/master( header={}, onLoad={}, body={
	<div>
		$ui_modules:{k|
			<ul>
				<li>$k.title$
					<ul>
						$k.entries:{ep|<li><a href="$ui_baseUrl$$ep.relativeUrl$"	title="$ep.description$">$ep.menu$</a></li>}$
					</ul>
				</li>
			</ul>
		}$
	</div>
})$
