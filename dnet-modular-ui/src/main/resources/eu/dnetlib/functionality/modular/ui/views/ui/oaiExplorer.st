$common/master( header={
	<script type="text/javascript" src="../resources/js/angular.min.js" ></script>
	<script type="text/javascript" src="../resources/js/ng-grid-2.0.7.min.js"></script>
	<script type="text/javascript" src="../resources/js/dnet_oai_explorer.js"></script>
	<script type="text/javascript" src="../resources/js/jquery.flot.js"></script>
	
	<link rel="stylesheet" type="text/css" href="../resources/css/ng-grid.min.css" />
	<script type="text/javascript">
		function getInitialBaseUrl() {
			return $if(oaiBaseUrl)$'$oaiBaseUrl$'$else$''$endif$;
		}
	</script>
}, body={
	<div ng-app="oaiExplorerUI" ng-controller="oaiExplorerCtrl">
		<div class="row">
			<div class="form-inline col-xs-12 col-md-6">
				<form role="form">
					<div class="form-group has-feedback" ng-class="{ 'has-error' : !isValidUrl(tmpBaseUrl), 'has-success' : isValidUrl(tmpBaseUrl) }">
						<label for="inputBaseUrl" class="control-label">Base URL</label>
   						<input type="text" style="width: 400px" class="form-control" id="inputBaseUrl" placeholder="Enter base url" ng-model="tmpBaseUrl" />
						<span class="glyphicon form-control-feedback" ng-class="{ 'glyphicon-warning-sign' : !isValidUrl(tmpBaseUrl),  'glyphicon-ok' : isValidUrl(tmpBaseUrl) }"></span>
					</div>
					<button type="button" class="btn btn-primary" ng-click="verifyBaseUrl(tmpBaseUrl)" ng-disabled="!isValidUrl(tmpBaseUrl)">update</button>
				</form>
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="well well-sm pull-right" style="top: 4px"><a href="{{oaiUrl}}" target="_blank">{{oaiUrl}}</a></div>
			</div>
		</div>
		<div class="row" ng-show="baseUrl.length > 0">
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
				<div class="panel panel-default">
					<div class="panel-heading">Oai verbs</div>
					<div class="panel-body">
						<ul class="nav nav-pills nav-stacked">
							<li ng-class="{'active' : currentAction == 'Identify' }"><a href="javascript:void(0)" ng-click="identify()">Identify</a></li>
							<li ng-class="{'active' : currentAction == 'ListMetadataFormats' }"><a href="javascript:void(0)" ng-click="listMetadataFormats()">ListMetadataFormats</a></li>
							<li ng-class="{'active' : currentAction == 'ListSets' }"><a href="javascript:void(0)" ng-click="listSets()">ListSets</a></li>
							<li ng-class="{'active' : currentAction == 'ListRecords' }"><a href="javascript:void(0)" ng-click="listRecords('oai_dc')">ListRecords</a></li>
							<li ng-class="{'active' : currentAction == 'ListIdentifiers' }"><a href="javascript:void(0)" ng-click="listIdentifiers('oai_dc')">ListIdentifiers</a></li>
							<li ng-class="{'active' : currentAction == 'GetRecord' }"><a href="javascript:void(0)" ng-click="getRecord()">GetRecord</a></li>
						</ul>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading">Oai Tests</div>
					<div class="panel-body">
						<ul class="nav nav-pills nav-stacked">
							<li ng-class="{'active' : currentAction == 'testAllVerbs' }"><a href="javascript:void(0)" ng-click="testAllVerbs()">Test all verbs</a></li>
							<li ng-class="{'active' : currentAction == 'testHarvesting' }"><a href="javascript:void(0)" ng-click="testHarvesting()">Test harvesting</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
				<div class="well">
					<div ng-show="oaiDataHTML" ng-bind-html="oaiDataHTML" compile-template></div>
					<div ng-show="currentAction == 'testAllVerbs'">
						<h2>Test of all OAI verbs</h2>
						<table class="table">
							<thead>
								<tr>
									<th>verb</th>
									<th class="text-center col-xs-2">http code</th>
									<th class="text-center col-xs-2">is valid</th>
									<th class="text-right col-xs-2">time</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="r in testAllVerbsData" ng-class="{'success' : r.valid, 'danger' : !r.valid}">
									<th>{{r.verb}}</th>
									<td class="text-center">{{r.httpCode}}</td>
									<td class="text-center">
										<span ng-show="r.valid" class="glyphicon glyphicon-ok" title="Good"></span>
										<span ng-hide="r.valid" class="glyphicon glyphicon-warning-sign" title="{{r.error}}"></span>
									</td>
									<td class="text-right">{{r.time / 1000}} sec.</td>
								</tr>
							</tbody>						
						</table>
						<p ng-show="continueIteration">Running...</p>
						<p ng-hide="continueIteration">Completed !</p>
					</div>
					<div ng-show="currentAction == 'testHarvesting'">
						<h2>Test Harvesting</h2>
						<p>
							<form class="form-inline">
								<button ng-hide="continueIteration" class="btn btn-primary btn-sm" ng-click="startTestHarvesting()">start harvesting</button>
								<button ng-show="continueIteration" class="btn btn-primary btn-sm" ng-click="stopTestHarvesting()">stop harvesting</button>
								<button ng-hide="continueIteration || testHarvestingData.length == 0" class="btn btn-primary btn-sm" ng-click="updateGraph(testHarvestingData, 'time')" data-toggle="modal" data-target="#harvestingGraphModal">show time graph</button>
								<span ng-show="continueIteration">still running...</span>
							</form>
						</p>
						<div style="height: 400px; overflow-y:scroll">
							<table class="table" ng-show="testHarvestingData.length > 0">
								<thead>
									<tr>
										<th></th>
										<th class="text-right col-xs-1">size</th>
										<th class="text-right col-xs-1">cursor</th>
										<th class="text-right col-xs-1">total</th>
										<th class="text-center col-xs-1">http code</th>
										<th class="text-center col-xs-1">is valid</th>
										<th class="text-right col-xs-1">time</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="r in testHarvestingData" ng-class="{'success' : r.valid, 'danger' : !r.valid}">
										<th>{{r.verb}} - call {{\$index + 1}}</th>
										<td class="text-right">{{r.size   >= 0 ? r.size : '-'}}</td>
										<td class="text-right">{{r.cursor >= 0 ? r.cursor : '-'}}</td>
										<td class="text-right">{{r.total  >= 0 ? r.total : '-'}}</td>
										<td class="text-center">{{r.httpCode}}</td>
										<td class="text-center">
											<span ng-show="r.valid" class="glyphicon glyphicon-ok" title="Good"></span>
											<span ng-hide="r.valid" class="glyphicon glyphicon-warning-sign" title="{{r.error}}"></span>
										</td>
										<td class="text-right">{{r.time / 1000}} sec.</td>
									</tr>
								</tbody>						
							</table>
							
							
							<br />
							<div id="endPage"></div>
						</div>
						<table class="table table condensed">
							<tr>
								<th class="col-xs-1">Min</th>
								<td class="col-xs-1">{{time_min.toFixed(3)}} sec.</td>
								<th class="col-xs-1">Max</th>
								<td class="col-xs-1">{{time_max.toFixed(3)}} sec.</td>
								<th class="col-xs-1">Avg</th>
								<td class="col-xs-1">{{time_avg.toFixed(3)}} sec.</td>
								<th class="col-xs-1">Std Dev</th>
								<td class="col-xs-1">{{time_stddev.toFixed(3)}} sec.</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		
		<div class="modal fade" id="harvestingGraphModal" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Harvesting Graph</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div id="harvestingGraph" class="col-xs-12" style="height: 400px; width: 800px"></div>	
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>


	</div>
} )$
