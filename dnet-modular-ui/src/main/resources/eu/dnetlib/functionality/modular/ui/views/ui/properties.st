$common/master( header={}, onLoad={}, body={
	<table class="table table-hover table-condensed small" style="table-layout: fixed; word-wrap: break-word;">
		$properties.keys:{prop|
			<tr>
				<th class="col-xs-3">$prop$</th>
				<td class="col-xs-9">
					<ul>
					$properties.(prop):{info|
						<li>
                        	<span $if(info.isChecked)$ class="text-success" $endif$>$info.value$</span> <span class="pull-right $if(info.isChecked)$ text-success $endif$"><strong>$info.sourcePropertyFile$</strong></span> <br/>
                    	</li>
                    }$
                    </ul>
				</td>
			</tr>
		}$
	</table>
})$
