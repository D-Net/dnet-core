<!DOCTYPE html>
<html lang="en">

<head>
	<title>$if(ui_title)$$ui_title$$else$No title$endif$</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" media="screen" href="../resources/css/bootstrap-spacelab.min.css">
	<link rel="stylesheet" type="text/css" href="../resources/css/custom.css">
	<link rel="stylesheet" type="text/css" media="all"   href="../resources/css/jquery.pnotify.default.css">
	<script type="text/javascript" src="../resources/js/jquery-1.11.1.min.js" ></script>
	<script type="text/javascript" src="../resources/js/bootstrap.min.js" ></script>
	<script type="text/javascript" src="../resources/js/spin.js" ></script>
	<script type="text/javascript" src="../resources/js/jquery.pnotify.min.js"></script>
	<script type="text/javascript" src="../resources/js/custom.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	$header$
</head>

<body role="document">
	<div class="navbar $ui_navbar_class$ navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapsedNavbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">D-Net</a>
			</div>
	
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="collapsedNavbar">
				<ul class="nav navbar-nav">
				$ui_modules:{k|
				   <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">$k.title$ <b class="caret"></b></a>
						<ul class="dropdown-menu">
							$k.entries:{ep|<li><a href="$ui_baseUrl$$ep.relativeUrl$">$ep.menu$</a></li> }$
						</ul>
					</li>
				}$
				</ul>
				
				$if(ui_user)$
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">$ui_user.fullname$ <b
									class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="userDetails.do">details</a></li>
								$if(ui_logoutUrl)$
									<li class="divider"></li>
									<li><a href="$ui_logoutUrl$">logout</a></li>
								$endif$
							</ul>
						</li>
					</ul>
				$endif$
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</div>
	
	$if(ui_message)$
		<div style="position: absolute; top: -4px; right: 10px; z-index: 99999;
			border: 1px solid grey; 
			background-color: #ffffff;
			padding: 3px 20px;
			opacity: 0.9;
			border-radius: 6px;
			font-size: 11px;">$ui_message$</div>
	$endif$
	
	<!-- Le spinning indicator for operation wait -->
	<div id="spinnerdiv" class="overlaydiv">
		<span class="grayRectangle"><!--The spinner is added on loading here--></span>
	</div>

	<!-- Le main container -->
	<div role="main" class="container-fluid" style="margin-top: 40px; margin-bottom: 70px;">
		<div class="row">
			<div class="col-xs-12">
				<h3 class="page-header">$if(ui_title)$$ui_title$$else$No title$endif$</h3>
			</div>
		</div>
		$body$		
	</div><!-- /.main container -->

	<div class="corner-ribbon bottom-right sticky $ribbonAccent$ shadow">$environment$</div>
	<div class="navbar navbar-fixed-bottom" role="navigation">
		<div class="container pull-left">
			<p class="navbar-text navbar-left">
				Powered by D-NET - <a href="http://www.d-net.research-infrastructures.eu/">http://www.d-net.research-infrastructures.eu/</a>
			</p>
		</div>
	</div>
	
</body>

</html>

