for $x in collection("/db/DRIVER/ServiceResources/")/RESOURCE_PROFILE/HEADER
return
<service>
	<id>{$x/RESOURCE_IDENTIFIER/@value/string()}</id>
	<name>{replace($x/RESOURCE_TYPE/@value/string(),'ServiceResourceType$','')}</name>
	<wsdl>{$x/RESOURCE_URI/@value/string()}</wsdl>
</service>