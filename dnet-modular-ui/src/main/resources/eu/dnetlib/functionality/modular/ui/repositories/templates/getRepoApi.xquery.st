for \$x in 
	collection('/db/DRIVER/RepositoryServiceResources/RepositoryServiceResourceType')[.//RESOURCE_IDENTIFIER/@value='$dsId$' or .//DATASOURCE_ORIGINAL_ID = '$dsId$']//INTERFACE[@id='$ifaceId$']
let \$repoId := \$x/../../../../HEADER/RESOURCE_IDENTIFIER/@value/string()
return 
	<api>
		<id>{\$x/@id/string()}</id>
		<label>{\$x/@label/string()}</label>
		<removable>{\$x/@removable/string()}</removable>
		<protocol>{\$x/ACCESS_PROTOCOL/text()}</protocol>
		<repo 
			id="{\$repoId}" 
			country="{\$x/../../COUNTRY/text()}" 
			prefix="{\$x/../..//EXTRA_FIELDS/FIELD[./key='NamespacePrefix']/value}"
			type="{\$x/../../TYPOLOGY/text()}"
			email="{\$x/../../ADMIN_INFO/text()}">
			{\$x/../../OFFICIAL_NAME/text()}
		</repo>
		<commonParams>
			<param name="id">{\$x/@id/string()}</param>
			<param name="label">{\$x/@label/string()}</param>
			<param name="typology">{\$x/@typology/string()}</param>
			<param name="active">{\$x/@active/string()}</param>
			<param name="compliance">{\$x/@compliance/string()}</param>
		</commonParams>
		<accessParams>
			<param name="baseUrl">{\$x/BASE_URL/text()}</param>
			{
				for \$y in 
					\$x/ACCESS_PROTOCOL/@*
				return
					<param name="{\$y/name()}">{\$y/string()}</param>
			}
		</accessParams>
		<extraFields>
			{
				for \$y in 
					\$x/INTERFACE_EXTRA_FIELD
				return
					<field name="{\$y/@name/string()}">{\$y/text()}</field>
			}
		</extraFields>
		<metaWFs>
		{
			for \$y in 
				collection('/db/DRIVER/MetaWorkflowDSResources/MetaWorkflowDSResourceType') 
			where 
				\$y//DATAPROVIDER/@interface = \$x/@id and \$y//DATAPROVIDER/@id= \$repoId
			return
				<metaWF>
					<id>{\$y//RESOURCE_IDENTIFIER/@value/string()}</id>
					<name>{\$y//METAWORKFLOW_NAME/text()}</name>
					<status>{\$y//CONFIGURATION/@status/string()}</status>
					<destroyWorkflow>{\$y//CONFIGURATION/@destroyWorkflow/string()}</destroyWorkflow>
				</metaWF>
		}
		</metaWFs>
	</api> 


