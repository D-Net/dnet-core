$common/master( header={}, onLoad={}, body={
	$info.keys:{title|
		<div class="panel panel-primary">
			<div class="panel-heading" role="tab" id="heading_$i$">
				<h4 class="panel-title">
					<a data-toggle="collapse" href="#collapse_$i$">$title$</a>
				</h4>
			</div>
			<div id="collapse_$i$" class="panel-collapse collapse in" role="tabpanel">
				<table class="table table-striped table-condensed small">
					$info.(title).keys:{k|
						<tr>
							<th class="col-xs-3">$k$</th>
							<td class="col-xs-9">$info.(title).(k)$</td>
						</tr>
					}$
				</table>
			</div>
		</div>
	}$

	<div class="panel panel-primary">
		<div class="panel-heading" role="tab" id="heading_modules">
			<h4 class="panel-title">
				<a data-toggle="collapse" href="#collapse_modules">Modules</a>
			</h4>
		</div>
		<div id="collapse_modules" class="panel-collapse collapse in" role="tabpanel">
			<table class="table table-striped table-condensed small">
				<thead>
					<tr>
						<th>Group ID</th>
						<th>Artifact ID</th>
						<th>Version</th>
						<th>POM</th>
					</tr>
				</thead>
				<tbody>
				$modules:{m|
					<tr $if(m.warning)$class="danger"$endif$>
						<td>$m.group$</td>
						<td>$m.name$</td>
						<td>$m.versions:{v|$v$};separator=", "$</td>
						<td>$m.files:{f|$f$<br />}$</td>
					</tr>
				}$
				</tbody>
			</table>
		</div>
	</div>
})$
