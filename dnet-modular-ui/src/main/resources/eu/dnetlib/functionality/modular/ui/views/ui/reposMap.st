$common/master(
header={
	$ui/repos/header()$
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&language=en"></script>
	<script type="text/javascript" src="../resources/js/reposMap.js"></script>
},
onLoad={},
body={
	<div ng-app="reposMapUI" ng-controller="reposMapCtrl">
		<div class="row">
			<div id="map_canvas_container" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height: 400px;">
				<div id="map_canvas" style="height: 100%; border: 1px solid #336699"></div> 
			</div>
			$ui/repos/repoDetails()$
		</div>
	</div>
}
)$