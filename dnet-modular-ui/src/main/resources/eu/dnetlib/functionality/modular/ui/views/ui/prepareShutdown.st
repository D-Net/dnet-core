$common/master( header={
	<script type="text/javascript" src="../resources/js/angular.min.js"></script>
	<script type="text/javascript" src="../resources/js/shutdown/shutdown.js"></script>
}, onLoad={}, body={
	<div ng-app="shutdownUI" ng-controller="shutdownCtrl">
				
		<div>
			<div>
				<button class="btn btn-default btn-primary" ng-click="prepareShutdown()" ng-disabled="stopLaunched">prepare for shutdown</button>	
				<button class="btn btn-default btn-primary" ng-click="resumeShutdown()" ng-show="stopLaunched">resume</button>
				<button class="btn btn-default btn-default pull-right" ng-click="loadStoppables()"><span class="glyphicon glyphicon-refresh"></span></button>	
			</div>
			
			<div ng-show="stopEnded" class="well text-center alert alert-danger" style="margin-top: 20px;">
				<h3>Now you can perform the shutdown !!!</h3>
			</div>
						
			<div class="well" ng-repeat="s in stoppables" style="margin-top: 20px;" ng-hide="stopEnded">
				<fieldset>
					<legend>
						<h4>
							{{s.name}}
							<span class="label label-success pull-right" ng-show="s.status == 'RUNNING'">running</span>
							<span class="label label-warning pull-right" ng-show="s.status == 'S'">stopping</span>
							<span class="label label-danger pull-right" ng-show="s.status == 'STOPPED'">stopped</span>
						</h4>
					</legend>
					{{s.message}}
				</fieldset>
			</div>
		</div>
	</div>
})$
