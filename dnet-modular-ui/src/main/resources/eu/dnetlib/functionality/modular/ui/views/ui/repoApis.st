$common/master(

header={
	$ui/repos/header()$
	<script type="text/javascript" src="../resources/js/angular-route.min.js"></script>
	<script type="text/javascript" src="../resources/js/dnet_workflows_common.js"></script>
	<script type="text/javascript" src="../resources/js/dnet_param_values_functions.js"></script>
	<script type="text/javascript" src="../resources/js/repoControllers.js"></script>
	<script type="text/javascript" src="../resources/js/repos.js"></script>
	<script type="text/javascript">
		function getAvailableRepohiWfs()      { return $availableRepohiWfs$ }
		function getCompatibilityLevels()     { return $compatibilityLevels$ }
		function getBrowseFields()            { return $browseFields$ }
		function getValidatorAddress()        { return '$validatorAddress$' }
		function getValidatorServiceAddress() { return '$validatorServiceAddress$' }
	</script>
	
	<style>
		.popover { max-width: 450px;  width: auto; }
	</style>
},
onLoad={},
body={
	<div id="main" ng-app="reposUI">
		<div ng-view></div>
	</div>
}
)$