$common/master(

header={
	<script type="text/javascript" src="../resources/js/angular.min.js"></script>
	<script type="text/javascript" src="../resources/js/transform/transform.js"></script>
},
onLoad={},
body={
<div class="row" ng-app="transformUI" ng-controller="transformCtrl">
	<div class="col-xs-12 col-md-10 col-lg-8">
		<form>
			<div class="form-group">
				<label>Transformation rules:</label>
				<select name="rule" class="form-control" ng-model="rule">
					$rules:{<option $if(it.selected)$selected$endif$ value="$it.id$">$it.name$</option>}$
				</select>
			</div>
			<div class="form-group">
				<label>Input Record:</label>
				<textarea cols="100" rows="10" class="form-control" ng-model="inputRecord"></textarea>
			</div>
		
			<button class="btn btn-sm btn-primary" ng-click="transform(rule, inputRecord)" ng-disabled="!rule || !inputRecord">transform</button>
			
			<button disabled="disabled"                                        class="btn btn-sm btn-default pull-right" ng-if="!rule">show transformation rule</button>
			<a ng-href="./isManager.do#/profile/{{rule | encodeURIComponent}}" class="btn btn-sm btn-default pull-right" ng-if="rule" >show transformation rule</a>
			
		</form>
		
		<br />
		
		<p ng-if="outputRecord">
			<label>Output Record:</label>
			<pre ng-if="outputRecord">{{outputRecord}}</pre>
		</p>
		
		<p ng-if="error.message">
			<label>Error: <i>{{error.message}}</i></label>
			<pre ng-if="error.stacktrace">{{error.stacktrace}}</pre>
		</p>
	</div>
</div>	
}
)$
