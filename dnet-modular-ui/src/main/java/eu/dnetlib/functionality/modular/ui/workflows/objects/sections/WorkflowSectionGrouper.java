package eu.dnetlib.functionality.modular.ui.workflows.objects.sections;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import com.google.common.collect.Sets;

import eu.dnetlib.functionality.modular.ui.workflows.objects.MetaWorkflowDescriptor;
import eu.dnetlib.functionality.modular.ui.workflows.util.ISLookupClient;
import eu.dnetlib.miscutils.datetime.DateUtils;

public class WorkflowSectionGrouper {

	private Set<String> cache = Sets.newHashSet();
	private long cacheDate = 0;
	private static final long CACHE_DURATION = 600000; // 10 minutes
	
	@Resource
	private ISLookupClient isLookupClient;
	
	
	public List<MetaWorkflowDescriptor> listMetaWorflowsForDatasource(String dsId) {
		return isLookupClient.listMetaWorflowsForDatasource(dsId);
	}

	public List<MetaWorkflowDescriptor> listMetaWorflowsForSection(String name) {
		return isLookupClient.listMetaWorflowsForSection(name);
	}
	
	public String getDatasourceName(String dsId) {
		return isLookupClient.getDatasourceName(dsId);
	}

	public Set<String> getAllSectionNames() {
		synchronized (cache) {
			if (cache.isEmpty() || DateUtils.now() - cacheDate > CACHE_DURATION) {
				cache.clear();
				cache.addAll(isLookupClient.listSimpleWorflowSections());
				cacheDate = DateUtils.now();
			}
		}
		
		final Set<String> res = Sets.newHashSet(cache);
				
		res.remove("dataproviders");
	
		return res;
	}
	
}
