package eu.dnetlib.functionality.modular.ui.is.objects;

public class CollectionTypeDesc implements Comparable<CollectionTypeDesc> {
	private String name;
	private int size;
	
	public CollectionTypeDesc() {}
	
	public CollectionTypeDesc(String name, int size) {
		this.name = name;
		this.size = size;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getSize() {
		return size;
	}
	
	public void setSize(int size) {
		this.size = size;
	}

	@Override
	public int compareTo(CollectionTypeDesc o) {
		return name.compareTo(o.getName());
	}

}
