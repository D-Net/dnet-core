package eu.dnetlib.functionality.modular.ui.repositories.objects;

public class RepoMetaWfEntry implements Comparable<RepoMetaWfEntry> {

	private String id;
	private String name;
	private String status;
	private String destroyWorkflow;
	private int progress;

	public RepoMetaWfEntry(final String id, final String name, final String status, final String destroyWorkflow, final int progress) {
		this.id = id;
		this.name = name;
		this.status = status;
		this.destroyWorkflow = destroyWorkflow;
		this.progress = progress;
	}

	@Override
	public int compareTo(final RepoMetaWfEntry o) {
		return getName().compareTo(o.getName());
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getStatus() {
		return status;
	}

	public String getDestroyWorkflow() {
		return destroyWorkflow;
	}

	public int getProgress() {
		return progress;
	}

}
