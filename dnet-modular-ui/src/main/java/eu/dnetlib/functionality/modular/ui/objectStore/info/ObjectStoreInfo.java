package eu.dnetlib.functionality.modular.ui.objectStore.info;

import eu.dnetlib.data.objectstore.rmi.ObjectStoreFile;

import java.util.List;

/**
 * Created by Sandro La Bruzzo on 11/19/15.
 */
public class ObjectStoreInfo {

    private String objectStoreId;

    private int size;

    private String interpretation;

    private List<ObjectStoreFile> results;

    private String lastStorageDate;

    private String serviceURI;

    private boolean missingProfile = true;

    private boolean missingObjectStore = true;

    public boolean isMissingProfile() {
        return missingProfile;
    }

    public void setMissingProfile(boolean missingProfile) {
        this.missingProfile = missingProfile;
    }

    public boolean isMissingObjectStore() {
        return missingObjectStore;
    }

    public void setMissingObjectStore(boolean missingObjectStore) {
        this.missingObjectStore = missingObjectStore;
    }

    public String getLastStorageDate() {
        return lastStorageDate;
    }

    public void setLastStorageDate(String lastStorageDate) {
        this.lastStorageDate = lastStorageDate;
    }

    public String getServiceURI() {
        return serviceURI;
    }

    public void setServiceURI(String serviceURI) {
        this.serviceURI = serviceURI;
    }

    public List<ObjectStoreFile> getResults() {
        return results;
    }

    public void setResults(List<ObjectStoreFile> results) {
        this.results = results;
    }

    public String getObjectStoreId() {
        return objectStoreId;
    }

    public void setObjectStoreId(String objectStoreId) {
        this.objectStoreId = objectStoreId;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }


    public String getInterpretation() {
        return interpretation;
    }

    public void setInterpretation(String interpretation) {
        this.interpretation = interpretation;
    }
}
