package eu.dnetlib.functionality.modular.ui.workflows.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import eu.dnetlib.functionality.modular.ui.ModuleEntryPoint;

public class WfHistoryEntryPointController extends ModuleEntryPoint {

	@Override
	protected void initialize(final ModelMap map, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		if (request.getParameterMap().containsKey("procId")) {
			map.addAttribute("procId", request.getParameter("procId"));
		}
		if (request.getParameterMap().containsKey("family")) {
			map.addAttribute("family", request.getParameter("family"));
		}
	}

}
