package eu.dnetlib.functionality.modular.ui;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import eu.dnetlib.functionality.modular.ui.error.ErrorMessage;

public abstract class AbstractAjaxController {
	
	@ExceptionHandler(Exception.class)
	@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody ErrorMessage handleException(final Exception e) {
		return ErrorMessage.newInstance(e);
	}

}
