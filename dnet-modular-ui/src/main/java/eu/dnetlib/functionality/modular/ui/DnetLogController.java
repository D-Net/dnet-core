package eu.dnetlib.functionality.modular.ui;

import java.io.File;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.FileAppender;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.ui.ModelMap;

public class DnetLogController extends ModuleEntryPoint {

	@Override
	protected void initialize(final ModelMap map, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		map.addAttribute("logFile", getLogFileName());
	}

	public static String getLogFileName() {
		final Logger logger = LogManager.getRootLogger();
		final Enumeration<?> appenders = logger.getAllAppenders();
		while (appenders.hasMoreElements()) {
			final Object app = appenders.nextElement();
			if (app instanceof FileAppender) { return new File(((FileAppender) app).getFile()).getAbsolutePath(); }
		}
		return "";
	}
}
