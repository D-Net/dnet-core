package eu.dnetlib.functionality.modular.ui.workflows.values;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import com.google.common.collect.Lists;

import eu.dnetlib.msro.workflows.util.ValidNodeValuesFetcher;

public class ListFilesValues extends ValidNodeValuesFetcher {

	private final ResourcePatternResolver pathResolver = new PathMatchingResourcePatternResolver();

	@Override
	protected List<DnetParamValue> obtainValues(final Map<String, String> params) throws Exception {
		verifyParams(params, "path", "ext");

		final String pathTmp = params.get("path");

		final String path = pathTmp.startsWith("/") ? pathTmp : "/" + pathTmp;

		final List<DnetParamValue> values = Lists.newArrayList();
		for (Resource r : pathResolver.getResources("classpath*:" + path + "/*." + params.get("ext"))) {
			values.add(new DnetParamValue(path + File.separator + r.getFilename(), r.getFilename()));
		}

		return values;
	}

}
