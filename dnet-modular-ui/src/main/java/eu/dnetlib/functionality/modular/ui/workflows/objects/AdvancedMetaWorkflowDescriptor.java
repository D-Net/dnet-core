package eu.dnetlib.functionality.modular.ui.workflows.objects;

import java.util.Set;

import com.google.common.collect.Sets;

import eu.dnetlib.msro.workflows.util.WorkflowsConstants.WorkflowStatus;

public class AdvancedMetaWorkflowDescriptor extends MetaWorkflowDescriptor {
	private String email;
	private boolean scheduled;
	private String cronExpression;
	private int minInterval;
	private Set<String> innerWfs = Sets.newHashSet();
	private String html;

	public AdvancedMetaWorkflowDescriptor(final String id, final String name, final String email,
			final boolean scheduled, final String cronExpression, final int minInterval, final WorkflowStatus statusEnum, final String family, final Set<String> innerWfs, final String html) {
		super(id, name, statusEnum, family);
		this.email = email;
		this.scheduled = scheduled;
		this.cronExpression = cronExpression;
		this.minInterval = minInterval;
		this.innerWfs = innerWfs;
		this.setHtml(html);
	}

	public boolean isScheduled() {
		return scheduled;
	}

	public void setScheduled(final boolean scheduled) {
		this.scheduled = scheduled;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(final String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public int getMinInterval() {
		return minInterval;
	}

	public void setMinInterval(final int minInterval) {
		this.minInterval = minInterval;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}
	
	public Set<String> getInnerWfs() {
		return innerWfs;
	}

	public void setInnerWfs(Set<String> innerWfs) {
		this.innerWfs = innerWfs;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

}
