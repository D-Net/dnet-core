package eu.dnetlib.functionality.modular.ui.workflows.sarasvati.icons;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import com.googlecode.sarasvati.JoinType;
import com.googlecode.sarasvati.Node;
import com.googlecode.sarasvati.visual.icon.AbstractNodeIcon;

public abstract class AbstractIcon extends AbstractNodeIcon {

	/**
	 * corner radius.
	 */
	protected static final int ROUND_CORNER = 10;

	/**
	 * length of the space between dashes.
	 */
	protected static final int DASH_EMPTY = 4;

	/**
	 * length of the dash.
	 */
	protected static final int DASH_FULL = 8;

	/**
	 * font size.
	 */
	protected static final int FONT_SIZE = 10;

	/**
	 * node.
	 */
	private Node node;
	
	/**
	 * is join.
	 * 
	 */
	private boolean join;

	/**
	 * node label.
	 */
	private String label;

	/**
	 * node color.
	 */
	private Color color;

	/**
	 * dashes.
	 */
	private float[] dashes;

	/**
	 * true if terminal node.
	 */
	private boolean terminal;

	private boolean failed;

	/**
	 * constructs a blackboard node icon for a given runtime node.
	 * 
	 * @param node
	 *            node
	 * @param token
	 *            node token
	 */
	public AbstractIcon(final Node node) {
		super();

		this.node = node;

		this.label = node.getAdaptor(String.class);
		if (label == null)
			label = node.getName();

		this.join = node.getJoinType() == null ? false : node.getJoinType() != JoinType.OR;
		this.color = Color.lightGray;
		
		if (join)
			dashes = new float[] { DASH_FULL, DASH_EMPTY };

		this.terminal = node.getGraph().getOutputArcs(node).size() == 0;
	}

	
	public void resetGfx(final Graphics2D gfx) {
		gfx.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		gfx.setFont(Font.getFont(Font.MONOSPACED));

		if (isTerminal()) {
			gfx.setFont(gfx.getFont().deriveFont(Font.BOLD));
		}
				
		gfx.setColor(color);
	}

	public boolean isJoin() {
		return join;
	}

	public void setJoin(final boolean join) {
		this.join = join;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(final Color color) {
		this.color = color;
	}

	public float[] getDashes() {
		return dashes; // NOPMD
	}

	public void setDashes(final float[] dashes) { // NOPMD
		this.dashes = dashes;
	}

	public Node getNode() {
		return node;
	}

	public void setNode(final Node node) {
		this.node = node;
	}

	public boolean isTerminal() {
		return terminal;
	}

	public void setTerminal(final boolean terminal) {
		this.terminal = terminal;
	}

	public boolean isFailed() {
		return failed;
	}

	public void setFailed(boolean failed) {
		this.failed = failed;
	}

}
