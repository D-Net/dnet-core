package eu.dnetlib.functionality.modular.ui.workflows.menu;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import eu.dnetlib.functionality.modular.ui.AbstractMenu;
import eu.dnetlib.functionality.modular.ui.MenuEntry;
import eu.dnetlib.functionality.modular.ui.users.AccessLimited;
import eu.dnetlib.functionality.modular.ui.users.PermissionLevel;
import eu.dnetlib.functionality.modular.ui.workflows.objects.sections.WorkflowSectionGrouper;

public class InfrastructureManagementGroup extends AbstractMenu implements AccessLimited {

	private int order;
		
	@Resource
	private WorkflowSectionGrouper workflowSectionGrouper;

	@Override
	public List<MenuEntry> getEntries() {
		final List<String> list = Lists.newArrayList(workflowSectionGrouper.getAllSectionNames());
		
		Collections.sort(list);
		
		List<MenuEntry> res = Lists.newArrayList();
		for(int i=0; i<list.size(); i++) {
			res.add(new WorkflowSectionEntryPoint(list.get(i), i));
		}
		return res;
	}
	
	@Override
	public int getOrder() {
		return order;
	}
	
	public void setOrder(int order) {
		this.order = order;
	}

	@Override
	public Set<PermissionLevel> getPermissionLevels() {
		return Sets.newHashSet(PermissionLevel.WF_ADMIN, PermissionLevel.IS_ADMIN);
	}

}
