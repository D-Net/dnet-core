package eu.dnetlib.functionality.modular.ui.mdstore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import eu.dnetlib.enabling.locators.UniqueServiceLocator;
import eu.dnetlib.functionality.modular.ui.ModuleEntryPoint;

public class MDStoreServiceEntryPointController extends ModuleEntryPoint {

	/** The lookup locator. */
	@Resource
	private UniqueServiceLocator serviceLocator;

	@Override
	protected void initialize(final ModelMap map, final HttpServletRequest request, final HttpServletResponse response) throws Exception {

	}

}
