package eu.dnetlib.functionality.modular.ui.workflows.values;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.google.common.collect.Lists;

import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService;
import eu.dnetlib.enabling.locators.UniqueServiceLocator;
import eu.dnetlib.msro.workflows.util.ValidNodeValuesFetcher;

public class ListProfilesValues extends ValidNodeValuesFetcher {

	@Resource
	private UniqueServiceLocator serviceLocator;

	@Override
	protected List<DnetParamValue> obtainValues(final Map<String, String> params) throws Exception {
		verifyParams(params, "type", "xpath");

		final List<DnetParamValue> values = Lists.newArrayList();

		final String query = "for $x in /*[.//RESOURCE_TYPE/@value='" + params.get("type") + "']" + "return concat($x//RESOURCE_IDENTIFIER/@value, ' @@@ ', $x"
				+ params.get("xpath") + ")";

		final List<String> result = serviceLocator.getService(ISLookUpService.class).quickSearchProfile(query);
		for (String s : result) {
			String[] arr = s.split("@@@");
			values.add(new DnetParamValue(arr[0].trim(), arr[1].trim()));
		}

		return values;
	}

}
