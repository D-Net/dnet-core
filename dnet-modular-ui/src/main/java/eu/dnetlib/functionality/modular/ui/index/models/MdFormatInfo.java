package eu.dnetlib.functionality.modular.ui.index.models;

// TODO: Auto-generated Javadoc
/**
 * The Class MdFormatInfo.
 */
public class MdFormatInfo {

	/** The Constant SplitCharachters. */
	private static final String SplitCharachters = "::";

	/** The id. */
	private String id;

	/** The format. */
	private String format;

	/** The layout. */
	private String layout;

	/** The interpretation. */
	private String interpretation;

	/**
	 * The Constructor.
	 */
	public MdFormatInfo() {

	}

	/**
	 * The Constructor.
	 *
	 * @param id
	 *            the id
	 * @param format
	 *            the format
	 * @param layout
	 *            the layout
	 * @param interpretation
	 *            the interpretation
	 */
	public MdFormatInfo(final String id, final String format, final String layout, final String interpretation) {
		super();
		this.id = id;
		this.format = format;
		this.layout = layout;
		this.interpretation = interpretation;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the id
	 */
	public void setId(final String id) {
		this.id = id;
	}

	/**
	 * Gets the format.
	 *
	 * @return the format
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * Sets the format.
	 *
	 * @param format
	 *            the format
	 */
	public void setFormat(final String format) {
		this.format = format;
	}

	/**
	 * Gets the layout.
	 *
	 * @return the layout
	 */
	public String getLayout() {
		return layout;
	}

	/**
	 * Sets the layout.
	 *
	 * @param layout
	 *            the layout
	 */
	public void setLayout(final String layout) {
		this.layout = layout;
	}

	/**
	 * Gets the interpretation.
	 *
	 * @return the interpretation
	 */
	public String getInterpretation() {
		return interpretation;
	}

	/**
	 * Sets the interpretation.
	 *
	 * @param interpretation
	 *            the interpretation
	 */
	public void setInterpretation(final String interpretation) {
		this.interpretation = interpretation;
	}

	/**
	 * Create a new MdFormatInfo starting from the xquery result the string MUST be in this forma $format-$layout-$interpretation::$id
	 *
	 * @param result
	 *            the result
	 * @return the md format info
	 */
	public static MdFormatInfo initFromXqueryResult(final String result) {
		String[] values = result.split(SplitCharachters);
		if ((values == null) || (values.length != 2)) return null;

		String[] mdref = values[0].split("-");
		if ((mdref == null) || (mdref.length != 3)) return null;

		return new MdFormatInfo(values[1], mdref[0], mdref[1], mdref[2]);

	}

}
