package eu.dnetlib.functionality.modular.ui.workflows.objects;

import java.util.List;

import eu.dnetlib.msro.workflows.util.WorkflowParam;

public class NodeInfo {
	private String name;
	private String description;
	private List<WorkflowParam> params;
		
	public NodeInfo(String name, String description, List<WorkflowParam> params) {
		this.name = name;
		this.description = description;
		this.params = params;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<WorkflowParam> getParams() {
		return params;
	}

	public void setParams(List<WorkflowParam> params) {
		this.params = params;
	}

}
