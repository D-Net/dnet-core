package eu.dnetlib.functionality.modular.ui.workflows.objects;

import com.google.gson.Gson;

public abstract class AbstractWorkflowDescriptor implements Comparable<AbstractWorkflowDescriptor> {

	private String wfId;
	private String name;
	private String cssClass;

	public AbstractWorkflowDescriptor(final String wfId, final String name, final String cssClass) {
		super();
		this.wfId = wfId;
		this.name = name;
		this.cssClass = cssClass;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getCssClass() {
		return cssClass;
	}

	public void setCssClass(final String cssClass) {
		this.cssClass = cssClass;
	}

	public String toJson() {
		return (new Gson()).toJson(this);
	}

	public String getWfId() {
		return wfId;
	}

	public void setWfId(final String wfId) {
		this.wfId = wfId;
	}

	@Override
	public int compareTo(final AbstractWorkflowDescriptor o) {
		return getName().compareTo(o.getName());
	}

}
