package eu.dnetlib.functionality.modular.ui.workflows.sarasvati.viewer;

import java.util.Set;

import javax.swing.Icon;

import com.googlecode.sarasvati.Node;
import com.googlecode.sarasvati.visual.GraphToImageMapAdapter;

import eu.dnetlib.functionality.modular.ui.workflows.sarasvati.icons.GraphIcon;
import eu.dnetlib.msro.workflows.nodes.NodeStatus;

public class GraphToImageMapHelper extends GraphToImageMapAdapter {

	private String workflowId;
	private Set<String> notConfiguredNodes;
	
	public GraphToImageMapHelper(final String workflowId, final Set<String> notConfiguredNodes) {
		super();
		this.workflowId = workflowId;
		this.notConfiguredNodes = notConfiguredNodes;
	}

	@Override
	public String hrefForNode (Node node) {
		if (workflowId == null || workflowId.isEmpty()) {
			return "javascript:alert('TODO')";
		} else if ( node.getName().equals("success") || node.getName().equals("failure")) {
			return "javascript:void(0)";
		} else {
			return "javascript:getScope('map').getNodeDetails('" + workflowId + "', '" + node.getName() + "')";
		}
	}
	
	@Override
	public String hoverForNode (Node node) {
		return "Name: " + node.getName();
	}
	
	@Override
	public Icon iconForNode(Node node) {
		if (node.getName().equals("success") || node.getName().equals("failure") ) {
			return new GraphIcon(node, NodeStatus.SYSTEM);
		} else if (notConfiguredNodes != null && notConfiguredNodes.contains(node.getName())) {
			return new GraphIcon(node, NodeStatus.NOT_CONFIGURED);
		} else {
			return new GraphIcon(node, NodeStatus.CONFIGURED);
		}
	}
}
