package eu.dnetlib.functionality.modular.ui;

import java.util.Set;

import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Required;

import eu.dnetlib.functionality.modular.ui.users.AccessLimited;
import eu.dnetlib.functionality.modular.ui.users.PermissionLevel;

public abstract class MenuEntry implements Comparable<MenuEntry>, AccessLimited {

	private String menu;
	private String title;
	private String description;
	private int order = 50;
	private Set<PermissionLevel> permissionLevels;
		
	@Override
	public int compareTo(MenuEntry o) {
		return NumberUtils.compare(this.getOrder(), o.getOrder());
	}
	
	abstract public String getRelativeUrl();
	
	public String getMenu() {
		return menu;
	}
	
	@Required
	public void setMenu(String menu) {
		this.menu = menu;
	}

	public String getTitle() {
		return title;
	}
	@Required
	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}
	@Required
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public Set<PermissionLevel> getPermissionLevels() {
		return permissionLevels;
	}

	@Required
	public void setPermissionLevels(Set<PermissionLevel> permissionLevels) {
		this.permissionLevels = permissionLevels;
	}
	
	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}
	
}
