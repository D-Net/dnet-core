package eu.dnetlib.functionality.modular.ui.mdstore.model;

import java.util.List;
import java.util.Map;

/**
 * The Class MDStoreInfo.
 */
public class MDStoreInfo {

    private static String DEFAULT_XQUERY_DELIMITER = "@::@";

    /**
     * The id.
     */
    private String id;

    /**
     * The service uri.
     */
    private String serviceURI;

    /**
     * The format.
     */
    private String format;

    /**
     * The layout.
     */
    private String layout;

    /**
     * The interpretation.
     */
    private String interpretation;

    /**
     * The last storage data.
     */
    private String lastStorageDate;

    private Map<String, String> datasourcesInvolved;

    private List<String> indexFields;

    /**
     * The size.
     */
    private int size;

    public static MDStoreInfo fromXqueryResult(final String result) {
        MDStoreInfo info = new MDStoreInfo();
        String values[] = result.split(DEFAULT_XQUERY_DELIMITER);
        if (values == null || values.length != 5) {
            return null;
        }
        info.setServiceURI(values[0]);
        info.setId(values[1]);
        String mdFormat[] = values[2].split("\\(-\\)");
        if (mdFormat == null || mdFormat.length != 3) {
            return null;
        }
        info.setFormat(mdFormat[0]);
        info.setLayout(mdFormat[1]);
        info.setInterpretation(mdFormat[2]);
        info.setLastStorageDate(values[3]);
        info.setSize(new Integer(values[4]));
        return info;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * Gets the service uri.
     *
     * @return the service uri
     */
    public String getServiceURI() {
        return serviceURI;
    }

    /**
     * Sets the service uri.
     *
     * @param serviceURI the new service uri
     */
    public void setServiceURI(final String serviceURI) {
        this.serviceURI = serviceURI;
    }

    /**
     * Gets the format.
     *
     * @return the format
     */
    public String getFormat() {
        return format;
    }

    /**
     * Sets the format.
     *
     * @param format the new format
     */
    public void setFormat(final String format) {
        this.format = format;
    }

    /**
     * Gets the layout.
     *
     * @return the layout
     */
    public String getLayout() {
        return layout;
    }

    /**
     * Sets the layout.
     *
     * @param layout the new layout
     */
    public void setLayout(final String layout) {
        this.layout = layout;
    }

    /**
     * Gets the interpretation.
     *
     * @return the interpretation
     */
    public String getInterpretation() {
        return interpretation;
    }

    /**
     * Sets the interpretation.
     *
     * @param interpretation the new interpretation
     */
    public void setInterpretation(final String interpretation) {
        this.interpretation = interpretation;
    }

    /**
     * Gets the last storage data.
     *
     * @return the last storage data
     */
    public String getLastStorageDate() {
        return lastStorageDate;
    }

    /**
     * Sets the last storage data.
     *
     * @param lastStorageData the new last storage data
     */
    public void setLastStorageDate(final String lastStorageDate) {
        this.lastStorageDate = lastStorageDate;
    }

    /**
     * Gets the size.
     *
     * @return the size
     */
    public int getSize() {
        return size;
    }

    /**
     * Sets the size.
     *
     * @param size the new size
     */
    public void setSize(final int size) {
        this.size = size;
    }

    public Map<String, String> getDatasourcesInvolved() {
        return datasourcesInvolved;
    }

    public void setDatasourcesInvolved(Map<String, String> datasourcesInvolved) {
        this.datasourcesInvolved = datasourcesInvolved;
    }

    public List<String> getIndexFields() {
        return indexFields;
    }

    public void setIndexFields(List<String> indexFields) {
        this.indexFields = indexFields;
    }
}
