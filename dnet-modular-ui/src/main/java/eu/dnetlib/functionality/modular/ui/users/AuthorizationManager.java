package eu.dnetlib.functionality.modular.ui.users;

import javax.servlet.http.HttpServletRequest;

public interface AuthorizationManager {
	User obtainUserDetails(HttpServletRequest request);
}
