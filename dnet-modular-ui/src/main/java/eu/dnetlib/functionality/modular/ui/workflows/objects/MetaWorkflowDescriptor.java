package eu.dnetlib.functionality.modular.ui.workflows.objects;

import eu.dnetlib.msro.workflows.util.WorkflowsConstants.WorkflowStatus;

public class MetaWorkflowDescriptor extends AbstractWorkflowDescriptor {

	private WorkflowStatus statusEnum;
	private String familyName;

	public MetaWorkflowDescriptor(final String id, final String name, final WorkflowStatus statusEnum, final String familyName) {
		super(id, name, "");
		this.statusEnum = statusEnum;
		this.familyName = familyName;
	}

	public String getStatus() {
		return statusEnum.displayName;
	}

	public WorkflowStatus getStatusEnum() {
		return statusEnum;
	}

	public void setStatusEnum(final WorkflowStatus statusEnum) {
		this.statusEnum = statusEnum;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(final String familyName) {
		this.familyName = familyName;
	}

}
