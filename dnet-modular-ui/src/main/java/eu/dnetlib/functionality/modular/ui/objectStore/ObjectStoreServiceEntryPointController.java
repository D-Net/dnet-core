/**
 * Created by Sandro La Bruzzo on 11/18/15.
 */

package eu.dnetlib.functionality.modular.ui.objectStore;

import eu.dnetlib.functionality.modular.ui.ModuleEntryPoint;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ObjectStoreServiceEntryPointController extends ModuleEntryPoint {
    @Override
    protected void initialize(ModelMap modelMap, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {

    }
}
