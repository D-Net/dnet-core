package eu.dnetlib.functionality.modular.ui.error;

import org.apache.commons.lang.exception.ExceptionUtils;

public class ErrorMessage {
	private String message;
	private String stacktrace;
	
	public ErrorMessage() {}
	
	public static ErrorMessage newInstance(final Exception e) {
		return new ErrorMessage(e.getMessage(), ExceptionUtils.getStackTrace(e));
	}
 	
	public ErrorMessage(final String message, final String stacktrace) {
    	this.message = message;
    	this.stacktrace = stacktrace;
    }

	public String getMessage() {
		return message;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

	public String getStacktrace() {
		return stacktrace;
	}

	public void setStacktrace(final String stacktrace) {
		this.stacktrace = stacktrace;
	}
}