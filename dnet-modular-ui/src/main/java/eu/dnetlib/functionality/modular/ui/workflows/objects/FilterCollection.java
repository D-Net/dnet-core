package eu.dnetlib.functionality.modular.ui.workflows.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FilterCollection extends HashMap<String, List<FilterElem>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4856177328627952130L;

	public FilterCollection() {
		super();
	}

	public void addElement(String collection, FilterElem elem) {
		if (!this.containsKey(collection)) {
			this.put(collection, new ArrayList<FilterElem>());
		}
		this.get(collection).add(elem);
	}
	
	public void addElement(String collection, String name, String icon, String value) {
		addElement(collection, new FilterElem(name, icon, value));
	}
	
}
