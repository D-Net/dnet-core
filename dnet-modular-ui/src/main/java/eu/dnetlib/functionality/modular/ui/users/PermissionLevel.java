package eu.dnetlib.functionality.modular.ui.users;

public enum PermissionLevel {
	SUPER_ADMIN("Super admin", "Authenticated user with ALL management rights"), 
	DS_ADMIN ("datasource administrator", "Authenticated user that can manage datasources"), 
	WF_ADMIN("workflow administrator", "Authenticated user that can manage workflows"),
	IS_ADMIN("information administrator", "Authenticated user that can update IS profiles"),
	USER("simple user", "Authenticated user with no management rights"), 
	GUEST ("guest", "Anonymous user");

	private String label;
	private String details;
	
	private PermissionLevel(final String label, final String details) {
		this.label = label;
		this.details = details;
	}

	public String getLabel() {
		return label;
	}

	public String getDetails() {
		return details;
	}


}
