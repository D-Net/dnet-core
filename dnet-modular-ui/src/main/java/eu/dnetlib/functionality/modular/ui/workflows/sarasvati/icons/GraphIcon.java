package eu.dnetlib.functionality.modular.ui.workflows.sarasvati.icons;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

import com.googlecode.sarasvati.Node;
import com.googlecode.sarasvati.visual.common.NodeDrawConfig;
import com.googlecode.sarasvati.visual.util.FontUtil;

import eu.dnetlib.msro.workflows.nodes.NodeStatus;

public class GraphIcon extends AbstractIcon {
	private Color bgColor;
	
	public GraphIcon(final Node node, final NodeStatus status) {
		super(node);
		
		switch (status) {
		case CONFIGURED:
			this.bgColor = Color.decode("#0088cc");
			break;
		case NOT_CONFIGURED:
			this.bgColor = Color.decode("#dd8800");
			break;
		case SYSTEM:
			this.bgColor = Color.decode("#9999dd");
			break;
		default:
			this.bgColor = Color.decode("#aabbcc");
			break;
		}
		
		redrawImage();
	}
	
	@Override
	public void redrawImage(final Graphics2D gfx) {
		super.resetGfx(gfx);
		
		gfx.setColor(bgColor);
		
		gfx.fillOval(0, 0, WIDTH - 1, HEIGHT - 1);

		gfx.setColor(NodeDrawConfig.NODE_BORDER);

		int offset = 1;

		final BasicStroke stroke = new BasicStroke(2, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER, 10, getDashes(), 0);
		gfx.setStroke(stroke);

		final int width = WIDTH - ((offset << 1) + 1);
		final int height = HEIGHT - ((offset << 1) + 1);

		gfx.drawOval(offset, offset, width, height);
		offset += 2 + 1;

		gfx.setColor(Color.white);

		final int padding = 2 + offset;
		final int startX = padding;

		final int maxWidth = getIconWidth() - (padding << 1);

		FontUtil.setSizedFont(gfx, getLabel(), FONT_SIZE, maxWidth);
		final int strWidth = (int) Math.ceil(gfx.getFontMetrics().getStringBounds(getLabel(), gfx).getWidth());
		final int strHeight = gfx.getFontMetrics().getAscent();
		final int left = startX + ((maxWidth - strWidth) >> 1);
		final int top = ((getIconHeight() + strHeight) >> 1);
		gfx.drawString(getLabel(), left, top);
		
		super.resetGfx(gfx);
	}

	
	
	
}
