package eu.dnetlib.functionality.modular.ui.is.objects;

public class BlackboardMessage {
	private String profId;
	private String messageId;
	private String resourceType; 
	private String action;
	private String date;
	private String actionStatus;
	private String error;
	
	public BlackboardMessage() {}
	
	public BlackboardMessage(String profId, String messageId, String resourceType,	String action, String date, String actionStatus, String error) {
		this.profId = profId;
		this.messageId = messageId;
		this.resourceType = resourceType;
		this.action = action;
		this.date = date;
		this.actionStatus = actionStatus;
		this.error = error;
	}

	public String getProfId() {
		return profId;
	}

	public void setProfId(String profId) {
		this.profId = profId;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getActionStatus() {
		return actionStatus;
	}

	public void setActionStatus(String actionStatus) {
		this.actionStatus = actionStatus;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
