package eu.dnetlib.functionality.modular.ui.is;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import eu.dnetlib.functionality.modular.ui.ModuleEntryPoint;

public class InformationServiceEntryPointController extends ModuleEntryPoint {

	@Override
	protected void initialize(final ModelMap map, final HttpServletRequest request, final HttpServletResponse response) throws Exception {

	}

}
