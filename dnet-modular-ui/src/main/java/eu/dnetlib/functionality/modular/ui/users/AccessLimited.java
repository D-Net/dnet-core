package eu.dnetlib.functionality.modular.ui.users;

import java.util.Set;

public interface AccessLimited {
	public Set<PermissionLevel> getPermissionLevels();
}
