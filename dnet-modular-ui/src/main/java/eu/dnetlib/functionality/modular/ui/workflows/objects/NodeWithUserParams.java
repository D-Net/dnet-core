package eu.dnetlib.functionality.modular.ui.workflows.objects;

import java.util.List;

import eu.dnetlib.msro.workflows.util.WorkflowParam;

public class NodeWithUserParams {
	private String node;
	private String desc;
	private List<WorkflowParam> params;
		
	public NodeWithUserParams() {}
	
	public NodeWithUserParams(final String node, final String desc,	final List<WorkflowParam> params) {
		this.node = node;
		this.desc = desc;
		this.params = params;
	}

	public String getNode() {
		return node;
	}
	
	public void setNode(final String node) {
		this.node = node;
	}
	
	public String getDesc() {
		return desc;
	}
	
	public void setDesc(final String desc) {
		this.desc = desc;
	}
	
	public List<WorkflowParam> getParams() {
		return params;
	}
	
	public void setParams(final List<WorkflowParam> params) {
		this.params = params;
	}
	
}
