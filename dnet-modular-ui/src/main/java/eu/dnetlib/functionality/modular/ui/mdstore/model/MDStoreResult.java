package eu.dnetlib.functionality.modular.ui.mdstore.model;

import java.util.List;

/**
 * Created by sandro on 12/2/16.
 */
public class MDStoreResult {

    private int count;

    private List<String> result;

    public MDStoreResult() {

    }

    public MDStoreResult(int count, List<String> result) {
        this.count = count;
        this.result = result;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<String> getResult() {
        return result;
    }

    public void setResult(List<String> result) {
        this.result = result;
    }
}
