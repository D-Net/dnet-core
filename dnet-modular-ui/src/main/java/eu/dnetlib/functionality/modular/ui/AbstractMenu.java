package eu.dnetlib.functionality.modular.ui;

import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Required;

public abstract class AbstractMenu implements Comparable<AbstractMenu> {
	private String title;		

	abstract public int getOrder();

	abstract public List<? extends MenuEntry> getEntries();
	
	@Override
	public int compareTo(final AbstractMenu menu) {
		return NumberUtils.compare(getOrder(), menu.getOrder());
	}

	public String getTitle() {
		return title;
	}
	
	@Required
	public void setTitle(String title) {
		this.title = title;
	}
}



