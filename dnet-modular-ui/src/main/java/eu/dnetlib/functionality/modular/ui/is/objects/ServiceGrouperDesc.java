package eu.dnetlib.functionality.modular.ui.is.objects;

import java.util.List;

public class ServiceGrouperDesc {

	private String host;
	private int port;
	private String context;
	private List<ServiceDesc> services;

	public ServiceGrouperDesc() {}

	public ServiceGrouperDesc(final String host, final int port, final String context, final List<ServiceDesc> services) {
		this.host = host;
		this.port = port;
		this.context = context;
		this.services = services;
	}

	public String getHost() {
		return host;
	}

	public void setHost(final String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(final int port) {
		this.port = port;
	}

	public String getContext() {
		return context;
	}

	public void setContext(final String context) {
		this.context = context;
	}

	public List<ServiceDesc> getServices() {
		return services;
	}

	public void setServices(final List<ServiceDesc> services) {
		this.services = services;
	}
}
