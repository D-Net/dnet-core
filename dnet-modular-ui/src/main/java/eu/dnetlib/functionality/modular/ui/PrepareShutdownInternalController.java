package eu.dnetlib.functionality.modular.ui;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import eu.dnetlib.enabling.common.StoppableDetails;
import eu.dnetlib.functionality.modular.ui.utils.ShutdownUtils;

@Controller
public class PrepareShutdownInternalController {

	@Resource
	private ShutdownUtils shutdownUtils;

	@RequestMapping("/ui/shutdown/listStoppableDetails.json")
	public @ResponseBody
	List<StoppableDetails> listStoppableDetails() {
		return shutdownUtils.listStoppableDetails();
	}

	@RequestMapping("/ui/shutdown/stopAll.do")
	public @ResponseBody
	boolean stopAll() {
		shutdownUtils.stopAll();
		return true;
	}

	@RequestMapping("/ui/shutdown/resumeAll.do")
	public @ResponseBody
	boolean resumeAll() {
		shutdownUtils.resumeAll();
		return true;
	}

}
