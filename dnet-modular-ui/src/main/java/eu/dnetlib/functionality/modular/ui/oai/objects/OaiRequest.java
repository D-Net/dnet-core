package eu.dnetlib.functionality.modular.ui.oai.objects;

import com.google.common.collect.Lists;
import org.apache.http.message.BasicNameValuePair;

import java.util.List;

public class OaiRequest {
	
	private String baseUrl; 
	private String verb;
	private String mdf;
	private String set;
	private String id;
	private String token;
	
	public String getBaseUrl() {
		return baseUrl;
	}
	
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	
	public String getVerb() {
		return verb;
	}
	
	public void setVerb(String verb) {
		this.verb = verb;
	}
	
	public String getMdf() {
		return mdf;
	}
	
	public void setMdf(String mdf) {
		this.mdf = mdf;
	}
	
	public String getSet() {
		return set;
	}
	
	public void setSet(String set) {
		this.set = set;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getToken() {
		return token;
	}
	
	public void setToken(String token) {
		this.token = token;
	}

	public List<BasicNameValuePair> toQueryParams() {
		final List<BasicNameValuePair> params = Lists.newArrayList();
		
		if (verb != null && !verb.isEmpty()) {
			params.add(new BasicNameValuePair("verb", verb));
		}
		if (mdf != null && !mdf.isEmpty()) {
			params.add(new BasicNameValuePair("metadataPrefix", mdf));
		}
		if (set != null && !set.isEmpty()) {
			params.add(new BasicNameValuePair("set", set));
		}
		if (id != null && !id.isEmpty()) {
			params.add(new BasicNameValuePair("identifier", id));
		}
		if (token != null && !token.isEmpty()) {
			params.add(new BasicNameValuePair("resumptionToken", token));
		}
		
		return params;
	}
}
