package eu.dnetlib.functionality.modular.ui.users;

import javax.servlet.http.HttpServletRequest;

import com.google.common.collect.Sets;

public class MockAuthorizationManager implements AuthorizationManager {

	@Override
	public User obtainUserDetails(HttpServletRequest request) {
		final User userMock = new User();
		
		userMock.setId("mock");
		userMock.setFullname("Mock User");
		userMock.setPermissionLevels(Sets.newHashSet(PermissionLevel.SUPER_ADMIN));
		userMock.setEmail("mock@user.foo");
		return userMock;
	}

}
