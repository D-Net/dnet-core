package eu.dnetlib.functionality.modular.ui.users;

import java.util.Map;
import java.util.Set;

public interface AuthorizationDAO {
	void updatePermissionLevels(Map<String, Set<PermissionLevel>> map);
	Map<String, Set<PermissionLevel>> getPermissionLevels();
	Set<PermissionLevel> getPermissionLevels(String uid);
}
