package eu.dnetlib.functionality.modular.ui.repositories.objects;

public class VocabularyEntry implements Comparable<VocabularyEntry> {

	private String name;
	private String desc;

	public VocabularyEntry() {}

	public VocabularyEntry(final String name, final String desc) {
		this.name = name;
		this.desc = desc;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(final String desc) {
		this.desc = desc;
	}

	@Override
	public int compareTo(final VocabularyEntry o) {
		return getName().compareTo(o.getName());
	}

}
