package eu.dnetlib.functionality.modular.ui.is.objects;

import eu.dnetlib.enabling.is.sn.resourcestate.ResourceStateSubscription;

public class SubscriptionDesc {

	private String id;
	private String prefix;
	private String type;
	private String resourceId;
	private String xpath;

	public SubscriptionDesc() {}

	public SubscriptionDesc(final ResourceStateSubscription sub) {
		this(sub.getSubscriptionId(), sub.getPrefix(), sub.getType(), sub.getResourceId(), sub.getXpath());
	}

	public SubscriptionDesc(final String id, final String prefix, final String type, final String resourceId, final String xpath) {
		this.id = id;
		this.prefix = prefix;
		this.type = type;
		this.resourceId = resourceId;
		this.xpath = xpath;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(final String prefix) {
		this.prefix = prefix;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(final String resourceId) {
		this.resourceId = resourceId;
	}

	public String getXpath() {
		return xpath;
	}

	public void setXpath(final String xpath) {
		this.xpath = xpath;
	}

}
