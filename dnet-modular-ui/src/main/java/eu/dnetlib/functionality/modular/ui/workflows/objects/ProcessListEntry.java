package eu.dnetlib.functionality.modular.ui.workflows.objects;

import java.util.Date;

import com.googlecode.sarasvati.GraphProcess;

import eu.dnetlib.msro.workflows.util.ProcessUtils;

public class ProcessListEntry {
	private String procId;
	private String wfId;
	private String name;
	private String family;
	private String status;
	private long date;
	private String repo;
	
	public ProcessListEntry() {}

	public ProcessListEntry(String procId, String wfId, String name, String family, String status, long date, String repo) {
		this.procId = procId;
		this.wfId = wfId;
		this.name = name;
		this.family = family;
		this.status = status;
		this.date = date;
		this.repo = repo;
	}

	public ProcessListEntry(String procId, String wfId, String name, String family, String status, Date date, String repo) {
		this(procId, wfId, name, family, status, dateToLong(date), repo);
	}
	
	public ProcessListEntry(final String procId, final GraphProcess process) {
		this(procId, 
				ProcessUtils.calculateWfId(process), 
				ProcessUtils.calculateName(process),
				ProcessUtils.calculateFamily(process),
				ProcessUtils.calculateStatus(process),
				ProcessUtils.calculateLastActivityDate(process),
				ProcessUtils.calculateRepo(process));
	}

	public String getProcId() {
		return procId;
	}

	public void setProcId(String procId) {
		this.procId = procId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFamily() {
		return family;
	}

	public String getWfId() {
		return wfId;
	}

	public void setWfId(String wfId) {
		this.wfId = wfId;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public String getRepo() {
		return repo;
	}

	public void setRepo(String repo) {
		this.repo = repo;
	}
	
	private static long dateToLong(Date date) {
		return (date == null) ? Long.MAX_VALUE : date.getTime();
	}

}
