package eu.dnetlib.functionality.modular.ui.workflows.objects;

public class AtomicWorkflowDescriptor extends AbstractWorkflowDescriptor implements WfGraphProvider {

	private String mapContent;
	private String status;
	private String imageUrl;
	private boolean ready;
	private String start;
	private String lastExecutionDate;
	private long startDate;
	private long endDate;

	public AtomicWorkflowDescriptor(final String id, final String name, final String status, final String mapContent,
			final String imageUrl, final boolean ready, final String start, final String lastExecutionDate, long startDate, long endDate) {
		super(id, name, "");
		this.status = status;
		this.mapContent = mapContent;
		this.imageUrl = imageUrl;
		this.ready = ready;
		this.start = start;
		this.lastExecutionDate = lastExecutionDate;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public long getStartDate() {
		return startDate;
	}

	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}

	public long getEndDate() {
		return endDate;
	}

	public void setEndDate(long endDate) {
		this.endDate = endDate;
	}

	public void setMapContent(final String mapContent) {
		this.mapContent = mapContent;
	}

	public void setImageUrl(final String imageUrl) {
		this.imageUrl = imageUrl;
	}

	@Override
	public String getMapContent() {
		return mapContent;
	}

	@Override
	public String getImageUrl() {
		return imageUrl;
	}

	public boolean isReady() {
		return ready;
	}

	public void setReady(final boolean ready) {
		this.ready = ready;
	}

	public String getLastExecutionDate() {
		return lastExecutionDate;
	}

	public void setLastExecutionDate(final String lastExecutionDate) {
		this.lastExecutionDate = lastExecutionDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public String getStart() {
		return start;
	}

	public void setStart(final String start) {
		this.start = start;
	}

}
