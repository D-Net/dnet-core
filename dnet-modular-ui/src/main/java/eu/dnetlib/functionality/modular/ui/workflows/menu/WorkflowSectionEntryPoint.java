package eu.dnetlib.functionality.modular.ui.workflows.menu;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import eu.dnetlib.functionality.modular.ui.MenuEntry;

public class WorkflowSectionEntryPoint extends MenuEntry {

	public WorkflowSectionEntryPoint(String s, int order) {
		setTitle(s);
		setMenu(s);
		setDescription(s);
		setOrder(order);
	}

	@Override
	public String getRelativeUrl() {
		try {
			return "/ui/workflows.do?section=" + URLEncoder.encode(getMenu(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			return "javascript:void(0)";
		}
	}

	

}
