package eu.dnetlib.functionality.modular.ui;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

public class MainController extends ModuleEntryPoint {

	@Override
	protected void initialize(final ModelMap map, final HttpServletRequest request, final HttpServletResponse response) {}

}
