package eu.dnetlib.functionality.modular.ui.index.models;

/**
 * The Class IndexInfo.
 */
public class IndexInfo {

	/** The Constant SEPARATOR. */
	private static final String SEPARATOR = ":-:";

	/** The id. */
	private String id;

	/** The forma. */
	private String format;

	/** The layout. */
	private String layout;

	/** The interpretation. */
	private String interpretation;

	/** The backend id. */
	private String backendId;

	private int size;

	/**
	 * The Constructor.
	 */
	public IndexInfo() {

	}

	/**
	 * The Constructor.
	 *
	 * @param id
	 *            the id
	 * @param forma
	 *            the forma
	 * @param layout
	 *            the layout
	 * @param interpretation
	 *            the interpretation
	 * @param backendId
	 *            the backend id
	 */
	public IndexInfo(final String id, final String forma, final String layout, final String interpretation, final String backendId) {
		super();
		this.id = id;
		this.format = forma;
		this.layout = layout;
		this.interpretation = interpretation;
		this.backendId = backendId;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the id
	 */
	public void setId(final String id) {
		this.id = id;
	}

	/**
	 * Gets the forma.
	 *
	 * @return the forma
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * Sets the forma.
	 *
	 * @param forma
	 *            the forma
	 */
	public void setFormat(final String format) {
		this.format = format;
	}

	/**
	 * Gets the layout.
	 *
	 * @return the layout
	 */
	public String getLayout() {
		return layout;
	}

	/**
	 * Sets the layout.
	 *
	 * @param layout
	 *            the layout
	 */
	public void setLayout(final String layout) {
		this.layout = layout;
	}

	/**
	 * Gets the interpretation.
	 *
	 * @return the interpretation
	 */
	public String getInterpretation() {
		return interpretation;
	}

	/**
	 * Sets the interpretation.
	 *
	 * @param interpretation
	 *            the interpretation
	 */
	public void setInterpretation(final String interpretation) {
		this.interpretation = interpretation;
	}

	/**
	 * Gets the backend id.
	 *
	 * @return the backend id
	 */
	public String getBackendId() {
		return backendId;
	}

	/**
	 * Sets the backend id.
	 *
	 * @param backendId
	 *            the backend id
	 */
	public void setBackendId(final String backendId) {
		this.backendId = backendId;
	}

	public int getSize() {
		return size;
	}

	public void setSize(final int size) {
		this.size = size;
	}

	/**
	 * New instance from string. the string must have the form format:-:layout:-:interpretation:-:id:-:backendid:-:size
	 *
	 * @param serialized
	 *            the serialized
	 * @return the index info
	 */
	public static IndexInfo newInstanceFromString(final String serialized) {
		String[] values = serialized.split(SEPARATOR);
		if ((values == null) || (values.length != 6)) return null;
		IndexInfo tmp = new IndexInfo();
		tmp.format = values[0];
		tmp.layout = values[1];
		tmp.interpretation = values[2];
		tmp.id = values[3];
		tmp.backendId = values[4];
		tmp.size = Integer.parseInt(values[5]);
		return tmp;

	}
}
