package eu.dnetlib.functionality.modular.ui.index;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Resource;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import eu.dnetlib.data.provision.index.rmi.IndexServiceException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService;
import eu.dnetlib.enabling.locators.UniqueServiceLocator;
import eu.dnetlib.functionality.index.client.IndexClient;
import eu.dnetlib.functionality.index.client.IndexClientException;
import eu.dnetlib.functionality.index.client.response.BrowseEntry;
import eu.dnetlib.functionality.index.client.response.LookupResponse;
import eu.dnetlib.functionality.index.client.solr.SolrIndexClientFactory;
import eu.dnetlib.functionality.modular.ui.AbstractAjaxController;
import eu.dnetlib.functionality.modular.ui.index.models.IndexInfo;
import eu.dnetlib.functionality.modular.ui.index.models.MdFormatInfo;
import eu.dnetlib.miscutils.functional.xml.ApplyXslt;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * The Class IndexServiceInternalController.
 */
@Controller
public class IndexServiceInternalController extends AbstractAjaxController {

	/** The Constant log. */
	private static final Log log = LogFactory.getLog(IndexServiceInternalController.class);

	/** The lookup locator. */
	@Resource
	private UniqueServiceLocator serviceLocator;

	/** The index client factory. */
	@Autowired
	private SolrIndexClientFactory indexClientFactory;

	@Autowired
	private IndexClientMap clientMap;

	/**
	 * Index metadata formats.
	 *
	 * @param map
	 *            the map
	 * @return the list< md format info>
	 * @throws Exception
	 *             the exception
	 */
	@RequestMapping(value = "/ui/index/indexMetadataFormats.do")
	@ResponseBody
	public List<MdFormatInfo> indexMetadataFormats(final ModelMap map) throws Exception {
		final String xquery =
				"for $x in //RESOURCE_PROFILE[.//RESOURCE_TYPE/@value='MDFormatDSResourceType'] "
						+ "let $format:= $x//CONFIGURATION/NAME/string()  for $y in $x//LAYOUTS/LAYOUT  let $layout:= $y/@name/string() "
						+ "let $interpretation:= $x//CONFIGURATION/INTERPRETATION/text() let $id:=$x//RESOURCE_IDENTIFIER/@value/string() "
						+ " return concat($format,'-',$layout,'-',$interpretation,'::', $id) ";
		log.debug("Executing lookup query" + xquery);

		return quickSearchProfile(xquery).stream()
				.map(MdFormatInfo::initFromXqueryResult)
				.collect(Collectors.toList());
	}

	/**
	 * Index datastructures.
	 *
	 * @param backend
	 *            the backend
	 * @return the list< index info>
	 * @throws Exception
	 *             the exception
	 */
	@RequestMapping(value = "/ui/index/indexDatastructures.do")
	@ResponseBody
	public List<IndexInfo> indexDatastructures(@RequestParam(value = "backend", required = true) final String backend) throws Exception {
		final String xquery =
				"for $x in //RESOURCE_PROFILE[.//RESOURCE_TYPE/@value='IndexDSResourceType' and .//BACKEND/@ID='%s'] let $format := $x//METADATA_FORMAT "
						+ "let $layout := $x//METADATA_FORMAT_LAYOUT let $interpretation :=$x//METADATA_FORMAT_INTERPRETATION "
						+ "let $id :=$x//RESOURCE_IDENTIFIER/@value let $backendid := $x//BACKEND/@ID let $size := $x//INDEX_SIZE "
						+ "return concat($format, ':-:',$layout,':-:',$interpretation,':-:',$id,':-:',$backendid,':-:',$size)";
		log.debug("Executing lookup query" + String.format(xquery, backend));

		return quickSearchProfile(String.format(xquery, backend)).stream()
				.map(IndexInfo::newInstanceFromString)
				.collect(Collectors.toList());
	}

	/**
	 * Md format info.
	 *
	 * @param id
	 *            the id
	 * @param layout
	 *            the layout
	 * @return the list< string>
	 * @throws Exception
	 *             the exception
	 */
	@RequestMapping(value = "/ui/index/mdFormatInfo.do")
	@ResponseBody
	public List<String> mdFormatInfo(@RequestParam(value = "id", required = true) final String id,
			@RequestParam(value = "layout", required = true) final String layout) throws Exception {

		String xqueryTemplate =
				"//RESOURCE_PROFILE[.//RESOURCE_IDENTIFIER/@value='%s']" + "//LAYOUT[./@name/string()='%s'] "
						+ "//FIELD[./@tokenizable/string()='false' or ./@type = 'int' or ./@type = 'date']/@name/string()";
		log.debug("executing query: " + String.format(xqueryTemplate, id, layout));
		return quickSearchProfile(String.format(xqueryTemplate, id, layout));
	}

	/**
	 * Gets the backend available.
	 *
	 * @return the backend available
	 * @throws Exception
	 *             the exception
	 */
	@RequestMapping(value = "/ui/index/backend.do")
	@ResponseBody
	public Set<String> getBackendAvailable() throws Exception {

		String xquery = "for $x in //RESOURCE_PROFILE[.//RESOURCE_TYPE/@value='IndexDSResourceType'] return distinct-values($x//BACKEND/@ID/string())";
		log.debug("executing query: " + xquery);
		return Sets.newHashSet(quickSearchProfile(xquery));

	}

	/**
	 * Browse.
	 *
	 * @param map
	 *            the map
	 * @param backend
	 *            the backend
	 * @param format
	 *            the format
	 * @param layout
	 *            the layout
	 * @param interpretation
	 *            the interpretation
	 * @return the list< string>
	 * @throws IndexClientException
	 *             the index client exception
	 */
	@RequestMapping(value = "/ui/index/browse.do", method = RequestMethod.POST)
	@ResponseBody
	public List<BrowseEntry> browse(final ModelMap map,
			@RequestParam(value = "backend", required = true) final String backend,
			@RequestParam(value = "format", required = true) final String format,
			@RequestParam(value = "layout", required = true) final String layout,
			@RequestParam(value = "interpretation", required = true) final String interpretation,
			@RequestParam(value = "fields", required = true) final String fields,
			@RequestParam(value = "query", required = true) final String query) throws IndexClientException {

		List<String> browseFields = new Gson().fromJson(fields, new TypeToken<List<String>>() {}.getType());

		if (browseFields != null) {
			for (String s : browseFields) {
				log.debug("Browse field " + s);
			}
		}

		String indexClientKeys = format + "-" + layout + "-" + interpretation;

		IndexClient client = null;
		if (clientMap.getMap().containsKey(indexClientKeys)) {
			client = clientMap.getMap().get(indexClientKeys);
		} else {
			client = indexClientFactory.getClient(format, layout, interpretation);
			clientMap.getMap().put(indexClientKeys, client);
		}

		// LookupResponse result = client.lookup("*=*", null, 0, 10);

		List<BrowseEntry> result = client.browse(query, browseFields, 99);
		return result;
	}

	@RequestMapping(value = "/ui/index/delete.do", method = RequestMethod.POST)
	@ResponseBody
	public long delete(final ModelMap map,
			@RequestParam(value = "backend", required = true) final String backend,
			@RequestParam(value = "format", required = true) final String format,
			@RequestParam(value = "layout", required = true) final String layout,
			@RequestParam(value = "interpretation", required = true) final String interpretation,
			@RequestParam(value = "query", required = true) final String query,
			@RequestParam(value = "indexidentifier", required = false) final String indexId) throws IndexServiceException {

		String indexClientKeys = format + "-" + layout + "-" + interpretation;

		IndexClient client = null;
		if (clientMap.getMap().containsKey(indexClientKeys)) {
			client = clientMap.getMap().get(indexClientKeys);
		} else {
			client = indexClientFactory.getClient(format, layout, interpretation);
			clientMap.getMap().put(indexClientKeys, client);
		}
		String mquery = query;

		if (!StringUtils.isEmpty(indexId)) {
			mquery = query + " and __dsid exact \"" + indexId + "\"";
		}

		return client.delete(mquery);
	}

	@RequestMapping(value = "/ui/index/search.do", method = RequestMethod.POST)
	@ResponseBody
	public LookupResponse search(final ModelMap map,
			@RequestParam(value = "backend", required = true) final String backend,
			@RequestParam(value = "format", required = true) final String format,
			@RequestParam(value = "layout", required = true) final String layout,
			@RequestParam(value = "interpretation", required = true) final String interpretation,
			@RequestParam(value = "query", required = true) final String query,
			@RequestParam(value = "from", required = true) final int from,
			@RequestParam(value = "number", required = true) final int number,
			@RequestParam(value = "indexidentifier", required = false) final String indexId

			) throws IndexClientException {

		String indexClientKeys = format + "-" + layout + "-" + interpretation;

		log.debug(indexClientKeys);

		IndexClient client = null;

		if (!clientMap.getMap().containsKey(indexClientKeys)) {
			clientMap.getMap().put(indexClientKeys, indexClientFactory.getClient(format, layout, interpretation));
		}
		client = clientMap.getMap().get(indexClientKeys);

		List<String> filterId = null;
		if (indexId != null && !indexId.isEmpty()) {
			filterId = Lists.newArrayList("__dsid:\"" + indexId + "\"");
		}

		log.debug(String.format("query: '%s', filter: '%s', from: '%d', number: '%d'", query, filterId, from, number));

		LookupResponse result = client.lookup(query, filterId, from, number);

		ClassPathResource cpr = new ClassPathResource("/eu/dnetlib/functionality/modular/ui/xslt/gmf2document.xslt");
		ApplyXslt xslt = new ApplyXslt(cpr);
		List<String> convertedList = new ArrayList<String>();

		for (String s : result.getRecords()) {
			log.debug("response record: \n" + s);
			convertedList.add(xslt.evaluate(s));
		}
		result.setRecords(convertedList);
		return result;
	}

	/**
	 * Quick search profile.
	 *
	 * @param xquery
	 *            the xquery
	 * @return the list< string>
	 * @throws Exception
	 *             the exception
	 */
	private List<String> quickSearchProfile(final String xquery) throws Exception {
		try {
			return serviceLocator.getService(ISLookUpService.class).quickSearchProfile(xquery);
		} catch (ISLookUpException e) {
			throw new Exception(e);
		}
	}
}
