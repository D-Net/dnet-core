package eu.dnetlib.functionality.modular.ui.utils;

public class LogLine {

	private long id;
	private String level;
	private String name;
	private String date;
	private String message;
	private String stacktrace;

	public LogLine(final long id, final String level, final String name, final String date, final String message, final String stacktrace) {
		this.id = id;
		this.level = level;
		this.name = name;
		this.date = date;
		this.message = message;
		this.stacktrace = stacktrace;
	}

	public final long getId() {
		return id;
	}

	public final void setId(final long id) {
		this.id = id;
	}

	public final String getLevel() {
		return level;
	}

	public final void setLevel(final String level) {
		this.level = level;
	}

	public final String getName() {
		return name;
	}

	public final void setName(final String name) {
		this.name = name;
	}

	public final String getDate() {
		return date;
	}

	public final void setDate(final String date) {
		this.date = date;
	}

	public final String getMessage() {
		return message;
	}

	public final void setMessage(final String message) {
		this.message = message;
	}

	public final String getStacktrace() {
		return stacktrace;
	}

	public final void setStacktrace(final String stacktrace) {
		this.stacktrace = stacktrace;
	}

}
