package eu.dnetlib.functionality.modular.ui.is.objects;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;

public class CollectionDesc implements Comparable<CollectionDesc> {
	private String kind;
	private int size = 0;
	private List<CollectionTypeDesc> types = Lists.newArrayList();
	
	public CollectionDesc() {}
	
	public CollectionDesc(String kind, List<CollectionTypeDesc> types, int size) {
		this.kind = kind;
		this.types = types;
		this.size = size;
	}
	
	public CollectionDesc(String kind) {
		this(kind, new ArrayList<CollectionTypeDesc>(), 0);
	}

	public String getKind() {
		return kind;
	}
	
	public void setKind(String kind) {
		this.kind = kind;
	}
	
	public List<CollectionTypeDesc> getTypes() {
		return types;
	}
	
	public void setTypes(List<CollectionTypeDesc> types) {
		this.types = types;
	}

	public void addType(String type, int n) {
		types.add(new CollectionTypeDesc(type, n));
		this.size += n;
	}
	
	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	@Override
	public int compareTo(CollectionDesc o) {
		return kind.compareTo(o.getKind());
	}

}
