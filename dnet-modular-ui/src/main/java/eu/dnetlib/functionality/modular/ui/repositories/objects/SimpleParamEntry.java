package eu.dnetlib.functionality.modular.ui.repositories.objects;

public class SimpleParamEntry implements Comparable<SimpleParamEntry> {

	private String id;
	private String name;
	private Object value;
	private Object otherValue;

	public SimpleParamEntry(final String name, final Object value) {
		this(name, name, value, null);
	}

	public SimpleParamEntry(final String id, final String name, final Object value) {
		this(id, name, value, null);
	}
	
	public SimpleParamEntry(final String id, final String name, final Object value, final Object otherValue) {
		this.id = id;
		this.name = name;
		this.value = value;
		this.setOtherValue(otherValue);
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(final Object value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}


	public Object getOtherValue() {
		return otherValue;
	}

	public void setOtherValue(Object otherValue) {
		this.otherValue = otherValue;
	}

	@Override
	public int compareTo(final SimpleParamEntry o) {
		return getName().compareTo(o.getName());
	}
}
