package eu.dnetlib.functionality.modular.ui;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import eu.dnetlib.functionality.modular.ui.users.PermissionLevel;

public class UsersController extends ModuleEntryPoint {

	private List<Map<String, String>> listAvailableLevels = Lists.newArrayList();; 
		
	@Override
	protected void initialize(ModelMap map, HttpServletRequest request,	HttpServletResponse response) throws Exception {
		map.addAttribute("availableLevels", listPermissionLevels());
	}
	
	private List<Map<String, String>> listPermissionLevels() {
		if (listAvailableLevels.isEmpty()) {
			for (PermissionLevel level : PermissionLevel.values()) {
				final Map<String, String> map = Maps.newHashMap();
				map.put("level", level.toString());
				map.put("label", level.getLabel());
				map.put("details", level.getDetails());
				listAvailableLevels.add(map);
			}
		}
		return listAvailableLevels;
	}
	
}
