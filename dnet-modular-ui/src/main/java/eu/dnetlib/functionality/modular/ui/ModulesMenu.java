package eu.dnetlib.functionality.modular.ui;

import java.util.Collections;
import java.util.List;

import com.google.common.collect.Lists;

public class ModulesMenu extends AbstractMenu {

	private int order = Integer.MAX_VALUE;
	
	private List<ModuleEntryPoint> entries = Lists.newArrayList();
	
	public ModulesMenu(final String title) {
		super();
		setTitle(title);
	}
	
	public void addEntry(final ModuleEntryPoint entry) {
		entries.add(entry);
	}
	
	public void complete() {
		Collections.sort(entries);
		for (ModuleEntryPoint e : entries) {
			if (e.getGroupOrder() < this.order) {
				this.order = e.getGroupOrder();
			}
		}
	}

	@Override
	public int getOrder() {
		return order;
	}

	@Override
	public List<? extends MenuEntry> getEntries() {
		return entries;
	}
}