package eu.dnetlib.functionality.modular.ui.repositories;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.ui.ModelMap;

import com.google.gson.Gson;

import eu.dnetlib.enabling.locators.UniqueServiceLocator;
import eu.dnetlib.functionality.modular.ui.ModuleEntryPoint;

public class AddRepoEntryPointController extends ModuleEntryPoint {

	private String datasourceTypeVocabulary;
	private String datasourceCountryVocabulary;

	@Resource
	private UniqueServiceLocator serviceLocator;

	@Resource
	private RepoUIUtils repoUIUtils;

	@Override
	protected void initialize(final ModelMap map, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		final Gson gson = new Gson();
		map.addAttribute("types", gson.toJson(repoUIUtils.fetchVocabularyTerms(datasourceTypeVocabulary)));
		map.addAttribute("countries", gson.toJson(repoUIUtils.fetchVocabularyTerms(datasourceCountryVocabulary)));
	}

	public String getDatasourceTypeVocabulary() {
		return datasourceTypeVocabulary;
	}

	@Required
	public void setDatasourceTypeVocabulary(final String datasourceTypeVocabulary) {
		this.datasourceTypeVocabulary = datasourceTypeVocabulary;
	}

	public String getDatasourceCountryVocabulary() {
		return datasourceCountryVocabulary;
	}

	@Required
	public void setDatasourceCountryVocabulary(final String datasourceCountryVocabulary) {
		this.datasourceCountryVocabulary = datasourceCountryVocabulary;
	}

}
