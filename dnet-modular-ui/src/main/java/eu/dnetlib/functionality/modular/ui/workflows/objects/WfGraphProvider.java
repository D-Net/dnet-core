package eu.dnetlib.functionality.modular.ui.workflows.objects;

public interface WfGraphProvider {

	public String getMapContent();
	
	public String getImageUrl();

}
