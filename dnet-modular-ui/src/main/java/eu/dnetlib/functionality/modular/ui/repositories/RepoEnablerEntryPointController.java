package eu.dnetlib.functionality.modular.ui.repositories;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.ui.ModelMap;

import com.google.gson.Gson;

import eu.dnetlib.functionality.modular.ui.ModuleEntryPoint;

public class RepoEnablerEntryPointController extends ModuleEntryPoint {

	private String datasourceTypeVocabulary;

	@Resource
	private RepoUIUtils repoUIUtils;

	@Override
	protected void initialize(final ModelMap map, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		map.addAttribute("types", new Gson().toJson(repoUIUtils.fetchVocabularyTerms(getDatasourceTypeVocabulary())));
	}

	public String getDatasourceTypeVocabulary() {
		return datasourceTypeVocabulary;
	}

	@Required
	public void setDatasourceTypeVocabulary(final String datasourceTypeVocabulary) {
		this.datasourceTypeVocabulary = datasourceTypeVocabulary;
	}

}
