package eu.dnetlib.functionality.modular.ui.users;

import java.util.Set;

public class User implements Comparable<User> {
	private String id;
	private String fullname;
	private String email;
	private Set<PermissionLevel> permissionLevels;
	
	public User() {}
	
	public User(final String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Set<PermissionLevel> getPermissionLevels() {
		return permissionLevels;
	}
	public void setPermissionLevels(Set<PermissionLevel> permissionLevels) {
		this.permissionLevels = permissionLevels;
	}
	
	@Override
	public int compareTo(final User o) {
		return getId().compareTo(o.getId());
	}
	
	
}
