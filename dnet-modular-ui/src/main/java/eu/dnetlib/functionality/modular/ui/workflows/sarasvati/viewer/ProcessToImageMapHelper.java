package eu.dnetlib.functionality.modular.ui.workflows.sarasvati.viewer;

import javax.swing.Icon;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.googlecode.sarasvati.Node;
import com.googlecode.sarasvati.visual.ProcessToImageMapAdapter;
import com.googlecode.sarasvati.visual.process.VisualProcessNode;

import eu.dnetlib.functionality.modular.ui.workflows.sarasvati.icons.ProcessJobIcon;
import eu.dnetlib.msro.workflows.nodes.ProgressJobNode;
import eu.dnetlib.msro.workflows.util.ProgressProvider;

public class ProcessToImageMapHelper extends ProcessToImageMapAdapter {

	private String procId;

	private static final Log log = LogFactory.getLog(ProcessToImageMapHelper.class);

	public ProcessToImageMapHelper(final String procId) {
		super();
		this.procId = procId;
	}

	@Override
	public String hrefForNode(final VisualProcessNode node) {
		return "javascript:getScope('map').getProcessNodeDetails('" + procId + "', '" + node.getNode().getId() + "')";
	}

	@Override
	public String hoverForNode(final VisualProcessNode vnode) {
		final Node node = vnode.getNode();
		if (node instanceof ProgressJobNode) {
			try {
				final ProgressProvider progress = ((ProgressJobNode) node).getProgressProvider();
				if (progress == null) {
					return "Processed: -";
				} else if (progress.isInaccurate()) {
					return "Processed: " + progress.getCurrentValue();
				} else {
					return "Processed: " + progress.getCurrentValue() + " - Total: " + progress.getTotalValue();
				}
			} catch (Throwable e) {
				log.warn("Error obtainig provider info", e);
				return e.getMessage();
			}
		} else {
			return "Name: " + node.getName();
		}
	}

	@Override
	public Icon iconForNode(final VisualProcessNode vnode) {
		final Node node = vnode.getNode();

		final ProgressProvider progressProvider = node instanceof ProgressJobNode ? ((ProgressJobNode) node).getProgressProvider() : null;

		return new ProcessJobIcon(node, vnode.getToken(), progressProvider);
	}
}
