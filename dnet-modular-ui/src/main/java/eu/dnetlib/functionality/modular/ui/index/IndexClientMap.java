package eu.dnetlib.functionality.modular.ui.index;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.collect.Maps;

import eu.dnetlib.functionality.index.client.IndexClient;
import eu.dnetlib.functionality.index.client.IndexClientException;

public class IndexClientMap {

	private static final Log log = LogFactory.getLog(IndexClientMap.class);

	private Map<String, IndexClient> map = Maps.newHashMap();

	public void shutdown() throws IOException {
		log.debug("shutdown index clients");
		for (IndexClient client : map.values()) {
			client.close();
		}
	}

	public Map<String, IndexClient> getMap() {
		return map;
	}

	public void setMap(final Map<String, IndexClient> map) {
		this.map = map;
	}

}
