package eu.dnetlib.functionality.modular.ui.workflows.sarasvati.viewer;

import java.awt.image.BufferedImage;
import java.util.Set;

import javax.annotation.Resource;

import com.googlecode.sarasvati.Graph;
import com.googlecode.sarasvati.GraphProcess;
import com.googlecode.sarasvati.visual.GraphImageMapCreator;
import com.googlecode.sarasvati.visual.ProcessImageMapCreator;

import eu.dnetlib.functionality.modular.ui.workflows.util.ISLookupClient;
import eu.dnetlib.msro.workflows.sarasvati.loader.GraphLoader;
import eu.dnetlib.msro.workflows.sarasvati.registry.GraphProcessRegistry;

public class ProcessGraphGenerator {
	
	@Resource
	private GraphProcessRegistry graphProcessRegistry;
	
	@Resource
	private ISLookupClient isLookupClient;
	
	@Resource
	private GraphLoader graphLoader;

	public BufferedImage getProcessImage(final String procId) throws Exception {
		final GraphProcess proc = graphProcessRegistry.findProcess(procId);
		final ProcessImageMapCreator imageMapCreator = new ProcessImageMapCreator(proc, new ProcessToImageMapHelper(procId));
		return imageMapCreator.getImage();
	}
	
	public String getProcessImageMap(final String procId) throws Exception {
		final GraphProcess proc = graphProcessRegistry.findProcess(procId);
		final ProcessImageMapCreator imageMapCreator = new ProcessImageMapCreator(proc, new ProcessToImageMapHelper(procId));
		return imageMapCreator.getMapContents();
	}

	public BufferedImage getWfDescImage(final String id, final String xml, final Set<String> notConfiguredNodes) throws Exception {
		final Graph graph = graphLoader.loadGraph(xml);
		final GraphImageMapCreator creator = new GraphImageMapCreator(graph, new GraphToImageMapHelper(id, notConfiguredNodes));
		return creator.getImage();
	}
	
	public String getWfDescImageMap(String id, String xml) throws Exception {
		final Graph graph = graphLoader.loadGraph(xml);
		final GraphImageMapCreator creator = new GraphImageMapCreator(graph, new GraphToImageMapHelper(id, null));
		return creator.getMapContents();
	}
	
}
