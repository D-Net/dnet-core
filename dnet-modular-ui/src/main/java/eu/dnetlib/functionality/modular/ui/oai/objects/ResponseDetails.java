package eu.dnetlib.functionality.modular.ui.oai.objects;

public class ResponseDetails {
	private String verb;
	private long time;
	private int httpCode;
	private boolean valid;
	private String error;
	private int size;
	private int cursor;
	private int total;
	private OaiRequest nextCall;
		
	public String getVerb() {
		return verb;
	}
	public void setVerb(String verb) {
		this.verb = verb;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	public int getHttpCode() {
		return httpCode;
	}
	public void setHttpCode(int httpCode) {
		this.httpCode = httpCode;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public OaiRequest getNextCall() {
		return nextCall;
	}
	public void setNextCall(OaiRequest nextCall) {
		this.nextCall = nextCall;
	}
	public int getCursor() {
		return cursor;
	}
	public void setCursor(int cursor) {
		this.cursor = cursor;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	
}
