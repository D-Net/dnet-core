package eu.dnetlib.functionality.modular.ui.mdstore;

/**
 * Created by sandro on 12/1/16.
 */
public class MDStoreUIException extends Exception {

    public MDStoreUIException() {

    }

    public MDStoreUIException(String message) {
        super(message);
    }

    public MDStoreUIException(Throwable e) {
        super(e);
    }

}
