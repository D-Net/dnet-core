package eu.dnetlib.functionality.modular.ui.is.objects;

import java.util.ArrayList;
import java.util.List;

public class ServiceDesc {

	public enum ServiceStatus {
		UNKNOWN, ACTIVE, PENDING, NOT_RESPONDING, MISSING
	}

	private String id = "";
	private String name = "";
	private String wsdl = "";
	private ServiceStatus status = ServiceStatus.UNKNOWN;
	private List<SubscriptionDesc> subscriptions = new ArrayList<SubscriptionDesc>();

	public ServiceDesc() {}

	public ServiceDesc(final String id, final String name, final String wsdl) {
		this(id, name, wsdl, ServiceStatus.UNKNOWN, new ArrayList<SubscriptionDesc>());
	}

	public ServiceDesc(final String id, final String name, final String wsdl, final ServiceStatus status, final List<SubscriptionDesc> subscriptions) {
		this.id = id;
		this.name = name;
		this.wsdl = wsdl;
		this.status = status;
		this.subscriptions = subscriptions;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getWsdl() {
		return wsdl;
	}

	public void setWsdl(final String wsdl) {
		this.wsdl = wsdl;
	}

	public ServiceStatus getStatus() {
		return status;
	}

	public void setStatus(final ServiceStatus status) {
		this.status = status;
	}

	public List<SubscriptionDesc> getSubscriptions() {
		return subscriptions;
	}

	public void setSubscriptions(final List<SubscriptionDesc> subscriptions) {
		this.subscriptions = subscriptions;
	}

}
