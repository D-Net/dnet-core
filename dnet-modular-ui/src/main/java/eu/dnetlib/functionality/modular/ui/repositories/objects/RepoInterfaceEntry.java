package eu.dnetlib.functionality.modular.ui.repositories.objects;

import java.util.ArrayList;
import java.util.List;

import eu.dnetlib.enabling.datasources.common.SearchApisEntry;

public class RepoInterfaceEntry extends SearchApisEntry {

	private String label;
	private boolean removable = false;
	private String collDate = "";
	private int collTotal = 0;
	private String collMdId = "";
	private String aggrMdId = "";
	private String downloadDate = "";
	private int downloadTotal = 0;
	private String downloadObjId = "";
	private String repoType;
	private String email;
	private List<SimpleParamEntry> commonParams = new ArrayList<>();
	private List<SimpleParamEntry> accessParams = new ArrayList<>();
	private List<SimpleParamEntry> otherParams = new ArrayList<>();
	private List<RepoMetaWfEntry> metaWFs = new ArrayList<>();

	public String getLabel() {
		return label;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public String getCollDate() {
		return collDate;
	}

	public void setCollDate(final String collDate) {
		this.collDate = collDate;
	}

	public int getCollTotal() {
		return collTotal;
	}

	public void setCollTotal(final int collTotal) {
		this.collTotal = collTotal;
	}

	public List<SimpleParamEntry> getCommonParams() {
		return commonParams;
	}

	public void setCommonParams(final List<SimpleParamEntry> commonParams) {
		this.commonParams = commonParams;
	}

	public List<SimpleParamEntry> getAccessParams() {
		return accessParams;
	}

	public void setAccessParams(final List<SimpleParamEntry> accessParams) {
		this.accessParams = accessParams;
	}

	public List<SimpleParamEntry> getOtherParams() {
		return otherParams;
	}

	public void setOtherParams(final List<SimpleParamEntry> otherParams) {
		this.otherParams = otherParams;
	}

	public List<RepoMetaWfEntry> getMetaWFs() {
		return metaWFs;
	}

	public void setMetaWFs(final List<RepoMetaWfEntry> metaWFs) {
		this.metaWFs = metaWFs;
	}

	public String getAggrMdId() {
		return aggrMdId;
	}

	public void setAggrMdId(final String aggrMdId) {
		this.aggrMdId = aggrMdId;
	}

	public String getCollMdId() {
		return collMdId;
	}

	public void setCollMdId(final String collMdId) {
		this.collMdId = collMdId;
	}

	public String getDownloadDate() {
		return downloadDate;
	}

	public void setDownloadDate(final String downloadDate) {
		this.downloadDate = downloadDate;
	}

	public int getDownloadTotal() {
		return downloadTotal;
	}

	public void setDownloadTotal(final int downloadTotal) {
		this.downloadTotal = downloadTotal;
	}

	public String getDownloadObjId() {
		return downloadObjId;
	}

	public void setDownloadObjId(final String downloadObjId) {
		this.downloadObjId = downloadObjId;
	}

	public String getRepoType() {
		return repoType;
	}

	public void setRepoType(final String repoType) {
		this.repoType = repoType;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public boolean isRemovable() {
		return removable;
	}

	public void setRemovable(final boolean removable) {
		this.removable = removable;
	}

}
