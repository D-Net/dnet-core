package eu.dnetlib.data.transformation.ui;

public class SelectOption implements Comparable<SelectOption> {

	private final String id;
	private final String name;
	private final boolean selected;

	public SelectOption(final String id, final String name, final boolean selected) {
		this.id = id;
		this.name = name;
		this.selected = selected;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public boolean isSelected() {
		return selected;
	}

	@Override
	public int compareTo(final SelectOption o) {
		return getName().toLowerCase().compareTo(o.getName().toLowerCase());
	}
}
