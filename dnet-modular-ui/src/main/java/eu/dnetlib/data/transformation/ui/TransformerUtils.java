package eu.dnetlib.data.transformation.ui;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService;
import eu.dnetlib.enabling.locators.UniqueServiceLocator;

@Component
public class TransformerUtils {

	@Resource
	private UniqueServiceLocator serviceLocator;

	public List<SelectOption> obtainRuleProfiles(final String currentId) throws ISLookUpException {
		final String xquery = "for $x " +
				"in collection('/db/DRIVER/TransformationRuleDSResources/TransformationRuleDSResourceType') " +
				"return concat($x//RESOURCE_IDENTIFIER/@value, ' @@@ ', $x//TITLE)";

		return serviceLocator.getService(ISLookUpService.class)
				.quickSearchProfile(xquery)
				.stream()
				.map(s -> {
					final String[] arr = s.split("@@@");
					final String id = arr[0].trim();
					final String name = arr[1].trim();
					return new SelectOption(id, name, id.equals(currentId));
				})
				.sorted()
				.collect(Collectors.toList());

	}
}
