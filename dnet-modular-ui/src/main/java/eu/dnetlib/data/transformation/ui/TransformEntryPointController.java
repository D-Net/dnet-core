package eu.dnetlib.data.transformation.ui;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import eu.dnetlib.functionality.modular.ui.ModuleEntryPoint;

public class TransformEntryPointController extends ModuleEntryPoint {

	@Resource
	private TransformerUtils transformerUtils;

	@Override
	protected void initialize(final ModelMap map, final HttpServletRequest request, final HttpServletResponse response) throws Exception {
		map.addAttribute("rules", transformerUtils.obtainRuleProfiles(null));
	}

}
