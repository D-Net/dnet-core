package eu.dnetlib.msro;

import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Required;

import eu.dnetlib.enabling.tools.AbstractBaseService;
import eu.dnetlib.enabling.tools.blackboard.NotificationHandler;
import eu.dnetlib.msro.rmi.MSROService;
import eu.dnetlib.msro.workflows.sarasvati.registry.GraphProcessRegistry;

@WebService(targetNamespace = "http://services.dnetlib.eu/")
public class MSROServiceImpl extends AbstractBaseService implements MSROService {

	/**
	 * logger.
	 */
	private static final Log log = LogFactory.getLog(MSROServiceImpl.class); // NOPMD by marko on 11/24/08 5:02 PM

	/**
	 * notification handler.
	 */
	private NotificationHandler notificationHandler;

	/**
	 * graph process registry.
	 */
	private GraphProcessRegistry processRegistry;

	@Override
	public void notify(final String subscriptionId, final String topic, final String isId, final String message) {
		super.notify(subscriptionId, topic, isId, message);

		log.debug("got notification: " + topic);

		getNotificationHandler().notified(subscriptionId, topic, isId, message);
	}

	@Required
	public void setNotificationHandler(final NotificationHandler notHandler) {
		this.notificationHandler = notHandler;
	}

	public NotificationHandler getNotificationHandler() {
		return notificationHandler;
	}

	@Required
	public void setProcessRegistry(final GraphProcessRegistry processRegistry) {
		this.processRegistry = processRegistry;
	}

	public GraphProcessRegistry getProcessRegistry() {
		return processRegistry;
	}

}
