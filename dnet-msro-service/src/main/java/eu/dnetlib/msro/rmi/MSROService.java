package eu.dnetlib.msro.rmi;

import javax.jws.WebService;

import eu.dnetlib.common.rmi.BaseService;

@WebService(targetNamespace = "http://services.dnetlib.eu/")
public interface MSROService extends BaseService {

}
