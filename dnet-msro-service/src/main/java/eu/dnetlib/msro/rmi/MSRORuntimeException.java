package eu.dnetlib.msro.rmi;

/**
 * Created by alessia on 26/01/16.
 */
public class MSRORuntimeException extends RuntimeException {

	public MSRORuntimeException(String string) {
		super(string);
	}

	public MSRORuntimeException(String string, Throwable exception) {
		super(string, exception);
	}

	public MSRORuntimeException(Throwable exception) {
		super(exception);
	}

}
