package eu.dnetlib.msro.rmi;

import eu.dnetlib.common.rmi.RMIException;

public class MSROException extends RMIException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 767303634254723704L;

	public MSROException(String string) {
		super(string);
	}
	
	public MSROException(String string, Throwable exception) {
		super(string, exception);
	}

	public MSROException(Throwable exception) {
		super(exception);
	}

}