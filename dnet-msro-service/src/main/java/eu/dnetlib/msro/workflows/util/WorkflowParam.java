package eu.dnetlib.msro.workflows.util;

import java.util.ArrayList;
import java.util.List;

public class WorkflowParam {

	private String name;
	private String value;
	private boolean required;
	private boolean userParam;
	private String type;
	private String function;
	private List<WorkflowParamUI> uis;
	
	public WorkflowParam() {
		
	}
	
	public WorkflowParam(final String name, final String value, final boolean userParam) {
		this(name, value, false, userParam, null, null, new ArrayList<WorkflowParamUI>());
	}

	public WorkflowParam(final String name, final String value, final boolean required, final boolean userParam, final String type, final String function, final List<WorkflowParamUI> uis) {
		this.name = ((name == null) || name.isEmpty()) ? "" : name;
		this.value = ((value == null) || value.isEmpty()) ? "" : value;
		this.required = required;
		this.userParam = userParam;
		this.type = ((type == null) || type.isEmpty()) ? "string" : type;
		this.function = function;
		this.uis = uis;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(final boolean required) {
		this.required = required;
	}

	public boolean isUserParam() {
		return userParam;
	}
	
	public void setUserParam(boolean userParam) {
		this.userParam = userParam;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(final String function) {
		this.function = function;
	}

	public List<WorkflowParamUI> getUis() {
		return uis;
	}

	public void setUis(List<WorkflowParamUI> uis) {
		this.uis = uis;
	}

}
