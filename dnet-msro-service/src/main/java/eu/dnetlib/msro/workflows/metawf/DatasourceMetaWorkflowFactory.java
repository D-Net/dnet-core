package eu.dnetlib.msro.workflows.metawf;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class DatasourceMetaWorkflowFactory implements ApplicationContextAware {

	private transient ApplicationContext applicationContext;

	public DatasourceMetaWorkflow newMetaWorkflow(final String beanName) {
		final DatasourceMetaWorkflow prototypeMetaWf = (DatasourceMetaWorkflow) applicationContext.getBean(beanName, DatasourceMetaWorkflow.class);

		if (prototypeMetaWf != null) {
			return prototypeMetaWf;
		} else {
			throw new IllegalArgumentException("cannot find bean " + beanName);
		}
	}

	@Override
	public void setApplicationContext(final ApplicationContext context) {
		this.applicationContext = context;
	}

}
