package eu.dnetlib.msro.workflows.nodes.mdstore;

import java.util.Iterator;
import java.util.List;

import eu.dnetlib.enabling.locators.UniqueServiceLocator;
import eu.dnetlib.enabling.resultset.client.ResultSetClientFactory;

// TODO: Auto-generated Javadoc
/**
 * The Class MultipleMdStoreIterable.
 */
public class MultipleMdStoreIterable implements Iterable<String> {

	/** The service locator. */
	private UniqueServiceLocator serviceLocator;

	/** The md ids. */
	private List<String> mdIds;

	/** The result set client factory. */
	private ResultSetClientFactory resultSetClientFactory;

	/**
	 * Instantiates a new multiple md store iterable.
	 *
	 * @param serviceLocator
	 *            the service locator
	 * @param mdIds
	 *            the md ids
	 * @param resultSetClientFactory
	 *            the result set client factory
	 */
	public MultipleMdStoreIterable(final UniqueServiceLocator serviceLocator, final List<String> mdIds, final ResultSetClientFactory resultSetClientFactory) {
		super();
		this.serviceLocator = serviceLocator;
		this.mdIds = mdIds;
		this.resultSetClientFactory = resultSetClientFactory;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<String> iterator() {
		return new MultipleMdStoreIterator(serviceLocator, mdIds, resultSetClientFactory);
	}
}
