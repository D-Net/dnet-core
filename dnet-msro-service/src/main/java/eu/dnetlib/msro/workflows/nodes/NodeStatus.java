package eu.dnetlib.msro.workflows.nodes;

public enum NodeStatus {
	CONFIGURED, NOT_CONFIGURED, DISABLED, SYSTEM
}
