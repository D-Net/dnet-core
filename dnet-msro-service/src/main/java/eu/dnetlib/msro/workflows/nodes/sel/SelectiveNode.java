package eu.dnetlib.msro.workflows.nodes.sel;

import com.googlecode.sarasvati.Arc;
import com.googlecode.sarasvati.NodeToken;

import eu.dnetlib.msro.workflows.nodes.SimpleJobNode;
import eu.dnetlib.msro.workflows.util.WorkflowParam;
import eu.dnetlib.msro.workflows.util.WorkflowsConstants;

/**
 * The Class SelectiveNode allows to decide which path a workflow must take.
 */
public class SelectiveNode extends SimpleJobNode {

	/** The selection. */
	private String selection = Arc.DEFAULT_ARC;

	/*
	 * (non-Javadoc)
	 * 
	 * @see eu.dnetlib.msro.workflows.nodes.SimpleJobNode#execute(com.googlecode.sarasvati.NodeToken)
	 */
	@Override
	protected String execute(final NodeToken token) throws Exception {
		token.getEnv().setAttribute(WorkflowsConstants.MAIN_LOG_PREFIX + "selection", getSelection());
		return getSelection();
	}

	/**
	 * Gets the selection.
	 *
	 * @return the selection
	 */
	public String getSelection() {
		return selection;
	}

	/**
	 * Sets the selection.
	 *
	 * @param selection
	 *            the new selection
	 */
	public void setSelection(final String selection) {
		this.selection = selection;
	}

}
