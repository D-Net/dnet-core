package eu.dnetlib.msro.workflows.sarasvati.loader;

import java.util.Comparator;
import java.util.concurrent.PriorityBlockingQueue;

import org.apache.commons.lang.math.NumberUtils;

import com.googlecode.sarasvati.GraphProcess;

import eu.dnetlib.msro.workflows.util.WorkflowsConstants;

public class ProcessQueue extends PriorityBlockingQueue<GraphProcess> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8342381671561461487L;

	@Override
	public void put(GraphProcess e) {
		// TODO Auto-generated method stub
		super.put(e);
	}

	private static final int INITIAL_SIZE = 20;
	
	public ProcessQueue() {
		super(INITIAL_SIZE, new Comparator<GraphProcess>() {
			
			@Override
			public int compare(GraphProcess p1, GraphProcess p2) {
				int n1 = NumberUtils.toInt(p1.getEnv().getAttribute(WorkflowsConstants.SYSTEM_WF_PRIORITY), WorkflowsConstants.DEFAULT_WF_PRIORITY);
				int n2 = NumberUtils.toInt(p2.getEnv().getAttribute(WorkflowsConstants.SYSTEM_WF_PRIORITY), WorkflowsConstants.DEFAULT_WF_PRIORITY);
				return NumberUtils.compare(n1, n2);
			}
		});
	}
	
	

}
