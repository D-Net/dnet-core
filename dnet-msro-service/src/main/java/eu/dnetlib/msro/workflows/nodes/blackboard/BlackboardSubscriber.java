package eu.dnetlib.msro.workflows.nodes.blackboard;

import javax.annotation.Resource;
import javax.xml.ws.Endpoint;
import javax.xml.ws.wsaddressing.W3CEndpointReference;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Required;

import eu.dnetlib.enabling.is.sn.rmi.ISSNException;
import eu.dnetlib.enabling.is.sn.rmi.ISSNService;
import eu.dnetlib.enabling.locators.UniqueServiceLocator;
import eu.dnetlib.soap.EndpointReferenceBuilder;

public class BlackboardSubscriber {

	/**
	 * Logger.
	 */
	private static final Log log = LogFactory.getLog(BlackboardSubscriber.class); // NOPMD by marko on 11/24/08 5:02 PM

	/**
	 * service locator.
	 */
	@Resource
	private UniqueServiceLocator serviceLocator;

	/**
	 * injected EPR builder.
	 */
	@Resource(name = "jaxwsEndpointReferenceBuilder")
	private EndpointReferenceBuilder<Endpoint> eprBuilder;

	/**
	 * notification endpoint (normally the msro service).
	 */
	private Endpoint endpoint;

	/**
	 * performs the subscription.
	 * 
	 * @throws ISSNException
	 *             could happen
	 */
	public void subscribeAll() throws ISSNException {
		log.info("Subscribing msro service");

		final W3CEndpointReference endpointReference = eprBuilder.getEndpointReference(getEndpoint());
		serviceLocator.getService(ISSNService.class).subscribe(endpointReference, "UPDATE/*/*/RESOURCE_PROFILE/BODY/BLACKBOARD/LAST_RESPONSE", 0);
	}

	public Endpoint getEndpoint() {
		return endpoint;
	}

	@Required
	public void setEndpoint(final Endpoint endpoint) {
		this.endpoint = endpoint;
	}
}
