package eu.dnetlib.msro.workflows.nodes.info;

import java.util.Date;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.googlecode.sarasvati.Arc;
import com.googlecode.sarasvati.NodeToken;

import eu.dnetlib.enabling.datasources.common.LocalDatasourceManager;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService;
import eu.dnetlib.enabling.locators.UniqueServiceLocator;
import eu.dnetlib.miscutils.datetime.DateUtils;
import eu.dnetlib.msro.workflows.nodes.SimpleJobNode;

public class MDStoreToApiExtraFieldJobNode extends SimpleJobNode {

	private String mdId;
	private String datasourceId;
	private String datasourceInterface;
	private String extraFieldForTotal;
	private String extraFieldForDate;
	private String extraFieldForMdId;

	@Autowired
	private UniqueServiceLocator serviceLocator;

	@Autowired
	private LocalDatasourceManager<?, ?> dsManager;

	private static final Log log = LogFactory.getLog(MDStoreToApiExtraFieldJobNode.class);

	@Override
	protected String execute(final NodeToken token) throws Exception {
		final String xq = "for $x in collection('/db/DRIVER/MDStoreDSResources/MDStoreDSResourceType') " +
				"where $x//RESOURCE_IDENTIFIER/@value='" + mdId + "' " +
				"return concat($x//NUMBER_OF_RECORDS, ' @=@ ', $x//LAST_STORAGE_DATE)";

		final String s = serviceLocator.getService(ISLookUpService.class).getResourceProfileByQuery(xq);

		final String[] arr = s.split(" @=@ ");

		final int size = NumberUtils.toInt(arr[0].trim(), 0);
		final Date date = (new DateUtils()).parse(arr[1]);

		if (extraFieldForTotal.equals("last_collection_total")) {
			dsManager.setLastCollectionInfo(datasourceId, datasourceInterface, mdId, size, date);
		} else if (extraFieldForTotal.equals("last_aggregation_total")) {
			dsManager.setLastAggregationInfo(datasourceId, datasourceInterface, mdId, size, date);
		} else {
			log.warn("Invalid field for total: " + extraFieldForTotal);
		}

		return Arc.DEFAULT_ARC;
	}

	public String getMdId() {
		return mdId;
	}

	public void setMdId(final String mdId) {
		this.mdId = mdId;
	}

	public String getDatasourceId() {
		return datasourceId;
	}

	public void setDatasourceId(final String datasourceId) {
		this.datasourceId = datasourceId;
	}

	public String getDatasourceInterface() {
		return datasourceInterface;
	}

	public void setDatasourceInterface(final String datasourceInterface) {
		this.datasourceInterface = datasourceInterface;
	}

	public String getExtraFieldForTotal() {
		return extraFieldForTotal;
	}

	public void setExtraFieldForTotal(final String extraFieldForTotal) {
		this.extraFieldForTotal = extraFieldForTotal;
	}

	public String getExtraFieldForDate() {
		return extraFieldForDate;
	}

	public void setExtraFieldForDate(final String extraFieldForDate) {
		this.extraFieldForDate = extraFieldForDate;
	}

	public String getExtraFieldForMdId() {
		return extraFieldForMdId;
	}

	public void setExtraFieldForMdId(final String extraFieldForMdId) {
		this.extraFieldForMdId = extraFieldForMdId;
	}

}
