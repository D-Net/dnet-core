package eu.dnetlib.msro.workflows.nodes;

import eu.dnetlib.msro.workflows.util.ProgressProvider;

public interface ProgressJobNode {

	public ProgressProvider getProgressProvider();
}
