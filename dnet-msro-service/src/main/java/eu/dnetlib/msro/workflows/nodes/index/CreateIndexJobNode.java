package eu.dnetlib.msro.workflows.nodes.index;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Required;

import com.googlecode.sarasvati.Engine;
import com.googlecode.sarasvati.NodeToken;
import com.googlecode.sarasvati.env.Env;

import eu.dnetlib.data.provision.index.rmi.IndexService;
import eu.dnetlib.enabling.tools.blackboard.BlackboardJob;
import eu.dnetlib.msro.workflows.nodes.BlackboardJobNode;
import eu.dnetlib.msro.workflows.nodes.blackboard.BlackboardWorkflowJobListener;

// TODO: Auto-generated Javadoc
/**
 * The Class CreateIndexJobNode.
 */
public class CreateIndexJobNode extends BlackboardJobNode {

	/** The Constant log. */
	private static final Log log = LogFactory.getLog(CreateIndexJobNode.class);

	/** The output prefix. */
	private String outputPrefix = "index_";

	/** The default backend id. */
	private String defaultBackendId;

	/** The format. */
	private String format;

	/** The layout. */
	private String layout;

	/** The interpretation. */
	private String interpretation;

	@Override
	protected String obtainServiceId(final NodeToken token) {
		return getServiceLocator().getServiceId(IndexService.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see eu.dnetlib.msro.workflows.nodes.BlackboardJobNode#prepareJob(eu.dnetlib.enabling.tools.blackboard.BlackboardJob,
	 * com.googlecode.sarasvati.NodeToken)
	 */
	@Override
	protected void prepareJob(final BlackboardJob job, final NodeToken token) {
		final String env_format = token.getFullEnv().getAttribute("format");
		final String env_layout = token.getFullEnv().getAttribute("layout");
		final String env_interp = token.getFullEnv().getAttribute("interpretation");

		if (env_format != null && !env_format.isEmpty()) {
			this.format = env_format;
		}
		if (env_layout != null && !env_layout.isEmpty()) {
			this.layout = env_layout;
		}
		if (env_interp != null && !env_interp.isEmpty()) {
			this.interpretation = env_interp;
		}

		log.info("preparing CREATE blackboard job: " + format + "-" + layout + "-" + interpretation);

		job.setAction("CREATE");
		job.getParameters().put("format", format);
		job.getParameters().put("layout", layout);
		job.getParameters().put("interpretation", interpretation);
		job.getParameters().put("backend_Id", defaultBackendId);
	}

	/**
	 * Gets the output prefix.
	 * 
	 * @return the output prefix
	 */
	public String getOutputPrefix() {
		return outputPrefix;
	}

	/**
	 * Sets the output prefix.
	 * 
	 * @param outputPrefix
	 *            the output prefix
	 */
	public void setOutputPrefix(final String outputPrefix) {
		this.outputPrefix = outputPrefix;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see eu.dnetlib.msro.workflows.nodes.BlackboardJobNode#generateBlackboardListener(com.googlecode.sarasvati.Engine,
	 * com.googlecode.sarasvati.NodeToken)
	 */
	@Override
	protected BlackboardWorkflowJobListener generateBlackboardListener(final Engine engine, final NodeToken token) {
		return new BlackboardWorkflowJobListener(engine, token) {

			@Override
			protected void populateEnv(final Env env, final Map<String, String> responseParams) {
				env.setAttribute(getOutputPrefix() + "format", getFormat());
				env.setAttribute(getOutputPrefix() + "layout", getLayout());
				env.setAttribute(getOutputPrefix() + "interpretation", getInterpretation());
				env.setAttribute(getOutputPrefix() + "id", responseParams.get("id"));
			}
		};
	}

	/**
	 * Gets the default backend id.
	 * 
	 * @return the default backend id
	 */
	public String getDefaultBackendId() {
		return defaultBackendId;
	}

	/**
	 * Sets the default backend id.
	 * 
	 * @param defaultBackendId
	 *            the default backend id
	 */
	@Required
	public void setDefaultBackendId(final String defaultBackendId) {
		this.defaultBackendId = defaultBackendId;
	}

	/**
	 * Gets the format.
	 * 
	 * @return the format
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * Sets the format.
	 * 
	 * @param format
	 *            the format
	 */
	public void setFormat(final String format) {
		this.format = format;
	}

	/**
	 * Gets the layout.
	 * 
	 * @return the layout
	 */
	public String getLayout() {
		return layout;
	}

	/**
	 * Sets the layout.
	 * 
	 * @param layout
	 *            the layout
	 */
	public void setLayout(final String layout) {
		this.layout = layout;
	}

	/**
	 * Gets the interpretation.
	 * 
	 * @return the interpretation
	 */
	public String getInterpretation() {
		return interpretation;
	}

	/**
	 * Sets the interpretation.
	 * 
	 * @param interpretation
	 *            the interpretation
	 */
	public void setInterpretation(final String interpretation) {
		this.interpretation = interpretation;
	}

}
