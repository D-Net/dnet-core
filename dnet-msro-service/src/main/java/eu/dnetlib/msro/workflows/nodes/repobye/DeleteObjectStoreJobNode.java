package eu.dnetlib.msro.workflows.nodes.repobye;

import com.googlecode.sarasvati.NodeToken;
import eu.dnetlib.data.objectstore.rmi.ObjectStoreService;
import eu.dnetlib.enabling.tools.blackboard.BlackboardJob;
import eu.dnetlib.msro.workflows.nodes.BlackboardJobNode;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DeleteObjectStoreJobNode extends BlackboardJobNode {

	private static final Log log = LogFactory.getLog(DeleteObjectStoreJobNode.class);

	private String objectstoreId;

	@Override
	protected String obtainServiceId(final NodeToken token) {
		return getServiceLocator().getServiceId(ObjectStoreService.class);
	}

	@Override
	protected void prepareJob(final BlackboardJob job, final NodeToken token) throws Exception {
		log.info("preparing blackboard job: delete of objectstore " + getObjectstoreId());
		job.setAction("DELETE");
		job.getParameters().put("obsID", getObjectstoreId());
	}

	/**
	 * @return the objectstoreId
	 */
	public String getObjectstoreId() {
		return objectstoreId;
	}

	/**
	 * @param objectstoreId
	 *            the objectstoreId to set
	 */
	public void setObjectstoreId(final String objectstoreId) {
		this.objectstoreId = objectstoreId;
	}

}
