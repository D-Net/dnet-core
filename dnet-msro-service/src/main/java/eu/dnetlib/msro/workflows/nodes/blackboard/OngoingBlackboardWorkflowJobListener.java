package eu.dnetlib.msro.workflows.nodes.blackboard;

import com.googlecode.sarasvati.Arc;
import com.googlecode.sarasvati.Engine;
import com.googlecode.sarasvati.NodeToken;

import eu.dnetlib.enabling.tools.blackboard.BlackboardJob;

public class OngoingBlackboardWorkflowJobListener extends BlackboardWorkflowJobListener {

	public OngoingBlackboardWorkflowJobListener(Engine engine, NodeToken token) {
		super(engine, token);
	}

	@Override
	protected void onOngoing(BlackboardJob job) {
		super.onOngoing(job);
		complete(job, Arc.DEFAULT_ARC);
	}

}
