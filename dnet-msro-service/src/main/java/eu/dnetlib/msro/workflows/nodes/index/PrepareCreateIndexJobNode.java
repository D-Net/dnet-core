package eu.dnetlib.msro.workflows.nodes.index;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.googlecode.sarasvati.NodeToken;

import eu.dnetlib.msro.workflows.nodes.SimpleJobNode;

public class PrepareCreateIndexJobNode extends SimpleJobNode {

	private static final Log log = LogFactory.getLog(PrepareCreateIndexJobNode.class);

	private String layout;
	private String format;
	private String interpretation;

	@Override
	protected String execute(final NodeToken token) throws Exception {
		log.info("Preparing env for CreateIndexJobNode");
		token.getEnv().setAttribute("layout", layout);
		token.getEnv().setAttribute("format", format);
		token.getEnv().setAttribute("interpretation", interpretation);
		return null;
	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(final String layout) {
		this.layout = layout;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(final String format) {
		this.format = format;
	}

	public String getInterpretation() {
		return interpretation;
	}

	public void setInterpretation(final String interpretation) {
		this.interpretation = interpretation;
	}

}
