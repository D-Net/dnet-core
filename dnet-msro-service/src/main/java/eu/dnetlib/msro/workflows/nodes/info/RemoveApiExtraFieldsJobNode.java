package eu.dnetlib.msro.workflows.nodes.info;

import com.googlecode.sarasvati.Arc;
import com.googlecode.sarasvati.NodeToken;

import eu.dnetlib.msro.workflows.nodes.SimpleJobNode;

@Deprecated
public class RemoveApiExtraFieldsJobNode extends SimpleJobNode {

	@Deprecated
	private String datasourceId;
	@Deprecated
	private String datasourceInterface;
	@Deprecated
	private String fields;

	@Override
	protected String execute(final NodeToken token) throws Exception {
		return Arc.DEFAULT_ARC;
	}

	public String getFields() {
		return fields;
	}

	public void setFields(final String fields) {
		this.fields = fields;
	}

	public String getDatasourceId() {
		return datasourceId;
	}

	public void setDatasourceId(final String datasourceId) {
		this.datasourceId = datasourceId;
	}

	public String getDatasourceInterface() {
		return datasourceInterface;
	}

	public void setDatasourceInterface(final String datasourceInterface) {
		this.datasourceInterface = datasourceInterface;
	}

}
