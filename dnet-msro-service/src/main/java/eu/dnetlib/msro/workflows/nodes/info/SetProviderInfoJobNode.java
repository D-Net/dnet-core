package eu.dnetlib.msro.workflows.nodes.info;

import com.googlecode.sarasvati.Arc;
import com.googlecode.sarasvati.NodeToken;
import eu.dnetlib.msro.workflows.nodes.SimpleJobNode;
import eu.dnetlib.msro.workflows.util.WorkflowsConstants;

public class SetProviderInfoJobNode extends SimpleJobNode {

	private String providerId;
	private String providerName;
	private String api;

	@Override
	protected String execute(NodeToken token) throws Exception {
		token.getEnv().setAttribute(WorkflowsConstants.DATAPROVIDER_ID, getProviderId());
		token.getEnv().setAttribute(WorkflowsConstants.DATAPROVIDER_NAME, getProviderName());
		token.getEnv().setAttribute(WorkflowsConstants.DATAPROVIDER_INTERFACE, getApi());

		token.getFullEnv().setAttribute(WorkflowsConstants.DATAPROVIDER_ID, getProviderId());
		token.getFullEnv().setAttribute(WorkflowsConstants.DATAPROVIDER_NAME, getProviderName());
		token.getFullEnv().setAttribute(WorkflowsConstants.DATAPROVIDER_INTERFACE, getApi());

		token.getProcess().getEnv().setAttribute(WorkflowsConstants.DATAPROVIDER_ID, getProviderId());
		token.getProcess().getEnv().setAttribute(WorkflowsConstants.DATAPROVIDER_NAME, getProviderName());
		token.getProcess().getEnv().setAttribute(WorkflowsConstants.DATAPROVIDER_INTERFACE, getApi());

		return Arc.DEFAULT_ARC;
	}

	public String getProviderId() {
		return providerId;
	}

	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getApi() {
		return api;
	}

	public void setApi(String api) {
		this.api = api;
	}

}
