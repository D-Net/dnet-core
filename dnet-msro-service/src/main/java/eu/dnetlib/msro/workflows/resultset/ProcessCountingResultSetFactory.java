package eu.dnetlib.msro.workflows.resultset;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

import org.springframework.beans.factory.annotation.Required;

import com.googlecode.sarasvati.GraphProcess;

import eu.dnetlib.enabling.resultset.ResultSetFactory;
import eu.dnetlib.enabling.resultset.client.utils.EPRUtils;
import eu.dnetlib.enabling.resultset.rmi.ResultSetException;
import eu.dnetlib.enabling.resultset.rmi.ResultSetService;
import eu.dnetlib.enabling.tools.ServiceResolver;
import eu.dnetlib.msro.workflows.util.ResultsetProgressProvider;

public class ProcessCountingResultSetFactory {

	private ResultSetFactory resultSetFactory;
	
	private ServiceResolver serviceResolver;
	
	public ResultsetProgressProvider createProgressProvider(final GraphProcess process, final String epr) throws ResultSetException {
		return createProgressProvider(process, (new EPRUtils()).getEpr(epr));
	}
	
	public ResultsetProgressProvider createProgressProvider(final GraphProcess process, final W3CEndpointReference epr) throws ResultSetException {
		final ResultSetService service = serviceResolver.getService(ResultSetService.class, epr);
		final String rsId = serviceResolver.getResourceIdentifier(epr);
		return new ResultsetProgressProvider(resultSetFactory, new ProcessCountingResultSetListener(process, service, rsId));
	}

	public ResultSetFactory getResultSetFactory() {
		return resultSetFactory;
	}

	@Required
	public void setResultSetFactory(ResultSetFactory resultSetFactory) {
		this.resultSetFactory = resultSetFactory;
	}


	public ServiceResolver getServiceResolver() {
		return serviceResolver;
	}

	@Required
	public void setServiceResolver(ServiceResolver serviceResolver) {
		this.serviceResolver = serviceResolver;
	}
}
