package eu.dnetlib.msro.workflows.nodes.objectStore;

import com.googlecode.sarasvati.NodeToken;
import eu.dnetlib.data.objectstore.rmi.ObjectStoreService;
import eu.dnetlib.enabling.tools.blackboard.BlackboardJob;
import eu.dnetlib.msro.workflows.nodes.BlackboardJobNode;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DropContentObjectStoreJobNode extends BlackboardJobNode {

	private static final Log log = LogFactory.getLog(DropContentObjectStoreJobNode.class);

	private String objectstoreId;

	@Override
	protected String obtainServiceId(final NodeToken token) {
		return getServiceLocator().getServiceId(ObjectStoreService.class);
	}

	@Override
	protected void prepareJob(final BlackboardJob job, final NodeToken token) {
		log.info("preparing blackboard job for the creation of the objectStore ");
		job.getParameters().put("obsID", getObjectstoreId());
		job.setAction("DROP_CONTENT");
	}

	public String getObjectstoreId() {
		return objectstoreId;
	}

	public void setObjectstoreId(final String objectstoreId) {
		this.objectstoreId = objectstoreId;
	}
}
