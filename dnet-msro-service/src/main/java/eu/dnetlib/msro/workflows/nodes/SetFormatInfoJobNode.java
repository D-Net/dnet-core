package eu.dnetlib.msro.workflows.nodes;

import static java.lang.String.format;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.googlecode.sarasvati.Arc;
import com.googlecode.sarasvati.NodeToken;

import eu.dnetlib.msro.workflows.nodes.SimpleJobNode;

public class SetFormatInfoJobNode extends SimpleJobNode {

	/**
	 * logger.
	 */
	private static final Log log = LogFactory.getLog(SetFormatInfoJobNode.class); // NOPMD by marko on 11/24/08 5:02 PM

	private String format;
	private String formatParam = "format";

	private String layout;
	private String layoutParam = "layout";

	private String interpretation;
	private String interpretationParam = "interpretation";

	/**
	 * {@inheritDoc}
	 * 
	 * @see com.googlecode.sarasvati.mem.MemNode#execute(com.googlecode.sarasvati.Engine, com.googlecode.sarasvati.NodeToken)
	 */
	@Override
	public String execute(final NodeToken token) {

		log.info(format("setting mdFromat: %s, layout: %s, interpretation: %s", getFormat(), getLayout(), getInterpretation()));

		token.getEnv().setAttribute(getFormatParam(), getFormat());
		token.getEnv().setAttribute(getLayoutParam(), getLayout());
		token.getEnv().setAttribute(getInterpretationParam(), getInterpretation());

		return Arc.DEFAULT_ARC;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(final String format) {
		this.format = format;
	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(final String layout) {
		this.layout = layout;
	}

	public String getInterpretation() {
		return interpretation;
	}

	public void setInterpretation(final String interpretation) {
		this.interpretation = interpretation;
	}

	public String getFormatParam() {
		return formatParam;
	}

	public void setFormatParam(final String formatParam) {
		this.formatParam = formatParam;
	}

	public String getLayoutParam() {
		return layoutParam;
	}

	public void setLayoutParam(final String layoutParam) {
		this.layoutParam = layoutParam;
	}

	public String getInterpretationParam() {
		return interpretationParam;
	}

	public void setInterpretationParam(final String interpretationParam) {
		this.interpretationParam = interpretationParam;
	}

}
