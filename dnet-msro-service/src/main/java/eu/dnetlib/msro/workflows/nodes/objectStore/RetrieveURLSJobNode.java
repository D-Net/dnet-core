package eu.dnetlib.msro.workflows.nodes.objectStore;

import javax.annotation.Resource;
import javax.xml.ws.wsaddressing.W3CEndpointReference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.collect.Iterables;
import com.googlecode.sarasvati.Arc;
import com.googlecode.sarasvati.NodeToken;

import eu.dnetlib.enabling.resultset.IterableResultSetFactory;
import eu.dnetlib.enabling.resultset.client.ResultSetClientFactory;
import eu.dnetlib.enabling.resultset.client.utils.EPRUtils;
import eu.dnetlib.msro.workflows.nodes.SimpleJobNode;
import eu.dnetlib.msro.workflows.nodes.download.UrlExtractor;

// TODO: Auto-generated Javadoc
/**
 * The Class RetrieveURLSJobNode.
 */
public class RetrieveURLSJobNode extends SimpleJobNode {

	/** The epr param. */
	private String inputEprParam;

	/** The output epr param. */
	private String outputEprParam;

	/** The xpath. */
	private String xpath;

	/** The xpath metadata id. */
	private String xpathMetadataId;

	/** The xpath open access. */
	private String xpathOpenAccess;

	/** The xpath embargo date. */
	private String xpathEmbargoDate;

	/** The result set client factory. */
	@Autowired
	private ResultSetClientFactory resultSetClientFactory;

	/** The result set factory. */
	@Resource(name = "iterableResultSetFactory")
	private IterableResultSetFactory resultSetFactory;

	/*
	 * (non-Javadoc)
	 *
	 * @see eu.dnetlib.msro.workflows.nodes.SimpleJobNode#execute(com.googlecode.sarasvati.NodeToken)
	 */
	@Override
	protected String execute(final NodeToken token) throws Exception {
		final W3CEndpointReference inputEpr = (new EPRUtils()).getEpr(token.getEnv().getAttribute(inputEprParam));
		Iterable<String> input = resultSetClientFactory.getClient(inputEpr);
		Iterable<String> extractedUrls = Iterables.transform(input, new UrlExtractor(xpath, xpathMetadataId, xpathOpenAccess, xpathEmbargoDate));
		W3CEndpointReference eprUrls = resultSetFactory.createIterableResultSet(extractedUrls);
		token.getEnv().setAttribute(getOutputEprParam(), eprUrls.toString());
		return Arc.DEFAULT_ARC;
	}

	/**
	 * Gets the xpath.
	 *
	 * @return the xpath
	 */
	public String getXpath() {
		return xpath;
	}

	/**
	 * Sets the xpath.
	 *
	 * @param xpath
	 *            the xpath to set
	 */
	public void setXpath(final String xpath) {
		this.xpath = xpath;
	}

	/**
	 * Gets the xpath metadata id.
	 *
	 * @return the xpathMetadataId
	 */
	public String getXpathMetadataId() {
		return xpathMetadataId;
	}

	/**
	 * Sets the xpath metadata id.
	 *
	 * @param xpathMetadataId
	 *            the xpathMetadataId to set
	 */
	@Required
	public void setXpathMetadataId(final String xpathMetadataId) {
		this.xpathMetadataId = xpathMetadataId;
	}

	/**
	 * Gets the result set client factory.
	 *
	 * @return the resultSetClientFactory
	 */
	public ResultSetClientFactory getResultSetClientFactory() {
		return resultSetClientFactory;
	}

	/**
	 * Sets the result set client factory.
	 *
	 * @param resultSetClientFactory
	 *            the resultSetClientFactory to set
	 */
	public void setResultSetClientFactory(final ResultSetClientFactory resultSetClientFactory) {
		this.resultSetClientFactory = resultSetClientFactory;
	}

	/**
	 * Gets the result set factory.
	 *
	 * @return the resultSetFactory
	 */
	public IterableResultSetFactory getResultSetFactory() {
		return resultSetFactory;
	}

	/**
	 * Sets the result set factory.
	 *
	 * @param resultSetFactory
	 *            the resultSetFactory to set
	 */
	public void setResultSetFactory(final IterableResultSetFactory resultSetFactory) {
		this.resultSetFactory = resultSetFactory;
	}

	/**
	 * Gets the output epr param.
	 *
	 * @return the outputEprParam
	 */
	public String getOutputEprParam() {
		return outputEprParam;
	}

	/**
	 * Sets the output epr param.
	 *
	 * @param outputEprParam
	 *            the outputEprParam to set
	 */
	public void setOutputEprParam(final String outputEprParam) {
		this.outputEprParam = outputEprParam;
	}

	/**
	 * Gets the input epr param.
	 *
	 * @return the inputEprParam
	 */
	public String getInputEprParam() {
		return inputEprParam;
	}

	/**
	 * Sets the input epr param.
	 *
	 * @param inputEprParam
	 *            the inputEprParam to set
	 */
	public void setInputEprParam(final String inputEprParam) {
		this.inputEprParam = inputEprParam;
	}

	/**
	 * Gets the xpath open access.
	 *
	 * @return the xpath open access
	 */
	public String getXpathOpenAccess() {
		return xpathOpenAccess;
	}

	/**
	 * Sets the xpath open access.
	 *
	 * @param xpathOpenAccess
	 *            the new xpath open access
	 */
	public void setXpathOpenAccess(final String xpathOpenAccess) {
		this.xpathOpenAccess = xpathOpenAccess;
	}

	/**
	 * Gets the xpath embargo date.
	 *
	 * @return the xpath embargo date
	 */
	public String getXpathEmbargoDate() {
		return xpathEmbargoDate;
	}

	/**
	 * Sets the xpath embargo date.
	 *
	 * @param xpathEmbargoDate
	 *            the new xpath embargo date
	 */
	public void setXpathEmbargoDate(final String xpathEmbargoDate) {
		this.xpathEmbargoDate = xpathEmbargoDate;
	}

}
