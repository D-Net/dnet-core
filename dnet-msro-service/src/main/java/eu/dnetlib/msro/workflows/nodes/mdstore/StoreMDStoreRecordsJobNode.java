package eu.dnetlib.msro.workflows.nodes.mdstore;

import java.util.Map;

import com.googlecode.sarasvati.Engine;
import com.googlecode.sarasvati.NodeToken;
import com.googlecode.sarasvati.env.Env;
import eu.dnetlib.data.mdstore.MDStoreService;
import eu.dnetlib.enabling.resultset.rmi.ResultSetException;
import eu.dnetlib.enabling.tools.blackboard.BlackboardJob;
import eu.dnetlib.msro.workflows.nodes.BlackboardJobNode;
import eu.dnetlib.msro.workflows.nodes.ProgressJobNode;
import eu.dnetlib.msro.workflows.nodes.blackboard.BlackboardWorkflowJobListener;
import eu.dnetlib.msro.workflows.resultset.ProcessCountingResultSetFactory;
import eu.dnetlib.msro.workflows.util.ProgressProvider;
import eu.dnetlib.msro.workflows.util.ResultsetProgressProvider;
import eu.dnetlib.msro.workflows.util.WorkflowsConstants;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Required;

public class StoreMDStoreRecordsJobNode extends BlackboardJobNode implements ProgressJobNode {

	private static final Log log = LogFactory.getLog(StoreMDStoreRecordsJobNode.class); // NOPMD by marko on 11/24/08 5:02 PM

	private String eprParam;
	private String mdId;
	private String storingType;
	private ProcessCountingResultSetFactory processCountingResultSetFactory;
	private ResultsetProgressProvider progressProvider;

	@Override
	protected String obtainServiceId(final NodeToken token) {
		return getServiceLocator().getServiceId(MDStoreService.class, mdId);
	}

	@Override
	protected void prepareJob(final BlackboardJob job, final NodeToken token) throws ResultSetException {
		job.setAction("FEED");

		final String eprS = token.getEnv().getAttribute(getEprParam());

		this.progressProvider = processCountingResultSetFactory.createProgressProvider(token.getProcess(), eprS);

		//for storing records after incremental transformation
		final String overrideStoringType = token.getFullEnv().getAttribute("operationType");
		if (StringUtils.isNotBlank(overrideStoringType)) {
			token.getFullEnv().setAttribute("storingType", overrideStoringType);
			setStoringType(overrideStoringType);
		}
		job.getParameters().put("epr", progressProvider.getEpr().toString());
		job.getParameters().put("storingType", getStoringType());
		job.getParameters().put("mdId", getMdId());
	}

	@Override
	protected BlackboardWorkflowJobListener generateBlackboardListener(final Engine engine, final NodeToken token) {
		return new BlackboardWorkflowJobListener(engine, token) {

			@Override
			protected void populateEnv(final Env env, final Map<String, String> responseParams) {
				log.info("New mdstore size : " + responseParams.get("mdstoreSize"));
				log.info("Number of write operations: " + responseParams.get("writeOps"));
				env.setAttribute(WorkflowsConstants.MAIN_LOG_PREFIX + "sinkSize", responseParams.get("mdstoreSize"));
				env.setAttribute(WorkflowsConstants.MAIN_LOG_PREFIX + "processedSize", responseParams.get("writeOps"));
			}
		};
	}

	public String getEprParam() {
		return eprParam;
	}

	public void setEprParam(final String eprParam) {
		this.eprParam = eprParam;
	}

	public String getMdId() {
		return mdId;
	}

	public void setMdId(final String mdId) {
		this.mdId = mdId;
	}

	public String getStoringType() {
		return storingType;
	}

	public void setStoringType(final String storingType) {
		this.storingType = storingType;
	}

	public ProcessCountingResultSetFactory getProcessCountingResultSetFactory() {
		return processCountingResultSetFactory;
	}

	@Required
	public void setProcessCountingResultSetFactory(final ProcessCountingResultSetFactory processCountingResultSetFactory) {
		this.processCountingResultSetFactory = processCountingResultSetFactory;
	}

	@Override
	public ProgressProvider getProgressProvider() {
		return progressProvider;
	}

}
