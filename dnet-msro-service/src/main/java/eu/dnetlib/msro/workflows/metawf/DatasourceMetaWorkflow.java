package eu.dnetlib.msro.workflows.metawf;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.core.io.Resource;

import eu.dnetlib.enabling.is.registry.rmi.ISRegistryException;
import eu.dnetlib.enabling.is.registry.rmi.ISRegistryService;
import eu.dnetlib.enabling.locators.UniqueServiceLocator;

public class DatasourceMetaWorkflow {

	private WorkflowTree tree;

	private Resource destroyWorkflowTemplate;

	@javax.annotation.Resource
	private UniqueServiceLocator serviceLocator;

	public int registerAllWorkflows(final Map<String, String> params) throws ISRegistryException, IOException {
		return tree.registerAllWorkflows(params);
	}

	public String registerDestroyWorkflow(final Map<String, String> params) throws ISRegistryException, IOException {
		final String profile = WorkflowProfileCreator.generateProfile("Repo BYE", "REPO_BYE", WorkflowStartModeEnum.auto, params, destroyWorkflowTemplate);
		return serviceLocator.getService(ISRegistryService.class).registerProfile(profile);
	}

	public String asXML() {
		final StringWriter sw = new StringWriter();
		tree.populateMetaWfXml(sw);
		return sw.toString();
	}

	public WorkflowTree getTree() {
		return tree;
	}

	@Required
	public void setTree(final WorkflowTree tree) {
		this.tree = tree;
	}

	public Resource getDestroyWorkflowTemplate() {
		return destroyWorkflowTemplate;
	}

	@Required
	public void setDestroyWorkflowTemplate(final Resource destroyWorkflowTemplate) {
		this.destroyWorkflowTemplate = destroyWorkflowTemplate;
	}

}
