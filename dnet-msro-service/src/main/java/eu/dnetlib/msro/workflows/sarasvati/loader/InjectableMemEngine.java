package eu.dnetlib.msro.workflows.sarasvati.loader;

import org.springframework.beans.factory.annotation.Required;

import com.googlecode.sarasvati.load.GraphLoader;
import com.googlecode.sarasvati.load.GraphLoaderImpl;
import com.googlecode.sarasvati.load.GraphValidatorAdapter;
import com.googlecode.sarasvati.mem.MemEngine;
import com.googlecode.sarasvati.mem.MemGraph;
import com.googlecode.sarasvati.mem.MemGraphFactory;

public class InjectableMemEngine extends MemEngine{
	/**
	 * custom graph factory.
	 */
	private MemGraphFactory graphFactory;

	@Override
	public MemGraphFactory getFactory() {
		return graphFactory;
	}

	@Override
	public GraphLoader<MemGraph> getLoader() {
		return new GraphLoaderImpl<MemGraph>(getFactory(), getRepository(), new GraphValidatorAdapter());
	}

	public boolean isArcExecutionStarted() {
		return arcExecutionStarted;
	}

	@Required
	public void setGraphFactory(final MemGraphFactory graphFactory) {
		this.graphFactory = graphFactory;
	}

	public MemGraphFactory getGraphFactory() {
		return graphFactory;
	}
}
