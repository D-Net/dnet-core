package eu.dnetlib.msro.workflows.metawf;

public enum WorkflowStartModeEnum {
	auto, manual, disabled
}
