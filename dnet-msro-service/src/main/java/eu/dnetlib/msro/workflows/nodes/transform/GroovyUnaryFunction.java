package eu.dnetlib.msro.workflows.nodes.transform;

import java.util.Map;

import eu.dnetlib.miscutils.functional.UnaryFunction;

public abstract class GroovyUnaryFunction implements UnaryFunction<String, String> {

	private Map<String, String> params;

	@Override
	abstract public String evaluate(String input);

	public Map<String, String> getParams() {
		return params;
	}

	public void setParams(final Map<String, String> params) {
		this.params = params;
	}

}
