package eu.dnetlib.msro.workflows.sarasvati.loader;

import eu.dnetlib.msro.workflows.util.WorkflowsConstants;

public class WfProfileDescriptor {
		
	private String name;
	private String type;
	private int priority = WorkflowsConstants.DEFAULT_WF_PRIORITY;
	private boolean ready = false;
	
	private String workflowXml;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getWorkflowXml() {
		return workflowXml;
	}
	public void setWorkflowXml(String workflowXml) {
		this.workflowXml = workflowXml;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public boolean isReady() {
		return ready;
	}
	public void setReady(boolean ready) {
		this.ready = ready;
	}
}
