package eu.dnetlib.msro.workflows.nodes;

import com.googlecode.sarasvati.Arc;
import com.googlecode.sarasvati.NodeToken;

public class SetEnvParameterJobNode extends SimpleJobNode {

	private String parameterName;
	private String parameterValue;

	@Override
	protected String execute(final NodeToken token) throws Exception {
		token.getEnv().setAttribute(parameterName, parameterValue);
		return Arc.DEFAULT_ARC;
	}

	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(final String parameterName) {
		this.parameterName = parameterName;
	}

	public String getParameterValue() {
		return parameterValue;
	}

	public void setParameterValue(final String parameterValue) {
		this.parameterValue = parameterValue;
	}

}
