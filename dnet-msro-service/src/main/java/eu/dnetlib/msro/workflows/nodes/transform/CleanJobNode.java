package eu.dnetlib.msro.workflows.nodes.transform;

import javax.annotation.Resource;
import javax.xml.ws.wsaddressing.W3CEndpointReference;

import com.googlecode.sarasvati.Arc;
import com.googlecode.sarasvati.NodeToken;

import eu.dnetlib.data.utility.cleaner.rmi.CleanerService;
import eu.dnetlib.enabling.locators.UniqueServiceLocator;
import eu.dnetlib.enabling.resultset.client.utils.EPRUtils;
import eu.dnetlib.msro.workflows.nodes.SimpleJobNode;

public class CleanJobNode extends SimpleJobNode {

	private String inputEprParam;
	private String outputEprParam;
	private String ruleId;

	@Resource
	private UniqueServiceLocator serviceLocator;

	@Override
	protected String execute(final NodeToken token) throws Exception {

		final W3CEndpointReference inputEpr = new EPRUtils().getEpr(token.getEnv().getAttribute(inputEprParam));
		final W3CEndpointReference outputEpr = ruleId == null || ruleId.isEmpty() ? inputEpr : serviceLocator.getService(CleanerService.class).clean(inputEpr,
				ruleId);

		token.getEnv().setAttribute(outputEprParam, outputEpr.toString());

		return Arc.DEFAULT_ARC;
	}

	public String getInputEprParam() {
		return inputEprParam;
	}

	public void setInputEprParam(final String inputEprParam) {
		this.inputEprParam = inputEprParam;
	}

	public String getOutputEprParam() {
		return outputEprParam;
	}

	public void setOutputEprParam(final String outputEprParam) {
		this.outputEprParam = outputEprParam;
	}

	public String getRuleId() {
		return ruleId;
	}

	public void setRuleId(final String ruleId) {
		this.ruleId = ruleId;
	}

}
