package eu.dnetlib.msro.workflows.util;


public interface ProgressProvider {
	public int getTotalValue();
	public int getCurrentValue();
	public boolean isInaccurate();
}
