package eu.dnetlib.msro.workflows.sarasvati.loader;

import org.springframework.beans.factory.annotation.Required;

import com.googlecode.sarasvati.load.NodeFactory;
import com.googlecode.sarasvati.mem.MemGraphFactory;

public class InjectableMemGraphFactory extends MemGraphFactory {

	/**
	 * custom graph factory.
	 */
	private NodeFactory nodeFactory;

	@Override
	public NodeFactory getNodeFactory(final String type) {
		return nodeFactory;
	}
	
	@Required
	public void setNodeFactory(final NodeFactory nodeFactory) {
		this.nodeFactory = nodeFactory;
	}

}
