package eu.dnetlib.msro.workflows.nodes.repohi;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.googlecode.sarasvati.Engine;
import com.googlecode.sarasvati.NodeToken;
import com.googlecode.sarasvati.env.Env;

import eu.dnetlib.data.mdstore.MDStoreService;
import eu.dnetlib.enabling.locators.comparators.HandledDatastructuresComparator;
import eu.dnetlib.enabling.tools.blackboard.BlackboardJob;
import eu.dnetlib.msro.workflows.nodes.BlackboardJobNode;
import eu.dnetlib.msro.workflows.nodes.blackboard.BlackboardWorkflowJobListener;

public class CreateMDStoreJobNode extends BlackboardJobNode {

	private static final Log log = LogFactory.getLog(CreateMDStoreJobNode.class);

	private String format;
	private String layout;
	private String interpretation;
	private String outputPrefix = "mdstore";

	@Override
	protected String obtainServiceId(final NodeToken token) {
		return getServiceLocator().getServiceId(MDStoreService.class, new HandledDatastructuresComparator());
	}

	@Override
	protected void prepareJob(final BlackboardJob job, final NodeToken token) {
		log.info("preparing blackboard job: " + getFormat());

		job.setAction("CREATE");
		job.getParameters().put("format", getFormat());
		job.getParameters().put("interpretation", getInterpretation());
		job.getParameters().put("layout", getLayout());
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(final String format) {
		this.format = format;
	}

	public String getInterpretation() {
		return interpretation;
	}

	public void setInterpretation(final String interpretation) {
		this.interpretation = interpretation;
	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(final String layout) {
		this.layout = layout;
	}

	public String getOutputPrefix() {
		return outputPrefix;
	}

	public void setOutputPrefix(final String outputPrefix) {
		this.outputPrefix = outputPrefix;
	}

	@Override
	protected BlackboardWorkflowJobListener generateBlackboardListener(final Engine engine, final NodeToken token) {
		return new BlackboardWorkflowJobListener(engine, token) {

			@Override
			protected void populateEnv(final Env env, final Map<String, String> responseParams) {
				env.setAttribute(getOutputPrefix() + "format", format);
				env.setAttribute(getOutputPrefix() + "layout", layout);
				env.setAttribute(getOutputPrefix() + "interpretation", interpretation);
				env.setAttribute(getOutputPrefix() + "id", responseParams.get("id"));
			}
		};
	}

}
