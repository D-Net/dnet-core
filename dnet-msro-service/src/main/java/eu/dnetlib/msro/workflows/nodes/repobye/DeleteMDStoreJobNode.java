package eu.dnetlib.msro.workflows.nodes.repobye;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.googlecode.sarasvati.NodeToken;

import eu.dnetlib.data.mdstore.MDStoreService;
import eu.dnetlib.enabling.tools.blackboard.BlackboardJob;
import eu.dnetlib.msro.workflows.nodes.BlackboardJobNode;

public class DeleteMDStoreJobNode extends BlackboardJobNode {

	private static final Log log = LogFactory.getLog(DeleteMDStoreJobNode.class);

	private String mdstoreId;

	@Override
	protected String obtainServiceId(final NodeToken token) {
		return getServiceLocator().getServiceId(MDStoreService.class, mdstoreId);
	}

	@Override
	protected void prepareJob(final BlackboardJob job, final NodeToken token) {
		log.info("preparing blackboard job: delete of mdstore " + getMdstoreId());
		job.setAction("DELETE");
		job.getParameters().put("id", getMdstoreId());
	}

	public String getMdstoreId() {
		return mdstoreId;
	}

	public void setMdstoreId(final String mdstoreId) {
		this.mdstoreId = mdstoreId;
	}

}
