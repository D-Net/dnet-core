package eu.dnetlib.msro.workflows.util;

public class WorkflowParamUI {
	private String label = "";
	private String url  = "";
	private boolean paramRequired = false;
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public boolean isParamRequired() {
		return paramRequired;
	}
	public void setParamRequired(boolean paramRequired) {
		this.paramRequired = paramRequired;
	}
		
}
