package eu.dnetlib.msro.workflows.util;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

import eu.dnetlib.enabling.resultset.ResultSetFactory;
import eu.dnetlib.msro.workflows.resultset.ProcessCountingResultSetListener;

public class ResultsetProgressProvider implements ProgressProvider {

	private W3CEndpointReference epr;
	private ProcessCountingResultSetListener listener;
		
	public ResultsetProgressProvider(ResultSetFactory resultSetFactory,	ProcessCountingResultSetListener listener) {
		super();
		this.epr = resultSetFactory.createResultSet(listener);
		this.listener = listener;
	}

	@Override
	public int getTotalValue() {
		return listener.getSize();
	}

	@Override
	public int getCurrentValue() {
		return listener.getCount();
	}

	@Override
	public boolean isInaccurate() {
		return listener.isInaccurate();
	}

	public W3CEndpointReference getEpr() {
		return epr;
	}

	public void setEpr(W3CEndpointReference epr) {
		this.epr = epr;
	}

	public ProcessCountingResultSetListener getListener() {
		return listener;
	}

	public void setListener(ProcessCountingResultSetListener listener) {
		this.listener = listener;
	}

}
