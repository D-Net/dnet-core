package eu.dnetlib.msro.workflows.util;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Required;

public class WorkflowsConstants {

	public static final String SYSTEM_WF_NAME = "system:wfName";
	public static final String SYSTEM_WF_PROFILE_ID = "system:profileId";
	public static final String SYSTEM_WF_PROFILE_NAME = "system:profileName";
	public static final String SYSTEM_WF_PROFILE_FAMILY = "system:profileFamily";
	public static final String SYSTEM_WF_PRIORITY = "system:priority";
	public static final String SYSTEM_WF_PROCESS_ID = "system:processId";
	public static final String SYSTEM_ERROR = "system:error";
	public static final String SYSTEM_HAS_FAILED = "system:hasFailed";
	public static final String SYSTEM_START_HUMAN_DATE = "system:startHumanDate";
	public static final String SYSTEM_START_DATE = "system:startDate";
	public static final String SYSTEM_END_HUMAN_DATE = "system:endHumanDate";
	public static final String SYSTEM_END_DATE = "system:endDate";

	public static final String SYSTEM_COMPLETED_SUCCESSFULLY = "system:isCompletedSuccessfully";
	public static final String BLACKBOARD_IS_BLACKBOARD = "blackboard:isBlackboard";
	public static final String BLACKBOARD_JOB = "blackboard:job";
	public static final String BLACKBOARD_SERVICE_ID = "blackboard:serviceId";
	public static final String BLACKBOARD_IS_GOING = "blackboard:isOngoing";
	public static final String BLACKBOARD_PARAM_PREFIX = "blackboard:param:";

	public static final String DATAPROVIDER_PREFIX = "dataprovider:";
	public static final String DATAPROVIDER_ID = WorkflowsConstants.DATAPROVIDER_PREFIX + "id";
	public static final String DATAPROVIDER_NAME = WorkflowsConstants.DATAPROVIDER_PREFIX + "name";
	public static final String DATAPROVIDER_ACRONYM = WorkflowsConstants.DATAPROVIDER_PREFIX + "acronym";
	public static final String DATAPROVIDER_URL = WorkflowsConstants.DATAPROVIDER_PREFIX + "url";
	public static final String DATAPROVIDER_INTERFACE = WorkflowsConstants.DATAPROVIDER_PREFIX + "interface";
	public static final String DATAPROVIDER_INTERFACE_COMPLIANCE = DATAPROVIDER_INTERFACE + ":compliance";
	public static final String DATAPROVIDER_INTERFACE_BASEURL = DATAPROVIDER_INTERFACE + ":baseUrl";
	public static final String DATAPROVIDER_ORIGINALID = DATAPROVIDER_PREFIX + "originalid";
	public static final String DATAPROVIDER_NAMESPACE_PREFIX = WorkflowsConstants.DATAPROVIDER_PREFIX + "nsPrefix";

	public static final int MIN_WF_PRIORITY = 0;
	public static final int MAX_WF_PRIORITY = 100;
	public static final int DEFAULT_WF_PRIORITY = 50;
	public static final int MAX_PENDING_PROCS_SIZE = 100;
	public static final int MAX_WF_THREADS = 10;
	public static final String MAIN_LOG_PREFIX = "mainlog:";

	public enum WorkflowStatus {
		EXECUTABLE("Executable", "icon-ok"), WAIT_USER_SETTINGS("Waiting user settings", "icon-edit"), WAIT_SYS_SETTINGS("Waiting system settings",
				"icon-refresh"), ASSIGNED("Assigned", "icon-ok-circle"), MISSING("Missing workflow", "icon-warning-sign");

		public String displayName;
		public String icon;

		WorkflowStatus(final String displayName, final String icon) {
			this.displayName = displayName;
			this.icon = icon;
		}
	}

	private String dataproviderProtocolsJson;
	private String dataproviderTypologiesJson;
	private List<Map<String, String>> dataproviderProtocols;
	private List<Map<String, String>> dataproviderTypologies;
	private List<Map<String, String>> dataproviderWorkflowStatuses;

	@SuppressWarnings("unchecked")
	public void init() {
		Gson gson = new Gson();
		dataproviderProtocols = gson.fromJson(dataproviderProtocolsJson, List.class);
		dataproviderTypologies = gson.fromJson(dataproviderTypologiesJson, List.class);
		dataproviderWorkflowStatuses = Lists.newArrayList();
		for (WorkflowStatus s : WorkflowStatus.values()) {
			Map<String, String> map = Maps.newHashMap();
			map.put("name", s.displayName);
			map.put("icon", s.icon);
			map.put("value", s.toString());
			dataproviderWorkflowStatuses.add(map);
		}
	}

	public String getDataproviderProtocolsJson() {
		return dataproviderProtocolsJson;
	}

	@Required
	public void setDataproviderProtocolsJson(final String dataproviderProtocolsJson) {
		this.dataproviderProtocolsJson = dataproviderProtocolsJson;
	}

	public String getDataproviderTypologiesJson() {
		return dataproviderTypologiesJson;
	}

	@Required
	public void setDataproviderTypologiesJson(final String dataproviderTypologiesJson) {
		this.dataproviderTypologiesJson = dataproviderTypologiesJson;
	}

	public List<Map<String, String>> getDataproviderProtocols() {
		return dataproviderProtocols;
	}

	public List<Map<String, String>> getDataproviderTypologies() {
		return dataproviderTypologies;
	}

	public List<Map<String, String>> getDataproviderWorkflowStatuses() {
		return dataproviderWorkflowStatuses;
	}

}
