package eu.dnetlib.msro.workflows.nodes.repohi;

import javax.annotation.Resource;

import com.google.common.base.Splitter;
import com.googlecode.sarasvati.Arc;
import com.googlecode.sarasvati.NodeToken;

import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpDocumentNotFoundException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService;
import eu.dnetlib.enabling.locators.UniqueServiceLocator;
import eu.dnetlib.msro.rmi.MSROException;
import eu.dnetlib.msro.workflows.nodes.SimpleJobNode;
import eu.dnetlib.msro.workflows.util.WorkflowsConstants;

public class VerifyDatasourceJobNode extends SimpleJobNode {

	@Resource
	private UniqueServiceLocator serviceLocator;

	private String expectedInterfaceTypologyPrefixes;

	private String expectedCompliancePrefixes;

	@Override
	protected String execute(final NodeToken token) throws Exception {
		final String dsId = token.getFullEnv().getAttribute(WorkflowsConstants.DATAPROVIDER_ID);
		final String ifaceId = token.getFullEnv().getAttribute(WorkflowsConstants.DATAPROVIDER_INTERFACE);
		final ISLookUpService lookupService = serviceLocator.getService(ISLookUpService.class);

		String compliance;
		try {
			compliance = lookupService.getResourceProfileByQuery("/*[.//RESOURCE_IDENTIFIER/@value='" + dsId + "']//INTERFACE[@id = '" + ifaceId
					+ "']/INTERFACE_EXTRA_FIELD[@name='overriding_compliance']/text()");
		} catch (final ISLookUpDocumentNotFoundException e) {
			compliance = lookupService.getResourceProfileByQuery("/*[.//RESOURCE_IDENTIFIER/@value='" + dsId + "']//INTERFACE[@id = '" + ifaceId
					+ "']/@compliance/string()");
		}

		final String typology = lookupService.getResourceProfileByQuery("/*[.//RESOURCE_IDENTIFIER/@value='" + dsId + "']//INTERFACE[@id = '" + ifaceId
				+ "']/@typology/string()");

		verifyValue(compliance, expectedCompliancePrefixes);
		verifyValue(typology, expectedInterfaceTypologyPrefixes);
		token.getFullEnv().setAttribute(WorkflowsConstants.DATAPROVIDER_INTERFACE_COMPLIANCE, compliance);

		if (isPending(dsId)) {
			return "validateDs";
		} else {
			return Arc.DEFAULT_ARC;
		}
	}

	private void verifyValue(final String value, final String expected) throws Exception {
		if (expected != null && !expected.isEmpty()) {
			for (final String s : Splitter.on(",").omitEmptyStrings().trimResults().split(expected)) {
				if (value.toLowerCase().startsWith(s.toLowerCase())) { return; }
			}
			throw new MSROException("Invalid value: " + value + ", Valid term prefixes are: [" + expected + "]");
		}
	}

	private boolean isPending(final String id) throws ISLookUpDocumentNotFoundException, ISLookUpException {
		final String query = "/*[.//RESOURCE_IDENTIFIER/@value='" + id + "']//RESOURCE_KIND/@value/string()";
		final String res = serviceLocator.getService(ISLookUpService.class).getResourceProfileByQuery(query);
		return res.trim().equals("PendingRepositoryResources");
	}

	public String getExpectedInterfaceTypologyPrefixes() {
		return expectedInterfaceTypologyPrefixes;
	}

	public void setExpectedInterfaceTypologyPrefixes(final String expectedInterfaceTypologyPrefixes) {
		this.expectedInterfaceTypologyPrefixes = expectedInterfaceTypologyPrefixes;
	}

	public String getExpectedCompliancePrefixes() {
		return expectedCompliancePrefixes;
	}

	public void setExpectedCompliancePrefixes(final String expectedCompliancePrefixes) {
		this.expectedCompliancePrefixes = expectedCompliancePrefixes;
	}

}
