package eu.dnetlib.msro.workflows.nodes.index;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.googlecode.sarasvati.NodeToken;

import eu.dnetlib.data.provision.index.rmi.IndexService;
import eu.dnetlib.enabling.tools.blackboard.BlackboardJob;
import eu.dnetlib.msro.workflows.nodes.BlackboardJobNode;

public class DeleteIndexJobNode extends BlackboardJobNode {

	private static final Log log = LogFactory.getLog(DeleteIndexJobNode.class);

	private String indexId;
	private String defaultIndexId;

	@Override
	protected String obtainServiceId(final NodeToken token) {
		return getServiceLocator().getServiceId(IndexService.class);
	}

	@Override
	protected void prepareJob(final BlackboardJob job, final NodeToken token) throws Exception {
		log.info("deleting index id = " + getIndexId());

		job.setAction("DELETE");
		job.getParameters().put("id", getIndexId());
		job.getParameters().put("backend_Id", defaultIndexId);

	}

	public String getIndexId() {
		return indexId;
	}

	public void setIndexId(final String indexId) {
		this.indexId = indexId;
	}

	public String getDefaultIndexId() {
		return defaultIndexId;
	}

	public void setDefaultIndexId(final String defaultIndexId) {
		this.defaultIndexId = defaultIndexId;
	}
}
