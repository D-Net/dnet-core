package eu.dnetlib.common.logging;

public enum DnetLogLevel {
	DEBUG, INFO, WARN, ERROR, FATAL
}
