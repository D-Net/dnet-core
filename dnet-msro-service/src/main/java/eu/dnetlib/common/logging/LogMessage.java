package eu.dnetlib.common.logging;

import java.util.Date;
import java.util.Map;

import com.google.common.collect.Maps;

import eu.dnetlib.common.logging.dao.DnetLoggerDao;

public class LogMessage {

	private DnetLoggerDao dao;
	private String logname;
	private Map<String, Object> details;

	public static final String LOG_LEVEL_FIELD = "log:level";
	public static final String LOG_DATE_FIELD = "log:date";

	public LogMessage(final DnetLoggerDao dao, final String logname, final DnetLogLevel level) {
		this.dao = dao;
		this.details = Maps.newHashMap();
		this.details.put(LOG_LEVEL_FIELD, level.toString());
		this.details.put(LOG_DATE_FIELD, (new Date()).getTime());
		this.logname = logname;
	}

	public LogMessage addDetail(final String name, final String value) {
		this.details.put(name, value);
		return this;
	}

	public LogMessage addDetails(final Map<String, String> map) {
		this.details.putAll(map);
		return this;
	}

	public void flush() {
		dao.writeLog(logname, details);
	}

}
