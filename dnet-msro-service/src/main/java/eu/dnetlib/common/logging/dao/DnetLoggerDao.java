package eu.dnetlib.common.logging.dao;

import java.util.*;

public interface DnetLoggerDao {

	void init(final String collection);

	void writeLog(final String collection, final Map<String, Object> map);

	void configureIndex(final String collection, final Map<String, IndexConf> indexConf);

	Iterator<Map<String, String>> obtainLogIterator(final String collection);

	Map<String, String> findOne(final String collection, final String key, final String value);

	Iterator<Map<String, String>> find(final String collection, final String key, final String value);

	Iterator<Map<String, String>> find(final String collection, final Map<String, Object> criteria);

	Iterator<Map<String, String>> findByDateRange(final String collection, final Date startDate, final Date endDate, final String key, final String value);

	Iterator<Map<String, String>> findByDateRange(final String collection, final Date startDate, final Date endDate);

	Iterator<Map<String, String>> findByDateRange(final String name, final Date startDate, final Date endDate, final Map<String, Object> criteria);

	Iterator<Map<String, String>> find(final String collection, final String cql, final List<String> fields);
}
