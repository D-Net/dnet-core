package eu.dnetlib.common.logging.dao;

import java.util.Map;

public class IndexConf {

	private String field;
	private Map<String, String> params;

	public IndexConf() {}

	public IndexConf(final String field, final Map<String, String> params) {
		super();
		this.field = field;
		this.params = params;
	}

	public String getField() {
		return field;
	}

	public void setField(final String field) {
		this.field = field;
	}

	public Map<String, String> getParams() {
		return params;
	}

	public void setParams(final Map<String, String> params) {
		this.params = params;
	}

}
