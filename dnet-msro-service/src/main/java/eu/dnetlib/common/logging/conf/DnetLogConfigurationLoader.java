package eu.dnetlib.common.logging.conf;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.Resource;

import com.google.common.collect.Maps;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValue;

import eu.dnetlib.common.logging.LogMessage;
import eu.dnetlib.common.logging.dao.IndexConf;

public class DnetLogConfigurationLoader {

	public static Map<String, IndexConf> getIndexedPaths(final Resource indexConf) {
		Map<String, IndexConf> response = Maps.newHashMap();
		response.put(LogMessage.LOG_LEVEL_FIELD, null);
		response.put(LogMessage.LOG_DATE_FIELD, null);
		if (indexConf != null) {
			Config config = loadConfiguration(indexConf);
			if (config != null) {
				for (Entry<String, ConfigValue> e : config.getObject("dnet.log.index.conf").entrySet()) {
					response.put(e.getKey(), null);
				}
			}
		}
		return response;
	}

	public static final Config loadConfiguration(final Resource resource) {
		if (resource != null) {
			try {
				return ConfigFactory.parseString(IOUtils.toString(resource.getInputStream()));
			} catch (IOException e) {
				return null;
			}
		}
		return null;
	}

}
