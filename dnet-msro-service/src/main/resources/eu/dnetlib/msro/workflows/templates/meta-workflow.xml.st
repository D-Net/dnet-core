<?xml version="1.0" encoding="UTF-8"?>
<RESOURCE_PROFILE>
	<HEADER>
		<RESOURCE_IDENTIFIER value="" />
		<RESOURCE_TYPE value="MetaWorkflowDSResourceType" />
		<RESOURCE_KIND value="MetaWorkflowDSResources" />
		<RESOURCE_URI value="" />
		<DATE_OF_CREATION value="2006-05-04T18:13:51.0Z" />
	</HEADER>
	<BODY>
		<DATAPROVIDER id="$dsId$" interface="$ifaceId$">$dsName$</DATAPROVIDER>
		<METAWORKFLOW_NAME family="$wfFamily$">$wfName$</METAWORKFLOW_NAME>
		<METAWORKFLOW_DESCRIPTION></METAWORKFLOW_DESCRIPTION>
		<METAWORKFLOW_SECTION>$section$</METAWORKFLOW_SECTION>
		<ADMIN_EMAIL>$adminEmail$</ADMIN_EMAIL>
		<CONFIGURATION status="$status$" />
		<SCHEDULING enabled="false">
			<CRON>0 0 0 ? * *</CRON>
			<MININTERVAL>10080</MININTERVAL>
		</SCHEDULING>
	</BODY>
</RESOURCE_PROFILE>
