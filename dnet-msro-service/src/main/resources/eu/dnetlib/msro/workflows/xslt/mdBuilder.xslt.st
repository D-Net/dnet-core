<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:datetime="http://exslt.org/dates-and-times" xmlns:exslt="http://exslt.org/common"
    xmlns:oai="http://www.openarchives.org/OAI/2.0/"
    xmlns:dnet="eu.dnetlib.miscutils.functional.xml.DnetXsltFunctions"
    xmlns:dri="http://www.driver-repository.eu/namespace/dri"
    exclude-result-prefixes="xsl datetime exslt dnet">
    
    <xsl:variable name="status" select="/*[local-name() = 'record']/*[local-name() = 'header']/@status" />
    
    <xsl:template match="/">
        <oai:record xmlns:oai="http://www.openarchives.org/OAI/2.0/"
            xmlns:dri="http://www.driver-repository.eu/namespace/dri"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            
            <xsl:variable name="datestamp" select="datetime:dateTime()" />
            <xsl:variable name="evaluatedMetadataId" select="normalize-space($xpath$)" />
            
			<!-- Patch OAI header or create ex-novo if not oai -->
            <oai:header>
                <xsl:if test="\$status">
                    <xsl:attribute name="status" >
                        <xsl:value-of select="\$status"/>
                    </xsl:attribute>
                </xsl:if>
                <xsl:if test="not(.//dri:objIdentifier)">
                	<xsl:if test="\$evaluatedMetadataId != ''">
	                    <dri:objIdentifier><xsl:value-of select="concat('$namespacePrefix$::', dnet:md5(\$evaluatedMetadataId))"/></dri:objIdentifier>
                	</xsl:if>
                </xsl:if>
                <xsl:if test="not(.//dri:recordIdentifier)">
                    <dri:recordIdentifier><xsl:value-of select="\$evaluatedMetadataId"/></dri:recordIdentifier>
                </xsl:if>
                <xsl:if test="not(.//dri:dateOfCollection)">
                    <dri:dateOfCollection><xsl:value-of select="\$datestamp"/></dri:dateOfCollection>
                </xsl:if>
				<xsl:if test="not(.//dri:repositoryId)">
                    <dri:repositoryId>$datasourceId$</dri:repositoryId>
                </xsl:if>
                <xsl:if test="not(.//dri:datasourceprefix)">
                    <dri:datasourceprefix>$namespacePrefix$</dri:datasourceprefix>
                </xsl:if>
                
				<!-- Bulk copy of old header -->
                <xsl:for-each select="/*[local-name() = 'record']/*[local-name() = 'header']/*">
                    <xsl:copy-of select="."/>
                </xsl:for-each>
            </oai:header>
            
			<!-- Handle metadata block -->
            <xsl:choose>
            	<!-- If OAI then copy copy the metadata block along -->
                <xsl:when test="count(/*[local-name() = 'record']/*[local-name() = 'metadata']) &gt; 0">
                    <xsl:copy-of select="/*[local-name() = 'record']/*[local-name() = 'metadata']"/>
                </xsl:when>
                <!-- If OAI copy in bulk the metadata block -->
                <xsl:when test="\$status = 'deleted'" />
                <!-- If not-OAI copy ALL the record inside metadata block -->
                <xsl:otherwise>
                    <oai:metadata>
                        <xsl:copy-of select="."/>
                    </oai:metadata>
                </xsl:otherwise>
            </xsl:choose>
            
            <!-- Handle oai:about block -->
            <oai:about>
                <provenance xmlns="http://www.openarchives.org/OAI/2.0/provenance" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/provenance http://www.openarchives.org/OAI/2.0/provenance.xsd">
  					<originDescription harvestDate="{\$datestamp}" altered="true">
	    				<baseURL>$baseurl$</baseURL>
	    				<identifier><xsl:value-of select="//*[local-name()='header']/*[local-name()='identifier']"/></identifier>
	    				<datestamp><xsl:value-of select="//*[local-name()='header']/*[local-name()='datestamp']"/></datestamp>
	    				<metadataNamespace>$metadatanamespace$</metadataNamespace>
	    				<xsl:copy-of select="//*[local-name()='provenance']/*[local-name() = 'originDescription']"/>
  					</originDescription>
				</provenance>
			</oai:about>
        </oai:record>
    </xsl:template>
    
</xsl:stylesheet>
