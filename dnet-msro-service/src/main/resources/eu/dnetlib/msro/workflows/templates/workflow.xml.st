<?xml version="1.0" encoding="UTF-8"?>
<RESOURCE_PROFILE>
    <HEADER>
        <RESOURCE_IDENTIFIER value=""/>
        <RESOURCE_TYPE value="WorkflowDSResourceType"/>
        <RESOURCE_KIND value="WorkflowDSResources"/>
        <RESOURCE_URI value=""/>
        <DATE_OF_CREATION value="2006-05-04T18:13:51.0Z"/>
    </HEADER>
    <BODY>
        <WORKFLOW_NAME>$name$</WORKFLOW_NAME>
		<WORKFLOW_TYPE>$type$</WORKFLOW_TYPE>
		<WORKFLOW_PRIORITY>$priority$</WORKFLOW_PRIORITY>
        <CONFIGURATION start="$startMode$">
        	$conf$
        </CONFIGURATION>
        <STATUS />
    </BODY>
</RESOURCE_PROFILE>