<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:nxsl="http://www.w3.org/1999/XSL/TransformXX"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

	<xsl:output omit-xml-declaration="yes" method="xml"
		encoding="UTF-8" />
	<xsl:namespace-alias stylesheet-prefix="nxsl"
		result-prefix="xsl" />

	<xsl:param name="format" />

	<xsl:template match="/">
		<xsl:apply-templates select="//LAYOUT" />
	</xsl:template>

	<xsl:template match="LAYOUT">
		<nxsl:stylesheet version="1.0"
			xmlns:dr="http://www.driver-repository.eu/namespace/dr" xmlns:dc="http://purl.org/dc/elements/1.1/"
			xmlns:dri="http://www.driver-repository.eu/namespace/dri" xmlns:dnet="eu.dnetlib.miscutils.functional.xml.DnetXsltFunctions"
			xmlns:exsl="http://exslt.org/common"
      extension-element-prefixes="exsl">

			<nxsl:output version="1.0" omit-xml-declaration="yes"
				method="xml" encoding="UTF8" />

			<nxsl:variable name="format">
				<xsl:value-of select="$format" />
			</nxsl:variable>

			<nxsl:template match="/">
				<indexRecord>
					<indexRecordIdentifier>
						<nxsl:value-of select="//dri:objIdentifier" />
					</indexRecordIdentifier>
					<targetFields>
						<nxsl:if test="count(//*[local-name()='metadata']/*) &gt; 0">
							<xsl:apply-templates select="FIELDS/FIELD[@indexable='true']" />
						</nxsl:if>		
					</targetFields>
					<result>
						<header>
							<dri:objIdentifier>
								<nxsl:value-of select="//dri:objIdentifier" />
							</dri:objIdentifier>
							<dri:repositoryId>
								<nxsl:value-of select="//dri:repositoryId" />
							</dri:repositoryId>
							<dri:dateOfCollection>
								<nxsl:value-of select="//dri:dateOfCollection" />
							</dri:dateOfCollection>
							<xsl:apply-templates select="FIELDS/FIELD"
								mode="header" />
						</header>
						<metadata>
							<xsl:apply-templates select="FIELDS/FIELD"
								mode="result" />
						</metadata>
					</result>
				</indexRecord>
			</nxsl:template>
		</nxsl:stylesheet>
	</xsl:template>

	<xsl:template match="FIELD[@indexable='true']">
		<xsl:choose>
			<!-- 
			<xsl:when test="@name = 'description'">
				<nxsl:for-each select="{@xpath}">
					<xsl:element name="{@name}">
						<nxsl:value-of select="substring(.,1,210)" />
					</xsl:element>
				</nxsl:for-each>
			</xsl:when>
			 -->
			<xsl:when test="@constant">
				<xsl:element name="{@name}">
					<xsl:value-of select="@constant" />
				</xsl:element>
			</xsl:when>
			<xsl:when test="@value and not(@xpath)">
				<nxsl:element name="{@name}">
					<nxsl:value-of select="{@value}" />
				</nxsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="value">
					<xsl:choose>
						<xsl:when test="@value">
							<xsl:value-of select="@value" />
						</xsl:when>
						<xsl:otherwise>
							.
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<nxsl:for-each select="{@xpath}">
					<xsl:element name="{@name}">
						<xsl:if test="@external='true'">
							<xsl:attribute name="external">true</xsl:attribute>
						</xsl:if>
						<xsl:choose>
							<xsl:when test="@tokenizable='false'">
								<nxsl:value-of select="normalize-space({normalize-space($value)})" />
							</xsl:when>
							<xsl:otherwise>
								<nxsl:value-of select="{normalize-space($value)}" />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:element>
				</nxsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<xsl:template match="FIELD" mode="result">
		<xsl:if test="@result='true'">
			<nxsl:copy-of select="{@xpath}" />
		</xsl:if>
	</xsl:template>

	<xsl:template match="FIELD" mode="header">
		<xsl:if test="@header='true'">
			<nxsl:copy-of select="{@xpath}" />
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>
