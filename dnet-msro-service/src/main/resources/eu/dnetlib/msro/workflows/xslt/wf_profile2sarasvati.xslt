<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<xsl:variable name="wfname" select="translate(.//WORKFLOW_NAME, ' ', '_')" />

		<process-definition name="{$wfname}" xmlns="http://sarasvati.googlecode.com/ProcessDefinition">
			<xsl:apply-templates select=".//NODE"/>            
			
			<node name="success" type="Success" />
			<node name="failure" type="Failure" />
		</process-definition>
	</xsl:template>

	<xsl:template match="NODE">
		<xsl:element name="node" namespace="http://sarasvati.googlecode.com/ProcessDefinition">
			<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			<xsl:if test="@isJoin = 'true'">
				<xsl:attribute name="joinType">and</xsl:attribute>
			</xsl:if>
			<xsl:if test="@isStart = 'true'">
				<xsl:attribute name="isStart">true</xsl:attribute>
			</xsl:if>
			<xsl:if test="string-length(@type) &gt; 0">
				<xsl:attribute name="type"><xsl:value-of select="@type" /></xsl:attribute>
			</xsl:if>
			
			<xsl:apply-templates select=".//ARC"/>
			
			<arc xmlns="http://sarasvati.googlecode.com/ProcessDefinition" name="failed" to="failure" />
			
			<xsl:if test="count(.//PARAM[string-length(normalize-space(text())) &gt; 0]) &gt; 0">
				<custom xmlns="http://sarasvati.googlecode.com/ProcessDefinition">
					<xsl:apply-templates select=".//PARAM"/>
				</custom>
			</xsl:if>
		</xsl:element>
	</xsl:template>

	<xsl:template match="ARC">
		<xsl:element name="arc" namespace="http://sarasvati.googlecode.com/ProcessDefinition">
			<xsl:attribute name="to"><xsl:value-of select="@to" /></xsl:attribute>
			<xsl:if test="string-length(@name) &gt; 0">
				<xsl:attribute name="name"><xsl:value-of select="@name" /></xsl:attribute>
			</xsl:if>
		</xsl:element> 
	</xsl:template>
	
	<xsl:template match="PARAM">
		<xsl:if test="string-length(normalize-space(text())) &gt; 0">
			<xsl:element name="{@name}" namespace="http://sarasvati.googlecode.com/ProcessDefinition"><xsl:value-of select="text()" /></xsl:element>
		</xsl:if>
	</xsl:template>
	
</xsl:stylesheet>