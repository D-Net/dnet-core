<div>
	<p>**** This mail has been generated automatically by D-NET Manager Service, please don't reply ****</p>
	<h3>Infrastructure: $infrastructure$ </h3>
	<p>Workflow: <b>$wfName$</b></p>
	<p>Status: <b>FAILURE</b></p>
	<p>
		Error message: <i>$error$</i><br /><br />
		You can relaunch it clicking <a href="$baseUrl$/workflows.do?wfId=$wfId$">HERE</a>
	</p>
	
	<hr />
	
	<p>
		<b>Workflow details:</b><br /><br />
		<table>
			<tr><td>Process ID: </td><td><a href="$baseUrl$/workflow_journal.do?procId=$procId$">$procId$</a></td></tr>
			$if(responses)$
				$responses.keys:{k|<tr><td>$k$: </td><td><i>$responses.(k)$</i></td></tr>}$
			$endif$
		</table>
	</p>
	<hr />
	
	$if(pendingWfs)$
		<p>
			If it's OK, you can launch manually its dependencies:
			<ul>$pendingWfs.keys:{k|<li><a href="$baseUrl$/workflows.do?wfId=$k$">$pendingWfs.(k)$</a></li>}$</ul>
		</p>
	$endif$
</div>
