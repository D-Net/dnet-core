package eu.dnetlib.msro.notification;

import java.io.IOException;
import java.util.Map;

import org.antlr.stringtemplate.StringTemplate;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Maps;

public class EmailDispatcherTest {

	private String successTemplatePath = "/eu/dnetlib/msro/mail/wf_success.mail.st";
	private String failureTemplatePath = "/eu/dnetlib/msro/mail/wf_failed.mail.st";
	private EmailDispatcher dispatcher;

	@Before
	public void setUp() throws Exception {
		dispatcher = new EmailDispatcher();
		dispatcher.setBaseUrl("http://the.base.url");
		dispatcher.setInfrastructure("container.hostname");
	}

	@Test
	public void testPrepareMessageSuccess() throws IOException {

		final String template = IOUtils.toString(getClass().getResourceAsStream(successTemplatePath));
		final Map<String, Object> map = Maps.newHashMap();
		map.put("wfId", "WF_ID");
		map.put("wfName", "WF_NAME");
		map.put("procId", "PROC_ID");

		StringTemplate st = dispatcher.prepareMessage(template, map);
		System.out.println(st.toString());
	}

	@Test
	public void testPrepareMessageFailure() throws IOException {

		final String template = IOUtils.toString(getClass().getResourceAsStream(failureTemplatePath));
		final Map<String, Object> map = Maps.newHashMap();
		map.put("wfId", "WF_ID");
		map.put("wfName", "WF_NAME");
		map.put("procId", "PROC_ID");

		StringTemplate st = dispatcher.prepareMessage(template, map);
		System.out.println(st.toString());
	}
}
