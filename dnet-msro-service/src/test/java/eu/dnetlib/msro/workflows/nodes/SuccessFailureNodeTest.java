package eu.dnetlib.msro.workflows.nodes;

import org.junit.Test;

/**
 * Created by Alessia Bardi on 23/10/2017.
 *
 * @author Alessia Bardi
 */
public class SuccessFailureNodeTest {

	private SuccessFailureNode node = new SuccessFailureNode();

	@Test
	public void testEscape(){
		String val = "This is open {. This is closed }.";
		System.out.println(node.escape(val));
	}
}
