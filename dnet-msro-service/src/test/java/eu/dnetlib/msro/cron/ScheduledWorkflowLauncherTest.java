package eu.dnetlib.msro.cron;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;

import eu.dnetlib.miscutils.datetime.DateUtils;

public class ScheduledWorkflowLauncherTest {

	private ScheduledWorkflowLauncher launcher = new ScheduledWorkflowLauncher();

	private static final String WF_ID = "wf-1234567890";
	private static final String CRON_EXPR = "0 15 9 ? * SAT";
	private static final int ALMOST_TWO_WEEKS = 20000;

	private static final DateUtils dateUtils = new DateUtils();
	private static final Date date = dateUtils.parse("2016-10-29T09:16:00+02:00"); // Saturday
	private static final Date oneDayBefore = dateUtils.parse("2016-10-28T09:16:00+02:00");
	private static final Date oneWeekBefore = dateUtils.parse("2016-10-22T09:16:00+02:00");
	private static final Date twoWeekBefore = dateUtils.parse("2016-10-15T09:16:00+02:00");
	private static final Date tenMinutesAfter = dateUtils.parse("2016-10-29T09:26:00+02:00");
	private static final Date oneHourAfter = dateUtils.parse("2016-10-29T10:16:00+02:00");

	@Test
	public void testVerifyScheduled() {
		assertTrue(launcher.isReady(WF_ID, CRON_EXPR, ALMOST_TWO_WEEKS, null, date));
		assertTrue(launcher.isReady(WF_ID, CRON_EXPR, ALMOST_TWO_WEEKS, twoWeekBefore, date));
		assertTrue(launcher.isReady(WF_ID, CRON_EXPR, ALMOST_TWO_WEEKS, null, tenMinutesAfter));
		assertTrue(launcher.isReady(WF_ID, CRON_EXPR, ALMOST_TWO_WEEKS, twoWeekBefore, tenMinutesAfter));

		assertFalse(launcher.isReady(WF_ID, CRON_EXPR, ALMOST_TWO_WEEKS, null, oneDayBefore));
		assertFalse(launcher.isReady(WF_ID, CRON_EXPR, ALMOST_TWO_WEEKS, oneDayBefore, date));
		assertFalse(launcher.isReady(WF_ID, CRON_EXPR, ALMOST_TWO_WEEKS, oneWeekBefore, date));

		assertFalse(launcher.isReady(WF_ID, CRON_EXPR, ALMOST_TWO_WEEKS, null, oneHourAfter));
		assertFalse(launcher.isReady(WF_ID, CRON_EXPR, ALMOST_TWO_WEEKS, twoWeekBefore, oneHourAfter));
	}

}
