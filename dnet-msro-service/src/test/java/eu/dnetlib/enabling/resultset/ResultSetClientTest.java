package eu.dnetlib.enabling.resultset;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

import eu.dnetlib.data.mdstore.MDStoreService;
import eu.dnetlib.data.mdstore.MDStoreServiceException;
import eu.dnetlib.enabling.resultset.client.IterableResultSetClient;
import eu.dnetlib.enabling.resultset.client.ResultSetClientFactory;
import eu.dnetlib.enabling.tools.ServiceResolver;
import junit.framework.Assert;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by claudio on 15/11/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/eu/dnetlib/enabling/resultset/applicationContext-test-locators.xml"})
public class ResultSetClientTest {

	private static final Log log = LogFactory.getLog(ResultSetClientTest.class);
	public static final String MDID = "9f6d4071-ffa5-407d-a3d6-8eba1aa12cbc_TURTdG9yZURTUmVzb3VyY2VzL01EU3RvcmVEU1Jlc291cmNlVHlwZQ==";

	private ResultSetClientFactory rsClientFactory;

	@Autowired
	private MDStoreService mdStoreService;

	@Autowired
	private ServiceResolver serviceResolver;

	@Before
	public void setUp() {
		rsClientFactory = new ResultSetClientFactory();
		rsClientFactory.setTimeout(12000);
		rsClientFactory.setConnectTimeout(5000);
		rsClientFactory.setServiceResolver(serviceResolver);
	}

	@Ignore
	@Test
	public void testMDStore() throws MDStoreServiceException {

		final W3CEndpointReference epr = mdStoreService.deliverMDRecords(MDID, null, null, null);
		final IterableResultSetClient client = rsClientFactory.getClient(epr);

		for(String xml : client) {
			Assert.assertTrue(StringUtils.isNotBlank(xml));
		}
	}

}
