package eu.dnetlib.test.utils;

import javax.xml.namespace.QName;
import javax.xml.ws.wsaddressing.W3CEndpointReference;
import javax.xml.ws.wsaddressing.W3CEndpointReferenceBuilder;

/**
 * Test units have to create some EPRs. This class offers static methods for this quick and dirty task.
 *
 * @author marko
 *
 */
public final class EPRTestUtil {

	/**
	 * coverage tests complain about missing private constructor invocation: here it is.
	 */
	@SuppressWarnings("unused")
	private static EPRTestUtil useless = new EPRTestUtil();

	/**
	 * prevents instantiation.
	 */
	private EPRTestUtil() {
		// prevents instantiation
	}

	/**
	 * creates a test epr with some fixed address.
	 *
	 * @return a test epr
	 */
	public static W3CEndpointReference getTestEpr() {
		return getTestEpr("http://test");
	}

	/**
	 * sometimes you may need different eprs.
	 *
	 * @param address some fake address
	 * @return a test epr
	 */
	public static W3CEndpointReference getTestEpr(final String address) {
		final W3CEndpointReferenceBuilder builder = new W3CEndpointReferenceBuilder();
		builder.address(address);
		builder.serviceName(new QName("http://something", "myService"));
		return builder.build();
	}
}
