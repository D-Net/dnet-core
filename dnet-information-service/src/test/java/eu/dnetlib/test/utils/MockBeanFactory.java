package eu.dnetlib.test.utils;


import org.springframework.beans.factory.FactoryBean;

import static org.mockito.Mockito.mock;

/**
 * Return a mockito mock for a given class.
 * This class should be updated according to new Spring4 factory Bean
 *
 * @author marko
 *
 */
@Deprecated 
public class MockBeanFactory implements FactoryBean {

	/**
	 * class to mock.
	 */
	private Class<?> clazz;

	/**
	 * {@inheritDoc}
	 * @see FactoryBean#getObject()
	 */
	public Object getObject() throws Exception {
		return mock(clazz);
	}

	/**
	 * {@inheritDoc}
	 * @see FactoryBean#getObjectType()
	 */
	public Class<?> getObjectType() {
		return clazz;
	}

	/**
	 * {@inheritDoc}
	 * @see FactoryBean#isSingleton()
	 */
	public boolean isSingleton() {
		return true;
	}

	public Class<?> getClazz() {
		return clazz;
	}

	public void setClazz(final Class<?> clazz) {
		this.clazz = clazz;
	}

}
