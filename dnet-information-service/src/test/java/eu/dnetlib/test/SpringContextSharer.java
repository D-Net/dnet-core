package eu.dnetlib.test;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * integration tests may need to access the container application context, for example in order 
 * directly interact with components or access shared resources like the CXF bus.
 * 
 * This bean should be instantiated in the container context.
 * 
 * @author marko
 *
 */
public class SpringContextSharer implements ApplicationContextAware {
	/**
	 * logger. 
	 */
	private static final Log log = LogFactory.getLog(SpringContextSharer.class); // NOPMD by marko on 11/24/08 5:02 PM

	/**
	 * global application context.
	 */
	private static ApplicationContext globalContext;
	
	/** 
	 * {@inheritDoc}
	 * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
	 */
	@Override
	public void setApplicationContext(final ApplicationContext context) {
		log.debug("registering global application context");
		globalContext = context;
	}
	
	public static ApplicationContext getGlobalContext() {
		return globalContext;
	}

}
