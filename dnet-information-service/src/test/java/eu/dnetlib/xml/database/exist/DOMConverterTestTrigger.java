package eu.dnetlib.xml.database.exist;

import org.exist.collections.triggers.SAXTrigger;
import org.exist.collections.triggers.TriggerException;
import org.exist.dom.persistent.DocumentImpl;
import org.exist.storage.DBBroker;
import org.exist.storage.txn.Txn;
import org.exist.xmldb.XmldbURI;

/**
 * helper class for the ExistDOMConverterTest.
 *
 * An instance of this class is instantiated by eXist and all new registered xml documents
 * should trigger the invocation of the triggerCreate method
 *
 * @author marko
 *
 */
public class DOMConverterTestTrigger extends SAXTrigger {




	@Override
	public void beforeCreateDocument(final DBBroker dbBroker, final Txn txn, final XmldbURI xmldbURI) throws TriggerException {
		ExistDOMConverterTest.setTriggered(true);
	}

	@Override
	public void afterCreateDocument(final DBBroker dbBroker, final Txn txn, final DocumentImpl document) throws TriggerException {
		ExistDOMConverterTest.setTriggered(true);
	}

	@Override
	public void beforeUpdateDocument(final DBBroker dbBroker, final Txn txn, final DocumentImpl document) throws TriggerException {
		ExistDOMConverterTest.setTriggered(true);
	}

	@Override
	public void afterUpdateDocument(final DBBroker dbBroker, final Txn txn, final DocumentImpl document) throws TriggerException {
		ExistDOMConverterTest.setTriggered(true);
	}

	@Override
	public void beforeUpdateDocumentMetadata(final DBBroker dbBroker, final Txn txn, final DocumentImpl document) throws TriggerException {
		ExistDOMConverterTest.setTriggered(true);
	}

	@Override
	public void afterUpdateDocumentMetadata(final DBBroker dbBroker, final Txn txn, final DocumentImpl document) throws TriggerException {
		ExistDOMConverterTest.setTriggered(true);
	}

	@Override
	public void beforeCopyDocument(final DBBroker dbBroker, final Txn txn, final DocumentImpl document, final XmldbURI xmldbURI) throws TriggerException {
		ExistDOMConverterTest.setTriggered(true);
	}

	@Override
	public void afterCopyDocument(final DBBroker dbBroker, final Txn txn, final DocumentImpl document, final XmldbURI xmldbURI) throws TriggerException {
		ExistDOMConverterTest.setTriggered(true);
	}

	@Override
	public void beforeMoveDocument(final DBBroker dbBroker, final Txn txn, final DocumentImpl document, final XmldbURI xmldbURI) throws TriggerException {
		ExistDOMConverterTest.setTriggered(true);
	}

	@Override
	public void afterMoveDocument(final DBBroker dbBroker, final Txn txn, final DocumentImpl document, final XmldbURI xmldbURI) throws TriggerException {
		ExistDOMConverterTest.setTriggered(true);
	}

	@Override
	public void beforeDeleteDocument(final DBBroker dbBroker, final Txn txn, final DocumentImpl document) throws TriggerException {
		ExistDOMConverterTest.setTriggered(true);
	}

	@Override
	public void afterDeleteDocument(final DBBroker dbBroker, final Txn txn, final XmldbURI xmldbURI) throws TriggerException {
		ExistDOMConverterTest.setTriggered(true);
	}
}
