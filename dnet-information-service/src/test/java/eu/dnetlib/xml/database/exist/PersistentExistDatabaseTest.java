package eu.dnetlib.xml.database.exist;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

public class PersistentExistDatabaseTest {

	private PersistentExistDatabase db;

	@Before
	public void setUp() throws Exception {
		db = new PersistentExistDatabase();
	}

	@Test
	public void testFile() throws IOException {
		File file = File.createTempFile("test", "conf");
		FileWriter writer = new FileWriter(file);
		try {
			IOUtils.write("test enable-java-binding=\"no\" test", writer);
		} finally {
			writer.close();
		}

		db.enableJava(file, true);
	}

	@Test
	public void testPatch() {
		assertEquals("test123", db.patchConfigFileEnableJava("test123", true));
		assertEquals("enable-java-binding=\"yes\"", db.patchConfigFileEnableJava("enable-java-binding=\"yes\"", true));
		assertEquals("enable-java-binding=\"no\"", db.patchConfigFileEnableJava("enable-java-binding=\"yes\"", false));
		assertEquals("enable-java-binding=\"yes\"", db.patchConfigFileEnableJava("enable-java-binding=\"no\"", true));
		assertEquals("enable-java-binding=\"no\"", db.patchConfigFileEnableJava("enable-java-binding=\"no\"", false));

		assertEquals("yyy enable-java-binding=\"yes\" xxx", db.patchConfigFileEnableJava("yyy enable-java-binding=\"no\" xxx", true));
	}

}
