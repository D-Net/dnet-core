package eu.dnetlib.xml.database.exist;

import org.w3c.dom.Document;

import eu.dnetlib.xml.database.LoggingTrigger;

/**
 * test high level xml db trigger tester.
 *
 * @author marko
 *
 */
public class TestTrigger extends LoggingTrigger {

	/**
	 * true when the trigger is first called on 'create' event.
	 */
	private boolean created = false; // NOPMD
	/**
	 * true when the trigger is first called on 'update' event.
	 */
	private boolean updated = false; // NOPMD
	/**
	 * true when the trigger is first called on 'delete' event.
	 */
	private boolean deleted = false; // NOPMD

	/**
	 * last triggered file name. 
	 */
	private String lastFile;
	/**
	 * last triggered collection name.
	 */
	private String lastCollection;

	/**
	 * {@inheritDoc}
	 * @see eu.dnetlib.xml.database.Trigger#created(java.lang.String, java.lang.String, org.w3c.dom.Document)
	 */
	@Override
	public void created(final String file, final String collection, final Document newDoc) {
		super.created(file, collection, newDoc);

		setLastFile(file);
		setLastCollection(collection);
		created = true;
	}

	/**
	 * {@inheritDoc}
	 * @see eu.dnetlib.xml.database.Trigger#deleted(java.lang.String, java.lang.String, org.w3c.dom.Document)
	 */
	@Override
	public void deleted(final String file, final String collection, final Document oldDoc) {
		super.deleted(file, collection, oldDoc);

		setLastFile(file);
		setLastCollection(collection);
		deleted = true;
	}

	/**
	 * {@inheritDoc}
	 * @see eu.dnetlib.xml.database.Trigger#updated(java.lang.String, java.lang.String, org.w3c.dom.Document, org.w3c.dom.Document)
	 */
	@Override
	public void updated(final String file, final String collection, final Document oldDoc, final Document newDoc) {
		super.updated(file, collection, oldDoc, newDoc);

		setLastFile(file);
		setLastCollection(collection);
		updated = true;
	}

	/**
	 * reset crude sentinels.
	 */
	public void reset() {
		setCreated(false);
		setUpdated(false);
		setDeleted(false);
	}

	public boolean isCreated() {
		return created;
	}

	public void setCreated(final boolean created) {
		this.created = created;
	}

	public boolean isUpdated() {
		return updated;
	}

	public void setUpdated(final boolean updated) {
		this.updated = updated;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(final boolean deleted) {
		this.deleted = deleted;
	}

	String getLastFile() {
		return lastFile;
	}

	void setLastFile(final String lastFile) {
		this.lastFile = lastFile;
	}

	String getLastCollection() {
		return lastCollection;
	}

	void setLastCollection(final String lastCollection) {
		this.lastCollection = lastCollection;
	}
}
