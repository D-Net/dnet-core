package eu.dnetlib.xml.database.exist;

import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.exist.collections.triggers.SAXTrigger;
import org.exist.collections.triggers.TriggerException;
import org.exist.dom.persistent.DocumentImpl;
import org.exist.storage.DBBroker;
import org.exist.storage.txn.Txn;
import org.exist.xmldb.XmldbURI;

/**
 * simple test trigger.
 *
 * @author marko
 *
 */
public class TestExistTrigger extends SAXTrigger {
	/**
	 * logger.
	 */
	private static final Log log = LogFactory.getLog(TestExistTrigger.class); // NOPMD by marko on 11/24/08 5:02 PM
	/**
	 * expression.
	 */
	private transient XPathExpression expr;

	/**
	 * useless.
	 */
	public TestExistTrigger() {
		super();

		try {
			expr = XPathFactory.newInstance().newXPath().compile("//hello");
		} catch (XPathExpressionException e) {
			log.fatal("cannot parse xpath", e);
		}
	}



	@Override
	public void beforeCreateDocument(final DBBroker dbBroker, final Txn txn, final XmldbURI xmldbURI) throws TriggerException {

	}

	@Override
	public void afterCreateDocument(final DBBroker dbBroker, final Txn txn, final DocumentImpl document) throws TriggerException {
		try {
			log.debug("created new document: " + expr.evaluate(document));
		} catch (XPathExpressionException e) {
			log.fatal("xpatthing", e);
		}
	}

	@Override
	public void beforeUpdateDocument(final DBBroker dbBroker, final Txn txn, final DocumentImpl document) throws TriggerException {
		try {
			log.info("old value: " + expr.evaluate(document));

		} catch (XPathExpressionException e) {
			log.fatal("xpatthing", e);
		}
	}

	@Override
	public void afterUpdateDocument(final DBBroker dbBroker, final Txn txn, final DocumentImpl document) throws TriggerException {

		try {
			log.info("new value: " + expr.evaluate(document));
		} catch (XPathExpressionException e) {
			log.fatal("xpatthing", e);
		}

	}

	@Override
	public void beforeUpdateDocumentMetadata(final DBBroker dbBroker, final Txn txn, final DocumentImpl document) throws TriggerException {

	}

	@Override
	public void afterUpdateDocumentMetadata(final DBBroker dbBroker, final Txn txn, final DocumentImpl document) throws TriggerException {

	}

	@Override
	public void beforeCopyDocument(final DBBroker dbBroker, final Txn txn, final DocumentImpl document, final XmldbURI xmldbURI) throws TriggerException {

	}

	@Override
	public void afterCopyDocument(final DBBroker dbBroker, final Txn txn, final DocumentImpl document, final XmldbURI xmldbURI) throws TriggerException {

	}

	@Override
	public void beforeMoveDocument(final DBBroker dbBroker, final Txn txn, final DocumentImpl document, final XmldbURI xmldbURI) throws TriggerException {

	}

	@Override
	public void afterMoveDocument(final DBBroker dbBroker, final Txn txn, final DocumentImpl document, final XmldbURI xmldbURI) throws TriggerException {

	}

	@Override
	public void beforeDeleteDocument(final DBBroker dbBroker, final Txn txn, final DocumentImpl document) throws TriggerException {
		try {
			log.debug("deleted document: " + expr.evaluate(document));
		} catch (XPathExpressionException e) {
			log.fatal("xpatthing", e);
		}

	}

	@Override
	public void afterDeleteDocument(final DBBroker dbBroker, final Txn txn, final XmldbURI xmldbURI) throws TriggerException {

	}
}
