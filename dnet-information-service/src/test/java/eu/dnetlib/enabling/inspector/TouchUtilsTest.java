package eu.dnetlib.enabling.inspector;

import static org.junit.Assert.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.DocumentException;
import org.junit.Before;
import org.junit.Test;

public class TouchUtilsTest {
	private static final Log log = LogFactory.getLog(TouchUtilsTest.class); // NOPMD by marko on 11/24/08 5:02 PM

	private transient TouchUtils touch;

	@Before
	public void setUp() throws Exception {
		touch = new TouchUtils();
	}

	@Test
	public void testUpdateProfile() throws DocumentException {
		log.fatal("test");

		final String res = touch.updateProfile("<a><b/></a>", "/a/b");
		System.out.println(res);
	}

	@Test(expected = DocumentException.class)
	public void testUpdateProfile2() throws DocumentException {
		touch.updateProfile("<a><b/></a>", "/a/x");
	}

	@Test(expected = DocumentException.class)
	public void testUpdateProfile3() throws DocumentException {
		touch.updateProfile("<a>xx</a>", "/a/text()");
	}

	@Test
	public void testVerifyIsDate() {
		assertTrue(touch.verifyIsDate("2010-04-27T12:04:22+02:00"));
		assertTrue(touch.verifyIsDate("2010-04-27T12:04:22Z"));
		assertTrue(touch.verifyIsDate("2010-04-27T12:04:22+00:00"));
		assertTrue(touch.verifyIsDate("2010-04-27T12:04:22+0000"));
		assertFalse(touch.verifyIsDate("201asdas"));
	}

	@Test
	public void testSpanize() {
		String res = touch.spanize("<a><b/><b><c v='1'/><c v='2'/></b></a>");
		System.out.println(res);
	}

}
