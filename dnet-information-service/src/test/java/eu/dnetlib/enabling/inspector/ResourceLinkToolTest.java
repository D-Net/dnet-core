package eu.dnetlib.enabling.inspector;


import org.junit.Before;
import org.junit.Test;

import eu.dnetlib.enabling.tools.CompatResourceIdentifierResolverImpl;

public class ResourceLinkToolTest {

	private static String PROFILE = 
		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
		"<RESOURCE_PROFILE xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"+
		    "<HEADER>\n"+
		        "<RESOURCE_IDENTIFIER value=\"3cdc5e58-24c6-47ac-b678-401c84956e35_VHJhbnNmb3JtYXRpb25EU1Jlc291cmNlcy9UcmFuc2Zvcm1hdGlvbkRTUmVzb3VyY2VUeXBl\"/>\n"+
		        "<RESOURCE_TYPE value=\"TransformationDSResourceType\"/>\n"+
		        "<RESOURCE_KIND value=\"TransformationDSResources\"/>\n"+
		        "<RESOURCE_URI value=\"http://146.48.85.175:8280/efg-is/services/msro\"/>\n"+
		        "<DATE_OF_CREATION value=\"2010-10-27T10:41:24+02:00\"/>\n"+
		    "</HEADER>\n"+
		    "<BODY>\n"+
		        "<CONFIGURATION>\n"+
		            "<REPOSITORY_SERVICE_IDENTIFIER>d937bab1-d44c-44aa-bf7d-df5312a3b623_UmVwb3NpdG9yeVNlcnZpY2VSZXNvdXJjZXMvUmVwb3NpdG9yeVNlcnZpY2VSZXNvdXJjZVR5cGU=</REPOSITORY_SERVICE_IDENTIFIER>\n"+
		            "<TRANSFORMATION_MANAGER_SERVICE_IDENTIFIER/>\n"+
		            "<TRANSFORMATION_MANAGER_SERVICE_URI/>\n"+
		            "<SOURCE_METADATA_FORMATS>\n"+
		                "<SOURCE_METADATA_FORMAT name=\"oai_efg\" layout=\"store\" interpretation=\"native\"/>\n"+
		            "</SOURCE_METADATA_FORMATS>\n"+
		            "<SINK_METADATA_FORMAT interpretation=\"cleaned\" name=\"oai_efg\" layout=\"store\"/>\n"+
		            "<DATA_SOURCES>\n"+
		                "<DATA_SOURCE>dnet://MDStoreDS/8d1ab5bb-176c-4eef-9ed3-b46a0aa31d09_TURTdG9yZURTUmVzb3VyY2VzL01EU3RvcmVEU1Jlc291cmNlVHlwZQ==</DATA_SOURCE>\n"+
		            "</DATA_SOURCES>\n"+
		            "<DATA_SINK>dnet://MDStoreDS/a3a2c757-1726-41b2-9e37-bceb3f8bbd8b_TURTdG9yZURTUmVzb3VyY2VzL01EU3RvcmVEU1Jlc291cmNlVHlwZQ==?type=REFRESH</DATA_SINK>\n"+
		            "<TRANSFORMATION_RULE_DS_IDENTIFIER/>\n"+
		            "<SCHEDULING/>\n"+
		        "</CONFIGURATION>\n"+
		        "<STATUS>\n"+
		            "<LAST_UPDATE_DATE>2011-01-24T17:22:09+01:00</LAST_UPDATE_DATE>\n"+
		            "<LAST_UPDATE_STATUS>LAST_UPDATE_STATUS</LAST_UPDATE_STATUS>\n"+
		            "<LAST_UPDATE_ERROR_MESSAGE>LAST_UPDATE_ERROR_MESSAGE</LAST_UPDATE_ERROR_MESSAGE>\n"+
		        "</STATUS>\n"+
		        "<SECURITY_PARAMETERS>SECURITY_PARAMETERS</SECURITY_PARAMETERS>\n"+
		    "</BODY>\n"+
		"</RESOURCE_PROFILE>";		
	
	private ResourceLinkTool linkTool;
	
	@Before
	public void setUp() throws Exception {
		linkTool = new ResourceLinkTool();
		linkTool.setResolver(new CompatResourceIdentifierResolverImpl());
	}
	
	@Test
	public void linkfyToHtmlTest() {
		System.out.println(linkTool.linkfyToHtml(PROFILE));
	}

}
