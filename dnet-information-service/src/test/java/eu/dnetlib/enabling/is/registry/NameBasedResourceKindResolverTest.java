package eu.dnetlib.enabling.is.registry;


import static org.junit.Assert.*;

import javax.xml.xpath.XPathExpressionException;

import org.junit.Before;
import org.junit.Test;

/**
 * Test the name based resource kind resolver.
 * 
 * @author marko
 *
 */
public class NameBasedResourceKindResolverTest {

	/**
	 * instance under test.
	 */
	private NameBasedResourceKindResolver resolver;
	
	@Before
	public void setUp() throws Exception {
		resolver = new NameBasedResourceKindResolver();
	}
	
	@Test
	public void testService() throws XPathExpressionException {
		assertEquals("ServiceResources", resolver.getNormalKindForType("MyNewServiceResourceType"));
		assertEquals("PendingServiceResources", resolver.getPendingKindForType("MyNewServiceResourceType"));
	}
	
	@Test
	public void testGeneric() throws XPathExpressionException {
		assertEquals("MyNewResources", resolver.getNormalKindForType("MyNewResourceType"));
		assertEquals("PendingDSResources", resolver.getPendingKindForType("MyNewResourceType"));
	}

}
