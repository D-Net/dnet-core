package eu.dnetlib.enabling.is.sn.resourcestate.hib;

import java.util.Collection;

import javax.annotation.Resource;

import org.junit.After;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.dnetlib.enabling.is.sn.resourcestate.AbstractResourceStateSubscriptionRegistryTest;
import eu.dnetlib.enabling.is.sn.resourcestate.ResourceStateSubscription;
import eu.dnetlib.enabling.is.sn.resourcestate.ResourceStateSubscriptionDAO;

/**
 * Perform ResourceStateSubscriptionRegistryTest tests on a hibernate backed resource state subscription dao instance.
 * 
 * @author marko
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "HibernateResourceStateSubscriptionDAOImplTest-context.xml" })
public class HibernateResourceStateSubscriptionRegistryTest extends AbstractResourceStateSubscriptionRegistryTest { // NOPMD

	/**
	 * hibernate subscription dao.
	 */
	@Resource
	private transient ResourceStateSubscriptionDAO dao;

	/**
	 * cleanup db. hsql doesn't recreate the database on spring context reload.
	 */
	@After
	public void tearDown() {
		final Collection<ResourceStateSubscription> subs = dao.listSubscriptions();
		if (subs != null)
			for (ResourceStateSubscription r : subs)
				dao.removeSubscription(r.getSubscriptionId());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.resourcestate.AbstractResourceStateSubscriptionRegistryTest#setUpDao()
	 */
	@Override
	protected void setUpDao() {
		getRegistry().setSubscriptionDao(dao);
	}

}
