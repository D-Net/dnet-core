package eu.dnetlib.enabling.is.sn.resourcestate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

/**
 * Concrete resourcestate subscription dao test for memory backed subscription store.
 * 
 * @author marko
 *
 */
public class MemoryResourceStateSubscriptionDAOImplTest extends AbstractResourceStateSubscriptionDAOImplTest {

	/**
	 * instance under test (of the specific type).
	 */
	private transient MemoryResourceStateSubscriptionDAOImpl memdao;
	
	/** 
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.is.sn.resourcestate.AbstractResourceStateSubscriptionDAOImplTest#prepareDao()
	 */
	@Override
	protected void prepareDao() {
		memdao = new MemoryResourceStateSubscriptionDAOImpl();
		setDao(memdao);
	}

	/**
	 * test add subscription.
	 */
	@Test
	public void testAddSubscription() {
		assertNull("not registered", getDao().getSubscription(getSub().getSubscriptionId()));
	
		getDao().addSubscription(getSub());
		assertEquals("check that sub is registered", getSub(), memdao.getById().get(getSub().getSubscriptionId()));
		
		getDao().addSubscription(getSub2());
		assertEquals("check that sub2 is registered", getSub2(), memdao.getById().get(getSub2().getSubscriptionId()));
		
		getDao().addSubscription(getSub3());
		assertEquals("check that sub4 is registered", getSub3(), memdao.getById().get(getSub3().getSubscriptionId()));
		
		getDao().addSubscription(getSub4());
		assertEquals("check that sub4is registered", getSub4(), memdao.getById().get(getSub4().getSubscriptionId()));
	}

}
