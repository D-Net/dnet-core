package eu.dnetlib.enabling.is.store; // NOPMD

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.xmldb.api.base.XMLDBException;

import eu.dnetlib.enabling.is.store.rmi.ISStoreException;
import eu.dnetlib.xml.database.XMLDatabase;

/**
 * test the ISStore service.
 * 
 * @author marko
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ISStoreServiceImplTest {

	/**
	 * instance under test.
	 */
	private transient ISStoreServiceImpl storeService;
	
	/**
	 * xml database mock.
	 */
	@Mock 
	private transient XMLDatabase xmlDatabase; 
	
	/**
	 * setup class under test.
	 */
	@Before
	public void setUp() {
		storeService = new ISStoreServiceImpl();
		storeService.setXmlDatabase(xmlDatabase);
	}

	/**
	 * test that quick search profile returns null instead of an empty list.
	 * 
	 * @throws XMLDBException shouldn't happen
	 * @throws ISStoreException shouldn't happen
	 */
	@Test
	public void testQuickSearchXML() throws XMLDBException, ISStoreException {
		final Iterator<String> rset = mock(Iterator.class);
		when(rset.hasNext()).thenReturn(true, false);
		when(xmlDatabase.xquery(anyString())).thenReturn(rset);
		
		final List<String> res = storeService.quickSearchXML("someQuery");
		
		assertNotNull("empty result not null", res);
		assertFalse("empty result is empty", res.isEmpty());
	}

}
