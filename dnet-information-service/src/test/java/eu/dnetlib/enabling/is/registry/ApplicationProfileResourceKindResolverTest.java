package eu.dnetlib.enabling.is.registry;

import static org.junit.Assert.*;

import javax.xml.xpath.XPathExpressionException;

import org.junit.Before;
import org.junit.Test;

public class ApplicationProfileResourceKindResolverTest {

	private ApplicationProfileResourceKindResolver resolver;

	@Before
	public void setUp() throws Exception {
		resolver = new ApplicationProfileResourceKindResolver();
	}

	@Test(expected = NoSuchPendingCategoryException.class)
	public void testPending() throws XPathExpressionException {
		resolver.getPendingKindForType("TestType");
	}

	@Test
	public void testPendingExisting() throws XPathExpressionException {
		assertEquals("PendingServiceResources", resolver.getPendingKindForType("IndexServiceResourceType"));
	}

}
