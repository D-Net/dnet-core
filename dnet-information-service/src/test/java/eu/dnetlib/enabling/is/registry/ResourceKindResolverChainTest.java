package eu.dnetlib.enabling.is.registry;

import static org.junit.Assert.*;

import javax.xml.xpath.XPathExpressionException;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Lists;

/**
 * Test if the kind resolver chain correctly fails back.
 * 
 * @author marko
 * 
 */
public class ResourceKindResolverChainTest {

	private ResourceKindResolverChain chain;

	@Before
	public void setUp() throws Exception {
		chain = new ResourceKindResolverChain();
		chain.setResolvers(Lists.newArrayList(new ApplicationProfileResourceKindResolver(), new NameBasedResourceKindResolver()));
	}

	@Test
	public void testChain() throws XPathExpressionException {
		assertEquals("ServiceResources", chain.getNormalKindForType("MyServiceResourceType"));
		assertEquals("PendingServiceResources", chain.getPendingKindForType("MyServiceResourceType"));
		
		assertEquals("RepositoryServiceResources", chain.getNormalKindForType("RepositoryServiceResourceType"));
		assertEquals("PendingRepositoryResources", chain.getPendingKindForType("RepositoryServiceResourceType"));
	}
}
