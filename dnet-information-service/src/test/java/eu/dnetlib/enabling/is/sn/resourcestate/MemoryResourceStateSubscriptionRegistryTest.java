package eu.dnetlib.enabling.is.sn.resourcestate;


/**
 * Perform ResourceStateSubscriptionRegistryTest tests on a memory backed resource state subscription dao instance.
 * 
 * @author marko
 *
 */
public class MemoryResourceStateSubscriptionRegistryTest extends AbstractResourceStateSubscriptionRegistryTest { // NOPMD


	/** 
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.is.sn.resourcestate.AbstractResourceStateSubscriptionRegistryTest#setUpDao()
	 */
	@Override
	protected void setUpDao() {
		getRegistry().setSubscriptionDao(new MemoryResourceStateSubscriptionDAOImpl());
	}

}
