package eu.dnetlib.enabling.is.sn;

import static org.junit.Assert.*; // NOPMD

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

/**
 * abstract subscription registry test.
 * 
 * @author marko
 * 
 */
public class AbstractSubscriptionRegistryTest {

	/**
	 * object under test.
	 */
	private transient AbstractSubscriptionRegistry registry;

	/**
	 * setup .
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() {
		registry = new AbstractSubscriptionRegistry() {

			@Override
			protected Collection<String> getAcceptedPrefixes() {
				return Arrays.asList(new String[] { "CREATE", "DELETE", "UPDATE", "DEEP/SUB" });
			}

		};

	}

	/**
	 * test prefixes.
	 */
	@Test
	public void testMatchPrefix() {
		final SubscriptionRequest sub = new SubscriptionRequest();

		sub.setTopicExpression("CREATE/MyResourceType");
		assertNotNull("check create", registry.matchPrefix(sub));

		sub.setTopicExpression("DELETE/MyResourceType");
		assertNotNull("check delete", registry.matchPrefix(sub));

		sub.setTopicExpression("UPDATE/MyResourceType");
		assertNotNull("check update", registry.matchPrefix(sub));

		sub.setTopicExpression("VERIFY/MyResourceType");
		assertNull("check non declared prefix", registry.matchPrefix(sub));

		sub.setTopicExpression("UPDATED/MyResourceType");
		assertNull("check non delimited prefix", registry.matchPrefix(sub));

		sub.setTopicExpression("UPDATE");
		assertNotNull("check only prefix", registry.matchPrefix(sub));

		sub.setTopicExpression("DEEP/MyResourceType");
		assertNull("check deep", registry.matchPrefix(sub));

		sub.setTopicExpression("DEEP/SUB/MyResourceType");
		assertNotNull("check deep sub", registry.matchPrefix(sub));
	}

}
