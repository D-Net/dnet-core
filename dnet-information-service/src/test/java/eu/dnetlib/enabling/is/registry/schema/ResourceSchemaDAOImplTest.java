package eu.dnetlib.enabling.is.registry.schema;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService;
import eu.dnetlib.enabling.tools.StreamOpaqueResource;

/**
 * test resource schema dao.
 * 
 * @author marko
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class ResourceSchemaDAOImplTest {

	/**
	 * instance under test.
	 */
	private transient ResourceSchemaDAOImpl dao;

	/**
	 * lookup service.
	 */
	@Mock
	private transient ISLookUpService lookupService;

	/**
	 * prepare common stuff.
	 * 
	 * @throws Exception
	 *             shouldn't happen
	 */
	@Before
	public void setUp() throws Exception {
		final StringWriter writer = new StringWriter();
		IOUtils.copy(getClass().getResourceAsStream("schema.xsd"), writer);

		when(lookupService.getResourceTypeSchema(anyString())).thenReturn(writer.getBuffer().toString());

		dao = new ResourceSchemaDAOImpl();
		dao.setIsLookup(lookupService);

	}

	/**
	 * test dao by validating a resource.
	 * 
	 * @throws Exception
	 *             shouldn't happen
	 */
	@Test
	public void testGetResourceSchema() throws Exception { // NOPMD
		final OpaqueResourceValidatorImpl validator = new OpaqueResourceValidatorImpl();
		validator.setSchemaDao(dao);

		validator.validate(new StreamOpaqueResource(getClass().getResourceAsStream("valid-profile.xml")));
	}

}
