package eu.dnetlib.enabling.is.lookup;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpDocumentNotFoundException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpException;
import eu.dnetlib.enabling.is.store.rmi.ISStoreException;
import eu.dnetlib.enabling.is.store.rmi.ISStoreService;
import eu.dnetlib.enabling.tools.XQueryUtilsImpl;

/**
 * ISLookUpService test.
 * 
 * @author marko
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class ISLookUpServiceImplTest {

	/**
	 * test profile id.
	 */
	private static final String PROF_ID = "first_c2Vjb25k";

	/**
	 * List of ids.
	 */
	private static final List<String> ID_LIST = Arrays.asList(new String[] { "fff", "ggg", "hhh" });

	/**
	 * class under test.
	 */
	private transient ISLookUpServiceImpl lookup;

	/**
	 * is store service mock.
	 */
	@Mock
	private transient ISStoreService storeService;

	/**
	 * common test case init.
	 */
	@Before
	public void setUp() {
		lookup = new ISLookUpServiceImpl();
		lookup.setXqueryUtils(new XQueryUtilsImpl());
		lookup.setIsStore(storeService);
	}

	/**
	 * test get file name from id.
	 */
	@Test
	public void testGetFileNameForId() {
		assertEquals("check", "first", lookup.getFileNameForId(PROF_ID));
	}

	/**
	 * test get file coll from id.
	 */
	@Test
	public void testGetFileCollForId() {
		assertEquals("check", "second", lookup.getFileCollForId(PROF_ID));
	}

	/**
	 * test get resource profile.
	 * 
	 * @throws ISLookUpException
	 *             shouldn't happen
	 * @throws ISStoreException
	 *             shouldn't happen
	 */
	@Test
	public void testGetResourceProfile() throws ISLookUpException, ISStoreException {
		when(storeService.getXML("first", "/db/DRIVER/second")).thenReturn("<someResult/>");

		assertNotNull("get profile", lookup.getResourceProfile(PROF_ID));
	}

	/**
	 * test get resource profile when no such document exists.
	 * 
	 * @throws ISLookUpException
	 *             expected
	 */
	@Test(expected = ISLookUpDocumentNotFoundException.class)
	public void testGetResourceProfileNonExisting() throws ISLookUpException {
		lookup.getResourceProfile(PROF_ID);
	}

	/**
	 * test getCollection.
	 * 
	 * @throws IOException
	 *             could happen
	 * @throws ISStoreException
	 *             could happen
	 * @throws ISLookUpException
	 *             could happen
	 * @throws DocumentException
	 *             could happen
	 */
	@Test
	public void testGetCollection() throws IOException, ISStoreException, ISLookUpException, DocumentException {
		final StringWriter writer = new StringWriter();
		IOUtils.copy(getClass().getResourceAsStream("collProfile.xml"), writer);
		final String simpleProf = writer.getBuffer().toString();

		when(storeService.getXML(anyString(), anyString())).thenReturn(simpleProf);
		when(
				storeService.quickSearchXML("for $x in collection('/db/DRIVER/CollectionDSResources') where $x//FATHER/@id = '" + PROF_ID
						+ "' return $x//RESOURCE_IDENTIFIER/@value/string()")).thenReturn(ID_LIST);

		final String profile = lookup.retrieveCollection(PROF_ID);
		final SAXReader reader = new SAXReader();
		final Document doc = reader.read(new StringReader(profile));

		assertEquals("children", ID_LIST.size(), doc.selectNodes("//CHILDREN/CHILD").size());
		for (final String id : ID_LIST) {
			assertNotNull("child", doc.selectSingleNode("//CHILDREN/CHILD[@id='" + id + "']"));
		}
	}

}
