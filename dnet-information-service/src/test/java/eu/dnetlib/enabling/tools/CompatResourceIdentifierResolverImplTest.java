package eu.dnetlib.enabling.tools;

import static org.junit.Assert.*; // NOPMD

import org.junit.Before;
import org.junit.Test;

/**
 * test the CompatResourceIdentifierResolverImpl.
 * 
 * @author marko
 *
 */
public class CompatResourceIdentifierResolverImplTest {

	/**
	 * test resource identifier.
	 * 
	 */
	private static final String TEST_RES_ID = "first_c2Vjb25k";
	
	/**
	 * instance under test.
	 */
	private transient CompatResourceIdentifierResolverImpl resIdResolver;
	
	/**
	 * prepare the instance under test.
	 */
	@Before
	public void setUp() {
		resIdResolver = new CompatResourceIdentifierResolverImpl();
	}

	/**
	 * test get collection name.
	 */
	@Test
	public void testGetCollectionName() {
		assertEquals("check collection name", "second", resIdResolver.getCollectionName(TEST_RES_ID));
	}

	/**
	 * test get file name.
	 */
	@Test
	public void testGetFileName() {
		assertEquals("check file name", "first", resIdResolver.getFileName(TEST_RES_ID));		
	}

}
