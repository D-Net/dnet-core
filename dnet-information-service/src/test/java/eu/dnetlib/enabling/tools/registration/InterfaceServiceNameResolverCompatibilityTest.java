package eu.dnetlib.enabling.tools.registration;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import eu.dnetlib.enabling.is.store.ISStoreServiceImpl;

/**
 * compatibility interface service name tester.
 * @author marko
 *
 */
public class InterfaceServiceNameResolverCompatibilityTest {

	/**
	 * instance under test.
	 */
	private transient InterfaceServiceNameResolverCompatibility resolver;
	
	/**
	 * setup. 
	 */
	@Before
	public void setUp() {
		resolver = new InterfaceServiceNameResolverCompatibility();
	
	}

	/**
	 * test mapping without override.
	 */
	@Test
	public void testGetName() {
		assertEquals("check without override", "ISStoreService", resolver.getName(new ISStoreServiceImpl()));
	}
	
	/**
	 * test mapping override.
	 */
	@Test
	public void testGetNameWithOverride() {
		final Map<String, String> mapping = new HashMap<String, String>();
		mapping.put("ISStoreService", "IS_StoreService");
		resolver.setMapping(mapping);	
		
		assertEquals("check is store service name", "IS_StoreService", resolver.getName(new ISStoreServiceImpl()));
	}
	
	

}
