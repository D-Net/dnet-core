package eu.dnetlib.enabling.tools.registration;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import eu.dnetlib.enabling.is.registry.ISRegistryServiceImpl;
import eu.dnetlib.enabling.is.registry.rmi.ISRegistryService;

/**
 * test heuristic service name generator.
 * @author marko
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class InterfaceServiceNameGeneratorTest {

	/**
	 * instance under test.
	 */
	private transient ServiceNameResolver generator = new InterfaceServiceNameResolver();

	/**
	 * simulates the extension of the service interface.
	 * 
	 * @author marko
	 *
	 */
	interface SomeDummyInterface extends ISRegistryService {
	}
	
	/**
	 * simulates the implementation of the service interface.
	 * @author marko
	 *
	 */
	abstract class AbstractDummyService implements SomeDummyInterface {
	}
	
	/**
	 * simulates the subclassing of the service implementation class.
	 * 
	 * @author marko
	 *
	 */
	abstract class AbstractDummyServiceImpl extends AbstractDummyService {
	}
	
	/**
	 * service mock.
	 */
	@Mock
	private transient AbstractDummyServiceImpl service;
	
	/**
	 * setup.
	 */
	@Before
	public void setUp() {
		generator = new InterfaceServiceNameResolver();
	}

	/**
	 * test getName().
	 */
	@Test
	public void testGetName() {
		assertEquals("check service name", "ISRegistryService", generator.getName(service));
	}

	/**
	 * test getName() on real classes.
	 */
	@Test
	public void testGetNameReal() {
		assertEquals("check service name", "ISRegistryService", generator.getName(new ISRegistryServiceImpl()));
	}

	
}
