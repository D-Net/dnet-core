package eu.dnetlib.enabling.tools.blackboard;


import static org.junit.Assert.assertEquals;

import javax.xml.bind.JAXBException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;

import eu.dnetlib.miscutils.jaxb.JaxbFactory;

/**
 * Jaxb Blackboard message implementation test.
 * 
 * @author marko
 *
 */
public class BlackboardMessageImplTest {

	/**
	 * logger.
	 */
	private static final Log log = LogFactory.getLog(BlackboardMessageImplTest.class); // NOPMD by marko on 11/24/08 5:02 PM

	/**
	 * test message (de)serialization.
	 * 
	 * @throws JAXBException could happen
	 */
	@Test
	public void testMessage() throws JAXBException {
		final JaxbFactory<BlackboardMessage> factory = new JaxbFactory<BlackboardMessage>(BlackboardMessageImpl.class);
		
		final BlackboardMessage message = factory.newInstance();
		message.setAction("FEED");
		message.setActionStatus(ActionStatus.ONGOING);
		message.setId("12");
		message.setDate("5123");
		
		final BlackboardParameter param1 = new BlackboardParameterImpl();
		param1.setName("someName");
		param1.setValue("someValue");
		message.getParameters().add(param1);
		
		final BlackboardParameter param2 = new BlackboardParameterImpl();
		param2.setName("otherName");
		param2.setValue("otherValue");
		message.getParameters().add(param2);

		final String serialized = factory.serialize(message);
		log.info(serialized);
		
		final BlackboardMessage res = factory.parse(serialized);
		log.info(res.getParameters());
		
		assertEquals("list serialization", 2, res.getParameters().size());
		assertEquals("enum serialization", ActionStatus.ONGOING, res.getActionStatus());
	}

}
