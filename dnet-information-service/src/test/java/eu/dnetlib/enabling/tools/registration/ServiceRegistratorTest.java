package eu.dnetlib.enabling.tools.registration;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;

import javax.xml.namespace.QName;

import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpDocumentNotFoundException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.endpoint.ServerImpl;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.service.Service;
import org.apache.cxf.service.model.EndpointInfo;

import org.junit.Before;
import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import eu.dnetlib.enabling.is.registry.rmi.ISRegistryException;
import eu.dnetlib.enabling.is.registry.rmi.ISRegistryService;
import eu.dnetlib.enabling.locators.UniqueServiceLocator;
import eu.dnetlib.enabling.tools.HNMLocator;
import eu.dnetlib.soap.cxf.CxfEndpointReferenceBuilder;
import eu.dnetlib.soap.cxf.JaxwsEndpointReferenceBuilder;

/**
 * ServiceRegistrator test.
 *
 * @author marko
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ServiceRegistratorTest {
	/**
	 * instance under test.
	 */
	private transient ServiceRegistrator registrator;

	/**
	 * service mock.
	 */
	@Mock
	private transient Service service;

	/**
	 * endpoint info mock.
	 */
	@Mock
	private transient EndpointInfo endpointInfo;

	/**
	 * endpoint mock.
	 */
	@Mock
	private transient Endpoint endpoint;

	/**
	 * registry service mock.
	 */
	@Mock
	private transient ISRegistryService registryService;

	/**
	 * isLookup service mock.
	 */
	@Mock
	private transient ISLookUpService isLookUpService;

	/**
	 * jaxws endpoint mock.
	 */
	@Mock
	private transient EndpointImpl jaxwsEndpoint;

	/**
	 * cxf server mock.
	 */
	@Mock
	private transient ServerImpl server;

	/**
	 * hnm locator mock.
	 */
	@Mock
	private transient HNMLocator hnmLocator;
	@Mock
	private UniqueServiceLocator serviceLocator;

	/**
	 * setup common stuff.
	 *
	 */
    @Before
	public void disabled() throws ISLookUpException {
	     final String service = "TestService";
	     when(this.service.getName()).thenReturn(new QName("http://my.test", service));

		when(endpoint.getEndpointInfo()).thenReturn(endpointInfo);
		when(endpoint.getService()).thenReturn(this.service);
		when(endpointInfo.getAddress()).thenReturn("http://localhost/something");
		when(endpointInfo.getName()).thenReturn(new QName("http://my.test", "TestServiceEndpoint"));

		when(jaxwsEndpoint.getServer()).thenReturn(server);
		when(server.getEndpoint()).thenReturn(endpoint);

		when(hnmLocator.getHNMForUrl("http://localhost/something")).thenReturn("555444");

	    // serviceLocator.getService(ISLookUpService.class).getResourceTypeSchema(serviceName);

	    when(isLookUpService.getResourceTypeSchema(service)).thenThrow(ISLookUpDocumentNotFoundException.class);

	    when(serviceLocator.getService(ISLookUpService.class)).thenReturn(isLookUpService);
		when(serviceLocator.getService(ISRegistryService.class, true)).thenReturn(registryService);
		
		
		registrator = new ServiceRegistrator();
		registrator.setServiceLocator(serviceLocator);

		final CxfEndpointReferenceBuilder cxfEprBuilder = new CxfEndpointReferenceBuilder();

		final JaxwsEndpointReferenceBuilder eprBuilder = new JaxwsEndpointReferenceBuilder();
		eprBuilder.setBuilder(cxfEprBuilder);

		registrator.setEprBuilder(eprBuilder);
		registrator.setHnmLocator(hnmLocator);
	}

	/**
	 * test register service.
	 * @throws ISRegistryException shouldn't happen
	 */
	@Test
    //@Ignore
	public void testRegisterService() throws ISRegistryException {
		when(registryService.insertProfileForValidation(eq("TestServiceResourceType"), anyString())).thenReturn("123");

		final String rsId = registrator.registerService("TestService", jaxwsEndpoint);
		assertEquals("registered", "123", rsId);
	}

}
