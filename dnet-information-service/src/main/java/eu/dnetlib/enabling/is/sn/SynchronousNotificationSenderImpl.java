package eu.dnetlib.enabling.is.sn;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This implementation of NotificationSender simply sends notification synchronously.
 * 
 * NOTE: this is only for testing. Real-wold cases should use AsynchrnonousNotificationSender, because we have to 
 * get out the thread serving eXist queries.
 * 
 * @author marko
 * 
 */
public class SynchronousNotificationSenderImpl extends AbstractNotificationSender {

	/**
	 * logger.
	 */
	private static final Log log = LogFactory.getLog(SynchronousNotificationSenderImpl.class); // NOPMD by marko on 11/24/08 5:02 PM

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.NotificationSender#send(javax.xml.ws.wsaddressing.W3CEndpointReference,
	 *      eu.dnetlib.enabling.is.sn.NotificationMessage)
	 */
	@Override
	public void send(final W3CEndpointReference destination, final NotificationMessage message) {
		log.info("synchronously sending message " + message.getSubscriptionId() + ", " + message.getTopic() + ", " + message.getResourceId() + ", "
				+ message.getBody());
		try {
			getInvoker().send(destination, message, 0);
		} catch (javax.xml.ws.soap.SOAPFaultException t) {
			log.fatal("error sending notification to " + destination.toString(), t);
		}
	}
}
