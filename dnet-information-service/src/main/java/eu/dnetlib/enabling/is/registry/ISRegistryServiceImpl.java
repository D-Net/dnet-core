package eu.dnetlib.enabling.is.registry; // NOPMD

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Required;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.sun.xml.messaging.saaj.util.Base64;

import eu.dnetlib.common.rmi.APIDeprecatedException;
import eu.dnetlib.common.rmi.UnimplementedException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpDocumentNotFoundException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService;
import eu.dnetlib.enabling.is.registry.rmi.ISRegistryException;
import eu.dnetlib.enabling.is.registry.rmi.ISRegistryService;
import eu.dnetlib.enabling.is.registry.schema.OpaqueResourceValidator;
import eu.dnetlib.enabling.is.registry.schema.ValidationException;
import eu.dnetlib.enabling.is.registry.validation.ProfileValidationStrategy;
import eu.dnetlib.enabling.is.registry.validation.RegistrationPhase;
import eu.dnetlib.enabling.is.store.rmi.ISStoreException;
import eu.dnetlib.enabling.is.store.rmi.ISStoreService;
import eu.dnetlib.enabling.tools.AbstractBaseService;
import eu.dnetlib.enabling.tools.CompatResourceIdentifierResolverImpl;
import eu.dnetlib.enabling.tools.OpaqueResource;
import eu.dnetlib.enabling.tools.ResourceIdentifierResolver;
import eu.dnetlib.enabling.tools.ResourceType;
import eu.dnetlib.enabling.tools.StringOpaqueResource;
import eu.dnetlib.enabling.tools.UniqueIdentifierGenerator;
import eu.dnetlib.enabling.tools.UniqueIdentifierGeneratorImpl;
import eu.dnetlib.enabling.tools.XQueryUtils;

/**
 * Registry service implementation.
 * 
 * @author marko
 * 
 */
@WebService(targetNamespace = "http://services.dnetlib.eu/")
public class ISRegistryServiceImpl extends AbstractBaseService implements ISRegistryService { // NOPMD by marko on

	/**
	 * logger.
	 */
	private static final Log log = LogFactory.getLog(ISRegistryServiceImpl.class); // NOPMD by marko on 11/24/08 5:02 PM

	/**
	 * error message: secure profile registration.
	 */
	private static final String ERROR_SEC_PROFILE = "cannot register secure profile";

	/**
	 * error message: trying to retrieve the current stored version of the resource profile.
	 */
	private static final String CANT_FETCH = "cannot fetch original profile";

	/**
	 * error message: cannot create a resource.
	 */
	private static final String CANT_CREATE = "cannot create resource";

	/**
	 * IS Store
	 */
	private ISStoreService isStore;

	/**
	 * IS Lookup
	 */
	private ISLookUpService isLookup;

	/**
	 * uuid generator.
	 */
	@Resource
	private UniqueIdentifierGenerator uuidGenerator = new UniqueIdentifierGeneratorImpl();

	/**
	 * manages the pending state of resource. Different implementations can be plugged in.
	 */
	private PendingResourceManager pendingManager = new CompatPendingResourceManagerImpl();

	/**
	 * manages resource identifier mappings with the abstract xmldb namespace (files/collections).
	 */
	@Resource
	private ResourceIdentifierResolver resIdResolver = new CompatResourceIdentifierResolverImpl();

	/**
	 * used to validate resources.
	 */
	private OpaqueResourceValidator resourceValidator;

	/**
	 * used to validate resources w.r.t. a set of defined properties.
	 */
	@Resource
	private ProfileValidationStrategy profileValidationStrategy;

	/**
	 * query utils. Used to obtain xmldb collection names and other things useful for interacting with the IS_Store.
	 */
	private XQueryUtils xqueryUtils;

	/**
	 * the blackboard management stuff is factored out here.
	 */
	@Resource
	private RegistryBlackboardManager blackboardManager;

	@Override
	public void start() {
		super.start();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#addBlackBoardMessage(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void addBlackBoardMessage(final String profId, final String messageId, final String message) throws ISRegistryException {
		blackboardManager.addMessage(profId, messageId, message);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#addOrUpdateResourceType(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean addOrUpdateResourceType(final String resourceType, final String resourceSchema) throws ISRegistryException {
		return addResourceType(resourceType, resourceSchema);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#addProfileNode(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean addProfileNode(final String profId, final String xpath, final String node) throws ISRegistryException {
		// TODO Auto-generated method stub
		throw new UnimplementedException();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#addResourceType(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean addResourceType(final String resourceType, final String resourceSchema) throws ISRegistryException {
		try {
			return isStore.insertXML(resourceType, xqueryUtils.getRootCollection() + ResourceType.RESOURCE_TYPES,
					resourceSchema);
		} catch (ISStoreException e) {
			throw new ISRegistryException("cannot add resource type", e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#deleteBlackBoardMessage(java.lang.String, java.lang.String)
	 */
	@Override
	public void deleteBlackBoardMessage(final String profId, final String messageId) throws ISRegistryException {
		blackboardManager.deleteMessage(profId, messageId);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#deleteProfile(java.lang.String)
	 */
	@Override
	public boolean deleteProfile(final String resId) throws ISRegistryException {
		try {
			final boolean res = isStore.deleteXML(resIdResolver.getFileName(resId),
					"/db/DRIVER/" + resIdResolver.getCollectionName(resId));
			if (!res) { throw new ISRegistryDocumentNotFoundException("document " + resId + " not found"); }

			return true;
		} catch (ISStoreException e) {
			throw new ISRegistryException("cannot delete profile " + resId, e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#deleteProfiles(java.util.List)
	 */
	@Override
	public boolean deleteProfiles(final List<String> arrayprofId) throws ISRegistryException {
		throw new APIDeprecatedException();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#deleteResourceType(java.lang.String, java.lang.Boolean)
	 */
	@Override
	public boolean deleteResourceType(final String resourceType, final Boolean hierarchical) throws ISRegistryException {
		try {
			return isStore.deleteXML(resourceType, xqueryUtils.getRootCollection() + ResourceType.RESOURCE_TYPES);
		} catch (ISStoreException e) {
			throw new ISRegistryException("error deleting resource type " + resourceType, e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#executeXUpdate(java.lang.String)
	 */
	@Override
	public boolean executeXUpdate(final String xquery) throws ISRegistryException {
		try {
			return isStore.executeXUpdate(xquery);
		} catch (ISStoreException e) {
			throw new ISRegistryException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#insertProfileForValidation(java.lang.String, java.lang.String)
	 */
	@Override
	public String insertProfileForValidation(final String resourceType, final String resourceProfile) throws ISRegistryException {
		try {
			final OpaqueResource resource = new StringOpaqueResource(resourceProfile);
			if (!resourceType.equals(resource.getResourceType())) { throw new ISRegistryException("expected resource type doesn't match to resource"); }

			pendingManager.setPending(resource, true);

			return registerProfile(resource.asString());
		} catch (XPathExpressionException e) {
			throw new ISRegistryException(e);
		} catch (SAXException e) {
			throw new ISRegistryException(e);
		} catch (IOException e) {
			throw new ISRegistryException(e);
		} catch (ParserConfigurationException e) {
			throw new ISRegistryException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#refreshProfile(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean refreshProfile(final String profId, final String resourceType) throws ISRegistryException {
		// TODO Auto-generated method stub
		throw new UnimplementedException();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#registerProfile(java.lang.String)
	 */
	@Override
	public String registerProfile(final String resourceProfile) throws ISRegistryException {
		log.debug("registering profile");

		try {
			final OpaqueResource resource = new StringOpaqueResource(resourceProfile);

			// TODO: factor this out (possibly not within the OpaqueResource class)
			final String coll = xqueryUtils.getCollectionPath(resource);
			final String fileName = uuidGenerator.generateIdentifier();
			final String newId = fileName + "_" + new String(Base64.encode(coll.getBytes()));
			resource.setResourceId(newId);

			resource.setModificationDate(new Date());

			// log.info("validating to xml schema: " + resource.asString());
			resourceValidator.validate(resource);

			profileValidationStrategy.accept(resource, RegistrationPhase.Register);

			// TODO: factor out ResourceType class
			isStore.insertXML(fileName, xqueryUtils.getRootCollection() + coll, resource.asString());

			return resource.getResourceId();
		} catch (XPathExpressionException e) {
			throw new ISRegistryException(e);
		} catch (SAXException e) {
			throw new ISRegistryException(e);
		} catch (IOException e) {
			throw new ISRegistryException(e);
		} catch (ParserConfigurationException e) {
			throw new ISRegistryException(e);
		} catch (ISStoreException e) {
			throw new ISRegistryException(e);
		} catch (ValidationException e) {
			throw new ISRegistryException("profile is not conforming to the schema: " + e.getMessage(), e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#registerSecureProfile(java.lang.String, java.lang.String)
	 */
	@Override
	public String registerSecureProfile(final String resourceProfId, final String secureProfId) throws ISRegistryException {
		try {
			synchronized (this) {
				final String secureProfSrc;
				try {
					secureProfSrc = isLookup.getResourceProfile(secureProfId); // NOPMD
				} catch (ISLookUpDocumentNotFoundException e) {
					throw new ISRegistryException("cannot register secure profile, the given secure profile doesn't exist", e);
				}
				final OpaqueResource secureProf = new StringOpaqueResource(secureProfSrc);

				final String profId = validateProfile(resourceProfId);

				final XPath xpath = XPathFactory.newInstance().newXPath();
				final Element idEl = (Element) xpath.evaluate("/RESOURCE_PROFILE/BODY/CONFIGURATION/resourceId", secureProf.asDom(), XPathConstants.NODE);
				idEl.setTextContent(profId);

				if (!updateProfile(secureProfId, secureProf.asString(), secureProf.getResourceType())) { throw new ISRegistryException(
						"cannot update security profile (updateProfile returned false)"); }

				return profId;
			}
		} catch (XPathExpressionException e) {
			throw new ISRegistryException(ERROR_SEC_PROFILE, e);
		} catch (SAXException e) {
			throw new ISRegistryException(ERROR_SEC_PROFILE, e);
		} catch (IOException e) {
			throw new ISRegistryException(ERROR_SEC_PROFILE, e);
		} catch (ParserConfigurationException e) {
			throw new ISRegistryException(ERROR_SEC_PROFILE, e);
		} catch (ISLookUpException e) {
			throw new ISRegistryException("cannot register secure profile, problem fetching the given secure profile", e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#removeProfileNode(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean removeProfileNode(final String profId, final String nodeId) throws ISRegistryException {
		// TODO Auto-generated method stub
		throw new UnimplementedException();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#replyBlackBoardMessage(java.lang.String, java.lang.String)
	 */
	@Override
	public void replyBlackBoardMessage(final String profId, final String message) throws ISRegistryException {
		blackboardManager.replyMessage(profId, message);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#updateProfile(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean updateProfile(final String resId, final String resourceProfile, final String resourceType) throws ISRegistryException {
		try {
			final String fileName = resIdResolver.getFileName(resId);
			final String fileColl = "/db/DRIVER/" + resIdResolver.getCollectionName(resId);

			final String oldProfileSrc = isStore.getXML(fileName, fileColl);

			if (oldProfileSrc == null) { throw new ISRegistryException("cannot update a non existing profile " + resId); }

			final OpaqueResource oldResource = new StringOpaqueResource(oldProfileSrc);

			final StringOpaqueResource newResource = new StringOpaqueResource(resourceProfile);
			newResource.setResourceId(oldResource.getResourceId());
			newResource.setResourceKind(oldResource.getResourceKind());
			newResource.setResourceType(oldResource.getResourceType());
			newResource.setModificationDate(new Date());

			getResourceValidator().validate(newResource);

			return isStore.updateXML(fileName, fileColl, newResource.asString());
		} catch (ISStoreException e) {
			throw new ISRegistryException(e);
		} catch (XPathExpressionException e) {
			throw new ISRegistryException(e);
		} catch (ValidationException e) {
			throw new ISRegistryException("profile not conforming to xml schema", e);
		} catch (SAXException e) {
			throw new ISRegistryException(e);
		} catch (IOException e) {
			throw new ISRegistryException(e);
		} catch (ParserConfigurationException e) {
			throw new ISRegistryException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#updateProfileDHN(java.lang.String)
	 */
	@Override
	public String updateProfileDHN(final String resourceProfile) throws ISRegistryException {
		// TODO Auto-generated method stub
		throw new UnimplementedException();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#updateProfileNode(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean updateProfileNode(final String profId, final String xpath, final String node) throws ISRegistryException {
		return executeXUpdate("for $x in collection('/db/DRIVER')//RESOURCE_PROFILE[.//RESOURCE_IDENTIFIER/@value eq '" + profId + "']" + xpath
				+ " return update replace $x with " + node);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#updateRegionDescription(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean updateRegionDescription(final String profId, final String resourceProfile) throws ISRegistryException {
		// TODO Auto-generated method stub
		throw new UnimplementedException();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#validateProfile(java.lang.String)
	 */
	@Override
	public String validateProfile(final String profId) throws ISRegistryException {
		try {
			final String resourceProfile = isLookup.getResourceProfile(profId);
			final OpaqueResource resource = new StringOpaqueResource(resourceProfile);
			pendingManager.setValid(resource);

			return resource.getResourceId();
		} catch (ISLookUpException e) {
			throw new ISRegistryException(CANT_FETCH, e);
		} catch (XPathExpressionException e) {
			throw new ISRegistryException(CANT_CREATE, e);
		} catch (SAXException e) {
			throw new ISRegistryException(CANT_CREATE, e);
		} catch (IOException e) {
			throw new ISRegistryException(CANT_CREATE, e);
		} catch (ParserConfigurationException e) {
			throw new ISRegistryException(CANT_CREATE, e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#invalidateProfile(java.lang.String)
	 */
	@Override
	public String invalidateProfile(final String profId) throws ISRegistryException {
		try {
			final String resourceProfile = isLookup.getResourceProfile(profId);
			final OpaqueResource resource = new StringOpaqueResource(resourceProfile);
			pendingManager.setPending(resource);

			return resource.getResourceId();
		} catch (ISLookUpException e) {
			throw new ISRegistryException(CANT_FETCH, e);
		} catch (XPathExpressionException e) {
			throw new ISRegistryException(CANT_CREATE, e);
		} catch (SAXException e) {
			throw new ISRegistryException(CANT_CREATE, e);
		} catch (IOException e) {
			throw new ISRegistryException(CANT_CREATE, e);
		} catch (ParserConfigurationException e) {
			throw new ISRegistryException(CANT_CREATE, e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.rmi.ISRegistryService#validateProfiles(java.util.List)
	 */
	@Override
	public List<String> validateProfiles(final List<String> profIds) throws ISRegistryException {
		throw new APIDeprecatedException();
	}

	public UniqueIdentifierGenerator getUuidGenerator() {
		return uuidGenerator;
	}

	public void setUuidGenerator(final UniqueIdentifierGenerator uuidGenerator) {
		this.uuidGenerator = uuidGenerator;
	}

	public PendingResourceManager getPendingManager() {
		return pendingManager;
	}

	public void setPendingManager(final PendingResourceManager pendingManager) {
		this.pendingManager = pendingManager;
	}

	public ResourceIdentifierResolver getResIdResolver() {
		return resIdResolver;
	}

	public void setResIdResolver(final ResourceIdentifierResolver resIdResolver) {
		this.resIdResolver = resIdResolver;
	}

	public OpaqueResourceValidator getResourceValidator() {
		return resourceValidator;
	}

	@Required
	public void setResourceValidator(final OpaqueResourceValidator resourceValidator) {
		this.resourceValidator = resourceValidator;
	}

	@Required
	public void setXqueryUtils(final XQueryUtils xqueryUtils) {
		this.xqueryUtils = xqueryUtils;
	}

	public XQueryUtils getXqueryUtils() {
		return xqueryUtils;
	}

	public RegistryBlackboardManager getBlackboardManager() {
		return blackboardManager;
	}

	public void setBlackboardManager(final RegistryBlackboardManager blackboardManager) {
		this.blackboardManager = blackboardManager;
	}

	public ProfileValidationStrategy getProfileValidationStrategy() {
		return profileValidationStrategy;
	}

	public void setProfileValidationStrategy(final ProfileValidationStrategy profileValidationStrategy) {
		this.profileValidationStrategy = profileValidationStrategy;
	}

	public ISStoreService getIsStore() {
		return isStore;
	}

	@Required
	public void setIsStore(final ISStoreService isStore) {
		this.isStore = isStore;
	}

	public ISLookUpService getIsLookup() {
		return isLookup;
	}

	@Required
	public void setIsLookup(final ISLookUpService isLookup) {
		this.isLookup = isLookup;
	}

}
