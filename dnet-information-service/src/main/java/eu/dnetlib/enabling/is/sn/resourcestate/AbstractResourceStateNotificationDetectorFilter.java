package eu.dnetlib.enabling.is.sn.resourcestate;

import eu.dnetlib.enabling.is.sn.NotificationSender;

/**
 * sits on top of a ResourceStateNotificationDetector and filters out resources for which we don't want to generate
 * notifications.
 * 
 * @author marko
 * 
 * @param <T>
 *            type of the resource object
 */
public abstract class AbstractResourceStateNotificationDetectorFilter<T> implements ResourceStateNotificationDetector<T> {

	/**
	 * delegate.
	 */
	private ResourceStateNotificationDetector<T> delegate;

	/**
	 * implement this in order to decide which resources are interesting for notifications, and which are not (for
	 * example pending resources are by ignored in dnet 1.0).
	 * 
	 * @param resource
	 *            resource
	 * @return true if the resource is potentially accepted for notification
	 */
	abstract boolean accept(T resource);

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.resourcestate.ResourceStateNotificationDetector#resourceCreated(java.lang.Object)
	 */
	@Override
	public void resourceCreated(final T newResource) {
		if (accept(newResource))
			delegate.resourceCreated(newResource);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.resourcestate.ResourceStateNotificationDetector#resourceDeleted(java.lang.Object)
	 */
	@Override
	public void resourceDeleted(final T oldResource) {
		if (accept(oldResource))
			delegate.resourceDeleted(oldResource);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.resourcestate.ResourceStateNotificationDetector#resourceUpdated(java.lang.Object,
	 *      java.lang.Object)
	 */
	@Override
	public void resourceUpdated(final T oldResource, final T newResource) {
		if (accept(oldResource) || accept(newResource))
			delegate.resourceUpdated(oldResource, newResource);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.NotificationDetector#setSender(eu.dnetlib.enabling.is.sn.NotificationSender)
	 */
	@Override
	public void setSender(final NotificationSender sender) {
		delegate.setSender(sender);
	}

	public ResourceStateNotificationDetector<T> getDelegate() {
		return delegate;
	}

	public void setDelegate(final ResourceStateNotificationDetector<T> delegate) {
		this.delegate = delegate;
	}

}
