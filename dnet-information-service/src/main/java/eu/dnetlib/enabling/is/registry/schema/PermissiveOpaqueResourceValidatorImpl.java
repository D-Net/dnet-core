package eu.dnetlib.enabling.is.registry.schema;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.dnetlib.enabling.tools.OpaqueResource;

/**
 * Unlike the OpaqueResourceValidatorImpl, this implementation only logs an error but accepts every resource as valid.
 * 
 * Use only during testing/development!
 * 
 * @author marko
 * 
 */
public class PermissiveOpaqueResourceValidatorImpl extends OpaqueResourceValidatorImpl {
	/**
	 * logger.
	 */
	private static final Log log = LogFactory.getLog(PermissiveOpaqueResourceValidatorImpl.class); // NOPMD by marko on 11/24/08 5:02 PM

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.schema.OpaqueResourceValidatorImpl#validate(eu.dnetlib.enabling.tools.OpaqueResource)
	 */
	@Override
	public void validate(final OpaqueResource resource) throws ValidationException {
		try {
			super.validate(resource);
		} catch (ValidationException e) {
			log.fatal("validation error, continuing (permissive mode)", e);
		}
	}

}
