package eu.dnetlib.enabling.is.registry.validation;

import org.springframework.beans.factory.annotation.Required;

import eu.dnetlib.enabling.tools.OpaqueResource;

public abstract class TypeAwareProfileValidator extends AbstractProfileValidator {
	
	private String resourceType;

	@Override
	public boolean handle(OpaqueResource resource, RegistrationPhase phase) {
		return super.handle(resource, phase) && getResourceType().equals(resource.getResourceType());
	}
	
	@Required
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getResourceType() {
		return resourceType;
	}

}
