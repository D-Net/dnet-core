package eu.dnetlib.enabling.is.sn;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DnetPathVerifier {

	/**
	 * logger.
	 */
	private static final Log log = LogFactory.getLog(DnetPathVerifier.class); // NOPMD by marko on 11/24/08 5:02 PM
	
	public DnetPathVerifier(final String dnetPath) {
		File f = new File(dnetPath);
		if (f.exists() == false) {
			log.info("path '" + dnetPath + "' not found, creating...");
			f.mkdirs();
		}
	}

}
