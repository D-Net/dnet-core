package eu.dnetlib.enabling.is.sn;

/**
 * This class encapsulates the notification message sent to ISSN subscribers.
 * 
 * @author marko
 * 
 */
public class NotificationMessage {

	/**
	 * subscription identifier.
	 */
	private String subscriptionId;

	/**
	 * subscription topic that triggered the notification.
	 */
	private String topic;

	/**
	 * resource identifier that triggered the notification.
	 */
	private String resourceId;

	/**
	 * message body.
	 */
	private String body;

	/**
	 * default constructor.
	 */
	public NotificationMessage() {
		// no operation
	}
	
	/**
	 * constructor with full parameters.
	 * 
	 * @param subscriptionId subscription identifier
	 * @param topic produced topic
	 * @param resourceId resource identifier of the resource which triggered the notification, or null
	 * @param body body of the document which triggered the notification, or null
	 */
	public NotificationMessage(final String subscriptionId, final String topic, final String resourceId, final String body) {
		super();
		this.subscriptionId = subscriptionId;
		this.topic = topic;
		this.resourceId = resourceId;
		this.body = body;
	}

	public String getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(final String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(final String topic) {
		this.topic = topic;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(final String resourceId) {
		this.resourceId = resourceId;
	}

	public String getBody() {
		return body;
	}

	public void setBody(final String body) {
		this.body = body;
	}
}
