package eu.dnetlib.enabling.is.registry.validation;

/**
 * Resources registration phases.
 * 
 * @author claudio
 *
 */
public enum RegistrationPhase {
	Register,
	Validate // as in 'validateProfile', not this 'validation'.
}