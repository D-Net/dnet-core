package eu.dnetlib.enabling.is.registry;

import java.util.List;

import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Required;

/**
 * Returns the first resource resolver
 * 
 * @author marko
 * 
 */
public class ResourceKindResolverChain implements ResourceKindResolver {

	private static final Log log = LogFactory.getLog(ResourceKindResolverChain.class); // NOPMD by marko on 11/24/08 5:02 PM

	/**
	 * resolvers.
	 */
	private List<ResourceKindResolver> resolvers;

	@Override
	public String getNormalKindForType(final String resourceType) throws XPathExpressionException {
		for (final ResourceKindResolver resolver : resolvers) {
			final String res = resolver.getNormalKindForType(resourceType);
			if (res != null)
				return res;
		}
		return null;

	}

	@Override
	public String getPendingKindForType(final String resourceType) throws XPathExpressionException {
		for (final ResourceKindResolver resolver : resolvers) {
			try {
				final String res = resolver.getPendingKindForType(resourceType);
				if (res != null)
					return res;
			} catch (NoSuchPendingCategoryException e) {
				log.debug("resolver didn't find pending category", e);
				continue;
			}
		}
		log.debug("cannot find pending category because no resolver found a pending category");
		throw new NoSuchPendingCategoryException("no pending category for " + resourceType);
	}

	public List<ResourceKindResolver> getResolvers() {
		return resolvers;
	}

	@Required
	public void setResolvers(final List<ResourceKindResolver> resolvers) {
		this.resolvers = resolvers;
	}

}
