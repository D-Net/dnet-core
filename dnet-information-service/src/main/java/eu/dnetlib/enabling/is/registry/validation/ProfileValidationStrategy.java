package eu.dnetlib.enabling.is.registry.validation;

import java.util.List;

import javax.annotation.Resource;

import eu.dnetlib.enabling.is.registry.schema.ValidationException;
import eu.dnetlib.enabling.tools.OpaqueResource;

/**
 * ProfileValidationStrategy iterates all the profile validators,
 * once a suitable validator is found, it's called to accept or 
 * reject the given resource.
 * 
 * @author claudio
 *
 */
public class ProfileValidationStrategy {
	
	@Resource
	private List<ProfileValidator> profileValidators;

	public boolean accept(OpaqueResource resource, RegistrationPhase phase) throws ValidationException {
		for(ProfileValidator validator : profileValidators) {
			if (validator.handle(resource, phase)) {
				if (validator.accept(resource))
					return true; 
				else throw new ValidationException("Cannot validate the given resource: " + resource.asString());				
			}
		}
		// if no validator handles the resource 
		return true;
	}
}
