package eu.dnetlib.enabling.is.registry.schema;

import javax.xml.validation.Schema;

/**
 * data access object for managing resource schemas.
 * 
 * @author marko
 *
 */
public interface ResourceSchemaDAO {

	/**
	 * obtain a resource schema instance for a given resource type.
	 * 
	 * @param resourceType resource type
	 * @return schema instance
	 */
	Schema getResourceSchema(String resourceType);

}
