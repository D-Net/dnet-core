package eu.dnetlib.enabling.is.registry;

public class NoSuchPendingCategoryException extends IllegalStateException {

	public NoSuchPendingCategoryException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 6934748076388947172L;

}
