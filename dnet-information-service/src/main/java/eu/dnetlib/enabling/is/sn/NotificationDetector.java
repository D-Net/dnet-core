package eu.dnetlib.enabling.is.sn;


/**
 * Implementors of this interface are able to detect events which will trigger notifications.
 * @author marko
 *
 */
public interface NotificationDetector {

	/**
	 * injection point for NotificationSender.
	 * 
	 * @param sender sender
	 */
	void setSender(NotificationSender sender);
}
