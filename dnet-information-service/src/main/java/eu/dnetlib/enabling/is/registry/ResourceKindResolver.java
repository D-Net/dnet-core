package eu.dnetlib.enabling.is.registry;

import javax.xml.xpath.XPathExpressionException;

public interface ResourceKindResolver {
	String getNormalKindForType(final String resourceType) throws XPathExpressionException;
	String getPendingKindForType(final String resourceType) throws XPathExpressionException;
}
