package eu.dnetlib.enabling.is.sn;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.dnetlib.enabling.is.sn.rmi.ISSNException;

/**
 * As a complementary approach w.r.t unit tests, this class is an extension of 
 * the default ISSN Service which creates a number of initial subscriptions so that we can play
 * with some notifications.
 * 
 * This class plays well with TestISStoreServiceImpl, which creates some profiles at startup time
 * and schedules some automated mutations.
 * 
 * @author marko
 *
 */
public class TestISSNServiceImpl extends ISSNServiceImpl {
	/**
	 * logger. 
	 */
	private static final Log log = LogFactory.getLog(TestISSNServiceImpl.class); // NOPMD by marko on 11/24/08 5:02 PM

	/** 
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.tools.AbstractBaseService#start()
	 */
	@Override
	public void start() {
		super.start();
		
		try {
			subscribe(getEprBuilder().getEndpointReference(getEndpoint()), "CREATE/TestResourceType/*", 0);
		} catch (ISSNException e) {
			log.fatal("cannot subscribe test subscriptions", e);
		}
	}

}
