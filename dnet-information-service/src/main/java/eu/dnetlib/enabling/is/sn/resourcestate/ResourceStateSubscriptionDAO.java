package eu.dnetlib.enabling.is.sn.resourcestate;

import java.util.Collection;

import eu.dnetlib.enabling.is.sn.SubscriptionDAO;

/**
 * Stores ResourceState subscriptions.
 * 
 * @author marko
 *
 */
public interface ResourceStateSubscriptionDAO extends SubscriptionDAO<ResourceStateSubscription> {
	
	/**
	 * get all subscriptions matching a given prefix and a given resource type.
	 * 
	 * @param prefix null means any prefix
	 * @param resourceType resource type.
	 * @param resourceId resource identifier
	 * @return matching subscriptions
	 */
	Collection<ResourceStateSubscription> listSubscriptions(String prefix, String resourceType, String resourceId);
}
