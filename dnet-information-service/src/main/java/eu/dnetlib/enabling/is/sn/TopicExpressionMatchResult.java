package eu.dnetlib.enabling.is.sn;

/**
 * Groups a topic expression prefix match result.
 * 
 * <p>
 * Utility class.
 * </p>
 * 
 * @author marko
 * 
 */
public class TopicExpressionMatchResult {
	/**
	 * Topic expression prefix.
	 */
	private final transient String prefix;

	/**
	 * RestOf the topic expression.
	 */
	private final transient String rest;

	/**
	 * construct a new topic expression prefix match result.
	 * @param prefix
	 *            prefix
	 * @param rest
	 *            rest
	 */
	public TopicExpressionMatchResult(final String prefix, final String rest) {
		super();
		this.prefix = prefix;
		this.rest = rest;
	}

	public String getPrefix() {
		return prefix;
	}

	public String getRest() {
		return rest;
	}


}
