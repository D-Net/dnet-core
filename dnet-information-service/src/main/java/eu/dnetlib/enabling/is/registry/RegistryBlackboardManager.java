package eu.dnetlib.enabling.is.registry;

/**
 * Internally used by the Registry Service to perform modifications on the service blackboard, a funcionality provided
 * by the registry API.
 * 
 * @author marko
 * 
 */
public interface RegistryBlackboardManager {
	/**
	 * add a new blackboard message.
	 * 
	 * @param profId
	 *            service profile id
	 * @param messageId
	 *            message identifier, chosen by the client
	 * @param message
	 *            message body
	 */
	void addMessage(String profId, String messageId, String message);

	/**
	 * Reply to a given message replacing the current message with the parameter. The message ID is inferred from the
	 * new message body.
	 * 
	 * @param profId
	 *            service profile id
	 * @param message
	 *            message body
	 */
	void replyMessage(String profId, String message);

	/**
	 * delete a message for a given message id.
	 * 
	 * @param profId
	 *            service profile id
	 * @param messageId
	 *            message id
	 */
	void deleteMessage(String profId, String messageId);
}
