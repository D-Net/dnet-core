package eu.dnetlib.enabling.is.sn;

import java.util.Collection;
import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

/**
 * In memory implementation of a notification invocation logger.
 *
 * @author marko
 *
 */
public class MemoryNotificationInvocationLogger implements NotificationInvocationLogger {

	/**
	 * default log size.
	 */
	private static final int DEFAULT_SIZE = 100;

	/**
	 * Entry.
	 *
	 * @author marko
	 *
	 */
	public class MemoryEntry implements Entry {

		/**
		 * logged message.
		 */
		private NotificationMessage message;

		/**
		 * message status.
		 */
		private Status status = Status.Queued;

		/**
		 * saved exception in case of Failed state.
		 */
		private Throwable exception;

		/**
		 * destination.
		 */
		private W3CEndpointReference destinationEpr;

		/**
		 * start date time.
		 */
		private Date start;

		/**
		 * finish date time.
		 */
		private Date finish;

		/**
		 * construct a new message.
		 *
		 * @param endpointReference
		 *            destination
		 *
		 * @param message
		 *            message
		 */
		public MemoryEntry(final W3CEndpointReference endpointReference, final NotificationMessage message) {
			this.destinationEpr = endpointReference;
			this.message = message;
			this.start = new Date();
		}

		/**
		 * {@inheritDoc}
		 *
		 * @see eu.dnetlib.enabling.is.sn.NotificationInvocationLogger.Entry#ongoing()
		 */
		@Override
		public void ongoing() {
			status = Status.Ongoing;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @see eu.dnetlib.enabling.is.sn.NotificationInvocationLogger.Entry#failure(java.lang.Throwable)
		 */
		@Override
		public void failure(final Throwable exc) {
			status = Status.Failed;
			this.exception = exc;
			commit();
		}

		/**
		 * {@inheritDoc}
		 *
		 * @see eu.dnetlib.enabling.is.sn.NotificationInvocationLogger.Entry#success()
		 */
		@Override
		public void success() {
			status = Status.Succeeded;
			commit();
		}

		/**
		 * finalize log entry.
		 */
		protected void commit() {
			this.finish = new Date();
		}

		@Override
		public NotificationMessage getMessage() {
			return message;
		}

		public void setMessage(final NotificationMessage message) {
			this.message = message;
		}

		@Override
		public Throwable getException() {
			return exception;
		}

		public void setException(final Throwable exception) {
			this.exception = exception;
		}

		@Override
		public Status getStatus() {
			return status;
		}

		public void setStatus(final Status status) {
			this.status = status;
		}

		public W3CEndpointReference getDestinationEpr() {
			return destinationEpr;
		}

		public void setDestinationEpr(final W3CEndpointReference destinationEpr) {
			this.destinationEpr = destinationEpr;
		}

		@Override
		public String getDestination() {
			return EPRUtil.getAddress(destinationEpr);
		}

		/**
		 * {@inheritDoc}
		 *
		 * @see eu.dnetlib.enabling.is.sn.NotificationInvocationLogger.Entry#getErrorMessage()
		 */
		@Override
		public String getErrorMessage() {
			if (getException() == null)
				return "";
			return getException().getMessage();
		}

		@Override
		public Date getStart() {
			return start;
		}

		public void setStart(final Date start) {
			this.start = start;
		}

		@Override
		public Date getFinish() {
			return finish;
		}

		public void setFinish(final Date finish) {
			this.finish = finish;
		}

		/**
		 * {@inheritDoc}
		 * @see eu.dnetlib.enabling.is.sn.NotificationInvocationLogger.Entry#getDuration()
		 */
		@Override
		public String getDuration() {
			if (finish == null || start == null)
				return "-";
			return (finish.getTime() - start.getTime()) + " ms";
		}
	}

	/**
	 * the actual log storage.
	 */
	private ConcurrentLinkedQueue<Entry> queue = new ConcurrentLinkedQueue<Entry>();

	/**
	 * max log size.
	 */
	private int size = DEFAULT_SIZE;

	/**
	 * {@inheritDoc}
	 *
	 * @see eu.dnetlib.enabling.is.sn.NotificationInvocationLogger#startLogging(eu.dnetlib.enabling.is.sn.NotificationMessage)
	 */
	@Override
	public Entry startLogging(final W3CEndpointReference dest, final NotificationMessage message) {
		final Entry entry = createLoggingEntry(dest, message);
		queue.add(entry);
		ensureQueueLength();
		return entry;
	}

	/**
	 * ensure that the queue doesn't grow beyond size.
	 */
	private void ensureQueueLength() {
		if (queue.size() > size)
			queue.poll();
	}

	/**
	 * create a new memory log object.
	 *
	 * @param dest
	 *            destination
	 *
	 * @param message
	 *            message to be logged
	 * @return new entry instance
	 */
	protected Entry createLoggingEntry(final W3CEndpointReference dest, final NotificationMessage message) {
		return new MemoryEntry(dest, message);
	}

	public ConcurrentLinkedQueue<Entry> getQueue() {
		return queue;
	}

	public void setQueue(final ConcurrentLinkedQueue<Entry> queue) {
		this.queue = queue;
	}

	public int getSize() {
		return size;
	}

	public void setSize(final int size) {
		this.size = size;
	}

	@Override
	public Collection<Entry> getEntries() {
		return getQueue();
	}

}
