package eu.dnetlib.enabling.is.sn;

import java.util.Collection;
import java.util.Date;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

/**
 * A notification logger maintains a log of sent notifications, and their status (succes or failure with exception etc).
 *
 * @author marko
 *
 */
public interface NotificationInvocationLogger {

	/**
	 * A log entry. Used to create log entries in two phases. First a log entry is created with "startLogging" and after
	 * the notification is sent the log entry is committed with success() of failure().
	 *
	 * @author marko
	 *
	 */
	interface Entry {
		/**
		 * log entry status.
		 *
		 * @author marko
		 *
		 */
		enum Status {
			/**
			 * notification is queued but not started to be sent.
			 */
			Queued,
			/**
			 * notification is ongoing.
			 */
			Ongoing,
			/**
			 * notification succeeded.
			 */
			Succeeded,
			/**
			 * notification failed.
			 */
			Failed
		};

		/**
		 * sets the status to ongoing.
		 */
		void ongoing();

		/**
		 * commits the log entry tagging it as succeeded.
		 */
		void success();

		/**
		 * commits the log entry tagging it as failed.
		 *
		 * @param exception
		 *            throwable, cause of failure
		 */
		void failure(Throwable exception);

		/**
		 * get start date time.
		 *
		 * @return date
		 */
		Date getStart();

		/**
		 * get finish date time.
		 *
		 * @return date
		 */
		Date getFinish();

		/**
		 * get log status.
		 *
		 * @return log status
		 */
		Status getStatus();

		/**
		 * get log message.
		 *
		 * @return log message
		 */
		NotificationMessage getMessage();

		/**
		 * get destination.
		 *
		 * @return destination url
		 */
		String getDestination();

		/**
		 * get exception.
		 *
		 * @return exception
		 */
		Throwable getException();

		/**
		 * Get exception error message or empty string if succeded.
		 *
		 * @return error message
		 */
		String getErrorMessage();

		/**
		 * Human readable duration.
		 *
		 * @return human readable duration
		 */
		String getDuration();
	}

	/**
	 * Start logging a notification invocation.
	 *
	 * @param destination
	 *            destination
	 * @param message
	 *            message to be logged.
	 * @return an entry object which has to be committed.
	 *
	 */
	Entry startLogging(W3CEndpointReference destination, NotificationMessage message);

	/**
	 * get all entries.
	 *
	 * @return all entries
	 */
	Collection<Entry> getEntries();
}
