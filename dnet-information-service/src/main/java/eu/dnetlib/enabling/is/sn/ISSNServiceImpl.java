package eu.dnetlib.enabling.is.sn;

import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.ws.Endpoint;
import javax.xml.ws.wsaddressing.W3CEndpointReference;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.dnetlib.enabling.is.sn.rmi.ISSNException;
import eu.dnetlib.enabling.is.sn.rmi.ISSNService;
import eu.dnetlib.enabling.is.sn.rmi.SubscriptionRequestRejectedException;
import eu.dnetlib.enabling.tools.AbstractBaseService;
import eu.dnetlib.soap.EndpointReferenceBuilder;

/**
 * implementation of ISSNService.
 * 
 * @author marko
 * 
 */
public class ISSNServiceImpl extends AbstractBaseService implements ISSNService {

	/**
	 * logger.
	 */
	private static final Log log = LogFactory.getLog(ISSNServiceImpl.class); // NOPMD by marko on 11/24/08 5:02 PM

	/**
	 * obvious constant.
	 */
	private static final int MILLIS_IN_SECOND = 1000;

	/**
	 * service endpoint.
	 */
	private Endpoint endpoint;

	/**
	 * core business logic.
	 */
	private ISSNServiceCore core;

	/**
	 * injected EPR builder.
	 */
	@Resource(name = "jaxwsEndpointReferenceBuilder")
	private EndpointReferenceBuilder<Endpoint> eprBuilder;

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.rmi.ISSNService#actionCreatePerformed(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean actionCreatePerformed(final String resourceType, final String profileId, final String profile) throws ISSNException {
		log.fatal("actionCreatePerformed(String, String, String) not implemented");
		return false;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.rmi.ISSNService#actionDeletePerformed(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean actionDeletePerformed(final String resourceType, final String profileId) throws ISSNException {
		log.fatal("actionDeletePerformed(String, String) not implemented");
		return false;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.rmi.ISSNService#actionUpdatePerformed(java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	public boolean actionUpdatePerformed(final String resourceType, final String profileId, final String profileBefore, final String profileAfter)
			throws ISSNException {
		log.fatal("actionUpdatePerformed(String, String, String, String) not implemented");
		return false;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.rmi.ISSNService#getCurrentMessage(java.lang.String)
	 */
	@Override
	public String getCurrentMessage(final String topic) throws ISSNException {
		log.fatal("getCurrentMessage(String) not implemented");
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.rmi.ISSNService#listSubscriptions()
	 */
	@Override
	public List<String> listSubscriptions() {
		log.fatal("listSubscriptions() not implemented");
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.rmi.ISSNService#pauseSubscription(java.lang.String)
	 */
	@Override
	public boolean pauseSubscription(final String subscrId) throws ISSNException {
		log.fatal("pauseSubscription(String) not implemented");
		return false;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.rmi.ISSNService#renew(java.lang.String, int)
	 */
	@Override
	public boolean renew(final String subscrId, final int terminationTime) throws ISSNException {
		log.fatal("renew(String, int) not implemented");
		return false;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.rmi.ISSNService#resumeSubscription(java.lang.String)
	 */
	@Override
	public boolean resumeSubscription(final String subscrId) throws ISSNException {
		log.fatal("resumeSubscription(String) not implemented");
		return false;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.rmi.ISSNService#subscribe(javax.xml.ws.wsaddressing.W3CEndpointReference, java.lang.String, int)
	 */
	@Override
	public String subscribe(final W3CEndpointReference consumerReference, final String topicExpression, final int termTime) throws ISSNException, SubscriptionRequestRejectedException {

		log.debug("subscribe request from: " + consumerReference.toString() + " topic: " + topicExpression);

		int ttl;
		if (termTime == 0) {
			ttl = 0;
		} else {
			ttl = (int) (termTime - (Calendar.getInstance().getTimeInMillis() / MILLIS_IN_SECOND));
		}

		final String res = core.subscribe(consumerReference, topicExpression, ttl);
		if (res == null) throw new ISSNException("couldn't find a subscription registry for the given topic: " + topicExpression);

		log.debug("subscribe success, subscriptionId: " + res);

		return res;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.rmi.ISSNService#unsubscribe(java.lang.String)
	 */
	@Override
	public boolean unsubscribe(final String subscrId) throws ISSNException {
		log.debug("unsubscribe request for: " + subscrId);
		return core.unsubscribe(subscrId);
	}

	public Endpoint getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(final Endpoint endpoint) {
		this.endpoint = endpoint;
	}

	public ISSNServiceCore getCore() {
		return core;
	}

	public void setCore(final ISSNServiceCore core) {
		this.core = core;
	}

	public EndpointReferenceBuilder<Endpoint> getEprBuilder() {
		return eprBuilder;
	}

	public void setEprBuilder(final EndpointReferenceBuilder<Endpoint> eprBuilder) {
		this.eprBuilder = eprBuilder;
	}

}
