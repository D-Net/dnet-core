package eu.dnetlib.enabling.is.sn;


/**
 * NotificationSender interface defines an injection point for the NotificationInvoker instance.
 * 
 * <p>
 * It will be fairly common for implementors of NotificationSender to back the invoker reference into an instance
 * variable. This abstract class offers this for free. </p>
 * 
 * @author marko
 * 
 */
public abstract class AbstractNotificationSender implements NotificationSender {

	/**
	 * notification invoker used to send messages to destinations.
	 */
	private NotificationInvoker invoker;

	public NotificationInvoker getInvoker() {
		return invoker;
	}

	@Override
	public void setInvoker(final NotificationInvoker invoker) {
		this.invoker = invoker;
	}

}
