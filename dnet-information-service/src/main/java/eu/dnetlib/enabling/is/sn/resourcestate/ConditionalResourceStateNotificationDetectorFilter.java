package eu.dnetlib.enabling.is.sn.resourcestate;

import org.springframework.beans.factory.annotation.Required;

import eu.dnetlib.enabling.tools.OpaqueResource;
import eu.dnetlib.miscutils.coupling.ExternalCondition;

/**
 * Ignores pending profiles from notification detection.
 * 
 * @author marko
 * 
 */
public class ConditionalResourceStateNotificationDetectorFilter extends AbstractResourceStateNotificationDetectorFilter<OpaqueResource> {

	/**
	 * external condition that drives this filter. We accept notification while the external condition is false.
	 */
	private ExternalCondition inhibitionCondition; // NOPMD
	
	/** 
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.is.sn.resourcestate.AbstractResourceStateNotificationDetectorFilter#accept(java.lang.Object)
	 */
	@Override
	boolean accept(final OpaqueResource resource) {
		return !inhibitionCondition.isTrue();
	}

	public ExternalCondition getInhibitionCondition() {
		return inhibitionCondition;
	}

	@Required
	public void setInhibitionCondition(final ExternalCondition inhibitionCondition) { // NOPMD
		this.inhibitionCondition = inhibitionCondition;
	}

}
