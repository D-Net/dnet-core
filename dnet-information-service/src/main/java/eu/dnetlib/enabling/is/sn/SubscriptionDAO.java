package eu.dnetlib.enabling.is.sn;

import java.util.Collection;

/**
 * Stores subscriptions.
 * 
 * @author marko
 * 
 * @param <T>
 *            subscription type
 */
public interface SubscriptionDAO<T> {
	/**
	 * add a new subscription to the subscription store.
	 * 
	 * TODO: throw some exception on already existing subscription
	 * 
	 * @param subscription
	 *            subscription
	 */
	void addSubscription(T subscription);

	/**
	 * get a particular subscription.
	 * 
	 * @param subscriptionId identifier
	 * @return null if none
	 */
	T getSubscription(String subscriptionId);
	
	
	/**
	 * lists all subscriptions.
	 * 
	 * @return all subscriptions
	 */
	Collection<T> listSubscriptions();
	
	/**
	 * lists all subscriptions for a given prefix.
	 * 
	 * @param prefix topic expression prefix
	 * @return all matching subscriptions
	 */
	Collection<T> listSubscriptions(String prefix);
	
	/**
	 * removes a particular subscription.
	 * 
	 * @param subscriptionId identifier
	 * @return true if successful
	 */
	boolean removeSubscription(String subscriptionId);

}
