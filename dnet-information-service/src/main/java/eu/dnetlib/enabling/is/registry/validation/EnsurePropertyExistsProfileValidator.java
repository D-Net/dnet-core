package eu.dnetlib.enabling.is.registry.validation;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import eu.dnetlib.enabling.tools.OpaqueResource;

public class EnsurePropertyExistsProfileValidator extends TypeAwareProfileValidator {

	private String name; // property name

	@Override
	public boolean accept(OpaqueResource resource) {
		try {
			final XPath xpath = XPathFactory.newInstance().newXPath();
			
			final String xquery = "//EXTRA_FIELDS/FIELD[./key = '" + getName() + "']/value/text()";
			final String value = xpath.evaluate(xquery, resource.asDom().getDocumentElement()) + "";
			
			if (value.isEmpty())
				return false;
			
			return true;

		} catch (XPathExpressionException e) {
			return false;
		}
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
