package eu.dnetlib.enabling.is.sn.resourcestate.hib;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import eu.dnetlib.enabling.is.sn.resourcestate.ResourceStateSubscription;
import eu.dnetlib.enabling.is.sn.resourcestate.ResourceStateSubscriptionDAO;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;

/**
 * Hibernate storage for resource state subscriptions.
 * 
 * @author marko
 * 
 */
public class HibernateResourceStateSubscriptionDAOImpl implements ResourceStateSubscriptionDAO {

	private static final Log log = LogFactory.getLog(HibernateResourceStateSubscriptionDAOImpl.class);

	private SessionFactory sessionFactory;

	private Collection<ResourceStateSubscription> transformToSubscription(final List<Object> inputList) {
		return inputList.stream()
				.map(o -> (ResourceStateSubscription) o)
				.collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.resourcestate.ResourceStateSubscriptionDAO#listSubscriptions(java.lang.String, java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	@Transactional(readOnly = true)
	public Collection<ResourceStateSubscription> listSubscriptions(final String prefix, final String resourceType, final String resourceId) {

		final ResourceStateSubscription exampleEntity = new ResourceStateSubscription();
		exampleEntity.setPrefix(prefix);
		exampleEntity.setType(resourceType);
		exampleEntity.setResourceId(resourceId);

		return transformToSubscription(queryByCriteria(exampleEntity));
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.SubscriptionDAO#addSubscription(java.lang.Object)
	 */
	@Override
	@Transactional
	public void addSubscription(final ResourceStateSubscription subscription) {

		log.debug("evict cached subscriptions");

		getSessionFactory().getCurrentSession().save(subscription);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.SubscriptionDAO#getSubscription(java.lang.String)
	 */
	@Override
	@Transactional(readOnly = true)
	public ResourceStateSubscription getSubscription(final String subscriptionId) {
		return (ResourceStateSubscription) getSessionFactory().getCurrentSession().get(ResourceStateSubscription.class, subscriptionId);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.SubscriptionDAO#listSubscriptions()
	 */
	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public Collection<ResourceStateSubscription> listSubscriptions() {
		return getSessionFactory().getCurrentSession().createQuery("from subscriptions").list();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.SubscriptionDAO#listSubscriptions(java.lang.String)
	 */
	@Override
	@Transactional(readOnly = true)
	public Collection<ResourceStateSubscription> listSubscriptions(final String prefix) {
		final ResourceStateSubscription exampleEntity = new ResourceStateSubscription();
		exampleEntity.setPrefix(prefix);

		return transformToSubscription(queryByCriteria(exampleEntity));
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.sn.SubscriptionDAO#removeSubscription(java.lang.String)
	 */
	@Override
	@Transactional
	public boolean removeSubscription(final String subscriptionId) {
		if (StringUtils.isBlank(subscriptionId)) return false;

		final ResourceStateSubscription entity = getSubscription(subscriptionId);

		if (entity == null) return false;

		getSessionFactory().getCurrentSession().delete(entity);
		return true;
	}

	@SuppressWarnings("unchecked")
	private List<Object> queryByCriteria(final ResourceStateSubscription exampleEntity) {
		Criteria criteria = getSessionFactory().getCurrentSession().createCriteria(ResourceStateSubscription.class).add(Example.create(exampleEntity));
		return criteria.list();
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Required
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
