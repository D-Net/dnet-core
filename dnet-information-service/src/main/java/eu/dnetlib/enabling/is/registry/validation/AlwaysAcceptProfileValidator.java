package eu.dnetlib.enabling.is.registry.validation;

import eu.dnetlib.enabling.tools.OpaqueResource;

/**
 * Dummy bean used to populate the ProfileValidator list.
 * @see eu.dnetlib.enabling.is.registry.validation.ProfileValidationStrategy
 *
 * @author claudio
 *
 */
public class AlwaysAcceptProfileValidator implements ProfileValidator {

	/**
	 * {@inheritDoc}
	 *
	 * @see eu.dnetlib.enabling.is.registry.validation.ProfileValidator#accept(OpaqueResource)
	 */
	@Override
	public boolean accept(OpaqueResource resource) {
		return true;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see eu.dnetlib.enabling.is.registry.validation.ProfileValidator#handle(OpaqueResource, RegistrationPhase)
	 */
	@Override
	public boolean handle(OpaqueResource resource, RegistrationPhase phase) {
		return false;
	}

}
