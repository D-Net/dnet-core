package eu.dnetlib.enabling.is.registry.validation;

import org.springframework.beans.factory.annotation.Required;

import eu.dnetlib.enabling.tools.OpaqueResource;

public abstract class AbstractProfileValidator implements ProfileValidator {

	private RegistrationPhase phase;

	@Override
	public boolean handle(final OpaqueResource resource, final RegistrationPhase phase) {
		return this.phase == phase;
	}

	@Required
	public void setPhase(String phase) {
		this.phase = RegistrationPhase.valueOf(phase);
	}

	public String getPhase() {
		return phase.name();
	}
}
