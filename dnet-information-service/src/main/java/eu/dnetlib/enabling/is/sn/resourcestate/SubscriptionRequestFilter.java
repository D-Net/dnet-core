package eu.dnetlib.enabling.is.sn.resourcestate;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;

public class SubscriptionRequestFilter {

	/**
	 * logger.
	 */
	private static final Log log = LogFactory.getLog(SubscriptionRequestFilter.class); // NOPMD by marko on 11/24/08 5:02 PM

	/**
	 * resource wildcard
	 */
	private static final String ANY_RESOURCE = "*";

	@Value("${services.issn.subscription.filter.active}")
	private boolean active = false;

	public boolean accept(final ResourceStateSubscription rss) {

		if (!isActive()) return true;

		if (StringUtils.isBlank(rss.getXpath()) & StringUtils.equals(rss.getResourceId(), ANY_RESOURCE)) {
			log.debug(String.format("rejected subscription request, resourceId: '%s', xpath: '%s', from: %s", rss.getResourceId(), rss.getXpath(),
					rss.getSubscriber()));
			return false; // we reject wide subscriptions
		}

		return true;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
