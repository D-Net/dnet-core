package eu.dnetlib.enabling.is.sn;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

/**
 * A SubscriptionRequest encapsulates various information about a subscription request.
 * 
 * @author marko
 * 
 */
public class SubscriptionRequest {
	
	/**
	 * subscriber.
	 */
	private W3CEndpointReference subscriber;
	
	/**
	 * topic expression.
	 */
	private String topicExpression;
	
	/**
	 * Preallocated subscription Id. If registries accept the subscription, they will store this in the subscription
	 * identifier.
	 */
	private String subscriptionId;
	
	/**
	 * time to live.
	 */
	private int ttl;
	
	/**
	 * default constructor. 
	 */
	public SubscriptionRequest() {
		// no operation
	}
	
	/**
	 * construct with fields.
	 * 
	 * @param subscriptionId subscription id
	 * @param subscriber subscriber
	 * @param topicExpression topic expression 
	 * @param ttl time to live
	 */
	public SubscriptionRequest(final String subscriptionId, final W3CEndpointReference subscriber, final String topicExpression, final int ttl) {
		super();
		this.subscriptionId = subscriptionId;
		this.subscriber = subscriber;
		this.topicExpression = topicExpression;
		this.ttl = ttl;
	}

	public W3CEndpointReference getSubscriber() {
		return subscriber;
	}
	
	public void setSubscriber(final W3CEndpointReference subscriber) {
		this.subscriber = subscriber;
	}
	
	public String getTopicExpression() {
		return topicExpression;
	}
	
	public void setTopicExpression(final String topicExpression) {
		this.topicExpression = topicExpression;
	}
	
	public String getSubscriptionId() {
		return subscriptionId;
	}
	
	public void setSubscriptionId(final String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public int getTtl() {
		return ttl;
	}

	public void setTtl(final int ttl) {
		this.ttl = ttl;
	}
}
