package eu.dnetlib.enabling.is.sn;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

/**
 * Instances implementing this interfaces will send notifications to consumers.
 * 
 * <p>
 * Notifications will be already detected and the message body will be already computed.
 * </p>
 * 
 * <p>
 * There will be different NotificationSenders, depending on the performance and reliability characteristics required by
 * the message type.
 * <p>
 * 
 * <p>Each notification sender will use a NotificationInvoker to actually forward the notification</p>
 * 
 * @author marko
 * @see eu.dnetlib.enabling.is.sn.NotificationInvoker
 */
public interface NotificationSender {
	/**
	 * sends a given message according to specific policies.
	 * 
	 * @param destination destination EPR
	 * @param message message
	 */
	void send(W3CEndpointReference destination, NotificationMessage message);
	
	/**
	 * Sets the notification invoker which will actually perform the notification delivery.
	 * 
	 * @param invoker notification invoker
	 */
	void setInvoker(NotificationInvoker invoker);
}
