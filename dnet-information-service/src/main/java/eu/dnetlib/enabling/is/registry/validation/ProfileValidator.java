package eu.dnetlib.enabling.is.registry.validation;

import eu.dnetlib.enabling.tools.OpaqueResource;

public interface ProfileValidator {
	/**
	 * True if this validator will handle this profile, false otherwise. 
	 * 'accept' will be invoked only if 'handle' returns true
	 * 
	 * @param resource
	 * @param phase
	 * @return
	 */
	public boolean handle(OpaqueResource resource, RegistrationPhase phase);

	/**
	 * true if accept, false reject.
	 * 
	 * @param resource
	 * @return
	 */
	public boolean accept(OpaqueResource resource);
}
