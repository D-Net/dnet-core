package eu.dnetlib.enabling.is.registry;

import javax.xml.xpath.XPathExpressionException;

/**
 * Uses current driver convention and tries to guess the correct resource kinds (both normal and pending) depending on
 * the resource type name.
 * 
 * @author marko
 * 
 */
public class NameBasedResourceKindResolver implements ResourceKindResolver {

	/** 
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.is.registry.ResourceKindResolver#getNormalKindForType(java.lang.String)
	 */
	@Override
	public String getNormalKindForType(final String resourceType) throws XPathExpressionException {
		if (isService(resourceType))
			return "ServiceResources";
		return generateKind(resourceType);
	}

	/** 
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.is.registry.ResourceKindResolver#getPendingKindForType(java.lang.String)
	 */
	@Override
	public String getPendingKindForType(final String resourceType) throws XPathExpressionException {
		if (isService(resourceType))
			return "PendingServiceResources";
		return "PendingDSResources";
	}

	private boolean isService(final String resourceType) {
		return resourceType.matches(".*ServiceResourceType$");
	}

	private String generateKind(final String resourceType) {
		return resourceType.replaceAll("ResourceType$", "Resources");
	}

}
