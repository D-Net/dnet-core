package eu.dnetlib.enabling.is.sn;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

/**
 * Implementors of this interface decouple the notification producing and dispatching policies
 * with the actually on-wire sending of the notification message.
 * 
 * <p>
 * Notification invokers expose a fairly low level interface to notification dispatchers, so that 
 * dispatching policies can configure various aspects of the underlying transport.
 * </p>
 * 
 * @author marko
 *
 */
public interface NotificationInvoker {
	/**
	 * for now only timeout is exposed.
	 * 
	 * @param destination destination EPR
	 * @param message message
	 * @param timeout timeout in seconds
	 */
	void send(W3CEndpointReference destination, NotificationMessage message, int timeout);
}
