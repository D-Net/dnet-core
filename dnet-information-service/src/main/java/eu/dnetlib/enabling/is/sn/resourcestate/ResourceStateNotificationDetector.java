package eu.dnetlib.enabling.is.sn.resourcestate;

import eu.dnetlib.enabling.is.sn.NotificationDetector;

/**
 * Implementors detect topic assertions by sensing changes in resource state.
 * 
 * @author marko
 * @param <T>
 *            type of resource
 */
public interface ResourceStateNotificationDetector<T> extends NotificationDetector {
	/**
	 * called when a new resource is created.
	 * 
	 * @param newResource
	 *            new resource
	 */
	void resourceCreated(T newResource);

	/**
	 * called when a resource is modified.
	 * 
	 * @param oldResource
	 *            old resource
	 * @param newResource
	 *            new resource
	 */
	void resourceUpdated(T oldResource, T newResource);

	/**
	 * called when a resource is deleted.
	 * 
	 * @param oldResource
	 *            old resource
	 */
	void resourceDeleted(T oldResource);

}
