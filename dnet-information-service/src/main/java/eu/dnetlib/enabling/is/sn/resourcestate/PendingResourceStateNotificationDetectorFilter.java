package eu.dnetlib.enabling.is.sn.resourcestate;

import eu.dnetlib.enabling.tools.OpaqueResource;

/**
 * Ignores pending profiles from notification detection.
 * 
 * @author marko
 * 
 */
public class PendingResourceStateNotificationDetectorFilter extends AbstractResourceStateNotificationDetectorFilter<OpaqueResource> {

	/** 
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.is.sn.resourcestate.AbstractResourceStateNotificationDetectorFilter#accept(java.lang.Object)
	 */
	@Override
	boolean accept(final OpaqueResource resource) {
		if (resource.getResourceKind().startsWith("Pending"))
			return false;
		return true;
	}

}
