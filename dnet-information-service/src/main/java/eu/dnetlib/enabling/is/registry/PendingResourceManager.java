package eu.dnetlib.enabling.is.registry;

import eu.dnetlib.enabling.tools.OpaqueResource;

/**
 * The IS registry has to handle the 'pending' state of the resources in several ways.
 * 
 * <p>Implementors of this interface will change the profiles according to the pending profile management policy.</p>
 * 
 * <p>See implementors.</p>
 * 
 * @author marko
 *
 */
public interface PendingResourceManager {

	/**
	 * change a profile to a pending state.
	 * 
	 * @param resource opaque resource
	 */
	void setPending(OpaqueResource resource);

	/**
	 * change a profile to a valid state.
	 * 
	 * same as setPending(resource, false);
	 *  
	 * @param resource opaque resource
	 */
	void setValid(OpaqueResource resource);

	/**
	 * change a profile to a valid state.
	 * 
	 * @param resource resource
	 * @param local true if the resource is not persistent
	 */
	void setPending(OpaqueResource resource, boolean local);

}
