package eu.dnetlib.enabling.is.sn;

/**
 * NotificationDetector interface defines an injection point for the NotificationSender instance.
 * 
 * <p>
 * It will be fairly common for implementors of NotificatiorDetector to back the sender reference into an instance
 * variable. This abstract class offers this for free.
 * </p>
 * 
 * @author marko
 * 
 */
public abstract class AbstractNotificationDetector implements NotificationDetector {

	/**
	 * notification sender which will receive events triggered by this detector.
	 */
	private NotificationSender sender;

	public NotificationSender getSender() {
		return sender;
	}

	@Override
	public void setSender(final NotificationSender sender) {
		this.sender = sender;
	}

}
