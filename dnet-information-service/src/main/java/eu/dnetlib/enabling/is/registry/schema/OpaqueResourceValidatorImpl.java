package eu.dnetlib.enabling.is.registry.schema;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

import eu.dnetlib.enabling.tools.OpaqueResource;

/**
 * validates an opaque resource according to its declared resource type.
 * 
 * @author marko
 * 
 */
public class OpaqueResourceValidatorImpl implements OpaqueResourceValidator {

	/**
	 * schema dao.
	 */
	private ResourceSchemaDAO schemaDao;

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.is.registry.schema.OpaqueResourceValidator#validate(eu.dnetlib.enabling.tools.OpaqueResource)
	 */
	@Override
	public void validate(final OpaqueResource resource) throws ValidationException {
		final Schema schema = getSchemaDao().getResourceSchema(resource.getResourceType());
		if (schema == null)
			throw new ValidationException("no registered schema for " + resource.getResourceType());
		final Validator validator = schema.newValidator();

		try {
			// hack to workaround for #1500 
			validator.validate(new StreamSource(new StringReader(resource.asString())));
		} catch (SAXException e) {
			throw new ValidationException(e);
		} catch (IOException e) {
			throw new ValidationException(e);
		}
	}

	public ResourceSchemaDAO getSchemaDao() {
		return schemaDao;
	}

	public void setSchemaDao(final ResourceSchemaDAO schemaDao) {
		this.schemaDao = schemaDao;
	}

}
