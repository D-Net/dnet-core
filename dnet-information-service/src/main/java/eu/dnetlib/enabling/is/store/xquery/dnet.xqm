xquery version "1.0";

module namespace dnet="http://namespace.dnetlib.eu/xquery/dnet";
declare namespace xmldb="http://exist-db.org/xquery/xmldb";

declare function dnet:deleteDocument($x as element()) {

 xmldb:remove(replace($x/base-uri(),'/[^/]*$', ''), replace($x/base-uri(),'^.*/([^/]*)$', '$1'))

};
