package eu.dnetlib.enabling.inspector;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import eu.dnetlib.miscutils.datetime.DateUtils;
import eu.dnetlib.miscutils.datetime.HumanTime;

/**
 * This controller offers generic info about current node (ip, uptime, ...) 
 * 
 * @author michele
 *
 */
@Controller
public class GenericInfoController extends AbstractInspectorController {

	@Resource(name="containerInfo")
	Map<String,String> containerInfo;
	
	@RequestMapping(value = "/inspector/info.do")
	void query(final Model model) {
		RuntimeMXBean mxbean = ManagementFactory.getRuntimeMXBean();
		
		Map<String,String> sysinfo = new LinkedHashMap<String, String>();
		sysinfo.put("Uptime", HumanTime.exactly(mxbean.getUptime()));
		sysinfo.put("Start Time", DateUtils.calculate_ISO8601(mxbean.getStartTime()));

		sysinfo.put("JVM Name", mxbean.getVmName());
		sysinfo.put("JVM Vendor", mxbean.getVmVendor());
		sysinfo.put("JVM Version", mxbean.getVmVersion());
		sysinfo.put("JVM Spec Name", mxbean.getSpecName());
		sysinfo.put("JVM Spec Vendor", mxbean.getSpecVendor());
		sysinfo.put("JVM Spec Version", mxbean.getSpecVersion());

		sysinfo.put("Running JVM Name", mxbean.getName());
		sysinfo.put("Management Spec Version", mxbean.getManagementSpecVersion());
		
		sysinfo.put("Classpath", mxbean.getClassPath().replaceAll(":", " : "));
		sysinfo.put("Boot ClassPath", mxbean.getBootClassPath().replaceAll(":", " : "));
		sysinfo.put("Input arguments", mxbean.getInputArguments().toString());
		sysinfo.put("Library Path", mxbean.getLibraryPath().replaceAll(":", " : "));

		sysinfo.put("SystemProperties", mxbean.getSystemProperties().toString());
		
		
		model.addAttribute("containerInfo", containerInfo);
		model.addAttribute("sysInfo", sysinfo);
	}
	
}

