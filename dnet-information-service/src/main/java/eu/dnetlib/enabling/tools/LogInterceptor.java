package eu.dnetlib.enabling.tools;

import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.springframework.aop.interceptor.CustomizableTraceInterceptor;

/**
 * This class intercepts calls to the OAICore to measure its execution time. It uses Spring AOP.
 *
 * @author alessia
 */
public class LogInterceptor extends CustomizableTraceInterceptor {

	@Override
	protected void writeToLog(final Log logger, final String message, final Throwable ex) {
		if (ex != null) {
			logger.error(message, ex);
		} else {
			logger.debug(message);
		}
	}

	@Override
	protected boolean isInterceptorEnabled(final MethodInvocation invocation, final Log logger) {
		return true;
	}
}
