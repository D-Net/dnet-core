package eu.dnetlib.enabling.tools;

/**
 * Abstracts the creation of a resource identifier from xmldb file name and xmldb collection name.
 * 
 * @author marko
 * 
 */
public interface ResourceIdentifierComposer {
	/**
	 * Create a resId from a xmldb filename and collection name.
	 * 
	 * @param fileName
	 * @param coll
	 * @return
	 */
	String createResourceId(final String fileName, final String coll);
}
