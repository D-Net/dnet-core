package eu.dnetlib.enabling.tools;


/**
 * Standard DNet-1.0 compat xquery utils.
 * 
 * @author marko
 *
 */
public class XQueryUtilsImpl implements XQueryUtils {

	/** 
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.tools.XQueryUtils#getCollectionAbsPath(eu.dnetlib.enabling.tools.OpaqueResource)
	 */
	@Override
	public String getCollectionAbsPath(final OpaqueResource resource) {
		return getRootCollection() + getCollectionPath(resource);
	}

	/** 
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.tools.XQueryUtils#getCollectionPath(eu.dnetlib.enabling.tools.OpaqueResource)
	 */
	@Override
	public String getCollectionPath(final OpaqueResource resource) {
		final StringBuilder buffer = new StringBuilder();
		buffer.append(resource.getResourceKind());
		buffer.append('/');
		buffer.append(resource.getResourceType());
		return buffer.toString();
	}

	/** 
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.tools.XQueryUtils#getRootCollection()
	 */
	@Override
	public String getRootCollection() {
		return "/db/DRIVER/";
	}

	/** 
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.tools.XQueryUtils#getFileName(eu.dnetlib.enabling.tools.OpaqueResource)
	 */
	@Override
	public String getFileName(final OpaqueResource resource) {
		return resource.getResourceId().split("_")[0];
	}

}
