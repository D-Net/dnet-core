package eu.dnetlib.enabling.tools;

import com.sun.xml.messaging.saaj.util.Base64;

/**
 * resolve resource file and collection names from the resource identifier according to the DNet 1.0 policy, by encoding
 * the collection path in the identifier itself (as base64).
 * 
 * @author marko
 * 
 */
public class CompatResourceIdentifierResolverImpl implements ResourceIdentifierResolver, ResourceIdentifierComposer {

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.tools.ResourceIdentifierResolver#getCollectionName(java.lang.String)
	 */
	@Override
	public String getCollectionName(final String resId) {
		final String[] components = resId.split("_");
		if (components.length == 1)
			return "DefaultCollection";
		return Base64.base64Decode(components[1]);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.tools.ResourceIdentifierResolver#getFileName(java.lang.String)
	 */
	@Override
	public String getFileName(final String resId) {
		return resId.split("_")[0];
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.tools.ResourceIdentifierComposer#createResourceId(java.lang.String, java.lang.String)
	 */
	@Override
	public String createResourceId(final String fileName, final String coll) {
		return fileName + "_" + new String(Base64.encode(coll.getBytes()));
	}
}
