package eu.dnetlib.xml.database;

import org.w3c.dom.Document;

/**
 * xml db trigger.
 *
 * @author marko
 *
 */
public interface Trigger {
	/**
	 * each trigger has a user defined unique name.
	 *
	 * @return trigger name
	 */
	String getName();

	/**
	 * triggered when a new document is created.
	 *
	 * @param file
	 *            file name
	 * @param collection
	 *            collection name
	 * @param newDoc
	 *            newly created document
	 */
	void created(String file, String collection, Document newDoc);

	/**
	 * triggered when a document is updated.
	 *
	 * @param file
	 *            file name
	 * @param collection
	 *            collection name
	 * @param oldDoc
	 *            old version
	 * @param newDoc
	 *            new version
	 */
	void updated(String file, String collection, Document oldDoc, Document newDoc);

	/**
	 * triggered when a document is deleted.
	 *
	 * @param file
	 *            file name
	 * @param collection
	 *            collection name
	 * @param oldDoc
	 *            deleted document
	 */
	void deleted(String file, String collection, Document oldDoc);
}
