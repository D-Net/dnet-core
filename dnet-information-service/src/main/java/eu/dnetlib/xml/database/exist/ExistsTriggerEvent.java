package eu.dnetlib.xml.database.exist;

import org.w3c.dom.Document;

/**
 * Created by sandro on 3/14/16.
 */
public class ExistsTriggerEvent {

	private String collection;

	private String name;

	private EventType eventType;

	private Document document;

	private Document oldDocument;


	public String getCollection() {
		return collection;
	}

	public void setCollection(final String collection) {
		this.collection = collection;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public EventType getEventType() {
		return eventType;
	}

	public void setEventType(final EventType eventType) {
		this.eventType = eventType;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(final Document document) {
		this.document = document;
	}

	public Document getOldDocument() {
		return oldDocument;
	}

	public void setOldDocument(final Document oldDocument) {
		this.oldDocument = oldDocument;
	}
}
