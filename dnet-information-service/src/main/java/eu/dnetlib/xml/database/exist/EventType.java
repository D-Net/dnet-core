package eu.dnetlib.xml.database.exist;

/**
 * Created by claudio on 20/12/2016.
 */
public enum EventType {
	UPDATE,
	CREATE,
	DELETE
}
