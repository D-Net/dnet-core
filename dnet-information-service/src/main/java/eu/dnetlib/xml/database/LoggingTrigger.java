package eu.dnetlib.xml.database;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;

/**
 * This trigger simply logs all xmldb CRUDE events.
 *
 * @author marko
 *
 */
public class LoggingTrigger extends AbstractTrigger {
	/**
	 * logger.
	 */
	private static final Log log = LogFactory.getLog(LoggingTrigger.class); // NOPMD by marko on 11/24/08 5:02 PM

	/**
	 * {@inheritDoc}
	 * @see eu.dnetlib.xml.database.Trigger#created(java.lang.String, java.lang.String, org.w3c.dom.Document)
	 */
	@Override
	public void created(final String file, final String collection, final Document newDoc) {
		log.info("xml resource created: " + collection + "/" + file);
	}

	/**
	 * {@inheritDoc}
	 * @see eu.dnetlib.xml.database.Trigger#deleted(java.lang.String, java.lang.String, org.w3c.dom.Document)
	 */
	@Override
	public void deleted(final String file, final String collection, final Document oldDoc) {
		log.info("xml resource deleted: " + collection + "/" + file);
	}

	/**
	 * {@inheritDoc}
	 * @see eu.dnetlib.xml.database.Trigger#updated(java.lang.String, java.lang.String, org.w3c.dom.Document, org.w3c.dom.Document)
	 */
	@Override
	public void updated(final String file, final String collection, final Document oldDoc, final Document newDoc) {
		log.info("xml resource updated: " + collection + "/" + file);
	}

}
