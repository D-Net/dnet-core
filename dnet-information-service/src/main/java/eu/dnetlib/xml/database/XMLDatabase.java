package eu.dnetlib.xml.database;

import java.util.Iterator;
import java.util.List;

import org.exist.util.DatabaseConfigurationException;
import org.xmldb.api.base.XMLDBException;

/**
 * xmldb API is ugly.
 *
 * This is a thin wrapper to the xmldb API with focus on the primary operations used by dnet
 *
 * @author marko
 *
 */
public interface XMLDatabase { // NOPMD
	/**
	 * creates a new resource or updates the resource if it already exists.
	 *
	 * @param name
	 *            file name
	 * @param collection
	 *            collection name
	 * @param content
	 *            serialized xml string
	 * @throws XMLDBException
	 *             happens
	 */
	void create(String name, String collection, String content) throws XMLDBException;

	/**
	 * updates and already existing resource.
	 *
	 * @param name
	 *            file name
	 * @param collection
	 *            collection name
	 * @param content
	 *            serialized xml string
	 * @throws XMLDBException
	 *             fails if the resource doesn't exist
	 */
	void update(String name, String collection, String content) throws XMLDBException;

	/**
	 * removes a resource.
	 *
	 * @param name
	 *            file name
	 * @param collection
	 *            collection name
	 * @return false if the resource doesn't exist
	 * @throws XMLDBException
	 *             could happen
	 */
	boolean remove(String name, String collection) throws XMLDBException;

	/**
	 * read a resource string xml.
	 *
	 * @param name
	 *            file name
	 * @param collection
	 *            collection name
	 * @return null if the resource doesn't exist, otherwise it returns the xml string serialization
	 * @throws XMLDBException
	 *             happens
	 */
	String read(String name, String collection) throws XMLDBException;

	/**
	 * Execute an xquery.
	 *
	 * @param xquery
	 *            xquery source
	 * @return a xmldb resultset object
	 * @throws XMLDBException
	 *             happens
	 */
	Iterator<String> xquery(String xquery) throws XMLDBException;

	/**
	 * creates a new collection, non recursively.
	 *
	 * @param collection
	 *            collection name
	 * @throws XMLDBException
	 *             happens
	 */
	void createCollection(String collection) throws XMLDBException;


	/**
	 * remove a collection.
	 *
	 * @param collection
	 *            collection name
	 * @throws XMLDBException
	 *             happens also when the collection doesn't eXist
	 */
	void removeCollection(String collection) throws XMLDBException;

	void xupdate(String query) throws XMLDBException;

	/**
	 * check whether a collection exists.
	 *
	 * @param collection
	 *            collection name
	 * @return true if the collection exists
	 * @throws XMLDBException
	 *             happens
	 */
	boolean collectionExists(String collection) throws XMLDBException;

	/**
	 * lists child collections.
	 *
	 * @param collection
	 *            parent collections
	 * @return list of collection names
	 * @throws XMLDBException
	 *             happens
	 */
	List<String> listChildCollections(String collection) throws XMLDBException;

	/**
	 * list the content of a collection.
	 *
	 * @param collection
	 *            parent collection
	 * @return list of resource names
	 * @throws XMLDBException
	 *             happens
	 */
	List<String> list(String collection) throws XMLDBException;

	/**
	 * returns the name of the root collection.
	 *
	 * @return collection name
	 */
	String getRootCollection();

	/**
	 * register a new trigger on a collection.
	 *
	 * @param trigger
	 *            trigger instance
	 * @param collection
	 *            collection pattern
	 * @throws XMLDBException
	 *             happens
	 */
	void registerTrigger(Trigger trigger, String collection) throws XMLDBException;


	/**
	 * make a backup of entire database.
	 *
	 * @throws XMLDBException could happen
	 * @throws DatabaseConfigurationException could happen
	 * @return the path in which the Backup has been saved
	 */
	String backup() throws XMLDBException, DatabaseConfigurationException;

	/**
	 * Return the dir that contains all backups.
	 *
	 * @return the path in which the Backup has been saved
	 */
	String getBackupDir();

}
