package eu.dnetlib.xml.database;

/**
 * most trigger will hold the trigger name in an instance variable.
 *
 * The Trigger contract requires a getter (getName()). This class implements the setter too.
 *
 * @author marko
 *
 */
public abstract class AbstractTrigger implements Trigger {

	/**
	 * trigger name.
	 */
	private String name;

	@Override
	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

}
