package eu.dnetlib.xml.database.exist;

import java.util.HashMap;
import java.util.Map;

import eu.dnetlib.xml.database.Trigger;

/**
 * eXist triggers are instantiated directly by the exist database via reflection.
 *
 * we need a way to manage trigger objects from outside, in order to manage them manage them from the IoC container.
 *
 * @author marko
 *
 */
public final class ExistTriggerRegistry {

	/**
	 * singleton object.
	 */
	private static ExistTriggerRegistry singleton = new ExistTriggerRegistry();

	/**
	 * get the singleton instance.
	 *
	 * @return singleton object
	 */
	public static ExistTriggerRegistry defaultInstance() {
		return singleton;
	}

	/**
	 * forbids instantiation.
	 */
	private ExistTriggerRegistry() {
	}

	/**
	 * maps trigger names to trigger instances.
	 */
	private Map<String, Trigger> triggerRegistry = new HashMap<String, Trigger>();

	/**
	 * get a trigger for a given id.
	 *
	 * @param triggerId trigger identifier
	 * @return trigger instance
	 */
	public Trigger getTrigger(final String triggerId) {
		return getTriggerRegistry().get(triggerId);
	}

	/**
	 * associate a new trigger.
	 *
	 * @param name trigger name 
	 * @param trigger trigger instance
	 */
	public void registerTrigger(final String name, final Trigger trigger) {
		triggerRegistry.put(name, trigger);
	}

	Map<String, Trigger> getTriggerRegistry() {
		return triggerRegistry;
	}

	void setTriggerRegistry(final Map<String, Trigger> triggerRegistry) {
		this.triggerRegistry = triggerRegistry;
	}


}
