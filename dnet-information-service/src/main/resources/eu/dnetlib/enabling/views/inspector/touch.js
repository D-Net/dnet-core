
$(document).ready(function() {
	
	$("pre.profile.toggleEnabled span").live("click", touchHandler);
	
	$("#touchlink").click(function() { 
	 
	    if ($("pre.profile").is('.toggleEnabled')) {
	        $(this).html("touch");
	    } else {
	        $(this).html("untouch");
	    }
	    $("pre.profile").toggleClass("toggleEnabled");
	});
});

function touchHandler() {
	var field=$(this);
	$.ajax({
		url: "touch?xpath=" + field.attr('id'),
		success: function() {
			//field.html(data);
			location.reload(true);
		}
	});
}
