$inspector/master(header={

<link rel="stylesheet" href="$baseUrl$/static.do?src=touch.css">
<script type="text/javascript" src="$baseUrl$/static.do?src=touch.js"></script>

}, it={

<h2>Database inspector</h2>

$inspector/path()$

<ul id="subnav">
<li><a href="delete" onclick="return confirm('do you really want to delete?')">delete</a></li></li>
<li><a href="edit">edit</a></li></li>
<li><a href="raw">raw</a></li></li>
<li><a href="validate" onclick="return confirm('do you really want to validate the profile?')">validate</a></li></li>
<li><a href="invalidate" onclick="return confirm('do you really want to invalidate the profile?')">invalidate</a></li></li>
<li><a href="#" id="touchlink">touch</a></li></li>
</ul>

<pre class="profile">
$file$
</pre>

})$