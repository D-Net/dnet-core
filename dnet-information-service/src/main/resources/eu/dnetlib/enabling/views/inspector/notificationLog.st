$inspector/master(it={
<style type="text/css">
table * { font-size: 12px; word-wrap: break-word; }
th      { font-weight:bold; }
</style>

<p>Queued, ongoing and sent notifications:</p>

<table border="1" cellspacing="0" cellpadding="4">
	<thead>
	<tr>
		<th>Destination</th>
		<th>Topic</th>
		<th>Status</th>
		<th>Duration</th>
		<th>Error</th> 
	</tr>
	</thead>
	<tbody>
$log:{
	<tr>
		<td>$it.destination$</td>
		<td>$it.message.topic$</td>
		<td>$it.status$</td>
		<td><span title="$it.start$ - $it.finish$">$it.duration$</span></td>
		<td>$it.errorMessage$</td> 
	</tr>
}$
	</tbody>
</table>


})$