$inspector/master(it={
<h2>Database inspector - editing</h2>
$inspector/path()$

<ul id="subnav">
<li><a href="javascript:document.forms[0].submit()">save</a></li></li>
</ul>

<form action="save" method="POST" >
$if(creating)$
 <p>creating new file:</p>
$endif$

<p>You can edit the profile from within the browser. Upon save it will be checked for schema compliance.</p>
<textarea name="source" class="profile" id="profile" autofocus>
$file$
</textarea>

 <input type="hidden" name="creating" value="$creating$"/>
</form>

})$