$inspector/master(it={
  <h2>Database inspector</h2>
  $inspector/path()$
  <ul id="subnav">
		<li><a href="create">create xml file</a></li>
		<li><a onclick="return confirm('do you really want to delete the collection and all its children?') && confirm('really really sure?') && (window.location='deleteCollection')" href="">delete this collection</a></li>
		<li><a href="createsubcoll">create subcollection</a></li>
	</ul>
	

  <ul id="filelist">
    $collections:{<li>$it.collectionPath:{<a href="$it.url$/list">$it.name$</a>};separator=" / "$</li>}$
    $files:{<li><a href="$it$/show">$it$</a></li>}$
  </ul>
})$
