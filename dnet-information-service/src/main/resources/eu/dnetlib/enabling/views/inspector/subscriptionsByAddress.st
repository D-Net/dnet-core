$inspector/master(it={
<style type="text/css">
#subscriptions { list-style-type: }
</style>

<h2>Subscription &amp; Notifications (by address)</h2>

<p>subscriptions:</p>

<form>
	Address Prefix:
	<input type="text" value="$address$" name="address" size="30"/>
	<input type="submit" value="show" /> 
</form>

$if(address)$
<p>
	<form onSubmit="return confirm('Are you sure?')" action="deleteSubscriptionsByAddress.do">
		<input type="hidden" value="$address$" name="address" />
		<input type="submit" value="delete following subscriptions" /> 
	</form>
</p>
$endif$

$if(subscriptions)$
		$subscriptions:{
		<ul id="subscriptions">
			<li><b>Prefix: </b>$it.prefix$</li>
			<li><b>Type: </b>$it.type$</li>
			<li><b>ResId: </b>$it.resourceId$</li>
			<li><b>XPath: </b>$it.xpath$</li>
			<li><b>Address: </b>$it.address$</li>
			<li><b>SubscrID: </b>$it.id$</li>
		</ul><hr />
		}$
$endif$
})$