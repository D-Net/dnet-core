$inspector/master(it={

<h2>Subscription backups</h2>

<form method="POST" action="backupSubscriptions.do">
	<input type="hidden" name="exec" value="1" />
	<input type="submit" value="Execute a new backup"/>
</form>

<p>$message$</p>

<p>Backups (size = $size$)</p>
<table border="1">
$backups:{
	<tr>
		<td>$it$</td>
		<td>
			<form method="POST" action="backupDownload.do">
				<input type="hidden" name="backup" value="$it.name$" />
				<input type="hidden" name="type" value="subscription" />
				<input type="submit" value="download"/>
			</form>
		</td>
		<td>
			<form method="POST" action="backupLog.do">
				<input type="hidden" name="backup" value="$it.name$" />
				<input type="hidden" name="type" value="subscription" />
				<input type="submit" value="log"/>
			</form>
		</td>
		<td>
			<form method="POST" action="backupSubscriptions.do">
				<input type="hidden" name="delete" value="$it.name$" />
				<input type="submit" value="delete"/>
			</form>
		</td>
	<tr>
}$
<table>

})$