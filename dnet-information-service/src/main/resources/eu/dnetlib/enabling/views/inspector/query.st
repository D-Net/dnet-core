$inspector/master(it={

<h2>XQuery</h2>

<style type="text/css">
  #results {
    width: 100%;
  }
  
  #results td:first-child {
   width: 2em;
  }
  
  #results td {
    border: 1px solid #cecece;
  }
</style>

you can execute an arbitrary xquery on the xmldb:<br/>

<form "method="POST">
<textarea name="query" style="width: 880px; height: 420px" autofocus>$query$</textarea>
<br/>
<input type="submit" value="submit"/>
</form>

<p>$message$</p>

<p>$size$ results:</p>
<table id="results">
$results:{
<tr>
 <td><pre>$i$</pre></td>
 <td><pre>$it$</pre></td>
</tr>
}$
</table>

})$