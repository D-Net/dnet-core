$inspector/master(it={
<style type="text/css">
#subscriptions { list-style-type: }
</style>

<h2>Subscription &amp; Notifications</h2>

<p>Notifications active: <b>$enabled$</b> <a href="toggleNotifications.do" onclick="return confirm('are you sure you want to toggle notification behavior? If you disable notifications workflows might not work!')">toggle</a></p>
<p><a href="notificationLog.do">sent notfication log</a></p>

<p><a href="snByAddress.do">subscriptions filtered by address</a></p>

<p>subscriptions:</p>

$subscriptions:{
<ul id="subscriptions">
	<li><b>Prefix: </b>$it.prefix$</li>
	<li><b>Type: </b>$it.type$</li>
	<li><b>ResId: </b>$it.resourceId$</li>
	<li><b>XPath: </b>$it.xpath$</li>
	<li><b>Address: </b>$it.address$</li>
	<li><b>SubscrID: </b>$it.id$</li>
	<li><a href="deleteSubscription.do?id=$it.id$">delete</a></li>
</ul><hr />
}$

})$