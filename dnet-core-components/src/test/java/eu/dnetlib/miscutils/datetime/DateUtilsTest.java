package eu.dnetlib.miscutils.datetime;

import java.text.ParseException;
import java.util.Date;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DateUtilsTest {

	private final DateUtils dateUtils = new DateUtils();

	@Test
	public void testGetDuration() {
		String theDate = "1900-01-01T00:13:57";
		String duration = dateUtils.getDuration(theDate);
		assertEquals("00:13:57", duration);
	}

	@Test
	public void getISOdateTimeFromLong(){
		Long dateLong = Long.parseLong("1520531435465");
		String iso = DateUtils.calculate_ISO8601(dateLong);
		System.out.println(iso);
	}


	@Test
	public void test1() throws ParseException {
		String fromDate = "2019-04-11";
		Date date =  org.apache.commons.lang3.time.DateUtils.parseDate(
				fromDate,
				new String[] { "yyyy-MM-dd", "yyyy-MM-dd'T'HH:mm:ssXXX", "yyyy-MM-dd'T'HH:mm:ss.SSSX", "yyyy-MM-dd'T'HH:mm:ssZ",
						"yyyy-MM-dd'T'HH:mm:ss.SX" });
		long timestamp =  date.toInstant().toEpochMilli();
		System.out.println(timestamp);
	}

}
