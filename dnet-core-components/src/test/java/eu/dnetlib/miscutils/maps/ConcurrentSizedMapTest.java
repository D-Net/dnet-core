package eu.dnetlib.miscutils.maps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;

import eu.dnetlib.miscutils.maps.ConcurrentSizedMap;

public class ConcurrentSizedMapTest {

	private ConcurrentSizedMap<String, String> map;
	
	private int size = 2;
	
	@Before
	public void setUp() throws Exception {
		map = new ConcurrentSizedMap<String, String>();
		map.setQueueSize(size);
	}
	
	@Test
	public void testMap() throws InterruptedException {

		map.put("a", "value a");
		assertNotNull((map.get("a")));
		
		map.put("b", "value b");
		assertNotNull((map.get("b")));
		
		map.put("c", "value c");
		assertNotNull((map.get("c")));
		assertEquals(size, map.size());
		
		assertNull(map.get("a"));
		map.put("a", "new value a");

		assertEquals("new value a", map.get("a"));
		assertEquals(size, map.size());
	}

}
