package eu.dnetlib.miscutils.hstree.sample.wrongtree;

import eu.dnetlib.miscutils.hstree.TreeNode;
import eu.dnetlib.miscutils.hstree.sample.resources.ChildResource;
import eu.dnetlib.miscutils.hstree.sample.resources.SampleResource;
import eu.dnetlib.miscutils.hstree.sample.tree.MyLoggingVisitor;

public class WrongRootTree extends TreeNode<SampleResource, ChildResource, MyLoggingVisitor, WrongFirstChild> {

	public WrongRootTree(final SampleResource resource) {
		super(resource);
	}

}
