package eu.dnetlib.miscutils.hstree;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import eu.dnetlib.miscutils.hstree.sample.resources.ChildResource;
import eu.dnetlib.miscutils.hstree.sample.resources.SampleResource;
import eu.dnetlib.miscutils.hstree.sample.resources.SubResource;
import eu.dnetlib.miscutils.hstree.sample.wrongtree.WrongFirstChild;
import eu.dnetlib.miscutils.hstree.sample.wrongtree.WrongRootTree;
import eu.dnetlib.miscutils.hstree.sample.wrongtree.WrongSecondChild;

public class WrongTreeNodeTest {

	/**
	 * Try to break/misalign the signatures of the 'sample.wrongtree' package. The code should not be able to compile.
	 */
	@Test
	public void simpleTest() {
		final WrongRootTree root = new WrongRootTree(new SampleResource());

		final WrongFirstChild ch1 = root.addChild(new ChildResource());
		ch1.addChild(new SubResource());
		ch1.addChild(new SubResource());

		final WrongFirstChild ch2 = root.addChild(new ChildResource());
		ch2.addChild(new SubResource());
		ch2.addChild(new SubResource());
		final WrongSecondChild sc1 = ch2.addChild(new SubResource());

		// of course we can do obscene things like this,
		// but if somebody casts there is no escape.
		boolean gotRuntimeEx = false; // NOPMD
		try {
			sc1.addChild((Void) null);
		} catch (RuntimeException e) {
			if (e.getMessage().equals(TreeNode.CHILDR_UNDER_LEAF))
				gotRuntimeEx = true;
		}
		assertTrue("got exception", gotRuntimeEx); // TODO: exploit junit4 for checking exceptions

	}
}
