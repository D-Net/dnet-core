package eu.dnetlib.miscutils.hstree.sample.tree;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.dnetlib.miscutils.hstree.sample.resources.ChildResource;
import eu.dnetlib.miscutils.hstree.sample.resources.SampleResource;
import eu.dnetlib.miscutils.hstree.sample.resources.SubResource;

public class MyLoggingVisitor implements MyTreeVisitor {
	private static final Log log = LogFactory.getLog(MyLoggingVisitor.class); // NOPMD by marko on 11/24/08 5:02 PM

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * eu.rinfrastructures.genericstest.sample.wrongtree.MyTreeVisitor#visit(eu.rinfrastructures.genericstest.sample
	 * .resources.SampleResource)
	 */
	@Override
	public void visit(final SampleResource resource) {
		log.info("sample");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * eu.rinfrastructures.genericstest.sample.wrongtree.MyTreeVisitor#visit(eu.rinfrastructures.genericstest.sample
	 * .resources.ChildResource)
	 */
	@Override
	public void visit(final ChildResource resource) {
		log.info("child");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * eu.rinfrastructures.genericstest.sample.wrongtree.MyTreeVisitor#visit(eu.rinfrastructures.genericstest.sample
	 * .resources.SubResource)
	 */
	@Override
	public void visit(final SubResource resource) {
		log.info("sub");
	}

}
