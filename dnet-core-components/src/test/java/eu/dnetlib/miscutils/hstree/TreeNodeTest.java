package eu.dnetlib.miscutils.hstree;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import eu.dnetlib.miscutils.hstree.sample.resources.ChildResource;
import eu.dnetlib.miscutils.hstree.sample.resources.SampleResource;
import eu.dnetlib.miscutils.hstree.sample.resources.SubResource;
import eu.dnetlib.miscutils.hstree.sample.tree.FirstChild;
import eu.dnetlib.miscutils.hstree.sample.tree.MyTreeNode;
import eu.dnetlib.miscutils.hstree.sample.tree.RootTree;

public class TreeNodeTest {

	private static final String EXISTS = "exists";

	/**
	 * This test simply shows how to create a statically checked tree. The tree grammar for this test is defined in
	 * package eu.rinfrastructures.genericstest.sample.tree and it contains resource payloads defined in package
	 * eu.rinfrastructures.genericstest.sample.resources
	 */
	@Test
	public void simpleTest() {
		final MyTreeNode<SampleResource, ChildResource, FirstChild> root = new RootTree(new SampleResource());

		final FirstChild ch1 = root.addChild(new ChildResource());
		assertNotNull(EXISTS, ch1);
		
		ch1.addChild(new SubResource());
		ch1.addChild(new SubResource());

		final FirstChild ch2 = root.addChild(new ChildResource());
		assertNotNull(EXISTS, ch2);
		
		ch2.addChild(new SubResource());
		ch2.addChild(new SubResource());
		ch2.addChild(new SubResource());
		
	}

	@Test
	public void typeSafeness() {
		final MyTreeNode<SampleResource, ChildResource, FirstChild> root = new RootTree(new SampleResource());

		final FirstChild ch1 = root.addChild(new ChildResource());
		assertNotNull(EXISTS, ch1);
		
		ch1.addChild(new SubResource());

		// ILLEGAL:
		//c1.addChild(new SampleResource());
	}

	@Test
	public void cascade() {
		final MyTreeNode<SampleResource, ChildResource, FirstChild> root = new RootTree(new SampleResource());
		assertNotNull(EXISTS, root);
		
		root.addChild(new ChildResource()).appendChild(new SubResource()).appendChild(new SubResource()).addChild(new SubResource());

	}
}
