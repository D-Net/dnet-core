package eu.dnetlib.miscutils.hstree.sample.tree;

import eu.dnetlib.miscutils.hstree.NilTreeNode;
import eu.dnetlib.miscutils.hstree.sample.resources.SubResource;

public class SecondChild extends MyTreeNode<SubResource, Void, NilTreeNode<MyTreeVisitor>> {
	@Override
	public void accept(final MyTreeVisitor visitor) {
		visitor.visit(getResource());
	}

}
