package eu.dnetlib.miscutils.hstree.sample.wrongtree;

import eu.dnetlib.miscutils.hstree.TreeNode;
import eu.dnetlib.miscutils.hstree.sample.resources.ChildResource;
import eu.dnetlib.miscutils.hstree.sample.resources.SubResource;
import eu.dnetlib.miscutils.hstree.sample.tree.MyLoggingVisitor;

public class WrongFirstChild extends TreeNode<ChildResource, SubResource, MyLoggingVisitor, WrongSecondChild> { // NOPMD

}
