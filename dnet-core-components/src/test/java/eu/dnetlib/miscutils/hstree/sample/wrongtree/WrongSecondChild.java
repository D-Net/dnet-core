package eu.dnetlib.miscutils.hstree.sample.wrongtree;

import eu.dnetlib.miscutils.hstree.NilTreeNode;
import eu.dnetlib.miscutils.hstree.TreeNode;
import eu.dnetlib.miscutils.hstree.sample.resources.SubResource;
import eu.dnetlib.miscutils.hstree.sample.tree.MyLoggingVisitor;

public class WrongSecondChild extends TreeNode<SubResource, Void, MyLoggingVisitor, NilTreeNode<MyLoggingVisitor>> { // NOPMD

}
