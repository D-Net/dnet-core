package eu.dnetlib.miscutils.hstree;

import static org.junit.Assert.*; // NOPMD

import org.junit.Before;
import org.junit.Test;

import eu.dnetlib.miscutils.hstree.sample.resources.ChildResource;
import eu.dnetlib.miscutils.hstree.sample.resources.SampleResource;
import eu.dnetlib.miscutils.hstree.sample.resources.SubResource;
import eu.dnetlib.miscutils.hstree.sample.tree.FirstChild;
import eu.dnetlib.miscutils.hstree.sample.tree.MyComputingVisitor;
import eu.dnetlib.miscutils.hstree.sample.tree.MyLoggingVisitor;
import eu.dnetlib.miscutils.hstree.sample.tree.MyTreeNode;
import eu.dnetlib.miscutils.hstree.sample.tree.MyTreeVisitor;
import eu.dnetlib.miscutils.hstree.sample.tree.RootTree;

public class VisitorTest {

	transient RootTree root = null;
	transient MyTreeVisitor loggingVisitor = null;
	transient MyComputingVisitor computingVisitor = null;

	@Before
	public void setUp() throws Exception {
		root = new RootTree(new SampleResource());

		root.addChild(new ChildResource()).appendChild(new SubResource()).appendChild(new SubResource()).addChild(new SubResource());

		loggingVisitor = new MyLoggingVisitor();
		computingVisitor = new MyComputingVisitor();
	}

	@Test
	public void testVisit() {
		final MyTreeNode<SampleResource, ChildResource, FirstChild> root = new RootTree(new SampleResource());
		assertNotNull("exists", root);

		final FirstChild ch1 = root.addChild(new ChildResource());
		ch1.addChild(new SubResource());
		ch1.addChild(new SubResource());

		final FirstChild ch2 = root.addChild(new ChildResource());
		ch2.addChild(new SubResource());
		ch2.addChild(new SubResource());
		ch2.addChild(new SubResource());

		root.breadthFirst(loggingVisitor);
	}

	@Test
	public void testVisit2() {
		final MyTreeNode<SampleResource, ChildResource, FirstChild> root = new RootTree(new SampleResource());
		assertNotNull("exists", root);
		
		root.addChild(new ChildResource()).appendChild(new SubResource()).appendChild(new SubResource()).addChild(new SubResource());
		root.appendChild(new ChildResource()).appendChild(new ChildResource()).appendChild(new ChildResource()).addChild(new ChildResource()).addChild(
				new SubResource());

		root.breadthFirst(loggingVisitor);
	}

	@Test
	public void testVisit3() {
		final MyTreeNode<SampleResource, ChildResource, FirstChild> root = new RootTree(new SampleResource());
		assertNotNull("exists", root);

		root.addChild(new ChildResource()).appendChild(new SubResource()).appendChild(new SubResource()).addChild(new SubResource());
		root.appendChild(new ChildResource()).appendChild(new ChildResource()).appendChild(new ChildResource()).addChild(new ChildResource()).addChild(
				new SubResource());

		root.depthFirst(loggingVisitor);
	}

	@Test
	public void testMyVisitor() {
		assertNotNull("dummy", root);
		
		root.accept(loggingVisitor);
		root.getChildren().get(0).accept(loggingVisitor);
	}

	@Test
	public void testComputingVisitor() {
		assertNotNull("dummy", root);
		
		root.breadthFirst(computingVisitor);
		assertEquals("check count", 113, computingVisitor.getCount());
	}
}
