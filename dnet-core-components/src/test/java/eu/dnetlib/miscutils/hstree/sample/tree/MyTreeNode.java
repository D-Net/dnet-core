package eu.dnetlib.miscutils.hstree.sample.tree;

import eu.dnetlib.miscutils.hstree.TreeNode;

public class MyTreeNode<T, N, C extends TreeNode<N, ?, MyTreeVisitor, ?>> extends TreeNode<T, N, MyTreeVisitor, C> {

	public MyTreeNode() {
		// no action
	}

	public MyTreeNode(final T resource) {
		super(resource);
	}
}
