package eu.dnetlib.miscutils.hstree.sample.tree;

import eu.dnetlib.miscutils.hstree.sample.resources.ChildResource;
import eu.dnetlib.miscutils.hstree.sample.resources.SampleResource;

public class RootTree extends MyTreeNode<SampleResource, ChildResource, FirstChild> {

	public RootTree(final SampleResource resource) {
		super(resource);
	}

	@Override
	public void accept(final MyTreeVisitor visitor) {
		visitor.visit(getResource());
	}
}
