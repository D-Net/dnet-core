package eu.dnetlib.miscutils.hstree.sample.tree;

import eu.dnetlib.miscutils.hstree.sample.resources.ChildResource;
import eu.dnetlib.miscutils.hstree.sample.resources.SubResource;

public class FirstChild extends MyTreeNode<ChildResource, SubResource, SecondChild> {
	@Override
	public void accept(final MyTreeVisitor visitor) {
		visitor.visit(getResource());
	}

}
