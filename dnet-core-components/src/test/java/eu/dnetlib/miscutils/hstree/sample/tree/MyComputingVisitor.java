package eu.dnetlib.miscutils.hstree.sample.tree;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.dnetlib.miscutils.hstree.sample.resources.ChildResource;
import eu.dnetlib.miscutils.hstree.sample.resources.SampleResource;
import eu.dnetlib.miscutils.hstree.sample.resources.SubResource;

public class MyComputingVisitor implements MyTreeVisitor {
	private static final Log log = LogFactory.getLog(MyComputingVisitor.class); // NOPMD by marko on 11/24/08 5:02 PM

	int count;

	@Override
	public void visit(final SampleResource resource) {
		log.info("increasing sampleresource");
		count += 100;
	}

	@Override
	public void visit(final ChildResource resource) {
		log.info("increasing childresource");
		count += 10;
	}

	@Override
	public void visit(final SubResource resource) {
		log.info("increasing subresource");
		count += 1;
	}

	public int getCount() {
		return count;
	}

	public void setCount(final int count) {
		this.count = count;
	}

}
