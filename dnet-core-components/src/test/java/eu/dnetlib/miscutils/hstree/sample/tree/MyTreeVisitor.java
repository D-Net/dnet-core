package eu.dnetlib.miscutils.hstree.sample.tree;

import eu.dnetlib.miscutils.hstree.sample.resources.ChildResource;
import eu.dnetlib.miscutils.hstree.sample.resources.SampleResource;
import eu.dnetlib.miscutils.hstree.sample.resources.SubResource;

public interface MyTreeVisitor {

	void visit(SampleResource resource);

	void visit(ChildResource resource);

	void visit(SubResource resource);

}