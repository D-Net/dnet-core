package eu.dnetlib.miscutils.functional.xml;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.xml.transform.TransformerFactory;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.io.ClassPathResource;

@RunWith(MockitoJUnitRunner.class)
public class ApplyXsltTest {

	TransformerFactory tf = TransformerFactory.newInstance();
	
	private static final Class<?> transformerClass =  net.sf.saxon.TransformerFactoryImpl.class;
	
	@Test
	public void testTransformerFactoryType() {
		assertEquals(tf.getClass(), transformerClass);
	}
	
	@Test
	public void applyXslt() throws IOException {
		String record = IOUtils.toString((new ClassPathResource("/eu/dnetlib/miscutils/functional/xml/sampleRecord.xml")).getInputStream());
		String layout = IOUtils.toString((new ClassPathResource("/eu/dnetlib/miscutils/functional/xml/sampleIndexLayout.xml")).getInputStream());
		String indexXsltOfXslt = IOUtils.toString((new ClassPathResource("/eu/dnetlib/miscutils/functional/xml/layoutToRecordStylesheet.xsl")).getInputStream());
		ApplyXslt applyXslt = new ApplyXslt(IOUtils.toString((new ClassPathResource("/eu/dnetlib/miscutils/functional/xml/recordStylesheet.xsl")).getInputStream()));
		ApplyXslt xslt1 = new ApplyXslt(indexXsltOfXslt);
		String indexXslt = xslt1.evaluate(layout);
		ApplyXslt xslt2 = new ApplyXslt(indexXslt);
		String response = xslt2.evaluate(record);
		System.out.println(response);
		
	}

}
