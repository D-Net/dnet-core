package eu.dnetlib.miscutils.functional;

import eu.dnetlib.miscutils.functional.UnaryFunction;

public class UnsafeUnaryFunction implements UnaryFunction<String, Integer> {
	@Override
	public String evaluate(Integer arg) {
		return String.valueOf(arg);
	}
}
