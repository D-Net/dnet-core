package eu.dnetlib.miscutils.functional.string;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

public class SanitizerTest {

	@Test
	public void testIdentity() {
		verifyIdentity("+-@°#()!$%&/()?' ");
		verifyIdentity("asça");
		verifyIdentity("qwéèrtç°§");
		verifyIdentity("AÁaáCĆcćEÉeéIÍiíLĹlĺNŃnńOÓoóRŔrŕSŚsśUÚuúYÝyýZŹzź");
		verifyIdentity("AÂaâCĈcĉEÊeêGĜgĝHĤhĥIÎiîJĴjĵOÔoôSŜsŝUÛuûWŴwŵYŶyŷ");
		verifyIdentity("CÇcçGĢgģKĶkķLĻlļNŅnņRŖrŗSŞsşTŢtţ");
		verifyIdentity("ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩΆΈΉΊΌΎΏΪΫαβγδεζηθικλμνξοπρσςτυφχψωάέήίόύώΐΰ");
		verifyIdentity("АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ");
		verifyIdentity("абвгдеёжзийклмнопрстуфхцчшщъыьэюя");
		verifyIdentity("ـآ آ   ـا ا   بـبـب ب   تـتـت ت   ثـثـث ث   جـجـج ج   حـحـح ح   خـخـخ خ   ـد د   ـذ ذ   ـر ر   ـز ز   سـسـس س"); 
	}

	@Test
	public void testInvalid() throws Exception {
		StringWriter sw = new StringWriter();

		IOUtils.copy(getClass().getResourceAsStream("invalid.txt"), sw); 

		String pre = sw.toString();
		String post = Sanitizer.sanitize(pre); 

		assertFalse(pre.equals(post));	
	}

	private void verifyIdentity(String s) {
		assertEquals(s, Sanitizer.sanitize(s));
	}

}
