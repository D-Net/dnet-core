package eu.dnetlib.miscutils.functional.string;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

public class EscapeUnescapeTest {

	@Test
	public void testEscape() throws IOException {
		String xml = getRecord();
		System.out.println(xml);
		System.out.println("##############");
		String escaped = new EscapeHtml().evaluate(xml);
		System.out.println(escaped);

		String unescaped = new UnescapeHtml().evaluate(escaped);
		System.out.println("##############");
		System.out.println(unescaped);

	}

	private String getRecord() throws IOException {
		InputStream is = getClass().getResourceAsStream("record.xml");
		StringWriter sw = new StringWriter();
		IOUtils.copy(is, sw);

		return sw.toString();
	}

}
