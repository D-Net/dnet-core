/**
 * 
 */
package eu.dnetlib.miscutils.dom4j;

import static org.junit.Assert.assertEquals;

import java.io.StringReader;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.junit.Test;

/**
 * @author marko
 * 
 */
public class XPathHelperTest {

	/**
	 * Test method for {@link eu.dnetlib.miscutils.dom4j.XPathHelper#selectElements(org.dom4j.Node, java.lang.String)}.
	 * 
	 * @throws DocumentException
	 */
	@Test
	public void testSelectElements() throws DocumentException {
		final String xmlSource = "<root><child/>xxxx<child/>xxxx</root>";

		final Document document = new SAXReader().read(new StringReader(xmlSource));

		for (Element el : XPathHelper.selectElements(document, "//child")) {
			assertEquals("check elements", el.asXML(), "<child/>");
		}
	}

}
