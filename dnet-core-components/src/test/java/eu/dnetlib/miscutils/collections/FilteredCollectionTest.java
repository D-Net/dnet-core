package eu.dnetlib.miscutils.collections;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import eu.dnetlib.miscutils.functional.CompositeUnaryFunction;
import eu.dnetlib.miscutils.functional.UnaryFunction;

public class FilteredCollectionTest {

	@Test
	public void testFilterComposition() {
		final List<Integer> list = new ArrayList<Integer>();
		list.add(1);
		list.add(4);
		list.add(8);

		final Filter<Integer> fi1 = new Filter<Integer>() { // NOPMD
			@Override
			public Boolean evaluate(final Integer arg) {
				return arg < 8;
			}
		};

		final UnaryFunction<Integer, Integer> square = new UnaryFunction<Integer, Integer>() {
			@Override
			public Integer evaluate(final Integer arg) {
				return arg * arg;
			}
		};

		final FilteredCollection<Integer> fic = new FilteredCollection<Integer>(list, fi1);
		for (Integer el : fic)
			assertTrue("check array", el < 8);

		final CompositeUnaryFunction<Boolean, Integer> cf1 = new CompositeUnaryFunction<Boolean, Integer>(fi1);
		final FilteredCollection<Integer> fc2 = new FilteredCollection<Integer>(list, cf1.of(square));
		for (Integer el : fc2)
			assertTrue("check array", el < 8);

	}

}
