package eu.dnetlib.miscutils.collections;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class AffixCollectionTest {

	@Test
	public void testIterator() {
		final List<String> list = new ArrayList<String>();
		list.add("100");

		final AffixCollection mapped = new AffixCollection(list, "_test");
		for (String el : mapped)
			assertEquals("check array", "100_test", el);
	}

}
