package eu.dnetlib.miscutils.collections;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import eu.dnetlib.miscutils.functional.UnaryFunction;

public class MappedCollectionTest {

	@Test
	public void testIterator() {
		final List<Integer> list = new ArrayList<Integer>();
		list.add(100);

		final MappedCollection<String, Integer> mapped = new MappedCollection<String, Integer>(list, new UnaryFunction<String, Integer>() { //NOPMD
			@Override
			public String evaluate(final Integer arg) {
				return arg.toString();
			}
		});
		for (String el : mapped)
			assertEquals("check array", el, "100");
	}

}
