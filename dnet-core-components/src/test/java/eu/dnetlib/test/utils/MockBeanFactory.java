package eu.dnetlib.test.utils;


import static org.mockito.Mockito.mock;

import org.springframework.beans.factory.FactoryBean;

/**
 * Return a mockito mock for a given class.
 * This class should be updated according to new Spring4 factory Bean
 *
 * @author marko
 *
 */
@Deprecated 
public class MockBeanFactory implements FactoryBean {

	/**
	 * class to mock.
	 */
	private Class<?> clazz;

	/**
	 * {@inheritDoc}
	 * @see org.springframework.beans.factory.FactoryBean#getObject()
	 */
	public Object getObject() throws Exception {
		return mock(clazz);
	}

	/**
	 * {@inheritDoc}
	 * @see org.springframework.beans.factory.FactoryBean#getObjectType()
	 */
	public Class<?> getObjectType() {
		return clazz;
	}

	/**
	 * {@inheritDoc}
	 * @see org.springframework.beans.factory.FactoryBean#isSingleton()
	 */
	public boolean isSingleton() {
		return true;
	}

	public Class<?> getClazz() {
		return clazz;
	}

	public void setClazz(final Class<?> clazz) {
		this.clazz = clazz;
	}

}
