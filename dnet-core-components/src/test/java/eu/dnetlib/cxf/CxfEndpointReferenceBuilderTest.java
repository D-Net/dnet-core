package eu.dnetlib.soap.cxf;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.HashMap;

import javax.xml.namespace.QName;
import javax.xml.ws.EndpointReference;

import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.service.Service;
import org.apache.cxf.service.model.EndpointInfo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * test the CxfEndpointReferenceBuilder.
 *
 * @author marko
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class CxfEndpointReferenceBuilderTest {

	/**
	 * service mock.
	 */
	@Mock
	private Service service;

	/**
	 * endpoint info mock.
	 */
	@Mock
	private EndpointInfo endpointInfo;

	/**
	 * endpoint mock.
	 */
	@Mock
	private Endpoint endpoint;

	/**
	 * object under test.
	 */
	private CxfEndpointReferenceBuilder builder;

	/**
	 * initialize object under test and prepare a commmon endpoint mock.
	 */
	@Before
	public void setUp() {
		builder = new CxfEndpointReferenceBuilder();
		when(service.getName()).thenReturn(new QName("http://my.test", "TestService"));

		when(endpoint.getEndpointInfo()).thenReturn(endpointInfo);
		when(endpoint.getService()).thenReturn(service);
		when(endpointInfo.getAddress()).thenReturn("http://localhost/something");
		when(endpointInfo.getName()).thenReturn(new QName("http://my.test", "TestServiceEndpoint"));
	}

	/**
	 * test.
	 */
	@Test
	public void testGetEndpointReference() {

		final EndpointReference epr = builder.getEndpointReference(endpoint);
		assertTrue("check serialization", epr.toString().contains("TestServiceEndpoint"));
		assertTrue("check serialization", epr.toString().contains("http://localhost/something"));
		assertTrue("check serialization", epr.toString().contains("http://my.test"));

	}

	/**
	 * test method.
	 */
	@Test
	public void testGetEndpointReferenceMetadata() {
		final HashMap<String, Object> defaultMetadata = new HashMap<String, Object>();
		defaultMetadata.put("{http://someuri}somename", "somevalue");
		builder.setDefaultMetadata(defaultMetadata);

		final EndpointReference epr = builder.getEndpointReference(endpoint);
		
		assertTrue("check serialization", epr.toString().contains("somename xmlns=\"http://someuri\""));
		assertTrue("check serialization", epr.toString().contains("xmlns=\"http://someuri\""));
		assertTrue("check serialization", epr.toString().contains("somevalue"));
	}

	protected Service getService() {
		return service;
	}

	protected void setService(final Service service) {
		this.service = service;
	}

	protected EndpointInfo getEndpointInfo() {
		return endpointInfo;
	}

	protected void setEndpointInfo(final EndpointInfo endpointInfo) {
		this.endpointInfo = endpointInfo;
	}

	protected Endpoint getEndpoint() {
		return endpoint;
	}

	protected void setEndpoint(final Endpoint endpoint) {
		this.endpoint = endpoint;
	}

	protected CxfEndpointReferenceBuilder getBuilder() {
		return builder;
	}

	protected void setBuilder(final CxfEndpointReferenceBuilder builder) {
		this.builder = builder;
	}
}
