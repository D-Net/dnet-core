package eu.dnetlib.soap.cxf;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * test StandaloneCxfEndpointReferenceBuilderTest.
 * 
 * @author marko
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class StandaloneCxfEndpointReferenceBuilderTest extends CxfEndpointReferenceBuilderTest {

	/**
	 * some constant url prefix.
	 */
	private static final String HTTP_TEST_COM = "http://test.com";

	/**
	 * class under test.
	 */
	private transient StandaloneCxfEndpointReferenceBuilder builder;

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.soap.cxf.CxfEndpointReferenceBuilderTest#setUp()
	 */
	@Override
	@Before
	public void setUp() {
		super.setUp();
		builder = new StandaloneCxfEndpointReferenceBuilder();
	}

	/**
	 * test computeAddress.
	 */
	@Test
	public void testGetAddress() {
		assertEquals("no base, http", "http://localhost/something", builder.getAddress(getEndpoint()));

		when(getEndpointInfo().getAddress()).thenReturn("/localPath");
		assertEquals("no base, no http", "/localPath", builder.getAddress(getEndpoint()));

		builder.setBaseAddress("http://somebase");
		assertEquals("base, no http", "http://somebase/localPath", builder.getAddress(getEndpoint()));
	}

	/**
	 * make code coverage happy.
	 */
	@Test
	public void testGetBaseAddress() {
		builder.setBaseAddress(HTTP_TEST_COM);
		assertEquals("testing setter", HTTP_TEST_COM, builder.getBaseAddress());
	}

	/**
	 * make code coverage happy.
	 */
	@Test
	public void testSetBaseAddress() {
		builder.setBaseAddress(HTTP_TEST_COM);
		assertEquals("testing setter", HTTP_TEST_COM, builder.getBaseAddress());
	}

}
