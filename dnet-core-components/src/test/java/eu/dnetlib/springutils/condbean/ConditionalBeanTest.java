package eu.dnetlib.springutils.condbean;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class ConditionalBeanTest {

	private static final int FIVE = 5;

	private static final int THREE = 3;

	@Autowired
	@Qualifier("elementTest")
	transient TestBean elementBean;
	
	@Autowired
	@Qualifier("elementTest")
	transient TestBean attributeBean;
	
	@Autowired
	@Qualifier("elementTest2")
	transient TestBean boolBean1;
	
	@Autowired
	@Qualifier("elementTest2")
	transient TestBean boolBean2;
	
	@Autowired
	@Qualifier("elementTest3")
	transient TestBean boolBean3;
	
	@Autowired
	@Qualifier("elementTest3")
	transient TestBean boolBean4;
	
	
	/**
	 * required to be "BeforeClass" because otherwise the spring context will not see it.
	 */
	@BeforeClass
	public static void setProperties() {
		System.setProperty("some.existing.property", "pippo");
	}
	
	@Test
	public void element() {
		assertEquals("element", 1, elementBean.getValue());
	}
	
	@Test
	public void attribute() {
		assertEquals("attribute", 1, attributeBean.getValue());
	}
	
	@Test
	public void bool1() {
		assertEquals("boolean 1", THREE, boolBean1.getValue());
	}
	
	@Test
	public void bool2() {
		assertEquals("boolean 2", THREE, boolBean2.getValue());
	}
	
	@Test
	public void bool3() {
		assertEquals("boolean 3", FIVE, boolBean3.getValue());
	}
	
	@Test
	public void bool4() {
		assertEquals("boolean 4", FIVE, boolBean4.getValue());
	}

}
