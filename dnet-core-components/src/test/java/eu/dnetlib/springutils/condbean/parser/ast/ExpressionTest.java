package eu.dnetlib.springutils.condbean.parser.ast;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class ExpressionTest {
	
	private AbstractTerminal term1;
	private AbstractExpression exp1;
	private AbstractBinaryExpression exp2;

	@Test
	public void testExpression() {
		term1 = new IdentifierNode("pippo");
		exp1 = new NotExpression(term1);
		exp2 = new OrExpression(term1, exp1);
		
		assertEquals("check expression", exp2.getLeft(), term1);
		
	}

	public AbstractTerminal getTerm1() {
		return term1;
	}

	public void setTerm1(final AbstractTerminal term1) {
		this.term1 = term1;
	}

	public AbstractExpression getExp1() {
		return exp1;
	}

	public void setExp1(final AbstractExpression exp1) {
		this.exp1 = exp1;
	}

	public AbstractBinaryExpression getExp2() {
		return exp2;
	}

	public void setExp2(final AbstractBinaryExpression exp2) {
		this.exp2 = exp2;
	}
	
	
	
}
