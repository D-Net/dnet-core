package eu.dnetlib.springutils.condbean.parser.ast;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

public class NotExpressionTest {

	private NotExpression expr;

	private AbstractExpression expression;

	@Before
	public void setUp() throws Exception {
		expression = mock(AbstractExpression.class);
		expr = new NotExpression(expression);
	}

	@Test
	public void testTrue() {
		when(expression.evaluate()).thenReturn(true);
		assertFalse("check expression", (Boolean) expr.evaluate());
	}

	@Test
	public void testFalse() {
		when(expression.evaluate()).thenReturn(false);
		assertTrue("check expression", (Boolean) expr.evaluate());
	}

	public NotExpression getExpr() {
		return expr;
	}

	public void setExpr(final NotExpression expr) {
		this.expr = expr;
	}

	public AbstractExpression getExpression() {
		return expression;
	}

	public void setExpression(final AbstractExpression expression) {
		this.expression = expression;
	}

}
