package eu.dnetlib.springutils.condbean.parser.ast;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

public class RelationalExpressionTest {

	private static final String CHECK_EXPRESSION = "check expression";

	private RelationalExpression expr;

	private AbstractExpression left, right;

	@Before
	public void setUp() throws Exception {
		left = mock(AbstractExpression.class);
		right = mock(AbstractExpression.class);
		expr = new RelationalExpression(left, right, "<=");
	}

	@Test
	public void testString() {
		when(left.evaluate()).thenReturn("pippo");
		when(right.evaluate()).thenReturn("pippo");
		assertTrue(CHECK_EXPRESSION, (Boolean) expr.evaluate());
	}

	@Test
	public void testStringlex1() {
		when(left.evaluate()).thenReturn("pippo1");
		when(right.evaluate()).thenReturn("mippo");
		assertFalse(CHECK_EXPRESSION, (Boolean) expr.evaluate());
	}

	@Test
	public void testStringlex2() {
		when(left.evaluate()).thenReturn("pippo");
		when(right.evaluate()).thenReturn("rippo");
		assertTrue(CHECK_EXPRESSION, (Boolean) expr.evaluate());
	}

	@Test
	public void testInt() {
		when(left.evaluate()).thenReturn(1);
		when(right.evaluate()).thenReturn(1);
		assertTrue(CHECK_EXPRESSION, (Boolean) expr.evaluate());
	}

	@Test
	public void testIntString() {
		when(left.evaluate()).thenReturn("1");
		when(right.evaluate()).thenReturn(1);
		assertFalse(CHECK_EXPRESSION, (Boolean) expr.evaluate());
	}

	public RelationalExpression getExpr() {
		return expr;
	}

	public void setExpr(final RelationalExpression expr) {
		this.expr = expr;
	}

	public AbstractExpression getLeft() {
		return left;
	}

	public void setLeft(final AbstractExpression left) {
		this.left = left;
	}

	public AbstractExpression getRight() {
		return right;
	}

	public void setRight(final AbstractExpression right) {
		this.right = right;
	}

}
