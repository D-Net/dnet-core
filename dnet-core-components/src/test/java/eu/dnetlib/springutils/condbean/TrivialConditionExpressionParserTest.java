package eu.dnetlib.springutils.condbean;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TrivialConditionExpressionParserTest {

	transient TrivialConditionExpressionParser parser = new TrivialConditionExpressionParser();  
	
	@BeforeClass
	public static void setUpProperties() {
		System.setProperty("trivial.condition.true", "value");
	}
	
	@Before
	public void setUp() {
		parser.setFinder(new SystemPropertiesFinder());
	}
	
	@Test
	public void testExpressionValue() {
		assertTrue("check true condition", parser.expressionValue("trivial.condition.true"));
		assertFalse("check false condition", parser.expressionValue("trivial.condition.false"));
	}

	@Test
	public void testGetProperty() {
		assertEquals("check true condition", "value", parser.getProperty("trivial.condition.true"));
		assertEquals("check false condition", null, parser.getProperty("trivial.condition.false"));
	}

}
