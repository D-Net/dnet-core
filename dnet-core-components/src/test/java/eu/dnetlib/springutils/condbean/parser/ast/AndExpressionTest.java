package eu.dnetlib.springutils.condbean.parser.ast;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

public class AndExpressionTest {

	private static final String CHECK_EXPRESSION = "check expression";
	private AndExpression expr;

	private AbstractExpression left, right;

	@Before
	public void setUp() throws Exception {
		left = mock(AbstractExpression.class);
		right = mock(AbstractExpression.class);
		expr = new AndExpression(left, right);
	}

	@Test
	public void testTrue() {
		when(left.evaluate()).thenReturn(true);
		when(right.evaluate()).thenReturn(true);
		assertTrue(CHECK_EXPRESSION, (Boolean) expr.evaluate());
	}

	@Test
	public void testFalse() {
		when(left.evaluate()).thenReturn(true);
		when(right.evaluate()).thenReturn(false);
		assertFalse(CHECK_EXPRESSION, (Boolean) expr.evaluate());
	}

	@Test
	public void testOneNull() {
		when(left.evaluate()).thenReturn(null);
		when(right.evaluate()).thenReturn(1);
		assertFalse(CHECK_EXPRESSION, (Boolean) expr.evaluate());
	}

	@Test
	public void testBothNull() {
		when(left.evaluate()).thenReturn(null);
		when(right.evaluate()).thenReturn(null);
		assertFalse(CHECK_EXPRESSION, (Boolean) expr.evaluate());
	}

	public AndExpression getExpr() {
		return expr;
	}

	public void setExpr(final AndExpression expr) {
		this.expr = expr;
	}

	public AbstractExpression getLeft() {
		return left;
	}

	public void setLeft(final AbstractExpression left) {
		this.left = left;
	}

	public AbstractExpression getRight() {
		return right;
	}

	public void setRight(final AbstractExpression right) {
		this.right = right;
	}

}
