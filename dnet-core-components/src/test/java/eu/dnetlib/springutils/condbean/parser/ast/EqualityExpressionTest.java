package eu.dnetlib.springutils.condbean.parser.ast;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

public class EqualityExpressionTest {

	private static final String CHECK_EXPRESSION = "check expression";

	private EqualityExpression expr;

	private AbstractExpression left, right;

	@Before
	public void setUp() throws Exception {
		left = mock(AbstractExpression.class);
		right = mock(AbstractExpression.class);
		expr = new EqualityExpression(left, right, "==");

	}

	@Test
	public void testEquals() {
		when(left.evaluate()).thenReturn("pippo");
		when(right.evaluate()).thenReturn("pippo");
		assertTrue(CHECK_EXPRESSION, (Boolean) expr.evaluate());
	}

	@Test
	public void testNotEquals() {
		when(left.evaluate()).thenReturn("pippo1");
		when(right.evaluate()).thenReturn("puppo");
		assertFalse(CHECK_EXPRESSION, (Boolean) expr.evaluate());
	}

	@Test
	public void testInteger() {
		when(left.evaluate()).thenReturn("pippo1");
		when(right.evaluate()).thenReturn(1);
		assertFalse(CHECK_EXPRESSION, (Boolean) expr.evaluate());
	}

	@Test
	public void testOneNull() {
		when(left.evaluate()).thenReturn(null);
		when(right.evaluate()).thenReturn(1);
		assertFalse(CHECK_EXPRESSION, (Boolean) expr.evaluate());
	}

	@Test
	public void testBothNull() {
		when(left.evaluate()).thenReturn(null);
		when(right.evaluate()).thenReturn(null);
		assertTrue(CHECK_EXPRESSION, (Boolean) expr.evaluate());
	}

	@Test
	public void testIntString() {
		when(left.evaluate()).thenReturn(1);
		when(right.evaluate()).thenReturn("1");
		assertFalse(CHECK_EXPRESSION, (Boolean) expr.evaluate());
	}

	@Test
	public void testIntChar() {
		when(left.evaluate()).thenReturn(1);
		when(right.evaluate()).thenReturn('1');
		assertFalse(CHECK_EXPRESSION, (Boolean) expr.evaluate());
	}

	@Test
	public void testString() {
		when(left.evaluate()).thenReturn("2");
		when(right.evaluate()).thenReturn("3");
		assertFalse(CHECK_EXPRESSION, (Boolean) expr.evaluate());
	}

	public EqualityExpression getExpr() {
		return expr;
	}

	public void setExpr(final EqualityExpression expr) {
		this.expr = expr;
	}

	public AbstractExpression getLeft() {
		return left;
	}

	public void setLeft(final AbstractExpression left) {
		this.left = left;
	}

	public AbstractExpression getRight() {
		return right;
	}

	public void setRight(final AbstractExpression right) {
		this.right = right;
	}
}
