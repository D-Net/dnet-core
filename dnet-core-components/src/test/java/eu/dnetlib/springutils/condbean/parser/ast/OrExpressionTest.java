package eu.dnetlib.springutils.condbean.parser.ast;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

public class OrExpressionTest {
	private static final String CHECK_EXPRESSION = "check expression";

	private OrExpression expr;

	private AbstractExpression left, right;

	@Before
	public void setUp() throws Exception {
		left = mock(AbstractExpression.class);
		right = mock(AbstractExpression.class);
		expr = new OrExpression(left, right);
	}

	@Test
	public void testTrue() {
		when(left.evaluate()).thenReturn(true);
		when(right.evaluate()).thenReturn(true);
		assertTrue(CHECK_EXPRESSION, (Boolean) expr.evaluate());
	}

	@Test
	public void testFalse() {
		when(left.evaluate()).thenReturn(true);
		when(right.evaluate()).thenReturn(false);
		assertTrue(CHECK_EXPRESSION, (Boolean) expr.evaluate());
	}

	@Test
	public void testOneNull() {
		when(left.evaluate()).thenReturn(null);
		when(right.evaluate()).thenReturn(1);
		assertTrue(CHECK_EXPRESSION, (Boolean) expr.evaluate());
	}

	@Test
	public void testBothNull() {
		when(left.evaluate()).thenReturn(null);
		when(right.evaluate()).thenReturn(null);
		assertFalse(CHECK_EXPRESSION, (Boolean) expr.evaluate());
	}

	public OrExpression getExpr() {
		return expr;
	}

	public void setExpr(final OrExpression expr) {
		this.expr = expr;
	}

	public AbstractExpression getLeft() {
		return left;
	}

	public void setLeft(final AbstractExpression left) {
		this.left = left;
	}

	public AbstractExpression getRight() {
		return right;
	}

	public void setRight(final AbstractExpression right) {
		this.right = right;
	}

}
