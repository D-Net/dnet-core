package eu.dnetlib.springutils.template;

public class TestBean {
	private int value;

	private String address;
	
	private String prop;
	
	private TestBean next;
	
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public TestBean getNext() {
		return next;
	}

	public void setNext(TestBean next) {
		this.next = next;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getProp() {
		return prop;
	}

	public void setProp(String prop) {
		this.prop = prop;
	}
}
