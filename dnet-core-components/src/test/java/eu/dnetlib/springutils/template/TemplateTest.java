package eu.dnetlib.springutils.template;


import static org.junit.Assert.*; // NOPMD

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class TemplateTest {
	
	@Resource(name = "test")
	private transient TestBean testBean;
	
	@Resource(name = "testDefault1")
	private transient TestBean testDefault1;
	
	@Resource(name = "testDefault2")
	private transient TestBean testDefault2;
	
	@Resource(name = "testDefault3")
	private transient TestBean testDefault3;
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testTemplate() {
		assertEquals("check property value expansion", "expandedValue", testBean.getAddress());
		assertEquals("check bean reference", "tail-expandedValue", testBean.getNext().getAddress());
	}
	
	@Test
	public void testMultipleInstances() {
		assertEquals("check expanded value", "expandedValue", testDefault1.getAddress());
		assertEquals("check other value", "otherValue", testDefault2.getAddress());
	}
	
	@Test
	public void testDefault() {
		assertEquals("check expanded value", "expandedValue", testDefault1.getAddress());
		assertEquals("check default value", "defaultValue", testDefault3.getAddress());
	}
	
	@Test
	public void testProp() {
		assertEquals("check property", "test", testBean.getProp());
	}
}
