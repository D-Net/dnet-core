package eu.dnetlib.springutils.template;


import static org.junit.Assert.*; // NOPMD

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Templates should work also if parsed out of order (instantiations before declarations). 
 * 
 * @author marko
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class ReorderTemplateTest {

	@Resource(name = "test")
	private transient TestBean testBean;

	@Test
	public void testOutOfOrder() {
		assertNotNull(testBean);
	}
}
