package eu.dnetlib.springutils.aop;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SimpleBean {
	private static final Log log = LogFactory.getLog(SimpleBean.class); // NOPMD by marko on 11/24/08 5:02 PM

	private boolean initialized = false;
	
	public void init() {
		log.info("initialized");
		System.out.println("initialized");
		initialized = true;
	}

	public boolean isInitialized() {
		return initialized;
	}
}
