package eu.dnetlib.springutils.aop;

import static org.junit.Assert.*;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class MethodInvokingInitializerTest {

	@Resource
	private SimpleBean simpleBean;
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testStart() {
		assertTrue(simpleBean.isInitialized());
	}

}
