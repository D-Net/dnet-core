package eu.dnetlib.springutils.collections;


import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test resource->string transformer.
 * 
 * @author marko
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class ResourceReaderCollectionTest {

	/**
	 * resources filled by spring.
	 */
	@javax.annotation.Resource(name = "resourceList")
	List<Resource> resources;
	
	/**
	 * test resources
	 */
	@Test
	public void testResources() {
		boolean passed = false;
		for(String el : new ResourceReaderCollection(resources)) {
			passed = true;
			assertEquals("content", "test\n", el);
		}
		
		assertTrue("passed", passed);
	}
	
}
