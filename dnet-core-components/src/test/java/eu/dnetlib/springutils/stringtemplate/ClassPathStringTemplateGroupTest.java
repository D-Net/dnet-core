package eu.dnetlib.springutils.stringtemplate;

import static org.junit.Assert.assertEquals;
import java.io.StringReader;
import org.junit.Before;
import org.junit.Test;

public class ClassPathStringTemplateGroupTest {

	private ClassPathStringTemplateGroup group;
	private final transient String templates = "group simple;\nvardef(type,name) ::= \"<type> <name>;\""
			+ "method(type,name,args) ::= <<<type> <name>(<args; separator=\",\">) {  <statements; separator=\"\n\">}>>";;

	@Before
	public void setUp() throws Exception {
		group = new ClassPathStringTemplateGroup(new StringReader(templates));
	}

	@Test
	public void testName() {
		group.setPackage("eu.dnetlib");
		assertEquals("check Path", "/eu/dnetlib/pippo.st", group.getFileNameFromTemplateName("pippo"));
		assertEquals("check Path", "pippo", group.getTemplateNameFromFileName("/eu/dnetlib/pippo.st"));
	}

	public ClassPathStringTemplateGroup getGroup() {
		return group;
	}

	public void setGroup(final ClassPathStringTemplateGroup group) {
		this.group = group;
	}
}
