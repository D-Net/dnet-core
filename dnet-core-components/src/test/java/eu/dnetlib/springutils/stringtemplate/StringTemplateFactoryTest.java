package eu.dnetlib.springutils.stringtemplate;


import static org.junit.Assert.*;

import javax.annotation.Resource;

import org.antlr.stringtemplate.StringTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test a string template factory bean.
 * @author marko
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class StringTemplateFactoryTest {

	@Resource
	private StringTemplate template;

	@Test
	public void testStringTemplate() {
		assertEquals("check template", "test\n", template.toString());
	}

}
