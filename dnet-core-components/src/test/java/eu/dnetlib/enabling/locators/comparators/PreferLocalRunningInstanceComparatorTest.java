package eu.dnetlib.enabling.locators.comparators;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import eu.dnetlib.enabling.locators.ServiceRunningInstance;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PreferLocalRunningInstanceComparatorTest {

	/**
	 * Class Under test.
	 */
	private PreferLocalRunningInstanceComparator comparator;
	
	@Mock
	private ServiceRunningInstance s1;
	@Mock
	private ServiceRunningInstance s2;
	@Mock
	private ServiceRunningInstance s3;
	
	@Before
	public void setUp() throws Exception {
		comparator = new PreferLocalRunningInstanceComparator();
		when(s1.isLocal()).thenReturn(true);
		when(s2.isLocal()).thenReturn(false);
		when(s3.isLocal()).thenReturn(false);
	}

	@Test
	public void testCompare() {
		assertEquals(-1, comparator.compare(s1, s3));
		assertEquals(1, comparator.compare(s3, s1));
		assertEquals(0, comparator.compare(s2, s3));
	}

}
