package eu.dnetlib.enabling.locators.comparators;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import eu.dnetlib.enabling.locators.ServiceRunningInstance;

@RunWith(MockitoJUnitRunner.class)
public class HandledDatastructuresComparatorTest {

	/**
	 * Class Under test.
	 */
	private HandledDatastructuresComparator comparator;
	
	@Mock
	private ServiceRunningInstance s1;
	@Mock
	private ServiceRunningInstance s2;
	@Mock
	private ServiceRunningInstance s3;
	
	@Before
	public void setUp() throws Exception {
		comparator = new HandledDatastructuresComparator();
		when(s1.getHandledDatastructures()).thenReturn(0);
		when(s2.getHandledDatastructures()).thenReturn(0);
		when(s3.getHandledDatastructures()).thenReturn(10);
	}

	@Test
	public void testCompare() {
		assertEquals(0, comparator.compare(s1, s2));
		assertEquals(-1, comparator.compare(s1, s3));
		assertEquals(1, comparator.compare(s3, s1));
	}

}
