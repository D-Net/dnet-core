package eu.dnetlib.enabling.tools;

import static org.junit.Assert.*; // NOPMD
import static org.mockito.Mockito.*; // NOPMD

import java.util.List;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService;
import eu.dnetlib.enabling.resultset.rmi.ResultSetService;

/**
 * Test dynamic service locator.
 *
 * @author marko
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class DynamicServiceLocatorTest {

	/**
	 * instance under test.
	 */
	@Autowired
	private transient DynamicServiceLocator<ResultSetService> serviceLocator;

	/**
	 * list of mock service profiles saved in files.
	 */

	@javax.annotation.Resource(name = "serviceProfiles")
	private transient List<String> serviceUris;

	/**
	 * service resolver mock.
	 */
	private transient ServiceResolver serviceResolver;

	/**
	 * setup.
	 *
	 * @throws ISLookUpException
	 *             mock
	 */
	@Before
	public void setUp() throws ISLookUpException {
//		final ISLookUpService lookupService = mock(ISLookUpService.class);
//		serviceLocator.setLookUpLocator(new StaticServiceLocator<ISLookUpService>(lookupService));

		serviceResolver = mock(ServiceResolver.class);
		serviceLocator.setServiceResolver(serviceResolver);

		final ISLookUpService lookupService = serviceLocator.getLookUpLocator().getService();
		when(lookupService.quickSearchProfile(anyString())).thenReturn(serviceUris);
	}

	/**
	 * test get service.
	 */
	@Test
	public void testGetService() {
		assertNull("dummy", serviceLocator.getService());

		verify(serviceResolver).getService(eq(ResultSetService.class), (W3CEndpointReference) anyObject());
	}

}
