package eu.dnetlib.enabling.tools.registration;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import eu.dnetlib.enabling.is.registry.rmi.ISRegistryService;
import eu.dnetlib.enabling.resultset.rmi.ResultSetService;

public class InterfaceServiceNameResolverCompatibilityTest {

	private transient InterfaceServiceNameResolverCompatibility resolver;

	private interface IndexService {}

	private interface ISomethingService {}

	@Before
	public void setUp() throws Exception {
		resolver = new InterfaceServiceNameResolverCompatibility();
	}

	@Test
	public void testGetNameClassOfQ() {
		assertEquals("IndexService", resolver.getName(IndexService.class));
		assertEquals("SomethingService", resolver.getName(ISomethingService.class));
		assertEquals("ISRegistryService", resolver.getName(ISRegistryService.class));
		assertEquals("ResultSetService", resolver.getName(ResultSetService.class));
	}

}
