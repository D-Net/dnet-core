package eu.dnetlib.functionality.cql.lucene;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import eu.dnetlib.functionality.cql.CqlTranslator;
import eu.dnetlib.functionality.cql.CqlTranslatorImpl;
import eu.dnetlib.functionality.cql.CqlUtils;
import eu.dnetlib.functionality.cql.parse.*;
import org.junit.Before;
import org.junit.Test;
import org.z3950.zing.cql.CQLNode;
import org.z3950.zing.cql.CQLParseException;
import org.z3950.zing.cql.CQLParser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CqlTranslatorImplTest {

	private CqlTranslator translator;

	private Map<String, String> weights;

	@Before
	public void setUp() {
		translator = new CqlTranslatorImpl();

		weights = Maps.newHashMap();
		weights.put("title", "2");
		weights.put("ugo", "1.5");
		weights.put("tag", "3");
	}

	@Test
	public void testToSolr_BOW() throws Exception {

		String cqlQuery = "a b c";
		String luceneQuery = translator.toLucene(cqlQuery);

		printQuery(cqlQuery, luceneQuery);
	}

	@Test
	public void testToSolr_0() throws Exception {

		String cqlQuery = "\"a b x\" and r and \"eng/ita\" or f any \"pippo pluto\" and \"con: duepunti\" not (d < 5 and d > 10)";
		String luceneQuery = translator.toLucene(cqlQuery);

		printQuery(cqlQuery, luceneQuery);
	}

	@Test
	public void testToSolr_00() throws Exception {

		String cqlQuery = "resultdupid exact datacite____::b49fd4e3386d82df348a120cfe43516b OR objidentifier exact datacite____::b49fd4e3386d82df348a120cfe43516b";
		String luceneQuery = translator.toLucene(cqlQuery);

		printQuery(cqlQuery, luceneQuery);
	}

	@Test
	public void testToSolr_1() throws Exception {

		String cqlQuery = "publicationdate =/within \"2000-01-01 2010-01-01\"";
		String luceneQuery = translator.toLucene(cqlQuery);

		printQuery(cqlQuery, luceneQuery);
	}

	@Test
	public void testToSolr_2() throws Exception {
		String query = "(_all=faust AND _all=pippo) AND _all<>cinegiornale";
		Node node = new AndNode(new AndNode(new TermNode("_all", Relation.EQUAL, "faust"), new TermNode("_all", Relation.EQUAL, "pippo")), new TermNode("_all",
				Relation.NOT, "cinegiornale"));

		TranslatedQuery parsed = translator.getTranslatedQuery(query);

		printQuery(query, parsed.asLucene());
		System.out.println("NODE:   " + node.toLucene());
		System.out.println("NODE:   " + node.toString());
		assertEquals(node.toLucene(), parsed.asLucene());
	}

	@Test
	public void testToSolr_3() throws Exception {
		String query = "_all all faust";
		Node node = new TermNode("_all", Relation.ALL, "faust");
		TranslatedQuery parsed = translator.getTranslatedQuery(query);
		printQuery(query, parsed.asLucene());
		assertEquals(node.toLucene(), parsed.asLucene());

		query = "__all all \"caracas roma\"";

		parsed = translator.getTranslatedQuery(query);
		printQuery(query, parsed.asLucene());

		query = "__all all caracas roma";
		parsed = translator.getTranslatedQuery(query);
		printQuery(query, parsed.asLucene());

		query = "__all any caracas roma";
		parsed = translator.getTranslatedQuery(query);
		printQuery(query, parsed.asLucene());

		query = "__all any \"caracas roma\"";
		parsed = translator.getTranslatedQuery(query);
		printQuery(query, parsed.asLucene());

		query = "__all exact caracas roma";
		parsed = translator.getTranslatedQuery(query);
		printQuery(query, parsed.asLucene());
	}

	@Test
	public void testToSolr_4() throws Exception {

		String cqlQuery = "publicationdate =/within \"2000-01-01 2010-01-01\" and title = \"ddd\" and y < 2010 or y <= 2010 or y > 2010 or y >= 2010";
		String luceneQuery = translator.toLucene(cqlQuery);

		printQuery(cqlQuery, luceneQuery);
	}

	@Test
	public void testToSolr_5() throws Exception {

		String cqlQuery = "publicationdate within \"2000-01-01 2010-01-01\"";
		String luceneQuery = translator.toLucene(cqlQuery);

		printQuery(cqlQuery, luceneQuery);
	}

	@Test
	public void testToSolr_6() throws Exception {

		String cqlQuery = "cat sortBy title/sort.descending";
		final TranslatedQuery tr = translator.getTranslatedQuery(cqlQuery);
		printQuery(cqlQuery, tr.asLucene());
		System.out.println(tr.getOptions());

	}

	@Test
	public void testToSolr_7() throws Exception {

		String cqlQuery = "title exact true sortBy title/sort.ascending";
		CQLNode parsed = new CQLParser().parse(cqlQuery);

		TranslatedQuery luceneQuery = translator.getTranslatedQuery(parsed, new IdentityCqlValueTransformerMap());

		assertNotNull(luceneQuery.getOptions().getSort().getField());
		assertNotNull(luceneQuery.getOptions().getSort().getMode());

		assertEquals("title", luceneQuery.getOptions().getSort().getField());
		assertEquals(SortOperation.Mode.asc, luceneQuery.getOptions().getSort().getMode());

		System.out.println("LUCENE: " + luceneQuery.asLucene() + " OPTIONS:" + luceneQuery.getOptions().getSort().getField() + " "
				+ luceneQuery.getOptions().getSort().getMode());

		printQuery(cqlQuery, luceneQuery.asLucene());
	}

	@Test
	public void testToSolr_8() throws Exception {

		String cqlQuery = "__all all \"caracas - roma\"";
		CQLNode parsed = new CQLParser().parse(cqlQuery);
		TranslatedQuery luceneQuery = translator.getTranslatedQuery(parsed, new IdentityCqlValueTransformerMap());

		printQuery(cqlQuery, luceneQuery.asLucene());
	}

	@Test
	public void testToSolr_9() throws Exception {

		String cqlQuery = "__all all \"caracas roma\"";
		CQLNode parsed = new CQLParser().parse(cqlQuery);
		TranslatedQuery luceneQuery = translator.getTranslatedQuery(parsed, new IdentityCqlValueTransformerMap());

		printQuery(cqlQuery, luceneQuery.asLucene());

		cqlQuery = "__all all caracas roma";
		parsed = new CQLParser().parse(cqlQuery);
		luceneQuery = translator.getTranslatedQuery(parsed, new IdentityCqlValueTransformerMap());

		printQuery(cqlQuery, luceneQuery.asLucene());

		cqlQuery = "__all any caracas roma";
		parsed = new CQLParser().parse(cqlQuery);
		luceneQuery = translator.getTranslatedQuery(parsed, new IdentityCqlValueTransformerMap());

		printQuery(cqlQuery, luceneQuery.asLucene());

		cqlQuery = "__all any \"caracas roma\"";
		parsed = new CQLParser().parse(cqlQuery);
		luceneQuery = translator.getTranslatedQuery(parsed, new IdentityCqlValueTransformerMap());

		printQuery(cqlQuery, luceneQuery.asLucene());

		cqlQuery = "__all exact caracas roma";
		parsed = new CQLParser().parse(cqlQuery);
		luceneQuery = translator.getTranslatedQuery(parsed, new IdentityCqlValueTransformerMap());

		printQuery(cqlQuery, luceneQuery.asLucene());
	}

	@Test
	public void testToSolr_10() throws Exception {

		String cqlQuery = "__all all \"caracas - ro*\"";
		CQLNode parsed = new CQLParser().parse(cqlQuery);

		Map<String, List<String>> cqlOptions = Maps.newHashMap();
		BiMap<String, String> aliases = HashBiMap.create();

		cqlOptions.put("wildcard", Lists.newArrayList("true"));
		TranslatedQuery luceneQuery = translator.getTranslatedQuery(parsed, new IdentityCqlValueTransformerMap(), cqlOptions, aliases,
				new HashMap<String, String>());
		printQuery(cqlQuery, luceneQuery.asLucene());

		cqlOptions = Maps.newHashMap();
		luceneQuery = translator.getTranslatedQuery(parsed, new IdentityCqlValueTransformerMap(), cqlOptions, aliases, new HashMap<String, String>());

		printQuery(cqlQuery, luceneQuery.asLucene());
	}

	@Test
	public void testToSolr_11() throws Exception {

		String cqlQuery = "__all <> \"kreutz\" and __all <> \"austria\"";
		CQLNode parsed = new CQLParser().parse(cqlQuery);
		TranslatedQuery luceneQuery = translator.getTranslatedQuery(parsed, new IdentityCqlValueTransformerMap());

		printQuery(cqlQuery, luceneQuery.asLucene());
	}

	@Test
	public void testToSolr_12() throws Exception {

		String cqlQuery = "__all <> \"kreutz\" and __all <> \"austria\" or __all <> \"italia\" and __dsid exact \"wwwww\"";
		CQLNode parsed = new CQLParser().parse(cqlQuery);
		TranslatedQuery luceneQuery = translator.getTranslatedQuery(parsed, new IdentityCqlValueTransformerMap());

		printQuery(cqlQuery, luceneQuery.asLucene());
	}

	@Test
	public void testToSolr_13() throws Exception {

		String cqlQuery = "__all = faust and __all <> cinegiornale";

		CQLNode parsed = new CQLParser().parse(cqlQuery);
		TranslatedQuery luceneQuery = translator.getTranslatedQuery(parsed, new IdentityCqlValueTransformerMap());

		printQuery(cqlQuery, luceneQuery.asLucene());
	}

	@Test
	public void testToSolr_14() throws Exception {

		String cqlQuery = "__all <> cinegiornale and __all = faust";

		CQLNode parsed = new CQLParser().parse(cqlQuery);
		TranslatedQuery luceneQuery = translator.getTranslatedQuery(parsed, new IdentityCqlValueTransformerMap());

		printQuery(cqlQuery, luceneQuery.asLucene());
	}

	@Test
	public void testToSolr_15() throws Exception {

		String cqlQuery = "(asdasd or asfgqwr) and textual";

		CQLNode parsed = new CQLParser().parse(cqlQuery);
		TranslatedQuery luceneQuery = translator.getTranslatedQuery(parsed, new IdentityCqlValueTransformerMap());

		printQuery(cqlQuery, luceneQuery.asLucene());
	}

	@Test
	public void testToSolr_16() throws Exception {

		String cqlQuery = "title = kreutz and subject any \"austria italy\" or tag = pippo and blabla";
		CQLNode parsed = new CQLParser().parse(cqlQuery);

		Map<String, List<String>> options = Maps.newHashMap();
		BiMap<String, String> aliases = HashBiMap.create();
		IdentityCqlValueTransformerMap map = new IdentityCqlValueTransformerMap();

		CQLNode expand = CqlUtils.expand(parsed, weights.keySet());

		TranslatedQuery luceneQuery = translator.getTranslatedQuery(expand, map, options, aliases, weights);

		printQuery(cqlQuery, luceneQuery.asLucene());
	}

	@Test
	public void testToSolr_17() throws Exception {

		String cqlQuery = "a = 1 and b = 2 or c = 3 and blabla";
		CQLNode parsed = new CQLParser().parse(cqlQuery);

		Map<String, List<String>> options = Maps.newHashMap();
		BiMap<String, String> aliases = HashBiMap.create();
		IdentityCqlValueTransformerMap map = new IdentityCqlValueTransformerMap();

		Map<String, String> w = Maps.newHashMap();
		w.put("a", "2");
		w.put("c", "1.5");

		CQLNode expand = CqlUtils.expand(parsed, w.keySet());

		TranslatedQuery luceneQuery = translator.getTranslatedQuery(expand, map, options, aliases, w);

		printQuery(cqlQuery, luceneQuery.asLucene());
	}

	@Test
	public void testToSolr_18() throws Exception {

		String cqlQuery = "a exact 1";
		CQLNode parsed = new CQLParser().parse(cqlQuery);

		Map<String, List<String>> options = Maps.newHashMap();
		BiMap<String, String> aliases = HashBiMap.create();
		aliases.put("a", "b");

		IdentityCqlValueTransformerMap map = new IdentityCqlValueTransformerMap();

		Map<String, String> w = Maps.newHashMap();

		CQLNode expand = CqlUtils.expand(parsed, w.keySet());

		TranslatedQuery luceneQuery = translator.getTranslatedQuery(expand, map, options, aliases, w);

		printQuery(cqlQuery, luceneQuery.asLucene());
	}

	@Test
	public void testToSolr_19() throws Exception {
		String query = "_all<>faust";
		Node node = new TermNode("_all", Relation.NOT, "faust");

		TranslatedQuery parsed = translator.getTranslatedQuery(query);

		printQuery(query, parsed.asLucene());
		assertEquals(node.toLucene(), parsed.asLucene());

		query = "_all <> faust nozze";
		// node = new TermNode("_all", Relation.NOT, "faust nozze");
		parsed = translator.getTranslatedQuery(query);
		printQuery(query, parsed.asLucene());

	}

	@Test
	public void testToSolr_20() throws Exception {

		String query = "(title = ESTUDIO and title = abierto) not (title = mediante)";

		Node node = new NotNode(new AndNode(new TermNode("title", Relation.EQUAL, "ESTUDIO"), new TermNode("title", Relation.EQUAL, "abierto")), new TermNode(
				"title", Relation.EQUAL, "mediante"));

		TranslatedQuery parsed = translator.getTranslatedQuery(query);

		printQuery(query, parsed.asLucene());
		assertEquals(node.toLucene(), parsed.asLucene());
	}

	@Test
	public void testToSolr_21() throws Exception {

		String query = "(title = ESTUDIO and title = abierto) not (title = mediante or title = verde)";

		Node node = new NotNode(new AndNode(new TermNode("title", Relation.EQUAL, "ESTUDIO"), new TermNode("title", Relation.EQUAL, "abierto")), new OrNode(
				new TermNode("title", Relation.EQUAL, "mediante"), new TermNode("title", Relation.EQUAL, "verde")));

		TranslatedQuery parsed = translator.getTranslatedQuery(query);

		printQuery(query, parsed.asLucene());
		assertEquals(node.toLucene(), parsed.asLucene());
	}

	@Test
	public void testToSolr_22() throws Exception {
		String query = " and itemtype = *";
		// Node node = new TermNode("itemtype", Relation.EQUAL, "*");

		TranslatedQuery parsed = translator.getTranslatedQuery(query);

		printQuery(query, parsed.asLucene());

		query = " AND itemtype = *";
		parsed = translator.getTranslatedQuery(query);
		printQuery(query, parsed.asLucene());

		Map<String, List<String>> cqlOptions = Maps.newHashMap();
		BiMap<String, String> aliases = HashBiMap.create();

		cqlOptions.put("wildcard", Lists.newArrayList("true"));

		query = "AND itemtype = vid* borat";
		parsed = translator.getTranslatedQuery(query, cqlOptions);
		printQuery(query, parsed.asLucene());

		// + - && || ! ( ) { } [ ] ^ " ~ * ? : \
		query = " AND f = * vid* bo?ra bo+ra bo-ra bo&ra bo!ra bo]ra bo^ra bo*ra";// bo|ra bo(ra bo)ra bo{ra bo}ra
		// bo\"ra bo~ra bo:ra bo\\ra";

		CQLNode cqlNode = new CQLParser().parse(query);
		parsed = translator.getTranslatedQuery(cqlNode, new IdentityCqlValueTransformerMap(), cqlOptions, aliases, new HashMap<String, String>());
		printQuery(query, parsed.asLucene());

		parsed = translator.getTranslatedQuery(query, cqlOptions);
		printQuery(query, parsed.asLucene());

		parsed = translator.getTranslatedQuery(query);
		printQuery(query, parsed.asLucene());
	}

	@Test
	public void testToSolr_23() throws Exception {

		String query = "_all = cat sortBy title/sort.descending";
		TranslatedQuery parsed = translator.getTranslatedQuery(query);

		Node node = new TermNode("_all", Relation.EQUAL, "cat");

		printQuery(query, parsed.asLucene());
		assertEquals(node.toLucene(), parsed.asLucene());

		assertNotNull(parsed.getOptions().getSort().getField());
		assertNotNull(parsed.getOptions().getSort().getMode());

		assertEquals("title", parsed.getOptions().getSort().getField());
		assertEquals(SortOperation.Mode.desc, parsed.getOptions().getSort().getMode());
	}

	@Test
	public void testToSolr_24() throws Exception {

		String query = "invalid exact true sortBy title/sort.ascending";

		// Node node = new TermNode("invalid", Relation.EXACT, "true");

		TranslatedQuery parsed = translator.getTranslatedQuery(query);

		printQuery(query, parsed.asLucene());

		assertNotNull(parsed.getOptions().getSort().getField());
		assertNotNull(parsed.getOptions().getSort().getMode());

		assertEquals("title", parsed.getOptions().getSort().getField());
		assertEquals(SortOperation.Mode.asc, parsed.getOptions().getSort().getMode());
	}

	@Test
	public void testToSolr_25() throws Exception {

		// String query = "deleted all true and __dsid all xxxxxxxxxxx";

		// ciao (+a +b +c)
		String query = "ciao or (_all all \"a b c\")";
		TranslatedQuery parsed = translator.getTranslatedQuery(query);
		printQuery(query, parsed.asLucene());

		query = "(__all = ciao) or (__all all \"a b c\")";
		parsed = translator.getTranslatedQuery(query);
		printQuery(query, parsed.asLucene());

		query = "a and b";
		parsed = translator.getTranslatedQuery(query);
		printQuery(query, parsed.asLucene());
		query = "field=a and field=b";
		parsed = translator.getTranslatedQuery(query);
		printQuery(query, parsed.asLucene());

		query = "a or b";
		parsed = translator.getTranslatedQuery(query);
		printQuery(query, parsed.asLucene());

		query = "field=a or field=b";
		parsed = translator.getTranslatedQuery(query);
		printQuery(query, parsed.asLucene());

		query = "field all a or field all b";
		parsed = translator.getTranslatedQuery(query);
		printQuery(query, parsed.asLucene());
	}

	@Test
	public void testToSolr_26() throws Exception {

		String query = "publicationdate =/within \"2000-01-01 2010-01-01\"";
		Node node = new TermNode("publicationdate", Relation.WITHIN, "2000-01-01 2010-01-01");

		TranslatedQuery parsed = translator.getTranslatedQuery(query);

		printQuery(query, parsed.asLucene());
		assertEquals(node.toLucene(), parsed.asLucene());
	}

	@Test
	public void testToSolr_27() throws Exception {

		String query = "((((publicationdate =/within \"2000-01-01 2010-01-01\" and title = \"ddd\") and y < 2010) or y <= 2010) or y > 2010) or y >= 2010";
		Node node = new OrNode(new OrNode(new OrNode(new AndNode(new AndNode(new TermNode("publicationdate", Relation.WITHIN, "2000-01-01 2010-01-01"),
				new TermNode("title", Relation.EQUAL, "ddd")), new TermNode("y", Relation.LT, "2010")), new TermNode("y", Relation.LTE, "2010")), new TermNode(
				"y", Relation.GT, "2010")), new TermNode("y", Relation.GTE, "2010"));

		TranslatedQuery parsed = translator.getTranslatedQuery(query);

		printQuery(query, parsed.asLucene());
		assertEquals(node.toLucene(), parsed.asLucene());
	}

	@Test
	public void testToSolr_28() throws Exception {

		String query = "publicationdate =/within \"2000-01-01 2010-01-01\" and (title = \"ddd\" and (y < 2010 or (y <= 2010 or (y > 2010 or y >= 2010))))";

		Node node = new AndNode(new TermNode("publicationdate", Relation.WITHIN, "2000-01-01 2010-01-01"), new AndNode(new TermNode("title", Relation.EQUAL,
				"ddd"), new OrNode(new TermNode("y", Relation.LT, "2010"), new OrNode(new TermNode("y", Relation.LTE, "2010"), new OrNode(new TermNode("y",
				Relation.GT, "2010"), new TermNode("y", Relation.GTE, "2010"))))));

		TranslatedQuery parsed = translator.getTranslatedQuery(query);

		printQuery(query, parsed.asLucene());
		assertEquals(node.toLucene(), parsed.asLucene());
	}

	@Test
	public void testToSolr_29() throws Exception {

		String query = "dateaccepted =/within \"1900-01-01 2000-01-01\" and dateaccepted >= 2010-01-01";

		Node node = new AndNode(new TermNode("dateaccepted", Relation.WITHIN, "1900-01-01 2000-01-01"),
				new TermNode("dateaccepted", Relation.GTE, "2010-01-01"));

		TranslatedQuery parsed = translator.getTranslatedQuery(query);

		printQuery(query, parsed.asLucene());
		assertEquals(node.toLucene(), parsed.asLucene());

		query = "dateaccepted =/within \"1900-01-01 2000-01-01\" and dateaccepted > 2010-01-01";

		node = new AndNode(new TermNode("dateaccepted", Relation.WITHIN, "1900-01-01 2000-01-01"), new TermNode("dateaccepted", Relation.GT, "2010-01-01"));

		parsed = translator.getTranslatedQuery(query);

		printQuery(query, parsed.asLucene());
		assertEquals(node.toLucene(), parsed.asLucene());
	}

	@Test
	public void testToSolr_30() throws Exception {

		String query = "a = 1 and b = 2 and c = 3";
		Node node = new AndNode(new AndNode(new TermNode("a", Relation.EQUAL, "1"), new TermNode("b", Relation.EQUAL, "2")), new TermNode("c", Relation.EQUAL,
				"3"));

		TranslatedQuery parsed = translator.getTranslatedQuery(query);

		printQuery(query, parsed.asLucene());
		assertEquals(node.toLucene(), parsed.asLucene());
	}

	@Test
	public void testToSolr_31() throws Exception {

		String query = "a = \"pippo pluto\" and b = 2 and c = 3";
		Node node = new AndNode(new AndNode(new TermNode("a", Relation.EQUAL, "pippo pluto"), new TermNode("b", Relation.EQUAL, "2")), new TermNode("c",
				Relation.EQUAL, "3"));

		TranslatedQuery parsed = translator.getTranslatedQuery(query);

		printQuery(query, parsed.asLucene());
		assertEquals(node.toLucene(), parsed.asLucene());
	}

	@Test
	public void testToSolr_32() throws Exception {

		String query = "__all all \"caracas - ro*\"";

		TranslatedQuery parsed = translator.getTranslatedQuery(query);
		printQuery(query, parsed.asLucene());

		Map<String, List<String>> cqlOptions = Maps.newHashMap();
		cqlOptions.put("wildcard", Lists.newArrayList("true"));

		parsed = translator.getTranslatedQuery(query, cqlOptions);
		printQuery(query, parsed.asLucene());

	}

	@Test
	public void testToSolr_33() throws Exception {
		String query1 = "query=(deletedbyinference = false) AND (((oaftype exact project) and (test)) and (fundinglevel0_id exact corda_______::FP7))";
		printQuery(query1, translator.getTranslatedQuery(query1).asLucene());

		String query2 = "query=(((deletedbyinference = false) AND (oaftype exact project)) and (test)) and (fundinglevel0_id exact corda_______::FP7)";
		printQuery(query2, translator.getTranslatedQuery(query2).asLucene());

		String query3 = "(deletedbyinference = false) AND (((oaftype exact project) and (test)) and (fundinglevel0_id exact corda_______::FP7))";
		printQuery(query3, translator.getTranslatedQuery(query1).asLucene());

		String query4 = "(((deletedbyinference = false) AND (oaftype exact project)) and (test)) and (fundinglevel0_id exact corda_______::FP7)";
		printQuery(query4, translator.getTranslatedQuery(query2).asLucene());
	}

	@Test
	public void testWildcard() throws CQLParseException, IOException {

		Map<String, List<String>> options = new HashMap<String, List<String>>();

		String cqlQuery = "__all = \"test?\"";
		String lucene = translator.toLucene(cqlQuery, options);
		printQuery(cqlQuery, lucene);

		options.put("wildcard", Lists.newArrayList("true"));

		cqlQuery = "__all = \"test?\"";
		lucene = translator.toLucene(cqlQuery, options);
		printQuery(cqlQuery, lucene);
	}

	@Test
	public void testWildcard_2() throws CQLParseException, IOException {

		Map<String, List<String>> options = new HashMap<String, List<String>>();

		String cqlQuery = "thumbnail=localhost*";
		String lucene = translator.toLucene(cqlQuery, options);
		printQuery(cqlQuery, lucene);

		options.put("wildcard", Lists.newArrayList("true"));

		lucene = translator.toLucene(cqlQuery, options);
		printQuery(cqlQuery, lucene);
	}

	@Test
	public void testDateQuery() throws CQLParseException, IOException {
		String cqlQuery = "(resultdateofacceptance <= \"2012-03-15\")";
		Map<String, List<String>> options = new HashMap<String, List<String>>();
		String lucene = translator.toLucene(cqlQuery, options);
		printQuery(cqlQuery, lucene);
	}

	@Test
	public void testFullISODateQuery() throws CQLParseException, IOException {
		String cqlQuery = "(resultdateofacceptance <= 2012-03-15T00:00:00Z)";
		Map<String, List<String>> options = new HashMap<String, List<String>>();
		String lucene = translator.toLucene(cqlQuery, options);
		printQuery(cqlQuery, lucene);
	}

	@Test
	public void testNonDateQuery() throws CQLParseException, IOException {
		String cqlQuery = "(resultdateofacceptance <= 2012.03.15T00:00:00Z)";
		Map<String, List<String>> options = new HashMap<String, List<String>>();
		String lucene = translator.toLucene(cqlQuery, options);
		printQuery(cqlQuery, lucene);
	}

	@Test
	public void testNonDateQuery2() throws CQLParseException, IOException {
		String cqlQuery = "(resultdateofacceptance <= ciao)";
		Map<String, List<String>> options = new HashMap<String, List<String>>();
		String lucene = translator.toLucene(cqlQuery, options);
		printQuery(cqlQuery, lucene);
	}

	@Test
	public void testDateWrong() throws Exception {

		String cqlQuery = "publicationdate =/within \"2000-01-01 2010.99.01\"";
		String luceneQuery = translator.toLucene(cqlQuery);

		printQuery(cqlQuery, luceneQuery);
	}

	@Test
	public void testToDate() throws Exception {

		String cqlQuery = "publicationdate =/within \"* 2000-01-01\"";
		String luceneQuery = translator.toLucene(cqlQuery);

		printQuery(cqlQuery, luceneQuery);
	}

	private void printQuery(final String cql, final String lucene) throws CQLParseException, IOException {
		System.out.println("CQL:    " + cql);
		// System.out.println("PARSED: " + new CQLParser().parse(cql).toCQL());
		System.out.println("LUCENE: " + lucene + "\n");

	}

}
