package eu.dnetlib.functionality.index.query;

import java.util.List;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Lists;
import eu.dnetlib.functionality.index.client.IndexClientException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SolrIndexQueryFactoryTest {

	IndexQueryFactory factory;
	BiMap<String, String> browsingAliases = HashBiMap.create();

	@Before
	public void setUp() throws Exception {
		factory = new SolrIndexQueryFactory();
		browsingAliases.put("field1", "field1ForBrowsing");
		browsingAliases.put("field2", "field2ForBrowsing");
	}

	@Test
	public void testGetBrowsableFields() throws IndexClientException {
		List<String> browsables = factory.getBrowsableFields(Lists.newArrayList("field1", "field3", "field2"), browsingAliases);
		Assert.assertEquals(Lists.newArrayList("field1ForBrowsing", "field3", "field2ForBrowsing"), browsables);
	}

}
