package eu.dnetlib.data.download.rmi;

import junit.framework.Assert;

/**
 * Created by sandro on 3/21/14.
 */
public class DownloadItemTest {

	@org.junit.Test
	public void testGetFileName() throws Exception {
		DownloadItem di = new DownloadItem();
		di.setIdItemMetadata("id1");
		di.setUrl("http://www.test.com");
		di.setFileName("testFile");
		Assert.assertEquals("testFile", di.getFileName());
	}
}
