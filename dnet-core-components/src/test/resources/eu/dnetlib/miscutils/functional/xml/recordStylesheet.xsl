<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:dri="http://www.driver-repository.eu/namespace/dri"
                xmlns:dc="http://purl.org/dc/elements/1.1/"
                xmlns:dnet="eu.dnetlib.miscutils.functional.xml.DnetXsltFunctions"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:exsl="http://exslt.org/common"
                xmlns:dr="http://www.driver-repository.eu/namespace/dr"
                extension-element-prefixes="exsl"
                version="1.0">
   <xsl:output encoding="UTF8" method="xml" omit-xml-declaration="yes" version="1.0"/>
   <xsl:variable name="format">EMF</xsl:variable>
   <xsl:template match="/">
      <indexRecord>
         <indexRecordIdentifier>
            <xsl:value-of select="//dri:objIdentifier"/>
         </indexRecordIdentifier>
         <targetFields>
            <xsl:if test="$format != 'DMF' or string(//dc:title)">
               <xsl:if test="count(//*[local-name()='metadata']/*) &gt; 0">
                  <xsl:for-each select="//*[local-name()='title']">
                     <title>
                        <xsl:value-of select="."/>
                     </title>
                  </xsl:for-each>
                  <xsl:for-each select="//*[local-name()='description']">
                     <description>
                        <xsl:value-of select="."/>
                     </description>
                  </xsl:for-each>
                  <xsl:for-each select="//*[local-name()='entityType']">
                     <entitytype>
                        <xsl:value-of select="normalize-space(.)"/>
                     </entitytype>
                  </xsl:for-each>
                  <xsl:for-each select="//*[local-name()='material']">
                     <material>
                        <xsl:value-of select="."/>
                     </material>
                  </xsl:for-each>
                  <xsl:for-each select="//*[local-name()='inscriptionType']">
                     <inscriptionType>
                        <xsl:value-of select="."/>
                     </inscriptionType>
                  </xsl:for-each>
                  <xsl:for-each select="//*[local-name()='dnetResourceIdentifier']">
                     <dnetresourceidentifier>
                        <xsl:value-of select="normalize-space(.)"/>
                     </dnetresourceidentifier>
                  </xsl:for-each>
                  <xsl:for-each select="//*[local-name()='objIdentifier']">
                     <objidentifier>
                        <xsl:value-of select="normalize-space(.)"/>
                     </objidentifier>
                  </xsl:for-each>
                  <xsl:for-each select="//*[local-name()='repositoryId']">
                     <repositoryId>
                        <xsl:value-of select="."/>
                     </repositoryId>
                  </xsl:for-each>
                  <xsl:for-each select="//*[local-name()='providerName']">
                     <repositoryname>
                        <xsl:value-of select="normalize-space(.)"/>
                     </repositoryname>
                  </xsl:for-each>
                  <xsl:for-each select="//*[local-name()='ProviderURI']">
                     <repositorylink>
                        <xsl:value-of select="normalize-space(.)"/>
                     </repositorylink>
                  </xsl:for-each>
                  <xsl:for-each select="//*[local-name()='entityType']">
                     <entitytypeforbrowsing>
                        <xsl:value-of select="normalize-space(.)"/>
                     </entitytypeforbrowsing>
                  </xsl:for-each>
                  <xsl:for-each select="//*[local-name()='material']">
                     <materialforbrowsing>
                        <xsl:value-of select="normalize-space(.)"/>
                     </materialforbrowsing>
                  </xsl:for-each>
                  <xsl:for-each select="//*[local-name()='inscriptionType']">
                     <inscriptiontypeforbrowsing>
                        <xsl:value-of select="normalize-space(.)"/>
                     </inscriptiontypeforbrowsing>
                  </xsl:for-each>
               </xsl:if>
            </xsl:if>
         </targetFields>
         <result>
            <header>
               <dri:objIdentifier>
                  <xsl:value-of select="//dri:objIdentifier"/>
               </dri:objIdentifier>
               <dri:repositoryId>
                  <xsl:value-of select="//dri:repositoryId"/>
               </dri:repositoryId>
               <dri:dateOfCollection>
                  <xsl:value-of select="//dri:dateOfCollection"/>
               </dri:dateOfCollection>
            </header>
            <metadata>
               <xsl:copy-of select="//*[local-name()='providerName']"/>
               <xsl:copy-of select="//*[local-name()='ProviderURI']"/>
               <xsl:copy-of select="//*[local-name()='eagleObject']"/>
            </metadata>
         </result>
      </indexRecord>
   </xsl:template>
</xsl:stylesheet>
