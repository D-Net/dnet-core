<?xml version="1.0" encoding="UTF-8"?>
<RESOURCE_PROFILE>
    <HEADER>
        <RESOURCE_IDENTIFIER value="$id$"/>
        <RESOURCE_TYPE value="$name$ResourceType"/>
        <RESOURCE_KIND value="ServiceResources"/>
        <RESOURCE_URI value="http://localhost:8280/is/services/$name$?wsdl"/>
        <PARENT_ID value="" type="HNM"/>
        <DATE_OF_CREATION value="2014-11-04T19:34:56+01:00"/>
        <PROTOCOLS>
            <PROTOCOL name="SOAP" address="http://localhost:8280/is/services/$name$"/>
        </PROTOCOLS>
    </HEADER>
    <BODY>
        <CONFIGURATION>
            <TYPOLOGY/>
            <MAX_SIZE_OF_DATASTRUCTURE>0</MAX_SIZE_OF_DATASTRUCTURE>
            <AVAILABLE_DISKSPACE>0</AVAILABLE_DISKSPACE>
            <MAX_NUMBER_OF_DATASTRUCTURE>0</MAX_NUMBER_OF_DATASTRUCTURE>
            <SERVICE_PROPERTIES/>
        </CONFIGURATION>
        <STATUS>
            <HANDLED_DATASTRUCTURE>0</HANDLED_DATASTRUCTURE>
            <USED_DISKSPACE>0</USED_DISKSPACE>
            <LAST_UPDATE value="2000-01-01T00:00:00+00:00"/>
        </STATUS>
        <QOS>
            <AVAILABILITY/>
            <CAPACITY/>
            <RESPONSE_TIME>0</RESPONSE_TIME>
            <THROUGHPUT>0</THROUGHPUT>
        </QOS>
        <SECURITY_PARAMETERS/>
        <BLACKBOARD>
            <LAST_REQUEST />
            <LAST_RESPONSE />
        </BLACKBOARD>
    </BODY>
</RESOURCE_PROFILE>