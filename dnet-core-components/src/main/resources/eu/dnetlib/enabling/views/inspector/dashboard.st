$inspector/master(it={
  <h2>Inspector Dashboard</h2>
  
  <style type="text/css">
#grouplist {
  -moz-column-count: 3;
	-moz-column-gap: 20px;
	-webkit-column-count: 3;
	-webkit-column-gap: 20px;
	column-count: 3;
	column-gap: 20px;
}

#grouplist > li  {
  border: 1px solid #ccc;
  padding: 8px;
  margin: 8px;
}

#grouplist > li  {
  -moz-column-break-inside: avoid;
  -webkit-column-break-inside: avoid;
  break-inside: avoid-column;
}
  </style>
  
  <p>Installed inspector panels</p>
  <ul id="grouplist">
    $entryPointGroups:{
      <li>
       <div class="box">
       <span>$it.name$</span>
        <ul>
         $it.descriptors:{
          <li><a href="$baseUrl$/$it.relativeUrl$">$it.name$</a></li>
         }$
        </ul>
        </div>
      </li>
    }$
  </ul>
})$