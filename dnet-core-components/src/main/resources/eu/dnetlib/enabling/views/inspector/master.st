<html>
	<head>
		<title>$if(title)$$title$$else$No title$endif$</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script type="text/javascript" src="$baseUrl$/static.do?src=jquery.js"></script>
		<script type="text/javascript" src="$baseUrl$/static.do?src=jquery-ui.min.js"></script>
		<script type="text/javascript" src="$baseUrl$/static.do?src=jstorage.js"></script>
		<script type="text/javascript" src="$baseUrl$/static.do?src=inspector.js"></script>
		<link rel="stylesheet" href="$baseUrl$/static.do?src=inspector.css">
		$header$
		
		<script>
			setDefaultHiddenItems(",$entryPoints:{$if(it.hiddenAsDefault)$$it.name$,$endif$}$");
		</script>
	</head>
 
	<body style="background-image: url($baseUrl$/static.do?src=inspector-logo.png);">
		<div id="nav">
			<ul>
				$entryPoints:{<li class="menuItem"><a $if(it.current)$class="selected"$endif$ href="$baseUrl$/$it.relativeUrl$">$it.name$</a></li> }$
				<li>
					<a id="menu_P1" class="menuPerspective" href="javascript:changePerspective('P1')" title="perspective P1">P1</a>
				</li>
				<li>
					<a id="menu_P2" class="menuPerspective" href="javascript:changePerspective('P2')" title="perspective P2">P2</a>
				</li>
				<li>
					<a id="menu_P3" class="menuPerspective" href="javascript:changePerspective('P3')" title="perspective P3">P3</a>
				</li>
				<li>
					<a href="javascript:configPerspectives()" class="menuPerspective" title="perspective configuration">+</a>
				</li>
			</ul>
		</div>
		<div id="menuConfig">
			<div>
				<a href="#" onclick="configPerspectives(); return 0;">close</a> | 
				<a href="#" onclick="resetJStorage(); return 0;">reset all</a>
			</div>
			<ul>
			$entryPoints:{<li class="menuAction"><input name="perspective_check_$it.name$" type="checkbox" onchange="changePerspectiveItem(this)" ></input>$it.name$</li>}$
			</ul>
		</div>
		<div id="content">$it$</div>
	</body>
	
</html>