package eu.dnetlib.conf;

import javax.servlet.ServletContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.ServletContextAware;

/**
 * This spring bean detects the webapp context path, from the ServletContext. It requires servlet-api 2.5 (tomcat6 and
 * jetty 6.x).
 *
 * <p>
 * Concrete subclasses will know what to do with this information
 * </p>
 *
 * @author marko
 *
 */
public abstract class AbstractWebappContextProperty implements ServletContextAware {

	/**
	 * logger.
	 */
	private static final Log log = LogFactory.getLog(AbstractWebappContextProperty.class); // NOPMD by marko on 11/24/08 5:02 PM

	/**
	 * web application context.
	 */
	protected String context;

	/**
	 * saved servlet context.
	 */
	protected ServletContext servletContext;

	@Override
	public void setServletContext(final ServletContext servletContext) {
		this.servletContext = servletContext;

		try {
			context = servletContext.getContextPath().substring(1);
		} catch (final NoSuchMethodError e) {
			log.warn("cannot detect servlet context path. servlet-api 2.5 is required, falling back to property based configuration", e);
		}
	}

	public String getContext() {
		return context;
	}

	public void setContext(final String context) {
		this.context = context;
	}

	public ServletContext getServletContext() {
		return servletContext;
	}

}
