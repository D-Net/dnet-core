package eu.dnetlib.soap;

import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.wsaddressing.W3CEndpointReference;

/**
 * This is a generalization of org.apache.cxf.jaxws.EndpointReferenceBuilder and org.apache.cxf.jaxws.EndpointImpl
 *
 * <p>
 * javax.xml.ws.WebServiceContext org.apache.cxf.jaxws.EndpointReferenceBuilder doesn't expose the API for adding
 * additional metadata while org.apache.cxf.jaxws.EndpointImpl doesn't take the correct endpoint address from the
 * EndpointInfo object. org.apache.cxf.endpoint.Endpoint return a CXF proprietary endpoint reference and not the
 * javax/w3c standard definition.
 * </p>
 *
 * <p>
 * This interface is intended to provide an abstract way to construct an endpoint reference for a given service,
 * depending on the type of endpoint interface you have at hand (CXF abstract endpoint or jaxws endpoint)
 * </p>
 *
 * <p>
 * Normally the type parameter T will be bound to your endpoint type.
 * </p>
 *
 * <p>
 * In CXF jaxws applications you can easly get a WebServiceContext instance which returns an EndpointReference, however
 * the API is cumbersome because it requires instantiating w3c DOM Element instances for each reference parameter, and
 * it doesn't allow setting custom metadata elements.
 * </p>
 *
 * <p>
 * Implementors of this API will extract as many useful informations from the runtime besides the plain soap endpoint
 * address.
 * </p>
 *
 * @author marko
 * @param <T>
 *            all endpoint builders are parameterized to specific endpoint type which on the used framework (not on the
 *            service)
 *
 */
public interface EndpointReferenceBuilder<T> {
	/**
	 * get an endpoint reference with default metadata attached.
	 *
	 * @param endpoint
	 *            the endpoint
	 * @return an endpoint reference
	 */
	W3CEndpointReference getEndpointReference(T endpoint);

	/**
	 * get an endpoint reference with custom metadata attached.
	 *
	 * @param endpoint
	 *            the endpoint
	 * @param attrs
	 *            metadata attribute map
	 * @return an endpoint reference
	 */
	W3CEndpointReference getEndpointReference(T endpoint, Map<QName, Object> attrs);

	/**
	 * get an endpoint reference with a WSA reference parameter.
	 *
	 * @param endpoint
	 *            the endpoint
	 * @param referenceParam
	 *            reference parameters
	 * @return an endpoint reference
	 */
	W3CEndpointReference getEndpointReference(T endpoint, String referenceParam);

	/**
	 * get an endpoint reference with custom metadata attached and WSA reference parameter.
	 *
	 * @param endpoint
	 *            endpoint
	 * @param referenceParam
	 *            reference parameters
	 * @param attrs
	 *            metadata attribute map
	 * @return an endpoint reference
	 */
	W3CEndpointReference getEndpointReference(T endpoint, String referenceParam, Map<QName, Object> attrs);

	/**
	 * Sometimes we need only the address.
	 *
	 * @param endpoint endpoint
	 * @return address
	 */
	String getAddress(T endpoint);
}
