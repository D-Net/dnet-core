package eu.dnetlib.soap;

import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.wsaddressing.W3CEndpointReference;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.dnetlib.enabling.tools.OpaqueResource;
import eu.dnetlib.soap.cxf.CxfEndpointReferenceBuilder;

/**
 * Build an epr from a data structure.
 *
 * @author marko
 *
 */
public class DataStructureProfileEndpointReferenceBuilder extends AbstractEndpointReferenceBuilder<OpaqueResource> implements
		EndpointReferenceBuilder<OpaqueResource> {

	private static final Log log = LogFactory.getLog(DataStructureProfileEndpointReferenceBuilder.class); // NOPMD by marko on 11/24/08 5:02 PM


	/**
	 * low level epr builder used to create the actual epr.
	 *
	 * TODO: factor out the address based epr building out of CXF specific code.
	 *
	 */
	private transient CxfEndpointReferenceBuilder lowEprBuilder = new CxfEndpointReferenceBuilder();

	/**
	 * {@inheritDoc}
	 * @see eu.dnetlib.soap.EndpointReferenceBuilder#getAddress(java.lang.Object)
	 */
	@Override
	public String getAddress(OpaqueResource profile) {
		return profile.getResourceUri().replace("?wsdl", "");
	}

	/**
	 * {@inheritDoc}
	 * @see eu.dnetlib.soap.AbstractEndpointReferenceBuilder#getEndpointReference(java.lang.Object, java.util.Map)
	 */
	@Override
	public W3CEndpointReference getEndpointReference(OpaqueResource profile, Map<QName, Object> attrs) {
		return getEndpointReference(profile, profile.getResourceId(), attrs);
	}

	/**
	 * {@inheritDoc}
	 * @see eu.dnetlib.soap.AbstractEndpointReferenceBuilder#getEndpointReference(java.lang.Object)
	 */
	@Override
	public W3CEndpointReference getEndpointReference(OpaqueResource profile) {
		log.info("GETTING EPR short");

		return getEndpointReference(profile, profile.getResourceId());
	}

	/**
	 * {@inheritDoc}
	 * @see eu.dnetlib.soap.EndpointReferenceBuilder#getEndpointReference(java.lang.Object, java.lang.String, java.util.Map)
	 */
	@Override
	public W3CEndpointReference getEndpointReference(OpaqueResource profile, String referenceParam, Map<QName, Object> attrs) {
		return lowEprBuilder.getEndpointReference(getAddress(profile), null, null, getAddress(profile) + "?wsdl", referenceParam,
				new HashMap<QName, Object>());
	}

}
