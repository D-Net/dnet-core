package eu.dnetlib.soap;

import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.wsaddressing.W3CEndpointReference;

/**
 *
 * default implementation short methods.
 *
 * @author marko
 *
 * @param <T>
 */
public abstract class AbstractEndpointReferenceBuilder<T> implements EndpointReferenceBuilder<T> {

	/**
	 * {@inheritDoc}
	 *
	 * @see eu.dnetlib.soap.EndpointReferenceBuilder#getEndpointReference(java.lang.Object)
	 */
	@Override
	public W3CEndpointReference getEndpointReference(final T endpoint) {
		return getEndpointReference(endpoint, (Map<QName, Object>)null);
	}

	/**
	 * {@inheritDoc}
	 * @see eu.dnetlib.soap.EndpointReferenceBuilder#getEndpointReference(java.lang.Object, java.lang.String)
	 */
	@Override
	public W3CEndpointReference getEndpointReference(final T endpoint, final String referenceParam) {
		return getEndpointReference(endpoint, referenceParam, null);
	}

	/**
	 * {@inheritDoc}
	 * @see eu.dnetlib.soap.EndpointReferenceBuilder#getEndpointReference(java.lang.Object, java.util.Map)
	 */
	@Override
	public W3CEndpointReference getEndpointReference(final T endpoint, final Map<QName, Object> attrs) {
		return getEndpointReference(endpoint, null, attrs);
	}

}
