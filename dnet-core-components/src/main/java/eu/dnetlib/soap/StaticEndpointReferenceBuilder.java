package eu.dnetlib.soap;

import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.wsaddressing.W3CEndpointReference;
import javax.xml.ws.wsaddressing.W3CEndpointReferenceBuilder;

/**
 * This endpoint reference builder builds always the same epr, with a fixed address, for any incoming endpoint (even
 * null endpoints). Useful when registering service profiles for external services like the old perl Aggregator.
 *
 * @author marko
 *
 * @param <T>
 *            endpoint type
 */
public class StaticEndpointReferenceBuilder<T> implements EndpointReferenceBuilder<T> {

	/**
	 * service address.
	 */
	private String address;

	@Override
	public String getAddress(final T endpoint) {
		return address;
	}

	@Override
	public W3CEndpointReference getEndpointReference(final T endpoint) {
		final W3CEndpointReferenceBuilder builder = new W3CEndpointReferenceBuilder();
		builder.address(address);
		return builder.build();
	}

	@Override
	public W3CEndpointReference getEndpointReference(final T endpoint, final Map<QName, Object> attrs) {
		return getEndpointReference(endpoint);
	}

	@Override
	public W3CEndpointReference getEndpointReference(final T endpoint, final String referenceParam) {
		return getEndpointReference(endpoint);
	}

	@Override
	public W3CEndpointReference getEndpointReference(final T endpoint, final String referenceParam, final Map<QName, Object> attrs) {
		return getEndpointReference(endpoint);
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(final String address) {
		this.address = address;
	}

}
