package eu.dnetlib.soap.cxf;

import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.Endpoint;
import javax.xml.ws.wsaddressing.W3CEndpointReference;

import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Required;

import eu.dnetlib.soap.AbstractEndpointReferenceBuilder;

/**
 * This EndpointReferenceBuilder implementation takes a jaxws endpoint and extracts the cxf endpoint from it.
 *
 * jaxws endpoints are the most readily available endpoint objects since they constructed from jaxws:endpoint spring
 * beans, as shown in the CXF documentation.
 *
 * Since this implementation forwards the job to CxfEndpointReferenceBuilder, the 'builder' property has to be set:
 *
 * <pre>
 * &lt;bean name=&quot;jaxwsEndpointReferenceBuilder&quot; class=&quot;eu.dnetlib.soap.cxf.JaxwsEndpointReferenceBuilder&quot;
 *   p:builder-ref=&quot;cxfEndpointReferenceBuilder&quot; /&gt;
 * </pre>
 *
 * @author marko
 * @see CxfEndpointReferenceBuilder
 *
 */
public class JaxwsEndpointReferenceBuilder extends AbstractEndpointReferenceBuilder<Endpoint> {

	/**
	 * required reference to the cxf endpoint builder.
	 */
	private CxfEndpointReferenceBuilder builder = null;

	/**
	 * simply unpacks the cxf endpoint from the jaxws endpoint and forwards it to CxfEndpointReferenceBuilder.
	 *
	 * {@inheritDoc}
	 *
	 * @see eu.dnetlib.soap.EndpointReferenceBuilder#getEndpointReference(java.lang.Object, java.util.Map)
	 */
	@Override
	public W3CEndpointReference getEndpointReference(final Endpoint endpoint, final String referenceParam, final Map<QName, Object> attrs) {
		return builder.getEndpointReference(((EndpointImpl) endpoint).getServer().getEndpoint(), referenceParam, attrs);
	}

	public CxfEndpointReferenceBuilder getBuilder() {
		return builder;
	}

	@Required
	public void setBuilder(final CxfEndpointReferenceBuilder builder) {
		this.builder = builder;
	}

	@Override
	public String getAddress(Endpoint endpoint) {
		return builder.getAddress(((EndpointImpl) endpoint).getServer().getEndpoint());
	}

}
