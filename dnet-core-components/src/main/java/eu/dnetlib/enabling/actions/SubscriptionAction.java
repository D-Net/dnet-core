package eu.dnetlib.enabling.actions;

import eu.dnetlib.enabling.tools.blackboard.NotificationHandler;

/**
 * Manager subscription action.
 *
 * @author marko
 *
 */
public interface SubscriptionAction extends NotificationHandler {
	/**
	 * topic expression associated with this subscription action.
	 *
	 * @return SN topic expression
	 */
	String getTopicExpression();

	/**
	 * react on this topic prefix. Might be shorter than the topic expression.
	 *
	 * @return SN topic prefix
	 */
	String getTopicPrefix();
}
