package eu.dnetlib.enabling.actions;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Required;

import eu.dnetlib.enabling.actions.SubscriptionAction;
import eu.dnetlib.enabling.tools.Enableable;

/**
 * Common subscription action.
 *
 * @author marko
 *
 */
public abstract class AbstractSubscriptionAction implements SubscriptionAction, Enableable {

	/**
	 * topic expression.
	 */
	private String topicExpression;
	
	/**
	 * topic prefix.
	 */
	private String topicPrefix;

	/**
	 * true if enabled.
	 */
	private boolean enabled = true;

	/**
	 * init the default topic prefix.
	 */
	@PostConstruct
	public void init() {
		if (topicPrefix == null)
			topicPrefix = getTopicExpression().replace("/", ".");
	}

	@Override
	public String getTopicExpression() {
		return topicExpression;
	}
	
	@Required
	public void setTopicExpression(final String topicExpression) {
		this.topicExpression = topicExpression;
	}

	@Override
	public String getTopicPrefix() {
		return topicPrefix;
	}

	public void setTopicPrefix(final String topicPrefix) {
		this.topicPrefix = topicPrefix;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
