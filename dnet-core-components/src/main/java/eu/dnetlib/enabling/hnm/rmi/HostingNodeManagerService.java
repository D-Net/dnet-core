package eu.dnetlib.enabling.hnm.rmi;

import javax.jws.WebParam;
import javax.jws.WebService;

import eu.dnetlib.common.rmi.BaseService;

/**
 * The HostingNodeManager Service is used to ...
 *
 *
 */

@WebService(targetNamespace = "http://services.dnetlib.eu/")
public interface HostingNodeManagerService extends BaseService {
	public String echo(@WebParam(name = "s") String s);
}
