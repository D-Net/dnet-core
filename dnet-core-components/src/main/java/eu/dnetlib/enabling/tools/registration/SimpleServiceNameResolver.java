package eu.dnetlib.enabling.tools.registration;

/**
 * Dummy service name generator implementation. Simply returns the class name. This will never work in practice because
 * people have freedom calling their implementations of the service interface as they want.
 * 
 * @author marko
 * 
 */
@Deprecated
public class SimpleServiceNameResolver implements ServiceNameResolver {

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.tools.registration.ServiceNameResolver#getName(java.lang.Object)
	 */
	@Override
	public String getName(final Object service) {
		return getName(service.getClass());
	}
	
	/** 
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.tools.registration.ServiceNameResolver#getName(java.lang.Class)
	 */
	@Override
	public String getName(final Class<?> iface) {
		return iface.getSimpleName();
	}

}
