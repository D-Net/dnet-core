package eu.dnetlib.enabling.tools;

import java.util.List;

/**
 * A service enumerator returns a bunch of service descriptions. The logic depends on the service enumerator.
 *
 * @author marko
 *
 */
@Deprecated
public interface ServiceEnumerator<T> {
	/**
	 * Obtain a list of services.
	 *
	 * @return a list of service running instance descriptions
	 */
	List<ServiceRunningInstance<T>> getServices();
}
