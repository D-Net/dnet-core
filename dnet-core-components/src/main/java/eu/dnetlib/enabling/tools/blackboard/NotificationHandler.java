package eu.dnetlib.enabling.tools.blackboard;


/**
 * Each service may have a chain of NotificationHandlers, which process incoming notifications.
 * 
 * @author marko
 *
 */
public interface NotificationHandler {
	/**
	 * Incoming notification received.
	 * 
	 * @param subscrId subscriptionId
	 * @param topic topic
	 * @param rsId resource id
	 * @param profile resource profile
	 */
	void notified(String subscrId, final String topic, final String rsId, final String profile);
	
	
}
