package eu.dnetlib.enabling.tools.blackboard;

/**
 * Helper component used to bridge the high level blackboard job handling from the low level blackboard protocol based
 * on notifications and blackboard messages.
 *
 * @author marko
 *
 */
public interface BlackboardServerHandler extends BlackboardHandler {
	/**
	 * Sets the ongoing action status to the given job, publishing this new state.
	 *
	 * @param job
	 *            blackboard job
	 */
	void ongoing(BlackboardJob job);

	/**
	 * Sets the "failed" action status to the given job, publishing this new state along with the error message obtained
	 * from the exception.
	 *
	 * @param job
	 *            blackboard job
	 * @param exception
	 *            exception which caused the failure
	 */
	void failed(BlackboardJob job, Throwable exception);

	/**
	 * Set the "done" action status to the given job, publishing the new state.
	 *
	 * @param job
	 *            blackboard job
	 */
	void done(BlackboardJob job);
}
