package eu.dnetlib.enabling.tools;

import java.util.UUID;

/**
 * generates unique UUID identifiers.
 * 
 * @author marko
 * 
 */
public class UniqueIdentifierGeneratorImpl implements UniqueIdentifierGenerator {

	/**
	 * helps identify valid identifiers.
	 */
	public static final String UUID_REGEX = "[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}";
	
	/**
	 * prefixes all identifiers with this string.
	 */
	private String prefix = "";

	/**
	 * create a new instance with an empty prefix.
	 */
	public UniqueIdentifierGeneratorImpl() {
		//
	}

	/**
	 * construct a new instance with a given prefix.
	 * 
	 * @param prefix prefix
	 */
	public UniqueIdentifierGeneratorImpl(final String prefix) {
		this.prefix = prefix;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(final String prefix) {
		this.prefix = prefix;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.tools.UniqueIdentifierGenerator#generateIdentifier()
	 */
	@Override
	public String generateIdentifier() {
		return prefix + createUUID();
	}

	/**
	 * the real job of creating the uuid is here.
	 * 
	 * @return new uuid
	 */
	private String createUUID() {
		return UUID.randomUUID().toString();
	}

	/** 
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.tools.UniqueIdentifierGenerator#getRegEx()
	 */
	@Override
	public String getRegEx() {
		return UUID_REGEX;
	}
}
