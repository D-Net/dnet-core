package eu.dnetlib.enabling.tools;

/**
 * simplest HNM locator: doesn't find any.
 * 
 * @author marko
 * 
 */
public class NullHNMLocator implements HNMLocator {

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.tools.HNMLocator#getHNMForUrl(java.lang.String)
	 */
	@Override
	public String getHNMForUrl(final String url) {
		return "";
	}

}
