package eu.dnetlib.enabling.tools;

/**
 * Resource identifiers may carry, implicitly or explicity, some informations about the location of the resource in the
 * hierarchical xmldb namespace.
 * 
 * Implementors of this interface may take different approachs to the problem.
 * 
 * @author marko
 * 
 */
public interface ResourceIdentifierResolver {
	/**
	 * get the xmldb filename associated to this resource identifier, usually extracted from the identifier itself.
	 * 
	 * @param resId resource identifier
	 * @return xmldb file name
	 */
	String getFileName(String resId);

	/**
	 * get the xmldb collection name associated to this resource identifier.
	 * Implementors may decide to encode it in the identifier itself or use a secondary associative memory.
	 * to quickly transform identifiers.
	 * 
	 * @param resId resource identifier
	 * @return xmldb collection name
	 */
	String getCollectionName(String resId);
}
