package eu.dnetlib.enabling.tools.blackboard;

import java.util.Map;

/**
 * High level representation of a blackboard job.
 * 
 * @author marko
 * 
 */
public interface BlackboardJob {

	/**
	 * Get the message id.
	 * 
	 * @return id
	 */
	String getId();

	/**
	 * Set message id.
	 * 
	 * @param identifier
	 *            id
	 */
	void setId(String identifier);

	/**
	 * Get the action name.
	 * 
	 * @return action name
	 */
	String getAction();

	/**
	 * Set the action name.
	 * 
	 * @param action
	 *            action name
	 */
	void setAction(String action);

	/**
	 * Get the action status.
	 * 
	 * @return action status
	 */
	ActionStatus getActionStatus();

	/**
	 * Set the action status.
	 * 
	 * @param actionStatus
	 *            action status
	 */
	void setActionStatus(ActionStatus actionStatus);

	/**
	 * Get the message date.
	 * 
	 * @return date
	 */
	String getDate();

	/**
	 * Set the message date.
	 * 
	 * @param date
	 *            date
	 */
	void setDate(String date);

	/**
	 * obtains a mutable parameter map (key/value).
	 * 
	 * @return mutable parameter map
	 */
	Map<String, String> getParameters();

	/**
	 * Get the error message, if the actionStatus is FAILED. Usually the error message is also present in the parameter map but it's
	 * preferred to use this method.
	 * 
	 * @return error message
	 */
	String getError();

	/**
	 * Set the error message, if the actionStatus is FAILED. Usually the error message is also present in the parameter map but it's
	 * preferred to use this method.
	 * 
	 * @param error
	 *            error message
	 */
	void setError(String error);

	/**
	 * Get the setviceId.
	 * 
	 * @return the serviceId
	 */
	String getServiceId();

	/**
	 * True if the action status is one of the termination statuses (e.g. done and failed).
	 * 
	 * @return true if completed
	 */
	boolean isCompleted();

	/**
	 * Get the bb message.
	 * 
	 * @return the bb message
	 */
	BlackboardMessage getMessage();

}
