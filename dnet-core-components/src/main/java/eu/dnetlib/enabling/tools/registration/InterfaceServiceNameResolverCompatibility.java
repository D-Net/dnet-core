package eu.dnetlib.enabling.tools.registration;

import java.util.HashMap;
import java.util.Map;

/**
 * Allow manual (external) override of service types, through an appropriate mapping (e.g. injected with spring).
 *
 * @author marko
 *
 */
public class InterfaceServiceNameResolverCompatibility extends InterfaceServiceNameResolver {

	/**
	 * Service names resolved with InterfaceServiceNameResolver will be used as keys in this map. If there is a match,
	 * the mapping is used instead of the inferred name.
	 */
	private Map<String, String> mapping = new HashMap<String, String>();

	public Map<String, String> getMapping() {
		return mapping;
	}

	public void setMapping(final Map<String, String> mapping) {
		this.mapping = mapping;
	}

	/**
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.tools.registration.InterfaceServiceNameResolver#getName(java.lang.Object)
	 */
	@Override
	public String getName(final Class<?> iface) {
		final String name = super.getName(iface);

		final String override = getMapping().get(name);
		if (override != null)
			return override;

		if(name.charAt(0) == 'I' && Character.isUpperCase(name.charAt(1)) && !Character.isUpperCase(name.charAt(2)))
			return name.substring(1);

		return name;
	}
}
