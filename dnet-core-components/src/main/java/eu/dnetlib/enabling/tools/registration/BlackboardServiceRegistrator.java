package eu.dnetlib.enabling.tools.registration;

import javax.xml.ws.Endpoint;

import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService;
import eu.dnetlib.enabling.is.sn.rmi.ISSNService;
import eu.dnetlib.enabling.tools.OpaqueResource;
import eu.dnetlib.enabling.tools.StringOpaqueResource;

/**
 * This service registrator also subscribes the service to it's own blackboard.
 * 
 * XXX: this class is a hack. It only works with the ValidatingServiceRegistrationManager
 * 
 * @author marko
 * 
 */
public class BlackboardServiceRegistrator extends ServiceRegistrator {

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.tools.registration.ServiceRegistrator#validateProfile(java.lang.String)
	 */
	@Override
	public String validateProfile(final String profId, final Endpoint endpoint) {
		final String profileId = super.validateProfile(profId, endpoint);

		try {
			final OpaqueResource serviceProfile = new StringOpaqueResource(getServiceLocator().getService(ISLookUpService.class).getResourceProfile(profileId));

			getServiceLocator().getService(ISSNService.class, true).subscribe(getEprBuilder().getEndpointReference(endpoint),
					"UPDATE/" + serviceProfile.getResourceType() + "/" + profileId + "/RESOURCE_PROFILE/BODY/BLACKBOARD/LAST_REQUEST", 0);
		} catch (Exception e) {
			throw new IllegalStateException("cannot validate service profile", e);
		}
		return profileId;
	}

}
