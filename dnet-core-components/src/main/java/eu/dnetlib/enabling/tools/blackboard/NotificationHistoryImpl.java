package eu.dnetlib.enabling.tools.blackboard;

import java.util.HashMap;
import java.util.Map;


public class NotificationHistoryImpl implements NotificationHistory {
	
	private Map<String, Map<String, NotificationInfo>> notifications = new HashMap<String, Map<String, NotificationInfo>>();

	@Override
	public NotificationInfo obtainNotification(String bean, String rsId) {
		if (!notifications.containsKey(bean))
			return null;
		return notifications.get(bean).get(rsId);
	}
	
	@Override
	public void saveNotification(NotificationInfo info) {
		String bean = info.getName();
		String rsId = info.getRsId();
		
		if (!notifications.containsKey(bean))
			notifications.put(bean, new HashMap<String, NotificationInfo>());
		
		notifications.get(bean).put(rsId, info);
	}

	@Override
	public Map<String, Map<String, NotificationInfo>> obtainAllNotifications() {
		return notifications;
	}

	@Override
	public void clearNotification(String bean, String rsId) {
		if (notifications.containsKey(bean))
			notifications.get(bean).remove(rsId);
	}
}
