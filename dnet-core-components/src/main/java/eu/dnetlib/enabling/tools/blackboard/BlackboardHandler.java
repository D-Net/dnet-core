package eu.dnetlib.enabling.tools.blackboard;

import eu.dnetlib.enabling.tools.OpaqueResource;

/**
 * Basic blackboard handler.
 *
 * @author marko
 *
 */
public interface BlackboardHandler {
	/**
	 * Get the current job from, as notified in the service profile.
	 *
	 * @param profile
	 *            service profile
	 * @return notified blackboard job
	 */
	BlackboardJob getJob(OpaqueResource profile);
}
