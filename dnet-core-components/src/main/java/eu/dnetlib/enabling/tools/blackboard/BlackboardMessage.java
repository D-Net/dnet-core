package eu.dnetlib.enabling.tools.blackboard;

import java.util.List;

/**
 * Implementor of this interface represent blackboard messages nodes as the appear in the service profile blackboard.
 *
 * This is the low-level representation of the blackboard message. The BlackboardJob is a higher level representation of a blackboard message,
 * and a BlackboardHandler helps converting between the two.
 *
 * Implementors of this interface should simply provide a serialization so that the message can be stored in the service profile.
 *
 * @author marko
 *
 */
public interface BlackboardMessage {
	/**
	 * blackboard message date.
	 *
	 * @return date
	 */
	String getDate();

	/**
	 * sets the blackboard message date/timestamp.
	 *
	 * @param date
	 *            date
	 */
	void setDate(String date);

	/**
	 * Blackboard message identifier.
	 *
	 * @return identifier
	 */
	String getId();

	/**
	 * sets the blackboard message identifier.
	 *
	 * @param id identifier
	 */
	void setId(String id); // NOPMD

	/**
	 * Blackboard message action name.
	 *
	 * @return action
	 */
	String getAction();

	/**
	 * Sets the blackboard message action name.
	 *
	 * @param action
	 *            action
	 */
	void setAction(String action);

	/**
	 * Blackboard message parameter list.
	 *
	 * @return parameter list
	 */
	List<BlackboardParameter> getParameters();

	/**
	 * Status of the action associated with this message.
	 *
	 * @return action status
	 */
	ActionStatus getActionStatus();

	/**
	 * sets the action status.
	 *
	 * @param actionStatus
	 *            action status
	 */
	void setActionStatus(ActionStatus actionStatus);
}
