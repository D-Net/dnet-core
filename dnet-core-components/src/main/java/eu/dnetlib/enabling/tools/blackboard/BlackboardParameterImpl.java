package eu.dnetlib.enabling.tools.blackboard;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Blackboard parameter.
 * 
 * @author marko
 * 
 */
@XmlRootElement(name = "PARAMETER")
public class BlackboardParameterImpl implements BlackboardParameter {

	/**
	 * hash seed.
	 */
	private static final int HASH_SEED_2 = 59;

	/**
	 * hash seed.
	 */
	private static final int HASH_SEED = 35;

	/**
	 * parameter name.
	 */
	private String name;

	/**
	 * parameter value.
	 */
	private String value;

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof BlackboardParameter))
			return false;

		if (this == obj)
			return true;

		final BlackboardParameter rhs = (BlackboardParameter) obj;
		return new EqualsBuilder().append(name, rhs.getName()).append(value, rhs.getValue()).isEquals();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(HASH_SEED, HASH_SEED_2).append(name).append(value).toHashCode();
	}

	@Override
	@XmlAttribute
	public String getName() {
		return name;
	}

	@Override
	public void setName(final String name) {
		this.name = name;
	}

	@Override
	@XmlAttribute
	public String getValue() {
		return value;
	}

	@Override
	public void setValue(final String value) {
		this.value = value;
	}

}
