package eu.dnetlib.enabling.tools;

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

/**
 * This resolver chains a list of resolvers.
 * 
 * @author marko
 * 
 */
public class ServiceResolverChain implements ServiceResolver {

	/**
	 * service resolvers in decreasing order or priority.
	 */
	private List<ServiceResolver> resolvers = new ArrayList<ServiceResolver>();

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.tools.ServiceResolver#getService(java.lang.Class,
	 *      javax.xml.ws.wsaddressing.W3CEndpointReference)
	 */
	@Override
	public <T> T getService(final Class<T> clazz, final W3CEndpointReference epr) {
		for (ServiceResolver resolver : getResolvers()) {
			final T service = resolver.getService(clazz, epr);

			if (service != null)
				return service;
		}
		return null;
	}

	/** 
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.tools.ServiceResolver#getResourceIdentifier(javax.xml.ws.wsaddressing.W3CEndpointReference)
	 */
	@Override
	public String getResourceIdentifier(final W3CEndpointReference epr) {
		for (ServiceResolver resolver : getResolvers()) {
			final String rsId = resolver.getResourceIdentifier(epr);

			if (rsId != null)
				return rsId;
		}
		return null;	
	}
	
	public List<ServiceResolver> getResolvers() {
		return resolvers;
	}

	public void setResolvers(final List<ServiceResolver> resolvers) {
		this.resolvers = resolvers;
	}

}
