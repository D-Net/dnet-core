package eu.dnetlib.enabling.tools.blackboard;

import java.util.Map;

public interface NotificationHistory {

	/**
	 * Return a saved notification associated to a bean and to a rsId.
	 * 
	 * @param name
	 * @return
	 */
	public NotificationInfo obtainNotification(String bean, String rsId);
	
	/**
	 * Save a notification.
	 * 
	 * @param info
	 */
	void saveNotification(NotificationInfo info);

	
	/**
	 * Return all saved notifications.
	 * 
	 * @return
	 */
	Map<String, Map<String, NotificationInfo>> obtainAllNotifications();
	
	/**
	 * Remove a saved notifications.
	 * 
	 * @return
	 */
	public void clearNotification(String bean, String rsId);
	
}
