package eu.dnetlib.enabling.tools; // NOPMD

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.io.IOUtils;
import org.xml.sax.SAXException;

/**
 * implements a possibly optimized OpaqueResource constructed from a stream containing the xml source.
 * 
 * <p>the optimization consists of postponing the parsing as possibly and parse the profile passing the stream
 * directly to the xml parser.<p>
 * 
 * <p>
 * TODO: implement the optimization, currenly converts to string and reuses StringOpaqueResource
 * </p>
 * 
 * @author marko
 *
 */
public class StreamOpaqueResource extends StringOpaqueResource {

	/**
	 * construct with an input stream.
	 * 
	 * @param source source stream of xml text
	 * @throws XPathExpressionException shouldn't happen
	 * @throws SAXException could happen
	 * @throws IOException could happen
	 * @throws ParserConfigurationException shouldn't happen
	 */
	public StreamOpaqueResource(final InputStream source) throws XPathExpressionException, SAXException, IOException, ParserConfigurationException {
		super(stringify(source));
	}

	/**
	 * helper method: reads a stream to a string.
	 * 
	 * @param source input stream
	 * @return string containing the whole stream content.
	 * @throws IOException could happen.
	 */
	private static String stringify(final InputStream source) throws IOException {
		final StringWriter writer = new StringWriter();
		IOUtils.copy(source, writer);
		return writer.getBuffer().toString();
	}
	
}
