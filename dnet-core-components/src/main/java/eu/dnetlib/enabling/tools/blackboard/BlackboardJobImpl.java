package eu.dnetlib.enabling.tools.blackboard;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * High level representation of a blackboard job.
 * 
 * @author marko
 * 
 */
public class BlackboardJobImpl implements BlackboardJob {

	/**
	 * underlying low level blackboard message.
	 */
	private BlackboardMessage message;

	/**
	 * service identifier.
	 */
	private String serviceId;

	/**
	 * parameters.
	 */
	private final transient Map<String, String> parameters = new HashMap<String, String>();

	/**
	 * Construct a new blackboard job from a blackboard message.
	 * 
	 * @param serviceId
	 *            service identifier
	 * @param message
	 *            underlying low-level blackboard message
	 */
	public BlackboardJobImpl(final String serviceId, final BlackboardMessage message) {
		super();
		this.message = message;
		this.serviceId = serviceId;

		for (final BlackboardParameter param : message.getParameters()) {
			parameters.put(param.getName(), param.getValue());
		}
	}

	@Override
	public String getId() {
		return message.getId();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.tools.blackboard.BlackboardJob#setId(java.lang.String)
	 */
	@Override
	public void setId(final String identifier) {
		message.setId(identifier);
	}

	@Override
	public String getAction() {
		return message.getAction();
	}

	@Override
	public ActionStatus getActionStatus() {
		return message.getActionStatus();
	}

	@Override
	public String getDate() {
		return message.getDate();
	}

	@Override
	public String getError() {
		return getParameters().get("error");
	}

	@Override
	public Map<String, String> getParameters() {
		return parameters;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.tools.blackboard.BlackboardJob#setAction(java.lang.String)
	 */
	@Override
	public void setAction(final String action) {
		message.setAction(action);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.tools.blackboard.BlackboardJob#setActionStatus(eu.dnetlib.enabling.tools.blackboard.ActionStatus)
	 */
	@Override
	public void setActionStatus(final ActionStatus actionStatus) {
		message.setActionStatus(actionStatus);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.tools.blackboard.BlackboardJob#setDate(java.lang.String)
	 */
	@Override
	public void setDate(final String date) {
		message.setDate(date);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.tools.blackboard.BlackboardJob#setError(java.lang.String)
	 */
	@Override
	public void setError(final String error) {
		getParameters().put("error", error);
	}

	/**
	 * returns the blackboard message, potentially modified.
	 * 
	 * @return underlying blackboard message
	 */
	@Override
	public BlackboardMessage getMessage() {
		message.getParameters().clear();
		for (final Entry<String, String> entry : getParameters().entrySet()) {
			final BlackboardParameterImpl param = new BlackboardParameterImpl(); // NOPMD
			param.setName(entry.getKey());
			param.setValue(entry.getValue());
			message.getParameters().add(param);
		}
		return message;
	}

	public void setMessage(final BlackboardMessage message) {
		this.message = message;
	}

	@Override
	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(final String serviceId) {
		this.serviceId = serviceId;
	}

	@Override
	public boolean isCompleted() {
		return getActionStatus() == ActionStatus.DONE || getActionStatus() == ActionStatus.FAILED;
	}

}
