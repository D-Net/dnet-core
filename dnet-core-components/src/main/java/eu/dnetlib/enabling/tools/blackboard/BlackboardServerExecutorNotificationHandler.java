package eu.dnetlib.enabling.tools.blackboard;

import javax.annotation.PostConstruct;

/**
 * In most cases a service just dispatches every incoming blackboard execution request to the blackboard server
 * executor, so that the right action call back will be called.
 * 
 * <p>
 * This bean will wrap a BlackboardServerExecutor as a BlackboardNotificationHandler so that it can be installed as a
 * notification handler of a server.</p>
 * 
 * @author marko
 * 
 * @param <T>
 */
public class BlackboardServerExecutorNotificationHandler<T extends Enum<T>> extends AbstractBlackboardNotificationHandler<BlackboardServerHandler> {

	private BlackboardServerActionExecutor<T> blackboardExecutor;

	@PostConstruct
	public void init() {
		setBlackboardHandler(blackboardExecutor.getBlackboardHandler());
	}

	@Override
	protected void processJob(final BlackboardJob job) {
		blackboardExecutor.execute(job);
	}

	public BlackboardServerActionExecutor<T> getBlackboardExecutor() {
		return blackboardExecutor;
	}

	public void setBlackboardExecutor(BlackboardServerActionExecutor<T> blackboardExecutor) {
		this.blackboardExecutor = blackboardExecutor;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.tools.blackboard.AbstractBlackboardNotificationHandler#setBlackboardHandler(eu.dnetlib.enabling.tools.blackboard.BlackboardHandler)
	 * 
	 *      Redefined here to avoid inheriting the @Required property. It simplifies the spring config for an
	 *      unnecessarry property, since we can get it from the blackboardExecutor.
	 */
	@Override
	public void setBlackboardHandler(BlackboardServerHandler handler) {
		super.setBlackboardHandler(handler);
	}

}
