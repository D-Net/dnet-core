package eu.dnetlib.enabling.tools.registration;

/**
 * Useful if you want to register a service external to this framework.
 *
 * @author marko
 *
 */
@Deprecated
public class StaticServiceNameResolver implements ServiceNameResolver {

	/**
	 * service name.
	 */
	private String name;

	/**
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.tools.registration.ServiceNameResolver#getName(java.lang.Object)
	 */
	@Override
	public String getName(final Object service) {
		return name;
	}

	/**
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.tools.registration.ServiceNameResolver#getName(java.lang.Class)
	 */
	@Override
	public String getName(final Class<?> serviceInterface) {
		return name;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

}
