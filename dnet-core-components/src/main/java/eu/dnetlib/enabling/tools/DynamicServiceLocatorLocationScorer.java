package eu.dnetlib.enabling.tools;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Implementors of this interface provide custom methods to assign scores to services based solely on their location.
 * @author marko
 *
 */
@Deprecated
public interface DynamicServiceLocatorLocationScorer {
	/**
	 * Compute the score assigned to a given service location (url).
	 * 
	 * @param url service url
	 * @return score (the higher the better)
	 * @throws MalformedURLException could happen
	 */
	int score(URL url) throws MalformedURLException;
}
