package eu.dnetlib.enabling.tools.blackboard;

/**
 * Helpers for the blackboard protocol client.
 *
 * @author marko
 *
 */
public interface BlackboardClientHandler extends BlackboardHandler {
	/**
	 * Create a new job.
	 *
	 * @param serviceId service identifier
	 * @return newly created blackboard job
	 */
	BlackboardJob newJob(String serviceId);

	/**
	 * Assign a blackboard job to a service.
	 *
	 * @param job blackboard job to send
	 */
	void assign(BlackboardJob job);

	/**
	 * The client can delete the job after it has reached a final state
	 * or the job timeout has expired.
	 *
	 * @param job blackboard job to delete.
	 */
	void delete(BlackboardJob job);
}
