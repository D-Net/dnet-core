package eu.dnetlib.enabling.tools.blackboard;

import eu.dnetlib.enabling.tools.blackboard.BlackboardJob;
import eu.dnetlib.enabling.tools.blackboard.BlackboardServerHandler;

/**
 * Callback interface for serverside blackboard actions.
 * 
 * <p>
 * Actions should use the 'handler' methods to set the 'ongoing' or 'done' status.
 * </p>
 * 
 * <p>
 * failed status is automatically set upon catching an exception thrown by this interface, so feel free to simply throw
 * whatever you want
 * </p>
 * 
 * @author marko
 * 
 * @param <X>
 */
public interface BlackboardServerAction<X extends Enum<?>> {
	void execute(BlackboardServerHandler handler, BlackboardJob job) throws Exception;
}
