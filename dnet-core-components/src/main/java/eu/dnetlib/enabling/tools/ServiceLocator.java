package eu.dnetlib.enabling.tools;

/**
 * A service locator provides a reference to a, possibly remote, service.
 * 
 * @author marko
 * 
 * @param <T>
 *            the type of the service to return
 * @deprecated As of release 2.0.0, use instead {@link eu.dnetlib.enabling.locators.DefaultUniqueServiceLocator}
 */
@Deprecated
public interface ServiceLocator<T> {

	/**
	 * locate and return a service of this type.
	 * 
	 * @return a service client instance
	 */
	T getService();

	/**
	 * Locate using a profileID (service or datastructure) the service of this type.
	 * 
	 * @param profileId
	 * @param clazz
	 * @return a service client instance
	 */
	T getService(final String profileId, final Class<T> clazz);

	/**
	 * locate and return a service ID.
	 * 
	 * @return a service ID
	 */
	String getServiceId();

	/**
	 * locate and return a service ID.
	 * 
	 * @param profileId
	 *            (the id of the service or the id of one of its datastructures)
	 * @return a service ID.
	 */
	String getServiceId(final String profileId);

}
