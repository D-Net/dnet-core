package eu.dnetlib.enabling.tools;

import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.Lifecycle;

import eu.dnetlib.common.rmi.BaseService;

/**
 * This class contains default definition for BaseService contract and basic service lifecycle.
 * 
 * TODO: split BaseService contract implementation from lifecycle and other helper method
 * 
 * @author marko
 * 
 */
@WebService(targetNamespace = "http://services.dnetlib.eu/")
public abstract class AbstractBaseService implements BaseService, Lifecycle {

	/**
	 * logger.
	 */
	private static final Log log = LogFactory // NOPMD by marko on 11/24/08 5:02 PM
			.getLog(AbstractBaseService.class);

	private boolean started = false;

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.common.rmi.BaseService#identify()
	 */
	@Override
	public String identify() {
		return getClass().getName();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.common.rmi.BaseService#notify(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void notify(final String subscriptionId, final String topic, final String isId, final String message) {
		log.debug("got notification: " + topic + ", profile: " + isId + ", body: " + message);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.common.rmi.BaseService#start()
	 */
	@Override
	public void start() {
		log.info("Starting service " + identify());
		if (started) {
			log.warn("Service " + this + "already started, check bean initializations!");
		}
		started = true;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.context.Lifecycle#isRunning()
	 */
	@Override
	public boolean isRunning() {
		log.debug("called isRunning " + this);
		return false;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.context.Lifecycle#stop()
	 */
	@Override
	public void stop() {
		log.info("Stopping service " + this);
	}

}
