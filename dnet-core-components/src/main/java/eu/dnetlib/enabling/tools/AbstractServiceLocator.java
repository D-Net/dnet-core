package eu.dnetlib.enabling.tools;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;

import javax.annotation.Resource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.ws.wsaddressing.W3CEndpointReference;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.google.common.collect.Maps;

import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService;
import eu.dnetlib.soap.cxf.StandaloneCxfEndpointReferenceBuilder;

@Deprecated
public abstract class AbstractServiceLocator<T> implements ServiceLocator<T> {

	/**
	 * lookup locator.
	 */
	@Resource(name="lookupLocator")
	private ServiceLocator<ISLookUpService> lookUpLocator;	
	
	/**
	 * build epr.
	 */
	@Resource
	private StandaloneCxfEndpointReferenceBuilder eprBuilder;
	
	/**
	 * service resolver. used to create proxies for discovered services.
	 */
	@Resource(name="serviceResolver")
	private ServiceResolver serviceResolver;
	
	/**
	 * logger.
	 */
	private static final Log log = LogFactory.getLog(AbstractServiceLocator.class); // NOPMD by marko on 11/24/08 5:02 PM

	
	@Override
	public T getService(final String profileId, final Class<T> clazz) {
		final String profile = executeQuery(profileId, null);
		
		try {
			final XPathFactory factory = XPathFactory.newInstance();
			final XPath xpath = factory.newXPath();
	
			final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			final Document doc = docBuilder.parse(new InputSource(new StringReader(profile)));
			final String url = xpath.evaluate("//url", doc);
			final String serviceId = xpath.evaluate("//id", doc);
	
			final NodeList propElements = (NodeList) xpath.evaluate("//PROPERTY", doc, XPathConstants.NODESET);
			final Map<String, String> props = Maps.newHashMap();
	
			for (int i = 0; i < propElements.getLength(); i++) {
				Element propElement = (Element) propElements.item(i);
				props.put(propElement.getAttribute("key"), propElement.getAttribute("value"));
			}
	
			final W3CEndpointReference epr = eprBuilder.getEndpointReference(url, null, null, url + "?wsdl", null, null);
	
			final ServiceRunningInstance<T> instance = new ServiceRunningInstance<T>(epr, serviceId, url, props);
			
			return serviceResolver.getService(clazz, instance.getEpr());
		} catch(Exception e) {
			log.error("cannot instantiate service from id: " + profileId, e);
			throw new IllegalStateException("cannot instantiate service from id: " + profileId, e);
		}
	}

	@Override
	public String getServiceId(final String profileId) {
		return executeQuery(profileId, "/RESOURCE_PROFILE/HEADER/RESOURCE_IDENTIFIER/@value/string()");
	}
		
	private String executeQuery(final String profileId, final String xpath) {
		final StringWriter sw = new StringWriter();
		sw.append("let $uri:=/RESOURCE_PROFILE/HEADER[./RESOURCE_IDENTIFIER/@value='");
		sw.append(profileId);
		sw.append("']/RESOURCE_URI/@value/string()");
		sw.append("\n\n");
		sw.append("for $x in collection('/db/DRIVER/ServiceResources')/RESOURCE_PROFILE/HEADER");
		sw.append("\n");		
		sw.append("where $x/RESOURCE_PROFILE/HEADER/RESOURCE_URI/@value = $uri");
		sw.append("\n");		
		sw.append("return $x");
		if (xpath != null) {
			sw.append(xpath);
		}
		final String xq = sw.toString();
		
		try {
			return lookUpLocator.getService().getResourceProfileByQuery(xq);
		} catch (ISLookUpException e) {
			log.error("cannot locate service using query: " + xq, e);
			throw new IllegalStateException("cannot locate service using query: " + xq, e);
		}
	}
	

}