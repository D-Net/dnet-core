package eu.dnetlib.enabling.tools;

/**
 * Simple interface for classes, which's objects actions can be scheduled.
 * 
 * @author claudio
 */
public interface Schedulable extends Enableable {
	/**
	 * Updates the cron expression.
	 * 
	 * @param cronExpression
	 */
	void updateCronExpression(String cronExpression);
	
	/**
	 * Getter for the cron expression.
	 * 
	 * @return the String representing the cron expression.
	 */
	String getCronExpression();
	
	/**
	 * Computes the next time the doExecute method will be invoked.
	 * 
	 * @return the String representing the next fire time.
	 */
	String getNextFireTime();

	/**
	 * True if the object has been paused.
	 * 
	 * @return if the bean has been paused.
	 */
	boolean isPaused();
	
	/**
	 * Prevents the bean for being scheduled for future invocations.
	 */
	void pause();
	
	/**
	 * Resumes the bean for being scheduled for future invocations.
	 */
	void resume();
	
	/**
	 * Must be implemented to perform the real action.
	 */
	void execute();	
	
}
