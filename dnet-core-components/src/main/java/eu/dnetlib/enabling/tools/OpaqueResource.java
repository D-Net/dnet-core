package eu.dnetlib.enabling.tools;

import java.util.Date;

import org.w3c.dom.Document;

/**
 * Some services, in particular the enabling layer, needs to manipulate all types of resources without knowing the exact
 * structure of their content, but only some well defined properties defined here.
 * 
 * <p>
 * Different wrappers can be provided for different kind of resources. For example resources returned from the xmldb
 * layer can be cheaply mapped to an OpaqueResource, without going to full xml->bean conversion.
 * </p>
 * 
 * @author marko
 * 
 */
public interface OpaqueResource {
	/**
	 * Resource type.
	 * 
	 * @return resource type string
	 */
	String getResourceType();

	/**
	 * Resource kind.
	 * 
	 * @return resource kind string
	 */
	String getResourceKind();

	/**
	 * Resource identifier.
	 * 
	 * @return resource identifier string
	 */
	String getResourceId();
	
	/**
	 * Resource URI.
	 * 
	 * @return resource uri string
	 */
	String getResourceUri();
	
	/**
	 * get modification time stamp.
	 * 
	 * @return time stamp
	 */
	Date getModificationDate();

	/**
	 * Implementors may need to serialize the resource to a xml string representation.
	 * 
	 * @return xml serialization
	 */
	String asString();

	/**
	 * Implementors may store the DOM in the first place. Otherwise they should
	 * return a parsed w3c DOM instance.
	 * 
	 * @return DOM document
	 */
	Document asDom();

	/**
	 * change the resource identifier.
	 * 
	 * @param identifier new identifier
	 */
	void setResourceId(String identifier);

	/**
	 * change the resource kind.
	 * 
	 * @param kind new kind
	 */
	void setResourceKind(String kind);
	
	/**
	 * change the resource uri.
	 * 
	 * @param uri new uri
	 */
	void setResourceUri(String uri);
	
	/**
	 * set modification timestamp.
	 * 
	 * @param timeStamp modification time stamp
	 */
	void setModificationDate(Date timeStamp);

}
