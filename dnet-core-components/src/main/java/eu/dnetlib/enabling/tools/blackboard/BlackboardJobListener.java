package eu.dnetlib.enabling.tools.blackboard;

/**
 * Implement this interface in order to receive notifications for specific blackboard messages.
 *
 * @author marko
 *
 */
public interface BlackboardJobListener {

	/**
	 * process the given job.
	 *
	 * @param job job to process
	 */
	void processJob(BlackboardJob job);

}
