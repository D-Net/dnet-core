package eu.dnetlib.enabling.tools;

import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.wsaddressing.W3CEndpointReference;

/**
 * This service resolver asks jaxws to obtain a proxy to a remote service.
 *   
 * @author marko
 *
 */
public class JaxwsServiceResolverImpl extends AbstractServiceResolverImpl implements ServiceResolver {

	/** 
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.tools.ServiceResolver#getService(java.lang.Class, javax.xml.ws.wsaddressing.W3CEndpointReference)
	 */
	@Override
	public <T> T getService(final Class<T> clazz, final W3CEndpointReference epr) {
		synchronized(this) {
			return epr.getPort(clazz, new WebServiceFeature[] {});
		}
	}

}
