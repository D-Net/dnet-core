package eu.dnetlib.enabling.tools.registration;

/**
 * Infers the actual name of the service from a service instance.
 * 
 * @author marko
 *
 */
public interface ServiceNameResolver {
	/**
	 * returns the service name for a given service.
	 * 
	 * @param service service instance
	 * @return service name
	 */
	String getName(Object service);
	
	
	/**
	 * Get the service name for a given service interface.
	 * 
	 * @param serviceInterface service interface
	 * @return service name.
	 */
	String getName(Class<?> serviceInterface);
}
