package eu.dnetlib.enabling.tools.blackboard;

/**
 * Utility abstract class which dispatches to commodity onSomething() methods, one for each interesting
 * blackboard job state.
 *
 * @author marko
 *
 */
public abstract class AbstractBlackboardJobListener implements BlackboardJobListener {

	/**
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.tools.blackboard.BlackboardJobListener#processJob(eu.dnetlib.enabling.tools.blackboard.BlackboardJob)
	 */
	@Override
	public void processJob(final BlackboardJob job) {
		if (job.getActionStatus() == ActionStatus.DONE)
			onDone(job);
		else if (job.getActionStatus() == ActionStatus.FAILED)
			onFailed(job);
		else if (job.getActionStatus() == ActionStatus.ASSIGNED)
			onAssigned(job);
		else if (job.getActionStatus() == ActionStatus.ONGOING)
			onOngoing(job);
	}

	/**
	 * Called when the job enters the ASSIGNED state.
	 *
	 * @param job job
	 */
	protected void onAssigned(final BlackboardJob job) { // NOPMD
		// default no operation
		// TODO: increase job expiry time
	}

	/**
	 * Called when the job enters the ONGOING state.
	 *
	 * @param job job
	 */
	protected void onOngoing(final BlackboardJob job) { // NOPMD
		// default no operation
		// TODO: increase job expiry time
	}

	/**
	 * Called when the job finishes in the FAILED state.
	 *
	 * @param job job
	 */
	protected abstract void onFailed(BlackboardJob job);

	/**
	 * Called when the job finishes in the DONE state.
	 *
	 * @param job job
	 */
	protected abstract void onDone(BlackboardJob job);

}
