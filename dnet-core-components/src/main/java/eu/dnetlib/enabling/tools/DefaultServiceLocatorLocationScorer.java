package eu.dnetlib.enabling.tools;

import java.net.MalformedURLException;
import java.net.URL;

import javax.annotation.Resource;

import eu.dnetlib.soap.cxf.StandaloneCxfEndpointReferenceBuilder;

/**
 * Assign better scores for near services. Can be configured.
 * 
 * @author marko
 *
 */
@Deprecated
public class DefaultServiceLocatorLocationScorer implements DynamicServiceLocatorLocationScorer {

	/**
	 * default score assigned when the other service has the same host.
	 */
	private static final int LOCAL_HOST_SCORE = 5;

	/**
	 * default score assigned when the other service has the same host and port (same container).
	 */
	private static final int LOCAL_PORT_SCORE = 10;

	/**
	 * default score assigned when the other service has the same host and port (same container and context).
	 */
	private static final int LOCAL_SRV_SCORE = 15;
	
	
	/**
	 * score assigned when the other service has the same host.
	 */
	private int localHostScore = LOCAL_HOST_SCORE;
	
	/**
	 * score assigned when the other service has the same host and port (same container).
	 */
	private int localPortScore = LOCAL_PORT_SCORE;
	
	/**
	 * score assigned when the other service has the same host and port (same container and context).
	 */
	private int localSrvScore = LOCAL_SRV_SCORE; 

	/**
	 * build epr.
	 */
	@Resource
	private StandaloneCxfEndpointReferenceBuilder eprBuilder;
	
	/** 
	 * {@inheritDoc}
	 * @throws MalformedURLException 
	 * @see eu.dnetlib.enabling.tools.DynamicServiceLocatorLocationScorer#score(java.net.URL)
	 */
	@Override
	public int score(final URL url) throws MalformedURLException {
		final URL localBase = new URL(eprBuilder.getBaseAddress());
		if (url.toString().startsWith(localBase.toString()))
			return localSrvScore;

		if (localBase.getHost().equals(url.getHost())) {
			if (localBase.getPort() == url.getPort())
				return localPortScore;
			return localHostScore;
		}
		return 0;
	}

	public StandaloneCxfEndpointReferenceBuilder getEprBuilder() {
		return eprBuilder;
	}

	public void setEprBuilder(final StandaloneCxfEndpointReferenceBuilder eprBuilder) {
		this.eprBuilder = eprBuilder;
	}

	public int getLocalHostScore() {
		return localHostScore;
	}

	public void setLocalHostScore(final int localHostScore) {
		this.localHostScore = localHostScore;
	}

	public int getLocalPortScore() {
		return localPortScore;
	}

	public void setLocalPortScore(final int localPortScore) {
		this.localPortScore = localPortScore;
	}

	public int getLocalSrvScore() {
		return localSrvScore;
	}

	public void setLocalSrvScore(final int localSrvScore) {
		this.localSrvScore = localSrvScore;
	}

}
