package eu.dnetlib.enabling.tools;

import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternUtils;

/**
 * Some beans aren't directly instantiated (like quartz jobs for instance), so they cannot receive the resource loader
 * by implementing the ResourceLoaderAware interface.
 * 
 * This class is a temporary solution to this problem until we find a way to obtain a bean reference to the
 * application context (the actual resource loader) itself and inject it directly. It could be done by turning this
 * class into a bean factory and expose the resource loader through it.   
 * 
 * @author marko
 *
 */
public class ResourceLoaderHelper implements ResourceLoaderAware {

	/**
	 * injected resource loader.
	 */
	private ResourceLoader resourceLoader;

	/**
	 * obtain a resource pattern resolver for a given resource loader.
	 * 
	 * @return pattern resolver
	 */
	public ResourcePatternResolver getResourcePatternResolver() {
		return ResourcePatternUtils.getResourcePatternResolver(getResourceLoader());
	}
	
	public ResourceLoader getResourceLoader() {
		return resourceLoader;
	}

	@Override
	public void setResourceLoader(final ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}
	
}
