package eu.dnetlib.enabling.tools.blackboard;

/**
 * Invokes a chain of notification handlers for each incoming notification. Normally an instance of this class is
 * registered as the notification handler for a given service.
 * 
 * @author marko
 * 
 */
public interface NotificationHandlerChain extends NotificationHandler {

	public void delegateNotification(final String subscrId, final String topic, final String rsId, final String profile, final NotificationHandler handler);

}
