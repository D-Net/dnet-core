package eu.dnetlib.enabling.tools.blackboard;

/**
 * Registers BlackboardJobListeners to be called when related incoming blackboard job notifications arrive.
 * @author marko
 *
 */
public interface BlackboardJobRegistry {
	/**
	 * Registers a new job listener. It will be notified when the job changes state. The listener will be unregistered
	 * when the job arrives to a final state (DONE, or FAILED).
	 *
	 * @param job
	 *            job
	 * @param listener
	 *            job listener
	 */
	void registerJobListener(BlackboardJob job, BlackboardJobListener listener);
}
