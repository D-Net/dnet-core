package eu.dnetlib.enabling.tools.blackboard;

import eu.dnetlib.miscutils.datetime.DateUtils;

public class NotificationInfo {
	private String name;
	private String subscrId;
	private String topic;
	private String rsId;
	private String profile;
	private String date;
	
	public NotificationInfo() {	
		this.date = DateUtils.now_ISO8601();
	}
	
	public NotificationInfo(String name, String subscrId, String topic, String rsId, String profile) {
		super();
		this.name = name;
		this.subscrId = subscrId;
		this.topic = topic;
		this.rsId = rsId;
		this.profile = profile;
		this.date = DateUtils.now_ISO8601();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSubscrId() {
		return subscrId;
	}

	public void setSubscrId(String subscrId) {
		this.subscrId = subscrId;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getRsId() {
		return rsId;
	}

	public void setRsId(String rsId) {
		this.rsId = rsId;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}
