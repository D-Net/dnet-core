package eu.dnetlib.enabling.tools.blackboard;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * standard serialization of the blackboard message.
 *
 * @author marko
 *
 */
@XmlRootElement(namespace = "", name = "MESSAGE")
@XmlAccessorType(XmlAccessType.NONE)
public class BlackboardMessageImpl implements BlackboardMessage {

	/**
	 * hash seed.
	 */
	private static final int HASH_SEED_2 = 63;

	/**
	 * hash seed.
	 */
	private static final int HASH_SEED = 13;

	/**
	 * blackboard message timestamp.
	 */
	@XmlAttribute
	private String date;

	/**
	 * blackboard message identifier.
	 */
	@XmlAttribute
	private String id; // NOPMD

	/**
	 * blackboard message action name.
	 */
	@XmlElement(name = "ACTION", required = true)
	private String action;

	/**
	 * blackboard message parameters (key/value).
	 */
	@XmlElement(name = "PARAMETER", type = BlackboardParameterImpl.class)
	private List<BlackboardParameter> parameters = new ArrayList<BlackboardParameter>();

	/**
	 * the status of the action described by this message.
	 */
	@XmlElement(name = "ACTION_STATUS", required = true)
	private ActionStatus actionStatus;

	/**
	 * {@inheritDoc}
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof BlackboardMessage))
			return false;

		if (this == obj)
			return true;

		final BlackboardMessage rhs = (BlackboardMessage) obj;
		return new EqualsBuilder().append(id, rhs.getId()).append(date, rhs.getDate()).append(action, rhs.getAction()).append(actionStatus,
				rhs.getActionStatus()).append(parameters, rhs.getParameters()).isEquals();
	}

	/**
	 * {@inheritDoc}
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder(HASH_SEED, HASH_SEED_2).append(id).append(date).append(action).append(actionStatus).append(parameters).toHashCode();
	}

	@Override
	public String getDate() {
		return date;
	}

	@Override
	public void setDate(final String date) {
		this.date = date;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(final String id) { // NOPMD
		this.id = id;
	}

	@Override
	public String getAction() {
		return action;
	}

	@Override
	public void setAction(final String action) {
		this.action = action;
	}

	@Override
	public List<BlackboardParameter> getParameters() {
		return parameters;
	}

	public void setParameters(final List<BlackboardParameter> parameters) {
		this.parameters = parameters;
	}

	@Override
	public ActionStatus getActionStatus() {
		return actionStatus;
	}

	@Override
	public void setActionStatus(final ActionStatus actionStatus) {
		this.actionStatus = actionStatus;
	}

}
