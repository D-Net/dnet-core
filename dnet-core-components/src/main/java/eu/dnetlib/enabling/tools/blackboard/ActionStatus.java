package eu.dnetlib.enabling.tools.blackboard;

/**
 * Blackboard action status.
 * 
 * @author marko
 *
 */
public enum ActionStatus {
	/**
	 * The job/action is assigned but not jet taken into started execution.
	 */
	ASSIGNED,
	/**
	 * The job/action is ongoing.
	 */
	ONGOING,
	/**
	 * The job/action is completed successfully.
	 */
	DONE,
	/**
	 * The job/action is completed with failure.
	 */
	FAILED
}
