package eu.dnetlib.enabling.tools;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Required;

import eu.dnetlib.enabling.is.registry.rmi.ISRegistryException;
import eu.dnetlib.enabling.is.registry.rmi.ISRegistryService;
import eu.dnetlib.enabling.locators.UniqueServiceLocator;

/**
 * Schedulable implementation of an xUpdate.
 * 
 * @author claudio
 * 
 */
public class SchedulableProfileUpdater extends AbstractSchedulable {

	private static final Log log = LogFactory.getLog(SchedulableProfileUpdater.class); // NOPMD by marko on 11/24/08 5:02 PM

	private String xUpdate;

	@Resource
	private UniqueServiceLocator serviceLocator;

	@Override
	protected void doExecute() {
		try {
			log.info("triggering scheduled reindex: " + getxUpdate());
			serviceLocator.getService(ISRegistryService.class).executeXUpdate(getxUpdate());
		} catch (ISRegistryException e) {

			throw new RuntimeException(e);
		}
	}

	public String getxUpdate() {
		return xUpdate;
	}

	@Required
	public void setxUpdate(String xUpdate) {
		this.xUpdate = xUpdate;
	}

}
