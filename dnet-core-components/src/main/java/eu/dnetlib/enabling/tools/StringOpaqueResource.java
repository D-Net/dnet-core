package eu.dnetlib.enabling.tools; // NOPMD

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Opaque resource initialized from a string.
 * 
 * Currently it extends a DOMOpaqueResource, but in future it should postpone xml parsing as late as possible.
 * 
 * @author marko
 *
 */
public class StringOpaqueResource extends DOMOpaqueResource {

	/**
	 * construct an opaque resource from a xml source string.
	 * 
	 * @param source xml source
	 * @throws XPathExpressionException happens
	 * @throws SAXException happens
	 * @throws IOException happens
	 * @throws ParserConfigurationException happens
	 */
	public StringOpaqueResource(final String source) throws XPathExpressionException, SAXException, IOException, ParserConfigurationException {
		super(getBuilderFactory().newDocumentBuilder().parse(new InputSource(new StringReader(source))));
		
	}

	/**
	 * get a namespace aware document builder factory. 
	 * 
	 * @return document builder factory.
	 */
	private static DocumentBuilderFactory getBuilderFactory() {
		final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		return factory;
	}
	
}
