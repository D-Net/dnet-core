package eu.dnetlib.enabling.tools.blackboard;

/**
 * Blackboard parameter used in a BlackboardMessage.
 * 
 * @author marko
 *
 */
public interface BlackboardParameter {
	
	/**
	 * parameter name (key).
	 * @return name
	 */
	String getName();
	
	/**
	 * setter.
	 * 
	 * @param name name
	 */
	void setName(String name);
	
	/**
	 * parameter value.
	 * @return value
	 */
	String getValue();
	
	/**
	 * setter.
	 * 
	 * @param value value
	 */
	void setValue(String value);
}
