package eu.dnetlib.enabling.tools;

/**
 * Implementations of this interface will provide functionalities useful to create xqueries or parts of thereof, like
 * xmldb collection names for a given profile.
 * 
 * @author marko
 * 
 */
public interface XQueryUtils {
	/**
	 * obtain the name of the xmldb collection for a given resource.
	 * @param resource resource
	 * @return path of the collection relative to the xmldb dnet root.
	 */
	String getCollectionPath(OpaqueResource resource);
	
	/**
	 * obtain the absolute xmldb collection path for a given resource.
	 * @param resource resource
	 * @return collection absolute path
	 */
	String getCollectionAbsPath(OpaqueResource resource);
	
	/**
	 * get the absolute xmldb collection path.
	 * 
	 * @return root xmldb root collection path
	 */
	String getRootCollection();

	/**
	 * Return the xmldb filename for a given resource.
	 * 
	 * @param resource resource
	 * @return xmldb filename
	 */
	String getFileName(OpaqueResource resource);
}
