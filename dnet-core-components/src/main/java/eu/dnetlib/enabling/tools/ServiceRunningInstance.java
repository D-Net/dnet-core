package eu.dnetlib.enabling.tools;

import java.util.HashMap;
import java.util.Map;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

/**
 * This bean packages the minimum information for describing a service running instance.
 *
 * @author marko
 *
 */
@Deprecated
public class ServiceRunningInstance<T> {
	/**
	 * service endpoint.
	 */
	private W3CEndpointReference epr;

	/**
	 * service resource id.
	 */
	private String serviceId;

	/**
	 * service endpoint url.
	 */
	private String url;

	/**
	 * arbitrary service properties stored in the profile.
	 */
	private Map<String, String> serviceProperties;

	/**
	 * Create a service running instance.
	 *
	 * @param epr service epr
	 * @param serviceId service profile resource id
	 * @param url service endpoint url
	 */
	public ServiceRunningInstance(final W3CEndpointReference epr, final String serviceId, final String url) {
		this(epr, serviceId, url, new HashMap<String, String>());
	}

	/**
	 * Create a service running instance.
	 *
	 * @param epr service epr
	 * @param serviceId service profile resource id
	 * @param url service endpoint url
	 * @param serviceProperties service property map
	 */
	public ServiceRunningInstance(final W3CEndpointReference epr, final String serviceId, final String url, final Map<String, String> serviceProperties) {
		super();
		this.epr = epr;
		this.serviceId = serviceId;
		this.url = url;
		this.serviceProperties = serviceProperties;
	}

	public W3CEndpointReference getEpr() {
		return epr;
	}

	public void setEpr(final W3CEndpointReference epr) {
		this.epr = epr;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(final String serviceId) {
		this.serviceId = serviceId;
	}

	public Map<String, String> getServiceProperties() {
		return serviceProperties;
	}

	public void setServiceProperties(final Map<String, String> serviceProperties) {
		this.serviceProperties = serviceProperties;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
