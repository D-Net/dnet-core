package eu.dnetlib.enabling.tools;

/**
 * finds an HNM profile for a giver service url.
 * 
 * @author marko
 * 
 */
public interface HNMLocator {

	/**
	 * finds an HNM profile for a giver service url.
	 * 
	 * @param url
	 *            service address
	 * @return hnm id
	 */
	String getHNMForUrl(String url);
}
