package eu.dnetlib.enabling.tools;

import javax.xml.transform.dom.DOMResult;
import javax.xml.ws.wsaddressing.W3CEndpointReference;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * implement common functionality of ServiceResolvers.
 * 
 * @author marko
 *
 */
public abstract class AbstractServiceResolverImpl implements ServiceResolver {
	
	/** 
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.tools.ServiceResolver#getResourceIdentifier(javax.xml.ws.wsaddressing.W3CEndpointReference)
	 */
	@Override
	public String getResourceIdentifier(final W3CEndpointReference epr) {
		final DOMResult dom = new DOMResult();
		epr.writeTo(dom);

		try {
			return XPathFactory.newInstance().newXPath().evaluate("//*[local-name() = 'ResourceIdentifier']", dom.getNode());
		} catch (XPathExpressionException e) {
			throw new IllegalStateException("cannot construct xpath expression", e);
		} 
	}

}
