package eu.dnetlib.enabling.tools;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

/**
 * Instances of this type resolve EPR references to service endpoints.
 * 
 * @author marko
 * 
 */
public interface ServiceResolver {
	/**
	 * return a service client given an EPR instance.
	 * 
	 * @param <T>
	 *            the service type
	 * @param clazz
	 *            needed for type inference of type parameter <T>
	 * @param epr
	 *            EPR
	 * @return a service client reference
	 */
	<T> T getService(Class<T> clazz, W3CEndpointReference epr);

	/**
	 * Extract the resource identifier embedded in the EPR.
	 * 
	 * @param epr end point reference
	 * @return resource identifier
	 */
	String getResourceIdentifier(W3CEndpointReference epr);
}
