package eu.dnetlib.enabling.tools;

/**
 * Simple interface for classes, which's objects can be enabled or disabled.
 * 
 * 
 * @author marko
 * 
 */
public interface Enableable {
	/**
	 * True if this object enabled.
	 * 
	 * @return if this object is currently enabled.
	 */
	boolean isEnabled();

	/**
	 * Enables of disables this object.
	 * 
	 * @param enabled
	 *            enabled
	 */
	void setEnabled(boolean enabled);
}
