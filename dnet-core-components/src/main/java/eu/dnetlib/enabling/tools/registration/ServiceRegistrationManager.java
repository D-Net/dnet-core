package eu.dnetlib.enabling.tools.registration;

import javax.xml.ws.Endpoint;

import org.springframework.beans.factory.annotation.Required;

import eu.dnetlib.enabling.tools.OpaqueResource;

/**
 * A service registration manager manages the registration of the service profile for a given service.
 * 
 * @author marko
 *
 */
public interface ServiceRegistrationManager {
	/**
	 * service to register.
	 * 
	 * @param service service instance
	 */
	@Required
	void setService(Object service);
	
	/**
	 * service endpoint.
	 * 
	 * @param endpoint service endpoint
	 */
	@Required
	void setEndpoint(Endpoint endpoint);
	
	/**
	 * set to true if we want to disable service registration.
	 * 
	 * @param disabled
	 */
	@Required
	void setDisabled(boolean disabled);
	
	/**
	 * returns the service profile associated with the service.
	 * 
	 * @return service profile
	 */
	OpaqueResource getServiceProfile();

	/**
	 * returns the registered service
	 *
	 * @return the service
	 */
	Object getService();

	
}
