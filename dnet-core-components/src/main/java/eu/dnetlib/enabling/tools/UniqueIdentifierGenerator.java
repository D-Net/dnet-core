package eu.dnetlib.enabling.tools;

/**
 * genrate unique identifiers.
 * 
 * @author marko
 *
 */
public interface UniqueIdentifierGenerator {

	/**
	 * generate a new unique identifier.
	 * 
	 * @return string identifier
	 */
	String generateIdentifier();
	
	/**
	 * helps identify valid identifiers.
	 * 
	 * @return string regex
	 */
	String getRegEx();

}
