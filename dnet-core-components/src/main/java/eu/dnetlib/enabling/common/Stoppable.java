package eu.dnetlib.enabling.common;

public interface Stoppable {
	void stop();
	void resume();
	StoppableDetails getStopDetails();
}
