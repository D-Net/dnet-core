package eu.dnetlib.enabling.common;

public class StoppableDetails {

	public enum StopStatus {
		RUNNING, STOPPED, STOPPING
	}

	private String name;
	private String message;
	private StopStatus status;
	
	public StoppableDetails() {}
	
	public StoppableDetails(String name, String message, StopStatus status) {
		this.name = name;
		this.message = message;
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public StopStatus getStatus() {
		return status;
	}

	public void setStatus(StopStatus status) {
		this.status = status;
	}
	
}