package eu.dnetlib.enabling.inspector;

import java.util.Collection;

/**
 * Declare a spring bean and put a list of entry points for each module defining.
 *
 * @author marko
 *
 */
public class StaticEntryPointDescriptorGroup implements EntryPointDescriptorGroup {

	/**
	 * entry point descriptors.
	 */
	private Collection<EntryPointDescriptor> descriptors;

	/**
	 * group name.
	 */
	private String name;

	@Override
	public Collection<EntryPointDescriptor> getDescriptors() {
		return descriptors;
	}

	public void setDescriptors(final Collection<EntryPointDescriptor> descriptors) {
		this.descriptors = descriptors;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}


}
