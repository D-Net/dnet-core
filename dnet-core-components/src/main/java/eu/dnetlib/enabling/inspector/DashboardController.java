package eu.dnetlib.enabling.inspector;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DashboardController extends AbstractInspectorController {
	
	@RequestMapping("/inspector")
	public String dashboard(final Model model) {
		
		return "inspector/dashboard";
	}
	


	
}
