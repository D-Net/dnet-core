package eu.dnetlib.enabling.inspector;

/**
 * Entry point descriptor.
 *
 * @author marko
 *
 */
public class StaticEntryPointDescriptor implements EntryPointDescriptor {

	/**
	 * name.
	 */
	private String name;

	/**
	 * relative url.
	 */
	private String relativeUrl;
	
	/**
	 * Default value for visibility.
	 */
	private boolean hiddenAsDefault = false;
	
	@Override
	public String getRelativeUrl() {
		return relativeUrl;
	}

	public void setRelativeUrl(String relativeUrl) {
		this.relativeUrl = relativeUrl;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setHiddenAsDefault(boolean hiddenAsDefault) {
		this.hiddenAsDefault = hiddenAsDefault;
	}

	@Override
	public boolean isHiddenAsDefault() {
		return hiddenAsDefault;
	}

}
