package eu.dnetlib.enabling.inspector;

/**
 * Used as MVC model for actual entry point descriptor. It contains also a boolean if it's the current page.
 * 
 * @author marko
 * 
 */
public class EntryPointDescriptorModel implements EntryPointDescriptor {
	
	/**
	 * name.
	 */
	private String name;

	/**
	 * relative url.
	 */
	private String relativeUrl;
	
	private boolean current;
	
	/**
	 * Default value for visibility.
	 */
	private boolean hiddenAsDefault = false;

	/**
	 * Compatibility for other users of EntryPointDescriptorModel.
	 * 
	 * @param name
	 * @param relativeUrl
	 * @param current
	 */
	public EntryPointDescriptorModel(final String name, final String relativeUrl, final boolean current) {
			this(name, relativeUrl, current, false);
	}
	
	public EntryPointDescriptorModel(final String name, final String relativeUrl, final boolean current, final boolean hiddenAsDefault) {
		super();
		this.name = name;
		this.relativeUrl = relativeUrl;
		this.current = current;
		this.hiddenAsDefault = hiddenAsDefault;
	}
	
	@Override
	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}
	
	@Override
	public String getRelativeUrl() {
		return relativeUrl;
	}

	public void setRelativeUrl(final String relativeUrl) {
		this.relativeUrl = relativeUrl;
	}

	public boolean isCurrent() {
		return current;
	}

	public void setCurrent(final boolean current) {
		this.current = current;
	}

	@Override
	public boolean isHiddenAsDefault() {
		return hiddenAsDefault;
	}

	public void setHiddenAsDefault(boolean hiddenAsDefault) {
		this.hiddenAsDefault = hiddenAsDefault;
	}
}
