package eu.dnetlib.enabling.inspector;

import java.util.Collection;

/**
 * Entry points are grouped in entry point groups. The master template will probably order groups by name, the order
 * within a group is decided by implementors of this interface.
 *
 * @author marko
 *
 */
public interface EntryPointDescriptorGroup {
	/**
	 * group name.
	 *
	 * @return name
	 */
	String getName();

	/**
	 * descriptors.
	 *
	 * @return descriptors
	 */
	Collection<EntryPointDescriptor> getDescriptors();
}
