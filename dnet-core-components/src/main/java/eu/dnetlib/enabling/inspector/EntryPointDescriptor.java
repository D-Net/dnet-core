package eu.dnetlib.enabling.inspector;

/**
 * Each module can declare beans implementing this interface, defining entry points to inspector controllers defined in
 * the module.
 *
 * <p>
 * The master template will join all these and create the navigation bar
 * </p>
 *
 * <p>
 * Entry point descriptors are grouped in InspectorEntryPointGroups, which contain an ordered list of
 * InspectorEntryPointDescriptors
 * </p>
 *
 * @author marko
 *
 */
public interface EntryPointDescriptor {
	/**
	 * entry point url relative to the baseUri.
	 *
	 * @return url
	 */
	String getRelativeUrl();

	/**
	 * Get entry point name, as displayed in the web page.
	 *
	 * @return entry point name
	 */
	String getName();
	
	/**
	 * Return visible visible as default
	 *
	 * @return visible
	 */
	boolean isHiddenAsDefault();
	
}
