package eu.dnetlib.enabling.is.sn.rmi;

/**
 * Thrown when a subscription request is rejected.
 * 
 * @author claudio
 *
 */
public class SubscriptionRequestRejectedException extends ISSNException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 263095606953662098L;

	public SubscriptionRequestRejectedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	
}
