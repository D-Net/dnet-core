package eu.dnetlib.enabling.is.registry.schema;

import eu.dnetlib.enabling.tools.OpaqueResource;

/**
 * validates the conformity of a resource to a give resource type.
 * 
 * @author marko
 * 
 */
public interface OpaqueResourceValidator {
	/**
	 * check if the given resource is valid according to it's schema.
	 * 
	 * @param resource opaque resource
	 * @throws ValidationException thrown if the validation fails, along with a description of the cause.
	 */
	void validate(OpaqueResource resource) throws ValidationException;
}
