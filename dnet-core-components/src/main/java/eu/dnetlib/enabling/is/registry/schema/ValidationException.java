package eu.dnetlib.enabling.is.registry.schema;


/**
 * encapsulates a schema validation exception.
 * 
 * @author marko
 *
 */
public class ValidationException extends Exception {

	/**
	 * version.
	 */
	private static final long serialVersionUID = -6886927707534508655L;

	/**
	 * construct a validation exception based upon an encapsulated cause.
	 * @param cause cause
	 */
	public ValidationException(final Throwable cause) {
		super(cause);
	}

	/**
	 * construct a validation exception with a message.
	 * 
	 * @param message message
	 */
	public ValidationException(final String message) {
		super(message);
	}

	
}
