package eu.dnetlib.enabling.is.registry;

import eu.dnetlib.enabling.is.registry.rmi.ISRegistryException;

public class ISRegistryDocumentNotFoundException extends ISRegistryException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1304948213334188538L;

	public ISRegistryDocumentNotFoundException(String string, Throwable e) {
		super(string, e);
		// TODO Auto-generated constructor stub
	}

	public ISRegistryDocumentNotFoundException(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public ISRegistryDocumentNotFoundException(Throwable e) {
		super(e);
		// TODO Auto-generated constructor stub
	}

}
