package eu.dnetlib.enabling.is.store;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Required;
import org.xml.sax.SAXException;

import eu.dnetlib.enabling.is.registry.rmi.ISRegistryException;
import eu.dnetlib.enabling.is.registry.rmi.ISRegistryService;
import eu.dnetlib.enabling.locators.UniqueServiceLocator;
import eu.dnetlib.enabling.tools.StreamOpaqueResource;

/**
 * Abstract resource loading code.
 * 
 * @author marko, michele
 * 
 */
public class AbstractContentInitializer {

	/**
	 * logger.
	 */
	private static final Log log = LogFactory.getLog(AbstractContentInitializer.class); // NOPMD by marko on 11/24/08 5:02 PM
	/**
	 * service locator.
	 */
	private UniqueServiceLocator serviceLocator;
	/**
	 * helper class used to bypass the registry and import resources as-is from a backup dump.
	 */
	private BulkResourceImporter bulkImporter;

	private int timeToSleep;

	/**
	 * register a schema from a local resource.
	 * 
	 * @param url
	 *            url
	 * @throws IOException
	 *             happens
	 * @throws ISRegistryException
	 *             could happen
	 */
	protected void registerSchema(final URL url) throws IOException, ISRegistryException {
		final String resourceType = FilenameUtils.getBaseName(url.getPath());
		log.debug("registering schema: " + resourceType);

		final StringWriter writer = new StringWriter();
		IOUtils.copy(url.openStream(), writer);
		ISRegistryService service = null;
		while (service == null) {
			try {
				service = serviceLocator.getService(ISRegistryService.class, true);
				service.addResourceType(resourceType, writer.getBuffer().toString());
				log.info("The is registry service is ready ");
			} catch (Exception e) {
				log.fatal("The is registry service is not ready ", e);
				try {
					Thread.sleep(timeToSleep);
				} catch (InterruptedException e1) {
					log.error(e1);
				}
			}
		}

	}

	/**
	 * register a profile from a local resource.
	 * 
	 * @param url
	 *            url
	 * @throws IOException
	 *             could happen
	 * @throws ISRegistryException
	 *             could happen
	 * @throws ParserConfigurationException
	 *             could happen
	 * @throws SAXException
	 *             could happen
	 * @throws XPathExpressionException
	 *             could happen
	 */
	protected void registerProfile(final URL url) throws IOException, ISRegistryException, XPathExpressionException, SAXException, ParserConfigurationException {
		log.debug("registering profile: " + url);

		bulkImporter.importResource(new StreamOpaqueResource(url.openStream()));
	}

	@Required
	public void setBulkImporter(final BulkResourceImporter bulkImporter) {
		this.bulkImporter = bulkImporter;
	}

	public BulkResourceImporter getBulkImporter() {
		return bulkImporter;
	}

	/**
	 * @return the timeToSleep
	 */
	public int getTimeToSleep() {
		return timeToSleep;
	}

	/**
	 * @param timeToSleep
	 *            the timeToSleep to set
	 */
	@Required
	public void setTimeToSleep(final int timeToSleep) {
		this.timeToSleep = timeToSleep;
	}

	public UniqueServiceLocator getServiceLocator() {
		return serviceLocator;
	}

	@Required
	public void setServiceLocator(final UniqueServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}

}
