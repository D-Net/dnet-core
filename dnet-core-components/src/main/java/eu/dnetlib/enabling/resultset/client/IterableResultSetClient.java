package eu.dnetlib.enabling.resultset.client;

import java.util.Iterator;

import eu.dnetlib.enabling.resultset.rmi.ResultSetService;

/**
 * 
 * @author claudio
 *
 */
public class IterableResultSetClient implements Iterable<String> {

	/**
	 * reference to resultset service.
	 */
	private ResultSetService resultSet;
	
	/**
	 * resultset id
	 */
	private String rsId;
	
	/**
	 * page size.
	 */
	private int pageSize;
	
	/**
	 * timeout
	 */
	private long timeout;
	
	public IterableResultSetClient(ResultSetService resultSet, String rsId, int pageSize) {
		this.resultSet = resultSet;
		this.rsId = rsId;
		this.pageSize = pageSize;
		this.timeout = 0;
	} 
	
	public IterableResultSetClient(ResultSetService resultSet, String rsId, int pageSize, long timeout) {
		this(resultSet, rsId, pageSize);
		this.timeout = timeout;
	} 
	
	@Override
	public Iterator<String> iterator() {
		if (timeout == 0)
			return new ResultSetClientIterator(resultSet, rsId, pageSize);
		return new ResultSetClientIterator(resultSet, rsId, pageSize, timeout);
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

}
