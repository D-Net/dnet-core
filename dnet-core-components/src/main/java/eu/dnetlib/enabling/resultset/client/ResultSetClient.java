package eu.dnetlib.enabling.resultset.client;


import javax.xml.ws.wsaddressing.W3CEndpointReference;

/**
 * 
 * @author claudio
 *
 */
public interface ResultSetClient {

	/**
	 * 
	 * @param epr
	 * @return
	 */
	public IterableResultSetClient getClient(W3CEndpointReference epr);
	
	/**
	 * 
	 * @param epr
	 * @return
	 */
	public IterableResultSetClient getClient(String epr);
	
	/**
	 * 
	 * @param epr
	 * @param pageSize
	 * @return
	 */
	public IterableResultSetClient getClient(W3CEndpointReference epr, int pageSize);
	
	/**
	 * 
	 * @param epr
	 * @param pageSize
	 * @return
	 */
	public IterableResultSetClient getClient(String epr, int pageSize);
	
	
}
