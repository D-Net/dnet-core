package eu.dnetlib.enabling.resultset.client.utils;

import java.io.StringReader;

import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.EndpointReference;
import javax.xml.ws.wsaddressing.W3CEndpointReference;

/**
 * 
 * @author claudio
 *
 */
public class EPRUtils {

	/**
	 * builds an epr from its string representation
	 * 
	 * @param epr
	 * 			String epr
	 * @return 
	 * 			W3CEndpointReference epr
	 * 		
	 */
	public W3CEndpointReference getEpr(String epr) {
		return (W3CEndpointReference) EndpointReference.readFrom(new StreamSource(new StringReader(epr)));
	}
	
}
