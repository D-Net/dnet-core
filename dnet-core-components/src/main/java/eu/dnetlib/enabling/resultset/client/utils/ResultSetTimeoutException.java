package eu.dnetlib.enabling.resultset.client.utils;

public class ResultSetTimeoutException extends RuntimeException {

	private static final long serialVersionUID = -3713991101055085620L;
	
	public ResultSetTimeoutException(String message) {
		super(message);
	}

}
