package eu.dnetlib.enabling.resultset.client.utils;

public class ResultSetRuntimeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5131499590327995897L;
	
	public ResultSetRuntimeException(String message) {
		super(message);
	}
	
	public ResultSetRuntimeException(Throwable e) {
		super(e);
	}

}
