package eu.dnetlib.enabling.resultset.rmi;

import eu.dnetlib.common.rmi.RMIException;

public class ResultSetException extends RMIException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7130554407601059627L;

	public ResultSetException(Throwable e) {
		super(e);
		// TODO Auto-generated constructor stub
	}

	public ResultSetException(String string) {
		super(string);
	}

}
