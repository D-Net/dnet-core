package eu.dnetlib.enabling.datasources.common;

public class DsmForbiddenException extends Exception {

	private int code;

	public DsmForbiddenException(int code, String msg) {
		super(msg);
		this.code = code;
	}

	public DsmForbiddenException(int code, Throwable e) {
		super(e);
		this.code = code;
	}

	public DsmForbiddenException(int code, String msg, Throwable e) {
		super(msg, e);
		this.code = code;
	}

	public DsmForbiddenException(String msg) {
		this(403, msg);
	}

	public int getCode() {
		return code;
	}

	public void setCode(final int code) {
		this.code = code;
	}

}
