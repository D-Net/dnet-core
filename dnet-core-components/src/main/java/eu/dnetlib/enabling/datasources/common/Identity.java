package eu.dnetlib.enabling.datasources.common;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Created by claudio on 13/04/2017.
 */
@MappedSuperclass
public class Identity {

	@Id
	protected String pid;

	protected String issuertype;

	public Identity() {}

	public String getPid() {
		return this.pid;
	}

	public String getIssuertype() {
		return this.issuertype;
	}

	public Identity setPid(final String pid) {
		this.pid = pid;
		return this;
	}

	public Identity setIssuertype(final String issuertype) {
		this.issuertype = issuertype;
		return this;
	}

}
