package eu.dnetlib.enabling.datasources.common;

/**
 * Created by claudio on 12/09/16.
 */
public enum AggregationStage {
	COLLECT, TRANSFORM;

	public static AggregationStage parse(final String s) {
		switch (s) {
			case "collect":
			case "collection":
			case "COLLECT":
			case "COLLECTION":
				return AggregationStage.COLLECT;
			case "transform":
			case "transformation":
			case "TRANSFORM":
			case "TRANSFORMATION":
			case "transformDatasets":
			case "transformPublications":
				return AggregationStage.TRANSFORM;
		}
		throw new IllegalArgumentException("invalid AggregationStage: " + s);
	}
}
