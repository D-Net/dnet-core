package eu.dnetlib.enabling.datasources.common;

public interface ApiParam {

	String getValue();

	void setValue(String value);

	String getParam();

	void setParam(String param);

}
