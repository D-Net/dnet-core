package eu.dnetlib.enabling.datasources.common;

import javax.xml.bind.annotation.XmlRootElement;

public class SearchApisEntry implements Comparable<SearchApisEntry> {

	private String id;
	private String compliance;
	private String protocol;
	private boolean active;
	private String repoId = "unknown";
	private String repoName = "unknown";
	private String alternativeName = "unknown";
	private String repoOrganization = "unknown";
	private String repoCountry = "-";
	private String repoPrefix = "";
	private String aggrDate = "";
	private int aggrTotal = 0;

	public SearchApisEntry() {}

	public SearchApisEntry(final String id, final String compliance, final String protocol, final boolean active, final String repoId,
			final String repoName, final String repoCountry,
			final String repoPrefix, final String aggrDate, final int aggrTotal) {
		this.id = id;
		this.compliance = compliance;
		this.protocol = protocol;
		this.active = active;
		this.repoId = repoId;
		this.repoName = repoName;
		this.repoCountry = repoCountry;
		this.repoPrefix = repoPrefix;
		this.aggrDate = aggrDate;
		this.aggrTotal = aggrTotal;
	}

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getCompliance() {
		return compliance;
	}

	public void setCompliance(final String compliance) {
		this.compliance = compliance;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(final boolean active) {
		this.active = active;
	}

	public String getRepoId() {
		return repoId;
	}

	public void setRepoId(final String repoId) {
		this.repoId = repoId;
	}

	public String getRepoName() {
		return repoName;
	}

	public void setRepoName(final String repoName) {
		this.repoName = repoName;
	}

	public String getRepoCountry() {
		return repoCountry;
	}

	public void setRepoCountry(final String repoCountry) {
		this.repoCountry = repoCountry;
	}

	public String getRepoPrefix() {
		return repoPrefix;
	}

	public void setRepoPrefix(final String repoPrefix) {
		this.repoPrefix = repoPrefix;
	}

	@Override
	public int compareTo(final SearchApisEntry e) {
		return compliance.compareTo(e.getCompliance());
	}

	public String getAggrDate() {
		return aggrDate;
	}

	public void setAggrDate(final String aggrDate) {
		this.aggrDate = aggrDate;
	}

	public int getAggrTotal() {
		return aggrTotal;
	}

	public void setAggrTotal(final int aggrTotal) {
		this.aggrTotal = aggrTotal;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(final String protocol) {
		this.protocol = protocol;
	}

	public String getAlternativeName() {
		return alternativeName;
	}

	public void setAlternativeName(String alternativeName) {
		this.alternativeName = alternativeName;
	}

	public String getRepoOrganization() {
		return repoOrganization;
	}

	public void setRepoOrganization(String repoOrganization) {
		this.repoOrganization = repoOrganization;
	}

}
