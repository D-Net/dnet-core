package eu.dnetlib.enabling.datasources.common;

import java.util.HashSet;
import java.util.Set;

public class SimpleDatasource implements Comparable<SimpleDatasource> {

	private String id;
	private String name;
	private String typology;
	private String origId;
	private Set<String> apis = new HashSet<>();
	private boolean valid;

	public String getId() {
		return id;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(final boolean valid) {
		this.valid = valid;
	}

	public String getTypology() {
		return typology;
	}

	public void setTypology(final String typology) {
		this.typology = typology;
	}

	public String getOrigId() {
		return origId;
	}

	public void setOrigId(final String origId) {
		this.origId = origId;
	}

	public Set<String> getApis() {
		return apis;
	}

	public void setApis(final Set<String> apis) {
		this.apis = apis;
	}

	@Override
	public int compareTo(final SimpleDatasource e) {
		return getName().compareTo(e.getName());
	}

}
