package eu.dnetlib.enabling.datasources.common;

public class DsmException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8980196353591772127L;

	private int code;

	public DsmException(int code, String msg) {
		super(msg);
		this.code = code;
	}

	public DsmException(int code, Throwable e) {
		super(e);
		this.code = code;
	}

	public DsmException(int code, String msg, Throwable e) {
		super(msg, e);
		this.code = code;
	}

	public DsmException(String msg) {
		this(500, msg);
	}

	public int getCode() {
		return code;
	}

	public void setCode(final int code) {
		this.code = code;
	}
}
