package eu.dnetlib.enabling.datasources.common;

public class BrowseTermImpl implements BrowseTerm {

	private String code;
	private String term;
	private long total;

	public BrowseTermImpl() {}

	public BrowseTermImpl(final String code, final String term, final int total) {
		this.code = code;
		this.term = term;
		this.total = total;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public void setCode(final String code) {
		this.code = code;
	}

	@Override
	public String getTerm() {
		return term;
	}

	@Override
	public void setTerm(final String term) {
		this.term = term;
	}

	@Override
	public long getTotal() {
		return total;
	}

	@Override
	public void setTotal(final long total) {
		this.total = total;
	}
}
