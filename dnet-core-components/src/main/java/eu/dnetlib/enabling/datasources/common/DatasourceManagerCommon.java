package eu.dnetlib.enabling.datasources.common;

import java.util.List;

public interface DatasourceManagerCommon<DS extends Datasource<?, ?>, API extends Api<?>> {

	DS getDs(String id) throws DsmException;

	List<? extends API> getApis(String dsId) throws DsmException;

	void deleteDs(String dsId) throws DsmException;

	void deleteApi(String dsId, String apiId) throws DsmException;

	void addApi(API api) throws DsmException;

	void setManaged(String id, boolean managed) throws DsmException;

	boolean isManaged(String id) throws DsmException;

	void saveDs(DS datasource) throws DsmException;

	void updateCompliance(String dsId, String apiId, String compliance, boolean override) throws DsmException;

}
