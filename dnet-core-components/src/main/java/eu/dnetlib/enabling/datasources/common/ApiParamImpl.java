package eu.dnetlib.enabling.datasources.common;

public class ApiParamImpl implements ApiParam {

	private String param;
	private String value;

	@Override
	public String getParam() {
		return param;
	}

	@Override
	public void setParam(final String param) {
		this.param = param;
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public void setValue(final String value) {
		this.value = value;
	}
}
