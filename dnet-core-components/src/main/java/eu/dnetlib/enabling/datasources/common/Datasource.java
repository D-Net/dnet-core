package eu.dnetlib.enabling.datasources.common;

import java.sql.Date;
import java.util.Set;
import javax.persistence.*;

/**
 * Datasource
 */
@MappedSuperclass
public class Datasource<ORG extends Organization, ID extends Identity> {

	@Id
	protected String id;

	@Column(nullable = false)
	protected String officialname;
	protected String englishname;
	protected String websiteurl;
	protected String logourl;
	protected String contactemail;
	protected Double latitude;
	protected Double longitude;
	protected String timezone;

	@Column(name = "namespaceprefix", columnDefinition = "bpchar(12)", nullable = false, updatable = false)
	protected String namespaceprefix;

	protected String languages;

	protected String od_contenttypes;
	protected String collectedfrom;
	protected Date dateofvalidation;
	protected String typology;
	protected String provenanceaction;
	protected Date dateofcollection;
	protected String platform;

	@Column(name = "activationid")
	protected String activationId;
	protected String description;

	protected Date releasestartdate;
	protected Date releaseenddate;
	protected String missionstatementurl;
	protected Boolean dataprovider;
	protected Boolean serviceprovider;

	protected String databaseaccesstype;
	protected String datauploadtype;
	protected String databaseaccessrestriction;
	protected String datauploadrestriction;

	protected Boolean versioning;
	protected String citationguidelineurl;
	protected String qualitymanagementkind;
	protected String pidsystems;

	protected String certificates;
	protected String aggregator;

	protected String issn;
	protected String eissn;
	protected String lissn;

	protected String registeredby;

	private Date registrationdate;

	protected String subjects;

	protected Boolean managed;

	@Column(name = "consenttermsofuse")
	protected Boolean consentTermsOfUse;

	@Column(name = "fulltextdownload")
	protected Boolean fullTextDownload;

	@ManyToMany(
			cascade = { CascadeType.PERSIST, CascadeType.MERGE },
			fetch = FetchType.LAZY)
	@JoinTable(
			name = "dsm_datasource_organization",
			joinColumns = @JoinColumn(name="datasource"),
			inverseJoinColumns = @JoinColumn(name="organization"))
	protected Set<ORG> organizations;

	@ManyToMany(
			cascade = { CascadeType.PERSIST, CascadeType.MERGE },
			fetch = FetchType.LAZY )
	@JoinTable(
			name = "dsm_datasourcepids",
			joinColumns = @JoinColumn(name = "datasource"),
			inverseJoinColumns = @JoinColumn(name = "pid"))
	protected Set<ID> identities;

	public Datasource() {}

	public String getId() {
		return id;
	}

	public String getOfficialname() {
		return officialname;
	}

	public String getEnglishname() {
		return englishname;
	}

	public String getWebsiteurl() {
		return websiteurl;
	}

	public String getLogourl() {
		return logourl;
	}

	public String getContactemail() {
		return contactemail;
	}

	public Double getLatitude() {
		return latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public String getTimezone() {
		return timezone;
	}

	public String getNamespaceprefix() {
		return namespaceprefix;
	}

	public String getLanguages() {
		return languages;
	}

	public String getOd_contenttypes() {
		return od_contenttypes;
	}

	public String getCollectedfrom() {
		return collectedfrom;
	}

	public Date getDateofvalidation() {
		return dateofvalidation;
	}

	public String getTypology() {
		return typology;
	}

	public String getProvenanceaction() {
		return provenanceaction;
	}

	public Date getDateofcollection() {
		return dateofcollection;
	}

	public String getPlatform() {
		return platform;
	}

	public String getActivationId() {
		return activationId;
	}

	public String getDescription() {
		return description;
	}

	public Date getReleasestartdate() {
		return releasestartdate;
	}

	public Date getReleaseenddate() {
		return releaseenddate;
	}

	public String getMissionstatementurl() {
		return missionstatementurl;
	}

	public Boolean getDataprovider() {
		return dataprovider;
	}

	public Boolean getServiceprovider() {
		return serviceprovider;
	}

	public String getDatabaseaccesstype() {
		return databaseaccesstype;
	}

	public String getDatauploadtype() {
		return datauploadtype;
	}

	public String getDatabaseaccessrestriction() {
		return databaseaccessrestriction;
	}

	public String getDatauploadrestriction() {
		return datauploadrestriction;
	}

	public Boolean getVersioning() {
		return versioning;
	}

	public String getCitationguidelineurl() {
		return citationguidelineurl;
	}

	public String getQualitymanagementkind() {
		return qualitymanagementkind;
	}

	public String getPidsystems() {
		return pidsystems;
	}

	public String getCertificates() {
		return certificates;
	}

	public String getAggregator() {
		return aggregator;
	}

	public String getIssn() {
		return issn;
	}

	public String getEissn() {
		return eissn;
	}

	public String getLissn() {
		return lissn;
	}

	public String getRegisteredby() {
		return registeredby;
	}

	public Date getRegistrationdate() {
		return registrationdate;
	}

	public String getSubjects() {
		return subjects;
	}

	public Boolean getManaged() {
		return managed;
	}

	public Set<ORG> getOrganizations() {
		return organizations;
	}

	public Set<ID> getIdentities() {
		return identities;
	}

	public Datasource<ORG, ID> setId(final String id) {
		this.id = id;
		return this;
	}

	public Datasource<ORG, ID> setOfficialname(final String officialname) {
		this.officialname = officialname;
		return this;
	}

	public Datasource<ORG, ID> setEnglishname(final String englishname) {
		this.englishname = englishname;
		return this;
	}

	public Datasource<ORG, ID> setWebsiteurl(final String websiteurl) {
		this.websiteurl = websiteurl;
		return this;
	}

	public Datasource<ORG, ID> setLogourl(final String logourl) {
		this.logourl = logourl;
		return this;
	}

	public Datasource<ORG, ID> setContactemail(final String contactemail) {
		this.contactemail = contactemail;
		return this;
	}

	public Datasource<ORG, ID> setLatitude(final Double latitude) {
		this.latitude = latitude;
		return this;
	}

	public Datasource<ORG, ID> setLongitude(final Double longitude) {
		this.longitude = longitude;
		return this;
	}

	public Datasource<ORG, ID> setTimezone(final String timezone) {
		this.timezone = timezone;
		return this;
	}

	public Datasource<ORG, ID> setNamespaceprefix(final String namespaceprefix) {
		this.namespaceprefix = namespaceprefix;
		return this;
	}

	public Datasource<ORG, ID> setLanguages(final String languages) {
		this.languages = languages;
		return this;
	}

	public Datasource<ORG, ID> setOd_contenttypes(final String od_contenttypes) {
		this.od_contenttypes = od_contenttypes;
		return this;
	}

	public Datasource<ORG, ID> setCollectedfrom(final String collectedfrom) {
		this.collectedfrom = collectedfrom;
		return this;
	}

	public Datasource<ORG, ID> setDateofvalidation(final Date dateofvalidation) {
		this.dateofvalidation = dateofvalidation;
		return this;
	}

	public Datasource<ORG, ID> setTypology(final String typology) {
		this.typology = typology;
		return this;
	}

	public Datasource<ORG, ID> setProvenanceaction(final String provenanceaction) {
		this.provenanceaction = provenanceaction;
		return this;
	}

	public Datasource<ORG, ID> setDateofcollection(final Date dateofcollection) {
		this.dateofcollection = dateofcollection;
		return this;
	}

	public Datasource<ORG, ID> setPlatform(final String platform) {
		this.platform = platform;
		return this;
	}

	public Datasource<ORG, ID> setActivationId(final String activationId) {
		this.activationId = activationId;
		return this;
	}

	public Datasource<ORG, ID> setDescription(final String description) {
		this.description = description;
		return this;
	}

	public Datasource<ORG, ID> setReleasestartdate(final Date releasestartdate) {
		this.releasestartdate = releasestartdate;
		return this;
	}

	public Datasource<ORG, ID> setReleaseenddate(final Date releaseenddate) {
		this.releaseenddate = releaseenddate;
		return this;
	}

	public Datasource<ORG, ID> setMissionstatementurl(final String missionstatementurl) {
		this.missionstatementurl = missionstatementurl;
		return this;
	}

	public Datasource<ORG, ID> setDataprovider(final Boolean dataprovider) {
		this.dataprovider = dataprovider;
		return this;
	}

	public Datasource<ORG, ID> setServiceprovider(final Boolean serviceprovider) {
		this.serviceprovider = serviceprovider;
		return this;
	}

	public Datasource<ORG, ID> setDatabaseaccesstype(final String databaseaccesstype) {
		this.databaseaccesstype = databaseaccesstype;
		return this;
	}

	public Datasource<ORG, ID> setDatauploadtype(final String datauploadtype) {
		this.datauploadtype = datauploadtype;
		return this;
	}

	public Datasource<ORG, ID> setDatabaseaccessrestriction(final String databaseaccessrestriction) {
		this.databaseaccessrestriction = databaseaccessrestriction;
		return this;
	}

	public Datasource<ORG, ID> setDatauploadrestriction(final String datauploadrestriction) {
		this.datauploadrestriction = datauploadrestriction;
		return this;
	}

	public Datasource<ORG, ID> setVersioning(final Boolean versioning) {
		this.versioning = versioning;
		return this;
	}

	public Datasource<ORG, ID> setCitationguidelineurl(final String citationguidelineurl) {
		this.citationguidelineurl = citationguidelineurl;
		return this;
	}

	public Datasource<ORG, ID> setQualitymanagementkind(final String qualitymanagementkind) {
		this.qualitymanagementkind = qualitymanagementkind;
		return this;
	}

	public Datasource<ORG, ID> setPidsystems(final String pidsystems) {
		this.pidsystems = pidsystems;
		return this;
	}

	public Datasource<ORG, ID> setCertificates(final String certificates) {
		this.certificates = certificates;
		return this;
	}

	public Datasource<ORG, ID> setAggregator(final String aggregator) {
		this.aggregator = aggregator;
		return this;
	}

	public Datasource<ORG, ID> setIssn(final String issn) {
		this.issn = issn;
		return this;
	}

	public Datasource<ORG, ID> setEissn(final String eissn) {
		this.eissn = eissn;
		return this;
	}

	public Datasource<ORG, ID> setLissn(final String lissn) {
		this.lissn = lissn;
		return this;
	}

	public Datasource<ORG, ID> setRegisteredby(final String registeredby) {
		this.registeredby = registeredby;
		return this;
	}

	public Datasource setRegistrationdate(final Date registrationdate) {
		this.registrationdate = registrationdate;
		return this;
	}

	public Datasource<ORG, ID> setSubjects(final String subjects) {
		this.subjects = subjects;
		return this;
	}

	public Datasource<ORG, ID> setManaged(final Boolean managed) {
		this.managed = managed;
		return this;
	}

	public Datasource<ORG, ID> setOrganizations(final Set<ORG> organizations) {
		this.organizations = organizations;
		return this;
	}

	public Datasource<ORG, ID> setIdentities(final Set<ID> identities) {
		this.identities = identities;
		return this;
	}

	public Boolean getConsentTermsOfUse() {
		return consentTermsOfUse;
	}

	public void setConsentTermsOfUse(Boolean consentTermsOfUse) {
		this.consentTermsOfUse = consentTermsOfUse;
	}

	public Boolean getFullTextDownload() {
		return fullTextDownload;
	}

	public void setFullTextDownload(Boolean fullTextDownload) {
		this.fullTextDownload = fullTextDownload;
	}


}
