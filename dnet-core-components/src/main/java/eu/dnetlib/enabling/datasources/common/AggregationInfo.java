package eu.dnetlib.enabling.datasources.common;

public abstract class AggregationInfo {

	private int numberOfRecords;

	private String date;

	private AggregationStage aggregationStage;

	private boolean indexedVersion = false;

	public AggregationInfo() {}

	public int getNumberOfRecords() {
		return numberOfRecords;
	}

	public void setNumberOfRecords(final int numberOfRecords) {
		this.numberOfRecords = numberOfRecords;
	}

	public String getDate() {
		return date;
	}

	public void setDate(final String date) {
		this.date = date;
	}

	public AggregationStage getAggregationStage() {
		return aggregationStage;
	}

	public void setAggregationStage(final AggregationStage aggregationStage) {
		this.aggregationStage = aggregationStage;
	}

	public boolean isIndexedVersion() {
		return indexedVersion;
	}

	public void setIndexedVersion(final boolean indexedVersion) {
		this.indexedVersion = indexedVersion;
	}
}
