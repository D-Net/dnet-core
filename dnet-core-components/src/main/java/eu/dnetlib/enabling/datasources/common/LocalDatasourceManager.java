package eu.dnetlib.enabling.datasources.common;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface LocalDatasourceManager<DS extends Datasource<?, ?>, API extends Api<?>> extends DatasourceManagerCommon<DS, API> {

	Set<String> listManagedDatasourceIds() throws DsmRuntimeException;

	List<SimpleDatasource> searchDatasourcesByType(String type) throws DsmException;

	List<? extends SearchApisEntry> searchApis(String field, Object value) throws DsmException;

	List<? extends BrowsableField> listBrowsableFields() throws DsmException;

	List<BrowseTerm> browseField(String field) throws DsmException;

	void setActive(String dsId, String apiId, boolean active) throws DsmException;

	boolean isActive(String dsId, String apiId) throws DsmException;

	void setLastCollectionInfo(String dsId, String apiId, String mdId, Integer size, Date date) throws DsmException;

	void setLastAggregationInfo(String dsId, String apiId, String mdId, Integer size, Date date) throws DsmException;

	void setLastDownloadInfo(String dsId, String apiId, String objId, Integer size, Date date) throws DsmException;

	void setLastValidationJob(String dsId, String apiId, String jobId) throws DsmException;

	void updateApiDetails(String dsId, String apiId, String metadataIdentifierPath, String baseUrl, Map<String, String> params) throws DsmException;

	boolean isRemovable(String dsId, String apiId) throws DsmException;

	void regenerateProfiles() throws DsmException;

}
