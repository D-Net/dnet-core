package eu.dnetlib.enabling.datasources.common;

public interface BrowseTerm {

	String getCode();

	void setCode(String code);

	String getTerm();

	void setTerm(String term);

	long getTotal();

	void setTotal(long total);

}
