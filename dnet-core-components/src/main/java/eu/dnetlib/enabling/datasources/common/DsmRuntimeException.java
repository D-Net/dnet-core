package eu.dnetlib.enabling.datasources.common;

public class DsmRuntimeException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 8980196353591772127L;

	public DsmRuntimeException(String msg) {
		super(msg);
	}

	public DsmRuntimeException(String msg, Throwable e) {
		super(msg, e);
	}

	public DsmRuntimeException(Throwable e) {
		super(e);
	}

}
