package eu.dnetlib.enabling.datasources.common;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.google.common.collect.ComparisonChain;

/**
 * Created by claudio on 13/04/2017.
 */
@Embeddable
@MappedSuperclass
public class ApiParamKey<A extends Api> implements Serializable {

	/**
	 * 
	 */
	protected static final long serialVersionUID = 1640636806392015938L;

	@ManyToOne
	@JoinColumn(name = "api")
	protected A api = null;

	protected String param;

	public ApiParamKey() {}

	public String getParam() {
		return param;
	}

	public ApiParamKey setParam(final String param) {
		this.param = param;
		return this;
	}

	public A getApi() {
		return api;
	}

	public void setApi(final A api) {
		this.api = api;
	}

	public int hashCode() {
		return Objects.hash(getParam(), getApi().getId());
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) { return true; }
		if (o == null || getClass() != o.getClass()) { return false; }
		ApiParamKey apk = (ApiParamKey) o;
		return ComparisonChain.start()
				.compare(getParam(), apk.getParam())
				.compare(getApi(), apk.getApi())
				.result() == 0;
	}

}
