package eu.dnetlib.enabling.datasources.common;

public class DsmNotFoundException extends Exception {

	private int code;

	public DsmNotFoundException(int code, String msg) {
		super(msg);
		this.code = code;
	}

	public DsmNotFoundException(int code, Throwable e) {
		super(e);
		this.code = code;
	}

	public DsmNotFoundException(int code, String msg, Throwable e) {
		super(msg, e);
		this.code = code;
	}

	public DsmNotFoundException(String msg) {
		this(404, msg);
	}

	public int getCode() {
		return code;
	}

	public void setCode(final int code) {
		this.code = code;
	}
}
