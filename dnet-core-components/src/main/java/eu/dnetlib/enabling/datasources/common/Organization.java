package eu.dnetlib.enabling.datasources.common;

import java.sql.Date;
import java.util.Set;
import javax.persistence.*;

@MappedSuperclass
public class Organization<DS extends Datasource<?, ?>> {

	@Id
	protected String id;
	protected String legalshortname;
	protected String legalname;
	protected String websiteurl;
	protected String logourl;

	protected String country;
	protected String collectedfrom;

	protected Date dateofcollection;
	protected String provenanceaction;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER, mappedBy = "organizations")
	protected Set<DS> datasources;

	public Organization() {}

	public String getId() {
		return id;
	}

	public String getLegalshortname() {
		return legalshortname;
	}

	public String getLegalname() {
		return legalname;
	}

	public String getWebsiteurl() {
		return websiteurl;
	}

	public String getLogourl() {
		return logourl;
	}

	public String getCountry() {
		return country;
	}

	public String getCollectedfrom() {
		return collectedfrom;
	}

	public Date getDateofcollection() {
		return dateofcollection;
	}

	public String getProvenanceaction() {
		return provenanceaction;
	}

	public Organization<DS> setId(final String id) {
		this.id = id;
		return this;
	}

	public Organization<DS> setLegalshortname(final String legalshortname) {
		this.legalshortname = legalshortname;
		return this;
	}

	public Organization<DS> setLegalname(final String legalname) {
		this.legalname = legalname;
		return this;
	}

	public Organization<DS> setWebsiteurl(final String websiteurl) {
		this.websiteurl = websiteurl;
		return this;
	}

	public Organization<DS> setLogourl(final String logourl) {
		this.logourl = logourl;
		return this;
	}

	public Organization<DS> setCountry(final String country) {
		this.country = country;
		return this;
	}

	public Organization<DS> setCollectedfrom(final String collectedfrom) {
		this.collectedfrom = collectedfrom;
		return this;
	}

	public Organization<DS> setDateofcollection(final Date dateofcollection) {
		this.dateofcollection = dateofcollection;
		return this;
	}

	public Organization<DS> setProvenanceaction(final String provenanceaction) {
		this.provenanceaction = provenanceaction;
		return this;
	}

	public Set<DS> getDatasources() {
		return datasources;
	}

	public void setDatasources(final Set<DS> datasources) {
		this.datasources = datasources;
	}

}
