package eu.dnetlib.enabling.database.rmi;

import java.util.Date;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.wsaddressing.W3CEndpointReference;

import eu.dnetlib.common.rmi.BaseService;

@WebService(targetNamespace = "http://services.dnetlib.eu/")
public interface DatabaseService extends BaseService {

	@WebMethod(operationName = "dumpTable")
	W3CEndpointReference dumpTable(@WebParam(name = "db") String db, @WebParam(name = "table") String table) throws DatabaseException;

	@WebMethod(operationName = "dumpTableAndLogs")
	W3CEndpointReference dumpTableAndLogs(
			@WebParam(name = "db") String db,
			@WebParam(name = "table") String table,
			@WebParam(name = "from") Date from,
			@WebParam(name = "until") Date until) throws DatabaseException;

	@WebMethod(operationName = "importFromEPR")
	void importFromEPR(@WebParam(name = "db") String db, @WebParam(name = "epr") W3CEndpointReference epr, @WebParam(name = "xslt") String xslt)
			throws DatabaseException;

	@WebMethod(operationName = "searchSQL")
	W3CEndpointReference searchSQL(@WebParam(name = "db") String db, @WebParam(name = "sql") String sql) throws DatabaseException;
	
	@WebMethod(operationName = "alternativeSearchSQL")
	W3CEndpointReference alternativeSearchSQL(@WebParam(name = "db") String db,
			@WebParam(name = "sql") String sql,
			@WebParam(name = "sqlForSize") String sqlForSize) throws DatabaseException;

	@WebMethod(operationName = "updateSQL")
	void updateSQL(@WebParam(name = "db") String db, @WebParam(name = "sql") String sql) throws DatabaseException;

	@WebMethod(operationName = "xsltSearchSQL")
	W3CEndpointReference xsltSearchSQL(@WebParam(name = "db") String db, @WebParam(name = "sql") String sql, @WebParam(name = "xslt") String xslt)
			throws DatabaseException;

	@WebMethod(operationName = "alternativeXsltSearchSQL")
	W3CEndpointReference alternativeXsltSearchSQL(@WebParam(name = "db") String db,
			@WebParam(name = "sql") String sql,
			@WebParam(name = "sqlForSize") String sqlForSize,
			@WebParam(name = "xslt") String xslt) throws DatabaseException;

	@WebMethod(operationName = "contains")
	boolean contains(@WebParam(name = "db") String db,
			@WebParam(name = "table") String table,
			@WebParam(name = "column") String column,
			@WebParam(name = "value") String value) throws DatabaseException;

}
