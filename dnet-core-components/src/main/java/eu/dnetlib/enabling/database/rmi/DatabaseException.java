package eu.dnetlib.enabling.database.rmi;

import eu.dnetlib.common.rmi.RMIException;

public class DatabaseException extends RMIException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8464953372238046419L;
	
	public DatabaseException(Throwable cause) {
		super(cause);
	}

	public DatabaseException(String message, Throwable cause) {
		super(message, cause);
	}

	public DatabaseException(String message) {
		super(message);
	}
}
