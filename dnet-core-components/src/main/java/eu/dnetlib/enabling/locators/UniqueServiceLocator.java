package eu.dnetlib.enabling.locators;

import java.util.Comparator;
import java.util.Set;

import eu.dnetlib.common.rmi.BaseService;

public interface UniqueServiceLocator {
	<T extends BaseService> T getService(Class<T> clazz);
	<T extends BaseService> T getService(Class<T> clazz, Comparator<ServiceRunningInstance> comparator);
	<T extends BaseService> T getService(Class<T> clazz, String profileId);
	<T extends BaseService> T getService(Class<T> clazz, boolean local);
	
	<T extends BaseService> String getServiceId(Class<T> clazz);
	<T extends BaseService> String getServiceId(Class<T> clazz, Comparator<ServiceRunningInstance> comparator);
	<T extends BaseService> String getServiceId(Class<T> clazz, String profileId);

	<T extends BaseService> Set<T> getAllServices(Class<T> clazz);
	<T extends BaseService> Set<String> getAllServiceIds(Class<T> clazz);
}
