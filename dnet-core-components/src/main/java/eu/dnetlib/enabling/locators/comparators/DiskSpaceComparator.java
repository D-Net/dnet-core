package eu.dnetlib.enabling.locators.comparators;

import java.util.Comparator;

import eu.dnetlib.enabling.locators.ServiceRunningInstance;

public class DiskSpaceComparator implements Comparator<ServiceRunningInstance> {

	@Override
	public int compare(final ServiceRunningInstance s0, ServiceRunningInstance s1) {
		return Integer.compare(s0.getUsedDiskSpace(), s1.getUsedDiskSpace());
	}

}
