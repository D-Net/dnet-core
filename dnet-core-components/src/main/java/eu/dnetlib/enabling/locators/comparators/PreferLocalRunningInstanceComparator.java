package eu.dnetlib.enabling.locators.comparators;

import java.util.Comparator;

import eu.dnetlib.enabling.locators.ServiceRunningInstance;

public class PreferLocalRunningInstanceComparator implements Comparator<ServiceRunningInstance> {

	@Override
	public int compare(final ServiceRunningInstance s1, final ServiceRunningInstance s2) {
		if (s1.isLocal()) {
			return -1;
		} else if (s2.isLocal()) {
			return 1;
		} else {
			return 0;
		}
	}

}
