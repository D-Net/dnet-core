package eu.dnetlib.enabling.locators.comparators;

import java.util.Comparator;

import eu.dnetlib.enabling.locators.ServiceRunningInstance;

public class HandledDatastructuresComparator implements Comparator<ServiceRunningInstance> {

	@Override
	public int compare(final ServiceRunningInstance s1, final ServiceRunningInstance s2) {
		return Integer.compare(s1.getHandledDatastructures(), s2.getHandledDatastructures());
	}

}
