package eu.dnetlib.enabling.locators;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.*;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import eu.dnetlib.common.rmi.BaseService;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService;
import eu.dnetlib.enabling.tools.registration.ServiceNameResolver;
import eu.dnetlib.enabling.tools.registration.ValidatingServiceRegistrationManagerImpl;
import eu.dnetlib.miscutils.collections.EnsureCollection;
import eu.dnetlib.soap.cxf.StandaloneCxfEndpointReferenceBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class DefaultUniqueServiceLocator implements UniqueServiceLocator, ApplicationContextAware {

	private ApplicationContext appContext;

	private Comparator<ServiceRunningInstance> defaultComparator; // = new
	// PreferLocalRunningInstanceComparator();

	/**
	 * An instance of isLookupService (local or stub)
	 */
	@Autowired
	private ISLookUpService isLookupService;

	@Autowired
	private ServiceNameResolver serviceNameResolver;

	/**
	 * XML Parser
	 */
	private SAXReader reader = new SAXReader();

	/**
	 * build epr.
	 */
	@Autowired
	private StandaloneCxfEndpointReferenceBuilder eprBuilder;

	/**
	 * logger.
	 */
	private static final Log log = LogFactory.getLog(DefaultUniqueServiceLocator.class);

	@Override
	public <T extends BaseService> T getService(final Class<T> clazz) {
		return getService(clazz, true);
	}

	@Override
	public <T extends BaseService> T getService(final Class<T> clazz, final Comparator<ServiceRunningInstance> comparator) {
		final String serviceName = serviceNameResolver.getName(clazz);
		return findRunningInstances(serviceName, comparator).get(0).obtainClient(clazz, eprBuilder);
	}

	@Override
	public <T extends BaseService> T getService(final Class<T> clazz, final String profileId) {
		final String profile = obtainServiceProfile(profileId);

		try {
			return obtainRunningInstance(profile, obtainLocalServices()).obtainClient(clazz, eprBuilder);
		} catch (Exception e) {
			log.error("cannot instantiate service from id: " + profileId, e);
			throw new IllegalStateException("cannot instantiate service from id: " + profileId, e);
		}
	}

	@Override
	public <T extends BaseService> T getService(final Class<T> clazz, final boolean local) {
		if (clazz.isInstance(isLookupService)) return clazz.cast(isLookupService);

		if (local) {
			try {
				final Map<String, T> beans = appContext.getBeansOfType(clazz);
				if ((beans != null) && !beans.isEmpty()) return beans.values().iterator().next();
			} catch (Throwable e) {
				log.warn("No beans found in context, class " + clazz);
			}
		}

		return getService(clazz, defaultComparator);
	}

	@Override
	public <T extends BaseService> String getServiceId(final Class<T> clazz) {
		return getServiceId(clazz, defaultComparator);
	}

	@Override
	public <T extends BaseService> String getServiceId(final Class<T> clazz, final Comparator<ServiceRunningInstance> comparator) {
		return findRunningInstances(serviceNameResolver.getName(clazz), comparator).get(0).getServiceId();
	}

	@Override
	public <T extends BaseService> String getServiceId(final Class<T> clazz, final String profileId) {
		final String profile = obtainServiceProfile(profileId);
		final ServiceRunningInstance instance = obtainRunningInstance(profile, obtainLocalServices());
		return instance.getServiceId();
	}

	@Override
	public <T extends BaseService> Set<T> getAllServices(final Class<T> clazz) {
		final Set<T> res = Sets.newHashSet();
		for (ServiceRunningInstance instance : findRunningInstances(serviceNameResolver.getName(clazz), null)) {
			res.add(instance.obtainClient(clazz, eprBuilder));
		}
		return res;
	}

	@Override
	public <T extends BaseService> Set<String> getAllServiceIds(final Class<T> clazz) {
		final Set<String> res = Sets.newHashSet();
		for (ServiceRunningInstance instance : findRunningInstances(serviceNameResolver.getName(clazz), null)) {
			res.add(instance.getServiceId());
		}
		return res;
	}

	private synchronized <T extends BaseService> ServiceRunningInstance obtainRunningInstance(final String profile, final Map<String, BaseService> locals) {
		try {
			final Document doc = reader.read(new StringReader(profile));
			final String url = doc.valueOf("//PROTOCOL[@name = 'SOAP']/@address");
			final String id = doc.valueOf("//RESOURCE_IDENTIFIER/@value");
			final Map<String, String> props = Maps.newHashMap();
			final BaseService local = locals.containsKey(id) ? locals.get(id) : null;
			final int usedDiskpace = NumberUtils.toInt(doc.valueOf("//USED_DISKSPACE"), 0);
			final int handledDatastructures = NumberUtils.toInt(doc.valueOf("//HANDLED_DATASTRUCTURE"), 0);;

			for (Object o : doc.selectNodes("//SERVICE_PROPERTIES/PROPERTY")) {
				final Element p = (Element) o;
				props.put(p.valueOf("@key"), p.valueOf("@value"));
			}

			return new ServiceRunningInstance(id, url, local, usedDiskpace, handledDatastructures, props);
		} catch (DocumentException e) {
			log.error("Error parsing profile: " + profile, e);
			throw new RuntimeException("Error parsing profile: " + profile, e);
		}
	}

	private List<ServiceRunningInstance> findRunningInstances(final String serviceName, final Comparator<ServiceRunningInstance> comparator) {
		final List<ServiceRunningInstance> list = findRegisteredServices(serviceName);

		if (list.isEmpty()) {
			log.error("Service not found, name: " + serviceName);
			throw new RuntimeException("Service not found, name: " + serviceName);
		}

		if (comparator != null) {
			Collections.sort(list, comparator);
		}

		return list;
	}

	private List<ServiceRunningInstance> findRegisteredServices(final String serviceName) {
		log.debug("searching for service: " + serviceName);

		final String xquery = "for $x in collection('/db/DRIVER/ServiceResources/" + serviceName + "ResourceType') return $x";
		log.debug(xquery);

		try {
			final List<String> services = isLookupService.quickSearchProfile(xquery);
			final List<ServiceRunningInstance> instances = Lists.newArrayList();
			final Map<String, BaseService> locals = obtainLocalServices();

			for (final String source : EnsureCollection.list(services)) {
				final ServiceRunningInstance instance = obtainRunningInstance(source, locals);
				instances.add(instance);
			}
			return instances;
		} catch (final Exception e) {
			throw new IllegalStateException("cannot locate service " + serviceName, e);
		}
	}

	private Map<String, BaseService> obtainLocalServices() {
		final Map<String, BaseService> locals = Maps.newHashMap();

		for (ValidatingServiceRegistrationManagerImpl r : appContext.getBeansOfType(ValidatingServiceRegistrationManagerImpl.class).values()) {
			if (r.getService() instanceof BaseService) {
				if (!StringUtils.isBlank(r.getProfileId())) {
					final BaseService baseService = (BaseService) r.getService();
					if (baseService != null) {
						locals.put(r.getProfileId(), baseService);
						log.debug(" -> Service: " + r.getService().getClass().getName() + " has id " + r.getServiceProfile().getResourceId());
					}
				}
			}
		}
		return locals;
	}

	private String obtainServiceProfile(final String profileId) {
		final StringWriter sw = new StringWriter();
		sw.append("let $uri:=/RESOURCE_PROFILE/HEADER[./RESOURCE_IDENTIFIER/@value='");
		sw.append(profileId);
		sw.append("']/RESOURCE_URI/@value/string()");
		sw.append("\n\n");
		sw.append("for $x in collection('/db/DRIVER/ServiceResources')");
		sw.append("\n");
		sw.append("where $x/RESOURCE_PROFILE/HEADER/RESOURCE_URI/@value = $uri");
		sw.append("\n");
		sw.append("return $x");

		final String xq = sw.toString();

		try {
			return isLookupService.getResourceProfileByQuery(xq);
		} catch (ISLookUpException e) {
			log.error("cannot locate service using query: " + xq, e);
			throw new IllegalStateException("cannot locate service using query: " + xq, e);
		}
	}

	@Override
	public void setApplicationContext(final ApplicationContext appContext) throws BeansException {
		this.appContext = appContext;
	}

	public Comparator<ServiceRunningInstance> getDefaultComparator() {
		return defaultComparator;
	}

	@Required
	public void setDefaultComparator(final Comparator<ServiceRunningInstance> defaultComparator) {
		this.defaultComparator = defaultComparator;
	}

	public ISLookUpService getIsLookupService() {
		return isLookupService;
	}

	public void setIsLookupService(final ISLookUpService isLookupService) {
		this.isLookupService = isLookupService;
	}

	public ServiceNameResolver getServiceNameResolver() {
		return serviceNameResolver;
	}

	public void setServiceNameResolver(final ServiceNameResolver serviceNameResolver) {
		this.serviceNameResolver = serviceNameResolver;
	}

}
