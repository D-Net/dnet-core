package eu.dnetlib.common.rmi;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(targetNamespace = "http://services.dnetlib.eu/")
public interface BaseService {

	/**
	 * All DRIVER services must implement method notify() in order to communicate with the IS_SN
	 * 
	 * @param subsrciptionId
	 * @param topic
	 * @param isId
	 * @param message
	 */
	@WebMethod(operationName = "notify")
	public void notify(@WebParam(name = "subscrId") String subscriptionId,
			@WebParam(name = "topic") String topic,
			@WebParam(name = "is_id") String isId,
			@WebParam(name = "message") String message);

	/**
	 * Identifies the service's version. Version syntax: ${NAME}-${MAJOR}.${MINOR}.${MICRO}[-${LABEL}]
	 * 
	 * @return the service's version
	 */
	@WebMethod(operationName = "identify")
	public String identify();

	public void start();

}
