package eu.dnetlib.springutils.beans.factory;

import org.springframework.beans.factory.FactoryBean;

/**
 * Use this class if you want to have a named bean defined as a null value. (Especially useful in some unit tests, when the bean to be
 * tested has \@Required properties)
 * 
 * @author marko
 * 
 */
public class NullBeanFactory implements FactoryBean<Void> {

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.beans.factory.FactoryBean#getObject()
	 */
	@Override
	public Void getObject() throws Exception {
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.beans.factory.FactoryBean#getObjectType()
	 */
	@Override
	public Class<?> getObjectType() {
		return null;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.beans.factory.FactoryBean#isSingleton()
	 */
	@Override
	public boolean isSingleton() {
		return true;
	}

}
