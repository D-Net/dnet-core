package eu.dnetlib.springutils.beans.factory;

import eu.dnetlib.miscutils.factory.Factory;

/**
 * Helper class to support simple Factory definitions with prototype scoped instances:
 *
 * <p>Example:</p>
 *
 * <pre>
 * 	&lt;bean id="resultSetDescriptorPrototype" class="eu.dnetlib.enabling.resultset.push.ResultSetDescriptor"
 *		p:rangeLength="200" scope="prototype" />
 *
 *	&lt;bean id="resultSetDescriptorFactory" class="eu.dnetlib.springutils.beans.factory.PrototypeFactory">
 *		&lt;lookup-method name="newInstance" bean="resultSetDescriptorPrototype" />
 *	&lt;/bean>
 * </pre>
 *
 * @author marko
 *
 * @param <T>
 */
public class PrototypeFactory<T> implements Factory<T> {

	/**
	 * {@inheritDoc}
	 *
	 * <p>
	 * meant to be replaced by spring method injection.
	 * </p>
	 * .
	 *
	 * @see eu.dnetlib.miscutils.factory.Factory#newInstance()
	 */
	@Override
	public T newInstance() {
		return null;
	}
}
