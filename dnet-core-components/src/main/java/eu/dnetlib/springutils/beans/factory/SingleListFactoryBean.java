package eu.dnetlib.springutils.beans.factory;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ListFactoryBean;

/**
 * this bean factory allows to quickly declare a list object containing only one element. This is useful
 * when we need to avoid a nested &lt;property>&lt;list>...&lt;/list>&lt/property>.
 * 
 * <p>
 * For example using the template custom beans defined in this library we currently cannot expand values deeply nested
 * inside &list> elements, so we need to resort to this trick in order to populate list properties with a single list element. 
 * </p>
 * 
 * @author marko
 *
 */
public class SingleListFactoryBean extends ListFactoryBean {
	
	/** 
	 * create a list with one value.
	 * 
	 * @param value value of the singleton list
	 */
	public void setValue(Object value) {
		List<Object> list = new ArrayList<Object>();
		list.add(value);
		setSourceList(list);
	}
}
