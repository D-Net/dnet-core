package eu.dnetlib.springutils.template;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.xml.BeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.w3c.dom.Element;

/**
 * This class performs no operation, it's just needed because spring requires a definition parser for every xml element
 * he finds.
 * 
 * @author marko
 * 
 */
public class TemplateValueDefinitionParser implements BeanDefinitionParser {

	@Override
	public BeanDefinition parse(Element arg0, ParserContext arg1) {
		return null;
	}

}
