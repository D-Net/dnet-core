package eu.dnetlib.springutils.template;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.xml.BeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.w3c.dom.Element;

/**
 * This bean definition parser allows you to define parameterized bean definition templates.
 * 
 * <pre>
 * 	&lt;template:define name=&quot;testTemplate&quot;&gt;
 *    &lt;bean name=&quot;dummyName&quot; t:name=&quot;$beanName$&quot;
 *       class=&quot;eu.dnetlib.springutils.template.TestBean&quot; p:address=&quot;$someValue$&quot;&gt;
 *    &lt;/bean&gt;
 *  &lt;/template:define&gt;
 * </pre>
 * 
 * you can put a template parameter in property values
 * 
 * @author marko
 * 
 */
public class TemplateDefineDefinitionParser implements BeanDefinitionParser {
	/**
	 * logger.
	 */
	private static final Log log = LogFactory.getLog(TemplateDefineDefinitionParser.class); // NOPMD by marko on 11/24/08 5:02 PM

	/**
	 * allows registering new templates.
	 */
	private BeanTemplateDao templateDao;

	/**
	 * construct a new template:define definition parser.
	 * 
	 * @param templateDao
	 *            dao object
	 */
	public TemplateDefineDefinitionParser(final BeanTemplateDao templateDao) {
		super();
		this.templateDao = templateDao;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.beans.factory.xml.BeanDefinitionParser#parse(org.w3c.dom.Element,
	 *      org.springframework.beans.factory.xml.ParserContext)
	 */
	@Override
	public BeanDefinition parse(final Element element, final ParserContext parserContext) {
		log.debug("parsing template DEFINE: " + element.getChildNodes());

		BeanTemplate template = new BeanTemplate();
		template.setName(element.getAttribute("name"));
		template.setRoot(element);

		templateDao.registerTemplate(template);

		return null;
	}

}
