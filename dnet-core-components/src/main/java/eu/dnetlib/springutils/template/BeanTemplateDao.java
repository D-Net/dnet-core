package eu.dnetlib.springutils.template;

import java.util.List;

public interface BeanTemplateDao {
	void registerTemplate(BeanTemplate template);

	BeanTemplate getTemplate(String name);
	
	void registerTemplateInstance(TemplateInstance instance);
	List<TemplateInstance> getInstancesForTemplate(BeanTemplate template);
}
