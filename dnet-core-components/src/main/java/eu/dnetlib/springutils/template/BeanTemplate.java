package eu.dnetlib.springutils.template;

import org.w3c.dom.Element;

/**
 * Stores a bean template definition.
 * 
 * @author marko
 * 
 */
public class BeanTemplate {
	/**
	 * template name.
	 */
	private String name;

	/**
	 * template definition element.
	 */
	private Element root;

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Element getRoot() {
		return root;
	}

	public void setRoot(final Element root) {
		this.root = root;
	}

}
