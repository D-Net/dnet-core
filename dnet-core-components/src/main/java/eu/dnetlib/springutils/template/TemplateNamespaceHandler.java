package eu.dnetlib.springutils.template;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

/**
 * declares all the definition parsers for elements declared in the template schema.
 * 
 * @author marko
 * 
 */
public class TemplateNamespaceHandler extends NamespaceHandlerSupport {
	/** 
	 * {@inheritDoc}
	 * @see org.springframework.beans.factory.xml.NamespaceHandler#init()
	 */
	@Override
	public void init() {
		BeanTemplateDao dao = new BeanTemplateDaoImpl();

		super.registerBeanDefinitionParser("define", new TemplateDefineDefinitionParser(dao));

		super.registerBeanDefinitionParser("instance", new TemplateInstanceDefinitionParser(dao));

		super.registerBeanDefinitionParser("value", new TemplateValueDefinitionParser());
	}
}
