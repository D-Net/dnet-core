package eu.dnetlib.springutils.aop;

import java.lang.reflect.Method;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Required;

/**
 * Ever tried to use an init-method on a bean which should be wrapped by a aop transaction? This helper class allow you
 * to invoke an init method <b>through</b> the aop proxy, thus starting the transactions or other stuff, something the
 * plain spring init method doesn't.
 * 
 * @author marko
 * 
 */
public class MethodInvokingInitializer implements InitializingBean {

	private static final Log log = LogFactory.getLog(MethodInvokingInitializer.class); // NOPMD by marko on 11/24/08 5:02 PM

	private Object target;
	private String methodName;

	public Object getTarget() {
		return target;
	}

	@Required
	public void setTarget(Object target) {
		this.target = target;
	}

	public String getMethodName() {
		return methodName;
	}

	@Required
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		log.info("starting");
		System.out.println("starting");

		Method method = target.getClass().getMethod(methodName, new Class[] {});
		method.invoke(target, new Object[] {});
	}

}
