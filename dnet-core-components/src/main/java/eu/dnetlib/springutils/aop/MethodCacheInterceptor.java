package eu.dnetlib.springutils.aop;

/*
 * Copyright 2002-2004 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.Serializable;

import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

/**
 * This AOP interceptor allows to implement method call memoization, e.g. cache each single invocation of a
 * method or group of methods (possibly all of them) of a given object. It associates the method call result to a hash
 * based on the parameter names, which should somehow represent their identity when converted toString().
 * 
 */
public class MethodCacheInterceptor implements MethodInterceptor, InitializingBean {
	/**
	 * logger.
	 */
	private static final Log log = LogFactory.getLog(MethodCacheInterceptor.class); // NOPMD by marko on 11/24/08 5:02 PM

	/**
	 * cache.
	 */
	private transient Cache cache;
	/**
	 * if true, the cache is updated every time and the method is executed even if the result is already cached.
	 */
	private boolean writeOnly;

	/**
	 * construct a read-write cache interceptor.
	 */
	public MethodCacheInterceptor() {
		this.writeOnly = false;
	}

	/**
	 * sets cache name to be used.
	 * 
	 * @param cache
	 *            cache instance
	 */
	public void setCache(final Cache cache) {
		this.cache = cache;
	}

	/**
	 * Checks if required attributes are provided.
	 */
	@Override
	public void afterPropertiesSet() {
		Assert.notNull(cache, "A cache is required. Use setCache(Cache) to provide one.");
	}

	/**
	 * main method caches method result if method is configured for caching. method results must be serializable
	 * 
	 * @param invocation
	 *            method invocation object
	 * @throws Throwable
	 *             transparent
	 * @return transparent
	 */
	@Override
	public Object invoke(final MethodInvocation invocation) throws Throwable {
		final String targetName = invocation.getThis().getClass().getName();
		final String methodName = invocation.getMethod().getName();
		final Object[] arguments = invocation.getArguments();
		Object result;

		log.debug("looking for method result in cache: " + isWriteOnly());
		final String cacheKey = getCacheKey(targetName, methodName, arguments);
		Element element = cache.get(cacheKey);
		if (element == null || isWriteOnly()) {
			// call target/sub-interceptor
			log.debug("calling intercepted method");
			result = invocation.proceed();

			// cache method result
			log.debug("caching result");
			element = new Element(cacheKey, (Serializable) result);
			cache.put(element);
		} else {
			log.debug("found in method cache");
		}
		return element.getObjectValue();
	}

	/**
	 * creates cache key: targetName.methodName.argument0.argument1...
	 * 
	 * @param targetName target name
	 * @param methodName method name
	 * @param arguments actual arguments
	 * @return cache key based on the parameters 
	 */
	private String getCacheKey(final String targetName, final String methodName, final Object[] arguments) {
		final StringBuffer sbuf = new StringBuffer();
		sbuf.append(targetName).append('.').append(methodName);
		if ((arguments != null) && (arguments.length != 0)) {
			for (int i = 0; i < arguments.length; i++) {
				sbuf.append('.').append(arguments[i]);
			}
		}

		return sbuf.toString();
	}

	public boolean isWriteOnly() {
		return writeOnly;
	}

	public void setWriteOnly(final boolean writeOnly) {
		this.writeOnly = writeOnly;
	}

}
