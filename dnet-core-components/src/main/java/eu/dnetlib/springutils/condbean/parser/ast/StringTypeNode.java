package eu.dnetlib.springutils.condbean.parser.ast;

public class StringTypeNode extends LiteralNode {

	public StringTypeNode(final String data) {
		super(data.substring(1, data.length() - 1));
	}
}
