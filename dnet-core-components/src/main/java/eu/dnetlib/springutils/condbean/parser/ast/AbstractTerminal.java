package eu.dnetlib.springutils.condbean.parser.ast;

public abstract class AbstractTerminal extends AbstractExpression {

	private String data;

	public AbstractTerminal(final String input) {
		super();
		this.data = input;
	}

	@Override
	public Object evaluate() {
		return data;
	}

	public String getData() {
		return data;
	}

	public void setData(final String data) {
		this.data = data;
	}
}
