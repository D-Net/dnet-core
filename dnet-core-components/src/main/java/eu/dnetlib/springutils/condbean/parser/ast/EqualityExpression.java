package eu.dnetlib.springutils.condbean.parser.ast;

public class EqualityExpression extends AbstractBinaryExpression {

	private String operator;

	private boolean eval() {
		if (getLeft().evaluate() == null && getRight().evaluate() == null)
			return true;
		else if (getLeft().evaluate() == null || getRight().evaluate() == null)
			return false;
		return getLeft().evaluate().equals(getRight().evaluate());
	}

	public EqualityExpression(final AbstractExpression left, final AbstractExpression right, final String oper) {
		super(left, right);
		operator = oper;
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object evaluate() {
		if ("==".equals(operator))
			return eval();
		return !eval();
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(final String operator) {
		this.operator = operator;
	}
}
