package eu.dnetlib.springutils.condbean;

import java.util.Collection;

/**
 * Chain property finder.
 * 
 * @author marko
 * 
 */
public class ChainPropertyFinder implements PropertyFinder {

	/**
	 * finders.
	 */
	Collection<PropertyFinder> finders;

	@Override
	public String getProperty(final String name) {
		for (final PropertyFinder finder : finders) {
			final String res = finder.getProperty(name);
			if (res != null)
				return res;
		}
		return null;
	}

	public Collection<PropertyFinder> getFinders() {
		return finders;
	}

	public void setFinders(Collection<PropertyFinder> finders) {
		this.finders = finders;
	}

}
