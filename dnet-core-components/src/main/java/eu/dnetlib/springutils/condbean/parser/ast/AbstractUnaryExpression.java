package eu.dnetlib.springutils.condbean.parser.ast;

public abstract class AbstractUnaryExpression extends AbstractExpression {

	private AbstractExpression expression;

	public AbstractUnaryExpression(final AbstractExpression expression) {
		super();
		this.expression = expression;
	}

	public AbstractExpression getExpression() {
		return expression;
	}

	public void setExpression(final AbstractExpression expression) {
		this.expression = expression;
	}

}
