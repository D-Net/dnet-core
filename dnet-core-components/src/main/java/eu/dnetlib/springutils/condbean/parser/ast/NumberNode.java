package eu.dnetlib.springutils.condbean.parser.ast;

public class NumberNode extends AbstractTerminal {

	private final transient double num;

	public NumberNode(final String data) {
		super(data);
		num = Double.parseDouble(data);
	}

	@Override
	public Object evaluate() {
		return num;
	}
}
