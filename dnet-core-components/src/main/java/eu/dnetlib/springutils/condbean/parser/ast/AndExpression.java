package eu.dnetlib.springutils.condbean.parser.ast;

public class AndExpression extends AbstractBinaryExpression {

	private boolean eval() {
		return Converter.toBoolean(getLeft().evaluate()) && Converter.toBoolean(getRight().evaluate());
	}

	public AndExpression(final AbstractExpression left, final AbstractExpression right) {
		super(left, right);
	}

	@Override
	public Object evaluate() {
		return eval();
	}

}
