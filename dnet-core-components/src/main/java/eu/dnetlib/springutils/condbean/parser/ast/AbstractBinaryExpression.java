package eu.dnetlib.springutils.condbean.parser.ast;

public abstract class AbstractBinaryExpression extends AbstractExpression {
	private AbstractExpression left;
	private AbstractExpression right;

	public AbstractBinaryExpression(final AbstractExpression left, final AbstractExpression right) {
		super();
		this.left = left;
		this.right = right;
	}

	public AbstractExpression getLeft() {
		return left;
	}

	public void setLeft(final AbstractExpression left) {
		this.left = left;
	}

	public AbstractExpression getRight() {
		return right;
	}

	public void setRight(final AbstractExpression right) {
		this.right = right;
	}

}
