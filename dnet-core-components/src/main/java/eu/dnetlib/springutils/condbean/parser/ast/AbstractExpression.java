package eu.dnetlib.springutils.condbean.parser.ast;

public abstract class AbstractExpression {

	public abstract Object evaluate();

}
