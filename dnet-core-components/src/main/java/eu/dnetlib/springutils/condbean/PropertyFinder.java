package eu.dnetlib.springutils.condbean;

public interface PropertyFinder {
	String getProperty(String name);
}
