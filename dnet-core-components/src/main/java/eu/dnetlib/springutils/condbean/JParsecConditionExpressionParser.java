package eu.dnetlib.springutils.condbean;

import org.codehaus.jparsec.Terminals;

/**
 * This class implements a simple ConditionExpressionParser using jparsec.
 * 
 * @author marko
 * 
 */

@SuppressWarnings("unused")
public class JParsecConditionExpressionParser implements ConditionExpressionParser {

	private static final String[] OPERATORS = { "&&", "||", "+", "-", "*", "/", ">", "<", ">=", "<=", // and all other operators.
	};
	private static final String[] KEYWORDS = { "true", "false" };
	private static final Terminals TERMINALS = Terminals.caseSensitive(OPERATORS, KEYWORDS);

	@Override
	public boolean expressionValue(final String expression) {
		return false;
	}

}
