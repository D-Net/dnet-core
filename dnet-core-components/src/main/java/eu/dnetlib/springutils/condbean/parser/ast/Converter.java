package eu.dnetlib.springutils.condbean.parser.ast;

public final class Converter { //NOPMD

	private Converter() {

	}

	public static boolean toBoolean(final Object obj) {
		if (obj instanceof String)
			return !((String) obj).isEmpty();
		else if (obj instanceof Boolean)
			return ((Boolean) obj).booleanValue();
		else if (obj instanceof Integer || obj instanceof Double) {
			return !((Double) (Double.parseDouble("0"))).equals(obj);
		} else
			return false;
	}

	@SuppressWarnings("unchecked")
	public static int compareTo(final Object obj1, final Object obj2) { //NOPMD

		/**
		 * This method has the following return values:
		 * 
		 * - result of compareTo if the two objects are Comparable and of the same runtime class
		 * 
		 */
		if (obj1 == null && obj2 == null)
			return 0;
		else if (obj1 == null)
			return -1;
		else if (obj2 == null)
			return 1;
		else if (obj1 == obj2)//NOPMD
			return 0;
		else if (obj1 instanceof Comparable && obj2 instanceof Comparable && obj1.getClass().equals(obj2.getClass()))
			return ((Comparable<Object>) obj1).compareTo(obj2);
		else
			return (obj1.hashCode() > obj2.hashCode()) ? 1 : (obj1.hashCode() == obj2.hashCode() ? 0 : -1);

	}

}
