package eu.dnetlib.springutils.condbean.parser.ast;

public class OrExpression extends AbstractBinaryExpression {

	private boolean eval() {
		return Converter.toBoolean(getLeft().evaluate()) || Converter.toBoolean(getRight().evaluate());
	}

	public OrExpression(final AbstractExpression left, final AbstractExpression right) {
		super(left, right);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object evaluate() {
		return eval();
	}

}
