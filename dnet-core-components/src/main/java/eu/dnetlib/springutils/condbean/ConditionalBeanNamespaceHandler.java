package eu.dnetlib.springutils.condbean;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

/**
 * register conditional bean parsers for the condbean namespace and schema. 
 * 
 * @author marko
 * 
 */
public class ConditionalBeanNamespaceHandler extends NamespaceHandlerSupport {
	/** 
	 * {@inheritDoc}
	 * @see org.springframework.beans.factory.xml.NamespaceHandler#init()
	 */
	@Override
	public void init() {
		super.registerBeanDefinitionDecoratorForAttribute("ifdefined", new ConditionalBeanAttributeDecorator());

		super.registerBeanDefinitionParser("cond", new ConditionalBeanDefinitionParser());
	}
}
