package eu.dnetlib.springutils.condbean;

/**
 * A ConditionExpressionParser parses simple expressions like
 * 
 * ${some.property} > 10 and !${some.other.property}.
 * 
 * @author marko
 * 
 */
public interface ConditionExpressionParser {

	/**
	 * return the boolean value of a given expression.
	 * 
	 * @param expression
	 *            expression source string
	 * @return expression truth value
	 */
	boolean expressionValue(String expression);

}
