package eu.dnetlib.springutils.condbean.parser.ast;

public class BooleanNode extends AbstractTerminal {

	private final transient boolean bool;

	public BooleanNode(final String input) {
		super(input);
		bool = Boolean.parseBoolean(input);
	}

	@Override
	public Object evaluate() {
		return bool;
	}

}
