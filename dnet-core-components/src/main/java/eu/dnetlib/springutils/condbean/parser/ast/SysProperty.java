package eu.dnetlib.springutils.condbean.parser.ast;

import eu.dnetlib.springutils.condbean.PropertyFinder;

public class SysProperty extends AbstractUnaryExpression {

	private PropertyFinder propertyFinder;

	public SysProperty(final PropertyFinder propertyFinder, final LiteralNode expression) {
		super(expression);
		this.propertyFinder = propertyFinder;
	}

	@Override
	public Object evaluate() {
		if (propertyFinder == null)
			return System.getProperty(((LiteralNode) getExpression()).getData());

		return propertyFinder.getProperty(((LiteralNode) getExpression()).getData());
	}

}
