package eu.dnetlib.springutils.condbean.parser.ast;

public class NotExpression extends AbstractUnaryExpression {

	public NotExpression(final AbstractExpression expression) {
		super(expression);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object evaluate() {
		return !Converter.toBoolean(getExpression().evaluate());
	}

}
