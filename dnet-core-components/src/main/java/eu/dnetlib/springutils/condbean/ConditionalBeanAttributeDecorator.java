package eu.dnetlib.springutils.condbean;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.xml.BeanDefinitionDecorator;
import org.springframework.beans.factory.xml.ParserContext;
import org.w3c.dom.Node;

/**
 * 
 * TODO: unfinished.
 * 
 * @author marko
 * 
 */
public class ConditionalBeanAttributeDecorator implements BeanDefinitionDecorator {

	private static final Log log = LogFactory.getLog(ConditionalBeanAttributeDecorator.class); // NOPMD by marko on 11/24/08 5:02 PM

	@Override
	public BeanDefinitionHolder decorate(final Node source, final BeanDefinitionHolder holder, final ParserContext ctx) {

		final String name = holder.getBeanName();
		log.fatal("NAME: " + name);

		return holder;
	}

}
