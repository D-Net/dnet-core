package eu.dnetlib.springutils.condbean.parser;

import eu.dnetlib.springutils.condbean.PropertyFinder;
import eu.dnetlib.springutils.condbean.parser.ast.AbstractExpression;
import eu.dnetlib.springutils.condbean.parser.ast.AndExpression;
import eu.dnetlib.springutils.condbean.parser.ast.BooleanNode;
import eu.dnetlib.springutils.condbean.parser.ast.EqualityExpression;
import eu.dnetlib.springutils.condbean.parser.ast.IdentifierNode;
import eu.dnetlib.springutils.condbean.parser.ast.IsdefExpression;
import eu.dnetlib.springutils.condbean.parser.ast.NotExpression;
import eu.dnetlib.springutils.condbean.parser.ast.NumberNode;
import eu.dnetlib.springutils.condbean.parser.ast.OrExpression;
import eu.dnetlib.springutils.condbean.parser.ast.QualNameNode;
import eu.dnetlib.springutils.condbean.parser.ast.RelationalExpression;
import eu.dnetlib.springutils.condbean.parser.ast.StringTypeNode;
import eu.dnetlib.springutils.condbean.parser.ast.SysProperty;
import fri.patterns.interpreter.parsergenerator.semantics.ReflectSemantic;

public class CondBeanParser extends ReflectSemantic { // NOPMD
	
	private PropertyFinder propertyFinder;
	
	public CondBeanParser() {
	}
	
	public Object logicalOrExpression(final Object orExp, final Object logicalor, final Object andExp) {
		return new OrExpression((AbstractExpression) orExp, (AbstractExpression) andExp);
	}

	public Object logicalAndExpression(final Object andExp, final Object logicaland, final Object equalExpr) {
		return new AndExpression((AbstractExpression) andExp, (AbstractExpression) equalExpr);
	}

	public Object equalityExpression(final Object equalExpr, final Object operator, final Object relExpr) {
		return new EqualityExpression((AbstractExpression) equalExpr, (AbstractExpression) relExpr, (String) operator);
	}

	public Object relationalExpression(final Object relExpr, final Object operator, final Object unaryExpression) {
		return new RelationalExpression((AbstractExpression) relExpr, (AbstractExpression) unaryExpression, (String) operator);
	}

	public Object unaryExpression(final Object lcb, final Object literal, final Object rcb) {
		return new SysProperty(propertyFinder, (QualNameNode) literal);
	}

	public Object unaryExpression(final Object exmark, final Object unaryExpression) {
		return new NotExpression((AbstractExpression) unaryExpression);
	}

	public Object simpleType(final Object literals) {
		return new IdentifierNode((String) literals);
	}

	public Object qualifiedType(final Object literals) {
		return new QualNameNode((String) literals);
	}

	public Object stringType(final Object astring) {
		return new StringTypeNode((String) astring);
	}

	public Object booleanType(final Object literals) {
		return new BooleanNode((String) literals);
	}

	public Object unaryExpression(final Object isdef, final Object leftpar, final Object identifier, final Object rightpar) {
		return new IsdefExpression((AbstractExpression) identifier);
	}

	public Object primaryExpression(final Object leftpar, final Object expression, final Object rightpar) {
		return expression;
	}

	public Object numberType(final Object number) {
		return new NumberNode((String) number);
	}

	public PropertyFinder getPropertyFinder() {
		return propertyFinder;
	}

	public void setPropertyFinder(PropertyFinder propertyFinder) {
		this.propertyFinder = propertyFinder;
	}

}
