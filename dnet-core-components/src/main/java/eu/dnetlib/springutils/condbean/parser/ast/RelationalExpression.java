package eu.dnetlib.springutils.condbean.parser.ast;

public class RelationalExpression extends AbstractBinaryExpression {

	private String operator;

	private boolean eval() {

		int compResult;

		compResult = Converter.compareTo(getLeft().evaluate(), getRight().evaluate());

		if (compResult > 0)
			return (">".equals(operator) || ">=".equals(operator));
		else if (compResult < 0)
			return ("<".equals(operator) || "<=".equals(operator));
		else
			return ("<=".equals(operator) || ">=".equals(operator));
	}

	public RelationalExpression(final AbstractExpression left, final AbstractExpression right, final String oper) {
		super(left, right);
		operator = oper;
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object evaluate() {
		return eval();
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(final String operator) {
		this.operator = operator;
	}

}
