package eu.dnetlib.springutils.condbean;

public class SystemPropertiesFinder implements PropertyFinder {

	@Override
	public String getProperty(String name) {
		return System.getProperty(name);
	}

}
