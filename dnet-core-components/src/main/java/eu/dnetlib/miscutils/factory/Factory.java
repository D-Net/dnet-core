package eu.dnetlib.miscutils.factory;

/**
 * Provides a common interface for factory objects. 
 * 
 * @author marko
 *
 * @param <T>
 */
public interface Factory<T> {
	/**
	 * returns a new instance.
	 * 
	 * @return
	 */
	T newInstance();
}
