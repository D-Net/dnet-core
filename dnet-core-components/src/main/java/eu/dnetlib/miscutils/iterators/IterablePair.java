package eu.dnetlib.miscutils.iterators;

import java.util.Iterator;
import java.util.List;

import eu.dnetlib.miscutils.collections.Pair;

public class IterablePair <A, B> implements Iterable<Pair<A, B>> {
    private final List<A> first;
    private final List<B> second;

    public IterablePair(List<A> first, List<B> second) { 
        this.first = first;
        this.second = second;
    }

    @Override
    public Iterator<Pair<A, B>> iterator() {
        return new ParallelIterator<A, B>(first.iterator(), second.iterator());
    }
}