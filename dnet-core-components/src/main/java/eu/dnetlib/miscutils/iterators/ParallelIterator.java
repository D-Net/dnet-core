package eu.dnetlib.miscutils.iterators;

import java.util.Iterator;

import eu.dnetlib.miscutils.collections.Pair;

public class ParallelIterator <A, B> implements Iterator<Pair<A, B>> {

    private final Iterator<A> iA;
    private final Iterator<B> iB;

    public ParallelIterator(Iterator<A> iA, Iterator<B> iB) { 
        this.iA = iA; this.iB = iB;
    }

    @Override
    public boolean hasNext() { return iA.hasNext() && iB.hasNext(); }

    @Override
    public Pair<A, B> next() {
        return new Pair<A, B>(iA.next(), iB.next());
    }

    @Override
    public void remove() {
        iA.remove();
        iB.remove();
    }
}
