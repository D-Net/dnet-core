package eu.dnetlib.miscutils.iterators;

import java.util.Iterator;

/**
 * Useful if you want to quickly iterate an iterable with the new java 5 syntax.
 *
 * @author marko
 *
 * @param <T>
 */
public class IterableIterator<T> implements Iterable<T> {
        private Iterator<T> iter;

        public IterableIterator(Iterator<T> iter) {
                this.iter = iter;
        }

        @Override
		public Iterator<T> iterator() {
                return iter;
        }
}

