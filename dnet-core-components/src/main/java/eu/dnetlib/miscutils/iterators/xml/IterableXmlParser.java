package eu.dnetlib.miscutils.iterators.xml;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Iterator;

public class IterableXmlParser implements Iterable<String> {

	private String element;

	private InputStream inputStream;

	public IterableXmlParser(String element, InputStream inputStream) {
		this.element = element;
		this.inputStream = inputStream;
	}

	public IterableXmlParser(String element, String xml) {
		this.element = element;
		this.inputStream = new ByteArrayInputStream(xml.getBytes(Charset.forName(XMLIterator.UTF_8)));
	}

	@Override
	public Iterator<String> iterator() {
		return new XMLIterator(element, inputStream);
	}

}
