package eu.dnetlib.miscutils.hstree;

/**
 * This is the terminator node in a tree definition chain:
 * 
 * TreeNode<L0, L1, TreeNode<L1, L2, TreeNode<L2, Void, NilTreeNode>>>
 * 
 * Note the use of "Void" as the next-resource-parameter value in the last level.
 * 
 * 
 * @author marko
 * 
 */
abstract public class NilTreeNode<V> extends TreeNode<Void, Void, V, TreeNode<Void, ?, V, ?>> { // NOPMD

}
