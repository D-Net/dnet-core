package eu.dnetlib.miscutils.collections;

import eu.dnetlib.miscutils.functional.UnaryFunction;

public interface Filter<T> extends UnaryFunction<Boolean, T> {
}
