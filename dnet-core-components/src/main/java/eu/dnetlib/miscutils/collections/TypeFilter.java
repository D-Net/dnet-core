package eu.dnetlib.miscutils.collections;

public class TypeFilter<T> implements Filter<T> {
	transient final Class<? extends T> cls;

	public TypeFilter(final Class<? extends T> cls) {
		this.cls = cls;
	}

	@Override
	public Boolean evaluate(final T element) {
		return (cls.isInstance(element));
	}
}