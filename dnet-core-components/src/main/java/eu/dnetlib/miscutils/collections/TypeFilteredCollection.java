package eu.dnetlib.miscutils.collections;

import java.util.Collection;
import java.util.Iterator;

public class TypeFilteredCollection<T, S extends T> implements Iterable<S> {
	private transient final Collection<T> coll;
	private transient final Class<? extends S> cls;

	public TypeFilteredCollection(final Collection<T> coll, final Class<? extends S> cls) {
		this.coll = coll;
		this.cls = cls;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Iterator<S> iterator() {
		final Filter<T> filter = new TypeFilter<T>(cls);
		return (Iterator<S>) (new FilteredCollection<T>(coll, filter)).iterator();
	}
}