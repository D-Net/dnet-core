package eu.dnetlib.miscutils.collections;

import java.util.Collection;

import eu.dnetlib.miscutils.functional.UnaryFunction;

/**
 * This is a utility class which helps you append suffixes or prefixes to a collection of strings.
 * 
 * @author marko
 *
 */
public class AffixCollection extends MappedCollection<String, String> {

	/**
	 * Position of the affix.
	 * 
	 * @author marko
	 *
	 */
	enum Position {
		/**
		 * prefix. 
		 */
		PREFIX,
		/**
		 * suffix.
		 */
		SUFFIX
	}
	
	/**
	 * append a given suffix to every element of the input collection.
	 * 
	 * @param coll
	 * @param suffix
	 */
	public AffixCollection(final Collection<String> coll, final String suffix) {
		this(coll, suffix, Position.SUFFIX);
	}
	
	/**
	 * add a given affix to every element of the input collection, either as prefix or suffix.
	 * 
	 * @param coll
	 * @param affix
	 * @param position
	 */
	public AffixCollection(final Collection<String> coll, final String affix, final Position position) {
		super(coll, new UnaryFunction<String, String>() {
			@Override
			public String evaluate(String arg) {
				return arg + affix;
			}
		});
	}

}
