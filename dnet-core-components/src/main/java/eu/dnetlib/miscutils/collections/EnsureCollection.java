package eu.dnetlib.miscutils.collections;

import java.util.List;

import com.google.common.collect.Lists;

/**
 * Utility class which checks ensures that a given collection is not null.
 * 
 * <p>
 * CXF for example cannot distinguish from nulls. This helper allows you write:
 * </p>
 * 
 * <pre>
 * for(String bla : EnsureCollection.list(somecode())) {....}
 * </pre>
 * 
 * @author marko
 * 
 */
public class EnsureCollection {
	
	/**
	 * returns the argument or a new list if the argument is null.
	 * 
	 * @param <T> a type
	 * @param aList a list
	 * @return passthrough or empty list
	 */
	public static <T> List<T> list(final List<T> aList) {
		if (aList == null)
			return Lists.newArrayList();
		return aList;
	}

	
	/**
	 * Alias for static imports.
	 * 
	 * @param <T> a type
	 * @param aList a list
	 * @return passthrough or empty list
	 */
	public static <T> List<T> notNullList(final List<T> aList) {
		return list(aList);
	}

}
