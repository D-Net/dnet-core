package eu.dnetlib.miscutils.cache;

/**
 * A cache element encapsulates a value and a number of cache specific parameters like the expiration time.
 * 
 * @author marko
 * 
 * @param <T>
 *            type of payload
 */
@Deprecated
public class CacheElement<T> {
	private T value;
	private int timeToLive;
	private int timeToIdle;
	private long creationTime;

	/**
	 * reference to the underlying implementation specific element instance, for all other properties not exposed here.
	 */
	Object specificElement;

	CacheElement(final T value) {
		this.value = value;
	}

	CacheElement(final T value, final int timeToLive, final int timeToIdle) {
		this.value = value;
		this.timeToLive = timeToLive;
		this.timeToIdle = timeToIdle;
	}

	public T getValue() {
		return value;
	}

	public void setValue(final T value) {
		this.value = value;
	}

	public Integer getTimeToLive() {
		return timeToLive;
	}

	public void setTimeToLive(final Integer timeToLive) {
		this.timeToLive = timeToLive;
	}

	public Integer getTimeToIdle() {
		return timeToIdle;
	}

	public void setTimeToIdle(final Integer timeToIdle) {
		this.timeToIdle = timeToIdle;
	}

	public Object getSpecificElement() {
		return specificElement;
	}

	public void setSpecificElement(final Object specificElement) {
		this.specificElement = specificElement;
	}

	public long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(long creationTime) {
		this.creationTime = creationTime;
	}

}
