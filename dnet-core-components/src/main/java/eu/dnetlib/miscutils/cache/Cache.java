package eu.dnetlib.miscutils.cache;

import java.util.Map;
import java.util.Set;

/**
 * Generic strongly typed cache.
 * 
 * @author marko
 * 
 * @param <K>
 *            key type
 * @param <V>
 *            value type
 */
@Deprecated
public interface Cache<K, V> extends Map<K, V> {
	V put(K key, CacheElement<V> element);

	CacheElement<V> getElement(K key);

	/**
	 * Short verision of put(K key, CacheElement<V> element) using default values for expiry time and other caching
	 * options.
	 * 
	 * @param key
	 *            key
	 * @param value
	 *            value
	 */
	@Override
	V put(K key, V value);

	@Override
	V get(Object key);
	
	@Override
	Set<K> keySet();
}
