package eu.dnetlib.miscutils.maps;

import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 *  
 * @author claudio
 *
 * @param <K>
 * @param <V>
 */
public class ConcurrentSizedMap<K, V> extends ConcurrentHashMap<K, V> {

	private static final long serialVersionUID = 5864196164431521782L;

	public int DEFAULT_SIZE = 4;
	
	private int queueSize;
	
	private Queue<K> queue;

	public ConcurrentSizedMap() {
		super();
		queue = new ConcurrentLinkedQueue<K>();
		this.queueSize = DEFAULT_SIZE;
	}

	@Override
	public V put(K key, V value) {
		queue.add(key);
		if (queue.size() > queueSize)
			super.remove(queue.poll());
		
		return super.put(key, value);
	}

	@Override
	public V remove(Object key) {
		queue.remove(key);
		return super.remove(key);
	}

	public void setQueueSize(int queueSize) {
		this.queueSize = queueSize;
	}

	public int getQueueSize() {
		return queueSize;
	}
	
}
