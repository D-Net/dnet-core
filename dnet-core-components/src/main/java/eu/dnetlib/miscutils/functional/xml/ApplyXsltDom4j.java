package eu.dnetlib.miscutils.functional.xml;

import java.util.Map;

import javax.xml.transform.Source;

import org.dom4j.Document;
import org.dom4j.io.DocumentSource;
import org.springframework.core.io.Resource;

/**
 * Applies a stylesheet to a XML DOM4j and returns an XML string.
 * 
 * @author marko
 * 
 */
public class ApplyXsltDom4j extends AbstractApplyXslt<Document> {

	public ApplyXsltDom4j(Resource xslt) {
		super(xslt);
	}

	public ApplyXsltDom4j(Source xslt, String name) {
		super(xslt, name);
	}

	public ApplyXsltDom4j(Source xslt) {
		super(xslt);
	}

	public ApplyXsltDom4j(String xslt, String name) {
		super(xslt, name);
	}

	public ApplyXsltDom4j(String xslt) {
		super(xslt);
	}

	public ApplyXsltDom4j(Resource xslt, Map<String, String> parameters) {
		super(xslt, parameters);
	}

	public ApplyXsltDom4j(Source xslt, String name, Map<String, String> parameters) {
		super(xslt, name, parameters);
	}

	public ApplyXsltDom4j(String xslt, String name, Map<String, String> parameters) {
		super(xslt, name, parameters);
	}

	@Override
	public Source toStream(Document input) {
		return new DocumentSource(input);
	}

	@Override
	public String toString(Document input) {
		return input.asXML();
	}

}
