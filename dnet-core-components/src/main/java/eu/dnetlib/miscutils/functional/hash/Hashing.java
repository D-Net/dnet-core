package eu.dnetlib.miscutils.functional.hash;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;

public class Hashing {

	public static String md5(String s) {
		return DigestUtils.md5Hex(s);
	}

	public static String decodeBase64(String s) {
		return new String(Base64.decodeBase64(s.getBytes()));
	}

	public static String encodeBase64(String s) {
		return new String(Base64.encodeBase64(s.getBytes()));
	}

}
