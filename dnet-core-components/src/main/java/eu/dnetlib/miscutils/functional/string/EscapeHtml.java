package eu.dnetlib.miscutils.functional.string;


import eu.dnetlib.miscutils.functional.UnaryFunction;
import org.apache.commons.lang3.StringEscapeUtils;

public class EscapeHtml implements UnaryFunction<String, String> {
	@Override
	public String evaluate(String arg) {
		return StringEscapeUtils.escapeHtml4(arg);
	}
}
