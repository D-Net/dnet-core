package eu.dnetlib.miscutils.functional.string;
import eu.dnetlib.miscutils.functional.UnaryFunction;
import org.apache.commons.lang3.StringEscapeUtils;

public class EscapeXml implements UnaryFunction<String, String> {
	@Override
	public String evaluate(String arg) {
		return StringEscapeUtils.escapeXml11(arg);
	}
}
