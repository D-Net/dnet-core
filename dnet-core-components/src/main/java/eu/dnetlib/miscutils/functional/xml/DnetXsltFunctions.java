package eu.dnetlib.miscutils.functional.xml;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Joiner;
import eu.dnetlib.miscutils.functional.hash.Hashing;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This class provides some XSLT functions.
 * 
 * <xsl:stylesheet ... xmlns:dnet="eu.dnetlib.miscutils.functional.xml.DnetXsltFunctions"> ... </xsl:stylesheet>
 * 
 * @author michele
 * 
 */
public class DnetXsltFunctions {

	private static final Log log = LogFactory.getLog(DnetXsltFunctions.class); // NOPMD by marko on 11/24/08 5:02 PM

	private static volatile long seedUniquifier = 8682522807148012L;

	private static String[] dateFormats = { "yyyy-MM-dd", "yyyy/MM/dd" };

	private static final String[] normalizeDateFormats = { "yyyy-MM-dd'T'hh:mm:ss", "yyyy-MM-dd", "yyyy/MM/dd", "yyyy" };

	private static final String normalizeOutFormat = new String("yyyy-MM-dd'T'hh:mm:ss'Z'");

	public static String extractYear(String s) throws ParseException {
		Calendar c = new GregorianCalendar();
		for (String format : dateFormats) {
			try {
				c.setTime(new SimpleDateFormat(format).parse(s));
				String year = String.valueOf(c.get(Calendar.YEAR));
				return year;
			} catch (ParseException e) {}
		}
		return "";
	}

	public static String normalizeDate(final String s, final boolean strict) {

		final String date = s != null ? s.trim() : "";

		for (String format : normalizeDateFormats) {
			try {
				Date parse = new SimpleDateFormat(format).parse(date);
				String res = new SimpleDateFormat(normalizeOutFormat).format(parse);
				return res;
			} catch (ParseException e) {}
		}
		if (strict) {
			throw new IllegalArgumentException("unable to normalize date: " + date);
		} else {
			//log.warn("unable to normalize date: " + s);
			return "";
		}
	}

	public static String randomInt(int max) {
		return String.valueOf(new Random(++seedUniquifier + System.nanoTime()).nextInt(max));
	}

	public static String md5(String s) {
		return Hashing.md5(s);
	}

	public static String decodeBase64(String s) {
		return Hashing.decodeBase64(s);
	}

	public static String encodeBase64(String s) {
		return Hashing.encodeBase64(s);
	}

	public static String lc(String s) {
		return s.toLowerCase();
	}

	public static String uc(String s) {
		return s.toUpperCase();
	}

	public static String decade(String s) {
		String res = _decade(s.trim());
		log.debug(s + "--> " + res);
		return res;
	}

	private static String _decade(String s) {
		Matcher m1 = Pattern.compile("(\\d\\d\\d)\\d").matcher(s);
		if (m1.find()) {
			String part = m1.group(1);
			return part + "0-" + part + "9";
		}
		Matcher m2 = Pattern.compile("(\\d)\\d").matcher(s);
		if (m2.find()) {
			String part = m2.group(1);
			return "19" + part + "0-19" + part + "9";
		}
		return "n/a";
	}

	public static String join(final String s1, final String separator, final String s2) {
		if(StringUtils.isBlank(s1) || StringUtils.isBlank(s2)){
			return "";
		}
		return Joiner.on(separator).join(s1, s2);
	}

	public static String pickFirst(final String s1, final String s2) {
		return StringUtils.isNotBlank(s1) ? s1 : StringUtils.isNotBlank(s2) ? s2 : "";
	}
}
