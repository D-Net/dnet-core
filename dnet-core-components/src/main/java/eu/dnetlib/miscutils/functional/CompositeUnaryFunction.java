package eu.dnetlib.miscutils.functional;

public class CompositeUnaryFunction<T, K> implements UnaryFunction<T, K> {

	protected transient UnaryFunction<T, K> function;

	public CompositeUnaryFunction(final UnaryFunction<T, K> function) {
		this.function = function;
	}

	class UnaryHelper<X> extends CompositeUnaryFunction<T, X> {

		private transient final UnaryFunction<K, X> comp;
		private transient final UnaryFunction<T, K> base;

		public UnaryHelper(final UnaryFunction<T, K> base, final UnaryFunction<K, X> comp) {
			super(null);

			this.comp = comp;
			this.base = base;
		}

		@Override
		public T evaluate(final X arg) {
			return base.evaluate(comp.evaluate(arg));
		}
	}

	public <X> CompositeUnaryFunction<T, X> of(UnaryFunction<K, X> comp) { // NOPMD
		return new UnaryHelper<X>(this, comp);
	}

	@Override
	public T evaluate(final K arg) {
		return function.evaluate(arg);
	}

}
