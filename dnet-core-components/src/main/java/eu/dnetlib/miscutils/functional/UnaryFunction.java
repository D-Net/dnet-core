/**
 * 
 */
package eu.dnetlib.miscutils.functional;

/**
 * @author marko
 * 
 */
public interface UnaryFunction<T, K> {
	T evaluate(K arg);
}
