package eu.dnetlib.miscutils.functional.string;

import eu.dnetlib.miscutils.functional.UnaryFunction;

public class FastEscapeXml implements UnaryFunction<String, String> {

	@Override
	public String evaluate(String arg) {
		return "<![CDATA[" + arg.replace("]]>", "]]]]><![CDATA[>") + "]]>";
	}

}
