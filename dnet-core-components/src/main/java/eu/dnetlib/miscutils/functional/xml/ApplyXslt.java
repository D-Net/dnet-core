package eu.dnetlib.miscutils.functional.xml;

import java.io.StringReader;
import java.util.Map;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.springframework.core.io.Resource;

/**
 * Applies a stylesheet to a XML string and returns an XML string.
 * 
 * @author marko
 *
 */
public class ApplyXslt extends AbstractApplyXslt<String> {

	public ApplyXslt(Resource xslt) {
		super(xslt);
	}

	public ApplyXslt(Source xslt, String name) {
		super(xslt, name);
	}

	public ApplyXslt(Source xslt) {
		super(xslt);
	}

	public ApplyXslt(String xslt, String name) {
		super(xslt, name);
	}

	public ApplyXslt(String xslt) {
		super(xslt);
	}
	
	public ApplyXslt(Resource xslt, Map<String, String> parameters) {
		super(xslt, parameters);
	}

	public ApplyXslt(Source xslt, String name, Map<String, String> parameters) {
		super(xslt, name, parameters);
	}

	public ApplyXslt(String xslt, String name, Map<String, String> parameters) {
		super(xslt, name, parameters);
	}

	@Override
	public Source toStream(String input) {
		return new StreamSource(new StringReader(input));
	}
	
	@Override
	public String toString(String input) {
		return input;
	}
}
