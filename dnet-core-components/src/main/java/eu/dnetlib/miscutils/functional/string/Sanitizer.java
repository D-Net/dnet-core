package eu.dnetlib.miscutils.functional.string;

import eu.dnetlib.miscutils.functional.UnaryFunction;

/**
 * Removes control characters from a string
 * 
 * @author claudio
 *
 */
public class Sanitizer implements UnaryFunction<String, String> {

	@Override
	public String evaluate(String s) {
		return sanitize(s);
	}
	
	
	public static String sanitize(String s) {
		return s.replaceAll("\\p{Cntrl}", "");
	}
	
}
