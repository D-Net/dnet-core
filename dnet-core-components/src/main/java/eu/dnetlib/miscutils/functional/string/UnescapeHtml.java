package eu.dnetlib.miscutils.functional.string;

import eu.dnetlib.miscutils.functional.UnaryFunction;
import org.apache.commons.lang3.StringEscapeUtils;

public class UnescapeHtml implements UnaryFunction<String, String> {

	@Override
	public String evaluate(final String arg) {
		return StringEscapeUtils.unescapeHtml4(arg);
	}
}
