package eu.dnetlib.miscutils.functional;

/**
 * Identity function
 * 
 * @author marko
 * 
 * @param <T>
 */
public class IdentityFunction<T> implements UnaryFunction<T, T> {

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.miscutils.functional.UnaryFunction#evaluate(java.lang.Object)
	 */
	@Override
	public T evaluate(final T arg) {
		return arg;
	}

}
