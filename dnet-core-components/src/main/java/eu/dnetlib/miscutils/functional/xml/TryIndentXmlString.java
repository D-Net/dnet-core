package eu.dnetlib.miscutils.functional.xml;

/**
 * The Class TryIndentXmlString. Tries to indent an xml string. If an error occurs (e.g. broken xml) it returns the original xml.
 */
public class TryIndentXmlString extends IndentXmlString {

	/*
	 * (non-Javadoc)
	 * 
	 * @see eu.dnetlib.miscutils.functional.xml.IndentXmlString#evaluate(java.lang.String)
	 */
	@Override
	public String evaluate(final String unformattedXml) {
		try {
			return doIndent(unformattedXml);
		} catch (Exception e) {
			return unformattedXml;
		}
	}

}
