package eu.dnetlib.miscutils.coupling;

/**
 * Simple implementation of an ExternalCondition object, where a condition provider has directly control to the global condition.
 * 
 * @author marko
 *
 */
public class StaticCondition implements ExternalCondition {

	/**
	 * condition provider can set this field.
	 */
	boolean condition;
	
	/** 
	 * {@inheritDoc}
	 * @see eu.dnetlib.miscutils.coupling.ExternalCondition#isTrue()
	 */
	@Override
	public synchronized boolean isTrue() {
		return isCondition();
	}

	public synchronized boolean isCondition() {
		return condition;
	}

	public synchronized void setCondition(boolean condition) {
		this.condition = condition;
	}

}
