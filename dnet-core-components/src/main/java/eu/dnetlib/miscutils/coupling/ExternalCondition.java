package eu.dnetlib.miscutils.coupling;

/**
 * In many occasions one component wants to disable some other component or some functionalty of another component
 * but it's not advisable to couple these components too tightly. This class offers an abstraction to the concept of "Condition",
 * where a component can parameterize it's behavior according to some external condition, which can be driven by another component.  
 * 
 * @author marko
 *
 */
public interface ExternalCondition {
	/**
	 * true if true.
	 * 
	 * @return true if true
	 */
	boolean isTrue();
}
