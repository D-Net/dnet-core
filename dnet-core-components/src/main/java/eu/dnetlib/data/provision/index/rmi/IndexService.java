package eu.dnetlib.data.provision.index.rmi;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import eu.dnetlib.common.rmi.BaseService;

/**
 * Interface for the IndexService.
 *
 * @author alessia
 *
 */
@WebService(targetNamespace = "http://services.dnetlib.eu/")
public interface IndexService extends BaseService {

	/**
	 * Returns list of all stored indices.
	 *
	 * @return list of all stored indices
	 * @throws IndexServiceException
	 */
	@WebMethod(operationName = "getListOfIndices", action = "getListOfIndices")
	public List<String> getListOfIndices() throws IndexServiceException;
}
