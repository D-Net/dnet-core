package eu.dnetlib.data.provision.index.rmi;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <pre>
 * {@code
 * <groupresult field="facetFieldName">
 * 	<value>facetFieldValue</value>
 * 	<count>1</count>
 * </groupresult>
 * }
 * </pre>
 * 
 * @author claudio
 * 
 */
@XmlRootElement(namespace = "", name = "groupresult")
public class GroupResult {

	private String name;

	private String value;

	private int count;

	public GroupResult() {}

	/**
	 * Builds a groupResult.
	 * 
	 * @param fieldName
	 * @param fieldValue
	 * @param count
	 */
	public GroupResult(final String name, final String value, final int count) {
		this.name = name;
		this.value = value;
		this.count = count;
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof GroupResult)) return false;
		final GroupResult g = (GroupResult) obj;
		if ((this.getCount() == g.getCount()) && this.getName().equals(g.getName()) && this.getValue().equals(g.getValue())) return true;
		return false;
	}

	@XmlAttribute(name = "field", required = true)
	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

	public int getCount() {
		return count;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	public void setCount(final int count) {
		this.count = count;
	}

}
