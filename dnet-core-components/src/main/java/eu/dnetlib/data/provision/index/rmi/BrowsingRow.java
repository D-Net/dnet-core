package eu.dnetlib.data.provision.index.rmi;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * serialization of the browsing result.
 * 
 * <row> <groupresult field="facetFieldName1"> <value>facetFieldValue</value> <count>1</count> </groupresult>
 * 
 * <groupresult field="facetFieldName2"> <value>facetFieldValue</value> <count>1</count> </groupresult>
 * 
 * </row>
 * 
 * @author claudio
 * 
 */
@XmlRootElement(namespace = "", name = "row")
@XmlAccessorType(XmlAccessType.FIELD)
public class BrowsingRow {

	@XmlElement(name = "groupresult", required = true)
	private List<GroupResult> groupresult;

	public BrowsingRow() {}

	public BrowsingRow(final List<GroupResult> groupresult) {
		this.groupresult = groupresult;
	}

	/**
	 * adds a GroupResult.
	 * 
	 * @param fieldName
	 * @param fieldValue
	 * @param count
	 */
	public void addBrowsingRow(final String fieldName, final String fieldValue, final int count) {
		groupresult.add(new GroupResult(fieldName, fieldValue, count));
	}

	@Override
	public boolean equals(final Object obj) {

		if (!(obj instanceof BrowsingRow)) return false;

		final BrowsingRow brws = (BrowsingRow) obj;

		return groupresult.equals(brws.getGroupResult());
	}

	public List<GroupResult> getGroupResult() {
		return groupresult;
	}

	public void setGroupResult(final List<GroupResult> groupresult) {
		this.groupresult = groupresult;
	}

}
