package eu.dnetlib.data.provision.index.rmi.protocols;

public enum IndexProtocols {
	SOLR, SOLR_CLOUD
}
