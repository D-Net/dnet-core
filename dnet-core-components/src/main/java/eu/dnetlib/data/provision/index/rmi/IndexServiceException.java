package eu.dnetlib.data.provision.index.rmi;

public class IndexServiceException extends Exception {

	private static final long serialVersionUID = 8330264706967294512L;

	public IndexServiceException() {
		super();
	}

	public IndexServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public IndexServiceException(String message) {
		super(message);
	}

	public IndexServiceException(Throwable cause) {
		super(cause);
	}

}
