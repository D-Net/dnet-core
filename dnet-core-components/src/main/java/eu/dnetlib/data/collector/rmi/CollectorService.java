package eu.dnetlib.data.collector.rmi;

import java.util.List;
import java.util.Map;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.wsaddressing.W3CEndpointReference;

import eu.dnetlib.common.rmi.BaseService;

@WebService(targetNamespace = "http://services.dnetlib.eu/")
public interface CollectorService extends BaseService {

	W3CEndpointReference collect(@WebParam(name = "interface") final InterfaceDescriptor interfaceDescriptor) throws CollectorServiceException;

	W3CEndpointReference dateRangeCollect(
			@WebParam(name = "interface") final InterfaceDescriptor interfaceDescriptor,
			@WebParam(name = "from") final String from,
			@WebParam(name = "until") final String until) throws CollectorServiceException;

	List<ProtocolDescriptor> listProtocols();

	List<ProtocolParameterValue> listValidValuesForParam(
			@WebParam(name = "protocol") String protocol,
			@WebParam(name = "baseUrl") String baseUrl,
			@WebParam(name = "param") String param,
			@WebParam(name = "otherParams") Map<String, String> otherParams) throws CollectorServiceException;

}
