package eu.dnetlib.data.collector.plugin;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

import eu.dnetlib.data.collector.plugin.CollectorPlugin;
import eu.dnetlib.data.collector.rmi.ProtocolDescriptor;
import eu.dnetlib.data.collector.rmi.ProtocolParameter;

public abstract class AbstractCollectorPlugin implements CollectorPlugin {

	private ProtocolDescriptor protocolDescriptor;

	@Override
	public final String getProtocol() {
		return getProtocolDescriptor().getName();
	}

	@Override
	public final List<String> listNameParameters() {
		return Lists.newArrayList(Lists.transform(getProtocolDescriptor().getParams(), new Function<ProtocolParameter, String>() {

			@Override
			public String apply(final ProtocolParameter p) {
				return p.getName();
			}
		}));
	}

	@Override
	public final ProtocolDescriptor getProtocolDescriptor() {
		return protocolDescriptor;
	}

	@Required
	public void setProtocolDescriptor(final ProtocolDescriptor protocolDescriptor) {
		this.protocolDescriptor = protocolDescriptor;
	}
}
