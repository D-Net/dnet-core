package eu.dnetlib.data.collector.plugin;

import java.util.List;

import eu.dnetlib.data.collector.rmi.CollectorServiceException;
import eu.dnetlib.data.collector.rmi.InterfaceDescriptor;
import eu.dnetlib.data.collector.rmi.ProtocolDescriptor;

public interface CollectorPlugin {

	Iterable<String> collect(InterfaceDescriptor interfaceDescriptor, String fromDate, String untilDate) throws CollectorServiceException;

	ProtocolDescriptor getProtocolDescriptor();

	String getProtocol();

	List<String> listNameParameters();
}
