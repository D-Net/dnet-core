package eu.dnetlib.data.collector.rmi;

public class CollectorServiceRuntimeException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 6317717870955037359L;

	public CollectorServiceRuntimeException(final String string) {
		super(string);
	}

	public CollectorServiceRuntimeException(final String string, final Throwable exception) {
		super(string, exception);
	}

	public CollectorServiceRuntimeException(final Throwable exception) {
		super(exception);
	}

}
