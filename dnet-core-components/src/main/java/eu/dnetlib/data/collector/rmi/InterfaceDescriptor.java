package eu.dnetlib.data.collector.rmi;

import java.util.HashMap;

import javax.xml.bind.annotation.XmlRootElement;

import org.dom4j.Node;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.collect.Maps;

@XmlRootElement
public class InterfaceDescriptor {

	private String id;

	private String baseUrl;

	private String protocol;

	private HashMap<String, String> params = Maps.newHashMap();

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(final String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getId() {
		return id;
	}

	@Required
	public void setId(final String id) {
		this.id = id;
	}

	public HashMap<String, String> getParams() {
		return params;
	}

	public void setParams(final HashMap<String, String> params) {
		this.params = params;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(final String protocol) {
		this.protocol = protocol;
	}

	public static InterfaceDescriptor newInstance(final Node node) {
		final InterfaceDescriptor ifc = new InterfaceDescriptor();
		ifc.setId(node.valueOf("./@id"));
		ifc.setBaseUrl(node.valueOf("./BASE_URL"));
		ifc.setProtocol(node.valueOf("./ACCESS_PROTOCOL"));

		for (Object o : node.selectNodes("./ACCESS_PROTOCOL/@*")) {
			final Node n = (Node) o;
			ifc.getParams().put(n.getName(), n.getText());
		}

		return ifc;
	}

}
