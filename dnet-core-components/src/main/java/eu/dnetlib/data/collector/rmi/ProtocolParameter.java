package eu.dnetlib.data.collector.rmi;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.springframework.beans.factory.annotation.Required;

import eu.dnetlib.data.collector.functions.ParamValuesFunction;

@XmlRootElement
public class ProtocolParameter {

	private String name;
	private boolean optional = false;
	private ProtocolParameterType type = ProtocolParameterType.TEXT;
	private String regex = null;
	private transient ParamValuesFunction populateFunction = null;
	private boolean functionPopulated = false;

	public ProtocolParameter() {}

	public ProtocolParameter(final String name, final boolean optional, final ProtocolParameterType type, final String regex) {
		this(name, optional, type, regex, null);
	}

	public ProtocolParameter(final String name, final boolean optional, final ProtocolParameterType type, final String regex,
			final ParamValuesFunction populateFunction) {
		this.name = name;
		this.optional = optional;
		this.type = type;
		this.regex = regex;
		this.populateFunction = populateFunction;
		this.functionPopulated = this.populateFunction != null;
	}

	public String getName() {
		return name;
	}

	@Required
	public void setName(final String name) {
		this.name = name;
	}

	public boolean isOptional() {
		return optional;
	}

	public void setOptional(final boolean optional) {
		this.optional = optional;
	}

	public ProtocolParameterType getType() {
		return type;
	}

	public void setType(final ProtocolParameterType type) {
		this.type = type;
	}

	public String getRegex() {
		return regex;
	}

	public void setRegex(final String regex) {
		this.regex = regex;
	}

	@XmlTransient
	public ParamValuesFunction getPopulateFunction() {
		return populateFunction;
	}

	public void setPopulateFunction(final ParamValuesFunction populateFunction) {
		this.populateFunction = populateFunction;
		this.functionPopulated = this.populateFunction != null;
	}

	public boolean isFunctionPopulated() {
		return functionPopulated;
	}

	public void setFunctionPopulated(final boolean functionPopulated) {
		this.functionPopulated = functionPopulated;
	}

}
