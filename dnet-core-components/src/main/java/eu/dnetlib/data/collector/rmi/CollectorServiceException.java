package eu.dnetlib.data.collector.rmi;

import eu.dnetlib.common.rmi.RMIException;

public class CollectorServiceException extends RMIException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7523999812098059764L;

	public CollectorServiceException(String string) {
		super(string);
	}
	
	
	public CollectorServiceException(String string, Throwable exception) {
		super(string, exception);
	}

	public CollectorServiceException(Throwable exception) {
		super(exception);
	}

}
