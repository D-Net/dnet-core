package eu.dnetlib.data.collector.rmi;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.beans.factory.annotation.Required;

@XmlRootElement
public class ProtocolDescriptor {

	private String name;
	private List<ProtocolParameter> params = new ArrayList<ProtocolParameter>();

	public ProtocolDescriptor() {}

	public ProtocolDescriptor(final String name, final List<ProtocolParameter> params) {
		this.name = name;
		this.params = params;
	}

	public String getName() {
		return name;
	}

	@Required
	public void setName(final String name) {
		this.name = name;
	}

	public List<ProtocolParameter> getParams() {
		return params;
	}

	public void setParams(final List<ProtocolParameter> params) {
		this.params = params;
	}
}
