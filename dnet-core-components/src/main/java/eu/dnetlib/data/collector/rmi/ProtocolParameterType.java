package eu.dnetlib.data.collector.rmi;

import javax.xml.bind.annotation.XmlEnum;

@XmlEnum
public enum ProtocolParameterType {
	TEXT, NUMBER, LIST, BOOLEAN
}
