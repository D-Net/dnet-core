package eu.dnetlib.data.objectstore.rmi;

import com.google.gson.Gson;

import java.io.Serializable;

public class MetadataObjectRecord implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5640196854633924754L;

	public static MetadataObjectRecord initFromJson(final String input) {
		return new Gson().fromJson(input, MetadataObjectRecord.class);
	}

	public MetadataObjectRecord() {

	}

	public MetadataObjectRecord(final String id, final String record, final String mime) {
		super();
		this.id = id;
		this.record = record;
		this.mime = mime;
	}

	private String id;

	private String record;

	private String mime;

	public String getId() {
		return id;
	}

	public String getRecord() {
		return record;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public void setRecord(final String record) {
		this.record = record;
	}

	public String toJSON() {
		return new Gson().toJson(this);
	}

	public String getMime() {
		return mime;
	}

	public void setMime(final String mime) {
		this.mime = mime;
	}

}
