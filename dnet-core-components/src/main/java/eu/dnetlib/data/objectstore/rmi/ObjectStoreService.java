package eu.dnetlib.data.objectstore.rmi;

import eu.dnetlib.common.rmi.BaseService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.wsaddressing.W3CEndpointReference;
import java.util.List;

/**
 * Main ObjectStore Service interface.
 *
 * @author <a href="mailto:sandro.labruzzo at isti.cnr.it">Sandro La Bruzzo</a>
 * @version 0.0.1
 */
@WebService(targetNamespace = "http://services.dnetlib.eu/")
public interface ObjectStoreService extends BaseService {

	/**
	 * Returns ResultSet EPR for delivered ObjectStore records in a particular range date.
	 * <p>
	 * Please check service implementations for details on the expected format of the records in the result set epr.
	 * </p>
	 * <p>
	 * This method could be used for a bulk deliver of all objects in the store
	 * </p>
	 *
	 * @param obsId
	 *            identifier of the ObjectStore
	 * @param from
	 *            the minimum date of the object
	 * @param until
	 *            the maximum date of the object
	 * @return a ResultSet EPR. Each element of the result set contains the objIdentifier of a record and its URL for retrieve the
	 *         inputstream of the file.
	 * @throws ObjectStoreServiceException
	 *             the object store service exception
	 */
	@WebMethod(operationName = "deliverObjects", action = "deliverObjects")
	public W3CEndpointReference deliverObjects(@WebParam(name = "obsId") String obsId, @WebParam(name = "from") Long from, @WebParam(name = "until") Long until)
			throws ObjectStoreServiceException;

	/**
	 * Returns ResultSet EPR for delivered ObjectStore records.
	 * <p>
	 * Please check service implementations for details on the expected format of the records in the result set epr.
	 * </p>
	 *
	 * @param obsId
	 *            identifier of the ObjectStore
	 * @param eprId
	 *            id of a ResultSet EPR with the identifiers of the interesting objects. Each element of the result set contains the
	 *            objIdentifier of a record
	 * @return a ResultSet EPR. Each element of the result set contains the objIdentifier of a record and its URL for retrieve the
	 *         inputstream of the file.
	 * @throws ObjectStoreServiceException
	 *             the object store service exception
	 */
	@WebMethod(operationName = "deliverObjectsByIds", action = "deliverObjectsByIds")
	public W3CEndpointReference deliverObjectsByIds(@WebParam(name = "obsId") String obsId, @WebParam(name = "eprId") W3CEndpointReference eprId)
			throws ObjectStoreServiceException;

	/**
	 * Returns an URL to retrieve the ObjectStore record.
	 *
	 * @param obsId
	 *            identifier of the ObjectStore
	 * @param objectId
	 *            the id of the object
	 * @return the URL for retrieve the record
	 * @throws ObjectStoreServiceException
	 *             the object store service exception
	 */
	@WebMethod(operationName = "deliverObject", action = "deliverObject")
	public String deliverRecord(@WebParam(name = "obsId") String obsId, @WebParam(name = "objectId") String objectId) throws ObjectStoreServiceException;

	/**
	 * Feed the object in the objectStore
	 *
	 * @param obsId
	 *            identifier of the ObjectStore
	 * @param objectMetadata
	 *            the String serialized of the JSON object ObjectStoreFile
	 * @throws ObjectStoreServiceException
	 */

	@WebMethod(operationName = "feedObject", action = "feedObject")
	public void feedObject(@WebParam(name = "obsId") String obsId, @WebParam(name = "objectMetadata") String objectMetadata) throws ObjectStoreServiceException;

	/**
	 * Returns list of all stored indices.
	 *
	 * @return list of all stored indices
	 */
	@WebMethod(operationName = "getListOfObjectStores", action = "getListOfObjectStores")
	public List<String> getListOfObjectStores();

	/**
	 * Gets the size of the objectStore ID.
	 *
	 * @param obsId
	 *            the obs id
	 * @return the size
	 */
	@WebMethod(operationName = "getSize", action = "getSize")
	public int getSize(@WebParam(name = "obsId") String obsId) throws ObjectStoreServiceException;
}
