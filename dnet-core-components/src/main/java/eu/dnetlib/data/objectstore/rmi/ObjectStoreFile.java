package eu.dnetlib.data.objectstore.rmi;

import com.google.gson.Gson;

import java.io.Serializable;

/**
 * The Class ObjectStoreFile.
 */
public class ObjectStoreFile implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6501291693572693712L;

	/** The metadata related id. */
	private String metadataRelatedID;

	/** The object id. */
	private String objectID;

	/** The mime type. */
	private String mimeType;

	/** The access protocol. */
	private Protocols accessProtocol;

	/** The object uri . */
	private String URI;

	/** The username auth. */
	private String usernameAuth;

	/** The password auth. */
	private String passwordAuth;

	/** The downloaded url. */
	private String downloadedURL;

	/** The md5 sum of the file. */
	private String md5Sum;

	/** The file size. */
	private long fileSizeKB;

	/**
	 * Gets the downloaded url.
	 *
	 * @return the downloaded url
	 */
	public String getDownloadedURL() {
		return downloadedURL;
	}

	/**
	 * Sets the downloaded url.
	 *
	 * @param downloadedURL
	 *            the new downloaded url
	 */
	public void setDownloadedURL(final String downloadedURL) {
		this.downloadedURL = downloadedURL.replace("\\u003d", "=").replace("\\u0026", "&");;
	}

	/**
	 * Instantiates a new object store file.
	 */
	public ObjectStoreFile() {
		this.accessProtocol = Protocols.None;

	}

	/**
	 * Return a new instance of the object from a json String.
	 *
	 * @param jsonObject
	 *            the json object
	 * @return the object store file
	 */
	public static ObjectStoreFile createObject(final String jsonObject) {
		Gson g = new Gson();
		return g.fromJson(jsonObject, ObjectStoreFile.class);
	}

	/**
	 * Gets the object id.
	 *
	 * @return the object id
	 */
	public String getObjectID() {
		return objectID;
	}

	/**
	 * Sets the object id.
	 *
	 * @param objectID
	 *            the new object id
	 */
	public void setObjectID(final String objectID) {
		this.objectID = objectID;
	}

	/**
	 * Gets the mime type.
	 *
	 * @return the mime type
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * Sets the mime type.
	 *
	 * @param mimeType
	 *            the new mime type
	 */
	public void setMimeType(final String mimeType) {
		this.mimeType = mimeType;
	}

	/**
	 * Gets the access protocol.
	 *
	 * @return the access protocol
	 */
	public Protocols getAccessProtocol() {
		return accessProtocol;
	}

	/**
	 * Sets the access protocol.
	 *
	 * @param accessProtocol
	 *            the new access protocol
	 */
	public void setAccessProtocol(final Protocols accessProtocol) {
		this.accessProtocol = accessProtocol;
	}

	/**
	 * Gets the username auth.
	 *
	 * @return the username auth
	 */
	public String getUsernameAuth() {
		return usernameAuth;
	}

	/**
	 * Sets the username auth.
	 *
	 * @param usernameAuth
	 *            the new username auth
	 */
	public void setUsernameAuth(final String usernameAuth) {
		this.usernameAuth = usernameAuth;
	}

	/**
	 * Gets the password auth.
	 *
	 * @return the password auth
	 */
	public String getPasswordAuth() {
		return passwordAuth;
	}

	/**
	 * Sets the password auth.
	 *
	 * @param passwordAuth
	 *            the new password auth
	 */
	public void setPasswordAuth(final String passwordAuth) {
		this.passwordAuth = passwordAuth;
	}

	/**
	 * Gets the uri.
	 *
	 * @return the uri
	 */
	public String getURI() {
		return URI != null ? URI.replace("\\u003d", "=").replace("\\u0026", "&") : "";
	}

	/**
	 * Sets the uri.
	 *
	 * @param uri
	 *            the new uri
	 */
	public void setURI(final String uri) {
		if (uri != null) {
			URI = uri.replace("\\u003d", "=").replace("\\u0026", "&");
		}
	}

	/**
	 * Convert the object into a json String.
	 *
	 * @return the string
	 */
	public String toJSON() {
		Gson g = new Gson();
		return g.toJson(this).replace("\\u003d", "=").replace("\\u0026", "&");
	}

	/**
	 * Gets the related metadata id.
	 *
	 * @return the related metadata id.
	 */
	public String getMetadataRelatedID() {
		return metadataRelatedID;
	}

	/**
	 * Sets the related metadata id.
	 *
	 * @param metadataRelatedID
	 *            the related metadata id.
	 */
	public void setMetadataRelatedID(final String metadataRelatedID) {
		this.metadataRelatedID = metadataRelatedID;
	}

	/**
	 * @return the md5Sum
	 */
	public String getMd5Sum() {
		return md5Sum;
	}

	/**
	 * @param md5Sum
	 *            the md5Sum to set
	 */
	public void setMd5Sum(final String md5Sum) {
		this.md5Sum = md5Sum;
	}

	/**
	 * @return the fileSizeKB
	 */
	public long getFileSizeKB() {
		return fileSizeKB;
	}

	/**
	 * @param fileSizeKB the fileSizeKB to set
	 */
	public void setFileSizeKB(long fileSizeKB) {
		this.fileSizeKB = fileSizeKB;
	}

}
