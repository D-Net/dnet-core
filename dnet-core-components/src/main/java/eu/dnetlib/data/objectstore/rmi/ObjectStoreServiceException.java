package eu.dnetlib.data.objectstore.rmi;

import eu.dnetlib.common.rmi.RMIException;

/**
 * The Class ObjectStoreServiceException.
 */
public class ObjectStoreServiceException extends RMIException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7073846285219543315L;

	/**
	 * Instantiates a new object store service exception.
	 *
	 * @param string
	 *            the string
	 */
	public ObjectStoreServiceException(final String string) {
		super(string);
	}

	/**
	 * Instantiates a new object store service exception.
	 *
	 * @param string
	 *            the string
	 * @param exception
	 *            the exception
	 */
	public ObjectStoreServiceException(final String string, final Throwable exception) {
		super(string, exception);
	}

	/**
	 * Instantiates a new object store service exception.
	 *
	 * @param exception
	 *            the exception
	 */
	public ObjectStoreServiceException(final Throwable exception) {
		super(exception);
	}

}
