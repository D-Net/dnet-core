package eu.dnetlib.data.objectstore.rmi;

/**
  * The Enum Protocols.
  */
 public enum Protocols {
	 None,
	 HTTP, 
	 HTTPS,
	 FTP,
	 File_System,
	 FTPS
}
