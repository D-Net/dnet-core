package eu.dnetlib.data.objectstore.rmi;

/**
 * The Class ObjectStoreFileNotFoundException.
 */
public class ObjectStoreFileNotFoundException extends ObjectStoreServiceException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5418923557898374219L;

	/**
	 * Instantiates a new object store file not found exception.
	 *
	 * @param string
	 *            the string
	 */
	public ObjectStoreFileNotFoundException(final String string) {
		super(string);

	}

}
