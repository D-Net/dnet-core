package eu.dnetlib.data.transformation.service.rmi;

import eu.dnetlib.common.rmi.RMIException;

public class TransformationServiceException extends RMIException {

	private static final long serialVersionUID = 6186153317305098215L;

	public TransformationServiceException(String string) {
		super(string);
	}

	public TransformationServiceException(String string, Throwable exception) {
		super(string, exception);
	}

	public TransformationServiceException(Throwable exception) {
		super(exception);
	}

}
