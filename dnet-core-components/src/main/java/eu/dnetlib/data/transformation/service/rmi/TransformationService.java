package eu.dnetlib.data.transformation.service.rmi;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.wsaddressing.W3CEndpointReference;

import eu.dnetlib.common.rmi.BaseService;

@WebService(targetNamespace = "http://services.dnetlib.eu/")
public interface TransformationService extends BaseService {

	W3CEndpointReference transform(@WebParam(name = "ruleid") final String ruleid, @WebParam(name = "epr") final W3CEndpointReference epr)
			throws TransformationServiceException;
}
