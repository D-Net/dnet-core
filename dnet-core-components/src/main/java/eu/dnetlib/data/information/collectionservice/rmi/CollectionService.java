package eu.dnetlib.data.information.collectionservice.rmi;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;

import eu.dnetlib.common.rmi.BaseService;

/**
 * The Collection Service is used to ...
 * 
 * 
 */

@WebService(targetNamespace = "http://services.dnetlib.eu/")
public interface CollectionService extends BaseService {

	public String getCollection(@WebParam(name = "collId") final String collId) throws CollectionServiceException;

	public List<String> getCollections(@WebParam(name = "collIds") final List<String> collIds) throws CollectionServiceException;

	public void updateCollection(@WebParam(name = "coll") final String coll) throws CollectionServiceException;

	public void deleteCollection(@WebParam(name = "collId") final String collId) throws CollectionServiceException;

	public String createCollection(@WebParam(name = "coll") final String coll) throws CollectionServiceException;
}
