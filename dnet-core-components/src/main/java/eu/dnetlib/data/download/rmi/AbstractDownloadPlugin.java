package eu.dnetlib.data.download.rmi;

import java.util.List;

import com.google.gson.Gson;
import eu.dnetlib.data.download.rmi.DownloadItem.OpenAccessValues;
import org.joda.time.DateTime;
import org.joda.time.Days;

// TODO: Auto-generated Javadoc

/**
 * The Class AbstractDownloadPlugin.
 */
public abstract class AbstractDownloadPlugin {

	public static final int DEFAULT_TIMEOUT = 5000;

    /**
     * The regular expression.
     */
    protected List<String> regularExpression;


    /**
     * Check open access.
     *
     * @param input the input
     * @return the download item
     */
    public DownloadItem checkOpenAccess(final DownloadItem input) {
        if (input != null) {
            OpenAccessValues openAccess = OpenAccessValues.valueOf(input.getOpenAccess());
            switch (openAccess) {
                case OPEN:
                    return input;
                case CLOSED:
                case RESTRICTED:
                case UNKNOWN:
                    return null;
                case EMBARGO:
                    if (input.getEmbargoDate() == null) return null;
                    DateTime embargoDate = new DateTime(input.getEmbargoDate());
                    DateTime today = new DateTime();
                    Days days = Days.daysBetween(embargoDate, today);
                    if (days.getDays() <= 0) return input;
                    return null;
            }
        }
        return null;
    }


    public DownloadItem filterByRegexp(final DownloadItem input) {
        if (this.regularExpression != null && this.regularExpression.size() > 0) {
            final String baseURLs = input.getUrl();
            final List<String> urlsList = new Gson().fromJson(baseURLs, List.class);

            for (final String baseURL : urlsList) {
                for (final String regExp : regularExpression) {
                    if (baseURL.matches(regExp))
                        return input;
                }
            }
            return null;
        } else return input;
    }

    protected boolean checkUrlsNotNull(DownloadItem input, List<String> urls) {
        for (String s : urls) {
            String newURL = extractURL(s);
            if (newURL != null) {
                input.setOriginalUrl(s);
                input.setUrl(newURL);
                return true;
            }
        }
        return false;
    }

	public abstract String extractURL(final String baseURL) throws DownloadPluginException;


    /**
     * Gets the regular expression.
     *
     * @return the regular expression
     */
    public List<String> getRegularExpression() {
        return regularExpression;
    }


    /**
     * Sets the regular expression.
     *
     * @param regularExpression the new regular expression
     */
    public void setRegularExpression(final List<String> regularExpression) {
        this.regularExpression = regularExpression;
    }

}
