package eu.dnetlib.data.download.rmi;

import java.util.Date;

import com.google.gson.Gson;
import eu.dnetlib.miscutils.functional.hash.Hashing;

// TODO: Auto-generated Javadoc
/**
 * Download Item Class that serialize information in the resultset.
 */

public class DownloadItem {

	/** The id item metadata. */
	private String idItemMetadata;
	/**
	 * The url.
	 */
	private String url;
	/** The file name. */
	private String fileName;
	/** The original URL should be appear instead of the downloaded URL. */
	private String originalUrl;
	/** The open access. */
	private OpenAccessValues openAccess;
	/** The embargo date. */
	private Date embargoDate;

	/**
	 * From json.
	 *
	 * @param inputJson
	 *            the input json
	 * @return the download item
	 */
	public static DownloadItem newObjectfromJSON(final String inputJson) {
		Gson g = new Gson();
		DownloadItem instance = g.fromJson(inputJson, DownloadItem.class);
		return instance;
	}

	/**
	 * To json.
	 *
	 * @return the string
	 */
	public String toJSON() {
		return new Gson().toJson(this).replace("\\u003d", "=").replace("\\u0026", "&");
	}

	/**
	 * Gets the id item metadata.
	 *
	 * @return the id item metadata
	 */
	public String getIdItemMetadata() {
		return idItemMetadata;
	}

	/**
	 * Sets the id item metadata.
	 *
	 * @param idItemMetadata
	 *            the new id item metadata
	 */
	public void setIdItemMetadata(final String idItemMetadata) {
		this.idItemMetadata = idItemMetadata;
	}

	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		if (url == null) return url;
		return url.replace("\\u003d", "=").replace("\\u0026", "&");
	}

	/**
	 * Sets the url.
	 *
	 * @param url
	 *            the new url
	 */
	public void setUrl(final String url) {
		this.url = url;

	}

	/**
	 * Gets the original url.
	 *
	 * @return the originalUrl
	 */
	public String getOriginalUrl() {
		if (originalUrl == null) return originalUrl;
		return originalUrl.replace("\\u003d", "=").replace("\\u0026", "&");
	}

	/**
	 * Sets the original url.
	 *
	 * @param originalUrl
	 *            the originalUrl to set
	 */
	public void setOriginalUrl(final String originalUrl) {
		this.originalUrl = originalUrl;

	}

	/**
	 * Gets the file name.
	 *
	 * @return the file name
	 */
	public String getFileName() {
		if ((fileName == null) || "".equals(fileName)) return getIdItemMetadata() + "::" + Hashing.md5(getOriginalUrl());
		return fileName;
	}

	/**
	 * Sets the file name.
	 *
	 * @param fileName
	 *            the new file name
	 */
	public void setFileName(final String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the open access.
	 *
	 * @return the openAccess
	 */
	public String getOpenAccess() {
		return openAccess.toString();
	}

	/**
	 * Sets the open access.
	 *
	 * @param openAccess
	 *            the openAccess to set
	 */
	public void setOpenAccess(final String openAccess) {
		for (OpenAccessValues oaValue : OpenAccessValues.values()) {
			if (oaValue.toString().trim().toLowerCase().equals(openAccess.trim().toLowerCase())) {
				this.openAccess = oaValue;
				return;
			}
		}
		this.openAccess = OpenAccessValues.UNKNOWN;
	}

	/**
	 * Gets the embargo date.
	 *
	 * @return the embargoDate
	 */
	public Date getEmbargoDate() {
		return embargoDate;
	}

	/**
	 * Sets the embargo date.
	 *
	 * @param embargoDate
	 *            the embargoDate to set
	 */
	public void setEmbargoDate(final Date embargoDate) {
		this.embargoDate = embargoDate;
	}

	public enum OpenAccessValues {
		OPEN, RESTRICTED, CLOSED, EMBARGO, UNKNOWN
	}
}
