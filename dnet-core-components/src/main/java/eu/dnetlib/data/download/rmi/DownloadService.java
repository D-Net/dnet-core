package eu.dnetlib.data.download.rmi;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.wsaddressing.W3CEndpointReference;

import eu.dnetlib.common.rmi.BaseService;

/**
 *
 */
@WebService(targetNamespace = "http://services.dnetlib.eu/")
public interface DownloadService extends BaseService {

	/**
	 * Download the file in the resultset to the objectStore.
	 *
	 * @param resultSet
	 *            the resultset of the url Object
	 * @param plugin
	 *            the name of the plugin to be applied to retrieve the URL
	 * @param objectStoreID
	 *            the object store id where download the file
	 * @throws DownloadServiceException
	 *             the download service exception
	 */
	@WebMethod(operationName = "downloadFromResultSet", action = "downloadFromResultSet")
	public void downloadFromResultSet(W3CEndpointReference resultSet, String plugin, String objectStoreID, String protocol, String mimeType)
			throws DownloadServiceException;

	@WebMethod(operationName = "listPlugins", action = "listPlugins")
	public List<String> listPlugins() throws DownloadServiceException;

}
