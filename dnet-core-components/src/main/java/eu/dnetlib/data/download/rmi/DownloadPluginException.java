package eu.dnetlib.data.download.rmi;

public class DownloadPluginException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = -8480343562060711656L;

	public DownloadPluginException(final String string, final Throwable exception) {
		super(string, exception);
	}

	public DownloadPluginException(final Throwable exception) {
		super(exception);
	}

	public DownloadPluginException(final String msg) {
		super(msg);

	}

}
