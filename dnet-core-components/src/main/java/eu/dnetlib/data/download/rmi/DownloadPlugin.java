package eu.dnetlib.data.download.rmi;

import java.util.List;

/**
 * The DownloadPluin should retrieve the correct URL of the file to download
 */
public interface DownloadPlugin {

    /**
     * If Sets the base path, the plugin will start to download from the basePath in the file system
     *
     * @param basePath the base path
     */
    void setBasePath(String basePath);

    /**
     * This method retrieve the correct Url of the file from the starting URL
     *
     * @param item the starting url
     * @return the Url of the correct file
     */
    DownloadItem retrieveUrl(DownloadItem item) throws DownloadPluginException;

    /**
     * This method retrieve the correct Url of the file from a list of starting URL
     *
     * @param items the list of starting url
     * @return the list of the file URL
     */
    Iterable<DownloadItem> retrieveUrls(Iterable<DownloadItem> items) throws DownloadPluginException;

    /**
     * Get the name of the plugin
     *
     * @return the name of the plugin
     */
    String getPluginName();

    /**
     * Gets the regular expression.
     *
     * @return the regular expression
     */
    List<String> getRegularExpression();

    /**
     * Sets the regular expression.
     *
     * @param regularExpression the new regular expression
     */
    void setRegularExpression(final List<String> regularExpression);

}
