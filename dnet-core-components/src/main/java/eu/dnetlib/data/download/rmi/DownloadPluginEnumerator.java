package eu.dnetlib.data.download.rmi;

import java.util.Map;

import org.springframework.beans.factory.BeanFactoryAware;

public interface DownloadPluginEnumerator extends BeanFactoryAware {
	
	/**
	 * Get all beans implementing the DownloadPlugin interface, hashed by name.
	 * 
	 * @return the map of available download plugins
	 */
	Map<String, DownloadPlugin> getAll();
	
	/**
	 * Get all beans implementing the DownloadPlugin interface, hashed by protocol name.
	 * 
	 * @return the map of available download plugins
	 */
	Map<String, DownloadPlugin> getByProtocols();
	
}
