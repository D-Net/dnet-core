package eu.dnetlib.data.download.rmi;

import eu.dnetlib.common.rmi.RMIException;

public class DownloadServiceException extends RMIException {

	/**
	 *
	 */
	private static final long serialVersionUID = -5031281288958769185L;

	public DownloadServiceException(final String string) {
		super(string);
	}

	public DownloadServiceException(final String string, final Throwable exception) {
		super(string, exception);
	}

	public DownloadServiceException(final Throwable exception) {
		super(exception);
	}

}
