package eu.dnetlib.data.utility.cleaner.rmi;

import eu.dnetlib.common.rmi.BaseService;

import javax.jws.WebService;
import javax.xml.ws.wsaddressing.W3CEndpointReference;

/**
 * @author michele
 * 
 */
@WebService(targetNamespace = "http://services.dnetlib.eu/")
public interface CleanerService extends BaseService {

	/**
	 * @param epr
	 *            an epr of a resultset with dirty records
	 * @param ruleId
	 *            the identifier of a rule
	 * @return an epr of a resultset with clean records
	 * @throws CleanerException
	 */
	W3CEndpointReference clean(final W3CEndpointReference epr, final String ruleId) throws CleanerException;

}
