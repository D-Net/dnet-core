package eu.dnetlib.data.utility.cleaner.rmi;

import eu.dnetlib.common.rmi.RMIException;

public class CleanerException extends RMIException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7889315488590536918L;

	public CleanerException(final Throwable e) {
		super(e);
	}

	public CleanerException(final String message, final Throwable e) {
		super(message, e);
	}

	public CleanerException(final String message) {
		super(message);
	}

}
