package eu.dnetlib.functionality.cql.parse;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Maps;

public final class Relations {

    public static final BiMap<Relation, String> MAP = createMap();
    
    private Relations() {}

    private static BiMap<Relation, String> createMap() {
        BiMap<Relation, String> map = HashBiMap.create();
        map.forcePut(Relation.SRC, "src");
        map.forcePut(Relation.WITHIN, "within");
        map.forcePut(Relation.EXACT, "exact");
        map.forcePut(Relation.EQUAL, "=");
        map.forcePut(Relation.NOT, "<>");
        map.forcePut(Relation.GT,  ">");
        map.forcePut(Relation.GTE, ">=");
        map.forcePut(Relation.LT,  "<");
        map.forcePut(Relation.LTE, "<=");
        map.forcePut(Relation.ANY, "any");
        map.forcePut(Relation.ALL, "all");
        return Maps.unmodifiableBiMap(map);
    }
    
    public static String get(Relation rel) {
    	return MAP.get(rel);
    }
    
    public static Relation get(String rel) {
    	return MAP.inverse().get(rel);
    }    
    
    public static boolean contains(String value) {
    	return MAP.containsValue(value);
    }
    
    public static boolean contains(Relation rel) {
    	return MAP.inverse().containsValue(rel);
    }    
	
}
