package eu.dnetlib.functionality.cql.lucene;

public class QueryOptions {
	private SortOperation sort;

	public QueryOptions() {
	}

	public QueryOptions(SortOperation sort) {
		super();
		this.sort = sort;
	}

	public void merge(QueryOptions other) {
		if (getSort() != null) {
			setSort(other.getSort());
		}
	}

	public SortOperation getSort() {
		return sort;
	}

	public void setSort(SortOperation sort) {
		this.sort = sort;
	}

	public String appendOptions(String baseQuery) {
		if (sort != null) {
			return baseQuery + "&sort=" + sort.getField() + " " + sort.getMode();
		}
		return baseQuery;
	}

	@Override
	public String toString() {
		return sort.getField() + " " + sort.getMode();
	}
}
