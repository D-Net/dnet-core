package eu.dnetlib.functionality.cql.parse;

public enum Relation {
	SRC, WITHIN, EQUAL, NOT, GT, GTE, LT, LTE, ANY, ALL, EXACT
}
