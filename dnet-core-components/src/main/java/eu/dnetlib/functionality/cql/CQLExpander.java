package eu.dnetlib.functionality.cql;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.z3950.zing.cql.CQLBooleanNode;
import org.z3950.zing.cql.CQLNode;
import org.z3950.zing.cql.CQLOrNode;
import org.z3950.zing.cql.CQLParser;
import org.z3950.zing.cql.CQLTermNode;
import org.z3950.zing.cql.ModifierSet;

public class CQLExpander {

	private static final String OP = "or";

	private static final String _OP_ = " " + OP + " ";

	public CQLNode expand(CQLNode node, Set<String> fields) {
		return doExpand(node, fields);
	}

	private static CQLNode doExpand(CQLNode node, Set<String> fields) {

		if (node instanceof CQLBooleanNode) {
			return doExpand((CQLBooleanNode) node, fields);
		}

		if (node instanceof CQLTermNode) {
			return doExpand((CQLTermNode) node, fields);
		}

		//		if (node == null)
		//			return terms;

		throw new RuntimeException("error choice");
	}

	private static CQLNode doExpand(CQLBooleanNode node, Set<String> fields) {

		CQLNode left = doExpand(node.left, fields);
		CQLNode right = doExpand(node.right, fields);

		return new CQLOrNode(left, right, new ModifierSet("or"));
	}

	private static CQLNode doExpand(CQLTermNode node, Set<String> fields) {
		String expand = "";
		for (String field : fields) {
			expand += field + "=" + node.getTerm() + _OP_;
		}

		expand = StringUtils.removeEnd(expand, _OP_);
		try {
			return new CQLParser().parse(expand);
		} catch (Exception e) {
			throw new RuntimeException("unable to parse: " + expand);
		}
	}

}
