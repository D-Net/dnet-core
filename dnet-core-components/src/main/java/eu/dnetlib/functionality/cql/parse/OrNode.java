package eu.dnetlib.functionality.cql.parse;

public class OrNode extends BooleanNode {

	public OrNode(Node left, Node right) {
		super(left, right);
	}

	@Override
	public String op() {
		return "OR";
	}	
	
}
