package eu.dnetlib.functionality.cql.parse;

public class AndNode extends BooleanNode {

	public AndNode(Node left, Node right) {
		super(left, right);
	}

	@Override
	public String op() {
		return "AND";
	}

}
