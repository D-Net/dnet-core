package eu.dnetlib.functionality.cql.parse;

public class NotNode extends BooleanNode {

	public NotNode(Node left, Node right) {
		super(left, right);
	}

	@Override
	public String op() {
		return "NOT";
	}
	
	@Override
	public String toLucene() {
		return "(" + this.getLeft().toLucene() + " " + op() + " " + this.getRight().toLucene() + ")";
	}
	
	
}
