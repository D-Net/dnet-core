package eu.dnetlib.functionality.cql.parse;

public abstract class BooleanNode extends Node {

	private Node left;
	private Node right;

	public BooleanNode(Node left, Node right) {
		this.left = left;
		this.right = right;
	}

	public abstract String op();

	@Override
	public String toString() {
		return BooleanNode.class.getSimpleName() + "(" + getLeft() + ", " + getRight() + ")";
	}

	@Override
	public String toLucene() {
		return "(" + getLeft().toLucene() + " " + op() + " " + getRight().toLucene() + ")";
	}

	public Node getLeft() {
		return left;
	}

	public Node getRight() {
		return right;
	}

}
