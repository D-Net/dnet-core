package eu.dnetlib.functionality.cql.parse;

public abstract class Node {

	@Override
	public abstract String toString();
	
	public abstract String toLucene();
	
}
