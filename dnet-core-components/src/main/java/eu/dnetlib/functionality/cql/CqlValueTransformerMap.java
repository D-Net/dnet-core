package eu.dnetlib.functionality.cql;

import eu.dnetlib.miscutils.functional.UnaryFunction;

/**
 * Allows indices to register special field-specific value transformations, for example to normalize field values like
 * for solr dates for example.
 * 
 * @author marko
 * 
 */
public interface CqlValueTransformerMap {
	/**
	 * Return a function which normalizes the value.
	 * 
	 * @param field
	 *            field name
	 * @return normalized value
	 */
	UnaryFunction<String, String> transformerFor(String field);
}
