package eu.dnetlib.functionality.cql.lucene;

import eu.dnetlib.functionality.cql.CqlValueTransformerMap;
import eu.dnetlib.miscutils.functional.IdentityFunction;
import eu.dnetlib.miscutils.functional.UnaryFunction;

/**
 * Backward compatibiltiy implementation, which doesn't do anything to fields.
 * 
 * @author marko
 * 
 */
public class IdentityCqlValueTransformerMap implements CqlValueTransformerMap {

	private UnaryFunction<String, String> function = new IdentityFunction<String>();

	@Override
	public UnaryFunction<String, String> transformerFor(String field) {
		return function;
	}

}
