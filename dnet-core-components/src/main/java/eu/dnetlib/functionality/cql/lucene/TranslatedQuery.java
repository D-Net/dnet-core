package eu.dnetlib.functionality.cql.lucene;

import eu.dnetlib.functionality.cql.parse.Node;

public class TranslatedQuery {

	/**
	 * default field name.
	 */
	private static final String SERVER_CHOICE_FIELD = "cql.serverchoice:";

	private Node query;
	private QueryOptions options;

	public TranslatedQuery(Node query, QueryOptions options) {
		super();
		this.query = query;
		this.options = options;
	}

	public String asLucene() {
		return query.toLucene().replace(SERVER_CHOICE_FIELD, "").trim();
	}

	public Node getQuery() {
		return query;
	}

	public void setQuery(Node query) {
		this.query = query;
	}

	public QueryOptions getOptions() {
		return options;
	}

	public void setOptions(QueryOptions options) {
		this.options = options;
	}

}
