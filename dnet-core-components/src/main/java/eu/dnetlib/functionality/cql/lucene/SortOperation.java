package eu.dnetlib.functionality.cql.lucene;

import java.util.Vector;

import org.z3950.zing.cql.Modifier;

public class SortOperation {
	public enum Mode {
		asc, desc
	}

	private String field;
	private Mode mode;

	public SortOperation(String field, Mode mode) {
		super();
		this.field = field;
		this.mode = mode;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public Mode getMode() {
		return mode;
	}

	public void setMode(Mode mode) {
		this.mode = mode;
	}

	public static Mode modifierToMode(Modifier modifier) {
		if ("sort.ascending".equals(modifier.getType())) {
			return Mode.asc;
		}
		return Mode.desc;
	}

	public static Mode modifiersToMode(Vector<Modifier> vector) {
		if (vector.size() > 0) {
			return modifierToMode(vector.firstElement());
		}
		return Mode.desc;
	}
}
