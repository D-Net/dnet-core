package eu.dnetlib.functionality.index.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import eu.dnetlib.functionality.index.client.IndexClientException;
import eu.dnetlib.functionality.index.utils.MetadataReference;
import eu.dnetlib.miscutils.functional.UnaryFunction;

public abstract class QueryResponseFactory<T> {

	/**
	 * tells to getBrowsingResults method if empty fields must be returned or not.
	 */
	protected boolean returnEmptyFields;

	/**
	 * force solr to return record ranking or not.
	 */
	protected boolean includeRanking;

	/**
	 * utility for highlighting.
	 */
	protected UnaryFunction<String, String> highlightUtils;

	@Autowired
	protected BrowseAliases browseAliases;

	public abstract QueryResponseParser getQueryResponseParser(final IndexQueryResponse<T> queryRsp, final MetadataReference mdRef) throws IndexClientException;

	// /////////////////// setters and getters.

	@Required
	public void setReturnEmptyFields(final boolean returnEmptyFields) {
		this.returnEmptyFields = returnEmptyFields;
	}

	public boolean isReturnEmptyFields() {
		return returnEmptyFields;
	}

	@Required
	public void setHighlightUtils(final UnaryFunction<String, String> highlightUtils) {
		this.highlightUtils = highlightUtils;
	}

	@Required
	public void setIncludeRanking(final boolean includeRanking) {
		this.includeRanking = includeRanking;
	}

	public boolean isIncludeRanking() {
		return includeRanking;
	}

}
