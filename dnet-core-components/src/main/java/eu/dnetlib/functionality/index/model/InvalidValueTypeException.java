package eu.dnetlib.functionality.index.model;

/**
 * A runtime exception that will be thrown if the value types of the Any objects do not match the expected types.
 * 
 */
public class InvalidValueTypeException extends RuntimeException {

	/** exceptions can be serialized. */
	private static final long serialVersionUID = 1L;

	/** unsupported value type that caused this exception. */
	private final Class<?> _unsupportedType;

	/**
	 * Creates an InvalidValueTypeException with the given message.
	 * 
	 * @param message
	 *            The message for the user.
	 */
	public InvalidValueTypeException(final String message) {
		this(message, (Class<?>) null);
	}

	/**
	 * Instantiates a new invalid value type exception.
	 * 
	 * @param message
	 *            the message
	 * @param unsupportedType
	 *            the unsupported type
	 */
	public InvalidValueTypeException(final String message, final Class<?> unsupportedType) {
		super(message);
		_unsupportedType = unsupportedType;
	}

	/**
	 * Instantiates a new invalid value type exception.
	 * 
	 * @param unsupportedType
	 *            the unsupported type
	 */
	public InvalidValueTypeException(final Class<?> unsupportedType) {
		this("type not supported: " + unsupportedType, unsupportedType);
	}

	/**
	 * @param format
	 * @param e
	 */
	public InvalidValueTypeException(final String message, final Exception e) {
		super(message, e);
		_unsupportedType = null;
	}

	/**
	 * Gets the unsupported type. might be null.
	 * 
	 * @return the unsupported type
	 */
	public Class<?> getUnsupportedType() {
		return _unsupportedType;
	}

}
