package eu.dnetlib.functionality.index.utils;

/**
 * IndexMap keeps track of IndexDataStructure-to-physical index mappings and vice-versa.
 * 
 * @author claudio
 * 
 */
public class IndexFieldUtility {

	public static final String INDEX_FIELD_PREFIX = "__";

	public static final String INDEX_DSID_ALL = "ALL";

	public static final String SCORE_FIELD = "score";

	public static final String DS_ID = INDEX_FIELD_PREFIX + "dsid";

	public static final String DS_VERSION = INDEX_FIELD_PREFIX + "dsversion";

	public static final String RESULT = INDEX_FIELD_PREFIX + "result";

	public static final String ALL_FIELDS = INDEX_FIELD_PREFIX + "all";

	public static final String DELETE_DOCUMENT = INDEX_FIELD_PREFIX + "deleted";

	public static final String INDEX_RECORD_ID = INDEX_FIELD_PREFIX + "indexrecordidentifier";

	public static final String FULLTEXT_ID = INDEX_FIELD_PREFIX + "fulltext";

	public static final String INDEXNAME_PARAM = INDEX_FIELD_PREFIX + "indexName";

	public static final String queryAll = "*:*";

	public static final String FIELD_NAME = "name";

	public static final String FIELD_BROWSING_ALIAS_FOR = "browsingAliasFor";

	public static final String XPATH_BROWSING_ALIAS_FOR = "//FIELD[@" + FIELD_BROWSING_ALIAS_FOR + "]";

}