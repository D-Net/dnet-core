package eu.dnetlib.functionality.index.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import eu.dnetlib.data.provision.index.rmi.IndexServiceException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpDocumentNotFoundException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpException;
import eu.dnetlib.enabling.is.lookup.rmi.ISLookUpService;
import eu.dnetlib.enabling.is.registry.ISRegistryDocumentNotFoundException;
import eu.dnetlib.enabling.is.registry.rmi.ISRegistryException;
import eu.dnetlib.enabling.is.registry.rmi.ISRegistryService;
import eu.dnetlib.enabling.locators.UniqueServiceLocator;
import eu.dnetlib.functionality.index.client.IndexClientException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ServiceTools {

	private static final Log log = LogFactory.getLog(ServiceTools.class);

	@Resource
	private UniqueServiceLocator serviceLocator;

	@Resource
	private MetadataReferenceFactory mdFactory;

	public List<MetadataReference> listMDRefs() throws IndexClientException {
		return Lists.newArrayList(Iterables.transform(listMDRefsAsString(), s -> mdFactory.decodeMetadata(s)));
	}

	private List<String> quickSearchProfile(final String xquery) throws IndexClientException {
		try {
			return serviceLocator.getService(ISLookUpService.class).quickSearchProfile(xquery);
		} catch (ISLookUpException e) {
			throw new IndexClientException(e);
		}
	}

	public MetadataReference getMetadataRef(final String dsId) throws IndexServiceException {

		final String xquery = "for $x in //RESOURCE_PROFILE[.//RESOURCE_IDENTIFIER/@value='" + dsId + "']//CONFIGURATION " + "return concat("
				+ "$x/METADATA_FORMAT/text(),'-'," + "$x/METADATA_FORMAT_LAYOUT/text(),'-'," + "$x/METADATA_FORMAT_INTERPRETATION/text())";
		return mdFactory.decodeMetadata(getResourceProfileByQuery(xquery));
	}

	private String getResourceProfileByQuery(final String xquery) throws IndexServiceException {
		try {
			return serviceLocator.getService(ISLookUpService.class).getResourceProfileByQuery(xquery);
		} catch (ISLookUpDocumentNotFoundException e) {
			throw new IndexServiceException(e);
		} catch (ISLookUpException e) {
			throw new IndexServiceException(e);
		}
	}

	public String getIndexFields(final String dsId) throws IndexServiceException {

		return getIndexFields(getMetadataRef(dsId));
	}

	public String getIndexFields(final MetadataReference mdRef) {

		final String xquery = "for $x in collection('')/RESOURCE_PROFILE/BODY[CONFIGURATION/NAME='" + mdRef.getFormat()
				+ "'] return $x/STATUS/LAYOUTS/LAYOUT[@name='" + mdRef.getLayout() + "']/FIELDS";
		try {
			return getResourceProfileByQuery(xquery);
		} catch (IndexServiceException e) {
			log.warn("couldn't find Metadata format profile matching specs: " + mdRef.toString());
			throw new RuntimeException(e);
		}
	}

	public List<String> listDsIds() throws IndexClientException {
		final String xquery = "//RESOURCE_PROFILE[.//RESOURCE_TYPE/@value='IndexDSResourceType']//RESOURCE_IDENTIFIER/@value/string()";
		return quickSearchProfile(xquery);
	}

	private List<String> listMDRefsAsString() throws IndexClientException {
		final String xquery = "for $x in //RESOURCE_PROFILE[.//RESOURCE_TYPE/@value='MDFormatDSResourceType'] \n"
				+ "let $format:= $x//CONFIGURATION/NAME/string() \n"
				+ "let $interpretation:= $x//CONFIGURATION/INTERPRETATION/text() \n"
				+ "for $y in $x//LAYOUTS/LAYOUT \n"
				+ "  let $layout:= $y/@name/string() \n"
				+ "  return concat($format,'-',$layout,'-',$interpretation) ";
		return quickSearchProfile(xquery);
	}

	public Map<String, String> getIndexProperties(final String backendId) throws IndexClientException {

		String query = "for $x in /RESOURCE_PROFILE[.//RESOURCE_TYPE/@value=\"IndexServiceResourceType\"]//SERVICE_PROPERTIES/PROPERTY"
				+ " return concat($x/@key/string(),\":::\", $x/@value/string())";
		Map<String, String> indexProperties = new HashMap<String, String>();
		try {
			List<String> results = serviceLocator.getService(ISLookUpService.class).quickSearchProfile(query);
			if (results != null) {
				for (String s : results) {
					String[] values = s.split(":::");
					if (values != null && values.length == 2) {
						String key = values[0];
						String value = values[1];
						if (StringUtils.startsWith(key, backendId)) {
							indexProperties.put(StringUtils.substringAfter(key, backendId + ":"), value);
						}
					}
				}
			}
			return indexProperties;
		} catch (ISLookUpException e) {
			throw new IndexClientException();
		}
	}

	public String registerProfile(final String resourceProfile) throws IndexServiceException {
		try {
			return serviceLocator.getService(ISRegistryService.class).registerProfile(resourceProfile);
		} catch (ISRegistryException e) {
			throw new IndexServiceException(e);
		}
	}

	public boolean incrementHandledDataStructures(final String backendId) throws IndexServiceException {
		final String xquery = "let $x := //RESOURCE_PROFILE[HEADER/PROTOCOLS/PROTOCOL/@name='" + backendId + "'],"
				+ "$tot := $x//STATUS/HANDLED_DATASTRUCTURE/number() + 1 " + "return update replace $x//STATUS/HANDLED_DATASTRUCTURE with "
				+ "<HANDLED_DATASTRUCTURE>{$tot}</HANDLED_DATASTRUCTURE>";

		log.info("performing increment of HANDLED_DATASTRUCTURE");
		return executeXUpdate(xquery);
	}

	private boolean executeXUpdate(final String xquery) throws IndexServiceException {
		try {
			return serviceLocator.getService(ISRegistryService.class).executeXUpdate(xquery);
		} catch (ISRegistryException e) {
			throw new IndexServiceException(e);
		}
	}

	public String getServiceAddress(final String backendId) {
		final String xquery = "let $x := //RESOURCE_PROFILE[HEADER/PROTOCOLS/PROTOCOL/@name='" + backendId + "']"
				+ "return $x//PROTOCOL[./@name='SOAP']/@address/string()";
		try {
			return getResourceProfileByQuery(xquery);
		} catch (IndexServiceException e) {
			log.warn("couldn't find service Address for index Service with protocol: " + backendId);
			return "";
		}
	}

	public boolean deleteIndexDS(final String dsId) throws IndexServiceException {
		try {
			return serviceLocator.getService(ISRegistryService.class).deleteProfile(dsId);
		} catch (ISRegistryDocumentNotFoundException e) {
			throw new IndexServiceException(e);
		} catch (ISRegistryException e) {
			throw new IndexServiceException(e);
		}
	}

	public List<String> getBackendIds(final MetadataReference mdRef) throws IndexServiceException {
		String query = "distinct-values(//RESOURCE_PROFILE[.//METADATA_FORMAT='%s' and .//METADATA_FORMAT_LAYOUT='%s' and .//METADATA_FORMAT_INTERPRETATION='%s']//BACKEND/@ID/string())";
		try {
			String instanceQuery = String.format(query, mdRef.getFormat(), mdRef.getLayout(), mdRef.getInterpretation());
			log.debug("Executing query to IS: " + instanceQuery);
			return serviceLocator.getService(ISLookUpService.class).quickSearchProfile(instanceQuery);
		} catch (ISLookUpException e) {
			throw new IndexServiceException(e);
		}
	}
}
