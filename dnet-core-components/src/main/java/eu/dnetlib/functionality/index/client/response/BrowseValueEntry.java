package eu.dnetlib.functionality.index.client.response;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BrowseValueEntry implements Comparable<BrowseValueEntry> {
	
	private String value;
	
	private int size;
	
	public BrowseValueEntry() {}
	
	public BrowseValueEntry(final String value, final int size) {
		this.value = value;
		this.size = size;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(final String value) {
		this.value = value;
	}
	
	public int getSize() {
		return size;
	}
	
	public void setSize(final int size) {
		this.size = size;
	}

	@Override
	public int compareTo(final BrowseValueEntry e) {
		return Integer.compare(getSize(), e.getSize());
	}

}
