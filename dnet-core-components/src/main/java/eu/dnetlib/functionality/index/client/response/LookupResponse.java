package eu.dnetlib.functionality.index.client.response;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import eu.dnetlib.functionality.index.query.QueryResponseParser;

@XmlRootElement
public class LookupResponse {

	private long total;
	private long from;
	private long to;
	private List<String> records;

	public LookupResponse() {}

	public LookupResponse(final QueryResponseParser parser) {
		this.total = parser.getNumFound();
		this.from = parser.getStart();
		this.to = parser.getStart() + parser.getResults().size();
		this.records = parser.getResults();

	}

	public LookupResponse(final long total, final long from, final long to, final List<String> records) {
		this.total = total;
		this.from = from;
		this.to = to;
		this.records = records;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(final long total) {
		this.total = total;
	}

	public long getFrom() {
		return from;
	}

	public void setFrom(final long from) {
		this.from = from;
	}

	public long getTo() {
		return to;
	}

	public void setTo(final long to) {
		this.to = to;
	}

	public List<String> getRecords() {
		return records;
	}

	public void setRecords(final List<String> records) {
		this.records = records;
	}

}
