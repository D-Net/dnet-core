package eu.dnetlib.functionality.index.utils;


public class MetadataReferenceFactory {

	public static MetadataReference getMetadata(final String format, final String layout, final String interpretation) {
		return new MetadataReference(format, layout, interpretation);
	}

	public static MetadataReference decodeMetadata(final String encoded) {
		String[] split = encoded.split("-");
		if (split.length == 3) return getMetadata(split[0], split[1], split[2]);

		throw new IllegalStateException("malformed metadata reference: " + encoded);
	}

}
