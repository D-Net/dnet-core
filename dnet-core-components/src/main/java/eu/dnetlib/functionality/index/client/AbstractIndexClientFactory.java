package eu.dnetlib.functionality.index.client;

import org.springframework.beans.factory.annotation.Autowired;

import eu.dnetlib.functionality.index.utils.ServiceTools;

/**
 * The Class AbstractIndexClientFactory.
 */
public abstract class AbstractIndexClientFactory implements IndexClientFactory {

	/** The backend id. */
	private String backendId;

	@Autowired
	protected ServiceTools isQueryTools;

	/**
	 * Inits the class.
	 */
	public abstract void init() throws IndexClientException;

	/**
	 * {@inheritDoc}
	 *
	 * @see eu.dnetlib.functionality.index.client.IndexClientFactory#getBackendId()
	 */
	@Override
	public String getBackendId() {
		return backendId;
	}

	/**
	 * Sets the backend id.
	 *
	 * @param backendId
	 *            the backend id
	 */
	public void setBackendId(final String backendId) {
		this.backendId = backendId;
	}

}
