package eu.dnetlib.functionality.index.model.document;

/**
 * Possible IndexDocument Status.
 */
public enum Status {

	/** The marked. */
	MARKED,
	/** The error. */
	ERROR,
	/** The ok. */
	OK
}
