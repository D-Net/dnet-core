package eu.dnetlib.functionality.index.query;

public enum QueryLanguage {
	CQL, SOLR
}
