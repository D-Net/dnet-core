package eu.dnetlib.functionality.index.client.response;

import java.util.List;

public class BrowseEntry {
	private String field;
	private String label;
	private List<BrowseValueEntry> values;
	
	public BrowseEntry() {}
	
	public BrowseEntry(final String field, final String label,final  List<BrowseValueEntry> values) {
		this.field = field;
		this.label = label;
		this.values = values;
	}

	public String getField() {
		return field;
	}

	public void setField(final String field) {
		this.field = field;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public List<BrowseValueEntry> getValues() {
		return values;
	}

	public void setValues(final List<BrowseValueEntry> values) {
		this.values = values;
	}

}
