package eu.dnetlib.functionality.index.client;

import eu.dnetlib.functionality.index.utils.MetadataReference;

public interface IndexClientFactory {

	String getBackendId();

	IndexClient getClient(String format, String layout, String interpretation) throws IndexClientException;

	IndexClient getClient(MetadataReference mdRef) throws IndexClientException;

	IndexClient getClient(String collection) throws IndexClientException;

}
