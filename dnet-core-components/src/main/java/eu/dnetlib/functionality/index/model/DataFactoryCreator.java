package eu.dnetlib.functionality.index.model;

import eu.dnetlib.functionality.index.model.impl.DefaultDataFactoryImpl;

/**
 * Helper class to decouple Any and Record interfaces better from default implementation.
 */
public final class DataFactoryCreator {

	/**
	 * Private default constructor to avoid instance creation.
	 */
	private DataFactoryCreator() {
	}

	/**
	 * @return instance of the default DataFactory.
	 */
	public static DataFactory newInstance() {
		return new DefaultDataFactoryImpl();
	}
}
