package eu.dnetlib.functionality.index.solr.feed;

import com.google.common.base.Function;

/**
 * Created by claudio on 17/11/15.
 */
public abstract class ResultTransformer implements Function<String, String> {

	public enum Mode {compress, empty, xslt, base64}

	protected Mode mode;

	public ResultTransformer(final Mode mode) {
		this.mode = mode;
	}

	public Mode getMode() {
		return mode;
	}

	public void setMode(final Mode mode) {
		this.mode = mode;
	}

}
