package eu.dnetlib.functionality.index.model.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import eu.dnetlib.functionality.index.model.Any;
import eu.dnetlib.functionality.index.model.AnyMap;
import eu.dnetlib.functionality.index.model.AnySeq;
import eu.dnetlib.functionality.index.model.DataFactory;
import eu.dnetlib.functionality.index.model.Value;

/**
 * immutable decorator for an AnyMap.
 */
public class ImmutableAnyMapImpl implements AnyMap {

	/** The underlying anymap. */
	private final AnyMap _anyMap;

	/** The underlying map, as immutable. */
	private final Map<String, Any> _immutable;

	public ImmutableAnyMapImpl(AnyMap map) {
		_anyMap = map;
		_immutable = Collections.unmodifiableMap(map);
	}

	@Override
	public void add(String key, Any value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public AnyMap asMap() {
		return _anyMap.asMap();
	}

	@Override
	public AnySeq asSeq() {
		return _anyMap.asSeq();
	}

	@Override
	public Value asValue() {
		return _anyMap.asValue();
	}

	@Override
	public void clear() {
		_immutable.clear();
	}

	@Override
	public boolean containsKey(Object key) {
		return _immutable.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return _immutable.containsValue(value);
	}

	@Override
	public Set<java.util.Map.Entry<String, Any>> entrySet() {
		return _immutable.entrySet();
	}

	@Override
	public boolean equals(Object o) {
		return _immutable.equals(o);
	}

	@Override
	public Any get(Object key) {
		return _immutable.get(key);
	}

	@Override
	public Boolean getBooleanValue(String key) {
		return _anyMap.getBooleanValue(key);
	}

	@Override
	public Date getDateTimeValue(String key) {
		return _anyMap.getDateTimeValue(key);
	}

	@Override
	public Date getDateValue(String key) {
		return _anyMap.getDateValue(key);
	}

	@Override
	public Double getDoubleValue(String key) {
		return _anyMap.getDoubleValue(key);
	}

	@Override
	public DataFactory getFactory() {
		return _anyMap.getFactory();
	}

	@Override
	public Long getLongValue(String key) {
		return _anyMap.getLongValue(key);
	}

	@Override
	public AnyMap getMap(String key) {
		return _anyMap.getMap(key);
	}

	@Override
	public AnyMap getMap(String key, boolean create) {
		return _anyMap.getMap(key, create);
	}

	@Override
	public AnySeq getSeq(String key) {
		return _anyMap.getSeq(key);
	}

	@Override
	public AnySeq getSeq(String key, boolean create) {
		return _anyMap.getSeq(key, create);
	}

	@Override
	public String getStringValue(String key) {
		return _anyMap.getStringValue(key);
	}

	@Override
	public Value getValue(String key) {
		return _anyMap.getValue(key);
	}

	@Override
	public ValueType getValueType() {
		return _anyMap.getValueType();
	}

	@Override
	public int hashCode() {
		return _immutable.hashCode();
	}

	@Override
	public boolean isBoolean() {
		return _anyMap.isBoolean();
	}

	@Override
	public boolean isDate() {
		return _anyMap.isDate();
	}

	@Override
	public boolean isDateTime() {
		return _anyMap.isDateTime();
	}

	@Override
	public boolean isDouble() {
		return _anyMap.isDouble();
	}

	@Override
	public boolean isEmpty() {
		return _immutable.isEmpty();
	}

	@Override
	public boolean isLong() {
		return _anyMap.isLong();
	}

	@Override
	public boolean isMap() {
		return _anyMap.isMap();
	}

	@Override
	public boolean isNumber() {
		return _anyMap.isNumber();
	}

	@Override
	public boolean isSeq() {
		return _anyMap.isSeq();
	}

	@Override
	public boolean isString() {
		return _anyMap.isString();
	}

	@Override
	public boolean isValue() {
		return _anyMap.isValue();
	}

	@Override
	public Iterator<Any> iterator() {
		return _anyMap.iterator();
	}

	@Override
	public Set<String> keySet() {
		return _immutable.keySet();
	}

	@Override
	public Any put(String key, Any value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Any put(String key, Boolean value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Any put(String key, Number value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Any put(String key, String value) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void putAll(Map<? extends String, ? extends Any> m) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Any remove(Object key) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int size() {
		return _immutable.size();
	}

	@Override
	public Collection<Any> values() {
		return _immutable.values();
	}

}
