package eu.dnetlib.functionality.index.query;

import eu.dnetlib.functionality.index.client.IndexClientException;
import eu.dnetlib.functionality.index.utils.MetadataReference;
import org.apache.solr.client.solrj.response.QueryResponse;

/**
 * The Class SolrIndexQueryResponseFactory.
 */
public class SolrIndexQueryResponseFactory extends QueryResponseFactory<QueryResponse> {

	/**
	 * {@inheritDoc}
	 * 
	 * @throws IndexClientException
	 *
	 * @see QueryResponseFactory#getQueryResponseParser(IndexQueryResponse,
	 *      MetadataReference)
	 */
	@Override
	public QueryResponseParser getQueryResponseParser(final IndexQueryResponse<QueryResponse> queryRsp, final MetadataReference mdRef)
			throws IndexClientException {

		QueryResponse response = queryRsp.getContextualQueryResponse();
		return new SolrResponseParser(highlightUtils, browseAliases.get(mdRef), returnEmptyFields, includeRanking, response);
	}

}
