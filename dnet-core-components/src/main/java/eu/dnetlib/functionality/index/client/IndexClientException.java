package eu.dnetlib.functionality.index.client;

import eu.dnetlib.data.provision.index.rmi.IndexServiceException;

public class IndexClientException extends IndexServiceException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1851955470714206331L;

	public IndexClientException() {
		super();
	}

	public IndexClientException(final String message) {
		super(message);
	}

	public IndexClientException(final Throwable cause) {
		super(cause);
	}

	public IndexClientException(final String message, final Throwable cause) {
		super(message, cause);
	}

}
