package eu.dnetlib.functionality.index.query;

public interface IndexQueryResponse<T> {

	public T getContextualQueryResponse();

}
