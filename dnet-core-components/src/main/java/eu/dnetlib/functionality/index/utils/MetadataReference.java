package eu.dnetlib.functionality.index.utils;


import com.google.common.collect.ComparisonChain;

/**
 * Container class for metadata format, layout and interpretation.
 * 
 * @author claudio
 *
 */
public class MetadataReference {
	
	/**
	 * Metadata format.
	 */
	private String format;

	/**
	 * Metadata layout.
	 */
	private String layout;
	
	/**
	 * Metadata interpretation.
	 */
	private String interpretation;
	
	/**
	 * Constructor for MetadataReference.
	 * 
	 * @param format
	 * 			Metadata format
	 * @param layout
	 * 			Metadata layout
	 * @param interpretation
	 * 			Metadata interpretation
	 */
	public MetadataReference(final String format, final String layout, final String interpretation) {
		this.format = format;
		this.layout = layout;
		this.interpretation = interpretation;
	}
	
	public String getFormat() {
		return format;
	}

	public String getLayout() {
		return layout;
	}

	public String getInterpretation() {
		return interpretation;
	}

	@Override
	public boolean equals(final Object that) {
		
		if (!(that instanceof MetadataReference))
			return false;
		
		final MetadataReference mdRef = (MetadataReference) that;
		
		return ComparisonChain.start()
	        .compare(this.format, mdRef.getFormat())
	        .compare(this.layout, mdRef.getLayout())
	        .compare(this.interpretation, mdRef.getInterpretation())
	        .result() == 0;		
	}
	
	@Override
	public int hashCode() {
		return getFormat().hashCode() + getLayout().hashCode() + getInterpretation().hashCode();
	}	
	
	@Override
	public String toString() {
		return getFormat() + "-" + getLayout() + "-" + getInterpretation();
	}
	
}
