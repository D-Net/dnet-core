package eu.dnetlib.functionality.index.solr.cql;

import eu.dnetlib.miscutils.functional.UnaryFunction;

/**
 * Simply and not very roboust normalizer for solr dates. Basically it handles well yyyy-mm-dd and
 * yyyy-mm-ddThh:mm:ssZ
 * 
 * @author marko
 * 
 */
public class SimpleDateValueTransformer implements UnaryFunction<String, String> {
	@Override
	public String evaluate(final String value) {
		if (!value.endsWith("Z"))
			return value + "T00:00:00Z";
		return value;
	}
}