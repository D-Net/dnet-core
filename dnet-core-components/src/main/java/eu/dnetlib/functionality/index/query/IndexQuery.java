package eu.dnetlib.functionality.index.query;

/**
 * The Class IndexQuery.
 */
public interface IndexQuery {

	IndexQuery setQueryOffset(int offset);

	IndexQuery setQueryLimit(int limit);

}
