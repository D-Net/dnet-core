package eu.dnetlib.functionality.index.model.impl;

import java.io.Serializable;
import java.util.Date;

/**
 * Pair of a Date and optionally the original string from which it was parsed. Used to ensure that Date or Timestamp
 * Values parsed from JSON or BON string values are reproduced in exactly the same form when converted to a string
 * again.
 */
public final class DateValue implements Serializable {

	/** serializable. */
	private static final long serialVersionUID = 1L;

	/** the actual date or timestamp value. */
	private final Date _date;

	/** the original string, if the date/timestamp value is parsed from a string and not created immediately in Java. */
	private final String _originalString;

	/** create immediately from a java date. */
	DateValue(final Date date) {
		this(date, null);
	}

	/** create a pair of a date and the string it was parsed from. */
	public DateValue(final Date date, final String originalString) {
		super();
		_date = date;
		_originalString = originalString;
	}

	/** @return the date. */
	public Date getDate() {
		return _date;
	}

	/** @return {@link Date#getTime()} of the contained date. */
	public long getTime() {
		return _date.getTime();
	}

	/** @return the original string, if the date was parsed. */
	public String getOriginalString() {
		return _originalString;
	}

	/** @return true if an original string is stored. */
	public boolean hasOriginalString() {
		return _originalString != null;
	}

	/**
	 * @return true if the other object is a {@link DateValue}, too, and contains an equals date object. The original
	 *         string is not relevant for the comparison.
	 */
	@Override
	public boolean equals(final Object obj) {
		if (obj != null && obj instanceof DateValue) {
			return _date.equals(((DateValue) obj)._date);
		}
		return false;
	}

	/** @return hashCode of the contained date object. */
	@Override
	public int hashCode() {
		return _date.hashCode();
	}

	/** @return toString of contained date object. */
	@Override
	public String toString() {
		return _date.toString();
	}

}
