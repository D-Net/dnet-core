package eu.dnetlib.functionality.index.client;

import java.io.Closeable;
import java.util.List;
import java.util.Map;

import eu.dnetlib.functionality.cql.CqlValueTransformerMap;
import eu.dnetlib.functionality.index.client.response.BrowseEntry;
import eu.dnetlib.functionality.index.client.response.LookupResponse;
import eu.dnetlib.functionality.index.query.IndexQueryFactory;
import eu.dnetlib.functionality.index.utils.MetadataReference;

public interface IndexClient extends Closeable {

	LookupResponse lookup(String query, List<String> filterQuery, int from, int to) throws IndexClientException;

	List<BrowseEntry> browse(String query, List<String> browseFields, int max) throws IndexClientException;

	List<BrowseEntry> browse(String query, List<String> browseFields, int max, List<String> filterQuery) throws IndexClientException;

	long delete(String query) throws IndexClientException;

	CqlValueTransformerMap getCqlValueTransformerMap(final MetadataReference mdRef) throws IndexClientException;

	Map<String, String> getServiceProperties();

	IndexQueryFactory getIndexQueryFactory();

}
