package eu.dnetlib.functionality.index.model.document;

import java.util.Collection;

/**
 * The Interface IndexDocument.
 */
public interface IndexDocument {

	/**
	 * Adds a field with the given name, value and boost. If a field with the name already exists, then the given value is appended to the
	 * value of that field, with the new boost. If the value is a collection, then each of its values will be added to the field.
	 * 
	 * The class type of value and the name parameter should match schema.xml. schema.xml can be found in conf directory under the solr home
	 * by default.
	 * 
	 * @param name
	 *            Name of the field, should match one of the field names defined under "fields" tag in schema.xml.
	 * @param value
	 *            Value of the field, should be of same class type as defined by "type" attribute of the corresponding field in schema.xml.
	 */
	public void addField(String name, Object value);

	/**
	 * Set a field with implied null value for boost.
	 * 
	 * @param name
	 *            name of the field to set
	 * @param value
	 *            value of the field
	 */
	public void setField(String name, Object value);

	/**
	 * Get the first value for a field.
	 * 
	 * @param name
	 *            name of the field to fetch
	 * @return first value of the field or null if not present
	 */
	public Object getFieldValue(String name);

	/**
	 * Gets the field.
	 * 
	 * @param field
	 *            the field
	 * @return the field
	 */
	public IndexField getField(String field);

	/**
	 * Remove a field from the document.
	 * 
	 * @param name
	 *            The field name whose field is to be removed from the document
	 * @return the previous field with <tt>name</tt>, or <tt>null</tt> if there was no field for <tt>key</tt>.
	 */
	public IndexField removeField(String name);

	/**
	 * Get all the values for a field.
	 * 
	 * @param name
	 *            name of the field to fetch
	 * @return value of the field or null if not set
	 */
	public Collection<Object> getFieldValues(String name);

	/**
	 * Get all field names.
	 * 
	 * @return Set of all field names.
	 */
	public Collection<String> getFieldNames();

	/**
	 * return a copy of index Document.
	 * 
	 * @return the index document
	 */
	public IndexDocument deepCopy();

	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	public Status getStatus();

	/**
	 * The set status.
	 * 
	 * @param status
	 *            the status
	 * @return the index document
	 */
	public IndexDocument setStatus(Status status);

	/**
	 * If there was an error building the document, it is described here.
	 * 
	 * @return the error
	 */
	public Throwable getError();

	/**
	 * Sets the error.
	 * 
	 * @param error
	 *            the error
	 * @return the index document
	 */
	public IndexDocument setError(final Throwable error);

	/**
	 * Sets the ok status to the index document.
	 * 
	 * @return the index document
	 */
	public IndexDocument setOK();

	/**
	 * Sets the status marked to the index document.
	 * 
	 * @return the index document
	 */
	public IndexDocument setMarked();

}
