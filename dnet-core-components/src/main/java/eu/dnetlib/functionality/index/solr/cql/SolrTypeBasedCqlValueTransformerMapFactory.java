package eu.dnetlib.functionality.index.solr.cql;

import java.util.Map;

import eu.dnetlib.functionality.index.model.Any.ValueType;
import eu.dnetlib.miscutils.functional.UnaryFunction;
import org.springframework.beans.factory.annotation.Required;

/**
 * Factory for the SolrTypeBasedCqlValueTransformerMap class objects
 * 
 * @author claudio
 * 
 */
public class SolrTypeBasedCqlValueTransformerMapFactory {

	/**
	 * Map of functions, injected via spring.
	 */
	private Map<String, UnaryFunction<String, String>> transformerMap;

	/**
	 * Method returns a new instance of SolrTypeBasedCqlValueTransformerMap.
	 * 
	 * @param schema
	 * @return the SolrTypeBasedCqlValueTransformerMap
	 */
	public SolrTypeBasedCqlValueTransformerMap getIt(final Map<String, ValueType> schema) {
		return new SolrTypeBasedCqlValueTransformerMap(schema, getTransformerMap());
	}

	@Required
	public void setTransformerMap(Map<String, UnaryFunction<String, String>> transformerMap) {
		this.transformerMap = transformerMap;
	}

	public Map<String, UnaryFunction<String, String>> getTransformerMap() {
		return transformerMap;
	}

}
