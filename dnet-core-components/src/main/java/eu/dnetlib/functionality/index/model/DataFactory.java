package eu.dnetlib.functionality.index.model;

import java.util.Date;

import eu.dnetlib.functionality.index.model.Any.ValueType;

/**
 * Interface for creation of data objects.
 */
public interface DataFactory {

	/** default instance. */
	DataFactory DEFAULT = DataFactoryCreator.newInstance();

	/**
	 * @return The created AnyMap object
	 */
	AnyMap createAnyMap();

	/**
	 * @return The created AnySeq object
	 */
	AnySeq createAnySeq();

	/**
	 * @param value
	 *            the String to create the Value from.
	 * @return The created Value object.
	 */
	Value createStringValue(String value);

	/**
	 * @param value
	 *            the Boolean to create the Value from.
	 * @return The created Value object.
	 */
	Value createBooleanValue(Boolean value);

	/**
	 * @param value
	 *            the Long to create the Value from.
	 * @return The created Value object.
	 */
	Value createLongValue(Long value);

	/**
	 * @param value
	 *            the int to create the Value from.
	 * @return The created Value object.
	 */
	Value createLongValue(int value);

	/**
	 * @param value
	 *            the Double to create the Value from.
	 * @return The created Value object.
	 */
	Value createDoubleValue(Double value);

	/**
	 * @param value
	 *            the float to create the Value from.
	 * @return The created Value object.
	 */
	Value createDoubleValue(float value);

	/**
	 * @param value
	 *            the Date to create the Value from.
	 * @return The created Value object.
	 */
	Value createDateValue(Date value);

	/**
	 * @param value
	 *            the DateTime to create the Value from.
	 * @return The created Value object.
	 */
	Value createDateTimeValue(Date value);

	/**
	 * @param value
	 *            The value
	 * @param type
	 *            The type
	 * @return The Value object with correct type, InvalidvalueTypeException else.
	 */
	Value parseFromString(String value, String type);

	/**
	 * @param value
	 *            The value
	 * @param valueType
	 *            The value's type
	 * @return The Value object with correct type, InvalidvalueTypeException else.
	 */
	Value parseFromString(final String value, final ValueType valueType);

	/**
	 * Tries to convert the String to a Date or Timestamp Value, if not possible return a String Value.
	 * 
	 * @param value
	 *            The value to check for Date/Timestamp
	 * @return The Value object with guessed type, InvalidvalueTypeException if value is null.
	 */
	Value tryDateTimestampParsingFromString(String value);

	/**
	 * @param object
	 *            The object
	 * @return The value matching the class of given object, InvalidValueTypeException otherwise.
	 * @deprecated Use {@link #autoConvertValue(Object)} instead
	 */
	@Deprecated
	Value parseFromObject(final Object object);

	/**
	 * auto converts the given object into the object's corresponding Value.
	 * 
	 * @param object
	 *            The object, must be one of the simple types
	 * @return The value matching the class of given object, InvalidValueTypeException otherwise.
	 * 
	 */
	Value autoConvertValue(final Object object);

	/**
	 * Clone Any object.
	 * 
	 * @param source
	 *            the source
	 * 
	 * @return the attribute
	 */
	Any cloneAny(final Any source);

	/**
	 * Clone AnyMap object.
	 * 
	 * @param source
	 *            the source
	 * 
	 * @return the attribute
	 */
	AnyMap cloneAnyMap(final AnyMap source);

	/**
	 * Clone AnySeq object.
	 * 
	 * @param source
	 *            the source
	 * 
	 * @return the attribute
	 */
	AnySeq cloneAnySeq(final AnySeq source);

}
