package eu.dnetlib.functionality.index.query;

import eu.dnetlib.functionality.index.query.IndexQueryResponse;
import org.apache.solr.client.solrj.response.QueryResponse;

/**
 * The Class SolrIndexQueryResponse.
 */
public class SolrIndexQueryResponse implements IndexQueryResponse<QueryResponse> {

	private QueryResponse solrQueryResponse;

	public SolrIndexQueryResponse(final QueryResponse solrQueryResponse) {
		this.solrQueryResponse = solrQueryResponse;
	}

	@Override
	public QueryResponse getContextualQueryResponse() {
		return solrQueryResponse;
	}

}
