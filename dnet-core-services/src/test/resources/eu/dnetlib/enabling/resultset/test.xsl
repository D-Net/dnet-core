<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="first">
		<second><xsl:value-of select="."/></second>
	</xsl:template>
</xsl:stylesheet>