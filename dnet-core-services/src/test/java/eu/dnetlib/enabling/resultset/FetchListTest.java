package eu.dnetlib.enabling.resultset;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FetchListTest {
	
	/**
	 * object to test
	 */
	private FetchList<String> fetchList;
	
	private List<String> list;
	private Iterator<String> iter;
	
	private int FETCH_SIZE = 10;
	
	private int TOTAL_ELEMENTS = 100;

	@Before
	public void setUp() throws Exception {
		list = new ArrayList<String>();
		for (int i = 0; i < TOTAL_ELEMENTS; i++) {
			list.add("XXX" + i);
		}
		this.iter = list.iterator();
		fetchList = new FetchList<String>(iter, FETCH_SIZE);
		
	}

	@Test
	public void testFetchList() {
		assertNotNull(fetchList);
	}

	@Test
	public void testPoll_consumedElements() {
		assertEquals(0, fetchList.getConsumedElements());
		fetchList.poll();
		assertEquals(1, fetchList.getConsumedElements());
	}
	
	@Test
	public void testFill_totalElements() {
		assertEquals(FETCH_SIZE, fetchList.getTotalElements());
	}
	
	@Test
	public void testPoll() {
		for (int i = 0; i < TOTAL_ELEMENTS; i++) {
			assertEquals(i, fetchList.getConsumedElements());
			assertEquals(list.get(i), fetchList.poll());
		}
		assertEquals(TOTAL_ELEMENTS, fetchList.getConsumedElements());
		assertEquals(TOTAL_ELEMENTS, fetchList.getTotalElements());
	}
	
	@Test
	public void testPoll_null() {
		for (int i = 0; i < TOTAL_ELEMENTS; i++) {
			fetchList.poll();
		}
		assertNull(fetchList.poll());
		assertNull(fetchList.poll());
		assertNull(fetchList.poll());
	}
}
