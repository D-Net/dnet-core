package eu.dnetlib.enabling.resultset;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Lists;

public class StreamingResultSetListenerTest {

	private StreamingResultSetListener listener;
	private List<String> data;

	@Before
	public void setUp() throws Exception {
		data = Lists.newArrayList("uno", "due", "tre", "quattro");
		listener = new StreamingResultSetListener(data.iterator(), data.size());
	}

	@Test
	public void testGetResult() {
		assertEquals(data.size(), listener.getSize());

		final List<String> res = listener.getResult(1, 2);
		assertEquals(2, res.size());
		assertEquals("uno", res.get(0));
		assertEquals("due", res.get(1));
	}

	@Test
	public void testGetResult2() {
		listener.getResult(1, 2);
		final List<String> res = listener.getResult(3, 4);
		assertEquals(2, res.size());
		assertEquals("tre", res.get(0));
		assertEquals("quattro", res.get(1));
	}

	@Test
	public void testGetResult_retry() {
		listener.getResult(1, 2);
		final List<String> res = listener.getResult(1, 2);
		assertEquals(2, res.size());
		assertEquals("uno", res.get(0));
		assertEquals("due", res.get(1));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetResult_random() {
		listener.getResult(1, 2);
		listener.getResult(2, 3);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetResult_skip() {
		listener.getResult(1, 2);
		listener.getResult(4, 4);
	}
}
