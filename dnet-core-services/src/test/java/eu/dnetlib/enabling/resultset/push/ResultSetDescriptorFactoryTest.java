package eu.dnetlib.enabling.resultset.push;


import static org.junit.Assert.*;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.dnetlib.miscutils.factory.Factory;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class ResultSetDescriptorFactoryTest {

	@Resource
	Factory<ResultSetDescriptor> factory;

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testFactory() {
		ResultSetDescriptor desc1 = factory.newInstance();
		ResultSetDescriptor desc2 = factory.newInstance();

		assertTrue("check different instances", desc1 != desc2);
		assertEquals("check that it's a spring prototype", 200, desc1.getRangeLength());
		assertEquals("check that it's a spring prototype", 200, desc2.getRangeLength());
	}
}
