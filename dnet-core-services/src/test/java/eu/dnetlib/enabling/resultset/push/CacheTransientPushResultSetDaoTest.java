package eu.dnetlib.enabling.resultset.push;

import java.util.List;

import net.sf.ehcache.CacheManager;

import org.junit.After;

import eu.dnetlib.miscutils.cache.Cache;
import eu.dnetlib.miscutils.cache.EhCache;
import eu.dnetlib.miscutils.factory.Factory;

/**
 * concrete push resultset dao test.
 * 
 * @author marko
 * 
 */
public class CacheTransientPushResultSetDaoTest extends AbstractTransientPushResultSetDaoTest { // NOPMD

	/**
	 * max in memory elements.
	 */
	private static final int MAX_IN_MEM = 100;

	/**
	 * default time.
	 */
	private static final int DEFAULT_TTI = 1000;

	/**
	 * default time to live.
	 */
	private static final int DEFAULT_TTL = 1000;

	/**
	 * cache manager.
	 */
	private final transient CacheManager cacheManager = CacheManager.create();

	/**
	 * clean cache after test.
	 */
	@After
	public void cleanCache() {
		cacheManager.removeAllCaches();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.resultset.push.AbstractTransientPushResultSetDaoTest#newInstance()
	 */
	@Override
	protected TransientPushResultSetDao newInstance() {
		final net.sf.ehcache.Cache ehcache = new net.sf.ehcache.Cache("testCache", MAX_IN_MEM, false, false, DEFAULT_TTL, DEFAULT_TTI);
		cacheManager.addCache(ehcache);

		final net.sf.ehcache.Cache rsehcache = new net.sf.ehcache.Cache("rsetCache", MAX_IN_MEM, false, false, DEFAULT_TTL, DEFAULT_TTI);
		cacheManager.addCache(rsehcache);

		final CacheTransientResultSetDaoImpl resultSet = new CacheTransientResultSetDaoImpl();
		final Cache<String, List<String>> cache = new EhCache<String, List<String>>(ehcache);
		final Cache<String, ResultSetDescriptor> rscache = new EhCache<String, ResultSetDescriptor>(rsehcache);
		resultSet.setCache(cache);
		resultSet.setResultSetCache(rscache);

		resultSet.setResultSetDescriptorFactory(new Factory<ResultSetDescriptor>() {

			@Override
			public ResultSetDescriptor newInstance() {
				return new ResultSetDescriptor();
			}
		});
		return resultSet;
	}

}
