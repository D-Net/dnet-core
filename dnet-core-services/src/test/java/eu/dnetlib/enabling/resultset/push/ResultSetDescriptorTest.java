package eu.dnetlib.enabling.resultset.push;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;

import eu.dnetlib.enabling.resultset.push.ResultSetDescriptor.Range;

public class ResultSetDescriptorTest {

	private static final Log log = LogFactory.getLog(ResultSetDescriptorTest.class); // NOPMD by marko on 11/24/08 5:02 PM

	private transient ResultSetDescriptor descriptor;

	@Before
	public void setUp() throws Exception {
		descriptor = new ResultSetDescriptor();
	}

	@Test
	public void testGetRangesContaining() {
		log.debug("--- 1, 1");
		for(Range i : descriptor.getRangesContaining(1, 1))
			log.debug("i " + i);

		log.debug("--- 2, 1000");
		for(Range i : descriptor.getRangesContaining(2, 1000))
			log.debug("i " + i);

		log.debug("--- 5, 2000");
		for(Range i : descriptor.getRangesContaining(5, 2000))
			log.debug("i " + i);

		log.debug("--- 5, 2001");
		for(Range i : descriptor.getRangesContaining(5, 2001))
			log.debug("i " + i);
	}

	@Test
	public void testNext() {
	}

}
