package eu.dnetlib.enabling.hnm;

import eu.dnetlib.enabling.hnm.rmi.HostingNodeManagerService;
import eu.dnetlib.enabling.tools.AbstractBaseService;

/**
 * HNMService implementation.
 *
 * @author michele
 *
 */
public class HostingNodeManagerServiceImpl  extends AbstractBaseService implements HostingNodeManagerService {
	
	@Override
	public String echo(String s) {
		return s;
	}

}
