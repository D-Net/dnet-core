package eu.dnetlib.enabling.resultset;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

/**
 * This factory creates resultset bound to a given resultset registry shared with a resultset service which exposed them
 * to the outside world.
 *
 * It's tightly coupled with a local instance of the ResultSetService.
 *
 * @see eu.dnetlib.enabling.resultset.ResultSetServiceImpl
 *
 * @author marko
 *
 */
public class LocalResultSetFactoryImpl extends AbstractResultSetFactory {

	/**
	 * {@inheritDoc}
	 *
	 * @see eu.dnetlib.enabling.resultset.ResultSetFactory#createResultSet(eu.dnetlib.enabling.resultset.ResultSetListener)
	 */
	@Override
	public W3CEndpointReference createResultSet(final ResultSetListener provider) {
		return registerResultSet(createInstance(provider));
	}

	/**
	 * create a new local resultset instance. Subclasses may override this to supply additional configuration.
	 *
	 * @param provider provider
	 * @return a new resultset instance.
	 */
	protected LocalResultSetImpl createInstance(final ResultSetListener provider) {
		return new LocalResultSetImpl(provider);
	}

}
