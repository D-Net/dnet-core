package eu.dnetlib.enabling.resultset;

import java.util.List;

import eu.dnetlib.enabling.resultset.observer.Observable;

/**
 * A ResultSet represents a set of data results.
 * 
 * @author marko
 * 
 */
public interface ResultSet extends Observable {
	/**
	 * get the resource identifier.
	 * 
	 * @return rsId
	 */
	String getIdentifier();

	/**
	 * sets the resource identifier. The ResultSetRegistry will assign a new rsId.
	 * 
	 * @param rsId allocated resultset identifier
	 */
	void setIdentifier(String rsId);

	/**
	 * get a 'page' of results.
	 * 
	 * @param fromPosition
	 *            from 1
	 * @param toPosition
	 *            last included
	 * @return a page of data
	 */
	List<String> getResults(int fromPosition, int toPosition);

	/**
	 * get the number of result elements present in the resultset.
	 * 
	 * @return number of results
	 */
	int getNumberOfResults();

	/**
	 * Tells if the resultset is open or closed.
	 * 
	 * @return true if open
	 */
	boolean isOpen();

	/**
	 * close a resultset.
	 */
	void close();
	
	/**
	 * Tells if the resultset is destroyed.
	 * 
	 * @return true if the resultset is destroyed. 
	 */
	boolean isDestroyed();
	
	/**
	 * Destroy the resultset and free associated resources, remove it from the ResultSetRegistry.
	 * After calling this method, the resultset is no more accessible from other services.
	 */
	void destroy();

}
