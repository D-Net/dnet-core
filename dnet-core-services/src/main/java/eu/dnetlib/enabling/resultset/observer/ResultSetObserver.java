package eu.dnetlib.enabling.resultset.observer;

import eu.dnetlib.enabling.resultset.ResultSet;


/**
 * Implement the same interface as JDK java.util.Observer as java interface (not as a class) and introduce static type
 * safeness through generics.
 * 
 * @author marko, claudio, alessia, michele
 * 
 */
public interface ResultSetObserver {
	/**
	 * gets notified when an observed object is updated.
	 * 
	 * @param observed
	 *            observed object which modification triggered this call.
	 * @param arg
	 *            arg
	 */
	void update(ResultSet observed, Object arg);
}
