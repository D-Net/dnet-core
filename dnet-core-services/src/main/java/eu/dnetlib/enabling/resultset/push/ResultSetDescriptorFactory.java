package eu.dnetlib.enabling.resultset.push;

import eu.dnetlib.miscutils.factory.Factory;

public interface ResultSetDescriptorFactory extends Factory<ResultSetDescriptor> {

}
