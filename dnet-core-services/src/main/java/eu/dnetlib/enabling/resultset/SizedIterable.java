package eu.dnetlib.enabling.resultset;

public interface SizedIterable<T> extends Iterable<T> {

	public int getNumberOfElements();
	
}
