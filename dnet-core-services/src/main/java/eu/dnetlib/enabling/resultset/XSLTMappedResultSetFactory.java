package eu.dnetlib.enabling.resultset;

import java.util.Map;
import java.util.UUID;

import javax.xml.transform.Source;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.ws.wsaddressing.W3CEndpointReference;

import org.springframework.core.io.Resource;

import eu.dnetlib.miscutils.functional.xml.ApplyXslt;

/**
 * Create a mapped resultset using a given XSLT.
 * 
 * @author marko
 * 
 */
public class XSLTMappedResultSetFactory extends MappedResultSetFactory {

	public W3CEndpointReference createMappedResultSet(final W3CEndpointReference source, final String xslt) {
		return createMappedResultSet(source, new ApplyXslt(xslt));
	}

	/**
	 * Create a mapped resultset using a given XSLT.
	 * 
	 * @param source
	 *            source
	 * @param xslt
	 *            XSLT source
	 * @return epr of mapped resultset
	 * @throws TransformerConfigurationException
	 *             could happen
	 */
	public W3CEndpointReference createMappedResultSet(final W3CEndpointReference source, final String xslt, final String name)
			throws TransformerConfigurationException {
		return createMappedResultSet(source, new ApplyXslt(xslt, name));
	}

	public W3CEndpointReference createMappedResultSet(final W3CEndpointReference source, final Resource xslt) {
		return createMappedResultSet(source, new ApplyXslt(xslt));
	}

	public W3CEndpointReference createMappedResultSet(final W3CEndpointReference source, final Source xslt) {
		return createMappedResultSet(source, new ApplyXslt(xslt));
	}
	
	public W3CEndpointReference createMappedResultSet(final W3CEndpointReference source, final Resource xslt, final Map<String, String> parameters) {
		return createMappedResultSet(source, new ApplyXslt(xslt, parameters));
	}
	
	public W3CEndpointReference createMappedResultSet(final W3CEndpointReference source, final String xslt, final Map<String, String> parameters) {
		return createMappedResultSet(source, new ApplyXslt(xslt, "rs-" + UUID.randomUUID(), parameters));
	}

	/**
	 * Create a mapped resultset using a given XSLT.
	 * 
	 * @param source
	 *            source
	 * @param xslt
	 *            XSLT
	 * @return epr of mapped resultset
	 * @throws TransformerFactoryConfigurationError
	 * @throws TransformerConfigurationException
	 *             could happen
	 */
	public W3CEndpointReference createMappedResultSet(final W3CEndpointReference source, final Source xslt, final String name) {
		return createMappedResultSet(source, new ApplyXslt(xslt, name));
	}

}
