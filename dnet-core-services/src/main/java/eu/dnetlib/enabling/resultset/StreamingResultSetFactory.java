package eu.dnetlib.enabling.resultset;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

import org.springframework.beans.factory.annotation.Required;

public class StreamingResultSetFactory {
	/**
	 * underlying resultset factory, which exposes local resultsets to the world.
	 */
	private ResultSetFactory resultSetFactory;

	/**
	 * @param items
	 * @param size the size of the iterable
	 * @return
	 */
	public W3CEndpointReference createStreamingResultSet(final Iterable<String> items, int size) {
		return resultSetFactory.createResultSet(new StreamingResultSetListener(items.iterator(), size));
	}

	
	public ResultSetFactory getResultSetFactory() {
		return resultSetFactory;
	}

	@Required
	public void setResultSetFactory(ResultSetFactory resultSetFactory) {
		this.resultSetFactory = resultSetFactory;
	}

}
