package eu.dnetlib.enabling.resultset.observer;

import eu.dnetlib.enabling.resultset.ResultSetRegistry;

/**
 * Declares the JDK-like standard observable pattern as a java interface.
 * 
 * @author marko, claudio, alessia, michele
 */
public interface Observable {
	/**
	 * add a given observer to this object.
	 * 
	 * @param observer
	 *            observer
	 */
	void addObserver(ResultSetRegistry observer);
}
