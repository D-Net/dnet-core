package eu.dnetlib.enabling.resultset.push;


/**
 * creates a transient push resultset.
 * 
 * @author marko
 *
 */
public class TransientPushResultSetFactory extends AbstractPushResultSetFactoryImpl {

	/**
	 * dao.
	 */
	private TransientPushResultSetDao dao;
	
	/** 
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.resultset.push.AbstractPushResultSetFactoryImpl#newInstance()
	 */
	@Override
	protected PushResultSet newInstance() {
		return new TransientPushResultSetImpl(dao);
	}

	public TransientPushResultSetDao getDao() {
		return dao;
	}

	public void setDao(final TransientPushResultSetDao dao) {
		this.dao = dao;
	}

}
