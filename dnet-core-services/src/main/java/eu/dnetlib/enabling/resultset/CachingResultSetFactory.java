package eu.dnetlib.enabling.resultset;

import javax.annotation.Resource;
import javax.xml.ws.wsaddressing.W3CEndpointReference;

import org.springframework.beans.factory.annotation.Required;

import eu.dnetlib.enabling.tools.ServiceResolver;

public class CachingResultSetFactory {
	/**
	 * underlying resultset factory, which exposes local resultsets to the world.
	 */
	private ResultSetFactory resultSetFactory;

	@Resource
	private ServiceResolver serviceResolver;
	
	/**
	 * @param items
	 * @param size
	 *            the size of the iterable
	 * @return
	 */
	public W3CEndpointReference createCachingResultSet(W3CEndpointReference epr) {
		return resultSetFactory.createResultSet(new CachingResultSetListener(epr, serviceResolver));
	}

	public ResultSetFactory getResultSetFactory() {
		return resultSetFactory;
	}

	@Required
	public void setResultSetFactory(ResultSetFactory resultSetFactory) {
		this.resultSetFactory = resultSetFactory;
	}

	public ServiceResolver getServiceResolver() {
		return serviceResolver;
	}

	public void setServiceResolver(ServiceResolver serviceResolver) {
		this.serviceResolver = serviceResolver;
	}

}
