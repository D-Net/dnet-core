package eu.dnetlib.enabling.resultset.push;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

/**
 * creates a new push resultset.
 *
 * @author marko
 *
 */
public interface PushResultSetFactory {

	/**
	 * create a new push resultset.
	 *
	 * @param maxIdleTime max time the resultset can be idle
	 * @return push resultset epr
	 */
	W3CEndpointReference createPushResultSet(int maxIdleTime);

}
