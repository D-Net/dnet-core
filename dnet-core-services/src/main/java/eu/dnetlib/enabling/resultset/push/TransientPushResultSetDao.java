package eu.dnetlib.enabling.resultset.push;

import java.util.List;

/**
 * offer access to the underlying in-memory or pseudo-memory (serialization backed caches) storage for the transient
 * push-resultsets.
 *
 * This type of push resultsets doesn't even try to make efficient access to records, they just keep them as big lists
 * on the underlying storage. Actual implementations may decide to keep them on the heap or to use some caching
 * technology like ehcache.
 *
 * @author marko
 *
 */
public interface TransientPushResultSetDao {
	/**
	 * add elements to a given key.
	 *
	 * @param key
	 *            usually a rsid.
	 * @param elements
	 *            list of elements
	 */
	void addElements(String key, List<String> elements);

	/**
	 * obtain content for a given key (resultset) in a given range (1 based).
	 *
	 * @param key
	 *            usually a rsid
	 * @param fromPosition from position (1 based, inclusive)
	 * @param toPosition to position (1 based, inclusive)
	 * @return all the elements for a given key
	 */
	List<String> getElements(String key, int fromPosition, int toPosition);

	/**
	 * get the resultset size (number of elements).
	 *
	 * @param key usually a rsid
	 * @return resultset size
	 */
	int getSize(String key);
}
