package eu.dnetlib.enabling.resultset;

import java.util.List;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

import eu.dnetlib.enabling.tools.ServiceResolver;
import eu.dnetlib.miscutils.functional.IdentityFunction;
import eu.dnetlib.miscutils.functional.UnaryFunction;

/**
 * A resultset record counter.
 * 
 * @author claudio
 * 
 */
public class CountingResultSet extends MappedResultSet {

	/**
	 * counter.
	 */
	private int count;


	/**
	 * @param epr
	 * @param serviceResolver
	 */
	public CountingResultSet(final W3CEndpointReference epr, final ServiceResolver serviceResolver) {
		this(epr, new IdentityFunction<String>(), serviceResolver);
	}

	/**
	 * @param epr
	 * @param mapper
	 * @param serviceResolver
	 */
	public CountingResultSet(final W3CEndpointReference epr, final UnaryFunction<String, String> mapper, final ServiceResolver serviceResolver) {
		super(epr, mapper, serviceResolver);
		count = 0;
	}

	/**
	 * method sets the counter to the higher allowed value of toPosition.
	 */
	@Override
	public List<String> getResult(int fromPosition, int toPosition) {

		if (toPosition > count)
			count = toPosition;
		if (toPosition > super.getSize())
			count = super.getSize();

		return super.getResult(fromPosition, toPosition);
	}

	public int getCount() {
		return count;
	}
}
