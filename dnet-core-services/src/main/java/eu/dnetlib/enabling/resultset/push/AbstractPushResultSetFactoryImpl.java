package eu.dnetlib.enabling.resultset.push;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

import eu.dnetlib.enabling.resultset.ResultSetServiceImpl;

/**
 * push resultset factory.
 *
 * @author marko
 *
 */
public abstract class AbstractPushResultSetFactoryImpl implements PushResultSetFactory {
	/**
	 * resultset service.
	 */
	private ResultSetServiceImpl resultSetService;

	/**
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.resultset.push.PushResultSetFactory#createPushResultSet(int)
	 */
	@Override
	public W3CEndpointReference createPushResultSet(final int maxIdleTime) {
		final PushResultSet resultSet = newInstance();
		resultSetService.getResultsetRegistry().addResultSet(resultSet, maxIdleTime);
		return resultSetService.getEprBuilder().getEndpointReference(resultSetService.getEndpoint(), resultSet.getIdentifier());
	}

	/**
	 * subclasses should override it and create a new push resultset instance.
	 * @return push resultset instance
	 */
	protected abstract PushResultSet newInstance();

	public ResultSetServiceImpl getResultSetService() {
		return resultSetService;
	}

	public void setResultSetService(final ResultSetServiceImpl resultSetService) {
		this.resultSetService = resultSetService;
	}

}
