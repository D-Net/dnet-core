package eu.dnetlib.enabling.resultset;


/**
 * A ResultSetListener is a pull interface to a ResultSet.
 * 
 * The Resultset will call method of ResultSetListener implementors whenever it needs to pull some data and provide it
 * to clients. The ResultSet service acts as a decouple point between the producer and the consumer.
 * 
 * @author marko
 * 
 */
public interface ResultSetListener extends TypedResultSetListener<String> {

}
