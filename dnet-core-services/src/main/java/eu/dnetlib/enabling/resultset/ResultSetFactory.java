package eu.dnetlib.enabling.resultset;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

/**
 * Creates a resultset service from a producer.
 * 
 * @author marko
 *
 */
public interface ResultSetFactory {
	/**
	 * TODO other parameters like expiration time etc.
	 * 
	 * @param provider a resultset listener which provides the data
	 * @return an EPR to the resultset.
	 */
	W3CEndpointReference createResultSet(ResultSetListener provider);
}
