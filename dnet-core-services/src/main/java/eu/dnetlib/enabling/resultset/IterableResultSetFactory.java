package eu.dnetlib.enabling.resultset;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

import org.springframework.beans.factory.annotation.Required;

/**
 * 
 * @author claudio
 *
 */
public class IterableResultSetFactory {

	/**
	 * {@link ResultSetFactory}
	 */
	private LocalOpenResultSetFactoryImpl resultSetFactory;
	
	/**
	 * 
	 */
	private int fetchSize;
	
	/**
	 * 
	 * @param source
	 * @return
	 */
	public W3CEndpointReference createIterableResultSet(final Iterable<String> source) {
		return resultSetFactory.createResultSet(new IterableResultSet(source, fetchSize));
	}
	
	/**
	 * 
	 * @return
	 */
	public LocalOpenResultSetFactoryImpl getResultSetFactory() {
		return resultSetFactory;
	}

	/**
	 * 
	 * @param resultSetFactory
	 */
	@Required
	public void setResultSetFactory(final LocalOpenResultSetFactoryImpl resultSetFactory) {
		this.resultSetFactory = resultSetFactory;
	}
	
	/**
	 * 
	 * @param fetchSize
	 */
	@Required
	public void setFetchSize(int fetchSize) {
		this.fetchSize = fetchSize;
	}

	/**
	 * 
	 * @return
	 */
	public int getFetchSize() {
		return fetchSize;
	}

}
