package eu.dnetlib.enabling.resultset;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

/**
 * Create a new resultset which takes each record of the input resultset and counts them.
 * 
 * @author claudio
 * 
 */
public class CountingResultSetFactory extends MappedResultSetFactory {

	/**
	 * Create a new resultset which takes each record of the input resultset and counts them.
	 * 
	 * @param source
	 *            source resultset epr
	 * @param mapper
	 *            mapper function
	 * @return mapped resultset epr
	 */
	public W3CEndpointReference createCountingResultSet(final W3CEndpointReference source) {
		return getResultSetFactory().createResultSet(new CountingResultSet(source, getServiceResolver()));
	}

}
