package eu.dnetlib.enabling.resultset;

/**
 * ResultSet listeners can add this interface in order to receive the ResultSet they are bound to.
 * 
 * @author marko
 *
 */
public interface ResultSetAware {
	/**
	 * sets the resultset instance.
	 * 
	 * @param resultSet the resultset instance
	 */
	void setResultSet(ResultSet resultSet);
}
