package eu.dnetlib.enabling.resultset;

import java.util.List;

/**
 * A TypedResultSetListener is a pull interface to a ResultSet.
 * 
 * The Resultset will call method of ResultSetListener implementors whenever it needs to pull some data and provide it
 * to clients. The ResultSet service acts as a decouple point between the producer and the consumer.
 * 
 * @author marko
 * 
 */
public interface TypedResultSetListener<T> {

	/**
	 * get a single page of results.
	 * 
	 * @param fromPosition
	 *            from 1
	 * @param toPosition
	 *            last included
	 * @return page of results
	 */
	List<T> getResult(int fromPosition, int toPosition);

	/**
	 * get number of elements.
	 * 
	 * @return number of elements
	 */
	int getSize();

}
