package eu.dnetlib.enabling.resultset;

import eu.dnetlib.enabling.resultset.observer.ResultSetObserver;

/**
 * Instances of ResultSetRegistry manage a set of resultset objects and manage their garbage collection.
 *
 * @author marko
 *
 */
public interface ResultSetRegistry extends ResultSetObserver {
	/**
	 * add a resultset object to the registry.
	 *
	 * The given resultset object from now on is managed by the resultset registry.
	 *
	 * @param resultSet
	 *            a resultset object
	 */
	void addResultSet(ResultSet resultSet);
	
	/**
	 * add a resultset object to the registry.
	 *
	 * The given resultset object from now on is managed by the resultset registry.
	 *
	 * @param resultSet
	 *            a resultset object
	 * @param identifier the identifier you want for the resultset
	 */
	void addResultSet(ResultSet resultSet, String identifier);

	/**
	 * add a resultset object to the registry.
	 *
	 * The given resultset object from now on is managed by the resultset registry.
	 *
	 * @param resultSet
	 *            a resultset object
	 * @param maxIdleTime
	 *            max idle time
	 */
	void addResultSet(ResultSet resultSet, int maxIdleTime);
	
	/**
	 * add a resultset object to the registry.
	 *
	 * The given resultset object from now on is managed by the resultset registry.
	 *
	 * @param resultSet
	 *            a resultset object
	 * @param identifier the identifier you want for the resultset
	 * @param maxIdleTime max idle time
	 */
	void addResultSet(ResultSet resultSet, String identifier, int maxIdleTime);

	/**
	 * obtain the resultset with the given id.
	 *
	 * @param rsId
	 *            resultset id
	 * @return the resultset object matching the rsId or null
	 */
	ResultSet getResultSetById(String rsId);

	/**
	 * obtain the resultset's maxIdleTime parameter for a resultset with the given id.
	 *
	 * @param rsId
	 *            resultset id
	 * @return max idle time in seconds, as specified when the resultset has been registered
	 */
	int getMaxIdleTimeById(String rsId);

}
