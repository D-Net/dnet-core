package eu.dnetlib.enabling.resultset;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

import org.springframework.beans.factory.annotation.Required;

public class EPRPoolingLocalOpenResultSetFactoryImpl extends LocalOpenResultSetFactoryImpl {
	private ResultSetEPRPool pool;
	
	@Override
	public W3CEndpointReference registerResultSet(ResultSet resultSet) {
		return pool.registerResultSet(resultSet);
	}

	public ResultSetEPRPool getPool() {
		return pool;
	}

	@Required
	public void setPool(ResultSetEPRPool pool) {
		this.pool = pool;
	}
}
