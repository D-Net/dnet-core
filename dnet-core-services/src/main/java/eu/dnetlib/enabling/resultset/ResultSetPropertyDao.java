package eu.dnetlib.enabling.resultset;

import java.util.Map;

/**
 * access resultset properties.
 * 
 * @author marko
 *
 */
public interface ResultSetPropertyDao {
	/**
	 * return a map of custom resultset properties.
	 * 
	 * @param resultSet resultset
	 * @return property map
	 */
	Map<String, String> getProperties(ResultSet resultSet);
}
