package eu.dnetlib.enabling.resultset;

/**
 * Creates an "open" (unbounded, unfreezed) local resultset.
 * 
 * @author marko
 *
 */
public class LocalOpenResultSetFactoryImpl extends LocalResultSetFactoryImpl {

	/** 
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.resultset.LocalResultSetFactoryImpl#createInstance(eu.dnetlib.enabling.resultset.ResultSetListener)
	 */
	@Override
	protected LocalResultSetImpl createInstance(final ResultSetListener provider) {
		final LocalResultSetImpl instance = super.createInstance(provider);
		instance.setOpen(true);
		return instance;
	}


}
