package eu.dnetlib.enabling.resultset;

import java.util.HashMap;
import java.util.Map;

/**
 * fake resultset property dao.
 * 
 * @author marko
 *
 */
public class ResultSetPropertyDaoImpl implements ResultSetPropertyDao {

	/** 
	 * {@inheritDoc}
	 * @see eu.dnetlib.enabling.resultset.ResultSetPropertyDao#getProperties(eu.dnetlib.enabling.resultset.ResultSet)
	 */
	@Override
	public Map<String, String> getProperties(final ResultSet resultSet) {
		return new HashMap<String, String>();
	}

}
