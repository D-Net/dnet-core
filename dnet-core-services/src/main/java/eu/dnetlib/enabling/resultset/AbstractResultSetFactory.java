package eu.dnetlib.enabling.resultset;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

/**
 * Resultset factories bound to a resultset service, from which it takes the resultset registry and the epr builder.
 *
 * @author marko
 *
 */
public abstract class AbstractResultSetFactory implements ResultSetFactory {

	/**
	 * resultset service.
	 */
	private ResultSetServiceImpl resultSetService;

	/**
	 * Register a resultset instance to the underlying resultset registry and returns
	 * and EPR to accessible through the resultset service.
	 *
	 * @param resultSet resultset instance
	 * @return epr to the newly created resultset
	 */
	public W3CEndpointReference registerResultSet(final ResultSet resultSet) {
		resultSetService.getResultsetRegistry().addResultSet(resultSet);
		return resultSetService.getEprBuilder().getEndpointReference(resultSetService.getEndpoint(), resultSet.getIdentifier());
	}

	public ResultSetServiceImpl getResultSetService() {
		return resultSetService;
	}

	public void setResultSetService(final ResultSetServiceImpl resultSetService) {
		this.resultSetService = resultSetService;
	}

}
