package eu.dnetlib.enabling.resultset.push;

import java.util.List;

import eu.dnetlib.enabling.resultset.ResultSet;

/**
 * resultset which receive data through the "push" interface, i.e. data is directly appended to it.
 * 
 * @author marko
 *
 */
public interface PushResultSet extends ResultSet {

	/**
	 * add a list of elements at the end of the resultset.
	 * 
	 * @param elements list of elements
	 */
	void addElements(List<String> elements);

}
