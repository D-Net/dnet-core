package eu.dnetlib.enabling.resultset;

import javax.xml.ws.wsaddressing.W3CEndpointReference;

import eu.dnetlib.enabling.tools.ServiceResolver;
import eu.dnetlib.miscutils.functional.UnaryFunction;

/**
 * Create a new resultset which takes each record of the input resultset and applies the mapping function to it.
 *
 * @author marko
 *
 */
public class MappedResultSetFactory {

	/**
	 * underlying resultset factory, which exposes local resultsets to the world.
	 */
	private ResultSetFactory resultSetFactory;

	/**
	 * service resolver, transforms eprs to services.
	 */
	private ServiceResolver serviceResolver;

	/**
	 * Create a new resultset which takes each record of the input resultset and applies the mapping function to it.
	 *
	 * @param source source resultset epr
	 * @param mapper mapper function
	 * @return mapped resultset epr
	 */
	public W3CEndpointReference createMappedResultSet(final W3CEndpointReference source, final UnaryFunction<String, String> mapper) {
		return resultSetFactory.createResultSet(new MappedResultSet(source, mapper, serviceResolver));
	}

	public ResultSetFactory getResultSetFactory() {
		return resultSetFactory;
	}

	public void setResultSetFactory(final ResultSetFactory resultSetFactory) {
		this.resultSetFactory = resultSetFactory;
	}

	public ServiceResolver getServiceResolver() {
		return serviceResolver;
	}

	public void setServiceResolver(final ServiceResolver serviceResolver) {
		this.serviceResolver = serviceResolver;
	}

}
