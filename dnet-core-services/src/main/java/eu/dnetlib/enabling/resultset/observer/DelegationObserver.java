package eu.dnetlib.enabling.resultset.observer;

import eu.dnetlib.enabling.resultset.ResultSet;
import eu.dnetlib.enabling.resultset.ResultSetRegistry;

/**
 * This class delegates an observation event from the java.util.Observable producer to the real consumer which is not a
 * java.util.Observer (as expected by java.util.Observable) but instead a miscutils Observer.
 * 
 * @author marko, claudio, alessia, michele
 * 
 */
public class DelegationObserver implements java.util.Observer {
	/**
	 * 
	 */
	private transient final ResultSet observable;
	private transient final ResultSetRegistry observer;

	public DelegationObserver(final ResultSet observable, final ResultSetRegistry observer) {
		this.observable = observable;
		this.observer = observer;
	}

	@Override
	public void update(final java.util.Observable ignored, final Object arg) {
		observer.update(observable, arg);
	}
}