package eu.dnetlib.enabling.hcm.sn;

import eu.dnetlib.enabling.is.sn.rmi.ISSNException;

/**
 * This component takes care of subscribing the MSRO service to interesting events.
 * 
 * @author marko
 *
 */
public interface HCMSubscriber {
	/**
	 * performs the subscription.
	 * @throws ISSNException could happen
	 */
	void subscribeAll() throws ISSNException;
}
