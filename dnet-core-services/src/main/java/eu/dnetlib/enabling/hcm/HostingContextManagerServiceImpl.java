package eu.dnetlib.enabling.hcm;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Required;

import eu.dnetlib.enabling.hcm.rmi.HostingContextManagerService;
import eu.dnetlib.enabling.tools.AbstractBaseService;
import eu.dnetlib.enabling.tools.blackboard.NotificationHandler;

/**
 * CNR HostingContextManagerService implementation. Will conflict with NKUA! yes this is ok.
 * 
 * @author marko
 * 
 */
public class HostingContextManagerServiceImpl extends AbstractBaseService implements HostingContextManagerService {

	/**
	 * logger.
	 */
	private static final Log log = LogFactory.getLog(HostingContextManagerServiceImpl.class); // NOPMD by marko on 11/24/08 5:02 PM

	/**
	 * notification handler.
	 */
	private NotificationHandler notificationHandler; // NOPMD

	@Override
	public void notify(String subscrId, String topic, String isId, String message) {
		if (log.isDebugEnabled()) {
			log.debug("---- service got notification ----");
			log.debug("subscrId: " + subscrId);
			log.debug("topic " + topic);
			log.debug("isId " + isId);
			log.debug("msg: " + message);
			log.debug("____ now processing the notification ____");
		}
		getNotificationHandler().notified(subscrId, topic, isId, message);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see eu.dnetlib.enabling.tools.AbstractBaseService#start()
	 */
	@Override
	public void start() {
		log.info("staring hosting context manager");
	}

	public NotificationHandler getNotificationHandler() {
		return notificationHandler;
	}

	@Required
	public void setNotificationHandler(NotificationHandler notificationHandler) {
		this.notificationHandler = notificationHandler;
	}

}
