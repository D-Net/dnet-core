package eu.dnetlib.enabling.hcm.sn;

import java.util.List;

import com.google.common.collect.Lists;
import eu.dnetlib.enabling.actions.SubscriptionAction;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * This class acts as a simple registry of msro subscription actions.
 * 
 * <p>
 * Subscription actions are autodetected using spring autowiring from the current application context and are available for the manager for
 * handling subscriptions and delivering notification to particular code interested in those notifications.
 * </p>
 * 
 * @author marko
 * 
 */
@SuppressWarnings("rawtypes")
public class HCMSubscriptionListFactory implements FactoryBean<List<SubscriptionAction>> {

	/**
	 * actions. Spring injects all the declared SubscriptionActions here.
	 */
	@Autowired(required = false)
	private List<SubscriptionAction> actions = Lists.newArrayList();

	public List<SubscriptionAction> getActions() {
		return actions;
	}

	public void setActions(final List<SubscriptionAction> actions) {
		this.actions = actions;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @see org.springframework.beans.factory.FactoryBean#getObject()
	 */
	@Override
	public List<SubscriptionAction> getObject() {
		return getActions();
	}

	@Override
	public Class<List> getObjectType() {
		return List.class;
	}

	@Override
	public boolean isSingleton() {
		return false;
	}
}
